/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.
*
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
*
* Copyright (c) 1999-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
/****************************************************************************
 *
 * Module:    cssdbm.h
 *
 * Purpose:   Application Programming Interface to
 *            Custom System Selection.
 *
 ****************************************************************************
 *
 *
 *                          PVCS Header Information
 *
 *
 *       $Log: CSS_PRL.H $
 *
 ****************************************************************************
 ****************************************************************************/
#ifndef CSS_DBM_H
#define CSS_DBM_H

/*****************************************************************************/
/******************************** INCLUDES ***********************************/
/*****************************************************************************/
#include "cssdefs.h"
#include "dbmapi.h"
#include "pswnam.h"
#include "pswapi.h"
#include "uimapi.h"
#include "valapi.h"
#include "valdbmapi.h"

/*****************************************************************************/
/************************ DATA STRUCTURES & DEFINES **************************/
/*****************************************************************************/
typedef NV_PACKED_PREFIX struct
{
    uint16      mpssPilotStrengthThresh;
    uint32      tBsr[CSS_1X_NUM_T_BSR];
    uint32      reservedTBsr[CSS_1X_NUM_T_BSR_MAX-CSS_1X_NUM_T_BSR]; /* reserved for possible expansion of TBsr; delete once max is reached */
    uint32      tAvoidance[CSS_1X_NUM_T_AVOIDANCE];
    uint32      reservedTAvoid[CSS_1X_NUM_T_AVOIDANCE_MAX-CSS_1X_NUM_T_AVOIDANCE]; /* reserved for possible expansion of TAvoidance; delete once max is reached */
    uint16      manualAvoid1XSys[MAX_MANUAL_AVOID_1XSYS];
    uint32      tPsDelay;
    uint32      tEmergencySysLostScan;
    cssSidNid   operSidNid; /* Operator SID/NID of the first MRU entry */
} NV_PACKED_POSTFIX  css1xDbmParameters;

typedef NV_PACKED_PREFIX struct
{
    uint16   mpssPilotStrengthThresh; /* unused for now */
    uint32   tBsr[CSS_DO_NUM_T_BSR];
    uint32   reservedTBsr[CSS_DO_NUM_T_BSR_MAX-CSS_DO_NUM_T_BSR]; /* reserved for possible expansion of TBsr; delete once max is reached */
    uint32   tAvoidance[CSS_DO_NUM_T_AVOIDANCE];     /* unused for now */
    uint32   reservedTAvoid[CSS_DO_NUM_T_AVOIDANCE_MAX-CSS_DO_NUM_T_AVOIDANCE]; /* reserved for possible expansion of TAvoidance; delete once max is reached */
} NV_PACKED_POSTFIX  cssDODbmParameters;

#ifdef MTK_DEV_C2K_IRAT
typedef NV_PACKED_PREFIX struct
{
    Bool    b1xChnlLocked;
    uint16  rttChnl;
    Bool    bDoChnlLocked;
    uint16  doChnl;
} NV_PACKED_POSTFIX cssChannleLockInfo;
#endif

typedef NV_PACKED_PREFIX struct
{
#ifdef MTK_DEV_C2K_IRAT
    cssChannleLockInfo   chnlInfo;
#endif
    uint16      roamIndForNonPrlSysButHomeInNam;
    uint8       homeEriValNum;
    uint16      homeEriVals[MAX_ERI_HOME_VALUES];
    uint8       intlEriValNum;  /* international */
    uint16      intlEriVals[MAX_ERI_INTERNATIONAL_VALUES];
} NV_PACKED_POSTFIX  cssMiscDbmParameters;

/* do not modify segment definitions below. To add new parameters, use structures above */
typedef NV_PACKED_PREFIX struct
{
    css1xDbmParameters data;
    UINT16             checksum;  /* checksum,no used. */
    UINT8              padding[DBM_CSS_1X_SIZE - sizeof(uint16) - sizeof(css1xDbmParameters)];
} NV_PACKED_POSTFIX  css1xDbmSegment;

typedef NV_PACKED_PREFIX struct
{
    cssDODbmParameters data;
    UINT16             checksum;  /* checksum, no used. */
    UINT8              padding[DBM_CSS_DO_SIZE - sizeof(uint16) - sizeof(cssDODbmParameters)];
} NV_PACKED_POSTFIX  cssDODbmSegment;

typedef NV_PACKED_PREFIX struct
{
    cssMiscDbmParameters data;
    UINT16               checksum;  /* checksum,no used */
    UINT8                padding[DBM_CSS_MISC_SIZE - sizeof(uint16) - sizeof(cssMiscDbmParameters)];
} NV_PACKED_POSTFIX  cssMiscDbmSegment;

/*****************************************************************************/
/*************************** FUNCTION PROTOTYPES *****************************/
/*****************************************************************************/
void cssDbmInit(void);

/* PRI Set/Get */
void CssSetParmMsg(ValGenericSetParmMsgT* MsgP);
void CssGetParmMsg(ValGenericGetParmMsgT* MsgP);

void cssRegisterForDBUpdates(void);
void cssDbmInitDBtoDefault(CssDbmId DbmId);

/* NAM Data APIs */
NamNumber cssGetActiveNam(void);
UiccCardStatusType cssGetUiccStatus(void);
void cssUpdateActiveNam(uint8 ActiveNam);
BOOL cpCssIsStandardInNAM(void);
BOOL cpCssIsHomeOnlyInNAM(void);
UINT16 cpCssGetNamCdmaPriChA(void);
UINT16 cpCssGetNamCdmaSecChA(void);
UINT16 cpCssGetNamCdmaPriChB(void);
UINT16 cpCssGetNamCdmaSecChB(void);
UINT8 cpCssGetNamSystemSelectType(void);
#ifdef MTK_PLT_ON_PC_UT
void cssSetNamSystemSelectType(uint8 value);
#endif

UINT8 cpCssGetNamCdmaNumOfPosSids(void);
UINT16 cpCssGetNamCdmaHomeSid(UINT8 index);
UINT16 cpCssGetNamCdmaHomeNid(UINT8 index);
BOOL cpCssIsNamCdmaHomeSysMatch(UINT16 sid, UINT16 nid);
BOOL cpCssIsNamCdmaHomeSidMatch(UINT16 sid);
UINT8 cpCssGetNamCdmaNumOfNegSids(void);
UINT16 cpCssGetNamCdmaNegSid(UINT8 index);
UINT16 cpCssGetNamCdmaNegNid(UINT8 sid_list_index);
BOOL cpCssIsNamCdmaNegSysMatch(UINT16 sid, UINT16 nid);

bool css1xIsSystemSelectSettingValid(SystemSelectType *sysSelect);
void css1xCheckSystemSelect(void);
#ifdef MTK_CBP
uint16 cssGetRoamIndForNonPrlSysButHomeInNam(void);
#endif

/* Raw (unparsed) PRL APIs */
void cssPrlUpdated(void);
UINT8* cssGetPtrToPRL(void);

/* ERI APIs */
UINT16 cssGetERIVersionNumber(void);
BOOL cssCpGetEriTableEntryByRoamIndicator(ValRoamIndicationMsgT *MsgP);
BOOL cssIsThisERIHomeSystem(uint8 roamInd);
BOOL cssIsThisERIInternationalSystem(uint8 roamInd);

/* System select barring APIs */
BOOL cpCssIsInternationalVoiceRoamBarred(void);
BOOL cpCssIsDomesticVoiceRoamBarred(void);

/* MRU APIs */
BOOL css1xGetMruRecord(UINT8 mru_index, pCHANNEL_DESC ret_mru_rec);
void css1xClearMru(void);
void css1xSaveLastSystemInMru(void);
void css1xSaveChannel(SysBandChannelT channel);
void css1xMruDataWrite(void);
BOOL css1xIsSysInMRU(SysBandChannelT* system);
#ifdef MTK_CBP
bool css1xGetMru0Sys(SysBandChannelT* mru0Sys);
#endif

BOOL cssDoGetMruRecord(UINT8 mru_index, pCHANNEL_DESC ret_mru_rec);
void cssDoClearMru(void);
void cssDoSaveMru(void);
#ifdef MTK_DEV_C2K_IRAT
bool cssDoIsSysInMRU(SysBandChannelT *pSystem);
void cssSetSysReadyFlag(BOOL ready);
#endif

#ifdef MTK_DEV_C2K_IRAT
void cssNotify1xRalRecToGmss(void);
void cssNotify1xMruRecToGmss(void);
void cssNotifyDoMruRecToGmss(void);
bool cssUpdate1xMruRecs(kal_uint8 num, mmss_cdma_rec_type sysRecList[]);
void cssUpdateDoMruRecs(kal_uint8 num, mmss_cdma_rec_type sysRecList[]);
#endif

/* UIM updates APIs */
#if (!defined MTK_DEV_C2K_IRAT) || (!defined MTK_DEV_CARD_HOTPLUG)
void cssUimStatusNotifyMsg(UimNotifyMsgT* MsgP);
#endif
#ifdef MTK_DEV_C2K_IRAT
#ifdef MTK_PLT_ON_PC_UT
void css1xSetMruFirstEntryOperSidNid(bool valid, uint16 sid, uint16 nid);
#endif
cssSidNid css1xGetMruFirstEntryOperSidNid(void);
BOOL cssIsPllChannTestMode(uint8 system);
uint16 cssGetPllChannTestMode(uint8 system);
uint32 css1xGetPsDelayDuration(void);
#endif
/* MSIC 1x parameters, such as timer values, etc... */
#ifdef MTK_CBP
uint32 css1xGetTEmergencySysLostScanDuration(void);
#endif
BOOL css1xSetTbsrDuration(Css1xBsrTimerTypeT tbsr, UINT32 duration);
UINT32 css1xGetTbsrDuration(Css1xBsrTimerTypeT tbsr);
BOOL cssDOSetTbsrDuration(CssDOBsrTimerTypeT tbsr, UINT32 duration);
UINT32 cssDOGetTbsrDuration(CssDOBsrTimerTypeT tbsr);
BOOL css1xSetAvoidanceDuration(Css1xAvoidanceTimerTypeT avoidance, UINT32 duration);
UINT32 css1xGetAvoidanceDuration(Css1xAvoidanceTimerTypeT avoidance);
void css1xSetMpssPilotStrengthThresh(UINT16 pilotThresh);
UINT16 css1xGetMpssPilotStrengthThresh(void);
void css1xClearManualAvoidSys(void);
void css1xSetManualAvoidSys(void);
UINT16* css1xGetManualAvoidSys(void);
BOOL css1xCheckIfManualAvoidSys(UINT16 sid);
void cssDbmWriteAck(uint32 MsgId, DbmWriteRspMsgT *MsgP);
#ifdef MTK_CBP
bool cssIsDefaultMin(void);
UINT16 css1xGetMCC(void);
UINT8 cssGetRoamSetting(void);
#endif
#endif

