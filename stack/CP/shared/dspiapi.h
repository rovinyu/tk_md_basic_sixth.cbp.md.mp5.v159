/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef _DSPIAPI_H_
#define _DSPIAPI_H_
/*****************************************************************************

  FILE NAME: dspiapi.h
 
  DESCRIPTION:

    This file contains CP - DSP message/structure definitions 
    applicable to both DSPM and DSPV.

    NOTE: This file is shared between the DSP and the CP

*****************************************************************************/

#include "sysdefs.h"
#include "ipcmbox.h"

/* Define mailbox and message header size in uint16 */
#define IPC_MBOX_HEADER_SIZE           2
#define IPC_MSG_HEADER_SIZE            2

/*---------------------------------**
** Control mailbox data structures **
**---------------------------------*/

typedef PACKED_PREFIX struct
{
   uint16       NumWords;
   uint16       NumCmds;
} PACKED_POSTFIX  IpcMboxHeaderT;

typedef PACKED_PREFIX struct
{
   uint16       MsgId;
   uint16       MsgSize;
} PACKED_POSTFIX  IpcMsgHeaderT;

/*-------------------------**
** Types used in Messages  **
**-------------------------*/

/* IPC Processor ids */
typedef enum
{
   IPC_CP_PROC     = 0,
   IPC_DSPM_PROC,
   IPC_DSPV_PROC,
   IPC_CBP_PROC,      /* CBP reset by watchdog */
   IPC_CBP_AP_PROC    /* whole system reset by AP */
} IpcProcIdT;


/* Define IPC max sizes in uint16 */
#define IPC_MAX_PRINTF_STR_SIZE  11
#define IPC_MAX_PRINTF_ARG_SIZE  5
#define IPC_MAX_TRACE_ARG_SIZE   5
#define IPC_MAX_DEBUG_WORDS      160

/*-------------------------------------**
** CP to DSP Message Range Information **
**-------------------------------------*/

#define IPC_DSP_MON_MIN_MSG        0x000 /* Minimum message Id associated with the Mon Task */
#define IPC_DSP_MON_MAX_MSG        0x0FF /* Maximum message Id associated with the Mon Task */

#define IPC_DSM_FWD_MIN_MSG        0x100 /* Minimum message Id associated with the Forward Task */
#define IPC_DSM_FWD_MAX_MSG        0x1FF /* Maximum message Id associated with the Forward Task */

#define IPC_DSM_REV_MIN_MSG        0x200 /* Minimum message Id associated with the Reverse Task */
#define IPC_DSM_REV_MAX_MSG        0x2FF /* Maximum message Id associated with the Reverse Task */

#define IPC_DSM_RFC_MIN_MSG        0x300 /* Minimum message Id associated with the RF Control Task */
#define IPC_DSM_RFC_MAX_MSG        0x3FF /* Maximum message Id associated with the RF Control Task */

#define IPC_DSM_MSC_MIN_MSG        0x400 /* Minimum message Id associated with the Miscellaneous Task */
#define IPC_DSM_MSC_MAX_MSG        0x47F /* Maximum message Id associated with the Miscellaneous Task */

#define IPC_DSM_HGH_MIN_MSG        0x480 /* Minimum message Id associated with the Miscellaneous Task */
#define IPC_DSM_HGH_MAX_MSG        0x4FF /* Maximum message Id associated with the Miscellaneous Task */

#define IPC_DSM_SCH_MIN_MSG        0x500 /* Minimum message Id associated with the Searcher Task */
#define IPC_DSM_SCH_MAX_MSG        0x5FF /* Maximum message Id associated with the Searcher Task */

#define IPC_DSV_MIN_MSG            0x600 /* Minimum message Id associated with the DSPV */
#define IPC_DSV_MAX_MSG            0x6FF /* Maximum message Id associated with the DSPV */

#define IPC_DSV_RNGR_MIN_MSG       0x0700   /* Minimum message Id associated with the DSPV */
#define IPC_DSV_RNGR_MAX_MSG       0x077F

#define IPC_DSV_APP_MIN_MSG        0x0780
#define IPC_DSV_APP_MAX_MSG        0x07FF

#define IPC_DSV_VAP_MPP_MIN_MSG     0x0800
#define IPC_DSV_VAP_MPP_MAX_MSG     0x08FF

#define IPC_DSV_VAP_SPP_MIN_MSG     0x0900
#define IPC_DSV_VAP_SPP_MAX_MSG     0x09FF

#define IPC_DSV_VAP_AUDIO_MIN_MSG   0x0A00
#define IPC_DSV_VAP_AUDIO_MAX_MSG   0x0AFF

#define IPC_DSV_AMP_REV_CH_MIN_MSG  0x0B00
#define IPC_DSV_AMP_REV_CH_MAX_MSG  0x0BFF

#define IPC_DSV_AMP_FWD_CH_MIN_MSG  0x0C00
#define IPC_DSV_AMP_FWD_CH_MAX_MSG  0x0CFF  /* Maximum message Id associated with the DSPV */

#define IPC_CP_FROM_DSM_MIN_MSG     0x0D00
#define IPC_CP_FROM_DSM_MAX_MSG     0x0DFF

#define IPC_CP_FROM_DSV_MIN_MSG     0x0F00
#define IPC_CP_FROM_DSV_MAX_MSG     0x0FFF

#define IDC_G2E_MIN_MSG     0x1000
#define IDC_G2E_MAX_MSG     0x10FF

#define IDC_E2G_MIN_MSG     0x1100
#define IDC_E2G_MAX_MSG     0x11FF

#define IPC_C2E_ENC_MIN_MSG     0x1200
#define IPC_C2E_ENC_MAX_MSG     0x121F
#define IPC_C2E_HDDEC_MIN_MSG   0x1220
#define IPC_C2E_HDDEC_MAX_MSG   0x123F
#define IPC_C2E_DATDEC_MIN_MSG  0x1240
#define IPC_C2E_DATDEC_MAX_MSG  0x125F
#define IPC_C2E_IDC_MIN_MSG     0x1260
#define IPC_C2E_IDC_MAX_MSG     0x127F

#define IPC_CP_FROM_SDRV_MIN_MSG 0x1300

#define IPC_CP_FROM_SPH_ETS_MIN_MSG 0x1310

/*---------------------------------------------------------------**
** Control Mailbox Message Ids: Control Processor to DSP (_DS*_) **
**---------------------------------------------------------------*/
typedef enum
{
   /* Monitor Task Messages */
   IPC_DSP_DOWN_INIT_MSG                         = IPC_DSP_MON_MIN_MSG,
   IPC_DSP_CODE_MSG,
   IPC_DSP_VER_MSG,
   IPC_DSP_PEEK_MSG,
   IPC_DSP_POKE_MSG,
   IPC_DSP_LOOPBACK_MSG,
   IPC_DSP_MEMTEST_MSG,
   IPC_DSP_SPY_MSG,
   IPC_DSP_TRACE_MSG,
   IPC_DSP_FAULT_CTRL_MSG,
   IPC_DSP_HEART_BEAT_CTRL_MSG,
   IPC_DSP_DBUF_LOOPBACK_MSG,
#ifdef MTK_DEV_ENABLE_DSP_DUMP
   IPC_DSP_DUMP_REQ_MSG,
   IPC_DSP_DUMP_COMP_MSG,
#else
   IPC_DSP_RESERVED_1,
   IPC_DSP_RESERVED_2,
#endif
   IPC_DSP_BITWISE_OP_MSG,
   IPC_DSP_CODE_PEEK_MSG,
   IPC_DSP_CODE_POKE_MSG,
   IPC_DSP_CODE_MEMTEST_MSG,
   IPC_DSP_MIXED_SIG_REG_INIT_MSG,
   IPC_DSP_JTAG_PWR_CFG_MSG,
   IPC_DSP_SHARED_MEM_LOOPBACK_MSG,
   IPC_DSP_CHIP_ID_MSG,
   IPC_DSP_HWD_PROFILE_START_MSG,
   IPC_DSP_HWD_PROFILE_STOP_MSG,
   IPC_DSP_HWD_PROFILE_DATA_REQUEST_MSG
} IpcDspMsgIdT;






/*---------------------------------------------------------------**
** Control Mailbox Message Ids: DSP to Control Processor (_CP_)  **
**---------------------------------------------------------------*/


typedef enum
{
   /* Monitor Task Messages */
   IPC_CP_DOWN_COMP_MSG                     = 0x000,
   IPC_CP_ALIVE_MSG,
   IPC_CP_VER_RSP_MSG,
   IPC_CP_PEEK_RSP_MSG,
   IPC_CP_POKE_RSP_MSG,
   IPC_CP_LOOPBACK_RSP_MSG,
   IPC_CP_MEMTEST_RSP_MSG,
   IPC_CP_SPY_RSP_MSG,
   IPC_CP_TRACE_RSP_MSG,
   IPC_CP_FAULT_MSG,
   IPC_CP_BITWISE_OP_RSP_MSG,                      
   IPC_CP_HEART_BEAT_MSG,
   IPC_CP_CONFIG_MSG,
   IPC_CP_DBUF_LOOPBACK_RSP_MSG,
   IPC_CP_PRINTF_MSG,
   IPC_CP_CODE_PEEK_RSP_MSG,
   IPC_CP_CODE_POKE_RSP_MSG,
   IPC_CP_CODE_MEMTEST_RSP_MSG,
   IPC_CP_SHARED_MEM_LOOPBACK_RSP_MSG,
   IPC_CP_HEART_BEAT_CTRL_RSP_MSG,
   IPC_CP_CODE_CRASH_DBG_DATA_MSG,
   IPC_CP_HWD_PROFILE_DATA_MSG
#ifdef MTK_DEV_ENABLE_DSP_DUMP
   ,
   IPC_CP_DUMP_INIT_MSG,
   IPC_CP_DUMP_DATA_MSG
#endif
} IpcCpMsgIdT;








/*---------------------------------------------------------------------**
** Control Mailbox Message Stuctures: Control Processor to DSP (_DSP_) **
**---------------------------------------------------------------------*/

/*-------------------------------------------**
** CP to DSP Monitor Task Message Structures **
**-------------------------------------------*/

/* Download Init Message */
typedef PACKED_PREFIX struct
{
   uint16       LoadAddr;              
   uint16       CodeSize;
   uint16       MboxOffset;
} PACKED_POSTFIX  IpcDspDownInitMsgT;

/* Download Init Message for DSPV */
typedef PACKED_PREFIX struct
{
   uint16       DataSizeLo;
   uint16       DataSizeHi;
   uint16       MboxOffset;
   uint16       HeaderSize;
   uint16       Header[1];
} PACKED_POSTFIX  IpcDspHdrDownInitMsgT;

/* Code Download Message */
typedef PACKED_PREFIX struct
{
   uint16       Code[1];
} PACKED_POSTFIX  IpcDspCodeMsgT;

/* Version Message */
typedef PACKED_PREFIX struct
{
   uint16       CpTaskId;
   uint16       CpMailboxId;
   uint32       CpMsgId; 
} PACKED_POSTFIX  IpcCpExeRspMsgT;  /* similar (but not identical) to "ExeRspMsgT" on the CP side */

typedef PACKED_PREFIX struct
{
   IpcCpExeRspMsgT   RspInfo;
} PACKED_POSTFIX  IpcDspVerMsgT;

/* Peek Message */
typedef PACKED_PREFIX struct
{
   uint16       StartAddr;
   uint16       NumWords;
   uint16       RspTaskId; /* CP task to which the peek response should be routed */
   uint16       RspMboxId; /* CP mailbox to which the peek response should be routed */
} PACKED_POSTFIX  IpcDspPeekMsgT;

/* Poke Message */
typedef PACKED_PREFIX struct
{
   uint16       StartAddr;
   uint16       NumWords;
   uint16       Data[1];
} PACKED_POSTFIX  IpcDspPokeMsgT;

typedef enum
{
   MON_DSP_BITOP_TOGGLE = 0,
   MON_DSP_BITOP_SET,
   MON_DSP_BITOP_CLEAR
} BitwiseOpT;

/* Bitwise Operation Message */
typedef PACKED_PREFIX struct
{
   uint16       Addr;
   uint16       Operator;
   uint16       Mask;
} PACKED_POSTFIX  IpcDspBitwiseOpMsgT;

/* HW mailbox loopback message */
typedef PACKED_PREFIX struct
{
   uint16       Data[1];
} PACKED_POSTFIX  IpcDspMboxLoopMsgT;

/* RAM test Message */
typedef PACKED_PREFIX struct
{
   uint16       StartAddr;
   uint16       NumWords;
} PACKED_POSTFIX  IpcDspMemTestMsgT;

/* Spy Message */
typedef PACKED_PREFIX struct
{
   uint16       SpyId;
   uint16       SpyActive;
} PACKED_POSTFIX  IpcDspSpyMsgT;

/* Trace Message */
typedef PACKED_PREFIX struct
{
   uint16       TraceId;
   uint16       TraceActive;
} PACKED_POSTFIX  IpcDspTraceMsgT;

/* Fault Control Message values */
typedef enum
{
   IPC_FAULT_CTRL_DISABLE = FALSE,
   IPC_FAULT_CTRL_ENABLE  = TRUE
} IpcDspFaultCtrlT;

/* Fault Control Message */
typedef PACKED_PREFIX struct
{
   uint16       Control;
} PACKED_POSTFIX  IpcDspFaultCtrlMsgT;

/* Heart Beat Control Message values */
typedef enum
{
   IPC_HEART_BEAT_CTRL_DISABLE = FALSE,
   IPC_HEART_BEAT_CTRL_ENABLE  = TRUE
} IpcDspHeartBeatCtrlT;

/* Heart Beat Control Message */
typedef PACKED_PREFIX struct
{
   uint16       Control;
} PACKED_POSTFIX  IpcDspHeartBeatCtrlMsgT;

/* Direct Buffer Mailbox Loopback Message */
typedef PACKED_PREFIX struct
{
   uint32       NumLoops;
   uint16       NumWords;
   uint16       Data[1];
} PACKED_POSTFIX  IpcDspDBufLoopbackMsgT;

/* Profile Control Message */
typedef PACKED_PREFIX struct
{
   uint16       Control;
} PACKED_POSTFIX  IpcProfileCtrlMsgT;


typedef PACKED_PREFIX struct
{
   uint16       ChipId;
   uint16       ChipRev;
} PACKED_POSTFIX  IpcDspChipIdMsgT;


/* Hwd Profile Start Message */
typedef enum
{
   IPC_START_TRIG_IMMEDIATE   = 0,
   IPC_START_TRIG_CONDITIONAL
} IpcDspStartTriggersT;

typedef enum
{
   IPC_STOP_TRIG_NONE         = 0,
   IPC_STOP_TRIG_CONDITIONAL
} IpcDspStopTriggersT;

typedef PACKED_PREFIX struct {
   uint16         StartTrigger;        /* IpcDspStartTriggersT */
   uint16         StopTrigger;         /* IpcDspStopTriggersT */
   uint16         SpyInterval;         /* Rate of reporting profile data to CP, unit is ms */
   uint16         PeriodicDataReset;   /* If TRUE, reset data after reporting profile them to CP */
} PACKED_POSTFIX  IpcDspHwdProfileStartMsgT;


/*******************************************************
** Message Stuctures: DSP to Control Processor (_CP_) **
*******************************************************/

/*-----------------------**
** Monitor Task Messages **
**-----------------------*/

/* Download complete msg */
typedef PACKED_PREFIX struct
{
   uint16       Checksum;
} PACKED_POSTFIX  IpcCpDownCompMsgT;

/* Version response message */
typedef PACKED_PREFIX struct
{
   uint16            VersionMajor;
   uint16            VersionMinor;
   uint16            VersionPatch;
   uint16            Date[5];
} PACKED_POSTFIX  IpcCpVerRspMsgT;


/* Peek response message */
typedef PACKED_PREFIX struct
{
   uint16       StartAddr;
   uint16       NumWords;
   uint16       RspTaskId; /* CP task to which the peek response should be routed */
   uint16       RspMboxId; /* CP mailbox to which the peek response should be routed */
   uint16       Data[1];
} PACKED_POSTFIX  IpcCpPeekRspMsgT;

/* Poke response message */
typedef PACKED_PREFIX struct
{
   uint16       StartAddr;
   uint16       NumWords;
} PACKED_POSTFIX  IpcCpPokeRspMsgT;

/* Bitwise Operation Message */
typedef PACKED_PREFIX struct
{
   uint16       OldValue;
   uint16       NewValue;
} PACKED_POSTFIX  IpcCpBitwiseOpRspMsgT;

/* DSP Jtag Power Config Message */
typedef PACKED_PREFIX struct
{
   uint16       JtagPwrCfgMode;
} PACKED_POSTFIX  IpcCpDspJtagPwrCfgMsgT;

typedef enum
{
   IPC_DSP_JTAG_PWR_OFF,
   IPC_DSP_JTAG_PWR_ON
} IpcDspJtagPwrCfgT;

/* Fault message */
typedef PACKED_PREFIX struct
{
   uint16       UnitId;
   uint32       SystemTime;
   uint16       FaultId1;
   uint16       FaultId2;
   uint16       FaultType;
} PACKED_POSTFIX  IpcCpFaultMsgT;

/* HW mailbox loopback response message */
typedef PACKED_PREFIX struct
{
   uint16       Data[1];
} PACKED_POSTFIX  IpcCpMboxLoopRspMsgT;

/* RAM test Message */
typedef PACKED_PREFIX struct
{
   uint16       Result;
   uint16       ErrAddr;
} PACKED_POSTFIX  IpcCpMemTestRspMsgT;

/* Spy response message */
typedef PACKED_PREFIX struct
{
   uint16       SpyId;
   uint32       SystemTime;
   uint16       Data[1];
} PACKED_POSTFIX  IpcCpSpyRspMsgT;

/* Trace response message */
typedef PACKED_PREFIX struct
{
   uint16       TraceId;
   uint32       SystemTime;
   uint16       Args[1];
} PACKED_POSTFIX  IpcCpTraceRspMsgT;

/* Printf message */
typedef PACKED_PREFIX struct
{
   uint16       String[IPC_MAX_PRINTF_STR_SIZE];
   uint16       Args[1];
} PACKED_POSTFIX  IpcCpPrintfMsgT;

/* Heart Beat message */
typedef PACKED_PREFIX struct
{
   uint16       ProcessorId;
} PACKED_POSTFIX  IpcCpHeartBeatMsgT;

/* Configuration message */
typedef PACKED_PREFIX struct
{
   uint16       ProcessorId;
   uint16       ConfigDataL;
   uint16       ConfigDataH;
} PACKED_POSTFIX  IpcCpConfigMsgT;

/* Direct buffer mailbox loopback test response message */
typedef PACKED_PREFIX struct
{
   uint16       Result;
} PACKED_POSTFIX  IpcCpDBufLoopbackRspMsgT;


/* DSP alive message */
typedef PACKED_PREFIX struct
{
   uint16       IpcCompatibilityNum;
} PACKED_POSTFIX  IpcCpDspAliveMsgT;

/* DSPM crash dump */
typedef PACKED_PREFIX struct
{
   uint16       CrashDebugData[IPC_MAX_DEBUG_WORDS];
} PACKED_POSTFIX  IpcCpCrashDebugDataMsgT;

/* Hwd Profile Data Message */
typedef PACKED_PREFIX struct {
   uint16         ProcessorId;         /* IpcProcIdT */
   uint16         NumOfThreads;
   uint32         Data[1];             /* size=NumOfThreads, max=32 */
} PACKED_POSTFIX  IpcCpHwdProfileDataMsgT;

#ifdef MTK_DEV_ENABLE_DSP_DUMP
typedef struct
{
   uint16       base;
   uint16       size;
} TLDspMemoryT;

typedef struct
{
   uint16       numWordLo;
   uint16       numWordHi;
} TLDspDumpInitT;

typedef struct
{
   /* CBU Registers */
   uint32       a0;
   uint32       a1;
   uint32       b0;
   uint32       b1;
   uint32       p;
   uint16       x;
   uint16       y;
   uint16       sv;

   /* DAAU Registers */
   uint16       cfgi;
   uint16       cfgj;
   //uint16       r0;
   uint16       r1;
   uint16       r2;
   uint16       r3;
   uint16       r4;
   uint16       r5;
   uint16       rb;
   uint16       sp;
   uint16       mixp;

   /* PCU Registers */
   uint16       lc;

   /* General Registers */
   uint16       st0;
   uint16       st1;
   uint16       st2;
   uint16       icr;
   uint16       dvm;
} TLDspRegisterT;

#endif

#endif
