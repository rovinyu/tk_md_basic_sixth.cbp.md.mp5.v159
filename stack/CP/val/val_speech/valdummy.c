/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

#include "exeapi.h"
#include "valapi.h"
#include "valsndapi.h"
#include "sysKeydef.h"

ValAppStatusT ValMusicRecord (char *FileNameP, ValSoundFormatT Format, ValSndSamplingRatesT SamplingRate, uint16 NumChan)
{
	return VAL_APP_FAILED_ERROR; 
}
bool ValPcmFrameInput (uint16 *SpeechDataP)
{
	return FALSE;
}

void ValPcmFrameOutput (uint16 *SpeechDataP)
{
}

void ValSndMicPcmDetectErr (void)
{
}


void ValPcmProcessingDisable (void) 
{
}

void ValPcmProcessingEnable (ExeTaskIdT TaskId, ExeMailboxIdT MboxId, uint32 MsgId)
{
}

void ValSndEnableContKey (bool On)
{
}


ValSoundFormatT ValSndGetStatus (void)
{
	return VAL_SOUND_FORMAT_NUM;
}

void ValSndSilenceKey (bool On)
{
}

ValAppStatusT ValTonePlay (uint8 SoundId, ValSoundFormatT Format, uint8 Iterations, uint16 NumIterations)
{
	return VAL_APP_FAILED_ERROR;
}

bool ValSndIsMusicPlaying (void)
{
	return FALSE;
}

uint16 ValMusicPlayEx (ValMusicPlayParamT *MusicPlayExParam, ValMusicPlayIdT *MusicPlayId)
{
	return VAL_APP_FAILED_ERROR;
}

void ValSndKeyProcess (SysKeyIdT Key, bool IsPressed)
{
}


void ValSoundStop (ValSoundStopT StopType)
{
}

void ValSoundPlayAudio (PlayStructT *pData, uint32 *PlayId)
{
}

bool ValSoundSetDevice (ValSoundDeviceModesT DevMode, ValSoundDeviceT Device)
{
	return FALSE;
}
ValSoundDeviceT ValSoundDeviceGet (void)
{
	return VAL_SOUND_DEVICE_NUM;
}

// TODO: 
void ValVoiceLoopBack(bool Enable)
{
}

void ValSetVolume (ValSoundVolumeModesT VolMode, ValSoundVolumeT Volume)
{
}

ValSoundVolumeT ValGetVolume (ValSoundVolumeModesT VolMode)
{
	return VAL_SOUND_VOLUME_CURRENT;
}


// ============================================================================
#include "cpbuf.h"
#include "iopapi.h"
#include "sysapi.h"
#include "monapi.h"
#include "monids.h"



void ValSphSendDataTest(int choice)
{
	IopDataCpBuffDescT CpBuffDesc;
	CpBufferT* pCpBuf;	


	if(2 == choice) {
		uint32 msg[4] = {0xffffffff, 0xaf730976, 0x22224444, 0};

		MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 1, 10000);
		pCpBuf = CpBufGet(4*sizeof(uint32), CPBUF_FWD);
		MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 1, 10000);
	

		// SysMemset((uint8 *) pCpBuf->dataPtr, 0, (uint16)  640*sizeof(uint8));
		SysMemcpy ((uint8 *) pCpBuf->dataPtr,
   	           (uint8 *) msg,
      	        (uint16)  4*sizeof(uint32));

		CpBuffDesc.dataLen= 4*sizeof(uint32);
		
	} else if(1 == choice) {
		uint32 msg[6] = {0, 0x286, 0x33336666, 0xaf630286, 0x00052a2a, 0x11110280};

		MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 1, 20000);
		pCpBuf = CpBufGet(1024, CPBUF_REV);
		MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 1, 20000);

		SysMemset((uint8 *) pCpBuf->dataPtr, 0xcc, (uint16) 1024*sizeof(uint8));

		SysMemcpy ((uint8 *) pCpBuf->dataPtr,
   	           (uint8 *) msg,
      	        (uint16)  6*sizeof(uint32));
		
   	CpBuffDesc.dataLen= 646*sizeof(uint8);
	} else {		
		uint32 msg[4] = {0xffffffff, 0xaf700000, 0x22224444, 0};

		MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 1, 10000);
		pCpBuf = CpBufGet(4*sizeof(uint32), CPBUF_FWD);
		MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 1, 10000);
	

		// SysMemset((uint8 *) pCpBuf->dataPtr, 0, (uint16)  640*sizeof(uint8));
		SysMemcpy ((uint8 *) pCpBuf->dataPtr,
   	           (uint8 *) msg,
      	        (uint16)  4*sizeof(uint32));

		CpBuffDesc.dataLen= 4*sizeof(uint32);
	}

	// set CpBuffDesc 
	CpBuffDesc.bufPtr = pCpBuf;
	
   // CpBuffDesc.dataLen= 4*sizeof(uint32);
   CpBuffDesc.offset= 0;
   CpBuffDesc.nRLPFlow = 0 ;
   CpBuffDesc.streamType = 0;
   
   IopWrite(&CpBuffDesc, IopDataChannelVoice, 0);

	MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 2, 1111, 2222);
}

void ValSphRecDataTest(void)
{

	IopDataCpBuffDescT CurrentCpBuff;
	IopDataChRetStatus iopReadStatus;
	CpBufferT cpBuffer; 
	uint32 i=0;
	uint32 j=0; 
	
	MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 2, 3333, 4444);
	
	do {
		iopReadStatus = IopRead ( &CurrentCpBuff ,IopDataChannelVoice, 0 );
		
		MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 5, 
			i,
			CurrentCpBuff.dataLen,
			*(CurrentCpBuff.bufPtr),
			CurrentCpBuff.offset,
			CurrentCpBuff.bufPtr->len);

		while(j < CurrentCpBuff.dataLen ) {
			MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 7, 
				i, j, 7777, 
				*(CurrentCpBuff.bufPtr->dataPtr+j),
				*(CurrentCpBuff.bufPtr->dataPtr+j+1),
				*(CurrentCpBuff.bufPtr->dataPtr+j+2),
				*(CurrentCpBuff.bufPtr->dataPtr+j+3)); // dataPtr is 32bits alight
			j+=16; // 4bytes/print
		}

		CpBufFree(CurrentCpBuff.bufPtr);

		i++;
	} while((iopReadStatus != IopDataChRetErr) && (iopReadStatus != IopDataChRetOK_Empty));
	MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 2, 4444, 3333);
}
