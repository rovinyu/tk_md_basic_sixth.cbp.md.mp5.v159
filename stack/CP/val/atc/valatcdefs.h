/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 1998-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef _VALATCDEFS_H_
#define _VALATCDEFS_H_
/*****************************************************************************
* 
* FILE NAME   : valatcdefs.h
*
* DESCRIPTION :
*
*     This include file provides system wide global type declarations and 
*     constants
*
* HISTORY     :
*     See Log at end of file.
*
*****************************************************************************/

/*----------------------------------------------------------------------------
 Include Files
----------------------------------------------------------------------------*/
#include "sysdefs.h"
#include "exeapi.h"

/*----------------------------------------------------------------------------
 Global Defines and Macros
----------------------------------------------------------------------------*/
#ifdef MTK_CBP
#define VALAT_UNSO_RSP_MAX_REC_NUM_IN_LIST  40
#endif

#define ATC_MAX_SIZE_DATA        64
#define ATC_MAX_MON_SPY_LEN      64

#define ATC_MAX_BRSWR_DGT_SIZE   (32 +1)   
		                             /* NULL-terminated. */
#define ATC_MAX_NUMBER_OF_TIMERS 20
#define ATC_MAX_USRID_LEN        24
#define ATC_MAX_PSWD_LEN         24

#ifdef SYS_OPTION_ENHANCEDAT
#define ATC_ENHANCED_AT_CMD_LEN     1024
#endif
								 
/***********************************************************************/
/* Global Typedefs                                                     */
/***********************************************************************/
typedef enum 
{
  ATCOF_Intercept,
  ATCOF_Reorder,
  ATCOF_Release,
  ATCOF_Reject,
  ATCOF_Disabled,
  ATCOF_Busy,
  NUM_ATCOFs
} AtcOrigFailReason;


typedef enum
{
   ATC_AT_HL,                /* Serial port initiated data/fax call    */
   ATC_HL,                   /* Packet data mode with Rm interface.    */
   ATC_NONE                  /* data/fax call uses external PC card.   */
} AtcModeSelT;

typedef enum
{
   ATC_ANS_CONNECTSUCCESS,
   ATC_ANS_CONNECTFAILTCP,
   ATC_ANS_CONNECTFAILPPP,
   ATC_ANS_CONNECTFAILRLP,
   ATC_ANS_CONNECTFAILUART,
   ATC_ANS_DISCONNECTNORMAL,
   ATC_NUM_ANS
} AtcHlStatusT;

typedef enum   /* This definition shall agree with defines found in 7074API.h */
{
  ATC_ANS_ConnectSuccess,
  ATC_ANS_ConnectFailTCP,
  ATC_ANS_ConnectFailPPP,
  ATC_ANS_ConnectFailRLP,
  ATC_ANS_ConnectFailUart,
  ATC_ANS_DisconnectNormal = 5,
  ATC_NUM_ANSs
} AtcNspeStatus;

typedef enum 
{
  ATC_BS_Success,
  ATC_BS_Busy,
  ATC_BS_Rejected,
  ATC_BS_Failed,
  ATC_BS_NoSvc,
  ATC_BS_DisconnectNormal,
  ATC_BS_ConnectionDropped,
  ATC_BS_Dormant,
  ATC_BS_Reconnected,
  ATC_NUM_BSs
} AtcBrowserConnectStatus;

#ifdef SYS_OPTION_HL
typedef enum
{
   ATC_UDP_BROWSER          = 5, /* AIS_UpBrowser in Ai_data.h      */
   ATC_UDP_BROWSER_ON_ASYNC = 6, /* AIS_AsyncUpBrowser in Ai_data.h */
   ATC_TCP_CKT_BROWSER      = 7, /* AIS_TcpBrowser in Ai_data.h     */
   ATC_TCP_PKT_BROWSER      = 8, /* AIS_TcpBrowser in Ai_data.h     */
   ATC_PKT_NTWRK_RM         = 9, /* AIS_PacketNtwkRm in Ai_data.h   */
   ATC_PPP_ONLY             = 10 /*	AIS_PPPOnly in Ai_data.h        */
} AtcBrowsersT;
#endif

/***********************************************************************/
/* Message Definitions                                                 */
/***********************************************************************/
#ifdef SYS_OPTION_HL
typedef PACKED_PREFIX struct
{ 
   uint8*          DataBufP;        /* Points to the first byte of data*/
   uint16          DataLen;         /* size of data in byte            */
} PACKED_POSTFIX  AtcCmpCmpresRspMsgT;

typedef PACKED_PREFIX struct
{
   uint8*          DataBufP;        /* Points to the first byte of data*/
   uint16          DataLen;         /* size of data in byte            */
} PACKED_POSTFIX  AtcCmpExpandRspMsgT;

typedef PACKED_PREFIX struct
{
   uint16          ServiceOption;   /* Service option negotiated IS-95 */
   bool            SecondaryTraffic;/* FALSE if Primary Traffic        */
} PACKED_POSTFIX  AtcCpConnIndMsgT;

typedef PACKED_PREFIX struct
{
   uint8             Event;         /* Table 7.4.2-2 per IS707A.3      */
} PACKED_POSTFIX  AtcCpPktCallEvMsgT;

typedef PACKED_PREFIX struct
{
   uint8             NewState;      /* Table 7.4.2-2 per IS707A.3      */
} PACKED_POSTFIX  AtcCpPktCallStMsgT;

typedef PACKED_PREFIX struct
{
   uint8              ServiceType;   /* service type in Page Response   */   
} PACKED_POSTFIX  AtcCpPageIndMsgT;

typedef PACKED_PREFIX struct
{ 
   uint16             Reason;        /* Release reason.                 */
} PACKED_POSTFIX  AtcCpReleaseIndMsgT;

typedef PACKED_PREFIX struct
{ 
   uint8              cpEvent;       /* CP event information            */
} PACKED_POSTFIX  AtcCpEventIndMsgT;

typedef PACKED_PREFIX struct
{
   AtcOrigFailReason  Reason;        /* CP origination fail at data service. */
} PACKED_POSTFIX  AtcCpOrigFailIndMsgT;

typedef PACKED_PREFIX struct
{
   AtcModeSelT        Mode;   
} PACKED_POSTFIX  AtcOpModeMsgT;

typedef PACKED_PREFIX struct
{
   uint8             Browser;     
} PACKED_POSTFIX  AtcHlBrowserConnReqMsgT;

typedef PACKED_PREFIX struct
{
   uint8             Digit[ATC_MAX_BRSWR_DGT_SIZE];
   uint16            ServiceOption;
} PACKED_POSTFIX   AtcHlUpbDigitMsgT;               /* Digits used for browser to dial. */ 

typedef PACKED_PREFIX struct
{
   AtcNspeStatus     ConnResult;    /* ANS_ConnectSuccess,
                                       ANS_ConnectFailTCP,
                                       ANS_ConnectFailPPP,
                                       ANS_ConnectFailRLP,
                                       ANS_DisconnectNormal             */
   uint32            LocalIpAddr;
   uint32            RemoteIpAddr;   
} PACKED_POSTFIX  AtcHlPppConnRspMsgT;

typedef PACKED_PREFIX struct
{ 
   uint8*            DataBufP;      /* points to the first byte of data */
   uint16            DataLen;       /* data size in bytes.              */   
} PACKED_POSTFIX  AtcHlRxDataMsgT;

typedef PACKED_PREFIX struct
{ 
   AtcHlStatusT      Status;        /* Status of NSPE at transport layer*/
   uint32            MsIp;          /* mobile IP, if connected          */
   uint32            BsIp;          /* base station(IWF)IP, if connected*/
} PACKED_POSTFIX  AtcHlStatusMsgT;

typedef PACKED_PREFIX struct  
{
   uint8*            DataBufP;      /* points to the first byte of data */
   uint16            DataLen;       /* data size in bytes.              */   
} PACKED_POSTFIX  AtcHlTxReqMsgT;

typedef PACKED_PREFIX struct
{
   AtcNspeStatus     ConnResult;    /* ANS_ConnectSuccess,
                                       ANS_ConnectFailTCP,
                                       ANS_ConnectFailPPP,
                                       ANS_ConnectFailRLP,
                                       ANS_DisconnectNormal             */
   uint32            LocalIpAddr;
   uint32            RemoteIpAddr;   
} PACKED_POSTFIX  AtcHlUmPppStatusMsgT;

typedef PACKED_PREFIX struct
{
   AtcNspeStatus      ConnResult;    /* ANS_ConnectSuccess,
                                       ANS_ConnectFailTCP,
                                       ANS_ConnectFailPPP,
                                       ANS_ConnectFailRLP,
                                       ANS_DisconnectNormal             */
} PACKED_POSTFIX  AtcHlRmPppStatusMsgT;

typedef PACKED_PREFIX struct
{ 
   bool              Secondary;     /* FALSE, if primary.               */
} PACKED_POSTFIX  AtcRlpCloseRspMsgT;

typedef PACKED_PREFIX struct
{ 
   bool              Secondary;     /* FALSE, if primary.               */
   bool              Successful;    
} PACKED_POSTFIX  AtcRlpOpenRspMsgT;


typedef PACKED_PREFIX struct
{ 
   bool              Secondary;     /* FALSE, if primary.               */
} PACKED_POSTFIX  AtcRlpIdleIndMsgT;

typedef PACKED_PREFIX struct
{ 
   bool              Secondary;     /* FALSE, if primary.               */
} PACKED_POSTFIX  AtcRlpTxRspMsgT;

typedef PACKED_PREFIX struct
{ 
   uint8*            DataBufP;   /* point to the first byte of data.    */
   uint16            DataLen;    /* The size of data in byte.           */
   bool              Secondary;  /* False, if primary channel.          */
} PACKED_POSTFIX  AtcRlpRxDataMsgT;

typedef PACKED_PREFIX struct
{
   uint32            TimerId;                       /* Expired timer Id */
} PACKED_POSTFIX  AtcTimerExpiredMsgT;

typedef PACKED_PREFIX struct
{
   uint8             QNCStr[ATC_MAX_BRSWR_DGT_SIZE]; /* null-terminated */
} PACKED_POSTFIX  AtcSetQNCDialStrMsgT;

typedef PACKED_PREFIX struct 
{
   uint8 nIPServiceType;
} PACKED_POSTFIX  AtcSetIPServiceTypeMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;
} PACKED_POSTFIX  AtcGetIPServiceTypeMsgT;

typedef PACKED_PREFIX struct
{
   uint8 nIPServiceType;
} PACKED_POSTFIX  AtcGetIPServiceTypeRspMsgT;


typedef PACKED_PREFIX struct  /*shenchao add 2003/06/25*/
{
		uint8 Roam; 					  /* ROAM Status						  */
		uint16 Band;					  /* Current Operating Band 			  */
		uint16 Channel; 				  /* Current Channel Number 			  */
		uint8  Mode;					  /* current mode: PCS/Cellular/Analog	  */
		uint8  Block;					  /* current CDMA block (if CDMA system)  */
		uint8 ServingSystem;			  /* Serving System/Block				  */
		uint16 SysID;					  /* Last-Received System ID  (sid) 	  */
		uint16 LocArea; 				  /* Current Location Area ID (nid) 	  */
		uint16 PilotPn; 				  /* PILOT_PN							  */
} PACKED_POSTFIX AtcCpStatusRptMsgT;

typedef PACKED_PREFIX struct
{
   int16 Rssi;
} PACKED_POSTFIX AtcL1dRssiMsgT;

typedef PACKED_PREFIX struct
{
   uint8 Status;      
   uint8 BattLevel;
} PACKED_POSTFIX AtcBattStatusMsgT;
#endif

/*****************************************************************************
* $Log: atcdefs.h $
*
*****************************************************************************/

/*****************************************************************************
* End of File
*****************************************************************************/
#endif
/**Log information: \main\CBP7FeaturePhone\CBP7FeaturePhone_zjiang_href17412\1 2011-07-13 09:29:19 GMT zjiang
** HREF#17412.data call:用1x拨号上网后，拔出usb，再插入usb，还没拨号就死机 **/
/**Log information: \main\CBP7FeaturePhone\4 2011-07-14 00:27:05 GMT cshen
** merge from CBP7FeaturePhone_zjiang_href17412**/
/**Log information: \main\Trophy\Trophy_zjiang_href22313\1 2013-11-22 08:29:57 GMT zjiang
** HREF#22313**/
/**Log information: \main\Trophy\1 2013-11-22 08:32:39 GMT cshen
** href#22313**/
