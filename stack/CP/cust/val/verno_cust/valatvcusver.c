/*************************************************************
*
* Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*************************************************************/

/*****************************************************************************
 
	FILE NAME:
		valatvcusver.c
	DESCRIPTION:
		Contains MMI handlers for: AT commands for +VCUSVER

*****************************************************************************/

#include "exeapi.h"
#include "sysdefs.h" 
#include "valapi.h"
#include "valat.h"
#include "valatcdefs.h"
#include <string.h>
#include "cpver.h"
#include "valEnhAtDefs.h"
#include "valatmisc.h"
#include "stdio.h"
#include "monapi.h"

extern MonVersionT MonDspmVersion;
extern uint16 MonReadChipId(void);
extern uint8  CpVersion[];
extern uint8  AsicType[];
extern uint8 Verno_custBuildTime[];

#define DSP_FLASH_FILE_ID           "VTI"
#define NOT_AVAILABLE_STR           "N/A"

#ifdef SYS_OPTION_ENHANCEDAT
/***************************************************************************** 
        FUNCTION NAME:  MmiATVCUSVER
        DESCRIPTION:    
        PARAMETERS:     command body
        RETURNS:        None
*****************************************************************************/  
void MmiATVCUSVER (AtcSendAtMsgT* MsgDataP)
{
#ifndef MTK_PLT_ON_PC  
	AtcSendAtRespMsgT   RspMsg;
    uint8 chan = MsgDataP->chan;   
    SwVersionInfoT PlatformVer;
    uint8 CustVerNo[]=MTK_VERNO_STRING;

    PlatformVer = GetPlatformVersion();
	
	memset ((void*)&RspMsg, 0, sizeof(RspMsg));
	MemsetEnhCmdRspBuf(chan, 0, VAL_ENHANCED_AT_CMD_LEN);
	
	strcpy(RspMsg.cmdName, "+VCUSVER");

	sprintf (GetEnhCmdRspBuf(chan,0), "%s:%d,%d,%d,%02d,%02d,%02d,%02d,%02d,\"%s\"", RspMsg.cmdName, PlatformVer.MajorVersion, PlatformVer.MinorVersion, PlatformVer.ReviseVersion,
	                                           Verno_custBuildTime[0], Verno_custBuildTime[1], Verno_custBuildTime[2], Verno_custBuildTime[3], Verno_custBuildTime[4], CustVerNo);

	RspMsg.numOfValidLines = 1;
	RspMsg.LinesOfParms.ParmLine[0] = GetEnhCmdRspBuf(chan,0);
	RspMsg.rspStatus = TRUE;
	RspMsg.skipResultCode = FALSE;
    RspMsg.chan = chan;
	SendCmdRspMsgToAtc((void*)&RspMsg);     
#endif
}
#endif

/*****************************************************************************
  FUNCTION NAME: MonGetCBPSwVersion

  DESCRIPTION:   Fill in a CBP Version response message structure with
                 the CP, DSPM and DSPV software versions and build times.

  PARAMETERS:  RspMsgP - pointer to the CBP Version response message structure.

  RETURNED VALUES:  None.
*****************************************************************************/
void MonGetCBPSwVersion (MonCBPSwVersionRspMsgT *RspMsgP)
{
#ifndef MTK_PLT_ON_PC	
   char *      str_ptr;

   /* Zero out all fields */
   SysMemset ((void *) RspMsgP, 0, sizeof (MonCBPSwVersionRspMsgT));
   /* Fill in CP SW version and build time in the response message */
   str_ptr = (char *)&RspMsgP->CbpSwVersion[0];
#ifndef CUSTOM_SW_VERSION
   uint8       str_len;
   sprintf (&str_ptr[0], 
               "\n\tCP %s on %d-%d-20%d, %d:%d", 
               MTK_VERNO_STRING,
               Verno_custBuildTime[0], Verno_custBuildTime[1], Verno_custBuildTime[2], 
               Verno_custBuildTime[3], Verno_custBuildTime[4]);

   str_len = strlen((char *)RspMsgP->CbpSwVersion);
   str_ptr = (char *)&RspMsgP->CbpSwVersion[str_len];

   /* Fill in DSPM SW version and build time in the response message */
   if ( MonDspmVersion.UnitNum != FALSE)
   {
      sprintf (str_ptr, 
                 "\n\tDSPM %d.%d.%d on %d-%d-20%d, %d:%d",   
                 MonDspmVersion.VerInfo[0], MonDspmVersion.VerInfo[1], MonDspmVersion.VerInfo[2],
                 MonDspmVersion.TimeInfo[0], MonDspmVersion.TimeInfo[1], MonDspmVersion.TimeInfo[2],
                 MonDspmVersion.TimeInfo[3], MonDspmVersion.TimeInfo[4]);
   }
   else
   {
      sprintf(str_ptr, "\n\tDSPM not available");
   }

   str_len = strlen((char *)RspMsgP->CbpSwVersion);
   str_ptr = (char *)&RspMsgP->CbpSwVersion[str_len];

#ifndef MTK_PLT_AUDIO
   /* Fill in DSPV SW version and build time in the response message */
   if ( MonDspvVersion.UnitNum != FALSE)
   {
      sprintf (str_ptr, 
                  "\n\tDSPV %d.%d.%d on %d-%d-20%d, %d:%d", 
                  MonDspvVersion.VerInfo[0], MonDspvVersion.VerInfo[1], MonDspvVersion.VerInfo[2],
                  MonDspvVersion.TimeInfo[0], MonDspvVersion.TimeInfo[1], MonDspvVersion.TimeInfo[2],
                  MonDspvVersion.TimeInfo[3], MonDspvVersion.TimeInfo[4]);
   }
   else
   {
      sprintf(str_ptr, "\n\tDSPV not available");
   }
#endif
#else
    snprintf(str_ptr, MON_MAX_PRINTF_STR_SIZE, "\n\t%s %d-%d-20%d, %d:%d\n\r",
        M2S(CUSTOM_SW_VERSION),Verno_custBuildTime[0], Verno_custBuildTime[1], Verno_custBuildTime[2], Verno_custBuildTime[3], Verno_custBuildTime[4]);
#endif
   RspMsgP->CbpSwVerStrLen = strlen((char *)RspMsgP->CbpSwVersion) + 1;
#endif   
}

/*****************************************************************************

  FUNCTION NAME: CBPVersionMsg

  DESCRIPTION:

    Process CBP version command. The CP Asic version/revision information and
    DSP patch/match addresses are sent to ETS when this message is received.

    Response msg format:
      ChipId and
      For ROM version only
         DSPM BuildInfo
         DSPM PatchInfo[], if DSPMNumPatches > 0
         DSPV BuildInfo
         DSPV PatchInfo[], if DSPVNumPatches > 0

  PARAMETERS:

    MsgDataP - incoming message pointer

  RETURNED VALUES:

    None

*****************************************************************************/

void CBPVersionMsg(void *MsgDataP)
{
#ifndef MTK_PLT_ON_PC	
   MonCBPVersionMsgT      *RxMsgP;
   MonCBPVersionRspMsgT   *RspMsgP;
   BuildInfoT             *RspBuildInfoP;
   uint32                  MsgSize;
   uint16                  Index;
   /* Cast pointer to received message struct */
   RxMsgP = (MonCBPVersionMsgT *) MsgDataP;

   /* Determine the response message size */
   MsgSize = sizeof(MonCBPVersionRspMsgT);

   /**********************************************/
   /* Allocate a msg buffer for response message */
   /**********************************************/

   RspMsgP = (MonCBPVersionRspMsgT *)ExeMsgBufferGet(MsgSize);

   /* Zero out all fields */
   SysMemset ((void *) RspMsgP, 0, (uint16) MsgSize);

   /*******************************************/
   /* Fill in chip id in the response message */
   /*******************************************/
   SysMemcpy ((void *) RspMsgP->AsicType, (void *) AsicType,
              sizeof (RspMsgP->AsicType));
#ifndef CUSTOM_HW_VERSION
   RspMsgP->ChipIdLo = MonReadChipId();

   RspMsgP->ChipIdHi      = 0;
#else
    RspMsgP->ChipIdHi = (uint16)atoh(M2S(CUSTOM_HW_VERSION));
    char* ptr = M2S(CUSTOM_HW_VERSION);
    while(*ptr != '.' && *ptr) ptr++;
    RspMsgP->ChipIdLo = (uint16)atoh(ptr + 1);
#endif
   for (Index = 0; Index < 2; Index++)
   {
      /* get ptr to the DSP build info area in the response msg buffer */
      RspBuildInfoP = &RspMsgP->BuildInfo[Index];

      /* date */
      SysMemcpy ((void *) RspBuildInfoP->BuildDate,
                 (void *) NOT_AVAILABLE_STR,
                 sizeof (NOT_AVAILABLE_STR));

      /* time */
      SysMemcpy ((void *) RspBuildInfoP->BuildTime,
                 (void *) NOT_AVAILABLE_STR,
                 sizeof (NOT_AVAILABLE_STR));

      /* patch version */
      SysMemcpy ((void *) RspBuildInfoP->PatchRevision,
                 (void *) NOT_AVAILABLE_STR,
                 sizeof (NOT_AVAILABLE_STR));

      /* ID string */
      SysMemcpy ((void *) RspBuildInfoP->IdString,
                 (void *) NOT_AVAILABLE_STR,
                 sizeof (NOT_AVAILABLE_STR));
   }

   /***********************************************************************/
   /* Send response message back to sender of CBP version command message */
   /***********************************************************************/

   ExeMsgSend(RxMsgP->RspInfo.TaskId, RxMsgP->RspInfo.MailboxId,
              RxMsgP->RspInfo.MsgId, (void *) RspMsgP, MsgSize);
#endif              
}

