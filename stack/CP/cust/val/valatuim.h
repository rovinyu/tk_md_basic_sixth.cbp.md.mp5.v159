/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2005-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef VALATUIM_H
#define VALATUIM_H
/*************************************************************************************************
* 
* FILE NAME   : valatuim.h
*
* DESCRIPTION : This file contains the declarations, definitions and data structures
*
* HISTORY     :
*     See Log at end of file
*
**************************************************************************************************/

typedef enum 
{
   ATChv1NotVerified=0,
   ATChv1Verified=1,
   ATChv1InitVal=2
} ATUIMChv1VerifyT;/*2: initial value;  0:Uim need CHV1, PIN has not been inputed;  1:Uim need CHV1, PIN  has been inputed*/

#define AT_UTK_MAX_SMS 10
#define AT_UTK_CDMA_SMSPP_DOWNLOAD_TAG 0xD1 
#define AT_UTK_DEVICE_IDENT_TAG 0X02
#define AT_UTK_RESULT_TAG 0X83
#define AT_UTK_CDMA_SMS_TPDU_TAG 0X48
#define AT_UTK_PROACTIVE_COMMAND_TAG 0xD0
#define AT_UTK_COMMAND_DETAILS_TAG 0x81
#define AT_UTK_CDMA_SEND_SMS_CMD 0x13
#define AT_UTK_ALPHA_IDENT_TAG 0X05
#define AT_UTK_ADDRESS_TAG 0X06
#define AT_UTK_CDMA_SMS_TPDU_TAG 0X48

#ifdef FEATURE_UTK
//#define AT_UTK_ITEM_MAX_LEN 32
#define AT_UTK_ITEM_MAX_LEN 300
//#define AT_UTK_SMSPDU_MAX_LEN 100
#define AT_UTK_SMSPDU_MAX_LEN 200
//#define AT_UTK_BIN_DATA_MAX_LEN 201
#define AT_UTK_BIN_DATA_MAX_LEN 600
#define AT_UTK_GET_INPUTE_MAX_LEN 30
#define AT_UTK_ADDRESS_MAX_LEN 32
#define AT_UTK_PROACTIVE_CMD_RESPONSE_TIMEOUT 3000
#endif

enum
{
    AT_UTK_KEYPAD = 0x01,
    AT_UTK_DISPLAY = 0x02,
    AT_UTK_EARPIECE = 0x03,
    AT_UTK_RUIM = 0x81,
    AT_UTK_TERMINAL = 0x82,
    AT_UTK_NETWORK = 0x83
};
enum
{
    AT_UTK_BUSY = 33,
    AT_UTK_DATA_DOWNLOAD_ERROR = 39
};

#ifdef MTK_CBP
#define SPC_VALUE_LEN 6
#define VALLIDATE_SPC_BLOCK_ID 0
#define RC_ACCEPTED  0
#endif

typedef struct
{
    ValSmsAddressT        OrigAddress;        /* required for submit and delivery message */
    bool                  SubaddressPre;
    ValSmsSubaddressT     OrigSubaddress;     /* optional */
    uint16                MsgId;          /* message id, required, UI NEEDN'T fill it */
    ValSmsTeleSrvIdT      TeleSrvId;      /* teleservice id, required, UI MUST give it a correct value */
    bool                  IsPPdownload;
    uint8                 seq_id;
}ValAtUtkSmsInfoT;

#ifdef MTK_DEV_C2K_IRAT
typedef enum
{
    AT_UTK_UNABLE_TO_USE = 0,
    AT_UTK_INVALID_STATE = 255
}ValAtUtkStateT;
#endif

#ifdef MTK_CBP
#define VAL_UIM_MAX_ECC_NUM_LEN         7   /*((UIM_ECC_NUMBER_BCD_LEN * 2) +1)*/
#define VAL_UIM_MAX_ECC_NUM_COUNT       10  /*MAX_ECC_LIST*/

typedef struct
{
    uint8           valEccNumStr[VAL_UIM_MAX_ECC_NUM_LEN];     /*ECC Number*/
}ValAtUimEccNumT;

typedef struct
{
    uint8           uEccMaxNum;            /*the max number of aValEccNumList*/
    
    ValAtUimEccNumT aValEccNumList[VAL_UIM_MAX_ECC_NUM_COUNT];/*Ecc number list*/
} ValAtUimEccNumListT;

typedef enum
{
    AT_ECST_SERVICE_NOT_SUPPRORTED = 0,
    AT_ECST_SERVICE_SUPPRORTED,
    AT_ECST_SERVICE_ALLOCATED_NOT_ACTIVATED
}ValAtEcstResultT;
#endif

bool UimInserted(void);
#ifdef MTK_DEV_C2K_IRAT
void SendEUTKSTToAtc(ValAtUtkStateT utkstate);
#endif

void SendUIMSTToAtc(ATUimStateT uimState);
CTATUimStateT ATtoCTUimState(ATUimStateT uimState);
void AtUtkProActiveCmdRspTmoCallback(uint32 TimerId);

void ATUimNotifyHandler(RegIdT RegId, uint32 MsgId, void *MsgBufferP);
void MmiATCLCK (AtcSendAtMsgT* MsgDataP);
void MmiATVICCID (AtcSendAtMsgT* MsgDataP);
void ValAtSendUimRemovedInd(void);
void ValSetUimNamReady(bool value);
bool ValGetUimNamReady(void);
void ValAtSendGetUimNamInd(void);
void MmiATCSIM (AtcSendAtMsgT* MsgDataP);
void ValAtUimProcessCsimAckMsg(void* MsgDataP);
void MmiATCRSM (AtcSendAtMsgT* MsgDataP);
void ValAtUimProcessCrsmAckMsg( void* MsgDataP );
void MmiATUTKURCREG (AtcSendAtMsgT* MsgDataP);
void ValUtkProCmdAtUtcMsg( void* MsgDataP );
void MmiATCIMI (AtcSendAtMsgT* MsgDataP);
void MmiATESIMINFO (AtcSendAtMsgT* MsgDataP);
void MmiATCGLA (AtcSendAtMsgT* MsgDataP);
void MmiATCRLA (AtcSendAtMsgT* MsgDataP);
void MmiATCCHO (AtcSendAtMsgT* MsgDataP);
void MmiATCCHC (AtcSendAtMsgT* MsgDataP);

void MmiATCCID (AtcSendAtMsgT* MsgDataP);

void MmiATCPIN (AtcSendAtMsgT* MsgDataP);
void AtUimGenVpmCallback(uint32 TimerId);
void MmiATHCPIN (AtcSendAtMsgT* MsgDataP);
void MmiATHMD5(AtcSendAtMsgT* MsgDataP);
void MmiATHGSN(AtcSendAtMsgT* MsgDataP);
void MmiATHSSDUPD(AtcSendAtMsgT* MsgDataP);
void MmiATHSSDUPDCFM(AtcSendAtMsgT* MsgDataP);
void MmiATHUIMAUTH(AtcSendAtMsgT* MsgDataP);
void MmiATHVPM(AtcSendAtMsgT* MsgDataP);
void MmiATCPWD (AtcSendAtMsgT* MsgDataP);
void MmiATCPUK (AtcSendAtMsgT* MsgDataP);
void MmiATCPINC (AtcSendAtMsgT* MsgDataP);
void MmiATCPINE (AtcSendAtMsgT* MsgDataP);
void MmiATCPIN2 (AtcSendAtMsgT* MsgDataP);
void MmiATCPUK2 (AtcSendAtMsgT* MsgDataP);
void MmiATCSPN(AtcSendAtMsgT* MsgDataP);
void MmiATCSMSVP(AtcSendAtMsgT* MsgDataP);
#ifdef MTK_CBP
void MmiATECSIMP(AtcSendAtMsgT* MsgDataP);
void MmiATECSTQ(AtcSendAtMsgT* MsgDataP);
UimECstIndexT ValAtGetEcstIndexByServiceIndex(uint8 cardType,uint8 serviceIndex);
ValAtEcstResultT ValAtGetEcstResult(uint8 cardType,UimECstIndexT EcstIndex,uint8 result);
#endif
void MmiATUTRFSH(AtcSendAtMsgT* MsgDataP);
void MmiATVRESETUIM (AtcSendAtMsgT* MsgDataP);
void MmiATVTURNOFFUIM (AtcSendAtMsgT* MsgDataP);
void MmiATUTKPD (AtcSendAtMsgT* MsgDataP);
void MmiATUTKTERM (AtcSendAtMsgT* MsgDataP);
void MmiATUTKENV (AtcSendAtMsgT* MsgDataP);
void MmiATUTKCONF (AtcSendAtMsgT* MsgDataP);
void MmiATHCAVE(AtcSendAtMsgT* MsgDataP);
void MmiATVCIMI (AtcSendAtMsgT* MsgDataP);

#ifdef FEATURE_UTK
void MmiATUTGC(AtcSendAtMsgT* MsgDataP);
void MmiATUTCR(AtcSendAtMsgT* MsgDataP);
void MmiATUTPD(AtcSendAtMsgT* MsgDataP);
void MmiATUTMS(AtcSendAtMsgT* MsgDataP);
void MmiATUTRT(AtcSendAtMsgT* MsgDataP);
void MmiATUTTONE(AtcSendAtMsgT* MsgDataP);
void MmiATUTPC(AtcSendAtMsgT* MsgDataP);
#ifdef MTK_DEV_C2K_IRAT
void MmiATEUTK(AtcSendAtMsgT *pMsgData);
void MmiATUTKMENU(AtcSendAtMsgT *pMsgData);
void MmiATSTKMENU(AtcSendAtMsgT *pMsgData);
#endif
#endif

#ifdef MTK_DEV_C2K_IRAT
void MmiATEBTSAP(AtcSendAtMsgT * MsgDataP);
void ValAtUIMProcessBtsapRspMsg(uint32 MsgId,void* MsgDataP);
extern void ValAtAddUimInfoReadMask(uint8 mask);
#endif
extern uint8 getuint8( uint8 *data, uint16 startBit, uint8 numBits );
extern uint8 ValUimGetMaxSmsVPRecNum(void);
extern bool ValUimGetSmsVPRec(uint8 index, uint8* ptvp);
extern bool ValUimUpdateSmsVPRec(uint8 index,uint8 vp);
extern UINT32 ATgetAuthrRegistration(UINT32 rand_input, UINT32 esn, UINT32 imsi_s1, UINT16 CaveMsgId);
extern void ValAtRegUtkProCmd( void );
extern UINT8* PswGetMeidInDbPtr(void);
extern bool IsPhbUimInitOk(void);
extern bool IsPhbFlashInitOk(void);
extern bool ValSmsGetSmsStatus( void );
extern void InitAfterPV(void);
extern void ValSetIccidReady(bool status);
extern bool ValGetIccidReady(void);
extern void ValSmsPdu2Txt( uint8*          ValSmsPduP,
                    uint8           Length,
                    ValSmsTxtRecordT* ValSmsMsgP );
#ifdef SYS_OPTION_NO_UI
extern void ValSmsReInit(ValDeviceT DevType);
extern ValPhbResultT  ValPhbReInitialAllContact (ValDeviceT DevType);
#endif
#ifdef MTK_CBP
void ValAtUimSpcValidateReq(uint32 spc);

extern char ValUimBcdToChar
    (
        uint8               bcd
    );
extern uint8 ValUimBcdTostring
    (
        uint8               uBcdNum, 
        uint8              *pbcdArray, 
        uint8              *pStrArray, 
        uint8               uStrArrayLen
    );

extern void ValUimConvEccNumListToStr
    (
        uint8               uCntOfEccNumList, 
        ValAtUimEccNumT    *pStEccNumList,
        uint8               uEccNumStrLen, 
        uint8              *pEccNumStr
    );

extern void MmiATCECC 
    (
        AtcSendAtMsgT      *MsgDataP
    );
extern void SendCeccToAtc
    (
        uint8               uCntOfEccNumList, 
        ValAtUimEccNumT    *pStEccNumList
    );
extern void ValAtUimProcessValUimEccNumListIndMsg
    (
        UimEccListIndMsgT  *MsgP
    );

extern bool ValUimIsEmergencyCall
    (
        uint8 NumDigits,
        uint8 * DigitsP
    );
#endif                  
#endif

