/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
/**************************************************************************************************
* %version: 2.1.4 %  %instance: HZPT_2 %   %date_created: 2008/04/01 14:42:23 %  %created_by: hwang %  %derived_by: hwang %
**************************************************************************************************/

/*****************************************************************************
 
  FILE NAME: FsmApi.h

  DESCRIPTION:

    Fsm definitions and prototype.

*****************************************************************************/


#ifndef __FSM_API_H__

#define __FSM_API_H__

#include "fsmdefs.h"
#include "FsmDev.h"
#include "FsmFs.h"

/* Operation mode */

#define FSM_MAX_VFS_FD             16

/* These options can be ORed together. -- access mode. */
#define FSM_OPEN_READ			0x00000001
#define FSM_OPEN_WRITE			0x00000002
#define FSM_OPEN_SHARE			0x00000004

#define FSM_ACCESS_MODE_MASK	0x000000FF

/* These options exclude each other. -- open mode.     */
#define FSM_CREATE_NEW			0x00000100
#define FSM_CREATE_ALWAYS		0x00000200  
#define FSM_OPEN_EXISTING		0x00000400
#define FSM_OPEN_ALWAYS			0x00000800

#define FSM_OPEN_MODE_MASK		0x0000FF00

#define FSM_MODE_MASK			0x0000FFFF

/* one and only one mode in these could be set */
#define FSM_OPEN_FILE			0x00010000
#define FSM_OPEN_DIR			0x00020000



/* Seek mode */
#define FSM_FILE_BEGIN			1
#define FSM_FILE_END			2
#define FSM_FILE_CURRENT		3


#define INVALID_FILE_HANDLE		((uint32)(-1))


typedef uint32				HFSMFILE;

typedef void *				HENUM;


#define MAX_FILE_NAME_LEN	256      /*16*/	/*  max filename length is determined by Dfs/nffs/Dos design. */

typedef struct
{
	char			FileName[MAX_FILE_NAME_LEN + 1];
	uint32			FileLength;
	uint16			CreateDate;
	uint16			CreateTime;
	uint16			Attrib;
}FsmFileInfoT;

typedef struct
{
	uint32			FileLength;
	uint16			CreateDate;
	uint16			CreateTime;
	uint16			Attrib;
}FsmFileAttribT;

#define FSM_FORMAT_NAME      1
#define FSM_FORMAT_DEV       2

typedef struct
{
	FsmFsDrvT       *	FsP;
	FsmDevObjHdrT   *	DevP;
	uint8		FormatType;	 /* FsmSystemTypeT defined */

} FsmFormatArgumentT;

/* Define volume type */
typedef enum{

   FSM_VOLUME_TYPE_UNKNOWN = 0,
   FSM_VOLUME_TYPE_REMOVABLE, /* The drive has removable media; for example, a SD Card */
   FSM_VOLUME_TYPE_FLASH

}FsmVolumeTypeT;

 /* Define system type */
typedef enum{

  FSM_SYSTEM_TYPE_UNKNOWN = 0,
  FSM_SYSTEM_TYPE_DFS, 
  FSM_SYSTEM_TYPE_FAT12,
  FSM_SYSTEM_TYPE_FAT16,
  FSM_SYSTEM_TYPE_FAT32,
  FSM_SYSTEM_TYPE_NTFS

}FsmSystemTypeT;
 
typedef struct
{
   uint32         TotalSpace;
   uint32         FreeSpace;
   uint32         hTotalSpace;		/* high 32bit value of total space for >4GB */
   uint32         hFreeSpace;		/* high 32bit value of free  space for >4GB */
   FsmVolumeTypeT VolumeType;
   FsmSystemTypeT SystemType;
} FsmVolumeInfoT;

#define MAX_PATH_LENGTH			256		/*  max PATH length is determined by Dfs/nffs/Dos design. */


#define FSM_FILE_ATTRIB_NORMAL         0x00
#define FSM_FILE_ATTRIB_READONLY       0x01
#define FSM_FILE_ATTRIB_HIDDEN         0x02
#define FSM_FILE_ATTRIB_SYSTEM         0x04
#define FSM_FILE_ATTRIB_ARCHIVE		   0x08
#define FSM_FILE_ATTRIB_DRM		   0x40     /*add for app*/
#define FSM_FILE_ATTRIB_ROM		   0x80     /*add for app*/
#define FSM_FILE_ATTRIB_MASK             0xFF/*0x0F*/

#define FSM_FILE_ATTRIB_DIR            0x10
#define FSM_FILE_ATTRIB_FILE           0x20
#define FSM_FILE_TYPE_MASK			   0x30

#define MAX_VOLUME_NAME_LEN            16

#define FSM_ILLEGAL_VALUE 0x14

typedef struct
{
	char			* 	VolumeName;
	FsmFsDrvT       *	FsP;
	FsmDevObjHdrT   *	DevP;
   uint16         SectorSize;
} FsmDevListT;


/*========================= File System APIs ===============================*/

HFSMFILE	FsmOpen(const char * path, uint32 mode);

uint32		FsmClose(HFSMFILE hFile);
uint32		FsmCloseAll(void);

uint32		FsmRead(HFSMFILE hFile, void * buffer, uint32 size);
uint32		FsmWrite(HFSMFILE hFile, void * buffer, uint32 size);

uint32		FsmSeek(HFSMFILE hFile, int32 offset, uint32 whence);

uint32		FsmTell(HFSMFILE hFile);

uint32		FsmEof(HFSMFILE hFile, uint32 * Eof);

uint32		FsmError(HFSMFILE hFile);

uint32		FsmGetAttrib(HFSMFILE hFile, FsmFileAttribT * attrib);
uint32		FsmGetAttribByName(const char * path, FsmFileAttribT * attrib);

uint32		FsmSetAttrib(const char * path, FsmFileAttribT * attrib);

uint32		FsmTruncate(HFSMFILE hFile, uint32 size);

uint32		FsmRemove(const char* file_dir_name);

uint32		FsmRename(const char *path, const char *newname);

uint32		FsmMakeDir(const char *path);
uint32		FsmRemoveDir(const char *path);

HENUM		FsmFindFirstFile(const char * filename, FsmFileInfoT * Info);
uint32		FsmFindNextFile(HENUM hEnum, FsmFileInfoT * Info);

uint32		FsmFindClose(HENUM hEnum);

uint32		FsmCloseDir(HFSMFILE hFile);

uint32		FsmFlush(HFSMFILE hFile);
uint32		FsmFlushAll(void);

uint32		FsmGetFreeSpace(const char * VolumeName, uint32 * totalspace, uint32 * freespace);

uint32 		FsmGetVolumeInfo(const char * VolumeName, FsmVolumeInfoT *VolumeInfoP);

uint32		FsmFileIoCtrl(HFSMFILE hFile, uint32 cmd, void * arg);

HFSMFILE	FsmOpenDir(const char *path);

uint32		FsmReadDir(HFSMFILE hDir, FsmFileInfoT * DirEntry);

/*uint32		FsmFormat(const char * VolumeName);*/
uint32      FsmFormat(FsmFormatArgumentT * FormatArg); 

uint32		FsmMount(FsmFsDrvT * pFs, FsmDevObjHdrT * pDev, const char * VolumeName, uint32 sectorsize);

uint32		FsmUnMount(const char* VolumeName);

uint32 		FsmForceUnMount(const char* VolumeName);

uint32		FsmGetLastErrorCode(void);
bool 		FsmIsReclaimActive (void);

#endif /* __FSM_API_H__ */


/*****************************************************************************
* $Log: FsmApi.h $
* Revision 1.2  2008/05/19 14:53:54  hwang
* update for fsm
* Revision 1.1  2006/12/19 14:55:22  yjin
* Initial revision
* Revision 1.1  2006/11/26 21:35:25  yliu
* Initial revision
* Revision 1.2  2006/10/25 15:30:41  yjin
* fsm update
* Revision 1.1  2006/10/24 15:00:58  binye
* Initial revision
* Revision 1.2  2006/01/07 11:46:04  wavis
* Merging in VAL.
* Revision 1.1.1.2  2005/12/14 11:15:13  wavis
* Added support for DOS File System.
* Revision 1.1.1.1  2005/11/08 13:18:53  wavis
* Duplicate revision
* Revision 1.1  2005/11/08 13:18:53  vnarayana
* Initial revision
* Revision 1.1  2005/11/07 14:33:29  wavis
* Initial revision
* Revision 1.1.1.2  2005/10/14 16:33:21  agontar
* bug fixes
* Revision 1.1.1.1  2005/10/12 15:47:44  agontar
* Duplicate revision
* Revision 1.1  2005/10/12 15:47:44  dorloff
* Initial revision
* Revision 1.2  2005/04/05 19:05:21  lwang
* Synchronized from FSM baseline:
*    reduced the length of MAX file path and MAX file name.
* Revision 1.1  2005/01/30 11:01:56  lwang
* Initial revision
* Revision 1.3  2004/09/30 12:01:59  lwang
* for compatible
* Revision 1.2  2004/09/29 15:49:48  lwang
* Revision 1.1  2004/08/03 12:40:31  javese
* Initial revision
* Revision 1.2  2004/06/07 15:34:35  jjs
* delete PACKED qualifier in the structure definitions.
* Revision 1.1  2004/05/21 16:57:45  jjs
* Initial revision
* Revision 1.5  2004/03/17 12:58:32  zgy
* Revision 1.20  2004/03/16 15:59:34  jjs
* Revision 1.19  2004/03/11 14:57:12  jjs
* Format api support.
* Revision 1.18  2003/12/04 11:24:01  wsm
* correct #define FSM_TYPE_DIR            0x10
* Revision 1.17  2003/11/25 16:02:18  wsm
* 1.attrib define change
* Revision 1.16  2003/11/05 10:21:24  wsm
* Revision 1.15  2003/10/26 17:01:11  jjs
* Updated the function prototype of FsmEof.
* Revision 1.14  2003/10/24 17:06:56  jjs
* added FsmFormat declaration
* Revision 1.13  2003/10/16 11:13:05  jjs
* Revision 1.12  2003/10/08 17:59:49  jjs
* Revision 1.11  2003/10/08 17:55:22  jjs
* Revision 1.10  2003/10/08 12:52:17  jjs
* Revision 1.9  2003/09/19 11:24:56  wsm
* Revision 1.8  2003/09/17 14:08:58  wsm
* Revision 1.7  2003/09/16 17:11:32  wsm
* Revision 1.6  2003/09/16 11:32:15  wsm
* Revision 1.5  2003/09/14 16:56:10  jjs
* Revision 1.4  2003/09/12 15:27:13  wsm
* FD_MAX 
* Revision 1.3  2003/09/12 13:43:21  jjs
* Revision 1.2  2003/09/11 09:55:26  wsm
* update the Function name and include file
* Revision 1.1  2003/09/09 15:09:14  jjs
* Initial revision
*****************************************************************************/



/**Log information: \main\vtui2_5x\2 2008-06-02 08:31:55 GMT hwang
** HREF#416**/
/**Log information: \main\vtui2_5x\3 2008-10-13 05:17:43 GMT rli
** HREF#2435: add the interface force unmount in the vfs layer to support the sd card removed**/
/**Log information: \main\vtui2_5x\4 2008-12-22 12:09:04 GMT yjin
** HREF#4142**/
/**Log information: \main\helios_dev\helios_dev_hwang_href7865\1 2009-07-27 08:46:13 GMT hwang
** HREF#7865**/
/**Log information: \main\helios_dev\3 2009-07-27 09:08:14 GMT yjin
** href7865:provide share mode to open multi-files**/
/**Log information: \main\helios_dev\helios_dev_hwang_href9218\1 2009-11-19 10:11:09 GMT hwang
** HREF#9218**/
/**Log information: \main\helios_dev\4 2009-11-19 11:59:46 GMT yjin
** href9218**/
/**Log information: \main\VTUI3\VTUI3_hwang_href11124\1 2010-03-26 08:40:49 GMT hwang
** HREF#11124**/
/**Log information: \main\VTUI3\2 2010-03-29 05:46:08 GMT yjin
** href11124**/
