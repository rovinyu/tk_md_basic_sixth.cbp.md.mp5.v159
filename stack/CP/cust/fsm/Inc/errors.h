/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
/**************************************************************************************************
* %version: 2 %  %instance: HZPT_2 %   %date_created: Fri Mar 23 15:07:44 2007 %  %created_by: jingzhang %  %derived_by: jingzhang %
**************************************************************************************************/

/*****************************************************************************
 
  FILE NAME: errors.h

  DESCRIPTION:

    Defines error code.

*****************************************************************************/



#ifndef __ERRORS_H__

#define __ERRORS_H__


enum
{
	ERR_NONE = 0,
	ERR_PARAM = 0x1000,
	ERR_READ,
	ERR_WRITE,
	ERR_ERASE,
	ERR_TIMEOUT,
	ERR_SYSTEM,
	ERR_EXIST,
	ERR_NOTEXIST,
	ERR_FULL,
	ERR_INIT,
	ERR_UNKOWN,
	ERR_EOF,
	ERR_NOT_EOF,
	ERR_NOT_SUPPORT,
	ERR_FORMAT,
	ERR_MEMORY,
	ERR_ACCESS_DENY,
	ERR_ACCESS,
	ERR_MOUNTED,
	ERR_MAX_VOLUME,
	ERR_MAX_OPEN,
	ERR_UNMOUNTED,
	ERR_EXTERNAL_DEV,
	ERR_DEVICE
};


#endif /* __ERRORS_H__ */



/*****************************************************************************
* $Log: errors.h $
* Revision 1.1  2007/10/29 11:51:08  binye
* Initial revision
* Revision 1.1  2007/10/12 09:50:55  dsu
* Initial revision
* Revision 1.1  2006/12/19 14:55:21  yjin
* Initial revision
* Revision 1.1  2006/11/26 21:35:24  yliu
* Initial revision
* Revision 1.1  2006/10/24 15:00:58  binye
* Initial revision
* Revision 1.1  2005/11/08 13:18:52  vnarayana
* Initial revision
* Revision 1.1  2005/11/07 14:33:35  wavis
* Initial revision
* Revision 1.1  2005/10/12 15:47:43  dorloff
* Initial revision
* Revision 1.1  2005/01/30 12:01:54  lwang
* Initial revision
* Revision 1.2  2004/09/29 15:50:05  lwang
* Revision 1.1  2004/08/03 12:40:30  javese
* Initial revision
* Revision 1.1  2004/05/21 16:57:44  jjs
* Initial revision
* Revision 1.3  2004/03/17 12:58:41  zgy
* Revision 1.5  2004/03/16 15:59:32  jjs
* Revision 1.4  2003/11/05 10:52:11  zgy
* Revision 1.3  2003/10/16 11:39:07  jjs
* added ERR_ACCESS error code.
* Revision 1.2  2003/10/08 19:42:20  jjs
* Revision 1.1  2003/10/08 18:27:08  jjs
* Initial revision
* Initial revision
*****************************************************************************/


/**Log information: \main\vtui2_5x\2 2008-06-28 13:42:21 GMT khong
** HREF#747 :
|Merge selle software to baseline.**/
