/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef _HWDRFPUBLIC_H
#define _HWDRFPUBLIC_H

/*****************************************************************************
* 
* FILE NAME   : hwdrfpublic.h
*
* DESCRIPTION : Header file containing typedefs and definitions pertaining
*               to the RF custom files.
*****************************************************************************/

/*----------------------------------------------------------------------------
 Include Files
----------------------------------------------------------------------------*/
#include "sysdefs.h"
#include "dbmapi.h"
#include "hwdrfapi.h"
#include "hwdapi.h"
#include "dspiparm.h"


/*----------------------------------------------------------------------------
 Defines and Macros
----------------------------------------------------------------------------*/
#define IS_C2KRF_FCI_7790   (SYS_OPTION_RF_HW == SYS_RF_FCI_7790)
#define IS_C2KRF_ORION_C    (SYS_OPTION_RF_HW == SYS_RF_MTK_ORIONC)
#define IS_C2KRF_ORION_PLUS (SYS_OPTION_RF_HW == SYS_RF_MT6176)

#define NON_USED_BAND       (0xff)
#define NONE_USED_PRX       NON_USED_BAND
#define NONE_USED_DRX       NON_USED_BAND
#define TX_NULL_BAND        NON_USED_BAND
#define TX_DET_IO_NULL      NON_USED_BAND
#if IS_C2KRF_FCI_7790
/* FCI7790 driver do not need port selection.
   Only for HW SIM */
#define LNA_LB_1            (1)
#define LNA_HB_1            (2)
#define LNA_HB_2            (3)

#define TX_LB_1             (1)
#define TX_HB_1             (2)
#define TX_HB_2             (3)
#elif IS_C2KRF_ORION_C
#define LNA_LB_1            (1)
#define LNA_HB_1            (2)
#define LNA_HB_2            (3)

#define TX_LB_1             (1)
#define TX_HB_1             (2)
#define TX_HB_2             (3)
#elif IS_C2KRF_ORION_PLUS
#define PRX1                (0)
#define PRX2                (1)
#define PRX3                (2)
#define PRX4                (3)
#define PRX5                (4)
#define PRX6                (5)
#define PRX7                (6)
#define PRX8                (7)
#define PRX9                (8)
#define PRX10               (9)
#define PRX11               (10)
#define PRX12               (11)
#define PRX13               (12)
#define PRX14               (13)
#define DRX1                PRX1
#define DRX2                PRX2
#define DRX3                PRX3
#define DRX4                PRX4
#define DRX5                PRX5
#define DRX6                PRX6
#define DRX7                PRX7
#define DRX8                PRX8
#define DRX9                PRX9
#define DRX10               PRX10
#define DRX11               PRX11
#define DRX12               PRX12
#define DRX13               PRX13
#define DRX14               PRX14

#define TX_HB1              (0)
#define TX_HB2              (1)
#define TX_MB1              (2)
#define TX_MB2              (3)
#define TX_MB3              (4)
#define TX_MB4              (5)
#define TX_LB1              (6)
#define TX_LB2              (7)
#define TX_LB3              (8)
#define TX_LB4              (9)

#define TX_DET_IO_DET1      (0)
#define TX_DET_IO_DET2      (1)
#else
#error  "No RF chip defined!"
#endif

#define HWD_RF_SPI_PGA_CW_SEND_TIME_IN_CHIPS     (3)

#if (SYS_BOARD >= SB_JADE)
#define C2K_TAS_BPI_PIN_NULL                    (-1)
#define C2K_TAS_BPI_PIN_GEN(pIN1eN, pIN2eN, pIN3eN, sETiDX) \
    ((C2K_TAS_BPI_PIN1_##sETiDX == C2K_TAS_BPI_PIN_NULL ? 0 : ((pIN1eN) << C2K_TAS_BPI_PIN1_##sETiDX)) \
    |(C2K_TAS_BPI_PIN2_##sETiDX == C2K_TAS_BPI_PIN_NULL ? 0 : ((pIN2eN) << C2K_TAS_BPI_PIN2_##sETiDX)) \
    |(C2K_TAS_BPI_PIN3_##sETiDX == C2K_TAS_BPI_PIN_NULL ? 0 : ((pIN3eN) << C2K_TAS_BPI_PIN3_##sETiDX)))
#else
#define C2K_TAS_BPI_PIN_GEN(pIN1eN, pIN2eN, pIN3eN) \
    ((C2K_TAS_BPI_PIN1 == C2K_TAS_BPI_PIN_NULL ? 0 : ((pIN1eN) << C2K_TAS_BPI_PIN1)) \
    |(C2K_TAS_BPI_PIN2 == C2K_TAS_BPI_PIN_NULL ? 0 : ((pIN2eN) << C2K_TAS_BPI_PIN2)) \
    |(C2K_TAS_BPI_PIN3 == C2K_TAS_BPI_PIN_NULL ? 0 : ((pIN3eN) << C2K_TAS_BPI_PIN3)))
#endif
#if defined(MTK_PLT_RF_ORIONC) && !defined(MTK_DEV_HW_SIM_RF)
#define RF_CUST_SUPPORT_BAND_MAX_NUM    3
#else
#define RF_CUST_SUPPORT_BAND_MAX_NUM    HWD_RF_BAND_MAX
#endif
#define HWD_NUM_SAR_TX_POWER_OFFSET_TABLE   (9)
#define HWD_ANTENNA_MAX_NUM             (2)
#define HWD_RAT_MAX_NUM                 (2)
#define HWD_NUM_TX_POWER_OFFSET_TABLE   (2)

/*----------------------------------------------------------------------------
 Custom segments Typedefs
----------------------------------------------------------------------------*/
/** Data definition of custom data segement */
/* Band Class that supported */
typedef NV_PACKED_PREFIX struct
{
    SysCdmaBandT sysBandClass;
    bool supported;
    uint32 subClass;
} NV_PACKED_POSTFIX HwdSupportedBandClassT;

typedef NV_PACKED_PREFIX struct
{
    /* BPI data */
    uint32 xPDATA_PR1;       /* Main Rx On */
    uint32 xPDATA_PR2;
    uint32 xPDATA_PR2B;
    uint32 xPDATA_PR3;       /* Main Rx Off */
    uint32 xPDATA_PR3A;
    uint32 xPDATA_PT1;       /* Tx On */
    uint32 xPDATA_PT2;
    uint32 xPDATA_PT2B;
    uint32 xPDATA_PT3;       /* Tx Off */
    uint32 xPDATA_PT3A;
    uint32 xPDATA_RXD_PR1;   /* Diversity Rx On */
    uint32 xPDATA_RXD_PR2;
    uint32 xPDATA_RXD_PR2B;
    uint32 xPDATA_RXD_PR3;   /* Diversity Rx Off */
    uint32 xPDATA_RXD_PR3A;

    /* TXGATE data */
    uint32 xPDATA_PRG1;       /* Main Rx On */
    uint32 xPDATA_PRG2;
    uint32 xPDATA_PRG2B;
    uint32 xPDATA_PRG3;       /* Main Rx Off */
    uint32 xPDATA_PRG3A;
    uint32 xPDATA_PTG1;       /* Tx On */
    uint32 xPDATA_PTG2;
    uint32 xPDATA_PTG2B;
    uint32 xPDATA_PTG3;       /* Tx Off */
    uint32 xPDATA_PTG3A;
    uint32 xPDATA_RXD_PRG1;   /* Diversity Rx On */
    uint32 xPDATA_RXD_PRG2;
    uint32 xPDATA_RXD_PRG2B;
    uint32 xPDATA_RXD_PRG3;   /* Diversity Rx Off */
    uint32 xPDATA_RXD_PRG3A;
}NV_PACKED_POSTFIX HwdBpiDataExpandT;

typedef NV_PACKED_PREFIX struct
{
    /* TX Power Control data */
    uint32 xPDATA_PRPC1;       /* Main Rx On */
    uint32 xPDATA_PRPC2;
    uint32 xPDATA_PRPC2B;
    uint32 xPDATA_PRPC3;       /* Main Rx Off */
    uint32 xPDATA_PRPC3A;
    uint32 xPDATA_PTPC1;       /* Tx On */
    uint32 xPDATA_PTPC2;
    uint32 xPDATA_PTPC2B;
    uint32 xPDATA_PTPC3;       /* Tx Off */
    uint32 xPDATA_PTPC3A;
    uint32 xPDATA_RXD_PRPC1;   /* Diversity Rx On */
    uint32 xPDATA_RXD_PRPC2;
    uint32 xPDATA_RXD_PRPC2B;
    uint32 xPDATA_RXD_PRPC3;   /* Diversity Rx Off */
    uint32 xPDATA_RXD_PRPC3A;
} NV_PACKED_POSTFIX  HwdBpiTpcDataExpandT;

typedef NV_PACKED_PREFIX struct
{
    HwdBpiDataExpandT data[RF_CUST_SUPPORT_BAND_MAX_NUM];
    HwdBpiTpcDataExpandT tpcData;
} NV_PACKED_POSTFIX  HwdRfCustBpiDataT;

typedef NV_PACKED_PREFIX struct
{
    /* BPI data */
    uint32 xPMASK_PR1;       /* Main Rx On */
    uint32 xPMASK_PR2;
    uint32 xPMASK_PR2B;
    uint32 xPMASK_PR3;       /* Main Rx Off */
    uint32 xPMASK_PR3A;
    uint32 xPMASK_PT1;       /* Tx On */
    uint32 xPMASK_PT2;
    uint32 xPMASK_PT2B;
    uint32 xPMASK_PT3;       /* Tx Off */
    uint32 xPMASK_PT3A;
    uint32 xPMASK_RXD_PR1;   /* Diversity Rx On */
    uint32 xPMASK_RXD_PR2;
    uint32 xPMASK_RXD_PR2B;
    uint32 xPMASK_RXD_PR3;   /* Diversity Rx Off */
    uint32 xPMASK_RXD_PR3A;

    /* TXGATE data */
    uint32 xPMASK_PRG1;       /* Main Rx On */
    uint32 xPMASK_PRG2;
    uint32 xPMASK_PRG2B;
    uint32 xPMASK_PRG3;       /* Main Rx Off */
    uint32 xPMASK_PRG3A;
    uint32 xPMASK_PTG1;       /* Tx On */
    uint32 xPMASK_PTG2;
    uint32 xPMASK_PTG2B;
    uint32 xPMASK_PTG3;       /* Tx Off */
    uint32 xPMASK_PTG3A;
    uint32 xPMASK_RXD_PRG1;   /* Diversity Rx On */
    uint32 xPMASK_RXD_PRG2;
    uint32 xPMASK_RXD_PRG2B;
    uint32 xPMASK_RXD_PRG3;   /* Diversity Rx Off */
    uint32 xPMASK_RXD_PRG3A;
} NV_PACKED_POSTFIX HwdBpiMaskExpandT;

typedef NV_PACKED_PREFIX struct
{
    /* TX Power Control data */
    uint32 xPMASK_PRPC1;       /* Main Rx On */
    uint32 xPMASK_PRPC2;
    uint32 xPMASK_PRPC2B;
    uint32 xPMASK_PRPC3;       /* Main Rx Off */
    uint32 xPMASK_PRPC3A;
    uint32 xPMASK_PTPC1;       /* Tx On */
    uint32 xPMASK_PTPC2;
    uint32 xPMASK_PTPC2B;
    uint32 xPMASK_PTPC3;       /* Tx Off */
    uint32 xPMASK_PTPC3A;
    uint32 xPMASK_RXD_PRPC1;   /* Diversity Rx On */
    uint32 xPMASK_RXD_PRPC2;
    uint32 xPMASK_RXD_PRPC2B;
    uint32 xPMASK_RXD_PRPC3;   /* Diversity Rx Off */
    uint32 xPMASK_RXD_PRPC3A;
} NV_PACKED_POSTFIX  HwdBpiTpcMaskExpandT;

typedef NV_PACKED_PREFIX struct
{
    HwdBpiMaskExpandT mask[RF_CUST_SUPPORT_BAND_MAX_NUM];
    HwdBpiTpcMaskExpandT tpcMask;
} NV_PACKED_POSTFIX  HwdRfCustBpiMaskT;


/* BPI control timing, in unit of chips */
typedef NV_PACKED_PREFIX struct
{
    /*** RF Window timing */
    uint16 xTC_PR1;
    uint16 xTC_PR2;
    uint16 xTC_PR2B;

    uint16 xTC_PR3;
    uint16 xTC_PR3A;

    uint16 xTC_RXD_PR1;
    uint16 xTC_RXD_PR2;
    uint16 xTC_RXD_PR2B;

    uint16 xTC_RXD_PR3;
    uint16 xTC_RXD_PR3A;

    uint16 xTC_PT1;
    uint16 xTC_PT2;
    uint16 xTC_PT2B;

    uint16 xTC_PT3;
    uint16 xTC_PT3A;

    /*** RF Gate timing */
    uint16 xTC_PRG1;
    uint16 xTC_PRG2;
    uint16 xTC_PRG2B;

    uint16 xTC_PRG3;
    uint16 xTC_PRG3A;

    uint16 xTC_RXD_PRG1;
    uint16 xTC_RXD_PRG2;
    uint16 xTC_RXD_PRG2B;

    uint16 xTC_RXD_PRG3;
    uint16 xTC_RXD_PRG3A;

    uint16 xTC_PTG1;
    uint16 xTC_PTG2;
    uint16 xTC_PTG2B;

    uint16 xTC_PTG3;
    uint16 xTC_PTG3A;

    /* RF TX Power Control timing */
    int16 xTC_PRPC1;
    int16 xTC_PRPC2;
    int16 xTC_PRPC2B;

    int16 xTC_PRPC3;
    int16 xTC_PRPC3A;

    int16 xTC_RXD_PRPC1;
    int16 xTC_RXD_PRPC2;
    int16 xTC_RXD_PRPC2B;

    int16 xTC_RXD_PRPC3;
    int16 xTC_RXD_PRPC3A;

    int16 xTC_PTPC1;
    int16 xTC_PTPC2;
    int16 xTC_PTPC2B;

    int16 xTC_PTPC3;
    int16 xTC_PTPC3A;
} NV_PACKED_POSTFIX HwdRfBpiTimingT;

typedef NV_PACKED_PREFIX struct
{
    int16 dc2DcSettlingTime;
} NV_PACKED_POSTFIX HwdRfPaControlTimingT;


/* RF parameter data structure */
typedef NV_PACKED_PREFIX struct
{
    /* Record structure version for tool if adding members in this structure.
       Reserved for future use */
    uint8 structVersion;
    /* be update to be TRUE after tool update parameters. Default: FALSE.FALSE.
       Reserved for future use */
    uint8 isDataUpdate;
    /* Band Class supported */
    HwdSupportedBandClassT supportedBands[RF_CUST_SUPPORT_BAND_MAX_NUM];
#if defined(MTK_PLT_RF_ORIONC) && !defined(MTK_DEV_HW_SIM_RF)
    /* BPI function assignment */
    HwdRfCustBpiMaskT bpiMask;
    /* BPI configuration data */
    HwdRfCustBpiDataT bpiData;
#endif
    /* TX PA control timing */
    HwdRfBpiTimingT bpiTiming;
    /* BPI control timing */
    HwdRfPaControlTimingT paTiming;
    /* RX LNA port selection */
    uint8 rxLnaPortSel[RF_CUST_SUPPORT_BAND_MAX_NUM];
    /* RX Diversity LNA port selection */
    uint8 rxDivLnaPortSel[RF_CUST_SUPPORT_BAND_MAX_NUM];
    /* TX path selection */
    uint8 txPathSel[RF_CUST_SUPPORT_BAND_MAX_NUM];
#if !defined(MTK_PLT_RF_ORIONC)
    /* TX detect path selection */
    uint8 txDetPathSel[RF_CUST_SUPPORT_BAND_MAX_NUM];
#endif
    /* Rx diversity enable */
    bool rxDiversityEnable;
    /* Rx diversity test enable. The diversity path will be always on if this
       is TRUE. Used by HSC */
    bool rxDiversityTestEnable;
    /* PMU VPA control disable/enable */
    bool paVddPmuControlEnable;
    /* BATT VPA control disable/enable */
    bool paVddBattControlEnable;
    /* DC2DC VPA control disable/enable */
    bool paVddDc2DcControlEnable;
	/* HPUE VPA control disable/enable */
#ifdef SYS_OPTION_HPUE_ENABLED
    bool paVddHpueControlEnable;
#endif
    /* Temperature measurement ADC selection: 1: RF internal ADC, 0: external ADC*/
    bool customer_TM_enable;
#ifdef __SAR_TX_POWER_BACKOFF_SUPPORT__
    /* SAR disable/enable */
    bool TPO_enable;
    /* SAR meta disable/enable */
    bool TPO_meta_enable;
#endif
    /* ETM VPA control disable/enable */
    bool paVddEtmControlEnable;


} NV_PACKED_POSTFIX HwdRfCustomDataT;

typedef enum {
    ANT_NULL = 0, /* NO TAS control */
    ANT_1,
    ANT_2,
    ANT_3,
    ANT_4,
    ANT_5,
    ANT_6,
    ANT_7,

    ANT_MAX_NUM,

    ANT_INVALID_NUM = ANT_MAX_NUM
}HwdRfCustTasAntNumT;

/** Data definition of custom TAS segement */
/* C2K Transmit Antenna Switch(TAS) configurations */
typedef NV_PACKED_PREFIX struct
{
    /* TAS enable */
    bool tasEanble;
#if (SYS_BOARD >= SB_JADE)
    /* TAS test SIM enable */
    bool tasTestSimEnable;
#endif
    /* TAS initial antenna index */
    uint8 tasInitAntIndex;
    /* Force Tx antenna enalbe */
    bool forceTxAntEnable;
    /* Force Tx antenna index */
    uint8 forceTxAntIndex;
    /* TAS BPI pin mask. If BPI n is used, bit n->1; otherwise bit n->0 */
    uint32 tasMask;
    /* Every band has 7 TAS data, corresponds to 7 anntenas */
    uint32 tasData[RF_CUST_SUPPORT_BAND_MAX_NUM][ANT_MAX_NUM - 1]; /* [HwdRfBandT][]*/

    /* TAS test SIM initial antenna index */
    uint8 tasTestSimInitAntIndex[RF_CUST_SUPPORT_BAND_MAX_NUM];
} NV_PACKED_POSTFIX HwdRfCustTasT;


/*----------------------------------------------------------------------------
  Calibration Global Typedefs and Variables Defines
----------------------------------------------------------------------------*/
/* AFC calibration parameters */
typedef NV_PACKED_PREFIX struct
{
  uint16 InitDacValue;   /*DAC value*/
  uint16 AfcSlopeInv;    /*AFC slope*/
  uint32 CapID;          /*CapID*/
} NV_PACKED_POSTFIX HwdAfcCalibParmT;

/* AFC compensation parameters */
typedef NV_PACKED_PREFIX struct
{
  float c0Fac;
  float c1Fac;
  float c2Fac;
  float c3Fac;
} NV_PACKED_POSTFIX HwdAfcCompCalibParmT;

/* Temperature calibration parameter items */
typedef NV_PACKED_PREFIX struct
{
  int8   TempCelsius;
  uint16 AuxAdcValue;
} NV_PACKED_POSTFIX  HwdTempCalibrPointT;

/* Temperature calibration table */
typedef NV_PACKED_PREFIX struct
{
    HwdTempCalibrPointT tempCalibTbl[HWD_NUM_TEMP_CALIBR_POINTS];
} NV_PACKED_POSTFIX  HwdTempCalibTableT;

/* TX pagain calibration parameters */
typedef NV_PACKED_PREFIX struct
{
 int16 Start;  /* Tx power for L2M or M2H, unit is 1/32dBm (Q5)*/
 int16 End;    /* Tx power for H2M or M2L, unit is 1/32dBm (Q5)*/
} NV_PACKED_POSTFIX HwdTxPaHystDataT;

typedef NV_PACKED_PREFIX struct
{
  uint8            PaCalSectionMum; /*Total PA calibration point for PA */
  HwdRfPaContextT  TxPwrPaContext[HWD_NUM_GAIN_POINTS_TXAGC];
  HwdTxPaHystDataT TxHyst[HWD_PA_MODE_NUM - 1]; /*Hyst Region*/
  int16            paPhaseJumpComp[HWD_PA_MODE_NUM];
} NV_PACKED_POSTFIX  HwdTxPwrCalibDataT;

/* TX pagain frequency adjustment calibration parameters */
typedef NV_PACKED_PREFIX struct
{
  uint16 FreqChanNum;   /* frequency channel number coordinate. Units: channel number */
  int16 TxPwrFreqAdjDb[HWD_PA_MODE_NUM];    /* Tx gain adjustment coordinate. Units: dB with Q=IPC_DB_Q */
} NV_PACKED_POSTFIX  HwdTxMultiGainFreqDataT;  /* format matches HwdCoordinateT */

typedef NV_PACKED_PREFIX struct
{
  HwdTxMultiGainFreqDataT   HwdTxMultiGainFreqAdj[HWD_NUM_FREQ_POINTS_TXAGC];
} NV_PACKED_POSTFIX  HwdTxMultiGainFreqAdjDataT;    


/* TX pagain temperature adjustment calibration parameters */
typedef NV_PACKED_PREFIX struct
{
  int8 TempCelsius;   /* Temperature coordinate. Units: celsius */
  int16 TxPwrTempAdjDb[HWD_PA_MODE_NUM];    /* Tx gain adjustment coordinate. Units: dB with Q=IPC_DB_Q */
} NV_PACKED_POSTFIX  HwdTxMultiGainTempDataT;  

typedef NV_PACKED_PREFIX struct
{
   HwdTxMultiGainTempDataT   HwdTxMultiGainTempAdj[HWD_NUM_TEMP_POINTS_TXAGC];
} NV_PACKED_POSTFIX  HwdTxMultiGainTempAdjDataT;    /* typedef for the message */


/* Tx power detect couplerloss calibration parameters */

typedef NV_PACKED_PREFIX struct
{
  int16 TxPwrDetColLossdB[HWD_PA_MODE_NUM]; /*Q5*/
} NV_PACKED_POSTFIX HwdTxPwrDetCalibDataT;

/* Tx power detect couplerloss frequency adjustment calibration parameters */

typedef NV_PACKED_PREFIX struct
{
  uint16 FreqChanNum;
  int16 TxPwrDetFreqAdjDb;
} NV_PACKED_POSTFIX HwdTxPwrDetFreqDataT;

typedef NV_PACKED_PREFIX struct
{
   HwdTxPwrDetFreqDataT   TxPwrDetFreqAdjCoor[HWD_NUM_FREQ_POINTS_TX_PWR];
} NV_PACKED_POSTFIX  HwdTxPwrDetFreqAdjT;    

/* Rx AGC calibration parameters */
typedef PACKED_PREFIX struct
{
  uint16                 RxAgcDigitalGainStates; /*Calibrationt point numbers in total*/
  int16                  RxRefDigitalGain; /*reference digital gain,  dB Q5*/
  int16                  RxCalibPointsdBm[HWD_LNA_MODE_NUM]; /*, Power at calibration points, dBm Q5*/
  int16                  RxGainStepDeltadB[HWD_LNA_MODE_NUM - 1];/*Step Gain dB Q5*/
} PACKED_POSTFIX  HwdRxDagcCalibDataT;   

/* Rx AGC frequency adjustment calibration parameters */
typedef NV_PACKED_PREFIX struct
{
 uint16 FreqChanNum;
 int16  RxFreqAdjDb[HWD_LNA_MODE_NUM];
} NV_PACKED_POSTFIX HwdRxMultiGainDataT;

typedef NV_PACKED_PREFIX struct
{
  HwdRxMultiGainDataT  RxFreqAdjCoordinate[HWD_NUM_FREQ_POINTS_RXAGC];
} NV_PACKED_POSTFIX  HwdRxMultiGainFreqAdjDataT;    

/* Rx AGC temperature adjustment calibration parameters */

typedef NV_PACKED_PREFIX struct
{
   int8 TempCelsius;     /* Temperature coordinate. Units: celsius */
   int16 AdjDb;           /* Tx gain adjustment coordinate. Units: dB with Q=IPC_DB_Q */
} NV_PACKED_POSTFIX  HwdGainTempAdjCoordinateT;    /* format matches HwdCoordinateT */

typedef NV_PACKED_PREFIX struct
{
   HwdGainTempAdjCoordinateT   RxTempAdjCoordinate[HWD_NUM_TEMP_POINTS_RXAGC];
} NV_PACKED_POSTFIX  HwdRxTempAdjDataT;   

/* AFC Temperature calibration parameter */
typedef PACKED_PREFIX struct
{
  int8   TempCelsius;
  uint16 TempComp;
} PACKED_POSTFIX HwdAfcTempCompParmT;

typedef PACKED_PREFIX struct
{
  HwdAfcTempCompParmT AfcTempComp[HWD_NUM_TEMP_CALIBR_POINTS];
} PACKED_POSTFIX HwdAfcTempCompTblT;

/* Tx power detect path couplerloss calibration parameters */
typedef NV_PACKED_PREFIX struct
{
  int16 TxDetCpl[HWD_PA_MODE_NUM]; /* Unit:1/32dB,Q5*/
} NV_PACKED_POSTFIX HwdTxDetCouplerLossTblT;

/* temperature and frequency compensation parameters */
typedef NV_PACKED_PREFIX struct
{
  int8   TempCelsius[HWD_NUM_TEMP_CALIBR_POINTS]; /* Unit: celsius degree */
  uint16 FreqChanNum[HWD_NUM_FREQ_CALIBR_POINTS];  /* Unit: channel number */
  int16  TempFreqComp[HWD_NUM_TEMP_CALIBR_POINTS][HWD_NUM_FREQ_CALIBR_POINTS]; /* Units: 1/32dB, Q5 */
} NV_PACKED_POSTFIX HwdTempFreqCompParmT;

/* TX PA Gain temperature and frequency compensation calibration parameters */
typedef NV_PACKED_PREFIX struct
{
  HwdTempFreqCompParmT TxPaGainComp[HWD_PA_MODE_NUM];
} NV_PACKED_POSTFIX HwdTxPaGainCompTblT;

/* DET coupler loss temperature and frequency compensation calibration parameters */
typedef NV_PACKED_PREFIX struct
{
  HwdTempFreqCompParmT TxDetCplComp[HWD_PA_MODE_NUM];
} NV_PACKED_POSTFIX HwdTxDetCouplerLossCompTblT;

/* RX path loss temperature and frequency compensation calibration parameters */
typedef NV_PACKED_PREFIX struct
{
  HwdTempFreqCompParmT RxPathLossComp[HWD_LNA_MODE_NUM];
} NV_PACKED_POSTFIX HwdRxPathLossCompTblT;

/* Sar tx power offset parameters table*/
typedef NV_PACKED_PREFIX struct
{
  uint16 FreqChanNum;  /* Unit: channel number */
  int16  PowerOffset[HWD_ANTENNA_MAX_NUM][HWD_RAT_MAX_NUM];  /* TX power offset, in unit of 1/32 */
} NV_PACKED_POSTFIX HwdSarTxPowerOffsetParmT;

typedef NV_PACKED_PREFIX struct
{
  HwdSarTxPowerOffsetParmT TxPowerOffsetParm[HWD_NUM_FREQ_CALIBR_POINTS];
} NV_PACKED_POSTFIX HwdSarTxPowerOffsetTblT;

typedef NV_PACKED_PREFIX struct
{
  HwdSarTxPowerOffsetTblT TxPowerOffsetTbl[HWD_NUM_SAR_TX_POWER_OFFSET_TABLE];
} NV_PACKED_POSTFIX HwdSarTxPowerOffsetT;

/* Tx power offset parameters table*/
typedef NV_PACKED_PREFIX struct
{
  uint16 FreqChanNum;  /* Unit: channel number */
  int16  PowerOffset;  /* TX power offset, in unit of 1/32 */
} NV_PACKED_POSTFIX HwdTxPowerOffsetParmT;

typedef NV_PACKED_PREFIX struct
{
  HwdTxPowerOffsetParmT TxPowerOffsetParm[HWD_NUM_FREQ_CALIBR_POINTS];
} NV_PACKED_POSTFIX HwdTxPowerOffsetTblT;

typedef NV_PACKED_PREFIX struct
{
  HwdTxPowerOffsetTblT TxPowerOffsetTbl[HWD_NUM_TX_POWER_OFFSET_TABLE];
} NV_PACKED_POSTFIX HwdTxPowerOffsetT;

#if (SYS_BOARD >= SB_JADE)
/** Data definition of custom 1xrtt L1d Rf Info */
typedef struct
{
    /* Access Tx Pwr Offset ,Unit is dBm*/
    uint16  accTxPwrOffset;
    /* Sch Max Tx Pwer Backoff,Unit is 1/2 dBm*/
    uint16  schTxPwrBackoff;
    /**TO DO ADD*/
} Hwd1xL1dRfCustT;
#endif

#ifdef __DYNAMIC_ANTENNA_TUNING__
typedef struct
{
  int16 featureIndex;
  int16 scenaryIndex;
}HwdRfApSensorRelativeIndex;
#endif

/*----------------------------------------------------------------------------
 Global Variables Declaration
----------------------------------------------------------------------------*/
/** Default and internal use custom data */
extern const HwdRfCustomDataT hwdRfCustomData;
extern HwdRfCustomDataT hwdRfCustDataInput;
#if (SYS_BOARD >= SB_JADE)
extern const HwdRfCustBpiMaskT hwdRfCustomBpiMask;
extern const HwdRfCustBpiDataT hwdRfCustomBpiData;
extern HwdRfCustBpiMaskT hwdRfCustBpiMaskInput;
extern HwdRfCustBpiDataT hwdRfCustBpiDataInput;
#endif

extern const HwdRfCustTasT hwdRfCustomTas;
extern HwdRfCustTasT hwdRfCustTasInput;

extern const uint8 dummyCalData;
extern const uint8 dummyCustData;
extern uint8 dummyCustDataInput;


/** Custom data items of calibration */
#if defined(MTK_PLT_RF_ORIONC) && (!defined(MTK_DEV_HW_SIM_RF))
extern HwdAfcCalibParmT            HwdDefaultAfcValue;

extern HwdTempCalibTableT          HwdDefaultTemperatureCalibData;
extern HwdTxPwrCalibDataT          HwdDefaultTxPwrCalibData_BandA;
extern HwdTxPwrCalibDataT          HwdDefaultTxPwrCalibData_BandB;
extern HwdTxPwrCalibDataT          HwdDefaultTxPwrCalibData_BandC;

extern HwdTxMultiGainFreqAdjDataT  HwdDefaultTxAgcFreqAdjCalibData_BandA;
extern HwdTxMultiGainFreqAdjDataT  HwdDefaultTxAgcFreqAdjCalibData_BandB;
extern HwdTxMultiGainFreqAdjDataT  HwdDefaultTxAgcFreqAdjCalibData_BandC;

extern HwdTxMultiGainTempAdjDataT  HwdDefaultTxAgcTempAdjCalibData_BandA;
extern HwdTxMultiGainTempAdjDataT  HwdDefaultTxAgcTempAdjCalibData_BandB;
extern HwdTxMultiGainTempAdjDataT  HwdDefaultTxAgcTempAdjCalibData_BandC;

/* HwdHwdDefaultTxPdetCalibData_BandA/B/C */
extern HwdTxPwrDetCalibDataT       HwdDefaultTxPdetCalibData_BandA;
extern HwdTxPwrDetCalibDataT       HwdDefaultTxPdetCalibData_BandB;
extern HwdTxPwrDetCalibDataT       HwdDefaultTxPdetCalibData_BandC;
/* HwdDefaultTxPdetFreqAdjCalibData_BandA/B/C */
extern HwdTxPwrDetFreqAdjT         HwdDefaultTxPdetFreqAdjCalibData_BandA;
extern HwdTxPwrDetFreqAdjT         HwdDefaultTxPdetFreqAdjCalibData_BandB;
extern HwdTxPwrDetFreqAdjT         HwdDefaultTxPdetFreqAdjCalibData_BandC;
extern HwdRxDagcCalibDataT         HwdDefaultMainRxDagcCalibData_BandA;
extern HwdRxDagcCalibDataT         HwdDefaultMainRxDagcCalibData_BandB;
extern HwdRxDagcCalibDataT         HwdDefaultMainRxDagcCalibData_BandC;
                                   
extern HwdRxDagcCalibDataT         HwdDefaultDivRxDagcCalibData_BandA;
extern HwdRxDagcCalibDataT         HwdDefaultDivRxDagcCalibData_BandB;
extern HwdRxDagcCalibDataT         HwdDefaultDivRxDagcCalibData_BandC;
extern HwdRxMultiGainFreqAdjDataT  HwdDefaultMainRxAgcFreqAdjCalibData_BandA;
extern HwdRxMultiGainFreqAdjDataT  HwdDefaultMainRxAgcFreqAdjCalibData_BandB;
extern HwdRxMultiGainFreqAdjDataT  HwdDefaultMainRxAgcFreqAdjCalibData_BandC;


extern HwdRxMultiGainFreqAdjDataT  HwdDefaultDivRxAgcFreqAdjCalibData_BandA;
extern HwdRxMultiGainFreqAdjDataT  HwdDefaultDivRxAgcFreqAdjCalibData_BandB;
extern HwdRxMultiGainFreqAdjDataT  HwdDefaultDivRxAgcFreqAdjCalibData_BandC;


extern HwdRxTempAdjDataT           HwdDefaultMainRxAgcTempAdjCalibData_BandA;
extern HwdRxTempAdjDataT           HwdDefaultMainRxAgcTempAdjCalibData_BandB;
extern HwdRxTempAdjDataT           HwdDefaultMainRxAgcTempAdjCalibData_BandC;
                                   
                                   
extern HwdRxTempAdjDataT           HwdDefaultDivRxAgcTempAdjCalibData_BandA;
extern HwdRxTempAdjDataT           HwdDefaultDivRxAgcTempAdjCalibData_BandB;
extern HwdRxTempAdjDataT           HwdDefaultDivRxAgcTempAdjCalibData_BandC;
#else
#include "hwdnvcustdefs.h"
HWD_RF_CUST_CUSTOM_DATA_DECLARE(SetDefault);
HWD_RF_CAL_CUSTOM_DATA_DECLARE(SetDefault);

extern void* HwdRfCustCustomDataSetDefaultPtr[];
extern uint32 HwdRfCustCustomDataSetDefaultSize[];
extern void* HwdRfCalCustomDataSetDefaultPtr[];
extern uint32 HwdRfCalCustomDataSetDefaultSize[];
extern void* HwdMipiCustomDataSetDefaultPtr[];
extern uint32 HwdMipiCustomDataSetDefaultSize[];
#endif

/*----------------------------------------------------------------------------
 Global Variables Declaration
----------------------------------------------------------------------------*/
extern bool HwdRfCustDataChecked(void);
extern void HwdRfCustDataProcess(void *MsgDataPtr);
#if !defined(MTK_PLT_RF_ORIONC) || defined(MTK_DEV_HW_SIM_RF)
extern void HwdRfCustBpiMaskProcess(void *MsgDataPtr);
extern void HwdRfCustBpiDataProcess(void *MsgDataPtr);
#endif
extern bool HwdRfCustRxDiversityEnable(void);
extern bool HwdRfCustRxDiversityOnlyTestEnable(void);
extern bool HwdRfCustPmuPaVddEnable(void);
extern bool HwdRfCustBattPaVddEnable(void);
extern bool HwdRfCustDc2DcPaVddEnable(void);
#ifdef __SAR_TX_POWER_BACKOFF_SUPPORT__
extern bool HwdRfCustTpoEnable(void);
extern bool HwdRfCustTpoMetaEnable(void);
#endif
extern bool HwdRfCustTempMeasEnable(void);
extern bool HwdRfCustCheckBandValid(HwdRfBandT RfBand);
extern SysCdmaBandT HwdRfCustBandConvert(HwdRfBandT RfBand);
extern void HwdRfCustLastProcess(void *MsgDataPtr);
extern uint8 *HwdRfCustGetMainRxPort(void);
extern uint8 *HwdRfCustGetDivRxPort(void);
extern uint8 *HwdRfCustGetTxPort(void);
#if !defined(MTK_PLT_RF_ORIONC) || defined(MTK_DEV_HW_SIM_RF)
extern uint8 *HwdRfCustGetTxDetPort(void);
#endif
extern const HwdRfCustomDataT *HwdRfCustGetDefaultData(void);
extern HwdRfCustomDataT *HwdRfCustGetNvramData(void);
extern bool HwdRfCustTasChecked(void);
extern void HwdRfCustTasProcess(void *MsgDataPtr);
extern bool HwdRfTasIsEnable(void);
#if defined (IS_C2K_ET_FEATURE_SUPPORT) 
extern bool HwdRfCustEtmPaVddEnable(void);
#endif
#ifdef SYS_OPTION_HPUE_ENABLED
extern bool HwdRfCustHpuePaVaddEnable(void);
#endif


extern bool HwdRfTasIsForceEnable(void);
#ifdef __DYNAMIC_ANTENNA_TUNING__
extern bool HwdRfDatIsEnable(int16 featureIndex,int16 scenaryIndex);
extern void HwdRfGetRelativeIndex(void);
#endif
#if defined(__SAR_TX_POWER_BACKOFF_SUPPORT__)
extern bool HwdRfSarIsEnable(int16 featureIndex,int16 scenaryIndex,uint8 InterfaceType);
#endif
#if defined(__TX_POWER_OFFSET_SUPPORT__)||defined(__SAR_TX_POWER_BACKOFF_SUPPORT__)
extern void HwdRfCustDataCheckSet(DbmRfCustSegmentT seg);
#endif


/*****************************************************************************
* End of File
*****************************************************************************/
#endif  /* _HWDRFPUBLIC_H */

