/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*******************************************************************************
 *
 * Filename:
 * ---------
 * hwdtascustnv.c
 *
 * Project:
 * --------
 * C2K
 *
 * Description:
 * ------------
 * Implementation file pertaining
 * to the RF infrastructure.
 *
 * Author:
 * -------
 * 
 *
 *==============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 *
 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *==============================================================================
 *******************************************************************************/
#include "hwdtaspublic.h"
/** TAS control parameters */
const HwdRfTasTblEtyT HwdRfTasTbl[] =
{
    /* tasBMMask,    tasBMData,      initIndex,     tasSwitchEn */
    {0x10,  0x10,   TAS_IDX_FORCE,      FALSE},
    {0x1D,  0x04,   TAS_IDX_DEFAULT,    FALSE},
    {0x1D,  0x05,   TAS_IDX_DEFAULT,    TRUE},
#ifndef __TAS_ANTENNA_IDX_ON_TEST_SIM__
    {0x1F,  0x0C,   TAS_IDX_DEFAULT,       FALSE},
    {0x1F,  0x0D,   TAS_IDX_DEFAULT,       FALSE},
    {0x1F,  0x0E,   TAS_IDX_DEFAULT,       FALSE},
#else
    {0x1F,  0x0C,   TAS_IDX_DEFAULT_ON_TEST_SIM,       FALSE},
    {0x1F,  0x0D,   TAS_IDX_DEFAULT_ON_TEST_SIM,       FALSE},
    {0x1F,  0x0E,   TAS_IDX_DEFAULT_ON_TEST_SIM,       FALSE},
#endif
{0x1F,  0x0F,   TAS_IDX_DEFAULT,    TRUE},
#ifdef __TAS_OFF_WHEN_NO_SIM__
#ifdef __TAS_ANTENNA_IDX_ON_TEST_SIM__
    {0x14,  0x00,   TAS_IDX_DEFAULT_ON_TEST_SIM,       FALSE},
#else
    {0x14,  0x00,   TAS_IDX_DEFAULT,       FALSE},
#endif
#else
    {0x1D,  0x00,   TAS_IDX_DEFAULT,       FALSE},
    {0x1D,  0x01,   TAS_IDX_DEFAULT,    TRUE},
#endif  
};
const uint16 HwdRfTasTblSize = sizeof(HwdRfTasTbl) / sizeof(HwdRfTasTblEtyT);
