/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef _HWDCAL_H_
#define _HWDCAL_H_
/*****************************************************************************
* 
* FILE NAME   : hwdcal.h
*
* DESCRIPTION : Hardware calbration data control
*
* HISTORY     :
*     See Log at end of file
*
*****************************************************************************/

/*----------------------------------------------------------------------------
 Include Files
----------------------------------------------------------------------------*/
#include "ipcapi.h"
#include "hwdapi.h"
#include "do_rmcapi.h"
#if defined(MTK_CBP) && !defined(MTK_PLT_ON_PC)
#include "hwdrfpublic.h"
#include "dbmapi.h"
#endif

/*----------------------------------------------------------------------------
* This define should indicate the number of unique band classes supported by 
* the RF driver
----------------------------------------------------------------------------*/
#if defined(MTK_CBP) && !defined(MTK_PLT_ON_PC)
#define HWD_NUM_SUPPORTED_BAND_CLASS (3)
#else /* MTK_CBP */
#if (SYS_OPTION_RF_HW == SYS_RF_FCI_7790)
#define HWD_NUM_SUPPORTED_BAND_CLASS 4	  /* AWS and BC10*/
#elif (SYS_OPTION_RF_HW == SYS_RF_MTK_ORIONC)
#define HWD_NUM_SUPPORTED_BAND_CLASS 3
#else
#define HWD_NUM_SUPPORTED_BAND_CLASS 2   /* use 2 to be compatible with other projects */
#endif
#endif /* MTK_CBP */

/*----------------------------------------------------------------------------
 Global Defines and Macros
----------------------------------------------------------------------------*/

#define HWD_CAL_TXAGC_DYNAMIC_THRESHOLDS

/* Following defines used for Tx power limits */
#define POWER_MAX_BAND_A_DB_Q      0x05E0  /* 23.5dBm in Q6 */
#define POWER_MAX_BAND_B_DB_Q      0x05E0  /* 23.5dBm in Q6 */
#define POWER_MAX_BAND_C_DB_Q      0x05E0  /* 23.5dBm in Q6 */
#define POWER_MAX_BAND_D_DB_Q      0x05E0  /* 23.5dBm in Q6 */
#define POWER_MAX_BAND_E_DB_Q      0x05E0  /* 23.5dBm in Q6 */

#define POWER_MIN_BAND_A_DB_Q      -3520  /* -55dBm in Q6 */
#define POWER_MIN_BAND_B_DB_Q      -3520  /* -55dBm in Q6 */
#define POWER_MIN_BAND_C_DB_Q      -3520  /* -55dBm in Q6 */
#define POWER_MIN_BAND_D_DB_Q      -3520  /* -55dBm in Q6 */
#define POWER_MIN_BAND_E_DB_Q      -3520  /* -55dBm in Q6 */

/* PDM DAC value defines */
#define AFC_DAC_MAX_VAL ( (1 << 16) -1)  /* maximum value for 16 bit unsigned AFC DAC */
#define AFC_DAC_MIN_VAL 0                /* minimum value for 16 bit unsigned AFC DAC */

/* Q-factor of Rx AGC PDM corrections (specified by DSPM-RFC) */
#define HWD_RFC_PDM_CORRECTION_Q  3

/* Following defines use to determine Q-precision for various calibration applications */
#define PDM_TO_DB_SLOPE_Q  8

#define HWD_TRANS_FUNC_SLOPE_Q       8  /* Q-precision applied to all transfer function slopes that
                                         * don't have a specifically associated Q.
                                         */

#define HWD_TRANS_TEMP_FUNC_SLOPE_Q  14  /* Q-precision applied to temperature transfer function slopes 
                                         * Select so that a resolution of 1/4C is available over all of
                                         * of the 12-bit AUX ADC range (0x0000 to 0x0FFF)
                                         */

#define HWD_TRANS_BATT_FUNC_SLOPE_Q  8  /* Q-precision applied to battery transfer function slopes 
                                         */

#define HWD_TRANS_FUNC_SLOPE_GAIN_Q  7  /* Q-precision applied to all dB to step transfer function slopes.
                                         * Selected so that a maximum slope of 2^(15-7)=128 step/dB
                                         * is possible (this value should never be exceeded in practice)
                                         * and accuracy of 1/4 dB (or whatever X-axis is) is maintained 
                                         * over a 2^(8-2)=64dB segment.
                                         */

#define HWD_TRANS_FUNC_SLOPE_FREQ_Q  14 /* Q-precision applied to all freq to dB transfer function slopes.
                                         * Selected so that a resolution of 1/8dB is available over
                                         * 2048 frequency channels.
                                         */

#define HWD_TRANS_FUNC_SLOPE_TEMP_Q  11 /* Q-precision applied to all temp (C) to dB transfer function slopes.
                                         * Selected so that a resolution of 1/16dB is available over
                                         * 128 degC.
                                         */

#define HWD_TRANS_FUNC_SLOPE_BATT_Q  14  /* Q-precision applied to battery voltage (mV) to dB transfer function slopes.
                                         * Selected so that a resolution of 1/dB is available over
                                         * 4096 mV.
                                         */

#define MIN_TEMP_CHANGE_CELSIUS  2      /* minimum temperature change below which a new temperature
                                         * calibration adjustment is not generated, to save 
                                         * unnecessarily recalculating all the time.
                                         * Units of celsius.
                                         */
#define MIN_BATT_VOLTAGE_CHANGE  50     /* minimum battery voltage change below which a new battery voltage
                                         * calibration adjustment is not generated, to save 
                                         * unnecessarily recalculating all the time.
                                         * Units of mV.
                                         */
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
#define MAX_TEMP_CELSIUS (int16) 100     /* Maximum possible temperature for AGC adjustment */
#define MIN_TEMP_CELSIUS (int16)-40     /* Minimum possible temperature for AGC adjustment */
#define MAX_BATT_VOLTAGE (int16)5000    /* Maximum possible battery voltage (mV) for AGC adjustment */
#define MIN_BATT_VOLTAGE (int16)1000    /* Minimum possible battery voltage (mV) for AGC adjustment */
#else
#define MAX_TEMP_CELSIUS (int16) 100     /* Maximum possible temperature for AGC adjustment */
#define MIN_TEMP_CELSIUS (int16)-20     /* Minimum possible temperature for AGC adjustment */
#endif


#define SMALLEST_INT16_VALUE  ((int16)(-1 << 15)) /* smallest possible value that 
                                                * can be stored in int16. The left boundary of the 
                                                * leftmost region is moved to here to ensure there 
                                                * can be no overflow in searching for regions.
                                                */
#define LARGEST_INT16_VALUE  ((int16) ((1 << 15)-1)) /* largest possible value 
                                                   * that can be stored in int16. The left 
                                                   * boundary of the rightmost (extra) region 
                                                   * is moved to here to ensure there can be no 
                                                   * overflow in searching for regions.
                                                   */

#define RESET_TEMP_CALC ((int16)(-1 << 15))    /* define for argument to HwdCalibTempUpdate() that
                                                * resets it, since it only recalculates if the 
                                                * temperature has changed.
                                                */
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
#define RESET_BATT_CALC ((int16)(-1 << 15))    /* define for argument to HwdCalibBatteryUpdate() that
                                                * resets it, since it only recalculates if the 
                                                * temperature has changed.
                                                */
#endif

#define ShiftRightSigned(Shiftee, Shift)                                    \
              (((Shift) > 0) ? ((Shiftee) >> (Shift)) : ((Shiftee) << (-(Shift))))

/* --- define hysteresis state change thresholds --- */
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
#define RX_HYST_UPPER_THRESH_1_DB  ((int16) (-60 * (1 << IPC_DB_Q)))
                                                /* RxAgc hysterisis upper boundary in IPC_DB_Q */


#define RX_HYST_LOW_THRESH_1_DB    ((int16) (-76 * (1 << IPC_DB_Q)))
                                                 /* RxAgc hysterisis low boundary in IPC_DB_Q */

#define RX_HYST_UPPER_THRESH_2_DB  ((int16) (-80 * (1 << IPC_DB_Q)))
                                                /* RxAgc hysterisis upper boundary in IPC_DB_Q */


#define RX_HYST_LOW_THRESH_2_DB    ((int16) (-96 * (1 << IPC_DB_Q)))
                                                 /* RxAgc hysterisis low boundary in IPC_DB_Q */

#define RX_HYST_DELAY_THRESH_DB    ((int16) (-93 * (1 << IPC_DB_Q)))
                                    /* RxAgc hysterisis delay boundary in IPC_DB_Q */

#define RX_HYST_DELAY_COUNT         8*16        /* num PCGs required to be above hysterisis delay 
                                     * threshold before allowing a switch to next lower
                                     * hysterisis gain state
                                     */
#define TX_HYST_UPPER_THRESH_1_DB  ((int16) (0 * (1 << IPC_DB_Q)))
                                                /* TxAgc hysterisis upper boundary in IPC_DB_Q
                                                 * for the first hyst state
                                                 */


#define TX_HYST_LOW_THRESH_1_DB    ((int16) (-15 * (1 << IPC_DB_Q)))
                                                /* TxAgc hysterisis low boundary in IPC_DB_Q
                                                 * for the first hyst state
                                                 */

#define TX_HYST_UPPER_THRESH_2_DB  ((int16) (-20 * (1 << IPC_DB_Q)))
                                                /* TxAgc hysterisis upper boundary in IPC_DB_Q
                                                 * for the second hyst state
                                                 */


#define TX_HYST_LOW_THRESH_2_DB    ((int16) (-35 * (1 << IPC_DB_Q)))
                                                /* TxAgc hysterisis low boundary in IPC_DB_Q
                                                 * for the second hyst state
                                                 */
                                                
#define HWD_TRANS_FUNC_SLOPE_TXPWR_Q 14 /* Q-precision applied to all AUX ADC step to dB transfer function slopes.
                                         * Select so that a resolution of 1/4dB is available over all of
                                         * of the 12-bit AUX ADC range (0x0000 to 0x0FFF)
                                         */
#endif

#define HWD_ONE_LNA_GAIN_STATES   1
#define HWD_TWO_LNA_GAIN_STATES   2
#define HWD_THREE_LNA_GAIN_STATES 3

#define HWD_ONE_PA_GAIN_STATES    1
#define HWD_TWO_PA_GAIN_STATES    2
#define HWD_THREE_PA_GAIN_STATES  3

#define HWD_TX_PWR_CORR_DECAY     4     /* shift value for the decay of the correction */

#define HWD_CAL_POWER_DET_MEAS_LIMIT  (30 << IPC_DB_Q)   /* 30dB */

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
/* Reference points for auto battery calibration */
#define BATTERY_VOLTAGE_REF_1    1499  /* mV */
#define BATTERY_VOLTAGE_REF_2    1785  /* mV */
#define HWD_NUM_INL_POINTS       33
#define HWD_NUM_INL_SEGMENTS     HWD_NUM_INL_POINTS-1
#endif
/* Total number of Digital Gain bit combinations */
#define HWD_RX_DAGC_DIGIGAIN_SIZE  128
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
/* IP2 Calibration channel groups */
#define  IP2_CAL_CHNL_GROUP0     80    /* channels 0   - 80   */
#define  IP2_CAL_CHNL_GROUP1     440   /* channels 81  - 440  */
#define  IP2_CAL_CHNL_GROUP2     680   /* channels 441 - 680  */
#define  IP2_CAL_CHNL_GROUP3     990   /* channels 681 - 990  */
#define  IP2_CAL_CHNL_GROUP4     1199  /* channels 991 - 1199 */
#endif
/* Define Q-value used by CBP7x gain registers */
#define HWD_RMC_ALOG2_GAIN_Q   8

/* Define Q-precision applied to all ALog2 to step transfer function slopes. 
** Value is chosen to optimize range of ALog2 gain across a (64dB * 1/6) region 
** and to avoid overrun conditions when processing ALog2 coordinate pairs */
#define HWD_TRANS_FUNC_ALOG2_SLOPE_GAIN_Q  5  

/* Define Q-precision applied to APT SPDM correction only */
#define HWD_TRANS_FUNC_ALOG2_APT_SLOPE_GAIN_Q  4 

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
#define MAX_RSSI_CORRECTIONS  13
#define RSSI_P0_IDX    0
#define RSSI_P12_IDX   12
#else
#if (SYS_BOARD >= SB_JADE)
#define NUM_RSSI_CORRECTIONS  14
#else
#define NUM_RSSI_CORRECTIONS  16
#endif
#endif

#define HWD_CAL_RESTORE_TXAGC_THRESHOLDS   (-0x7fff)
#define HWD_CAL_TXAGC_ALT_THRES1_LOW       (-18 << IPC_DB_Q)
#define HWD_CAL_TXAGC_ALT_THRES1_HIGH      (-14 << IPC_DB_Q)
#define HWD_CAL_TXAGC_ALT_THRES2_LOW       (-8 << IPC_DB_Q)
#define HWD_CAL_TXAGC_ALT_THRES2_HIGH      (-4 << IPC_DB_Q)

/*----------------------------------------------------------------------------
 Global Typedefs
----------------------------------------------------------------------------*/
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
typedef struct
{
   uint16 RealValue;
   uint16 IdealValue;
}HwdInlTableT;
#endif

typedef PACKED_PREFIX struct
{
   int8  DigiBitSel;
   int8  DigiGain;
} PACKED_POSTFIX  HwdRxDagcRefBitsT;

/* Digital Rx AGC power calibration (baseline, without temp and freq channel adjustment) */
typedef PACKED_PREFIX struct
{
   int16 LowSwitchThresh;      /* Low Switch Threshold, Units: dB with Q=IPC_DB_Q */
   int16 HighSwitchThresh;     /* High Switch Threshold, Units: dB with Q=IPC_DB_Q */
} PACKED_POSTFIX  HwdRxDagcSwitchThreshT;

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
/** This definition is refer to MTK 3G/4G solution, in RFNVRAM table this value can be updated by customer.
*   Example:
*
*    -----                   TX_POWER_MAX       ----- 24dBm
*      |
*      |
*    H |
*      |    -----            HighSwitchThresh   ----- 14dBm  -> Start
*      |      |
*    -----    |              LowSwitchThresh    ----- 10dBm  -> End
*             |
*           M |
*             |    -----     HighSwitchThresh   ----- 4dBm   -> Start
*             |      |
*           -----    |       LowSwitchThresh    ----- 0dBm   -> End
*                    |  
*                  L |  
*                    |  
*                  -----     TX_POWER_MIN
*
*/
typedef PACKED_PREFIX struct
{
 int16 LowSwitchThresh;
 int16 HighSwitchThresh;
} PACKED_POSTFIX HwdTxSwitchThreshT;

#endif

/* Hardware calibration values to be read from the EEPROMs */
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
typedef struct
{
   int16  AfcHwvInterc;       /* AFC DAC intercept. This is the default DAC value required 
                                  * to set the VCO to the correct frequency (0 PPM deviation).
                                  */
   int16  AfcSlopeStepPerPpm; /* slope to convert PPM to HW val, with Q = HWD_TRANS_FUNC_SLOPE_Q */

   uint16 RxAgcLNAGainStatesMain[HWD_NUM_SUPPORTED_BAND_CLASS];
   uint16 RxAgcLNAGainStatesDiv[HWD_NUM_SUPPORTED_BAND_CLASS];
   uint16 RxAgcLNAGainStatesSec[HWD_NUM_SUPPORTED_BAND_CLASS];

   uint16 TxAgcPAGainStatesMain[HWD_NUM_SUPPORTED_BAND_CLASS];
   uint16 TxAgcPAGainStatesAux[HWD_NUM_SUPPORTED_BAND_CLASS];

   int16  TxAgcHystHighThresh1DbmMain[HWD_NUM_SUPPORTED_BAND_CLASS]; 
   int16  TxAgcHystHighThresh1DbmAux[HWD_NUM_SUPPORTED_BAND_CLASS]; 
                                   /* threshold at which to switch the Tx AGC first hyst state to high gain,
                                    * Tx power in dBm with Q = IPC_DB_Q
                                    */
   int16  TxAgcHystLowThresh1DbmMain[HWD_NUM_SUPPORTED_BAND_CLASS];  
   int16  TxAgcHystLowThresh1DbmAux[HWD_NUM_SUPPORTED_BAND_CLASS];  
                                   /* threshold at which to switch the Tx AGC first hyst state to low gain,
                                    * Tx power in dBm with Q = IPC_DB_Q
                                    */
   int16  TxAgcHystHighThresh2DbmMain[HWD_NUM_SUPPORTED_BAND_CLASS]; 
   int16  TxAgcHystHighThresh2DbmAux[HWD_NUM_SUPPORTED_BAND_CLASS]; 
                                   /* threshold at which to switch the Tx AGC second hyst state to high gain,
                                    * Tx power in dBm with Q = IPC_DB_Q
                                    */
   int16  TxAgcHystLowThresh2DbmMain[HWD_NUM_SUPPORTED_BAND_CLASS];  
   int16  TxAgcHystLowThresh2DbmAux[HWD_NUM_SUPPORTED_BAND_CLASS];  
                                   /* threshold at which to switch the Tx AGC second hyst state to low gain,
                                    * Tx power in dBm with Q = IPC_DB_Q    */

   int16  TxAgcAltHighThresh1DbmMain[HWD_NUM_SUPPORTED_BAND_CLASS]; 
   int16  TxAgcAltHighThresh1DbmAux[HWD_NUM_SUPPORTED_BAND_CLASS]; 
                                   /* threshold at which to switch the Tx AGC first hyst state to high gain,
                                    * Tx power in dBm with Q = IPC_DB_Q
                                    */
   int16  TxAgcAltLowThresh1DbmMain[HWD_NUM_SUPPORTED_BAND_CLASS];  
   int16  TxAgcAltLowThresh1DbmAux[HWD_NUM_SUPPORTED_BAND_CLASS];  
                                   /* threshold at which to switch the Tx AGC first hyst state to low gain,
                                    * Tx power in dBm with Q = IPC_DB_Q
                                    */
   int16  TxAgcAltHighThresh2DbmMain[HWD_NUM_SUPPORTED_BAND_CLASS]; 
   int16  TxAgcAltHighThresh2DbmAux[HWD_NUM_SUPPORTED_BAND_CLASS]; 
                                   /* threshold at which to switch the Tx AGC second hyst state to high gain,
                                    * Tx power in dBm with Q = IPC_DB_Q
                                    */
   int16  TxAgcAltLowThresh2DbmMain[HWD_NUM_SUPPORTED_BAND_CLASS];  
   int16  TxAgcAltLowThresh2DbmAux[HWD_NUM_SUPPORTED_BAND_CLASS];  
                                   /* threshold at which to switch the Tx AGC second hyst state to low gain,
                                    * Tx power in dBm with Q = IPC_DB_Q    */

   int16  TxDcBiasIOffsetMain;     /* Tx I ADC dc bias power up offset value */
   int16  TxDcBiasQOffsetMain;     /* Tx Q ADC dc bias power up offset value */

   int16  TxDcBiasIOffsetAux;      /* Tx I ADC dc bias power up offset value */
   int16  TxDcBiasQOffsetAux;      /* Tx Q ADC dc bias power up offset value */
} HwdCalibrParmT;
#elif defined(MTK_PLT_RF_ORIONC) && (!defined(MTK_DEV_HW_SIM_RF))
typedef struct
{
/*For AFC Calibratoin Data*/
   uint16 InitDacValue; /*AFC DAC value*/
   uint16 AfcSlopeInv;/*SLope*/
   uint32 CapId;/*CapId*/
/* Rx Part */
   uint8 RxAgcLNAGainStatesMain[HWD_NUM_SUPPORTED_BAND_CLASS]; /*Calibration point number for rx*/
   uint8 RxAgcLNAGainStatesDiv[HWD_NUM_SUPPORTED_BAND_CLASS];

   uint8 RxRefGainStateMain[HWD_NUM_SUPPORTED_BAND_CLASS];/*reference Rx gain states after interporlation */
   uint8 RxRefGainStateDiv[HWD_NUM_SUPPORTED_BAND_CLASS];

   int16 RxRefLeveldBmMain[HWD_NUM_SUPPORTED_BAND_CLASS];/*refernce Rx Power, dBm Q5*/
   int16 RxRefLeveldBmDiv[HWD_NUM_SUPPORTED_BAND_CLASS];

   int16 RxRefDigitalGainMain[HWD_NUM_SUPPORTED_BAND_CLASS]; /*reference digital gain, dB Q5 */
   int16 RxRefDigitalGainDiv[HWD_NUM_SUPPORTED_BAND_CLASS];

/*Tx Part*/
   uint16 TxAgcPAGainStatesMain[HWD_NUM_SUPPORTED_BAND_CLASS]; /*PA mode Number*/
   uint8  PaCalSectionMum[HWD_NUM_SUPPORTED_BAND_CLASS];/*calibration point number for tx*/
   HwdTxPaHystDataT TxHyst[HWD_NUM_SUPPORTED_BAND_CLASS][HWD_PA_MODE_NUM - 1]; /*dB Q5*/                  

   int16 paPhaseJumpComp[HWD_NUM_SUPPORTED_BAND_CLASS][HWD_PA_MODE_NUM];

} HwdCalibrParmT;

#else
typedef struct
{
   /*AFC calibration data*/
   uint16 InitDacValue; /*AFC DAC value*/
   int16 AfcSlopeInv;/*SLope*/
   uint32 CapId;/*CapId*/

   HwdAfcCompCalibParmT         AfcScurveParam;

   /* Rx calibration data */
   HwdRxPathLossCompTblT        RxPathLossCompTblHpm[HWD_RF_MAX_NUM_OF_RX][HWD_RF_BAND_MAX];
   HwdRxPathLossCompTblT        RxPathLossCompTblLpm[HWD_RF_MAX_NUM_OF_RX][HWD_RF_BAND_MAX];

   /*Tx calibration data*/
   uint16                       TxAgcPAGainStatesMain[HWD_RF_BAND_MAX]; /*PA mode Number*/
   uint8                        PaCalSectionMum[HWD_RF_BAND_MAX];/*calibration point number for tx*/
   HwdRfPaContextT              TxPaContext[HWD_RF_BAND_MAX][HWD_NUM_GAIN_POINTS_TXAGC];
   HwdTxPaHystDataT             TxHyst[HWD_RF_BAND_MAX][HWD_PA_MODE_NUM - 1]; /*dB Q5*/                  
   HwdTxPaGainCompTblT          TxPaGainCompTbl1xRtt[HWD_RF_BAND_MAX];
   HwdTxPaGainCompTblT          TxPaGainCompTblEvdo[HWD_RF_BAND_MAX];
   int16                        paPhaseJumpComp[HWD_RF_BAND_MAX][HWD_PA_MODE_NUM];

   /*Tx Det calibration data*/
   HwdTxDetCouplerLossTblT      TxDetCplTbl[HWD_RF_BAND_MAX];
   HwdTxDetCouplerLossCompTblT  TxDetCplCompTbl1xRtt[HWD_RF_BAND_MAX];
   HwdTxDetCouplerLossCompTblT  TxDetCplCompTblEvdo[HWD_RF_BAND_MAX];

   /* AGPS Group Delay */
   int16                        AGpsGrpDly[HWD_RF_BAND_MAX];

   /* TX power back off */
   HwdTxPwrBackOffTblT          TxPwrBackOffRegion[HWD_RF_BAND_MAX];
   /* Sar tx power offset*/
#ifdef __SAR_TX_POWER_BACKOFF_SUPPORT__
   HwdSarTxPowerOffsetT            SarTxPowerOffset[HWD_RF_BAND_MAX];
#endif
   /* Tx power offset*/
#ifdef __TX_POWER_OFFSET_SUPPORT__
   HwdTxPowerOffsetT            TxPowerOffset[HWD_RF_BAND_MAX];
#endif
} HwdCalibrParmT;
#endif

/* Calibration Messages */
/* generic XY coordinated type */
typedef PACKED_PREFIX struct
{
   int16 X;     /* horizontal (input) coordinate */
   int16 Y;     /* vertical (output) coordionate */
} PACKED_POSTFIX  HwdCoordinateT;

/* generic line segment type */
typedef PACKED_PREFIX struct
{
   int16 BoundaryLeft;    /* left boundary of segment */
   int16 Slope;           /* slope of segment */
   int16 Intercept;       /* y-intercept of segment */
} PACKED_POSTFIX  HwdLineSegmentT;

/* AFC calibration parameters -    HWD_CAL_AFC_PARMS_MSG */
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
typedef PACKED_PREFIX struct
{
   int16    AfcHwvInterc;           /* AFC DAC intercept. 
                                     * This is the default DAC value required 
                                     * to set the VCO to the correct frequency (0 PPB deviation).
                                     */
   int16    AfcSlopeStepPerPpm;    /* slope to convert PPM to HW val, with Q = HWD_TRANS_FUNC_SLOPE_Q */
} PACKED_POSTFIX  HwdAfcCalibParmT;    /* typedef for the message */

/* HWD_CAL_TEMPERATURE_PARMS_MSG calibration message */
typedef PACKED_PREFIX struct
{
    int8    TempCelsius;
    uint16  AuxAdcValue;
} PACKED_POSTFIX  HwdTempCalibrPointT;
#endif
typedef PACKED_PREFIX struct
{
    HwdTempCalibrPointT TempCalibrTable[HWD_NUM_TEMP_CALIBR_POINTS];
} PACKED_POSTFIX  HwdTempCalibrationMsgT;

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
typedef PACKED_PREFIX struct
{
    int16  TempOffset;
} PACKED_POSTFIX  HwdTempOffsetCalMsgT;
#endif

/* HWD_CAL_TEMP_OFFSET_PARMS_MSG calibration message */
typedef PACKED_PREFIX struct
{
   int16    AuxAdcOffset;    /* offset to basic temperature curve */
} PACKED_POSTFIX  HwdTempOffsetCalibParmT;    /* typedef for the message */

/* HWD_CAL_BATTERY_PARMS_MSG calibration message */
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
typedef PACKED_PREFIX struct
{
    int16   BattVoltage; /* mV */
    uint16  AuxAdcValue;
} PACKED_POSTFIX  HwdBattCalibrPointT;

typedef PACKED_PREFIX struct
{
    HwdBattCalibrPointT BattCalibrTable[HWD_NUM_BATT_CALIBR_POINTS];
} PACKED_POSTFIX  HwdBattCalibrationMsgT;

/* HWD_CAL_BATT_PDM_PARMS_MSG calibration message */
typedef PACKED_PREFIX struct
{
    uint16  PdmValue;
    int16   BattVoltage; /* mV */
} PACKED_POSTFIX  HwdBattPdmCoordinateT;

typedef PACKED_PREFIX struct
{
   HwdBattPdmCoordinateT PdmBattCharger[HWD_NUM_BATT_PDM_CALIBR_POINTS];
} PACKED_POSTFIX  HwdBatteryPdmCalibDataT;           /* typedef for the message */

/* HWD_CAL_BANDGAP_PARMS_MSG */
typedef PACKED_PREFIX struct
{
   uint8 CoarseTrimBits;      /* Bandgap trim setting for cf11[3:1] */
   uint8 FineTrimBits;        /* Bandgap trim setting for cf12[6:4] */
} PACKED_POSTFIX  HwdBandgapCalibDataT;       /* typedef for the message */    

/* TX/RX AGC calibration messages */
typedef PACKED_PREFIX struct
{
   uint16                TxAgcPAGainStates;       /* 2 or 3 PA Hysteresis Gain states */
   int16                 TxAgcHystHighThresh1Dbm; /* Threshold at which to switch from first hyst state
                                                   * to second state - Tx power in dBm, Q = IPC_DB_Q */   
   int16                 TxAgcHystLowThresh1Dbm;  /* Threshold at which to switch from second hyst state
                                                   * to first state - Tx Power in dBm, Q = IPC_DB_Q */ 
   int16                 TxAgcHystHighThresh2Dbm; /* Threshold at which to switch from second hyst state
                                                   * to third state - Tx Power in dBm, Q = IPC_DB_Q */
   int16                 TxAgcHystLowThresh2Dbm;  /* Threshold at which to switch from thried hyst state
                                                   * to second state - Tx Power in dBm, Q = IPC_DB_Q */
   HwdTxPwrCoordinateT   TxPwrCoordinate[HWD_NUM_HYST_STATES_TXAGC][HWD_NUM_GAIN_POINTS_TXAGC];
} PACKED_POSTFIX  HwdTxPwrCalibDataT;    /* typedef for the message */

/* TX AGC alternative thresholds */
typedef PACKED_PREFIX struct
{
   int16                 TxAgcHystHighThresh1Dbm; /* Threshold at which to switch from first hyst state
                                                   * to second state - Tx power in dBm, Q = IPC_DB_Q */   
   int16                 TxAgcHystLowThresh1Dbm;  /* Threshold at which to switch from second hyst state
                                                   * to first state - Tx Power in dBm, Q = IPC_DB_Q */ 
   int16                 TxAgcHystHighThresh2Dbm; /* Threshold at which to switch from second hyst state
                                                   * to third state - Tx Power in dBm, Q = IPC_DB_Q */
   int16                 TxAgcHystLowThresh2Dbm;  /* Threshold at which to switch from thried hyst state
                                                   * to second state - Tx Power in dBm, Q = IPC_DB_Q */
} PACKED_POSTFIX  HwdTxAgcAltThreshDataT;    /* typedef for the message */


/* Tx AGC temperature adjustment */

typedef PACKED_PREFIX struct
{
   int8 TempCelsius;     /* Temperature coordinate. Units: celsius */
   int16 AdjDb;           /* Tx gain adjustment coordinate. Units: dB with Q=IPC_DB_Q */
} PACKED_POSTFIX  HwdGainTempAdjCoordinateT;    /* format matches HwdCoordinateT */

typedef PACKED_PREFIX struct
{
   HwdGainTempAdjCoordinateT   TxTempAdjCoordinate[HWD_NUM_HYST_STATES_TXAGC][HWD_NUM_TEMP_POINTS_TXAGC];
} PACKED_POSTFIX  HwdTxTempAdjDataT;    /* typedef for the message */



/* Tx AGC frequency channel adjustment */

typedef PACKED_PREFIX struct
{
   int16 FreqChanNum;     /* frequency channel number coordinate. Units: channel number */
   int16 AdjDb;           /* Tx gain adjustment coordinate. Units: dB with Q=IPC_DB_Q */
} PACKED_POSTFIX  HwdGainFreqAdjCoordinateT;     /* format matches HwdCoordinateT */

typedef PACKED_PREFIX struct
{
   HwdGainFreqAdjCoordinateT   TxFreqAdjCoordinate[HWD_NUM_HYST_STATES_TXAGC][HWD_NUM_FREQ_POINTS_TXAGC];
} PACKED_POSTFIX  HwdTxFreqAdjDataT;    /* typedef for the message */

/* Tx battery voltage adjustment */
typedef PACKED_PREFIX struct
{
   uint16 BattVoltage;     /* battery voltage  - mV */
   int16  AdjDb;           /* Tx pwr adjustment coordinate. Units: dB with Q=IPC_DB_Q */
} PACKED_POSTFIX HwdBatteryAdjCoordinateT;

/* Tx Agc battery voltage adjustment */
typedef PACKED_PREFIX struct
{
   HwdBatteryAdjCoordinateT   TxBatteryAdjCoor[HWD_NUM_BATT_POINTS_TXAGC];
} PACKED_POSTFIX  HwdTxBatteryAdjT;    /* typedef for the message */


/* Tx Power Limit Freq Adj */
typedef PACKED_PREFIX struct
{
   HwdGainFreqAdjCoordinateT   TxFreqAdjCoordinate[HWD_NUM_FREQ_POINTS_TX_LIM];
} PACKED_POSTFIX  HwdTxLimitFreqAdjDataT;    /* typedef for the message */

/* Tx Limit temperature adjustment */
typedef PACKED_PREFIX struct
{
   HwdGainTempAdjCoordinateT   TxTempAdjCoordinate[HWD_NUM_TEMP_POINTS_TX_LIM];
} PACKED_POSTFIX  HwdTxLimitTempAdjDataT;    /* typedef for the message */

/* Tx Limit battery voltage adjustment */
typedef PACKED_PREFIX struct
{
   HwdBatteryAdjCoordinateT   TxBatteryAdjCoor[HWD_NUM_BATT_POINTS_TX_LIM];
} PACKED_POSTFIX  HwdTxLimitBatteryAdjT;    /* typedef for the message */

/* Digital Tx AGC SPDM power calibration */
typedef PACKED_PREFIX struct
{
   HwdTxPwrCoordinateT   TxPwrCoordinate[HWD_NUM_HYST_STATES_TXAGC][HWD_NUM_GAIN_POINTS_TXAGC];
} PACKED_POSTFIX  HwdTxAgcSPdmDataT;    /* typedef for the message */

typedef PACKED_PREFIX struct
{
   int16  GainStepDelta;       /* Gain Step, Units: dB with Q=IPC_DB_Q */
   uint16 LowDelayCount;       /* Delay Counts when switching to a lower gain state */
   uint16 HighDelayCount;      /* Delay Counts when switching to a higher gain state */
} PACKED_POSTFIX  HwdRxDagcGainParamsT;


typedef PACKED_PREFIX struct
{
   int16  HystThresh;      /* Threshold at which to switch from the highest hyst state
                           ** to the next lower state if Rx gain stays below its value
                           ** long enough (HystDelayCount PCGs) */                           
   uint16 HystDelayCount;  /* Number of PCGs to satisfy the delay threshold */

} PACKED_POSTFIX  HwdRxDagcHystParamsT;
#endif

#define HWD_RX_GAIN_PARAMS_SIZE HWD_MAX_NUM_DIGITAL_GAIN_STATES-1

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
 typedef PACKED_PREFIX struct
 {
   uint16                 RxAgcDigitalGainStates; /* 1 to 8 possible Digital Gain states */
   int16                  RxRefLeveldB;
   uint8                  RxRefGainState;
   HwdRxDagcRefBitsT      RxRefBitSettings;
   HwdRxDagcSwitchThreshT RxSwitchThresh[HWD_MAX_NUM_DIGITAL_GAIN_STATES];
   HwdRxDagcGainParamsT   RxGainParams[HWD_RX_GAIN_PARAMS_SIZE];
   HwdRxDagcHystParamsT   RxHystParams;
   int16                  RxRefGainALog2;         /* Ref gain in Log2, Q8 */
   int16                  DGainStepALog2[RFC_RX_DAGC_SGAIN_SZ];    /* Gain Steps in Log2, Q8 */

 } PACKED_POSTFIX  HwdRxDagcCalibDataT;    /* typedef for the message */

/* Rx AGC temperature adjustment */
typedef PACKED_PREFIX struct
{
   HwdGainTempAdjCoordinateT   RxTempAdjCoordinate[HWD_NUM_TEMP_POINTS_RXAGC];
} PACKED_POSTFIX  HwdRxTempAdjDataT;    /* typedef for the message */

/* Rx AGC frequency channel adjustment */
typedef PACKED_PREFIX struct
{
   HwdGainFreqAdjCoordinateT   RxFreqAdjCoordinate[HWD_NUM_FREQ_POINTS_RXAGC];
} PACKED_POSTFIX  HwdRxFreqAdjDataT;    /* typedef for the message */

/* Rx AGC Multi-Gain for freq adjustment */
typedef PACKED_PREFIX struct
{
   uint8                      GainStateMask;
   HwdGainFreqAdjCoordinateT  RxFreqAdjCoordinate[HWD_NUM_FREQ_POINTS_RXAGC];
} PACKED_POSTFIX  HwdRxMultiGainFreqAdjDataT;    /* typedef for the message */

/* Tx power detector expected measurement */
 typedef PACKED_PREFIX struct
 {
   uint16 TxPwrDetAdcMeas; /* Tx ADC reading */
   int16 TxPwrDbm;        /* Tx power coordinate, units: dBm Q= IPC_DB_Q */
 } PACKED_POSTFIX  HwdTxPwrDetCoordT;      /* format matches HwdCoordinateT */

/* Tx power detect expected measurement */
 typedef PACKED_PREFIX struct
 {
   HwdTxPwrDetCoordT   HwdTxPwrDetCoordinate[HWD_NUM_GAIN_POINTS_TX_PWR_DET];
 } PACKED_POSTFIX  HwdTxPwrDetTableT;    /* typedef for the message */

/* Tx Pwr detect temperature adjustment */
 typedef PACKED_PREFIX struct
 {
   HwdGainTempAdjCoordinateT   TxPwrDetTempAdjCoor[HWD_NUM_TEMP_POINTS_TX_PWR];
 } PACKED_POSTFIX  HwdTxPwrDetTempAdjT;    /* typedef for the message */


/* Tx Pwr detect frequency adjustment */
 typedef PACKED_PREFIX struct
 {
   HwdGainFreqAdjCoordinateT   TxPwrDetFreqAdjCoor[HWD_NUM_FREQ_POINTS_TX_PWR];
 } PACKED_POSTFIX  HwdTxPwrDetFreqAdjT;    /* typedef for the message */

/* Tx Pwr detect Max power limit battery voltage adjustment */
 typedef PACKED_PREFIX struct
 {
   HwdBatteryAdjCoordinateT   TxPwrDetBattMaxPwrAdjCoor[HWD_NUM_MAX_BATT_POINTS_TX_PWR];
 } PACKED_POSTFIX  HwdTxPwrDetBattMaxAdjT;    /* typedef for the message */

/* HWD_CAL_BAND_*_RX_BANK_CAL_MSG */
 typedef PACKED_PREFIX struct
 {
   uint8   CalData[16];
 } PACKED_POSTFIX HwdRxBankCalDataT;    /* typedef for the message */

/* HWD_CAL_BAND_*_RX_BANK_CAL_MSG */
 typedef PACKED_PREFIX struct
 {
   uint8   CalData[16];
 } PACKED_POSTFIX  HwdRfCalDataT;    /* typedef for the message */
#endif
/* generic line segment type */
typedef PACKED_PREFIX struct
{
   int16 BoundaryLeft[2];   /* left boundary of segment - for x and Y coordinates */
   int32 Slope;           /* slope of segment */
   int16 Intercept;       /* y-intercept of segment */
} PACKED_POSTFIX  NewHwdLineSegmentT;

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
typedef enum
{
   REFERENCE_1=0,
   REFERENCE_2,
   MAX_REFERENCE_PTS
}HwdAutoBattCalRefT;

typedef enum
{ 
   AUTO_BATT_CAL_IDLE=0,
   READ_REF,
   ADJUST_PDM_TO_REF,
   READ_ADC
} HwdAutoBattCalStateT;

typedef enum
{
   CAL_SUCCESS=0,
   CAL_FAILURE,
   CAL_IN_PROGRESS
} HwdAutoBattCalResultT;

 typedef struct
 {
   HwdAutoBattCalStateT state;
   uint16 SaveCfValue;
   HwdPwrSaveModesT PwrSaveMode;
   uint16 adcData[MAX_REFERENCE_PTS];
   uint16 pdmData[MAX_REFERENCE_PTS];
   uint16 mid;
   uint16 lower;
   uint16 upper;
   uint16 target;
   uint8 testPts;
   HwdAutoBattCalRefT refIndex;
   
   HwdBatteryPdmCalibDataT PdmBattTbl;
   HwdBattCalibrationMsgT BattCalTbl;

   ExeRspMsgT  RspInfo;

 } HwdAutoBattCalT;
#endif

typedef enum
{
   SOFT_INIT                        = 0xf000,
   COMMON_TEMP_TABLE,
   COMMON_BATTERY_TABLE,
   COMMON_BATT_PDM_TABLE,
   COMMON_INVALID_RF_BAND
} HwdCalErrorCodeT;


typedef PACKED_PREFIX struct
{
   int16 BoundaryLeftCelsius; /* left boundary of the region                   */
   int16 SlopeDbPerCelsius;   /* slope of the linear region, units dB/deg celsius (Q=HWD_TRANS_FUNC_SLOPE_GAIN_Q) */
   int16 InterceptDb;         /* DAC step intercept of region, units DAC steps */

} PACKED_POSTFIX  CelsiusToDbLinearRegionT;  /* structure for a region of the piecewise linear transfer function
                              * of a celsius temperature to a dB correction.
                              * Must match HwdLineSegmentT for HwdCalCoordToSlopeInterc() to work.
                              */
typedef PACKED_PREFIX struct
{
   int16 BoundaryLeftChanNum; /* left boundary of the region                   */
   int16 SlopeDbPerFreqChan;  /* slope of the linear region, units dB/frequency channel number
                               * (Q=HWD_TRANS_FUNC_SLOPE_FREQ_Q) 
                               */
   int16 InterceptDb;         /* DAC step intercept of region, units DAC steps */

} PACKED_POSTFIX  FreqChanToDbLinearRegionT; /* structure for a region of the piecewise linear transfer function
                              * of a frequency channel number to dB correction.
                              * Must match HwdLineSegmentT for HwdCalCoordToSlopeInterc() to work.
                              */
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
typedef PACKED_PREFIX struct
{
   int16 BoundaryLeftBatt;   /* left boundary of the region                   */
   int16 SlopeDbPerBatt;      /* slope of the linear region, units dB/batt voltage (Q=HWD_TRANS_FUNC_SLOPE_GAIN_Q) */
   int16 InterceptDb;         /* DAC step intercept of region, units DAC steps */

} PACKED_POSTFIX  BatteryToDbLinearRegionT;  /* structure for a region of the piecewise linear transfer function
                              * of a celsius temperature to a dB correction.
                              * Must match HwdLineSegmentT for HwdCalCoordToSlopeInterc() to work.
                              */

typedef PACKED_PREFIX struct
{
   int16 BoundaryLeftAdcStep; /* left boundary of the region, units ADC steps (Q=0)                 */
   int16 SlopeDbmPerAdcStep;  /* slope of the linear region, units dBm/ADC step (Q = HWD_TRANS_FUNC_SLOPE_GAIN_Q) */
   int16 InterceptDbm;        /* dBm intercept of region, units dBm (Q=DB_Q) */

} PACKED_POSTFIX  DbmToAdcStepLinearRegionT; /* structure for a region of the piecewise linear transfer function
                              * of an AUX ADC to dBm step conversion (i.e Tx power limit detect)
                              * Must match HwdLineSegmentT for HwdCalCoordToSlopeInterc() to work.
                              */
#endif

typedef struct
{
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
   int16 TotalDb[HWD_NUM_HYST_STATES_TXAGC]; /* total adjustment applied to power calc. Units dB in Q=IPC_DB_Q */
   int16 FreqDb[HWD_NUM_HYST_STATES_TXAGC];  /* adjustment for frequency channel, units dB in Q=IPC_DB_Q */
   int16 TempDb[HWD_NUM_HYST_STATES_TXAGC];  /* adjustment for temperature, units dB in Q=IPC_DB_Q */
   int16 BattDb;       /* adjustment for battery voltage, units dB in Q=IPC_DB_Q */
#else
   int16 TotalDb[HWD_PA_MODE_NUM]; /* total adjustment applied to power calc. Units dB in Q5 */
   int16 FreqDb[HWD_PA_MODE_NUM];  /* adjustment for frequency channel, units dB in Q5 */
   int16 TempDb[HWD_PA_MODE_NUM];  /* adjustment for temperature, units dB in Q5 */
#endif
} TxDbAdjustmentT;             /* structure for the adjustments made to power */


typedef struct
{
   int16 TotalDb;      /* total adjustment (freq chan + temperature) applied to a gain/power
                        * calculation. Units dB in Q=IPC_DB_Q 
                        */
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
   int16 FreqDb[HWD_MAX_NUM_DIGITAL_GAIN_STATES];       /* adjustment for frequency channel, units dB in Q=IPC_DB_Q */
#else
   int16 FreqDb[HWD_LNA_MODE_NUM];  /*max 8 range in MTK solution*/
#endif
   int16 TempDb;       /* adjustment for temperature, units dB in Q=IPC_DB_Q */
} RxDbAdjustmentT;             /* structure for the adjustments made to a gain or power due to frequency
                                * and temperature variations.
                                */
//#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
typedef struct
{
   int16 TotalDb;      /* total adjustment (freq chan + temperature) applied to a gain/power
                        * calculation. Units dB in Q=IPC_DB_Q 
                        */
   int16 FreqDb;       /* adjustment for frequency channel, units dB in Q=IPC_DB_Q */
   int16 TempDb;       /* adjustment for temperature, units dB in Q=IPC_DB_Q */
   int16 BattDb;       /* adjustment for battery voltage, units dB in Q=IPC_DB_Q */
   int16 PwrDet;       /* Tx Pwr Detect adjustment, units dB in Q=IPC_DB_Q */

} DbMaxLimitAdjT;               /* structure for the adjustments made to a gain or power due to frequency
                                * and temperature variations.
                                */
//#endif
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
typedef struct
{
   int16 TotalDb;      /* total adjustment (freq chan + temperature) applied to a gain/power
                        * calculation. Units dB in Q=IPC_DB_Q 
                        */
   int16 FreqDb;       /* adjustment for frequency channel, units dB in Q=IPC_DB_Q */
   int16 TempDb;       /* adjustment for temperature, units dB in Q=IPC_DB_Q */
   int16 RateDb;       /* adjustment for the data rate, units dB in Q=IPC_DB_Q */
   int16 TxPwrDb;      /* measured Tx Pwr Detect, units dB in Q=IPC_DB_Q */

} DbTxHDetAdjustmentT;   /* structure for the adjustments made to a gain or power due to frequency,
                            * temperature and battery voltage variations.
                            */
#else
typedef struct
{
   int16 TotalDb[HWD_PA_MODE_NUM];      /* total coupler loss with freq compensation, Units dB in Q5 */
   int16 FreqDb;                        /* adjustment for frequency channel, units dB in Q5 */
} DbTxHDetAdjustmentT;   /* structure for the adjustments made to a gain or power due to frequency */

#endif

typedef PACKED_PREFIX struct
{
   int16        BoundaryLeftDbm;  /* left boundary of the region, units dBm, Q=RFC_DB_Q */
   int16        SlopeStepPerDbm;  /* slope of the linear region, units DAC step/dBmQ (Q = SYS_LIN_APPROX_SLOPE_Q) */
   int16        InterceptStep;    /* DAC Step intercept of region, Q0 */
} PACKED_POSTFIX  RxDacStepToDbmLinearRegionT;      /* structure for a region of the piecewise linear transfer function */

typedef PACKED_PREFIX struct
{
   int16    BoundaryLeft;          /* left boundary of the region for Aux Adc values */
   int16    TempSlopeCelPerStep;   /* slope of ADC steps to celsius conversion, with 
                                    * Q = HWD_TRANS_FUNC_SLOPE_Q */
   int16    TempIntercC;           /* intercept for ADC reading to celsius conversion (degrees
                                    * celsius for a ADC reading = 0x000).
                                    */
} PACKED_POSTFIX  HwdTemperatureCalibParmT;
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
typedef PACKED_PREFIX struct
{
   int16    BoundaryLeft;          /* left boundary of the region for Aux Adc values */
   int16    BattSlopePerStep;      /* slope of ADC steps to battery voltage conversion, with 
                                    * Q = HWD_TRANS_FUNC_SLOPE_Q */
   int16    BattInterc;            /* intercept for ADC reading to battery voltage conversion
                                    */
} PACKED_POSTFIX HwdBattVoltageCalibParmT;

typedef PACKED_PREFIX struct
{
   int16    BoundaryLeft;          /* left boundary of the region for Battery voltage Q6 */
   int16    BattPdmSlopePerStep;   /* slope of battery voltage to PDM steps conversion, with 
                                    * Q = HWD_TRANS_FUNC_SLOPE_Q */
   int16    BattPdmInterc;         /* intercept for battery voltage to PDM values conversion
                                    */
} PACKED_POSTFIX HwdBattChargerCalibParmT;

/* SPY: All values associated with Tx Power Detect logic */
typedef PACKED_PREFIX struct
{
   uint16 MeasuredAuxAdc;           /* Measured AUX ADC value from Tx Power Channel */
   int16  TxPowerDbm;               /* Tx Power in dBm with Q=IPC_DB_Q as determined from cal tables */
   int16  TxPowerFreqAdjDbm;        /* Frequency Adjustment to Tx Power in dBm with Q=IPC_DB_Q */
   int16  TxPowerTempAdjDbm;        /* Temperature Adjustment to Tx Power in dBm with Q=IPC_DB_Q */
   int16  TxPowerRateAdjDbm;        /* Data Rate Adjustment to Tx Power in dBm with Q=IPC_DB_Q */
   int16  TxPowerUnAdjDbm;          /* Unadjusted Tx Power in dBm with Q=IPC_DB_Q */
} PACKED_POSTFIX  HwdTxAgcPwrDetectT;
#else 
typedef PACKED_PREFIX struct
{
   uint16 MeasuredAuxAdc;           /* Measured AUX ADC value from Tx Power Channel */
   int16  TxPowerDbm;               /* Tx Power in dBm with Q=IPC_DB_Q as determined from cal tables */
   int16  TxPowerFreqAdjDbm;        /* Frequency Adjustment to Tx Power in dBm with Q=IPC_DB_Q */
   int16  TxPowerTempAdjDbm;        /* Temperature Adjustment to Tx Power in dBm with Q=IPC_DB_Q */
   int16  TxPowerUnAdjDbm;          /* Unadjusted Tx Power in dBm with Q=IPC_DB_Q */
} PACKED_POSTFIX  HwdTxAgcPwrDetectT;
#endif

/* calibration init save response info type */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;             /* cmd/rsp info for acknowlegment */
   bool        DelayRsp;
} PACKED_POSTFIX  HwdCalSaveRspInfoT;

//#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo; /* cmd/rsp info for acknowlegment */
} PACKED_POSTFIX  HwdAuxAdcAutoCalT;
//#endif

typedef PACKED_PREFIX struct
{
   uint8 CalResult;       /* 0 = Success, 1 = Failure, 2 = Already in progress */
} PACKED_POSTFIX HwdAuxAdcAutoCalRspT;

/* generic line segment type */
typedef PACKED_PREFIX struct
{
   int16 BoundaryLeft;    /* left boundary of segment */
   int32 Slope;           /* slope of segment */
   int16 Intercept;       /* y-intercept of segment */
} PACKED_POSTFIX  HwdLineSegment2T;

/* calibration init message type */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;             /* cmd/rsp info for acknowlegment */
   uint8       CalibMode;
} PACKED_POSTFIX  HwdCalibInitMsgT;

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
/* Calibration init NVRAM message type */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;             /* cmd/rsp info for acknowlegment */
} PACKED_POSTFIX  HwdCalInitNvramMsgT;
#endif

/* Calibration data struct usec for representing Tx AGC calibration data in units of
** Amplitude log2 - required for use with the CBP7x RPC hardware architecture */
typedef PACKED_PREFIX struct
{
   int16  BoundaryLeftALog2;  /* left boundary of the region, units in ALog2, Q=RFC_DB_Q */
   int16  SlopeStepPerALog2;  /* slope of the linear region, units DAC step/ALog2Q (Q = SYS_LIN_APPROX_SLOPE_Q) */
   int16  InterceptStep;      /* DAC Step intercept of region, Q0 */
} PACKED_POSTFIX  HwdTxDacStepToALog2LinearRegionT; /* structure for a region of the piecewise linear transfer function */

/* Tx AGC Hysteresis points structure for determining Tx AGC switch points */
typedef struct
{
   int16  HighThresh1ALog2; /* thresh to switch the TxAGC 1st hyst state to med gain, TxGain in ALog2 with Q=HWD_RMC_ALOG2_GAIN_Q */
   int16  LowThresh1ALog2;  /* thresh to switch the TxAGC 1st hyst state to low gain, TxGain in ALog2 with Q=HWD_RMC_ALOG2_GAIN_Q */
   int16  HighThresh2ALog2; /* thresh to switch the TxAGC 2nd hyst state to high gain, TxGain in ALog2 with Q=HWD_RMC_ALOG2_GAIN_Q */
   int16  LowThresh2ALog2;  /* thresh to switch the TxAGC 2nd hyst state to med gain, TxGain in ALog2 with Q=HWD_RMC_ALOG2_GAIN_Q */

} HwdTxAgcHystPointsALog2T;

typedef struct
{
   uint16 segment;
   uint16 size;
}DbInfoT;

typedef enum
{
   PREFER_UNMODIFY,
   PREFER_HIGHER_GAIN,
   PREFER_LOWER_GAIN
}GainStateDirT;

typedef PACKED_PREFIX struct
{
   int16          HystThresh; /* Amplitude dB of HystThresh */
   GainStateDirT  Direction;
} PACKED_POSTFIX  HwdRxAgcGainPrefParamsT;

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
typedef PACKED_PREFIX struct
{
   uint16 PersistenceCount;  /* Number of PCGs to satisfy the delay threshold */
   HwdRxAgcGainPrefParamsT    GainPref[HWD_MAX_NUM_DIGITAL_GAIN_STATES-1];
} PACKED_POSTFIX  HwdRxAgcGainPrefT;
#endif

typedef PACKED_PREFIX struct
{
   RssiCorrectionDataT RssiCorrectTbl[MAX_RSSI_CORRECTIONS];
} PACKED_POSTFIX HwdRssiCorrectionMsgT;

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
typedef PACKED_PREFIX struct
{
   uint16 LowDelayCount;       /* Delay Counts when switching to a lower gain state */
   uint16 HighDelayCount;      /* Delay Counts when switching to a higher gain state */
} PACKED_POSTFIX HwdRxStepDelaysT;
#endif

typedef PACKED_PREFIX struct
{
   HwdRxDagcSwitchThreshT RxSwitchThresh[HWD_MAX_NUM_DIGITAL_GAIN_STATES];
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
   HwdRxStepDelaysT       SwitchDelays[HWD_MAX_NUM_DIGITAL_GAIN_STATES];
   HwdRxDagcHystParamsT   RxHystParams;
#endif
} PACKED_POSTFIX HwdRxAgcDefaultDataT;    /* typedef for the message */

typedef PACKED_PREFIX struct
{
   char   RfOption[20];
   char   RfDriverRev[10];
} PACKED_POSTFIX HwdRfVersionT;           /* typedef for the message */

typedef enum
{
   TEMP_UPD = 0,
   BATT_UPD,
   BAND_UPD,
   ADJ_ONLY_UPD,
   RX_PARMS_UPD,
   TX_PARMS_UPD,
   MAX_PARMS_UPD
}CalTraceModes;

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
typedef PACKED_PREFIX struct
{
   int16 FreqGrAdj[HWD_NUM_GAIN_STATE_RANGES];        /* total adjustment (freq chan) applied to a gain/power */
} PACKED_POSTFIX RxGainRangeAdjustmentT;
#endif

#define RX_DAGC_BASE_CH_ADJ_ONLY      12

typedef struct
{
   HwdRfBandT           Band;
   uint16               Chan;
} HwdCalConfigT;

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
/** the current frequency and temperature compensation after interpolasion*/
typedef struct
{
   /* Rx calibration compensation data */
   int16 RxPathLossCompHpm[HWD_RF_MAX_NUM_OF_RX][HWD_LNA_MODE_NUM];
   int16 RxPathLossCompLpm[HWD_RF_MAX_NUM_OF_RX][HWD_LNA_MODE_NUM];

   /*Tx calibration compensation data*/
   int16 TxPaGainComp1xRtt[HWD_PA_MODE_NUM];
   int16 TxPaGainCompEvdo[HWD_PA_MODE_NUM];

   /*Tx Det calibration compensation data*/
   int16 TxDetCplComp1xRtt[HWD_PA_MODE_NUM];
   int16 TxDetCplCompEvdo[HWD_PA_MODE_NUM];

}HwdCalibCompDataT;

#ifndef GEN_FOR_PC
#undef DBM_RF_CAL_ITEM
#define DBM_RF_CAL_ITEM(nAME, sIZE, dATA, fUNC, bAND)     DBM_##nAME##_SIZE = sIZE,
typedef enum
{
#include "dbmrfcalid.h"
    DBM_DUMMY_SEG_RF_CAL_SIZE
}DbmRfCalSegmentSizeT;
#endif

#endif

/*----------------------------------------------------------------------------
 Global Data
----------------------------------------------------------------------------*/

extern HwdRfCalDataStatusSpyT  HwdCalSavedData[SYS_MODE_MAX];

extern int16    CurrentTempCelsius[SYS_MODE_MAX];                 /* Q=0 */
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern int16    CurrentBatteryVolt;                               /* Q=0 */
#endif
extern bool     TxMsgSent;
extern bool     RxMsgSent;

//#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern uint16   HwdCalBandGapVtrimSettings;
//#endif

extern bool                     HwdTxPwrDetectCircuitEnabled[];
extern HwdCalibrParmT           HwdCalibrParm;
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern HwdCalibrParmT           HwdAuxCalibrParm;
#endif

/* CBP7x Rx AGC calibration data using Amplitude Log2 gain units */
extern int16 HwdCalibRxDagcDigiGainBitValuesALog2[HWD_RX_DAGC_DIGIGAIN_SIZE];

/* CBP7x DO Tx AGC calibration data using Amplitude Log2 gain units */
extern int16 TxHDetMinPowerTable[];

/* Tx calibration tables */
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern DacStepToDbmLinearRegionT  TxPwrCalibLine     [HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_HYST_STATES_TXAGC][HWD_NUM_GAIN_POINTS_TXAGC];
                                                               /* slope+intercept, online use */
extern CelsiusToDbLinearRegionT   TxAgcTempAdjLine   [HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_HYST_STATES_TXAGC][HWD_NUM_TEMP_POINTS_TXAGC];
extern BatteryToDbLinearRegionT   TxAgcBattAdjLine   [HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_BATT_POINTS_TXAGC];
                                                                 /* slope+intercept, online use */
extern FreqChanToDbLinearRegionT  TxAgcFreqAdjLine   [HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_HYST_STATES_TXAGC][HWD_NUM_FREQ_POINTS_TXAGC];
extern FreqChanToDbLinearRegionT  TxLimitFreqAdjLine [HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_FREQ_POINTS_TX_LIM];
                                                                 /* adjustment delta
                                                                  * used to limit maximum power.
                                                                  */

extern CelsiusToDbLinearRegionT   TxLimitTempAdjLine [HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_TEMP_POINTS_TXAGC];
extern CelsiusToDbLinearRegionT   TxPwrDetTempAdjLine [HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_TEMP_POINTS_TX_PWR];
                                                                /* slope+intercept, online use */
extern BatteryToDbLinearRegionT   TxLimitBattAdjLine [HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_BATT_POINTS_TX_LIM];
                                                                /* slope+intercept, online use */
#else 
extern HwdRfPaContextT TxPwrPaContext[HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_GAIN_POINTS_TXAGC];
extern CelsiusToDbLinearRegionT   TxAgcTempAdjLine   [HWD_NUM_SUPPORTED_BAND_CLASS][HWD_PA_MODE_NUM][HWD_NUM_TEMP_POINTS_TXAGC];
extern FreqChanToDbLinearRegionT  TxAgcFreqAdjLine   [HWD_NUM_SUPPORTED_BAND_CLASS][HWD_PA_MODE_NUM][HWD_NUM_FREQ_POINTS_TXAGC];
#endif 


extern TxDbAdjustmentT            TxAgcAdjust        [HWD_NUM_SUPPORTED_BAND_CLASS];
                                                                  /* total adjustment (excluding
                                                                   * TxFreqAdjustMaxPwr) for
                                                                   * temperature and frequency
                                                                   * channel.
                                                                   */

//#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern DbMaxLimitAdjT             TxMaxLimitAdjDb    [HWD_NUM_SUPPORTED_BAND_CLASS];   /* adjustment applied to calc max power limit */

//#endif
extern DbTxHDetAdjustmentT        TxPwrDetectAdjust  [HWD_NUM_SUPPORTED_BAND_CLASS];
                                                                  /* total adjustment for
                                                                   * temperature and frequency
                                                                   * channel.
                                                                   */
extern FreqChanToDbLinearRegionT  TxPwrDetFreqAdjLine [HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_FREQ_POINTS_TX_PWR];
                                                                  /* slope+intercept, online use */
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)                                                                  
extern BatteryToDbLinearRegionT   TxPwrDetMaxBattAdjLine [HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_MAX_BATT_POINTS_TX_PWR];
                                                                  /* slope+intercept, online use */
extern DacStepToDbmLinearRegionT  TxAgcSPdmLine[HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_HYST_STATES_TXAGC][HWD_NUM_GAIN_POINTS_TXAGC];
                                                                  /* slope+intercept, online use */
extern HwdTxDacStepToALog2LinearRegionT  TxAgcSPdmLineAlog2[HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_HYST_STATES_TXAGC][HWD_NUM_GAIN_POINTS_TXAGC];

/* Aux Tx calibration tables */

extern DacStepToDbmLinearRegionT  AuxTxPwrCalibLine     [HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_HYST_STATES_TXAGC][HWD_NUM_GAIN_POINTS_TXAGC];
                                                                  /* slope+intercept, online use */
extern CelsiusToDbLinearRegionT   AuxTxAgcTempAdjLine   [HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_HYST_STATES_TXAGC][HWD_NUM_TEMP_POINTS_TXAGC];
                                                                  /* slope+intercept, online use */
extern BatteryToDbLinearRegionT   AuxTxAgcBattAdjLine   [HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_BATT_POINTS_TXAGC];
                                                                  /* slope+intercept, online use */
extern FreqChanToDbLinearRegionT  AuxTxAgcFreqAdjLine   [HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_HYST_STATES_TXAGC][HWD_NUM_FREQ_POINTS_TXAGC];
                                                                  /* slope+intercept, online use */
extern FreqChanToDbLinearRegionT  AuxTxLimitFreqAdjLine [HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_FREQ_POINTS_TX_LIM];
                                                                  /* adjustment delta
                                                                   * used to limit maximum power.
                                                                   */
extern CelsiusToDbLinearRegionT   AuxTxLimitTempAdjLine [HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_TEMP_POINTS_TXAGC];
extern CelsiusToDbLinearRegionT   AuxTxPwrDetTempAdjLine [HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_TEMP_POINTS_TX_PWR];
                                                                  /* slope+intercept, online use */
extern BatteryToDbLinearRegionT   AuxTxLimitBattAdjLine [HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_BATT_POINTS_TX_LIM];
                                                                  /* slope+intercept, online use */
extern TxDbAdjustmentT            AuxTxAgcAdjust        [HWD_NUM_SUPPORTED_BAND_CLASS];
                                                                  /* total adjustment (excluding
                                                                   * TxFreqAdjustMaxPwr) for
                                                                   * temperature and frequency
                                                                   * channel.
                                                                   */

extern DbMaxLimitAdjT             AuxTxMaxLimitAdjDb    [HWD_NUM_SUPPORTED_BAND_CLASS];   /* adjustment applied to calc max power limit */

extern DbTxHDetAdjustmentT        AuxTxPwrDetectAdjust  [HWD_NUM_SUPPORTED_BAND_CLASS];
                                                                  /* total adjustment for
                                                                   * temperature and frequency
                                                                   * channel.
                                                                   */
extern FreqChanToDbLinearRegionT  AuxTxPwrDetFreqAdjLine [HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_FREQ_POINTS_TX_PWR];
                                                                  /* slope+intercept, online use */
extern BatteryToDbLinearRegionT   AuxTxPwrDetMaxBattAdjLine [HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_MAX_BATT_POINTS_TX_PWR];
                                                                  /* slope+intercept, online use */
extern DacStepToDbmLinearRegionT  AuxTxAgcSPdmLine[HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_HYST_STATES_TXAGC][HWD_NUM_GAIN_POINTS_TXAGC];
                                                                  /* slope+intercept, online use */
extern HwdTxDacStepToALog2LinearRegionT  AuxTxAgcSPdmLineAlog2[HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_HYST_STATES_TXAGC][HWD_NUM_GAIN_POINTS_TXAGC];

/* Rx calibration tables and variables */
extern DbToDacStepLinearRegionT   RxAgcCalibLine   [HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_HYST_STATES_RXAGC][SYS_MAX_NUM_GAIN_POINTS_RXAGC];
extern FreqChanToDbLinearRegionT  RxAgcFreqAdjLine [HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_GAIN_STATE_RANGES][HWD_NUM_FREQ_POINTS_RXAGC];
extern uint8                      MainRxGainRangeMask[HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_GAIN_STATE_RANGES];
#else
extern FreqChanToDbLinearRegionT  RxAgcFreqAdjLine [HWD_NUM_SUPPORTED_BAND_CLASS][HWD_LNA_MODE_NUM][HWD_NUM_FREQ_POINTS_RXAGC];
extern uint8                      MainRxGainRangeMask[HWD_NUM_SUPPORTED_BAND_CLASS][HWD_LNA_MODE_NUM];
#endif
extern CelsiusToDbLinearRegionT   RxAgcTempAdjLine [HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_TEMP_POINTS_RXAGC];

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern RxGainRangeAdjustmentT     RxMainGainRangeAdj[HWD_NUM_SUPPORTED_BAND_CLASS];
extern RxGainRangeAdjustmentT     RxDivGainRangeAdj[HWD_NUM_SUPPORTED_BAND_CLASS];
extern RxGainRangeAdjustmentT     RxSecGainRangeAdj[HWD_NUM_SUPPORTED_BAND_CLASS];
#endif

extern RxDbAdjustmentT            RxAdjust         [HWD_NUM_SUPPORTED_BAND_CLASS];
                                                                  /* total adjustment for
                                                                   * temperature and frequency
                                                                   * channel.
                                                                   */

/* Rx Diversity calibration tables and variables */
extern CelsiusToDbLinearRegionT   DivRxAgcTempAdjLine [HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_TEMP_POINTS_RXAGC];
                                                                  /* slope+intercept, online use */
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern FreqChanToDbLinearRegionT  DivRxAgcFreqAdjLine [HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_GAIN_STATE_RANGES][HWD_NUM_FREQ_POINTS_RXAGC];
extern uint8                      DivRxGainRangeMask[HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_GAIN_STATE_RANGES];
#else
extern FreqChanToDbLinearRegionT  DivRxAgcFreqAdjLine [HWD_NUM_SUPPORTED_BAND_CLASS][HWD_LNA_MODE_NUM][HWD_NUM_FREQ_POINTS_RXAGC];
extern uint8                      DivRxGainRangeMask[HWD_NUM_SUPPORTED_BAND_CLASS][HWD_LNA_MODE_NUM];
#endif
extern RxDbAdjustmentT            DivRxAdjust         [HWD_NUM_SUPPORTED_BAND_CLASS];
                                                                  /* total adjustment for
                                                                   * temperature and frequency
                                                                   * channel.
                                                                   */
/* Aux Rx calibration tables and variables */
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern CelsiusToDbLinearRegionT   SecRxAgcTempAdjLine [HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_TEMP_POINTS_RXAGC];
                                                                  /* slope+intercept, online use */
extern FreqChanToDbLinearRegionT  SecRxAgcFreqAdjLine [HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_GAIN_STATE_RANGES][HWD_NUM_FREQ_POINTS_RXAGC];
extern uint8                      SecRxGainRangeMask[HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_GAIN_STATE_RANGES];

extern RxDbAdjustmentT            SecRxAdjust         [HWD_NUM_SUPPORTED_BAND_CLASS];
                                                                  /* total adjustment for
                                                                   * temperature and frequency
                                                                   * channel.
                                                                   */

extern int16 TxHDetMaxPowerTable[HWD_NUM_SUPPORTED_BAND_CLASS];
extern int16 TxHDetMaxPowerAdj[HWD_NUM_SUPPORTED_BAND_CLASS];
extern int16 AuxTxHDetMaxPowerAdj[HWD_NUM_SUPPORTED_BAND_CLASS];
#endif

/* Main path Rx AGC calibration data Point */
extern HwdRxDagcCalibDataT *HwdRxDagcCalibDataP[HWD_NUM_SUPPORTED_BAND_CLASS];
/* Main path Rx AGC calibration data when IMD detected*/
extern HwdRxDagcCalibDataT  HwdRxDagcCalibData[HWD_NUM_SUPPORTED_BAND_CLASS];

/* Main path Rx AGC calibration data when No IMD detected */
extern HwdRxDagcCalibDataT  HwdRxDagcCalibDataNoIMD[HWD_NUM_SUPPORTED_BAND_CLASS];

/* Diversity path Rx AGC calibration data Point */
extern HwdRxDagcCalibDataT *HwdRxDivDagcCalibDataP[HWD_NUM_SUPPORTED_BAND_CLASS];
/* Diversity path Rx AGC calibration data when IMD detected (always uses Digital AGC format) */
extern HwdRxDagcCalibDataT  HwdRxDivDagcCalibData[HWD_NUM_SUPPORTED_BAND_CLASS];
/* Diversity path Rx AGC calibration data when No IMD detected (always uses Digital AGC format) */
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern HwdRxDagcCalibDataT  HwdRxDivDagcCalibDataNoIMD[HWD_NUM_SUPPORTED_BAND_CLASS];
/* Aux Rx AGC calibration data Point */
extern HwdRxDagcCalibDataT *HwdRxSecDagcCalibDataP[HWD_NUM_SUPPORTED_BAND_CLASS];

/* Aux Rx AGC calibration data (always uses Digital AGC format) */
extern HwdRxDagcCalibDataT  HwdRxSecDagcCalibData[HWD_NUM_SUPPORTED_BAND_CLASS];
/* Aux Rx AGC calibration data when No IMD detected (always uses Digital AGC format) */
extern HwdRxDagcCalibDataT  HwdRxSecDagcCalibDataNoIMD[HWD_NUM_SUPPORTED_BAND_CLASS];

/* CBP7x Outer-Loop Tx AGC calibration data structure */
extern HwdTxDacStepToALog2LinearRegionT HwdTxPwrALog2CalibLine[HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_HYST_STATES_TXAGC][HWD_NUM_GAIN_POINTS_TXAGC];
#else
extern HwdTxDacStepToALog2LinearRegionT HwdTxPwrALog2CalibLine[HWD_NUM_SUPPORTED_BAND_CLASS][HWD_PA_MODE_NUM][HWD_NUM_GAIN_POINTS_TXAGC];
#endif
extern HwdTxAgcHystPointsALog2T         HwdTxAgcHystPoints[HWD_NUM_SUPPORTED_BAND_CLASS];

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern HwdTxDacStepToALog2LinearRegionT HwdAuxTxPwrALog2CalibLine[HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_HYST_STATES_TXAGC][HWD_NUM_GAIN_POINTS_TXAGC];
extern HwdTxAgcHystPointsALog2T         HwdAuxTxAgcHystPoints[HWD_NUM_SUPPORTED_BAND_CLASS];
#endif
extern RssiCorrectionDataT RssiCorrectionTable[];


#ifdef DEFAULT_RX_HYSTERESIS
/* Main path Rx AGC default data */
extern HwdRxAgcDefaultDataT  HwdRxAgcDefaultData[HWD_NUM_SUPPORTED_BAND_CLASS];
/* Main path Rx AGC default data when No IMD detected */
extern HwdRxAgcDefaultDataT  HwdRxAgcDefaultDataNoIMD[HWD_NUM_SUPPORTED_BAND_CLASS];

/* Diversity path Rx AGC default data */
extern HwdRxAgcDefaultDataT  HwdRxDivAgcDefaultData[HWD_NUM_SUPPORTED_BAND_CLASS];
/* Diversity path Rx AGC default data when No IMD detected */
extern HwdRxAgcDefaultDataT  HwdRxDivAgcDefaultDataNoIMD[HWD_NUM_SUPPORTED_BAND_CLASS];
#endif

#if (defined SYS_OPTION_GPS_VASCO)

/* GPS characterization database */
extern HwdCalGpsCharDBT GPSCharacterizationDB;
#endif /* SYS_OPTION_GPS_VASCO */

 /* GPS characterization database */
extern HwdGpsCalNvDataT GPSCalibrationDB[];  

extern int16 SavedThresHigh1[HWD_NUM_SUPPORTED_BAND_CLASS];
extern int16 SavedThresLow1[HWD_NUM_SUPPORTED_BAND_CLASS];
extern int16 SavedThresHigh2[HWD_NUM_SUPPORTED_BAND_CLASS];
extern int16 SavedThresLow2[HWD_NUM_SUPPORTED_BAND_CLASS];

extern int16 SavedAuxThresHigh1[HWD_NUM_SUPPORTED_BAND_CLASS];
extern int16 SavedAuxThresLow1[HWD_NUM_SUPPORTED_BAND_CLASS];
extern int16 SavedAuxThresHigh2[HWD_NUM_SUPPORTED_BAND_CLASS];
extern int16 SavedAuxThresLow2[HWD_NUM_SUPPORTED_BAND_CLASS];

extern HwdTemperatureCalibParmT   TemperatureCalibrLine[];
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern HwdBattVoltageCalibParmT   BattVoltageCalibrLine[];
extern HwdBattChargerCalibParmT   BattChargerCalibrLine[];
extern DbmToAdcStepLinearRegionT  TxPwrDetectLine[HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_GAIN_POINTS_TX_PWR_DET];
extern DbmToAdcStepLinearRegionT  AuxTxPwrDetectLine[HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_GAIN_POINTS_TX_PWR_DET];
#else
extern HwdTxPwrDetCalibDataT  TxPwrDetectLine[HWD_NUM_SUPPORTED_BAND_CLASS];
#endif

/* ETS Aux Adc conversion request tracking variables */
extern bool HwdEtsTempRequest;
extern bool HwdEtsBattRequest;
extern bool HwdEtsTxPowerRequest;

extern int16 PpmPerRot[];

extern uint16 LowPersistenceCntAux, HighPersistenceCntAux;
extern uint16 LowPersistenceCntDiv, HighPersistenceCntDiv;
extern uint16 LowPersistenceCntMain, HighPersistenceCntMain;

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern NewHwdLineSegmentT   InlCorrectionLine[HWD_NUM_INL_SEGMENTS];
extern HwdRxAgcGainPrefT *HwdMainRxAgcGainPrefP[HWD_NUM_SUPPORTED_BAND_CLASS];
extern HwdRxAgcGainPrefT HwdMainRxAgcGainPref[HWD_NUM_SUPPORTED_BAND_CLASS];
extern HwdRxAgcGainPrefT HwdMainRxAgcGainPrefNoIMD[HWD_NUM_SUPPORTED_BAND_CLASS];
extern HwdRxAgcGainPrefT *HwdDivRxAgcGainPrefP[HWD_NUM_SUPPORTED_BAND_CLASS];
extern HwdRxAgcGainPrefT HwdDivRxAgcGainPref[HWD_NUM_SUPPORTED_BAND_CLASS];
extern HwdRxAgcGainPrefT HwdDivRxAgcGainPrefNoIMD[HWD_NUM_SUPPORTED_BAND_CLASS];


extern HwdRxAgcGainPrefT *HwdSecRxAgcGainPrefP[HWD_NUM_SUPPORTED_BAND_CLASS];
extern HwdRxAgcGainPrefT HwdSecRxAgcGainPref[HWD_NUM_SUPPORTED_BAND_CLASS];
extern HwdRxAgcGainPrefT HwdSecRxAgcGainPrefNoIMD[HWD_NUM_SUPPORTED_BAND_CLASS];
#endif

extern SysAirInterfaceT  HwdCalCurrentRfSystem[HWD_RF_MPA_MAX_PATH_NUM];
extern HwdCalConfigT     HwdCalCurrentConfig[SYS_MODE_MAX][HWD_RF_MPA_MAX_PATH_NUM];

extern uint16 HwdRfRxAssigned[SYS_MODE_MAX];
extern uint16 HwdRfTxAssigned[SYS_MODE_MAX];


#ifdef MTK_DEV_RF_TEST
extern void HwdRfCalDataInitTrace(void);
#endif

/*----------------------------------------------------------------------------
 Global Function Prototypes
----------------------------------------------------------------------------*/
/* calibration data base */
extern void   HwdCalibMainRfTranUpdate(HwdRfBandT RfBand, uint32 RfChannel, uint8 InterfaceType);
extern void   HwdCalibDivRfTranUpdate(HwdRfBandT RfBand, uint32 RfChannel, uint8 InterfaceType);

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern void   HwdCalibSecRfTranUpdate(HwdRfBandT RfBand, uint32 RfChannel, uint8 InterfaceType);
extern void   HwdCalibTempAdcProcess(uint16 TempAuxAdcReadVal, uint32 TxRfNum);
#else
// remove uint32 TxRfNum lator
extern void  HwdCalibTempAdcProcess(uint16 TempAdcReadVal, uint32 TxRfNum); 
extern int16  HwdCalibTempAdctoCelsius(uint16 TempAdcReadVal, bool AdcIncrease); 
#endif

extern void   HwdCalibBattAdcProcess(uint16 BattAuxAdcReadVal);

extern void   HwdCalibTempUpdate(HwdRfBandT RfBand, int16 TempCelsius, uint8 InterfaceType);

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern void   HwdCalibInitialize(HwdCalibInitMsgT *MsgP);
#endif
extern void   HwdCalibInit(void);

extern void   RfMainTranUpdate(HwdRfBandT RfBand, uint32 RfChannel);
extern void   RfDivTranUpdate(HwdRfBandT RfBand, uint32 RfChannel);

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern void   RfSecTranUpdate(HwdRfBandT RfBand, uint32 RfChannel);
extern void   RfTxAuxTranUpdate(HwdRfBandT RfBand, uint32 RfChannel, uint8 InterfaceType);
extern void   TxAgcMainCalParmsSend(bool NewBand, bool NewChannel, bool NewAdjustments);
extern void   TxAgcAuxCalParmsSend(bool NewBand, bool NewChannel, bool NewAdjustments);
extern void   HwdCalibInitNvram(HwdCalInitNvramMsgT *MsgP);
extern void   HwdInitializeNvramSection(void);
#else
extern void   TxAgcMainCalParmsSend(void);
#endif
extern void   RfTxMainTranUpdate(HwdRfBandT RfBand, uint32 RfChannel, uint8 InterfaceType);

extern void   SendMaxPwrAdjDb( void );
extern void   HwdUpdateTxPwrDetLimit(uint8 InterfaceType);
extern void   HwdAfcParmsSend(void);
extern void   HwdStartUpTimer(uint32 Timeout);
extern bool   HwdCalibrationMsgHandler(uint32 MsgId, void* MsgDataPtr, uint32 MsgSize);

extern void   HwdCalTxAgcThresholdUpdate(SysAirInterfaceT Mode, int16 ThresHigh1, int16 ThresLow1, int16 ThresHigh2, int16 ThresLow2);
extern void   HwdTxPwrDetectProcess(uint16 MeasuredADC, uint32 TxRfNum);
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern void   HwdAutoBattCalInit(void);
extern void   HwdAutoBattCalStart(HwdAuxAdcAutoCalT *MsgP);
extern void   HwdAutoBattCal(uint16 AuxAdcReadVal);
extern uint16 HwdRealStepToIdealProcess(uint16 inValue, uint8 xORy);
#endif
extern void   HwdAfcPpmPerRotInit(void);

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern void   HwdRxBankCalWrite(HwdRfBandT RfBand, HwdRxBankCalDataT *RxBankCalPtr);
extern void   HwdRxDivBankCalWrite(HwdRfBandT RfBand, HwdRxBankCalDataT *RxBankCalPtr);
#endif

extern void   RssiCorrectionTableSend( void );
extern void   HwdValTempReportAckProcess(void);
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
extern void   TxAgcTxPgaParmsSend(void);
#ifdef MTK_DEV_HW_SIM_RF
extern void   TxAgcTxPgaParmsSendHwSim(void);
#endif
extern void   SendMaxPwrAdjDb( void );
#endif

extern void   HwdValBattReportAckProcess(void);

extern void   HwdCalibBatteryUpdate(HwdRfBandT RfBand, int16 BatteryVolt);

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern void   HwdAutoCalBattPdmParmsProcess(HwdBatteryPdmCalibDataT* RxDataP);
extern void   HwdInlCorrectionProcess(void);
#endif

extern void   RxAgcCalParmsAdjustementSend(void);
extern void   TxAgcCalParmsAdjustementSend(void);

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern void RxAgcMainCalParmsSend(bool NewBand, bool NewChannel, bool NewAdjustments);
extern void RxAgcDivCalParmsSend(bool NewBand, bool NewChannel, bool NewAdjustments);
extern void RxAgcSecCalParmsSend(bool NewBand, bool NewChannel, bool NewAdjustments);
#endif

#ifdef SYS_OPTION_EVDO
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern void SendRmcTempAdjUpdate(bool MainAdj, int16 AdjDb);
extern void SendRmcFreqAdjUpdate(bool MainAdj, int16 *FreqAdjPtr);
#endif
extern void SendRcpAdjUpdate(int16 TxMaxPwr, int16 TxMinPwr, TxDbAdjustmentT *TxAdjPtr);
#endif

extern void  HwdCalUpdateTxAdjustments(SysAirInterfaceT ModeLocal);

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern uint16 HwdCalAuxAdcStepToMvProcess(uint16 AdcValue);
#endif

#if defined(MTK_CBP)&&(!defined(MTK_PLT_ON_PC))
extern void HwdCalibTxAgcTempAdjDataProcess(void *MsgP);
extern void HwdCalibTxAgcFreqAdjDataProcess(void *MsgP);
extern void HwdCalibTxPwrDetTblProcess(void *MsgDataPtr);
extern void HwdCalibTxPwrDetVsFreqProcess(void *MsgDataPtr);
extern void HwdCalibRxAgcMultiGainFreqAdjDataProcess(void *MsgP);
extern void HwdCalibRxAgcTempAdjDataProcess(void *MsgP);
extern void HwdCalibDivRxAgcMultiGainFreqAdjDataProcess(void *MsgP);
extern void HwdCalibDivRxAgcTempAdjDataProcess(void *MsgP);
#endif

#if defined(MTK_CBP)&&(!defined(MTK_PLT_ON_PC))&&(!defined(MTK_PLT_RF_ORIONC)||defined(MTK_DEV_HW_SIM_RF))

/*****************************************************************************

  FUNCTION NAME:   HwdCalibGetRfnvDataPtr

  DESCRIPTION:     Get RFNVRAM data variables pointer

  PARAMETERS:      void

  RETURNED VALUES: pointer of HwdCalibrParm

*****************************************************************************/
extern HwdCalibrParmT *HwdCalibGetRfnvDataPtr(void);

/*****************************************************************************

  FUNCTION NAME:   HwdCalibGetRfCompDataPtr

  DESCRIPTION:     Get RF compensation data variable pointer

  PARAMETERS:      void

  RETURNED VALUES: pointer of HwdCalibCompData

*****************************************************************************/
extern HwdCalibCompDataT *HwdCalibGetRfCompDataPtr(void);

/*****************************************************************************

  FUNCTION NAME:   HwdRxPathLossCompUpdate

  DESCRIPTION:     This function is used to interpolate RX path loss compensation
                   data with 2 dimesion.

  PARAMETERS:      rfRxPath    - RF logic path type
                   rfBand      - HwdRfBandT type
                   freqChanNum - frequency channel number
                   tempCelsius - current temperature

  RETURNED VALUES: void.

*****************************************************************************/
extern void HwdRxPathLossCompUpdate(HwdRfRxEnumT rfRxPath, HwdRfBandT rfBand, uint16 freqChanNum, int16 tempCelsius);

/*****************************************************************************

  FUNCTION NAME:   HwdTxPaGainCompUpdate

  DESCRIPTION:     This function is used to interpolate the paGain compensation
                   data with 2 dimesion.

  PARAMETERS:      rfBand      - HwdRfBandT type
                   freqChanNum - frequency channel number
                   tempCelsius - current temperature
                   ratType     - system RAT type

  RETURNED VALUES: void.

*****************************************************************************/
extern void HwdTxPaGainCompUpdate(HwdRfBandT rfBand, uint16 freqChanNum, int16 tempCelsius, uint16 ratType);

/*****************************************************************************

  FUNCTION NAME:   HwdTxDetCplCompUpdate

  DESCRIPTION:     This function is used to interpolate the coupler loss compensation
                   data with 2 dimesion.

  PARAMETERS:      rfBand      - HwdRfBandT type
                   freqChanNum - frequency channel number
                   tempCelsius - current temperature
                   ratType     - system RAT type

  RETURNED VALUES: void.

*****************************************************************************/
extern void HwdTxDetCplCompUpdate(HwdRfBandT rfBand, uint16 freqChanNum, int16 tempCelsius, uint16 ratType);


/*****************************************************************************

  FUNCTION NAME:   HwdCalib1xRttTxPaGainCompDataProcess

  DESCRIPTION:     Processes TX paGain frequency and temperature compensation
                   data from DBM

  PARAMETERS:      pMsg - pointer to message

  RETURNED VALUES: None

*****************************************************************************/
extern void HwdCalib1xRttTxPaGainCompDataProcess(void *pMsg);

/*****************************************************************************

  FUNCTION NAME:   HwdCalibEvdoTxPwrPaGainCompDataProcess

  DESCRIPTION:     Processes TX paGain frequency and temperature compensation
                   data from DBM

  PARAMETERS:      pMsg - pointer to message

  RETURNED VALUES: None

*****************************************************************************/
extern void HwdCalibEvdoTxPaGainCompDataProcess(void *pMsg);

/*****************************************************************************

  FUNCTION NAME:   HwdCalibTxDetCplDataProcess

  DESCRIPTION:     Processes TX DET coupler loss
                   data from DBM

  PARAMETERS:      pMsg - pointer to message

  RETURNED VALUES: None

*****************************************************************************/
extern void HwdCalibTxDetCplDataProcess(void *pMsg);

/*****************************************************************************

  FUNCTION NAME:   HwdCalib1xRttTxPdetCplCompDataProcess

  DESCRIPTION:     Processes TX DET coupler loss compensatoin
                   data from DBM

  PARAMETERS:      pMsg - pointer to message

  RETURNED VALUES: None

*****************************************************************************/
extern void HwdCalib1xRttTxDetCplCompDataProcess(void *pMsg);

/*****************************************************************************

  FUNCTION NAME:   HwdCalibEvdoTxPdetCplCompDataProcess

  DESCRIPTION:     Processes TX DET coupler loss compensatoin
                   data from DBM

  PARAMETERS:      pMsg - pointer to message

  RETURNED VALUES: None

*****************************************************************************/
extern void HwdCalibEvdoTxDetCplCompDataProcess(void *pMsg);

/*****************************************************************************

  FUNCTION NAME:   HwdCalibRxPathLossHpmDataProcess

  DESCRIPTION:     Processes Rx path loss high power mode
                   data from DBM

  PARAMETERS:      pMsg - pointer to message

  RETURNED VALUES: None

*****************************************************************************/
extern void HwdCalibRxPathLossHpmDataProcess(void *pMsg, HwdRfRxEnumT rfRxPath, HwdRfBandT rfBand);

/*****************************************************************************

  FUNCTION NAME:   HwdCalibRxPathLossLpmDataProcess

  DESCRIPTION:     Processes Rx path loss low power mode
                   data from DBM

  PARAMETERS:      pMsg - pointer to message

  RETURNED VALUES: None

*****************************************************************************/
extern void HwdCalibRxPathLossLpmDataProcess(void *pMsg, HwdRfRxEnumT rfRxPath, HwdRfBandT rfBand);

/*****************************************************************************

  FUNCTION NAME:   HwdCalibMainRxPathLossHpmDataProcess

  DESCRIPTION:     Processes Rx path loss
                   data from DBM

  PARAMETERS:      pMsg - pointer to message

  RETURNED VALUES: None

*****************************************************************************/
extern void HwdCalibMainRxPathLossHpmDataProcess(void *pMsg);

/*****************************************************************************

  FUNCTION NAME:   HwdCalibMainRxPathLossLpmDataProcess

  DESCRIPTION:     Processes Rx path loss
                   data from DBM

  PARAMETERS:      pMsg - pointer to message

  RETURNED VALUES: None

*****************************************************************************/
extern void HwdCalibMainRxPathLossLpmDataProcess(void *pMsg);

/*****************************************************************************

  FUNCTION NAME:   HwdCalibDivRxPathLossHpmDataProcess

  DESCRIPTION:     Processes Rx path loss
                   data from DBM

  PARAMETERS:      pMsg - pointer to message

  RETURNED VALUES: None

*****************************************************************************/
extern void HwdCalibDivRxPathLossHpmDataProcess(void *pMsg);

/*****************************************************************************

  FUNCTION NAME:   HwdCalibDivRxPathLossLpmDataProcess

  DESCRIPTION:     Processes Rx path loss
                   data from DBM

  PARAMETERS:      pMsg - pointer to message

  RETURNED VALUES: None

*****************************************************************************/
extern void HwdCalibDivRxPathLossLpmDataProcess(void *pMsg);

/*****************************************************************************

  FUNCTION NAME:   HwdCalibShdrRxPathLossHpmDataProcess

  DESCRIPTION:     Processes Rx path loss
                   data from DBM

  PARAMETERS:      pMsg - pointer to message

  RETURNED VALUES: None

*****************************************************************************/
extern void HwdCalibShdrRxPathLossHpmDataProcess(void *pMsg);

/*****************************************************************************

  FUNCTION NAME:   HwdCalibMainRxPathLossLpmDataProcess

  DESCRIPTION:     Processes Rx path loss
                   data from DBM

  PARAMETERS:      pMsg - pointer to message

  RETURNED VALUES: None

*****************************************************************************/
extern void HwdCalibShdrRxPathLossLpmDataProcess(void *pMsg);

/*****************************************************************************

  FUNCTION NAME:   HwdTxPocParmsSend()

  DESCRIPTION:     This function is used to send the TX POC table to DSPM
                   when band is changed.

  PARAMETERS:      void.

  RETURNED VALUES: void

*****************************************************************************/
extern void HwdTxPocParmsSend(void);

/*****************************************************************************

  FUNCTION NAME:   HwdDetPocParmsSend()

  DESCRIPTION:     This function is used to send the DET POC table to DSPM
                   when band is changed.

  PARAMETERS:      void.

  RETURNED VALUES: void

*****************************************************************************/
extern void HwdDetPocParmsSend(void);

/*****************************************************************************

  FUNCTION NAME:   HwdCalibAfcCompParmsProcess()

  DESCRIPTION:     This function is used to process AFC compensation data.

  PARAMETERS:      void.

  RETURNED VALUES: void

*****************************************************************************/
extern void HwdCalibAfcCompParmsProcess(void *MsgP);

extern void HwdCalibAGpsGrpDlyProcess(void *MsgP);

extern void HwdCalibTxPwrBackOffProcess(void *MsgP);

extern void HwdCcCalibParamProcess(const void **d);

#if defined(__SAR_TX_POWER_BACKOFF_SUPPORT__)
/*****************************************************************************

  FUNCTION NAME:   HwdCalibSArTxPowerOffsetDataProcess

  DESCRIPTION:     Processes Sar Tx power offset.

  PARAMETERS:      pMsg - pointer to message

  RETURNED VALUES: None

*****************************************************************************/
extern void HwdCalibSarTxPowerOffsetDataProcess(void *MsgP);


/*****************************************************************************

  FUNCTION NAME:   HwdRfGetTxPowerOffset
  
  DESCRIPTION:     Get the tx power offset table index from share memory

  PARAMETERS:      void
  
  RETURNED VALUES: tx power offset.

*****************************************************************************/
extern int16 HwdCalibGetSarTxPowerOffset(HwdRfBandT rfBand, uint16 rfChannel, uint16 tableIdx, uint16 antennaIdx, SysAirInterfaceT txmode);

/*****************************************************************************

  FUNCTION NAME:   SendSarPwrOffset
  
  DESCRIPTION:     Send the tx power offset value to dsp

  PARAMETERS:      offset - value of offset
  
  RETURNED VALUES: void.

*****************************************************************************/
extern void SendSarPwrOffset( int16 offset );
#endif
#if defined(__TX_POWER_OFFSET_SUPPORT__)

/*****************************************************************************

  FUNCTION NAME:   HwdCalibTxPowerOffsetDataProcess

  DESCRIPTION:     Processes Tx power offset.

  PARAMETERS:      pMsg - pointer to message

  RETURNED VALUES: None

*****************************************************************************/
extern void HwdCalibTxPowerOffsetDataProcess(void *MsgP);


/*****************************************************************************

  FUNCTION NAME:   HwdRfGetTxPowerOffset
  
  DESCRIPTION:     Get the tx power offset table index from share memory

  PARAMETERS:      void
  
  RETURNED VALUES: tx power offset.

*****************************************************************************/
extern int16 HwdCalibGetTxPowerOffset(HwdRfBandT rfBand, uint16 rfChannel, uint16 tableIdx);

#endif

#endif


/*****************************************************************************
* End of File
*****************************************************************************/
#endif

/**Log information: \main\Trophy\Trophy_ylxiao_href22033\1 2013-03-18 14:14:44 GMT ylxiao
** HREF#22033, merge 4.6.0**/
/**Log information: \main\Trophy\1 2013-03-19 05:19:00 GMT hzhang
** HREF#22033 to merge 0.4.6 code from SD.**/
