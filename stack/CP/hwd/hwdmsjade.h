/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*******************************************************************************
 *
 * Filename:
 * ---------
 * hwdmsjade.h
 *
 * Project:
 * --------
 * C2K
 *
 * Description:
 * ------------
 * Header file containing typedefs and definitions pertaining
 * to the RF infrastructure.
 *
 * Author:
 * -------
 * 
 *
 *==============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 *
 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *==============================================================================
 *******************************************************************************/
#include "hwdtxdfe.h"

/*************** Register definition *****************
Format:
Register Address
    Field0
    Field1
    ...
******************************************************/
#define BASE_ADDR               (0x0B41A000)
#define DIG_CON2                (BASE_ADDR + 0x008)
    #define BBTX_C2K_COMP_DATAPATH_SEL_NORMAL   (0 << 13)
    #define BBTX_C2K_COMP_DATAPATH_SEL_SINEGEN  (1 << 13)
    #define BBTX_C2K_COMP_TEST_PATTERN_SEL_SINGEN (0 << 12)
#define DIG_CON3                (BASE_ADDR + 0x00C)
    #define APC_C2K_SEL_ZERO        (0)
    #define APC_C2K_SEL_APC1        (1)
    #define APC_C2K_SEL_APC2        (2)
    #define APC_C2K_SEL_BOTH_APC    (3)
#define DIG_CONA5               (BASE_ADDR + 0x294)
    #define RCCAL_LTE_SEL_CAP(dAT)  (((dAT) & 0x3F) << 8)
    #define RCCAL_LTE_SEL_CAP_SW    (1 << 7)
    #define RCCAL_LTE_OFFSET_CAP(dAT)   ((dAT) & 0x3F)
#define DIG_CONA9               (BASE_ADDR + 0x2A4)
    #define DBBRX2_SWAP_IQ          (0 << 0)
    #define DBBRX2_SWAP_Q           (0 << 1)
    #define DBBRX2_SWAP_I           (0 << 2)
    #define PBBRX2_SWAP_IQ          (0 << 3)
    #define PBBRX2_SWAP_Q           (0 << 4)
    #define PBBRX2_SWAP_I           (0 << 5)
    #define DBBRX1_SWAP_IQ          (0 << 6)
    #define DBBRX1_SWAP_Q           (0 << 7)
    #define DBBRX1_SWAP_I           (0 << 8)
    #define PBBRX1_SWAP_IQ          (0 << 9)
    #define PBBRX1_SWAP_Q           (0 << 10)
    #define PBBRX1_SWAP_I           (0 << 11)
    #define TX_SWAP_IQ              (0 << 12)
    #define TX_SWAP_Q               (0 << 13)
    #define TX_SWAP_I               (0 << 14)
    #define ET_SWAP_I               (0 << 15)
#define DIG_CONAA               (BASE_ADDR + 0x2A8)
    #define RX1_ADC_SW_CTL          (1 << 7)
    #define RX2_ADC_SW_CTL          (1 << 6)
    #define RX1_ADC_SW_EN           (1 << 3)
    #define RX2_ADC_SW_EN           (1 << 2)
#define DIG_CONAB               (BASE_ADDR + 0x2AC)
    #define TXADC_SW_EN_SEL         (1 << 5)
    #define TXADC_SW_EN             (1 << 4)
    #define TXCIC_SW_EN_SEL         (1 << 1)
    #define TXCIC_SW_EN             (1 << 0)
#define DIG_CONBE               (BASE_ADDR + 0x2F8)
    #define CLK_26M_DIS             (0 << 15)
    #define CLK_26M_EN              (1 << 15)
    #define SEL_PBBRX1              (1 << 0)
    #define SEL_DBBRX1              (1 << 1)
    #define SEL_PBBRX2              (1 << 2)
    #define SEL_DBBRX2              (1 << 3)
    #define SEL_MASK                (0xf)
    #define RX1_OUT(sEL)            ((sEL) << 4)
    #define RX2_OUT(sEL)            ((sEL) << 0)
#define DIG_CONBF               (BASE_ADDR + 0x2FC)
    #define DPDADC_EN_CTL           (1 << 1)
    #define DPDADC_SW_EN            (1 << 0)
#define RSV_CONB                (BASE_ADDR + 0x42C)
    #define C2K_RCCAL_PATH_SWITCH_EN    (1 << 7)
    #define C2K_RCCAL_MASK              (0x3F)
    #define C2K_RCCAL_VAL(vAL)          (((vAL) & C2K_RCCAL_MASK) << 0)
#define DPDADC_CON3             (BASE_ADDR + 0x4AC)

#define LTEPBBRX1_CON1          (BASE_ADDR + 0x704)
#define LTEPBBRX1_CON13         (BASE_ADDR + 0x74C)
#define LTEPBBRX1_CON2E         (BASE_ADDR + 0x7B8)
#define LTEDBBRX1_CON1          (BASE_ADDR + 0x804)
#define LTEDBBRX1_CON13         (BASE_ADDR + 0x84C)
#define LTEDBBRX1_CON2E         (BASE_ADDR + 0x8B8)
#define LTEPBBRX2_CON1          (BASE_ADDR + 0x904)
#define LTEPBBRX2_CON13         (BASE_ADDR + 0x94C)
#define LTEPBBRX2_CON2E         (BASE_ADDR + 0x9B8)
#define LTEDBBRX2_CON1          (BASE_ADDR + 0xA04)
#define LTEDBBRX2_CON13         (BASE_ADDR + 0xA4C)
#define LTEDBBRX2_CON2E         (BASE_ADDR + 0xAB8)

#define RSV2_CON5               (BASE_ADDR + 0xB14)
#if (SYS_BOARD == SB_JADE)
    #define DA_LTEBBTX_SEQ_EN           (1 << 0)
    #define DA_LTEBBTX_SEQ_HW_CLR_EN    (1 << 1)
    #define DA_LTEBBTX_SEQ_EN_SEL_SW    (1 << 3)
#else
    #define DA_LTEBBTX_SEQ_EN           (1 << 4)
    #define DA_LTEBBTX_SEQ_EN_SEL_SW    (1 << 9)
#endif

#define DPDADC_CON3             (BASE_ADDR + 0x4AC)
#define DPDADC_CON9             (BASE_ADDR + 0x4C4)
#define DPDADC_CONC             (BASE_ADDR + 0x4D0)

#define LTEBBTX_CON0            (BASE_ADDR + 0x600)
#if (SYS_BOARD > SB_JADE)
#define LTEBBTX_CON4            (BASE_ADDR + 0x610)
#define LTEBBTX_CON8            (BASE_ADDR + 0x620)
#define LTEBBTX_CON13           (BASE_ADDR + 0x64C)
#define LTEBBTX_CON14           (BASE_ADDR + 0x650)
#define LTEPBBRX1_CON28         (BASE_ADDR + 0x7A0)
#define LTEDBBRX1_CON28         (BASE_ADDR + 0x8A0)
#define LTEPBBRX2_CON28         (BASE_ADDR + 0x9A0)
#define LTEDBBRX2_CON28         (BASE_ADDR + 0XAA0)
#endif

/* For seq en workaround */
#define DIG_CONB                (BASE_ADDR + 0x02C)
#define RSV_CON8                (BASE_ADDR + 0x420)
#define RSV_CON9                (BASE_ADDR + 0x424)
#define DIG_CON8E               (BASE_ADDR + 0x238)
#define RSV_CONA                (BASE_ADDR + 0x428)

/* TOPSM register definition */
#define TOPSM_BASE_ADDR         (0x0B41B000)
#define SM_PWR_CON0             (TOPSM_BASE_ADDR + 0x000)
#define SM_PWR_PER0             (TOPSM_BASE_ADDR + 0x0C0)
#define SM_PWR_PER1             (TOPSM_BASE_ADDR + 0x0C4)
#define SM_PWR_SW_CTRL_SEL      (TOPSM_BASE_ADDR + 0x0CC)
#define SM_TMR_REQ_MASK         (TOPSM_BASE_ADDR + 0x100)
#define SM_TMR_SYSCLK_MASK      (TOPSM_BASE_ADDR + 0x108)
#define SM_TMR_PLL_MASK0        (TOPSM_BASE_ADDR + 0x120)
#define SM_TMR_PWR_MASK0        (TOPSM_BASE_ADDR + 0x140)
#define SM_TMR_MAS_TRIG_MASK    (TOPSM_BASE_ADDR + 0x160)
#define SM_TMR_TIMER_TRIG_MASK  (TOPSM_BASE_ADDR + 0x170)
#define SM_TMR_CLIENT_ACT_MASK  (TOPSM_BASE_ADDR + 0x180)
#define SM_SLV_REQ_MASK         (TOPSM_BASE_ADDR + 0x200)
#define SM_SLV_PLL_MASK0        (TOPSM_BASE_ADDR + 0x220)
#define SM_SLV_PWR_MASK0        (TOPSM_BASE_ADDR + 0x240)
#define SM_SLV_MAS_TRIG_MASK    (TOPSM_BASE_ADDR + 0x260)
#define SM_SLV_TIMER_TRIG_MASK  (TOPSM_BASE_ADDR + 0x270)
#define SM_SLV_CLIENT_ACT_MASK  (TOPSM_BASE_ADDR + 0x280)
#define SM_DBG_REQ_MASK         (TOPSM_BASE_ADDR + 0x300)
#define SM_DBG_SYSCLK_MASK      (TOPSM_BASE_ADDR + 0x308)
#define SM_DBG_PLL_MASK0        (TOPSM_BASE_ADDR + 0x320)
#define SM_DBG_PWR_MASK0        (TOPSM_BASE_ADDR + 0x340)
#define SM_DBG_MAS_TRIG_MASK    (TOPSM_BASE_ADDR + 0x360)
#define SM_DBG_TIMER_TRIG_MASK  (TOPSM_BASE_ADDR + 0x370)
#define SM_DBG_CLIENT_ACT_MASK  (TOPSM_BASE_ADDR + 0x380)

#define SM_CLK_SETTLE           (TOPSM_BASE_ADDR + 0x400)

#define SM_TIMER_TRIG_SETTLE    (TOPSM_BASE_ADDR + 0x410)
#define SM_MAS_TRIG_MAX_SETTLE  (TOPSM_BASE_ADDR + 0x418)
#define SM_MAS_TRIG_GRP_SETTLE  (TOPSM_BASE_ADDR + 0x420)
#define SM_MAS_TRIG_GRP_SAL     (TOPSM_BASE_ADDR + 0x430)
#define SM_MAS_TRIG_SEL         (TOPSM_BASE_ADDR + 0x440)
#define SM_SLV_SW_TRIG          (TOPSM_BASE_ADDR + 0x450)
#define SM_DBG_SW_TRIG          (TOPSM_BASE_ADDR + 0x458)


/* Sine Gen regsiter definition */
#define DIG_CON90    (HWD_MS_BASE_ADDR+0x240)   /* Sine wave clock control */
#define DIG_CON91    (HWD_MS_BASE_ADDR+0x244)   /* Sine wave enable and fix value control */
#define DIG_CON92    (HWD_MS_BASE_ADDR+0x248)   /* Sine wave output fix value of I path */
#define DIG_CON93    (HWD_MS_BASE_ADDR+0x24C)   /* Sine wave output fix value of Q path */
#define DIG_CON94    (HWD_MS_BASE_ADDR+0x250)   /* Sine wave clock div and mode */
#define DIG_CON95    (HWD_MS_BASE_ADDR+0x254)   /* Sine wave clock division parameter on I path*/
#define DIG_CON96    (HWD_MS_BASE_ADDR+0x258)   /* Sine wave clock division parameter on Q path */
#define DIG_CON97    (HWD_MS_BASE_ADDR+0x25C)   /* Sine wave gain. i.e. full scale voltage */
#define DIG_CON98    (HWD_MS_BASE_ADDR+0x260)   /* Sine wave gain mode */
#define DIG_CON99    (HWD_MS_BASE_ADDR+0x264)   /* Sine wave frequency step on I path */
#define DIG_CON9A    (HWD_MS_BASE_ADDR+0x268)   /* Sine wave frequency step on Q path */
#define DIG_CON9B    (HWD_MS_BASE_ADDR+0x26C)   /* Sine wave phase on I path */
#define DIG_CON9C    (HWD_MS_BASE_ADDR+0x270)   /* Sine wave phase on Q path */

#ifndef MTK_DEV_HW_SIM_RF
#define MsWriteJ(aDDR, dATA)            HwdWrite(aDDR, dATA)
#define MsReadJ(aDDR)                   HwdRead(aDDR)
#define MsWriteBitJ(aDDR, mASK, dATA)   MsWriteJ(aDDR, \
                    (MsReadJ(aDDR) & ~(mASK)) | ((dATA) & (mASK)))
#define MsSetBitJ(aDDR, mASK)           HwdSetBit16(aDDR, mASK)

#define MsClrBitJ(aDDR, mASK)           HwdClearBit16(aDDR, mASK)


#define M_HwdMsAbbInit()                HwdMsAbbInitJade()
#define M_HwdMsTxDacEnable()            HwdMsTxDacEnableJade()
#define M_HwdMsTxDacDisable()           HwdMsTxDacDisableJade()
#define M_HwdMsRxAdcEnable(pARAM)       HwdMsRxAdcEnableJade(pARAM)
#define M_HwdMsRxAdcDisable(pARAM)      HwdMsRxAdcDisableJade(pARAM)
#define M_HwdMsRestore()                HwdMsRestoreJade()
#define M_HwdMsSineCfg(pARAM)           HwdTxSineCfgJade(pARAM)
#define M_HwdMsSineBoCfg(pARAM)
#define M_HwdMsSineGen(pARAM)           HwdTxSineGenJade(pARAM)
#define M_HwdMsAltSineCfg(pARAM)        HwdMsSineCfgJade(pARAM)
#define M_HwdMsAltSineGen(pARAM)        HwdMsSineGenJade(pARAM)
#define M_HwdMsRst()                    HwdMsRstJade()
#define M_HwdMsCal()
#define M_HwdMsApcDacInit()             HwdMsApcDacInit()
#define M_HwdMsApcDacOn(pARAM)          HwdMsApcDacOn(pARAM)
#define M_HwdMsApcDacOff(pARAM)         HwdMsApcDacOff(pARAM)
#define M_HwdMsApcDacWriteImmed(pARAM)  HwdMsApcDacWriteImmed(pARAM)
#define M_HwdMsApcDacWriteDelayLoad(pARAM1, pARAM2)  HwdMsApcDacWriteDelayLoad(pARAM1, pARAM2)
#define M_HwdMsTxDacPostCtrl()          HwdMsTxDacResetAFifo()
#endif /* MTK_DEV_HW_SIM_RF */

#if (defined MTK_DEV_DUMP_REG)
#define M_HwdMsRegLogRdAll()           HwdMsRegLogRdAllJade()
extern void HwdMsRegLogRdAllJade(void);
#endif

extern void HwdMsAbbInitJade(void);
extern void HwdMsTxDacEnableJade(void);
extern void HwdMsTxDacDisableJade(void);
extern void HwdMsRxAdcEnableJade(HwdRfRxEnumT rx);
extern void HwdMsRxAdcDisableJade(HwdRfRxEnumT rx);
extern void HwdMsRestoreJade(void);
extern void HwdMsSineCfgJade(HwdMsSinCfgT *cfgPtr);
extern void HwdMsSineGenJade(HwdMsSinGenT *sinGenPtr);
extern void HwdMsRstJade(void);
extern void HwdMsTxDacResetAFifo(void);

