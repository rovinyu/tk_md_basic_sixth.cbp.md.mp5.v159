/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*******************************************************************************
 *
 * Filename:
 * ---------
 * hwdtaspublic.c
 *
 * Project:
 * --------
 * C2K
 *
 * Description:
 * ------------
 * Implementation file pertaining
 * to the RF infrastructure.
 *
 * Author:
 * -------
 * 
 *
 *==============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 *
 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *==============================================================================
 *******************************************************************************/

#ifndef _HWDTASPUBLIC_H_
#define _HWDTASPUBLIC_H_

/** TAS control parameters */
#define M_TAS_BM(oFFSET, vAL) (((vAL) & 0x1) << (oFFSET))

#if defined(__DYNAMIC_ANTENNA_TUNING__)||defined(__SAR_TX_POWER_BACKOFF_SUPPORT__)
#define C2K_DAT_SCENARIO_DEFAULT         (-1)
#define C2K_DAT_CAT_B_ROUTE_NUM          10
#define C2K_DAT_MAX_STATE_NUM            8
#define C2K_DAT_MAX_CAT_A_ROUTE_NUM      10
#define C2K_DAT_MAX_CAT_B_ROUTE_NUM      40  
#define C2K_DAT_MAX_FE_ROUTE_NUM         HWD_RF_BAND_MAX
#define C2K_DAT_MIPI_TABLE_NULL          (0xFF)
#define C2K_DAT_FE_NULL                  (0xFF)
#define C2K_SAR_SCENARIO_DEFAULT         (-1)
#define C2K_SAR_MAX_STATE_NUM            9


#endif

typedef enum
{
    TAS_EN = 0,
    TAS_TEST_SIM_EN,
    TAS_SIM_INSERTED,
    TAS_IS_TEST_SIM,
    TAS_FORCE_EN,

    TAS_BM_OFS_MAX
} HwdRfTasBitMapOffsetT;

typedef enum
{
    TAS_IDX_ANT0 = 0,
    TAS_IDX_ANT1,
    TAS_IDX_ANT2,
    TAS_IDX_FORCE,
    TAS_IDX_DEFAULT,
    TAS_IDX_DEFAULT_ON_TEST_SIM,

    TAS_TBL_IDX_MAX
} HwdRfTasTblInitIdxT;

typedef struct
{
    int32 tasBMMask;
    int32 tasBMData;
    int32 initTblIndex;
    int32 tasSwitchEn;
} HwdRfTasTblEtyT;

#if defined(__DYNAMIC_ANTENNA_TUNING__)||defined(__SAR_TX_POWER_BACKOFF_SUPPORT__)
typedef enum
{
  DAT,
  SWTP
}HwdApSensorRelativeFeature;

typedef enum
{
   C2K_DAT_CAT_A_MIPI_TABLE_CONFIG_IDX0,
   C2K_DAT_CAT_A_MIPI_TABLE_CONFIG_IDX1,
   C2K_DAT_CAT_A_MIPI_TABLE_CONFIG_IDX2,
   C2K_DAT_CAT_A_MIPI_TABLE_CONFIG_IDX3,
   C2K_DAT_CAT_A_MIPI_TABLE_CONFIG_IDX4,
   C2K_DAT_CAT_A_MIPI_TABLE_CONFIG_IDX5,
   C2K_DAT_CAT_A_MIPI_TABLE_CONFIG_IDX6,
   C2K_DAT_CAT_A_MIPI_TABLE_CONFIG_IDX7,
   C2K_DAT_CAT_A_MIPI_TABLE_CONFIG_IDX8,
   C2K_DAT_CAT_A_MIPI_TABLE_CONFIG_IDX9,
   C2K_DAT_CAT_A_MIPI_TABLE_NULL = C2K_DAT_MIPI_TABLE_NULL,
}C2K_CUSTOM_DAT_CAT_A_MIPI_TBL_IDX_E;


typedef enum
{
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX0,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX1,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX2,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX3,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX4,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX5,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX6,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX7,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX8,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX9,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX10,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX11,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX12,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX13,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX14,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX15,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX16,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX17,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX18,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX19,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX20,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX21,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX22,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX23,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX24,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX25,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX26,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX27,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX28,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX29,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX30,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX31,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX32,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX33,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX34,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX35,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX36,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX37,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX38,
   C2K_DAT_CAT_B_MIPI_TABLE_CONFIG_IDX39,
   C2K_DAT_CAT_B_MIPI_TABLE_NULL = C2K_DAT_MIPI_TABLE_NULL,
}C2K_CUSTOM_DAT_CAT_B_MIPI_TBL_IDX_E;

typedef enum
{
   C2K_DAT_CAT_A_CONFIG_IDX0,
   C2K_DAT_CAT_A_CONFIG_IDX1,
   C2K_DAT_CAT_A_CONFIG_IDX2,
   C2K_DAT_CAT_A_CONFIG_IDX3,
   C2K_DAT_CAT_A_CONFIG_IDX4,
   C2K_DAT_CAT_A_CONFIG_IDX5,
   C2K_DAT_CAT_A_CONFIG_IDX6,
   C2K_DAT_CAT_A_CONFIG_IDX7,
   C2K_DAT_CAT_A_CONFIG_IDX8,
   C2K_DAT_CAT_A_CONFIG_IDX9,
   C2K_DAT_CAT_A_NULL = C2K_DAT_FE_NULL,
}C2K_CUSTOM_DAT_CAT_A_IDX_E;

typedef enum
{
   C2K_DAT_CAT_B_CONFIG_IDX0,
   C2K_DAT_CAT_B_CONFIG_IDX1,
   C2K_DAT_CAT_B_CONFIG_IDX2,
   C2K_DAT_CAT_B_CONFIG_IDX3,
   C2K_DAT_CAT_B_CONFIG_IDX4,
   C2K_DAT_CAT_B_CONFIG_IDX5,
   C2K_DAT_CAT_B_CONFIG_IDX6,
   C2K_DAT_CAT_B_CONFIG_IDX7,
   C2K_DAT_CAT_B_CONFIG_IDX8,
   C2K_DAT_CAT_B_CONFIG_IDX9,
   C2K_DAT_CAT_B_CONFIG_IDX10,
   C2K_DAT_CAT_B_CONFIG_IDX11,
   C2K_DAT_CAT_B_CONFIG_IDX12,
   C2K_DAT_CAT_B_CONFIG_IDX13,
   C2K_DAT_CAT_B_CONFIG_IDX14,
   C2K_DAT_CAT_B_CONFIG_IDX15,
   C2K_DAT_CAT_B_CONFIG_IDX16,
   C2K_DAT_CAT_B_CONFIG_IDX17,
   C2K_DAT_CAT_B_CONFIG_IDX18,
   C2K_DAT_CAT_B_CONFIG_IDX19,
   C2K_DAT_CAT_B_CONFIG_IDX20,
   C2K_DAT_CAT_B_CONFIG_IDX21,
   C2K_DAT_CAT_B_CONFIG_IDX22,
   C2K_DAT_CAT_B_CONFIG_IDX23,
   C2K_DAT_CAT_B_CONFIG_IDX24,
   C2K_DAT_CAT_B_CONFIG_IDX25,
   C2K_DAT_CAT_B_CONFIG_IDX26,
   C2K_DAT_CAT_B_CONFIG_IDX27,
   C2K_DAT_CAT_B_CONFIG_IDX28,
   C2K_DAT_CAT_B_CONFIG_IDX29,
   C2K_DAT_CAT_B_CONFIG_IDX30,
   C2K_DAT_CAT_B_CONFIG_IDX31,
   C2K_DAT_CAT_B_CONFIG_IDX32,
   C2K_DAT_CAT_B_CONFIG_IDX33,
   C2K_DAT_CAT_B_CONFIG_IDX34,
   C2K_DAT_CAT_B_CONFIG_IDX35,
   C2K_DAT_CAT_B_CONFIG_IDX36,
   C2K_DAT_CAT_B_CONFIG_IDX37,
   C2K_DAT_CAT_B_CONFIG_IDX38,
   C2K_DAT_CAT_B_CONFIG_IDX39,
   C2K_DAT_CAT_B_NULL = C2K_DAT_FE_NULL,
}C2K_CUSTOM_DAT_CAT_B_IDX_E;

typedef enum
{
   C2K_DAT_STATE0,
   C2K_DAT_STATE1,
   C2K_DAT_STATE2,
   C2K_DAT_STATE3,
   C2K_DAT_STATE4,
   C2K_DAT_STATE5,
   C2K_DAT_STATE6,
   C2K_DAT_STATE7,
   C2K_DAT_STATE_NULL,
}C2K_CUSTOM_DAT_STATE_E;
   
typedef struct
{
   kal_uint32   cat_a_route_num;
   kal_uint32   cat_b_route_num;
}C2K_CUSTOM_DAT_FE_ROUTE_MAP_T;


typedef struct
{
   C2K_CUSTOM_DAT_FE_ROUTE_MAP_T     dat_fe_setting[C2K_DAT_MAX_STATE_NUM];
}C2K_CUSTOM_DAT_ROUTE_SETTING_T;   

typedef struct
{
   bool   feature_enable;
   C2K_CUSTOM_DAT_ROUTE_SETTING_T    c2k_dat_fe_route_db[HWD_RF_BAND_MAX];
}C2K_CUSTOM_DAT_FE_ROUTE_DATABASE_T;   

typedef struct
{
   kal_uint32                     bpi_mask;
   kal_uint32                     bpi_value;
   C2K_CUSTOM_DAT_CAT_A_MIPI_TBL_IDX_E dat_mipi_table_index;
}C2K_CUSTOM_DAT_CAT_A_FE_SETTING_T;

typedef struct
{
   kal_uint32                     bpi_mask;
   kal_uint32                     bpi_value;
   C2K_CUSTOM_DAT_CAT_B_MIPI_TBL_IDX_E dat_mipi_table_index;
}C2K_CUSTOM_DAT_CAT_B_FE_SETTING_T;

typedef struct
{
   C2K_CUSTOM_DAT_CAT_A_FE_SETTING_T   dat_cat_a_fe_route[C2K_DAT_MAX_CAT_A_ROUTE_NUM];
}C2K_CUSTOM_DAT_FE_CAT_A_T;

typedef struct
{
   C2K_CUSTOM_DAT_CAT_B_FE_SETTING_T   dat_cat_b_fe_route[C2K_DAT_MAX_CAT_B_ROUTE_NUM];
}C2K_CUSTOM_DAT_FE_CAT_B_T;


typedef struct
{
   C2K_CUSTOM_DAT_FE_CAT_A_T   dat_cat_a_fe_db;
   C2K_CUSTOM_DAT_FE_CAT_B_T   dat_cat_b_fe_db;
}C2K_CUSTOM_DAT_FE_DATABASE_T;

typedef struct
{
   const C2K_CUSTOM_DAT_FE_ROUTE_DATABASE_T  *dat_fe_route;
}HwdCustomDynamicDatRoute;

typedef struct
{
   const C2K_CUSTOM_DAT_FE_CAT_A_T   *dat_cat_a_fe_db;
   const C2K_CUSTOM_DAT_FE_CAT_B_T   *dat_cat_b_fe_db;
}HwdCustomDynamicDatBpiData;

typedef struct
{
  uint32 bpimask;
  uint32 bpidata;
}HwdCustomDatBpiSetting;
#endif


extern bool HwdTasInitIndexIsSet;

extern bool HwdTasSetInitAnt(HwdRfBandT band);
extern void HwdTasRecoverAnt(HwdRfBandT band);
extern bool HwdTasInitIndexGetFlag(HwdRfBandT band);
extern void HwdTasInitIndexSetFlag(HwdRfBandT band, bool flag);
extern bool HwdTasOccupy(void);
extern void HwdTasRelease(void);
extern bool HwdRfTasIsEnable(void);
extern HwdRfCustTasAntNumT HwdRfTasGetInitIndex(HwdRfBandT band);
extern const HwdRfTasTblEtyT HwdRfTasTbl[];
extern const uint16 HwdRfTasTblSize;

#endif /* _HWDTASPUBLIC_H_ */

