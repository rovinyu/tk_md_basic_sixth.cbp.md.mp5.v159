/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*******************************************************************************
 *
 * Filename:
 * ---------
 * hwddelayjade.h
 *
 * Project:
 * --------
 * C2K
 *
 * Description:
 * ------------
 * Header file containing typedefs and definitions pertaining
 * to the RF infrastructure.
 *
 * Author:
 * -------
 * 
 *
 *==============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 *
 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *==============================================================================
 *******************************************************************************/

#ifndef _HWDDELAYJADE_H_
#define _HWDDELAYJADE_H_

/*----------------------------------------------------------------------------
 Include Files
----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 Typedefs
----------------------------------------------------------------------------*/
/* All Delay Loader triggers */
typedef enum {
    /* 0 */  DLYTRIG_DET_ADC,
    /* 1 */  DLYTRIG_TXON_INT_A,
    /* 2 */  DLYTRIG_TXON_INT_M,
    /* 3 */  DLYTRIG_APC_DAC,
    /* 4 */  DLYTRIG_FINEGAIN_OL,
    /* 5 */  DLYTRIG_FINEGAIN_CL,
    /* 6 */  DLYTRIG_PMIC,

    /* 7 */  DLYTRIG_BPI0,
    /* 8 */  DLYTRIG_BPI1,
    /* 9 */  DLYTRIG_BPI2,
    /* 10 */ DLYTRIG_BPI3,
    /* 11 */ DLYTRIG_BPI4,
    /* 12 */ DLYTRIG_BPI5,
    /* 13 */ DLYTRIG_BPI6,
    /* 14 */ DLYTRIG_BPI7,
    /* 15 */ DLYTRIG_BPI8,
    /* 16 */ DLYTRIG_BPI9,
    /* 17 */ DLYTRIG_BPI10,
    /* 18 */ DLYTRIG_BPI11,
    /* 19 */ DLYTRIG_BPI12,
    /* 20 */ DLYTRIG_BPI13,
    /* 21 */ DLYTRIG_BPI14,
    /* 22 */ DLYTRIG_BPI15,
    /* 23 */ DLYTRIG_BPI16,
    /* 24 */ DLYTRIG_BPI17,
    /* 25 */ DLYTRIG_BPI18,
    /* 26 */ DLYTRIG_BPI19,
    /* 27 */ DLYTRIG_BPI20,
    /* 28 */ DLYTRIG_BPI21,
    /* 29 */ DLYTRIG_BPI22,
    /* 30 */ DLYTRIG_BPI23,
    /* 31 */ DLYTRIG_BPI24,
    /* 32 */ DLYTRIG_BPI25,
    /* 33 */ DLYTRIG_BPI26,
    /* 34 */ DLYTRIG_BPI27,
    /* 35 */ DLYTRIG_BPI28,
    /* 36 */ DLYTRIG_BPI29,
    /* 37 */ DLYTRIG_BPI30,
    /* 38 */ DLYTRIG_BPICFT_0,
    /* 39 */ DLYTRIG_BPICFT_1,
    /* 40 */ DLYTRIG_BPI32,
    /* 41 */ DLYTRIG_BPI33,
    /* 42 */ DLYTRIG_BPI34,
    /* 43 */ DLYTRIG_BPI35,
    /* 44 */ DLYTRIG_BPI36,
    /* 45 */ DLYTRIG_BPI37,
    /* 46 */ DLYTRIG_BPI38,
    /* 47 */ DLYTRIG_BPI39,
    /* 48 */ DLYTRIG_BPI40,
    /* 49 */ DLYTRIG_BPI41,
    /* 50 */ DLYTRIG_BPI42,
    /* 51 */ DLYTRIG_BPI43,
    /* 52 */ DLYTRIG_BPI44,
    /* 53 */ DLYTRIG_BPI45,
    /* 54 */ DLYTRIG_BPI46,
    /* 55 */ DLYTRIG_BPI47,

    /* 56 */ DLYTRIG_MIPI0_1,
    /* 57 */ DLYTRIG_MIPI0_2,
    /* 58 */ DLYTRIG_MIPI1_1,
    /* 59 */ DLYTRIG_MIPI1_2,
    /* 60 */ DLYTRIG_MIPI2_1,
    /* 61 */ DLYTRIG_MIPI2_2,
    /* 62 */ DLYTRIG_MIPI3_1,
    /* 63 */ DLYTRIG_MIPI3_2,
    /* 64 */ DLYTRIG_BSI1_1,
    /* 65 */ DLYTRIG_BSI1_2,

    /* 66 */ DLYTRIG_TXNCO,
    /* 67 */ DLYTRIG_DDPC,

    /* 68 */ DLYTRIG_NUM
} HwdDlyLdrAllTrigIdJadeT;


/*----------------------------------------------------------------------------
 Global Functions and Data
----------------------------------------------------------------------------*/

extern HwdDlyDisMskConfT DlyDisMskRegsJade[DLYTRIG_NUM];
extern HwdDlyTrigConfT DlyTrigConfJade[2][DLYTRIG_NUM];
extern HwdDlyBsiBpiTrigT DlyBsiBpiTrigJade;
extern void HwdDelayInitTrigTblJade(uint16 *trigTblPtr);

#endif /* _HWDDELAYJADE_H_ */

