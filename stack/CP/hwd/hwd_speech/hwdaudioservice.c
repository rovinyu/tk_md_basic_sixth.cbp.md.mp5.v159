/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2012
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*******************************************************************************
 *
 * Filename:
 * ---------
 * l1audio_service.c
 *
 * Project:
 * --------
 *   MAUI
 *
 * Description:
 * ------------
 *   L1SP Task / L1Audio Service
 *
 * Author:
 * -------
 * Phil Hsieh
 *
 *==============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 * $Revision: $
 * $Modtime: $
 * $Log: $
 *
 * 01 29 2015 ys.hsieh
 * [SIXTH00002074] [C2K][FD216] sleep mode
 * .
 *
 * 01 13 2015 lanus.chao
 * [SIXTH00001470] [C2K] BT CVSD/mSBC check in
 * BT enable
 *
 * 11 23 2014 kenny.yang
 * [SIXTH00001567] [c2k] speech check in
 * C2K Speech DDL phase in
 *
 * 07 25 2014 fu-shing.ju
 * [MOLY00073593] [MT6290E2][VOLTE] VoLte data alignment
 * VoLte data alignment
 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *==============================================================================
 *******************************************************************************/

#if 0
#include "kal_public_defs.h"
#include "kal_public_api.h"
#include "intrCtrl.h"
#include "string.h"
#include "hisr_config.h"
#include "kal_public_defs.h"
#include "reg_base.h"
#include "l1sm_public.h"

#include "kal_trace.h"
#include "l1sp_trc.h"
#include "l1audio.h"
#include "audio_def.h"
#include "media.h"
#include "l1audio_trace.h"
#include "ddload.h"
#include "am.h"
#include "afe.h"
#include "common_def.h"
//#include "speech_def.h"
#include "sal_def.h"
#include "sal_exp.h"
#if defined( __CENTRALIZED_SLEEP_MANAGER__ )
#include "RM_public.h"
#endif

#if defined(__SMART_PHONE_MODEM__)
#include "bgSnd.h" // for BGSND_INIT() 
#endif // defined(__SMART_PHONE_MODEM__)

#if defined(MT6595)
#include "init.h" // for INT_SW_SecVersion
#endif

#include "mml1_rf_global.h"

#endif

#include "sysdefs.h"
#include "exeapi.h"
#include "sysapi.h"
// #include "assert.h"

#include "monapi.h"
#include "monids.h"

#include "hwdapi.h"

#include "hwdspherr.h"

#include "hwdaudioservice.h"
#include "fd216_drv.h"
#include "hwdaudio_dsp_d2c_def.h"
#include "hwdcommon_def.h"
#include "hwdsph.h"
#include "hwdam.h"
#include "hwdafe.h"
#include "hwdafe_def.h"
#include "hwdbgSnd.h"
#include "hwdsidetone.h"
#if defined(__CVSD_CODEC_SUPPORT__) 
#include "hwdbtsco_drv.h"
#endif
#include "ddload.h"
#include "hwdsal_exp.h"
#include "hwdsal_dsp.h"
#include "valapi.h"
#include "hwdsleep.h"


// TODO: c2k 
// done~ [add timer]
// [eventGroup]
// [chip option]
// [slow idle]
// [trace]
// [cdp]
// [dd44 dump]		
// [sleep handler]
// [dsp init waiting]
// [module init]
// [event waiting]
// [add SAL]

/*----------------------------------------------------------------------------
 Local Defines  Macros
----------------------------------------------------------------------------*/

#define true         (bool)(1==1)
#define false        (bool)(1==0)


#if 0
#if defined( __UMTS_RAT__ ) || defined( __UMTS_TDD128_MODE__ )

#if defined(MT6572) || defined(MT6582) || defined(MT6592) || defined(MT6571) || defined(MT6595) || defined(MT6752)
   #if defined(__VOLTE_SUPPORT__) // 4g
      #define  MAX_HISR_HANDLER       15 // 3g(ul+dl), pnw(ul+dl), DACA for bt (ul+dl), vm, epl, bgsnd(ul+dl), record, 4G AMR (ul+dl), 4G G-codec (ul+dl)
   #else //#if defined(__VOLTE_SUPPORT__)
      #define  MAX_HISR_HANDLER       12 // 3g, pnw, DACA for bt, vm, epl, bgsnd(ul+dl), record, cvsd
   #endif //#if defined(__VOLTE_SUPPORT__)
#else
	#error Need to check the HISR number for the new chip
#endif // by chip

#else // no 3g, NOT defined( __UMTS_RAT__ ) || defined( __UMTS_TDD128_MODE__ )

#if defined(MT6572) || defined(MT6582) || defined(MT6592) || defined(MT6571) || defined(MT6595) || defined(MT6752)
#define  MAX_HISR_HANDLER       10 // pnw, DACA for bt, vm, epl, bgsnd(ul+dl), record, cvsd
#else
	#error Need to check the HISR number for the new chip
#endif // by chip

#endif // defined( __UMTS_RAT__ ) || defined( __UMTS_TDD128_MODE__ )
#endif 

#if 1 // TODO: c2k [chip option] add here
	#define  MAX_HISR_HANDLER       12 
#else
	#error Need to check the HISR number for the new chip
#endif



/*--------------------------------------------------------------------------*/


// uint32 SaveAndSetIRQMask( void ); use SysIntDisable(SYS_ALL_INT) 
// void   RestoreIRQMask( uint32 ); use SysIntEnable(SYS_ALL_INT);
// void toneLoopbackRecInit( void );
// extern void DisableIRQ(void);


#ifdef MTK_SLEEP_ENABLE
   #define  GET_SLEEP_HANDLE()   {  /*l1audio.sleep_handle=SleepDrv_GetHandle();*/   }
   #define  SLEEP_LOCK()         {  MonDeepSleepSuspend( SYS_MODE_MAX, MON_HWD_SLEEP_DSPV_BUSY_FLAG); /*SleepDrv_SleepDisable( l1audio.sleep_handle );*/  }
   #define  SLEEP_UNLOCK()       {  MonDeepSleepResume( SYS_MODE_MAX, MON_HWD_SLEEP_DSPV_BUSY_FLAG);/*SleepDrv_SleepEnable( l1audio.sleep_handle );*/    }
   extern void HwdPwrSaveFd216ClkCtrl(bool HwState);
   extern bool HwdPwrSaveDspFd216ClkState(void);
#else
   #define  GET_SLEEP_HANDLE()
   #define  SLEEP_LOCK()
   #define  SLEEP_UNLOCK()
#endif

// extern uint32 L1I_GetTimeStamp( void );

#if (defined( __CENTRALIZED_SLEEP_MANAGER__ ) && defined(MTK_SLEEP_ENABLE) )
#if defined(__AUDIO_POWERON_RESET_DSP__)
   extern unsigned short  L1D_Audio_RestartDSP( void );
   extern void            L1D_Audio_NoNeedDSP( void );
//    extern void AFE_Init_status(bool flag);
#else
   extern uint8  L1SM_IntGetHandle( void );
   extern void       L1SM_Multi_SW_WakeUp(void); 
   extern void       L1SM_IntSleepDisable( uint8 handle );
   extern void       L1SM_IntSleepEnable( uint8 handle );
   extern uint8  L1D_MD2G_PWD_GetHandle( void );
   extern      void  L1D_MD2G_PWD_Disable( uint8 handle );
   extern      void  L1D_MD2G_PWD_Enable( uint8 handle );
#endif // defined(__AUDIO_POWERON_RESET_DSP__)
#endif //(defined( __CENTRALIZED_SLEEP_MANAGER__ ) && defined(MTK_SLEEP_ENABLE) )

/* ------------------------------------------------------------------------------ */
static ExeHisrT audioHisr;
static ExeSemaphoreT audEventSema; 

static struct {
	bool                 isInit;
   uint32               sleppModeLocker;
   uint32               dspUser;
	// done TODO: c2k [eventGroup]
	uint32               aud_events;
   ExeSemaphoreT        *aud_eventSema;// eventgrpid           aud_events;
   // end TODO: c2k [eventGroup]
   uint16               media_flag;
   uint16               d2c_itype;
   uint32               d2c_l1FN;
   uint32               retrieved_events;
   uint32               events_l1FN;
   ExeHisrT             *hisr;

   L1Audio_EventHandler evHandler[MAX_AUDIO_FUNCTIONS];
   void                 *evData[MAX_AUDIO_FUNCTIONS];
   uint32               id_flag;
   uint32               event_flag;
   L1Audio_EventHandler hisrHandler[MAX_HISR_HANDLER];
   uint16               hisrMagicNo[MAX_HISR_HANDLER];
   void                 *hisrUserData[MAX_HISR_HANDLER];
   uint16               hisrMagicFlag;
   uint8                sleep_handle;
   int8                 dsp_slow_idle_counter;

   uint16               debug_info[NUM_DEBUG_INFO];

   uint8                md2g_pdn_handle;
   uint8                l1sm_handle;
   uint8                audio_cpd_count;
   ExeSemaphoreT        sema;

	bool                 isD2MIntAvalibaleUnderSleep; // does Dsp to MCU interrupt can trigger under MCU sleep
} l1audio;


//for opendsp
#if defined(__OPEN_DSP_SPEECH_SUPPORT__)
typedef struct{
    
    uint32 ul_framesize,  dl_framesize;
    uint32 ul_write_addr, dl_write_addr;
    volatile uint32* pu32_scp_ul_read; // invalidate cache
    volatile uint16* pu16_fd216_ul_write;
    volatile uint32* pu32_scp_dl_read; // invalidate cache
    volatile uint16* pu16_fd216_dl_write;
    
    volatile uint32* pu32_scp_ul_read_tmp;// invalidate cache
    volatile uint32* pu32_scp_dl_read_tmp;// invalidate cache
    
    uint32 ul_addr, dl_addr;
    
    kal_uint32 t_ul_get_start, t_ul_get_end; // time
    kal_uint32 t_ul_put_start, t_ul_put_end;
    
    kal_uint32 t_dl_get_start, t_dl_get_end;
    kal_uint32 t_dl_put_start, t_dl_put_end;

    kal_uint32 t_before_while, t_after_while;

}OPENDSP_T;

OPENDSP_T OpenDsp;

extern void  od_memcpy(uint16 *dst, uint32 * src, uint32 len);

#endif

/* ------------------------------------------------------------------------------ */
extern void L1D_DSP_EnableDspSlowIdle( void );
int8 L1Audio_Disable_DSPSlowIdle(void)
{
   // uint32 savedMask;
   int8    dis_check_counter;

   SysIntDisable(SYS_IRQ_INT);// savedMask = SaveAndSetIRQMask();
   l1audio.dsp_slow_idle_counter++;
   dis_check_counter = l1audio.dsp_slow_idle_counter;

	// TODO: c2k [slow idle] 
	/* 
	   1. add dpram.h 2. 
	   2. #define DP_SLOW_IDLE_DIVIDER DP_DSP_TASK_STATUS_BASE + 0x005 0x3985
	   3. #define DP_SLOW_IDLE_CTRL (DPRAM_base(DP_SLOW_IDLE_CTRL)) 
	*/
   // DP_SLOW_IDLE_CTRL = 0xFFFF;
   // end TODO: c2k [slow idle] 
   SysIntEnable(SYS_IRQ_INT); //RestoreIRQMask( savedMask );

   return dis_check_counter;
}

int8 L1Audio_Enable_DSPSlowIdle(void)
{
   // uint32 savedMask;
   int8    en_check_counter;

   SysIntDisable(SYS_IRQ_INT);// savedMask = SaveAndSetIRQMask();
   l1audio.dsp_slow_idle_counter--;
   en_check_counter = l1audio.dsp_slow_idle_counter;

   if ( l1audio.dsp_slow_idle_counter == 0 ){
		// TODO: c2k [slow idle]
      // L1D_DSP_EnableDspSlowIdle();
   }

   SysIntEnable(SYS_IRQ_INT); //RestoreIRQMask( savedMask );
   ASSERT( en_check_counter >= 0, HWDSPH_ERR_FORCE_ASSERT, en_check_counter );

   return en_check_counter;
}

bool L1Audio_CheckAudioID( uint16 audio_id )
{
   if( l1audio.id_flag & (1 << audio_id) )
      return true;
   return false;
}
uint32 L1Audio_GetEventGroup( void )
{

// doone TODO: c2k [eventGroup]
   uint32 retrieved_events;
	ExeSemaphoreGet(l1audio.aud_eventSema, EXE_TIMEOUT_FALSE);
	retrieved_events = l1audio.aud_events;
	l1audio.aud_events = 0;
	ExeSemaphoreRelease(l1audio.aud_eventSema);
   // kal_retrieve_eg_events( l1audio.aud_events, 0xFFFF, KAL_OR_CONSUME, &retrieved_events, KAL_SUSPEND );
   return retrieved_events;
}

uint16 L1Audio_GetAudioID( void )
{
   uint32 I;   

   SysIntDisable(SYS_IRQ_INT);// savedMask = SaveAndSetIRQMask();
   for( I = 0; I < MAX_AUDIO_FUNCTIONS; I++ ) {
      if( (l1audio.id_flag & (1<<I)) == 0 ) {
         l1audio.id_flag |= (1<<I);
         break;
      }
   }
   SysIntEnable(SYS_IRQ_INT); //RestoreIRQMask( savedMask );

	MonTrace(MON_CP_HWD_SPH_DSP_USE_ID_TRACE_ID, 4, true, I, l1audio.id_flag, l1audio.sleppModeLocker);
	
   ASSERT( (l1audio.sleppModeLocker & (1 << I)) == 0, HWDSPH_ERR_FORCE_ASSERT, I );
   ASSERT( I < MAX_AUDIO_FUNCTIONS, HWDSPH_ERR_FORCE_ASSERT, I );
   return (uint16)I;
}

void L1Audio_FreeAudioID( uint16 aud_id )
{
   // uint32 savedMask;
   // done TODO: c2k [eventGroup]
   L1Audio_GetEventGroup(); // kal_retrieve_eg_events(l1audio.aud_events,(1<<aud_id),KAL_OR_CONSUME,&retrieved_events,0);
	// end TODO: c2k [eventGroup]

	MonTrace(MON_CP_HWD_SPH_DSP_USE_ID_TRACE_ID, 4, false, aud_id, l1audio.id_flag, l1audio.sleppModeLocker);
	
   ASSERT( (l1audio.sleppModeLocker & (1 << aud_id)) == 0, HWDSPH_ERR_FORCE_ASSERT, aud_id );
   ASSERT( l1audio.id_flag & (1<<aud_id), HWDSPH_ERR_FORCE_ASSERT, aud_id );
   SysIntDisable(SYS_IRQ_INT);// savedMask = SaveAndSetIRQMask();
   l1audio.id_flag &= ~(1<<aud_id);
   l1audio.retrieved_events &= ~(1<<aud_id);
   SysIntEnable(SYS_IRQ_INT); // RestoreIRQMask( savedMask );
}

void L1Audio_SetEventHandler( uint16 audio_id, L1Audio_EventHandler handler )
{
   l1audio.evHandler[audio_id] = handler;
}

void L1Audio_SetEvent( uint16 audio_id, void *data )
{
   l1audio.evData[audio_id] = data;
   /* Activate L1Audio Task */
	// done TODO: c2k [eventGroup]
   // kal_set_eg_events( l1audio.aud_events, 1 << audio_id, KAL_OR );
  // ExeSemaphoreGet(l1audio.aud_eventSema, EXE_TIMEOUT_FALSE);
	l1audio.aud_events |= (1 << audio_id);
	//ExeSemaphoreRelease(l1audio.aud_eventSema);

	ExeSignalSet(EXE_HWD_ID, HWD_SPH_AUDIO_SIGNAL);
   // end TODO: c2k [eventGroup]
}

void L1Audio_LSetEvent( uint16 audio_id, void *data )
{
   // uint32 savedMask;

   SysIntDisable(SYS_IRQ_INT);// savedMask = SaveAndSetIRQMask();
   l1audio.evData[audio_id] = data;
   l1audio.event_flag |= (1 << audio_id);
   SysIntEnable(SYS_IRQ_INT); // RestoreIRQMask( savedMask );

   /* Activate L1Audio HISR */
   // kal_activate_hisr( l1audio.hisr );
   ExeHisrActivate(l1audio.hisr);
}

bool L1Audio_CheckFlag( uint16 audio_id )
{
   ASSERT( l1audio.id_flag & (1 << audio_id), HWDSPH_ERR_FORCE_ASSERT, audio_id );
   if( l1audio.sleppModeLocker & (1 << audio_id) )
      return true;
   return false;
}



static void Audio_Wake_DSP(uint16 audio_id, bool flag)
{
	// TODO: c2k [trace]
	/*
   if (!if_hisr() && !kal_if_lisr())
      kal_trace( TRACE_GROUP_AUD_MD2GCTRL, L1AUDIO_MD2G_PWR_CTRL, audio_id, flag);
   else
      kal_dev_trace( TRACE_GROUP_AUD_MD2GCTRL, L1AUDIO_MD2G_PWR_CTRL, audio_id, flag);
      */
   if(flag)
   {
   	// TODO: c2k [cdp]
      // RM_Resource_Control (RM_MODEM_DSP_1, flag);

#if defined(MTK_SLEEP_ENABLE)
   #if defined(__AUDIO_POWERON_RESET_DSP__)
			
      HwdPwrSaveFd216ClkCtrl(true);  
      {
      	 bool test = HwdPwrSaveDspFd216ClkState();
      	 MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2,98765,test);      	 
      }
      AFE_RegisterStore();                               //restore AFE register
      {
         unsigned short DSP_status = L1D_Audio_RestartDSP();
      	 MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2,98765,DSP_status);   
         //ASSERT(!DSP_status, HWDSPH_ERR_FORCE_ASSERT, DSP_status);
      }      
   #else
      L1SM_IntSleepDisable( l1audio.l1sm_handle );
      L1SM_Multi_SW_WakeUp();
      L1D_MD2G_PWD_Disable(l1audio.md2g_pdn_handle);
      L1SM_IntSleepEnable( l1audio.l1sm_handle );
   #endif
#endif
      l1audio.audio_cpd_count++;
   } else {
#if defined(MTK_SLEEP_ENABLE)
   #if defined(__AUDIO_POWERON_RESET_DSP__)
      L1D_Audio_NoNeedDSP();      
      AFE_RegisterBackup();                         //Backup AFE register
      HwdPwrSaveFd216ClkCtrl(false);  
   #else
      L1D_MD2G_PWD_Enable(l1audio.md2g_pdn_handle);
   #endif
#endif
		// TODO: c2k [cdp]
      // RM_Resource_Control (RM_MODEM_DSP_1, flag);
      l1audio.audio_cpd_count--;
   }
	// TODO: c2k [trace]
	/*
   if (!kal_if_hisr() && !kal_if_lisr())
      kal_trace( TRACE_GROUP_AUD_MD2GCTRL, L1AUDIO_MD2G_PWR_CTRL_DONE, audio_id, flag);
   else
      kal_dev_trace( TRACE_GROUP_AUD_MD2GCTRL, L1AUDIO_MD2G_PWR_CTRL_DONE, audio_id, flag);
      */
}


void L1Audio_SetFlag( uint16 audio_id )
{
   MonTrace(MON_CP_HWD_SPH_DSP_SLEEP_TRACE_ID, 4, audio_id, true, l1audio.dspUser, l1audio.sleppModeLocker);

   ExeSemaphoreGet(&(l1audio.sema), EXE_TIMEOUT_FALSE); // kal_take_sem( l1audio.sema, KAL_INFINITE_WAIT );
   ASSERT( l1audio.id_flag & (1 << audio_id), HWDSPH_ERR_FORCE_ASSERT, audio_id );
   ASSERT( (l1audio.sleppModeLocker & (1 << audio_id)) == 0, HWDSPH_ERR_FORCE_ASSERT, audio_id );
	ASSERT( (l1audio.dspUser & (1 << audio_id)) == 0, HWDSPH_ERR_FORCE_ASSERT, audio_id );
	if (false == l1audio.isD2MIntAvalibaleUnderSleep && l1audio.sleppModeLocker == 0) {
      	SLEEP_LOCK();
	}
	
   if( l1audio.dspUser == 0 ) {
#if ( defined( __CENTRALIZED_SLEEP_MANAGER__ )&& defined( MTK_SLEEP_ENABLE ))
      Audio_Wake_DSP(audio_id, KAL_TRUE);
#endif
   }
		
   SysIntDisable(SYS_IRQ_INT);// savedMask = SaveAndSetIRQMask();
	if (false == l1audio.isD2MIntAvalibaleUnderSleep) {
	   l1audio.sleppModeLocker |= (1 << audio_id);	
	}
   l1audio.dspUser |= (1 << audio_id);
   SysIntEnable(SYS_IRQ_INT); //RestoreIRQMask( savedMask );
   ExeSemaphoreRelease(&(l1audio.sema)); //kal_give_sem( l1audio.sema ); 
}

void L1Audio_ClearFlag( uint16 audio_id )
{
   MonTrace(MON_CP_HWD_SPH_DSP_SLEEP_TRACE_ID, 4, audio_id, false, l1audio.dspUser, l1audio.sleppModeLocker);
  

	ExeSemaphoreGet(&(l1audio.sema), EXE_TIMEOUT_FALSE); // kal_take_sem( l1audio.sema, KAL_INFINITE_WAIT );
   ASSERT( l1audio.id_flag & (1 << audio_id), HWDSPH_ERR_FORCE_ASSERT, audio_id );
	ASSERT( l1audio.dspUser & (1 << audio_id), HWDSPH_ERR_FORCE_ASSERT, audio_id );
	if (false == l1audio.isD2MIntAvalibaleUnderSleep) {
   	ASSERT( l1audio.sleppModeLocker & (1 << audio_id), HWDSPH_ERR_FORCE_ASSERT, audio_id );
	}

   SysIntDisable(SYS_IRQ_INT);// savedMask = SaveAndSetIRQMask();
	if (false == l1audio.isD2MIntAvalibaleUnderSleep) {
	   l1audio.sleppModeLocker &= ~(1 << audio_id);
	}
   l1audio.dspUser &= ~(1 << audio_id);
   SysIntEnable(SYS_IRQ_INT); //RestoreIRQMask( savedMask );

   if( l1audio.dspUser == 0 ) {      
#if ( defined( MTK_SLEEP_ENABLE ))
//#if ( defined( __CENTRALIZED_SLEEP_MANAGER__ )&& defined( MTK_SLEEP_ENABLE ))
      Audio_Wake_DSP(audio_id, KAL_FALSE);
#endif
   }
   
   if((false == l1audio.isD2MIntAvalibaleUnderSleep) && (l1audio.sleppModeLocker == 0) ) {
      SLEEP_UNLOCK();
   }   
   ExeSemaphoreRelease(&(l1audio.sema)); //kal_give_sem( l1audio.sema ); 
}

void L1Audio_SetFlag_MD2G( uint16 audio_id )
{
   //uint32 savedMask;
   // TODO: c2k [trace]
   /*
   if (!kal_if_hisr() && !kal_if_lisr())
      kal_trace( TRACE_GROUP_AUD_MD2GCTRL, L1AUDIO_SETFLAG_MD2G_A,audio_id,l1audio.dspUser);
   else
      kal_dev_trace( TRACE_GROUP_AUD_MD2GCTRL, L1AUDIO_SETFLAG_MD2G_A,audio_id,l1audio.dspUser);
	*/
   ExeSemaphoreGet(&(l1audio.sema), EXE_TIMEOUT_FALSE); // kal_take_sem( l1audio.sema, KAL_INFINITE_WAIT );
   ASSERT( l1audio.id_flag & (1 << audio_id), HWDSPH_ERR_FORCE_ASSERT, audio_id );
   ASSERT( (l1audio.dspUser & (1 << audio_id)) == 0, HWDSPH_ERR_FORCE_ASSERT, audio_id );

   if( l1audio.dspUser == 0 ) {
#if defined( __CENTRALIZED_SLEEP_MANAGER__ )
      Audio_Wake_DSP(audio_id,KAL_TRUE);
#endif
   }

   SysIntDisable(SYS_IRQ_INT);// savedMask = SaveAndSetIRQMask();
   l1audio.dspUser |= (1 << audio_id);
   SysIntEnable(SYS_IRQ_INT); //RestoreIRQMask( savedMask );
   ExeSemaphoreRelease(&(l1audio.sema)); //kal_give_sem( l1audio.sema ); 
}

void L1Audio_ClearFlag_MD2G( uint16 audio_id )
{
   // uint32 savedMask;
   // TODO: c2k [trace]
   /*
   if (!kal_if_hisr() && !kal_if_lisr())  
      kal_trace( TRACE_GROUP_AUD_MD2GCTRL, L1AUDIO_CLEARFLAG_MD2G_A,audio_id,l1audio.dspUser);
   else
      kal_dev_trace( TRACE_GROUP_AUD_MD2GCTRL, L1AUDIO_CLEARFLAG_MD2G_A,audio_id,l1audio.dspUser);
      */
   ExeSemaphoreGet(&(l1audio.sema), EXE_TIMEOUT_FALSE); //kal_take_sem( l1audio.sema, KAL_INFINITE_WAIT );
   ASSERT( l1audio.id_flag & (1 << audio_id), HWDSPH_ERR_FORCE_ASSERT, audio_id );
   ASSERT( l1audio.dspUser & (1 << audio_id), HWDSPH_ERR_FORCE_ASSERT, audio_id );

   SysIntDisable(SYS_IRQ_INT);// savedMask = SaveAndSetIRQMask();
   l1audio.dspUser &= ~(1 << audio_id);
   SysIntEnable(SYS_IRQ_INT); //RestoreIRQMask( savedMask );
   
   if(l1audio.dspUser == 0 ) {
// #if defined( __CENTRALIZED_SLEEP_MANAGER__ )
      Audio_Wake_DSP(audio_id, false);
// #endif
   }
   ExeSemaphoreRelease(&(l1audio.sema)); //kal_give_sem( l1audio.sema ); 
}


void L1Audio_AllowSleep( uint16 audio_id )
{
   // uint32 savedMask;

   ASSERT( l1audio.id_flag & (1 << audio_id), HWDSPH_ERR_FORCE_ASSERT, audio_id );
   ASSERT( l1audio.sleppModeLocker & (1 << audio_id), HWDSPH_ERR_FORCE_ASSERT, audio_id );

   SysIntDisable(SYS_IRQ_INT);// savedMask = SaveAndSetIRQMask();
   l1audio.sleppModeLocker &= ~(1 << audio_id);
   SysIntEnable(SYS_IRQ_INT); //RestoreIRQMask( savedMask );

   if( l1audio.sleppModeLocker == 0)
      SLEEP_UNLOCK();
}

void L1Audio_DisallowSleep( uint16 audio_id )
{
   //uint32 savedMask;

   ASSERT( l1audio.id_flag & (1 << audio_id), HWDSPH_ERR_FORCE_ASSERT, audio_id );
   ASSERT(( l1audio.sleppModeLocker & (1 << audio_id)) == 0, HWDSPH_ERR_FORCE_ASSERT, audio_id );

   SysIntDisable(SYS_IRQ_INT);// savedMask = SaveAndSetIRQMask();
   l1audio.sleppModeLocker |= (1 << audio_id);
   SysIntEnable(SYS_IRQ_INT); //RestoreIRQMask( savedMask );

   if(l1audio.sleppModeLocker)
      SLEEP_LOCK();
}

/* ------------------------------------------------------------------------------ */
/*  L1Audio HISR                                                                  */
/* ------------------------------------------------------------------------------ */

void L1Audio_HISR( void )
{
   //uint32 savedMask;
   int32  I;

   L1Audio_Disable_DSPSlowIdle();

   if( l1audio.hisrMagicFlag ) {
      for( I = 0; I < MAX_HISR_HANDLER; I++ ) {
         if( l1audio.hisrMagicFlag & (1<<I) ) {
            SysIntDisable(SYS_IRQ_INT);// savedMask = SaveAndSetIRQMask();
            l1audio.hisrMagicFlag &= ~(1<<I);
            SysIntEnable(SYS_IRQ_INT); //RestoreIRQMask( savedMask );
            l1audio.hisrHandler[I]( l1audio.hisrUserData[I] );
         }
      }
   }

   if( l1audio.event_flag ) {
      int16 I;
      for( I = 0; I < MAX_AUDIO_FUNCTIONS; I++ ) {
         if( l1audio.event_flag & (1<<I) ) {
            SysIntDisable(SYS_IRQ_INT);// savedMask = SaveAndSetIRQMask();
            l1audio.event_flag &= ~(1<<I);
            SysIntEnable(SYS_IRQ_INT); //RestoreIRQMask( savedMask );
            L1Audio_SetEvent( I, l1audio.evData[I] );
         }
      }
   }

   // To restore slow idle ctrl for DSP
   L1Audio_Enable_DSPSlowIdle();
}


void L1Audio_TrigD2CHisr(uint16 magicNo)
{
	int32 I;
	   for( I = 0; I < MAX_HISR_HANDLER; I++ ) {
      if( magicNo == l1audio.hisrMagicNo[I] ) {
         l1audio.hisrMagicFlag |= (1<<I);
         // kal_activate_hisr(l1audio.hisr);
         ExeHisrActivate(l1audio.hisr);
         return; 
      }
   }
	
}
#if defined(__OPEN_DSP_SPEECH_SUPPORT__)

void L1SP_OD_UL_GET()
{
    OpenDsp.t_ul_get_start = HwdSleepRead32KCnt();
    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2, OpenDsp.t_ul_get_start, 0x11111111);      	 
    //L1Audio_Msg_DSP_OD_DELAY_TIME(L1AUDIO_OD_Val(0),SAL_OpenDSP_IsOFF());
    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2, SAL_OpenDSP_IsOFF(), 0x11111112);
	  ASSERT(SAL_OpenDSP_IsOFF() == 0, HWDSPH_ERR_FORCE_ASSERT, 0);     	 

		// check DSP and SCP Handshake
		if((*DPRAM_base(SPH_OD_HANDSHAKE_OD) != 1) && (*DPRAM_base(SPH_OD_HANDSHAKE_OD) != 2)) {
			// *DPRAM_base(SPH_OD_HANDSHAKE_OD) value
			MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2, *DPRAM_base(SPH_OD_HANDSHAKE_OD), 0x11111113); 
			ASSERT(0, HWDSPH_ERR_FORCE_ASSERT, 0);
		}	

    IopCcismWrite(IOP_CCISM_AUDIO_CHANNEL,NULL);      // mcu interrupts OpenDsp UL INT

 
    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2, *DPRAM_base(SPH_OD_HANDSHAKE_OD), 0x11111114); 
    OpenDsp.t_ul_get_end  = Hwd32kGetTimeWithOld(OpenDsp.t_ul_get_start);
    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2, OpenDsp.t_ul_get_end, 0x11111115);
    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2, (OpenDsp.t_ul_get_end - OpenDsp.t_ul_get_start), 0x11111116);

}

void L1SP_OD_UL_PUT()
{
    OpenDsp.t_ul_put_start  = Hwd32kGetTimeWithOld(OpenDsp.t_ul_get_start);
	  
    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2, OpenDsp.t_ul_put_start, 0x22222221);    
    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2, (OpenDsp.t_ul_put_start - OpenDsp.t_ul_get_start), 0x22222222); 
    
   	MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2, SAL_OpenDSP_IsOFF(), 0x22222223);
  	ASSERT(SAL_OpenDSP_IsOFF() == 0, HWDSPH_ERR_FORCE_ASSERT, 0);
  	
    uint32 hi = (*DPRAM_base(SPH_OD_TCM_ADDR_H)) << 16;
    uint32 lo = (*DPRAM_base(SPH_OD_TCM_ADDR_L));
    uint32 addr = (hi | lo) + 0xA0020000 ;  
    OpenDsp.ul_addr = addr;
    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2, addr, 0x22222224);
    OpenDsp.pu16_fd216_ul_write = SAL_PcmEx_GetBuf(SAL_PCMEX_OD_BUF_UL); // read UL DM
    OpenDsp.pu32_scp_ul_read = addr; 
    OpenDsp.ul_framesize = SAL_PcmEx_GetBufLen(SAL_PCMEX_OD_BUF_UL);
    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2, OpenDsp.pu32_scp_ul_read, 0x22222225); 
    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2, OpenDsp.pu16_fd216_ul_write, 0x22222226); 
    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2, OpenDsp.ul_framesize, 0x22222227);
    if(OpenDsp.ul_addr!=0 && OpenDsp.dl_addr!=0){
        OpenDsp.pu32_scp_dl_read_tmp = OpenDsp.dl_addr;
        MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 5 , OpenDsp.ul_addr, OpenDsp.pu32_scp_ul_read[0],     OpenDsp.pu32_scp_ul_read[1],     OpenDsp.pu32_scp_ul_read[2],     OpenDsp.pu32_scp_ul_read[3]); 
        MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 5 , OpenDsp.dl_addr, OpenDsp.pu32_scp_dl_read_tmp[0], OpenDsp.pu32_scp_dl_read_tmp[1], OpenDsp.pu32_scp_dl_read_tmp[2], OpenDsp.pu32_scp_dl_read_tmp[3]); 
        uint32 *t = OpenDsp.pu16_fd216_ul_write;
        MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 5 , OpenDsp.pu16_fd216_ul_write, t[0], t[1], t[2], t[3]);
    }

    od_memcpy(OpenDsp.pu16_fd216_ul_write, OpenDsp.pu32_scp_ul_read, OpenDsp.ul_framesize << 1);

    uint32 *tmp = OpenDsp.pu16_fd216_ul_write;
    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 5 , OpenDsp.pu16_fd216_ul_write, tmp[0], tmp[1], tmp[2], tmp[3]);

    SAL_OpenDSP_Handshake_MD(1, 0);

    OpenDsp.t_ul_put_end = Hwd32kGetTimeWithOld(OpenDsp.t_ul_get_start);
    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2, OpenDsp.t_ul_put_end, 0x22222228); 
    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2, (OpenDsp.t_ul_put_end - OpenDsp.t_ul_put_start), 0x22222229); 
    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2, (OpenDsp.t_ul_put_end - OpenDsp.t_ul_get_start), 0x2222222A); 
}


void L1SP_OD_DL_GET()
{
    OpenDsp.t_dl_get_start = HwdSleepRead32KCnt(); 
    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2, OpenDsp.t_dl_get_start, 0x33333331); 
      	
  	MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2, SAL_OpenDSP_IsOFF(), 0x33333332);
  	ASSERT(SAL_OpenDSP_IsOFF() == 0, HWDSPH_ERR_FORCE_ASSERT, 0);

		// check DSP and SCP Handshake
		if((*DPRAM_base(SPH_OD_HANDSHAKE_OD) != 1) && (*DPRAM_base(SPH_OD_HANDSHAKE_OD) != 2)) {
			MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2, *DPRAM_base(SPH_OD_HANDSHAKE_OD), 0x33333333);
			ASSERT(0, HWDSPH_ERR_FORCE_ASSERT, 0);
		}	
    
    IopCcismWrite(IOP_CCISM_AUDIO_CHANNEL,NULL);      // mcu interrupts OpenDsp UL INT
    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2, *DPRAM_base(SPH_OD_HANDSHAKE_OD), 0x33333334); 
    OpenDsp.t_dl_get_end  = Hwd32kGetTimeWithOld(OpenDsp.t_dl_get_start);
    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2, OpenDsp.t_dl_get_end, 0x33333335);
    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2, (OpenDsp.t_dl_get_end - OpenDsp.t_dl_get_start), 0x33333336);
}

void L1SP_OD_DL_PUT(){
	
    OpenDsp.t_dl_put_start  = Hwd32kGetTimeWithOld(OpenDsp.t_dl_get_start);
    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2, OpenDsp.t_dl_put_start, 0x44444441);    
    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2, (OpenDsp.t_dl_put_start - OpenDsp.t_dl_get_start), 0x44444442); 
    
  	MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2, SAL_OpenDSP_IsOFF(), 0x44444443);
  	ASSERT(SAL_OpenDSP_IsOFF() == 0, HWDSPH_ERR_FORCE_ASSERT, 0);
  	
    uint32 hi = (*DPRAM_base(SPH_OD_TCM_ADDR_H)) << 16;
    uint32 lo = (*DPRAM_base(SPH_OD_TCM_ADDR_L));
    uint32 addr = (hi | lo) + 0xA0020000 ;  
    OpenDsp.dl_addr = addr;
    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2, addr, 0x44444444);
    OpenDsp.pu16_fd216_dl_write = SAL_PcmEx_GetBuf(SAL_PCMEX_OD_BUF_DL); // read DL DM
    OpenDsp.pu32_scp_dl_read = addr; 
    OpenDsp.dl_framesize = SAL_PcmEx_GetBufLen(SAL_PCMEX_OD_BUF_DL);
    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2, OpenDsp.pu32_scp_dl_read, 0x44444445); // DL read address
    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2, OpenDsp.pu16_fd216_dl_write, 0x44444446); // DL write address
    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2, OpenDsp.dl_framesize, 0x44444447); // DL frame size
    if(OpenDsp.ul_addr!=0 && OpenDsp.dl_addr!=0){
        OpenDsp.pu32_scp_ul_read_tmp = OpenDsp.ul_addr;
        MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 5 , OpenDsp.dl_addr, OpenDsp.pu32_scp_ul_read_tmp[0], OpenDsp.pu32_scp_ul_read_tmp[1], OpenDsp.pu32_scp_ul_read_tmp[2], OpenDsp.pu32_scp_ul_read_tmp[3]); 
        MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 5 , OpenDsp.ul_addr, OpenDsp.pu32_scp_dl_read[0],     OpenDsp.pu32_scp_dl_read[1],     OpenDsp.pu32_scp_dl_read[2],     OpenDsp.pu32_scp_dl_read[3]); 
        
        uint32 *t = OpenDsp.pu16_fd216_dl_write;
        MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 5 , OpenDsp.pu16_fd216_dl_write, t[0], t[1], t[2], t[3]);
        
    }

    od_memcpy(OpenDsp.pu16_fd216_dl_write, OpenDsp.pu32_scp_dl_read, OpenDsp.dl_framesize << 1);

    uint32 *tmp = OpenDsp.pu16_fd216_dl_write;
    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 5 , OpenDsp.pu16_fd216_dl_write, tmp[0], tmp[1], tmp[2], tmp[3]);

    SAL_OpenDSP_Handshake_MD(0, 1);

    OpenDsp.t_dl_put_end = Hwd32kGetTimeWithOld(OpenDsp.t_dl_get_start);
    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2,  OpenDsp.t_dl_put_end, 0x44444448); 
    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2, (OpenDsp.t_dl_put_end - OpenDsp.t_dl_put_start), 0x44444449); 
    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID,2, (OpenDsp.t_dl_put_end - OpenDsp.t_dl_get_start), 0x4444444A); 
}
#endif

void L1SP_D2C_LISR( uint16 itype )
{
	uint32 sph_int = 0;
	bool dsp_ok = false;
	bool from_sph = false;

   l1audio.d2c_itype = itype;
   l1audio.d2c_l1FN = SysTimeGetFine().MostSignificant32Bits; //L1I_GetTimeStamp();
   // TODO: c2k [trace]
   // L1Audio_Msg_DSP_INT( itype );
   
// TODO: c2k [dd44 dump]		
/*
   if(itype == D2C_DSP_DEAD_INT_ID){
// 
#ifndef  L1D_TEST
      ASSERT_DUMP_PARAM_T dump_param;
      dump_param.addr[0] = (uint32)(DPRAM_CPU_base +0x0A0*2);
      dump_param.len[0]  = 70*2;
      dump_param.addr[1] = (uint32)(DPRAM2_CPU_base+0x130*2);
      dump_param.len[1]  = 180*2;
      dump_param.addr[2] = 0;    //End of dump param
      EXT_ASSERT_DUMP(0, 0x20060622, 0, 0, &dump_param);
#else
      extern void  L1DTest_AssertFail(void);
      L1DTest_AssertFail();
#endif
   }
	*/
	// end TODO: c2k [dd44 dump]		
	
	// TODO: c2k [add SAL]
   from_sph = SAL_DSPINT_Resolve(itype, &sph_int);//the itype should be the real one sent directly from DSP

	//done TODO: c2k [trace]
	// L1Audio_Msg_DSP_D2C_SPEECH_INT(L1AUDIO_Str_Bool(from_sph), sph_int);
	MonTrace(MON_CP_HWD_SPH_DSP_INTRUPT_MAPPING_TRACE_ID, 3, itype, from_sph, sph_int);

	if(from_sph) {
		
		int16 i;
		for( i=1; i< SAL_DSPINT_PRIO_MAX; i++) {
			if(sph_int & (1 << i)) {
            #if 0
				if( (SAL_DSPINT_PRIO_3G_UL == i) && AM_Is_4G()) {
					L1Audio_TrigD2CHisr(DP_D2C_INT_MAPPING_BASIC+SAL_DSPINT_PRIO_MAX+1);
				}else
				if( SAL_DSPINT_PRIO_3G_DL == i) {
				   if(AM_Is_4G()){
				      L1Audio_TrigD2CHisr(DP_D2C_INT_MAPPING_BASIC+SAL_DSPINT_PRIO_MAX+2);
				   }else{
   					// do nothing ! 
   					//Don't trigger LISR here. 3G driver will trigger HISR by it's timing
				   }
				} else 
				#endif /*#if 0*/
				{

					#if defined(__OPEN_DSP_SPEECH_SUPPORT__)
                	 if(SAL_DSPINT_PRIO_OD_UL_GET == i){
                        L1SP_OD_UL_GET();
                    }
                    if(SAL_DSPINT_PRIO_OD_UL_PUT == i){
                        L1SP_OD_UL_PUT();
                    }
                    if(SAL_DSPINT_PRIO_OD_DL_GET == i){
                        L1SP_OD_DL_GET();
                    }
                    if(SAL_DSPINT_PRIO_OD_DL_PUT == i){
                        L1SP_OD_DL_PUT();
                    }
          #endif
					
					L1Audio_TrigD2CHisr(DP_D2C_INT_MAPPING_BASIC+i); // please reference to 
				}				
	         dsp_ok |= true;
			}
		}
	 
      if(0 != sph_int){//After DSP send D2C and turn on bit in DP_D2C_SPEECH_UL_INT, but  MCU doesnt  receive D2C. Handover causes VBIReset which will clean DP_D2C_SPEECH_UL_INT
         if (!dsp_ok) {	
            // extern void L1D_WIN_DisableAllEvents(uint16 except_irq_mask);
				SysIntDisable(SYS_IRQ_INT); // DisableIRQ();
            // L1D_WIN_DisableAllEvents( 0 ); / disable all TDMA events /
            ASSERT(0, HWDSPH_ERR_FORCE_ASSERT, 0);
				SysIntEnable(SYS_IRQ_INT);
         }
      }else{
	   	// done TODO: c2k [trace]
			MonFault(MON_HWDSPH_FAULT_UNIT, HWDSPH_ERR_SKIP_SPH_INT, sph_int, MON_CONTINUE);// L1Audio_Msg_DSP_D2C_SPEECH_INT_SKIP(sph_int);
      }
	  
		return;
   } 	
   l1audio.media_flag = itype;
   // kal_activate_hisr(l1audio.hisr);
   ExeHisrActivate(l1audio.hisr);
}

void L1Audio_ActivateHisrHandler(uint16 magic_no, void *userData)
{
	uint32 I;
   //uint32 I, uMask32;
   SysIntDisable(SYS_IRQ_INT);// uMask32 = SaveAndSetIRQMask();
   for( I = 0; I < MAX_HISR_HANDLER; I++ ) {
      if( magic_no == l1audio.hisrMagicNo[I] ) {
         l1audio.hisrMagicFlag |= (1<<I);
         l1audio.hisrUserData[I] = userData;
      }
   }
   SysIntEnable(SYS_IRQ_INT); //RestoreIRQMask( uMask32 );
   if(l1audio.hisrMagicFlag != 0){
      //kal_activate_hisr(l1audio.hisr);
      ExeHisrActivate(l1audio.hisr);
   }
}

void L1Audio_HookHisrHandler( uint16 magic_no, L1Audio_EventHandler handler, void *userData )
{
   int32 I;
   for( I = 0; I < MAX_HISR_HANDLER; I++ ) {
      if( l1audio.hisrMagicNo[I] == 0 ) {
         l1audio.hisrMagicNo[I] = magic_no;
         l1audio.hisrHandler[I] = handler;
         l1audio.hisrUserData[I] = userData;
         break;
      }
   }
   ASSERT( I != MAX_HISR_HANDLER, HWDSPH_ERR_FORCE_ASSERT, 0 );
}

void L1Audio_UnhookHisrHandler( uint16 magic_no )
{
   int32 I;
   for( I = 0; I < MAX_HISR_HANDLER; I++ ) {
      if( l1audio.hisrMagicNo[I] == magic_no ) {
         l1audio.hisrMagicNo[I] = 0;
         break;
      }
   }
   ASSERT( I != MAX_HISR_HANDLER, HWDSPH_ERR_FORCE_ASSERT, 0 );
}

void L1Audio_SetDebugInfo( uint16 debug_info[NUM_DEBUG_INFO])
{
   memcpy(l1audio.debug_info, debug_info, NUM_DEBUG_INFO*sizeof(uint16));
}

void L1Audio_SetDebugInfoN( uint16 index, uint16 debug_info )
{
   l1audio.debug_info[index] = debug_info;
}

uint16 L1Audio_GetDebugInfo( uint8 index )
{
   ASSERT( index < NUM_DEBUG_INFO, HWDSPH_ERR_FORCE_ASSERT, index );
   return l1audio.debug_info[index];
}

/* ------------------------------------------------------------------------------ */
/*  Debug functions used to verify the data written to DSP                        */
/* ------------------------------------------------------------------------------ */
#if VERIFY_DATA_TO_DSP
#include   "fat_fs.h"
uint8  bDSPBuffer[8192];
uint16 uDSPBufferRead;
uint16 uDSPBufferWrite;
uint32 uDSPBufferSize;
uint16 fname[12] = { 'E', ':', '\\', 'D', 'A', 'T', 'A', '.', 'D', 'A', 'T', 0x00 };
FS_HANDLE  fs_handle = 0;

void VERIFY_DATA_TO_DSP_START( void )
{
   if( fs_handle )
      FS_Close( fs_handle );
   fs_handle = FS_Open( (const kal_wchar*)fname, FS_CREATE_ALWAYS );
   ASSERT( fs_handle > 0, HWDSPH_ERR_FORCE_ASSERT, 0 );
   uDSPBufferSize = 8192;
   uDSPBufferRead = 0;
   uDSPBufferWrite = 0;
}

void VERIFY_DATA_TO_DSP_RESUME( void )
{
   uint32 uFileSize;
   int32  ret;
   fs_handle = FS_Open( (const wchar*)fname, FS_READ_WRITE|FS_CREATE );
   ASSERT( fs_handle > 0, HWDSPH_ERR_FORCE_ASSERT, 0 );
   if(FS_GetFileSize(fs_handle, &uFileSize) < 0)
     ASSERT(0, HWDSPH_ERR_FORCE_ASSERT, 0);
   if(FS_Seek(fs_handle, uFileSize, FS_FILE_BEGIN) < 0)
     ASSERT(0, HWDSPH_ERR_FORCE_ASSERT, 0);
   uDSPBufferSize = 8192;
}

void VERIFY_DATA_TO_DSP_STOP( void )
{
   if( fs_handle )
   {
      if (FS_Close( fs_handle ) != 0)
         ASSERT(0, HWDSPH_ERR_FORCE_ASSERT, 0);
      fs_handle = 0;
   }
}

void VERIFY_DATA_TO_DSP_WRITE_DATA( const uint16 *buf, int32 len )
{
   uint32 uDSPBufferFree;
   if( uDSPBufferWrite < uDSPBufferRead )
   {  /// DDDDWxxxxxxxRDD
      uDSPBufferFree = uDSPBufferRead - uDSPBufferWrite - 1;
      ASSERT( len*2 <= uDSPBufferFree, HWDSPH_ERR_FORCE_ASSERT, 0 );
      memcpy(bDSPBuffer+uDSPBufferWrite, buf, len*2);
      uDSPBufferWrite += len*2;
   }
   else
   {  /// xxxRDDDDDDWxxxx
      uint32 free_1, free_2, write_count;
      free_1 = uDSPBufferSize - uDSPBufferWrite;
      free_2 = uDSPBufferRead - 1;
      uDSPBufferFree = free_1 + free_2;
      ASSERT( len*2 <= uDSPBufferFree, HWDSPH_ERR_FORCE_ASSERT, 0 );
      write_count = ( len*2 > free_1 )? free_1 : len*2;
      memcpy(bDSPBuffer+uDSPBufferWrite, buf, write_count);
      uDSPBufferWrite += write_count;
      if( uDSPBufferWrite==uDSPBufferSize )
         uDSPBufferWrite = 0;
      write_count = len*2 - write_count;
      if( write_count )
         memcpy(bDSPBuffer+uDSPBufferWrite, buf, write_count);
      uDSPBufferWrite += write_count;
   }
}

void VERIFY_DATA_TO_DSP_SAVE_DATA( void )
{
   if( fs_handle != 0 && uDSPBufferRead != uDSPBufferWrite )
   {
      uint32 uDSPBufferUsed;
      uint32 uDataSaved;
      if( uDSPBufferRead < uDSPBufferWrite )
      {  /// xxxRDDDDDDWxxxx
         uDSPBufferUsed = uDSPBufferWrite - uDSPBufferRead;
         FS_Write( fs_handle, bDSPBuffer+uDSPBufferRead, uDSPBufferUsed, &uDataSaved );
         ASSERT( uDSPBufferUsed == uDataSaved, HWDSPH_ERR_FORCE_ASSERT, 0 );
         uDSPBufferRead += uDataSaved;
      }
      else
      {  /// DDDDWxxxxxxxRDD
         uint32 used_1, used_2, write_count;
         used_1 = uDSPBufferSize - uDSPBufferRead;
         FS_Write( fs_handle, bDSPBuffer+uDSPBufferRead, used_1, &uDataSaved );
         ASSERT( used_1 == uDataSaved, HWDSPH_ERR_FORCE_ASSERT, 0 );
         used_2 = uDSPBufferWrite;
         FS_Write( fs_handle, bDSPBuffer, used_2, &uDataSaved );
         ASSERT( used_2 == uDataSaved, HWDSPH_ERR_FORCE_ASSERT, 0 );
         uDSPBufferRead = uDataSaved;
      }
   }
}
#endif

/* ------------------------------------------------------------------------------ */
/*  L1Audio Task                                                                  */
/* ------------------------------------------------------------------------------ */

// extern unsigned short L1D_Audio_ChkDspInitDone();

#ifdef __DHL_MODULE__
void tst_dummy_callback(const uint8* pData, uint32 nLen){
}	
#endif

#if defined(__VOLTE_SUPPORT__)
ExeSemaphoreT sp_handover_mutex; //just for 4G and 4G
ExeSemaphoreT sp4g_dl_mutex;
ExeSemaphoreT sp4g_ul_mutex;
uint32 g_retrieved_events;
#endif

void L1Audio_Task_process(void)
{
	uint32 I;
	// kal_retrieve_eg_events(l1audio.aud_events,0xFFFF,KAL_OR_CONSUME,&retrieved_events,KAL_SUSPEND);
	l1audio.retrieved_events = L1Audio_GetEventGroup(); // retrieved_events;
	l1audio.events_l1FN = SysTimeGetFine().MostSignificant32Bits; //L1I_GetTimeStamp();
	for( I = 0; I < MAX_AUDIO_FUNCTIONS; I++ ) {
		if ( l1audio.retrieved_events & (1<<I) ) {
			l1audio.evHandler[I]( l1audio.evData[I] );
		}
	}
}

// void L1Audio_Task(unsigned argc, void *argv)
void L1Audio_Init(void)
{
   // uint32 retrieved_events;
   uint32 I;
   uint16 initaud_id;

	if(true == l1audio.isInit) { // prevent re-init
#if (defined (SYS_DEBUG_UART_PRINTF) && defined (MTK_DEV_BRING_UP))
		dbg_print("L1Audio_Init(0) \n");
#endif 
		return; 
	}
	
#if 0 // def __DHL_MODULE__ // VOC 
   tst_vc_register_channel(TVCI_VM_LOGGING, tst_dummy_callback);
#endif

#if (defined (SYS_DEBUG_UART_PRINTF) && defined (MTK_DEV_BRING_UP))
	dbg_print("L1Audio_Init(2) \n");
#endif 

#if defined(__VOLTE_SUPPORT__)
	ExeSemaphoreCreate(&sp_handover_mutex, 1);    // sp_handover_mutex = kal_create_enh_mutex( "SP_HANDOVER_ENH_MUTEX" );
	ExeSemaphoreCreate(&sp4g_dl_mutex, 1);    // sp4g_dl_mutex = kal_create_enh_mutex( "SP4G_DL_MUTEX" );
	ExeSemaphoreCreate(&sp4g_ul_mutex, 1);    // sp4g_ul_mutex = kal_create_enh_mutex( "SP4G_UL_MUTEX" );
#endif
	// done TODO: c2k [eventGroup]
   ExeSemaphoreCreate(&audEventSema, 1);  // l1audio.aud_events = kal_create_event_group("L1Audio");   
   l1audio.aud_eventSema = &audEventSema; 
   // end TODO: c2k [eventGroup]
	l1audio.hisr = &audioHisr;
	
#if (defined (SYS_DEBUG_UART_PRINTF) && defined (MTK_DEV_BRING_UP))
	dbg_print("L1Audio_Init(3) \n");
#endif

	ExeHisrCreate(l1audio.hisr, EXE_SPHAUDIO_HISR_THREAD_ID, L1Audio_HISR, EXE_HISR_PRIO_0); // l1audio.hisr = kal_init_hisr(L1AUDIO_HISR); 
#if (defined (SYS_DEBUG_UART_PRINTF) && defined (MTK_DEV_BRING_UP))	
	dbg_print("L1Audio_Init(4) \n");
#endif

	// TODO: c2k [sleep handler]
	/*
   GET_SLEEP_HANDLE();
   
#if ( defined( __CENTRALIZED_SLEEP_MANAGER__ ) && defined(MTK_SLEEP_ENABLE) && !defined(__AUDIO_POWERON_RESET_DSP__) )
   l1audio.md2g_pdn_handle = L1D_MD2G_PWD_GetHandle();
   l1audio.l1sm_handle = L1SM_IntGetHandle();
#endif
	*/
	// end TODO: c2k [sleep handler]
	
#if defined( __UMTS_RAT__ ) || defined( __UMTS_TDD128_MODE__ )
   {
   extern kal_enhmutexid sp_mutex;
   sp_mutex = kal_create_enh_mutex( "SP_MUTEX" );
   }  
#endif   
	

	l1audio.isD2MIntAvalibaleUnderSleep = false;

   l1audio.sleppModeLocker        = 0;
   l1audio.dspUser  = 0;
   l1audio.dsp_slow_idle_counter = 0;
   l1audio.event_flag   = 0;
   l1audio.id_flag      = 0;
   l1audio.media_flag   = 0;
#if (defined (SYS_DEBUG_UART_PRINTF) && defined (MTK_DEV_BRING_UP))	
	dbg_print("L1Audio_Init(5) \n");
#endif
   ExeSemaphoreCreate(&(l1audio.sema), 1); //l1audio.sema = kal_create_sem( "Aud_Sema", 1 );  
#if (defined (SYS_DEBUG_UART_PRINTF) && defined (MTK_DEV_BRING_UP))   
	dbg_print("L1Audio_Init(6) \n");
#endif

   l1audio.hisrMagicFlag = 0;
   for( I = 0; I < MAX_HISR_HANDLER; I++ )
      l1audio.hisrMagicNo[I] = 0;
#if (defined (SYS_DEBUG_UART_PRINTF) && defined (MTK_DEV_BRING_UP))
	dbg_print("L1Audio_Init(7) \n");
#endif
	// [dsp init waiting]
	initaud_id = L1Audio_GetAudioID();
	L1Audio_SetFlag( initaud_id );
	FD216_DRV_DSP_Init();
#if (defined (SYS_DEBUG_UART_PRINTF) && defined (MTK_DEV_BRING_UP))
	dbg_print("L1Audio_Init(8) \n");
#endif
	/*
	// wait for DSP init done
	while( 0 == L1D_Audio_ChkDspInitDone()){
		ExeTaskWait(1); // kal_sleep_task(1);
	}
	*/
	//end TODO: [dsp init waiting]

	// TODO: [cdp]
// #if ( defined( __CENTRALIZED_SLEEP_MANAGER__ )&& defined( MTK_SLEEP_ENABLE ))

// #endif   
	// end TODO: [cdp]
	
#if (defined (SYS_DEBUG_UART_PRINTF) && defined (MTK_DEV_BRING_UP))
	dbg_print("L1Audio_Init(9) \n");
#endif
   DSP_DynamicDownload_Init();
#if (defined (SYS_DEBUG_UART_PRINTF) && defined (MTK_DEV_BRING_UP))
	dbg_print("L1Audio_Init(10) \n");
#endif
	
	// MonTrace( MON_CP_HWD_SPH_TRACE_INI2_ID, 1, 2222);	
   AM_Init();   
#if (defined (SYS_DEBUG_UART_PRINTF) && defined (MTK_DEV_BRING_UP))
	dbg_print("L1Audio_Init(11) \n");
#endif

	
   AFE_Init();
#if (defined (SYS_DEBUG_UART_PRINTF) && defined (MTK_DEV_BRING_UP))
	dbg_print("L1Audio_Init(12) \n");
#endif

#if defined(__AUDIO_POWERON_RESET_DSP__)
   {
      // uint32 _savedMask;
      SysIntDisable(SYS_IRQ_INT);// _savedMask = SaveAndSetIRQMask();
      AFE_Init_status(true);
      AFE_RegisterBackup();
      SysIntEnable(SYS_IRQ_INT); //RestoreIRQMask(_savedMask);
   }
#endif
#if (defined (SYS_DEBUG_UART_PRINTF) && defined (MTK_DEV_BRING_UP))
	dbg_print("L1Audio_Init(13) \n");
#endif

	 
   L1SP_Init();

#if (defined (SYS_DEBUG_UART_PRINTF) && defined (MTK_DEV_BRING_UP))
	dbg_print("L1Audio_Init(14) \n");
#endif


	// TODO: [module init]
	

/*

#if defined(__VOLTE_SUPPORT__)
#if defined(MT6276)
   ktInit( L1Audio_GetAudioID(), 0 );
   toneInit( L1Audio_GetAudioID(), 0 );
#else //#if defined(MT6276)
   for( I = 0; I < TOTAL_TONE_NUM; I++ )
   {
      ktInit( L1Audio_GetAudioID(), I );
      toneInit( L1Audio_GetAudioID(), I );
   }
#endif //#if defined(MT6276)
#endif //#if defined(__VOLTE_SUPPORT__)

   // mediaInit( L1Audio_GetAudioID() );

//    toneLoopbackRecInit(); // remove legency file and function

	
#ifdef __CTM_SUPPORT__
   l1ctm_init();
#endif

#if defined(__DATA_CARD_SPEECH__)
   SP_Strm_Init();
#endif
   memset( &(l1audio.debug_info), 0, sizeof(l1audio.debug_info) );
*/

	SIDETONE_Init();

	BGSND_INIT();


#if defined(__CVSD_CODEC_SUPPORT__) 
   BT_SCO_Init();
#endif

	// end TODO: [module init]
	L1Audio_ClearFlag( initaud_id );
   L1Audio_FreeAudioID( initaud_id );

#if (defined (SYS_DEBUG_UART_PRINTF) && defined (MTK_DEV_BRING_UP))
	dbg_print("L1Audio_Init(15) \n");
#endif

#if !defined(MTK_PLT_MODEM_ONLY) // notify VAL Speech that HWD speech init done 
	ExeMsgSend(EXE_VAL_ID, VAL_MAILBOX, VAL_SPH_HWDPART_INIT_DONE_MSG, NULL, 0);
#endif 

	l1audio.isInit = true;

	// done TODO: [event waiting]
	#if 0 
   while( 1 ) {
#if VERIFY_DATA_TO_DSP
      VERIFY_DATA_TO_DSP_SAVE_DATA();
#endif

		// TODO: move into the function		
		// L1Audio_Task_process();
		/*
      kal_retrieve_eg_events(l1audio.aud_events,0xFFFF,KAL_OR_CONSUME,&retrieved_events,KAL_SUSPEND);
      l1audio.events_l1FN = SysTimeGetFine().MostSignificant32Bits; //L1I_GetTimeStamp();
      for( I = 0; I < MAX_AUDIO_FUNCTIONS; I++ ) {
         if ( l1audio.retrieved_events & (1<<I) ) {
            l1audio.evHandler[I]( l1audio.evData[I] );
         }
      }
      // if( (l1audio.sleppModeLocker == 0))
      //   SLEEP_UNLOCK();
      	*/
   }
	#endif 
	// end TODO: [event waiting]
}


bool L1Audio_IsTaskInitilized(void)	
{
	return l1audio.isInit;
}

void L1Audio_ExeEventHandler( uint32 event )
{
   ASSERT( event < MAX_AUDIO_FUNCTIONS, HWDSPH_ERR_FORCE_ASSERT, event );
   l1audio.evHandler[event]( l1audio.evData[event] );
}

void L1Audio_ResetDevice(void)
{
	// TODO: c2k 
   // AFE_Init();
}
/* ------------------------------------------------------------------------------ */



void l1audio_console(uint8 index, char *string)
{
  
}


void l1audio_console_handler(char *string)
{
   //(void)string; //just for integrating MCU TONE
   //int ii=atoi(string);
	// TODO: c2k [trace]
   // kal_prompt_trace(MOD_L1SP, "l1audio_console_handler %d", ii);
   {   
//    void SpeechDVT(int ii);
//     SpeechDVT(ii);
   }

/*
   (void)string;
	if(strcmp(string, "speechOn")==0){
		L1SP_Speech_On(0);
	} else if(strcmp(string, "speechOff")==0) {
		L1SP_Speech_Off();
	} else if(strcmp(string, "get2gRF")==0) {
		uint16 L1D_GetRF(uint16 mode);
	   uint16 RF_2G = L1D_GetRF(MML1_RF_2G);
	   L1Audio_Msg_L1D_GetRF(RF_2G);
	}
#if defined(__ENABLE_SPEECH_DVT__)
        extern void Speech_DVT_Test_Main(kal_char *string);
        Speech_DVT_Test_Main(string);
#endif // defined(__ENABLE_SPEECH_DVT__)*/
}

// TODO: c2k MED prototype
/* ----------------------------------------------------------------------------- */
/*  Function ptr prototype [HAL rule]                                            */
/* ----------------------------------------------------------------------------- */
//[MED]
/*
static fp_audio_alloc_aud_mem      audio_alloc_aud_mem = NULL;
static fp_audio_alloc_aud_mem      audio_alloc_aud_mem_cacheable= NULL;
static fp_audio_free_aud_mem       audio_free_aud_mem = NULL;
static fp_send_proc_call_req       send_proc_call_req = NULL;
static fp_send_proc_call_req       send_proc_call_req2 = NULL;
static fp_audio_set_path_volume    audio_set_path_vol = NULL;
static fp_audio_get_active_mode    audio_get_active_mode = NULL;
static fp_get_meta_data_file       audio_get_meta_file = NULL;
static fp_get_meta_data_array      audio_get_meta_array = NULL;

void Audio_MedFuncReg(Media_Func_Reg_Type *func){
      audio_alloc_aud_mem           = (fp_audio_alloc_aud_mem)  func->alloc_mem;
      audio_alloc_aud_mem_cacheable = (fp_audio_alloc_aud_mem)  func->alloc_mem_cacheable;
      audio_free_aud_mem            = (fp_audio_free_aud_mem)   func->free_mem;
      audio_set_path_vol            = (fp_audio_set_path_volume)func->set_path_volume;
      audio_get_active_mode         = (fp_audio_get_active_mode)func->get_active_mode;
      send_proc_call_req            = (fp_send_proc_call_req)   func->send_proc_call;
      send_proc_call_req2           = (fp_send_proc_call_req)   func->send_proc_call2;
      audio_get_meta_file           = (fp_get_meta_data_file)   func->get_meta_file;
      audio_get_meta_array          = (fp_get_meta_data_array)  func->get_meta_array;
}

void *audio_alloc_ext_mem(int32 size, char* file_p, long line_p){
   void *ptr = NULL;
   if (size == 0){
        return NULL;
     }
   ptr = (void*) audio_alloc_aud_mem(size,file_p,line_p);
   if(ptr){
        memset(ptr, 0, size);
   }
	// TODO: c2k [trace]
   // kal_wap_trace(MOD_L1SP, TRACE_INFO, "[AUD][MEM] get buffer from: %s, %d", file_p, line_p);
   return ptr;
}

void *audio_alloc_ext_mem_cacheable(int32 size, char* file_p, long line_p){
   void *ptr = NULL;
   if (size == 0){
        return NULL;
   }
   ptr = (void*) audio_alloc_aud_mem_cacheable(size,file_p,line_p);
   if(ptr){
        memset(ptr, 0, size);
   }
	// TODO: c2k [trace]
   // kal_wap_trace(MOD_L1SP, TRACE_INFO, "[AUD][MEM] get buffer from: %s, %d", file_p, line_p);
   return ptr;
}

void audio_free_ext_mem(void **ptr, char* file_p, long line_p){
   audio_free_aud_mem(ptr,file_p,line_p);
   kal_wap_trace(MOD_L1SP, TRACE_INFO, "[AUD][MEM] free buffer from: %s, %d", file_p, line_p);
}

void L1Audio_InProcCall2(in_proc_call_type func, uint32 func_arg1, void* func_arg2)
{
#ifndef MED_NOT_PRESENT
   send_proc_call_req2(MOD_L1SP, func, func_arg1, func_arg2);
#endif
}

void L1Audio_InProcCall(in_proc_call_type func, uint32 func_arg1, void* func_arg2)
{
//#ifndef MED_NOT_PRESENT
   send_proc_call_req(MOD_L1SP, func, func_arg1, func_arg2);
//#endif
}

void aud_set_volume_internal(uint8 audio_mode){
   audio_set_path_vol(audio_mode);
}

uint8 aud_get_mode_internal(void){
  return audio_get_active_mode();
}

*/
// end TODO: c2k MED prototype


#if 0// unused in c2k 

#if defined(__SMART_PHONE_MODEM__)
#include "task_config.h"
#include "ccci.h"

extern void SpcIO_A2M_hisr(CCCI_BUFF_T *bufp);
extern void SpcIO_M2A_hisr(CCCI_BUFF_T *bufp);
extern void SpcIO_Init(void);
#endif

/***********************************************************/
/* Export to MED/AUDL task                                 */
/***********************************************************/

bool SP_Drv_Init_Bootup(void)
{
#if defined (__SMART_PHONE_MODEM__) // unused in c2k
   int32 ret;
   ret = ccci_owner_init(CCCI_PCM_CHANNEL_ACK, kal_get_task_by_moduleID(MOD_MED), SpcIO_A2M_hisr);
   if (ret != CCCI_SUCCESS) {
       ASSERT(0, HWDSPH_ERR_FORCE_ASSERT, 0);
   }
   ret = ccci_owner_init(CCCI_PCM_CHANNEL, kal_get_task_by_moduleID(MOD_MED), SpcIO_M2A_hisr);
   if (ret != CCCI_SUCCESS) {
       ASSERT(0, HWDSPH_ERR_FORCE_ASSERT, 0);
   }
#endif

   return true;
}

bool SP_Drv_Init_Task(void)
{
#if defined (__SMART_PHONE_MODEM__) // unused in c2k 
   SpcIO_Init();
#endif
   
   return true;
}
#endif 
