/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

#include "monapi.h"
#include "monids.h"

#include "reg_base.h"
#include "hwdpmuctrlintf.h"
#include "hwdApAudSysConfig.h"
#include "hwdspherr.h"

#if defined(MTK_PLT_AUDIO)
#define WriteREG(_addr, _value) (*(volatile uint32 *)(_addr) = (_value))
#else 
#define WriteREG(_addr, _value)                MonTrace(MON_CP_HWD_SPH_REG_WRITE32_TRACE_ID, 2, _addr, _value)
#endif


// #define AUDIO_PWR                       (CKSYS_BASE     + 0x629c)

// #define AP_AUDSYS_BASE   0xA1220000

#define AUDIO_TOP_CON0                  (AP_AUDSYS_base + 0x0000)
#define AUDIO_TOP_CON1                  (AP_AUDSYS_base + 0x0004)
#define AUDIO_TOP_CON2                  (AP_AUDSYS_base + 0x0008)
#define AUDIO_TOP_CON3                  (AP_AUDSYS_base + 0x000C)
#define AFE_DAC_CON0                    (AP_AUDSYS_base + 0x0010)
#define AFE_DAC_CON1                    (AP_AUDSYS_base + 0x0014)
#define AFE_I2S_CON                     (AP_AUDSYS_base + 0x0018)
#define AFE_DAIBT_CON0                  (AP_AUDSYS_base + 0x001C)
                                        
#define AP_AFE_CONN0                    (AP_AUDSYS_base + 0x0020)
#define AP_AFE_CONN1                    (AP_AUDSYS_base + 0x0024)
#define AP_AFE_CONN2                    (AP_AUDSYS_base + 0x0028)
#define AP_AFE_CONN3                    (AP_AUDSYS_base + 0x002C)
#define AP_AFE_CONN4                    (AP_AUDSYS_base + 0x0030)

#define AP_AFE_CONN5                    (AP_AUDSYS_base + 0x005C)
#define AP_AFE_CONN6                    (AP_AUDSYS_base + 0x00BC)


#define AP_AFE_CONN9                    (AP_AUDSYS_base + 0x0468)
#define AP_AFE_CONN10                   (AP_AUDSYS_base + 0x046C)

#define AFE_I2S_CON1                    (AP_AUDSYS_base + 0x0034)
#define AFE_I2S_CON2                    (AP_AUDSYS_base + 0x0038)
#define AFE_MRGIF_CON                   (AP_AUDSYS_base + 0x003C)
#define AFE_I2S_CON3                    (AP_AUDSYS_base + 0x004C)

#define AFE_ADDA_DL_SRC2_CON0           (AP_AUDSYS_base + 0x0108)
#define AFE_ADDA_DL_SRC2_CON1           (AP_AUDSYS_base + 0x010C)
#define AFE_ADDA_UL_SRC_CON0            (AP_AUDSYS_base + 0x0114)
#define AFE_ADDA_UL_SRC_CON1            (AP_AUDSYS_base + 0x0118)

#define AFE_ADDA_UL_DL_CON0             (AP_AUDSYS_base + 0x0124)
#define AFE_ADDA_NEWIF_CFG0             (AP_AUDSYS_base + 0x0138)
#define AFE_ADDA_NEWIF_CFG1             (AP_AUDSYS_base + 0x013C)

#define AFE_SIDDETONE_DEBUG             (AP_AUDSYS_base + 0x01D0)
#define AFE_SIDDETONE_MON               (AP_AUDSYS_base + 0x01D4)
#define AFE_SIDDETONE_CON0              (AP_AUDSYS_base + 0x01E0)
#define AFE_SIDDETONE_COEFF             (AP_AUDSYS_base + 0x01E4)
#define AFE_SIDDETONE_CON1              (AP_AUDSYS_base + 0x01E8)
#define AFE_SIDDETONE_GAIN              (AP_AUDSYS_base + 0x01EC)

#define AFE_SGEN_CON0                   (AP_AUDSYS_base + 0x01F0)
#define AFE_TOP_CON0                    (AP_AUDSYS_base + 0x0200)

#define AFE_GAIN2_CON0                  (AP_AUDSYS_base + 0x0428)
#define AFE_GAIN2_CONN                  (AP_AUDSYS_base + 0x0438)
#define AFE_GAIN2_CONN2                 (AP_AUDSYS_base + 0x0440)
#define AFE_GAIN2_CONN3                 (AP_AUDSYS_base + 0x0444)

#define PCM_INTF_CON1                   (AP_AUDSYS_base + 0x0530)
#define PCM_INTF_CON2                   (AP_AUDSYS_base + 0x0538)
#define PCM_INTF_CON                    (AP_AUDSYS_base + 0x053C)


#if (SYS_CUST_PLT == SYS_PLT_MT6755_EVB)

// ACC mode
uint32 writePMIC_Part1_Ul[18] = {
	// Jade
	
	0x0D060000,	//AUDDEC_ANA_CON10
	0x0D040155,	//AUDDEC_ANA_CON9
	0x029E0001,	//TOP_CLKSQ
	0x029C0001,	//TOP_CLKSQ
	0x0D0E0040,	//AUDENC_ANA_CON3
	0x0D0E0040, //AUDENC_ANA_CON3

	0x0D1A0021,	//AUDENC_ANA_CON9
	0x0D1A2121,	//AUDENC_ANA_CON9
	0x0D080311,	//AUDENC_ANA_CON0
	0x0D0A0331,	//AUDENC_ANA_CON1
	0x0D085311,	//AUDENC_ANA_CON0
	0x0D0A5331,	//AUDENC_ANA_CON1

	0x023E6000,	//TOP_CKPDN_CON0_CLR
	0x20140040,	//AUDIO_TOP_CON0
	0x20120000,
	0x20000001,	//AFE_UL_DL_CON0

	0x200A0002,	////AFE_UL_SRC_CON0_H
	0x200C0001,	//AFE_UL_SRC_CON0_L
	
};
#else
uint32 writePMIC_Part1_Ul[22] = {
// Jade
	0x0D060000,	//AUDDEC_ANA_CON10
	0x0D040155,	//AUDDEC_ANA_CON9
	0x029E0001,	//TOP_CLKSQ
	0x029C0001,	//TOP_CLKSQ
	0x0D0E0040,	//AUDENC_ANA_CON3
	0x0D0E0040, //AUDENC_ANA_CON3
// DCC mode
	0x20902060,
	0x20902061,	
	0x0D1A0021,	//AUDENC_ANA_CON9
	0x0D1A2121,	//AUDENC_ANA_CON9
	0x0D080317,	//AUDENC_ANA_CON0
	0x0D0A0337,	//AUDENC_ANA_CON1
	0x0D085317,	//AUDENC_ANA_CON0
	0x0D085313,	//AUDENC_ANA_CON1
	0x0D0A5337,
	0x0D0A5333,
//	
	0x023E6000,	//TOP_CKPDN_CON0_CLR
	0x20140040,	//AUDIO_TOP_CON0
	0x20120000,
	0x20000001,	//AFE_UL_DL_CON0

	0x200A0002,	////AFE_UL_SRC_CON0_H
	0x200C0001,	//AFE_UL_SRC_CON0_L
	
};
#endif


uint32 writePMIC_Part2_Dl[3] = {	
	0x0D040155,
	0x029C0001,	//TOP_CLKSQ_SET
	0x023EF000,	//TOP_CKPDN_CON0_CLR	
	//delay 0.1 ms	
};
uint32 writePMIC_Part3_Dl[3] = {		
	0x20148000,	//AUDIO_TOP_CON0
	0x20981515,	//AFE_NCP_CFG1
	0x20968C01,	//AFE_NCP_CFG0
	//delay 0.1 ms	
};

uint32 writePMIC_Part4_Dl[11] = {	
	0x20148000,	//AUDIO_TOP_CON0
	0x20200006,	//AFUNC_AUD_CON2
	0x201CC3A1,	//AFUNC_AUD_CON0
	0x20200003,	//AFUNC_AUD_CON2
	0x2020000B,	//AFUNC_AUD_CON2
	0x2008001E,	//AFE_DL_SDM_CON1
	0x20000001,	//AFE_UL_DL_CON0

	0x20383330,	////
	0x20023300,	////AFE_DL_SRC2_CON0_H
	0x20023300,	////AFE_DL_SRC2_CON0_H
	
	0x20040001,	//AFE_DL_SRC2_CON0_L
};

/*
uint32 writePMIC_Part5_sineGen[3] = {
	
// ;0x20120000  //;AFE_TOP_CON0
// ;0x20400080  //;AFE_SGEN_CFG0
// ;0x20420101  //;AFE_SGEN_CFG1
}
*/


uint32 writePMIC_Part6_DlOthers[13] = {
	0x0D04A155,	//AUDDEC_ANA_CON9
	0x0D060100,	//AUDDEC_ANA_CON10
	0x08000000,	//ZCD_CON0
	0x0CF22080,	//////// none --> add 
	0x0CFE0400, //AUDDEC_ANA_CON0
	0x0D04A055,	//AUDDEC_ANA_CON9
	0x0806001F,	//ZCD_CON3
	0x0CF40100,	//AUDDEC_ANA_CON1
	0x0D04A255,	//AUDDEC_ANA_CON9
	0x0CF22089,	//AUDDEC_ANA_CON0
	0x0CF22109,	//AUDDEC_ANA_CON0
	0x0CF22119,	//////// 0x0CFE0300  --> none
	0x08060009,
	// delay 0.1 ms
};

void PMIC_AudioConfig(void)
{
	#if (SYS_CUST_PLT == SYS_PLT_MT6755_EVB) 
		HwdPmicCtrlWrapIntfWriteImmed(writePMIC_Part1_Ul, 18);
	#else
		HwdPmicCtrlWrapIntfWriteImmed(writePMIC_Part1_Ul, 22);
	#endif
	HwdPmicCtrlWrapIntfWriteImmed(writePMIC_Part2_Dl, 3);
	//WaitForPmicImedDone();
	//Hwd32kDelay(1)
	SysDelayUs(100);
	HwdPmicCtrlWrapIntfWriteImmed(writePMIC_Part3_Dl, 3);
	SysDelayUs(100);
	HwdPmicCtrlWrapIntfWriteImmed(writePMIC_Part4_Dl, 11);
	// HwdPmicCtrlWrapIntfWriteImmed(writePMIC_Part5_sineGen , 20);
	HwdPmicCtrlWrapIntfWriteImmed(writePMIC_Part6_DlOthers, 13);
	SysDelayUs(100);
}

void ApAudSys_config(void)
{
	ASSERT(0, HWDSPH_ERR_FORCE_ASSERT, 0); // for pre-release fail, copy pmic config from MT6755, 
																				 // MT6750 pmic config find AP Speech
	
	WriteREG(AUDIO_TOP_CON0, 0x00004000); //script=0x60004000
	WriteREG(AFE_TOP_CON0, 0x00000000); //not in script

	WriteREG(AP_AFE_CONN0, 0x00000000); // from script
	WriteREG(AP_AFE_CONN1, 0x02000000); // MD2, I09->O03
	WriteREG(AP_AFE_CONN2, 0x24000200); // MD2,	I04->O08, I03->O07, I09->O04
	WriteREG(AP_AFE_CONN3, 0x08000000); // MD1, I14->O03
	WriteREG(AP_AFE_CONN4, 0x00012001); // MD1, I04->O18, I03->O17, I14->O04
	WriteREG(AP_AFE_CONN5, 0x00000000);
	WriteREG(AP_AFE_CONN6, 0x00000000);

	// sidetone
	WriteREG(AFE_SIDDETONE_CON1, 0x00000000);

	WriteREG(PCM_INTF_CON1, 0x0000006F); // MD2
	WriteREG(PCM_INTF_CON2, 0x00000100); 
	WriteREG(PCM_INTF_CON,  0x0000004F); // MD1


	// new interface
	// WriteREG(AFE_ADDA_NEWIF_CFG0, 0x03F87201);
	WriteREG(AFE_ADDA_NEWIF_CFG1, 0x03117580); // 0x1122013c

 	WriteREG(AFE_I2S_CON1, 0x00000409);

 	WriteREG(AFE_ADDA_UL_SRC_CON0, 0x000A0001); // 0x11220114
 	WriteREG(AFE_ADDA_UL_SRC_CON1, 0x00000000);
 	WriteREG(AFE_ADDA_DL_SRC2_CON0, 0x3f001821); // 0x11220108
 	WriteREG(AFE_ADDA_DL_SRC2_CON1, 0xFFFF0000);

 	WriteREG(AFE_ADDA_NEWIF_CFG0, 0x03f87200); // 0x11220138, from script
 	WriteREG(AFE_ADDA_NEWIF_CFG1, 0x03117580); // 0x1122013C, from script

 	WriteREG(AFE_ADDA_UL_DL_CON0, 0x00000001); // 0x11220124

 	WriteREG(AFE_DAC_CON1, 0x00000400); //0x11220014, from script

	//WriteREG(AFE_I2S_CON, 0x10000009); //0x11220018,  from script
	WriteREG(AFE_DAC_CON0, 0x00001001); // 0x11220010, from script, power on

// PMIC 
	PMIC_AudioConfig();


}

