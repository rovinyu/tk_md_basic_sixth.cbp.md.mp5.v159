/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

#ifndef _HWD_SP_ENHANCE_H
#define _HWD_SP_ENHANCE_H

#include "hwdsph.h"
#include "hwdcommon_def.h"

typedef enum {
	SPH_ENH_AND_FIR_SCENE_NORMAL = 0, //from 0 to 8, it is one to one mapping to speech mode. ie. SPH_MODE_NORMAL to SPH_MODE_LINEIN_VIA_BT_CORDLESS
	SPH_ENH_AND_FIR_SCENE_EARPHONE,
	SPH_ENH_AND_FIR_SCENE_LOUDSPK,
	SPH_ENH_AND_FIR_SCENE_BT_EARPHONE,
	SPH_ENH_AND_FIR_SCENE_BT_CORDLESS,
	SPH_ENH_AND_FIR_SCENE_BT_CARKIT, 
	SPH_ENH_AND_FIR_SCENE_AUX1,
	SPH_ENH_AND_FIR_SCENE_AUX2,
	SPH_ENH_AND_FIR_SCENE_HAC,
	SPH_ENH_AND_FIR_SCENE_USB,
    
	SPH_ENH_AND_FIR_SCENE_UNDEF = 0xFFFF,  
} SPH_ENH_AND_FIR_SCENE;

typedef enum {
	SPH_ENH_AND_FIR_UPDATE_TYPE_ALL = 0,
	SPH_ENH_AND_FIR_UPDATE_TYPE_NB_ENH_MODE = 0x2,
	SPH_ENH_AND_FIR_UPDATE_TYPE_NB_FIR = 0x4,	
	SPH_ENH_AND_FIR_UPDATE_TYPE_WB_ENH_MODE = 0x8,
	SPH_ENH_AND_FIR_UPDATE_TYPE_WB_FIR = 0x10,
	
} SPH_ENH_AND_FIR_UPDATE_TYPE;

#define SPH_FIR_COEFF_NORMAL           0
#define SPH_FIR_COEFF_HEADSET          1
#define SPH_FIR_COEFF_HANDFREE         2
#define SPH_FIR_COEFF_BT               3
#define SPH_FIR_COEFF_VOIP_NORMAL      4
#define SPH_FIR_COEFF_VOIP_HANDFREE    5
#define SPH_FIR_COEFF_HAC              6
#define SPH_FIR_COEFF_USB              7

// speech enhancement control functions
// #define SPE_ES_FLAG     (1 << 0) 
#define SPE_AEC_FLAG    (1 << 1) 
// #define SPE_EES_FLAG    (1 << 2) 
#define SPE_UL_NR_FLAG  (1 << 3) 
#define SPE_DL_NR_FLAG  (1 << 4) 
// #define SPE_TDDNC_FLAG  (1 << 5) // REMIND: NO TDDNC under C2k
#define SPE_DMNR_FLAG   (1 << 6)
#define SPE_AGC_FLAG    (1 << 7)
#define NUM_OF_SPH_FLAG 8 // if you add new SPE, please remind to revind the table define from uint8 to uint 16

//speech enhancement mask for l1sp.spe_mask usage
// #define SPH_ENH_MASK_ES    SPE_ES_FLAG
#define SPH_ENH_MASK_AEC   SPE_AEC_FLAG
// #define SPH_ENH_MASK_EES   SPE_EES_FLAG
#define SPH_ENH_MASK_ULNR  SPE_UL_NR_FLAG
#define SPH_ENH_MASK_DLNR  SPE_DL_NR_FLAG
// #define SPH_ENH_MASK_TDDNC SPE_TDDNC_FLAG
#define SPH_ENH_MASK_DMNR  SPE_DMNR_FLAG
#define SPH_ENH_MASK_AGC  SPE_AGC_FLAG
#define SPH_ENH_MASK_SIDETONE (1 << NUM_OF_SPH_FLAG)
#define SPH_ENH_MASK_NOTCH_FILTER (1 << (NUM_OF_SPH_FLAG+1))
#define SPH_ENH_MASK_ECHO_REF (1 << (NUM_OF_SPH_FLAG+2))



// ===============================

uint8 SPE_GetSpeechMode(void);
void L1SP_SetSpeechEnhanceAndFir(uint32 scene, uint32 updatedCoeff);
void SetSpeechEnhancement( bool ec );
void L1SP_EnableSpeechEnhancement( bool enable );
bool spe_isSpeFweOn(void);

void SPE_Init( void );
void SPE_TurnOnProcess(uint16 on_state);
void SPE_TurnOffProcess(uint16 off_state);
void SPE_Clear_DLL_Entry(uint16 cur_state);

uint16 *SPE_getAllWbSpeechModePara(void);
int16 *SPE_getAllWbSpeechFirCoeff_InputOnly(void);
int16 *SPE_getAllWbSpeechFirCoeff_OutputOnly(void);

void SPE_LoadDmnrCoeffs(int16 dmnr_para[NUM_DMNR_PARAM]); //enable only when "defined(__DUAL_MIC_SUPPORT__)"
void SPE_LoadDmnrLspCoeffs(kal_int16 dmnr_lsp_para[NUM_DMNR_PARAM]); 
void SPE_LoadWbDmnrCoeffs(int16 wb_dmnr_para[NUM_WB_DMNR_PARAM]); //enable only when "defined(__DUAL_MIC_SUPPORT__) && defined(__AMRWB_LINK_SUPPORT__)" 
void SPE_LoadWbDmnrLspCoeffs(kal_int16 wb_dmnr_lsp_para[NUM_WB_DMNR_PARAM]);
void SPE_LoadSpeechPara( uint16 c_para[NUM_COMMON_PARAS], uint16 m_para[NUM_MODE_PARAS],uint16 v_para[NUM_VOL_PARAS],
	uint16 m_paraWb[NUM_MODE_PARAS]);

void spe_updateSpeAppMask(uint8 updateFlags, bool enable);

void spe_updateSpeUsrMaskWithWholeValue(uint16 newValue);
void spe_updateSpeUsrSubMaskWithWholeValue(uint16 newValue);

void l1sp_setAllSpeechModePara(uint16 * speech_mode_para, int16 length);
void l1sp_setAllSpeechFirCoeff_InputOnly(int16 *speech_input_FIR_coeffs, int16 length);
void l1sp_setAllSpeechFirCoeff_OutputOnly(int16 *speech_output_FIR_coeffs, int16 length);

void l1sp_setAllWbSpeechModePara(uint16 * speech_mode_para, int16 length);
void l1sp_setAllWbSpeechFirCoeff_InputOnly(int16 *speech_input_FIR_coeffs, int16 length);
void l1sp_setAllWbSpeechFirCoeff_OutputOnly(	int16 *speech_output_FIR_coeffs, int16 length);

void L1SP_SetDMNRPara( const int16 DMNR_para[NUM_DMNR_PARAM] );
void L1SP_SetWbDMNRPara( const int16 WB_DMNR_para[NUM_WB_DMNR_PARAM] );
void L1SP_SetLSpkDMNRPara( const int16 LSpk_DMNR_para[NUM_DMNR_PARAM] );
void L1SP_SetLSpkWbDMNRPara( const int16 LSpk_Wb_DMNR_para[NUM_WB_DMNR_PARAM] );

void spe_setHacSpeechModePara(uint16 * modePara, int16 length);
void spe_setHacSpeechFirCoeff_InputOnly(int16 *fIR_coeffs, int16 length);
void spe_setHacSpeechFirCoeff_OutputOnly(int16 *fIR_coeffs, int16 length);
void spe_setHacWbSpeechModePara(uint16 * modePara, int16 length);
void spe_setHacWbSpeechFirCoeff_InputOnly(int16 *fIR_coeffs, int16 length);
void spe_setHacWbSpeechFirCoeff_OutputOnly(int16 *fIR_coeffs, int16 length);
void spe_setHacModeNeeded(bool isNeed);
void SPE_MagiClarityData(short MagiClarityData[32]);

// ----------------------------------------------------------------------------
// DSP filter/function related parameter/coefficient setting
// ----------------------------------------------------------------------------
void spe_setMagiConParam(uint16 *nbParam, uint16 *wbParam);
void spe_setNotchFilterParam(int16 *param, bool is2In1Spk);

void SPE_SetActiveEchoRefInfo(uint16 sphMode);
void SPE_SetEchoRefInfo(bool isEchoRefOn, uint16 echoRefMicIndex, uint16 delayAndSwitch4Dsp, uint16 sphMode);
bool SPE_AgcInfo_IsNxpPaOn(void);
bool SPE_AgcInfo_IsUSBEchoRefOn(void);
void spe_AGC_config(bool isOn, bool isEchoRefOn);

void SetSpeechEnhancement( bool ec );
#if defined(MTK_AUDIO_HIERARCHICAL_PARAM_SUPPORT) 
void spe_setMagiCon(void);
void spe_DisableMagiCon(void);
void SPE_Enable_DLL_Entry(uint16 cur_state,uint16 path);
void SPE_SetDynamicStatemachine(void);
void SetDynamicParToDSP(uint32 IDnumber);
bool isSPEEnable(void);
void Set2GDynPar(void);
void SetC2KDynPar(void);

void SPE_SetSALEnhNBFlag(void);
void SPE_SetSALEnhWBFlag(void);




#endif
void SetSPEnhancePath(uint8 path);
void spe_AGC_config_dc(bool isOn, bool isEchoRefOn);

typedef enum{
	SpeechEnh_All = 0, //default path : UL+DL
	SpeechEnh_UL, 
	SpeechEnh_DL, 
}SpeechEnh_path; 

#endif // _HWD_SP_ENHANCE_H
