/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2005
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE. 
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*******************************************************************************
 *
 * Filename:
 * ---------
 *	afe.c
 *
 * Project:
 * --------
 *   MAUI
 *
 * Description:
 * ------------
 *   Audio Front End
 *
 * Author:
 * -------
 *	Phil Hsieh
 *
 *------------------------------------------------------------------------------
 * $Revision:   1.21  $
 * $Modtime:   Jul 08 2005 16:31:52  $
 * $Log:   //mtkvs01/vmdata/Maui_sw/archives/mcu/l1audio/afe.h-arc  $
 *
 * 07 03 2014 fu-shing.ju
 * [MOLY00071405] Merge K2 Speech DVT code
 * Merge K2 Speech DVT code.
 *
 * .
 *
 *
 *******************************************************************************/
#ifndef HWD_AFE_H
#define HWD_AFE_H


// TODO: c2k
// [regAccess]

/*
============================================================================================================
------------------------------------------------------------------------------------------------------------
||                        Chapter:  INCLUDE FILES
------------------------------------------------------------------------------------------------------------
============================================================================================================
*/

#include "hwdafe_def.h"
//#include "hwdaudio_def.h"
#include "hwdsph.h"

#include "monapi.h"
#include "monids.h"

#if 0
#include "kal_general_types.h"
#include "kal_public_api.h"
#include "reg_base.h"
#include "kal_trace.h"
#include "l1audio_trace.h"
#include "l1sp_trc.h"
#include "audcoeff_default.h"
#include "afe_def.h"
#include "dcl.h"
#include "us_timer.h"
#include "speech_def.h"

#if defined(__BT_SUPPORT__)
   #include "bt_api.h"
#endif

#endif

/*
============================================================================================================
------------------------------------------------------------------------------------------------------------
||                        Chapter: Data Types and Structures
------------------------------------------------------------------------------------------------------------
============================================================================================================
*/


//=============================================================================================
//                  Section: Enum AFE_STATE_T
//=============================================================================================
typedef enum
{
   AFE_STATE_OFF = 0,
   AFE_STATE_ON,
   AFE_STATE_IDLE_OFF,
   AFE_STATE_CLASS_D_OFF,
   AFE_STATE_MAX
}  AFE_STATE_T;


//=============================================================================================
//                  Section: Structure DelayCmd
//=============================================================================================
typedef struct {
   volatile uint32*  addr;
   uint32            val;
} DelayCmd;

//=============================================================================================
//                  Section: Structure RegBackup
//=============================================================================================
typedef struct {
#if (defined(MT6595) || defined(MT6752) || defined(MTK_PLT_AUDIO) || defined(MTK_DEV_AUDIO))
   uint32     VMCU_CON;
   uint32     VMCU_CON1;
   uint32     PCM_CON0;
   uint32     AMCU_CON0;
   uint32     AMCU_CON1;
   uint32     IRQ_CON1;
   uint32     MCU_CON0;
   uint32     MCU_CON1;
   uint32     CONN0;
      
#elif 0 //(defined(MT6280) || defined(MT6589) || defined(MT6572) || defined(MT6582) || defined(MT6290))
   uint16     VMCU_CON;
   uint16     VDB_CON;
   uint16     VLB_CON;
   uint16     AMCU_CON0;
   uint16     AMCU_CON1;
   uint16     EDI_CON;
   uint16     AMCU_CON2;
   uint16     DAC_TEST;
   uint16     VMCU_CON1;
   uint16     VMCU_CON2;
#else 
// 	#error Please add the register needs to backup and implement function _AfeRegisterBackupByChip() and _AfeRegisterStoreByChip() by chip
#endif 
} RegBackup;

//=============================================================================================
//                  Section: Structure AFE_STRUCT_T (afe main structure)
//=============================================================================================
typedef struct {

	/// AFE Work usage flag (means 8k/path)
   /// bin n: aud function n
   /// [Set|Clear] by AFE_Turn[On|Off]PathWork
	uint16     pathWork_flag;

 

#ifndef ANALOG_AFE_PATH_EXIST	// only digital path
	int16      digitOnly_mic_volume; // actural value to DSP sw "AGC" gain!!
#endif
   
   struct   {
      /// L1SP_SPEAKER1, L1SP_SPEAKER2, or L1SP_LOUDSPEAKER
      /// Set by AFE_SetOutputDevice

      uint8   out_dev;
      uint8   volume;
		int16   digitOnly_digital_gain; // actural value to DSP 

      bool    mute;
   } aud[L1SP_MAX_AUDIO];
   uint16     aud_id;
	int16      digitOnly_sphEnhRef_digital_gain;
	
   bool       gpio_lock;
   bool       ext_op_on;

   bool       refresh;
   bool       loopback; 

   bool       bt_flag;

   bool       afe_init;
   uint8      voice8kMode;
   uint32     audio_fs;
   //RINGBUFFER_T(DelayCmd,8)  regq;

   AFE_STATE_T    afe_state; 

   int16      ext_op_delay; 
   bool       toneLoopbackRec;

#if __RELOAD_HW_COEFF__
   //for 65nm clock gating, digital AFE register backup
   RegBackup      regbak;
#endif

#if __CTIRQ_SLEEP_PLATFORM__
   kal_timerid    timer;
#endif
} AFE_STRUCT_T;

/*
============================================================================================================
------------------------------------------------------------------------------------------------------------
||                        Chapter: Special Configurations for Specific Usage
------------------------------------------------------------------------------------------------------------
============================================================================================================
*/

//=============================================================================================
//                   Section: Debug and Unit Test Configuration
//=============================================================================================
//#define __AFE_DEBUG__

#ifdef __AFE_DEBUG__
   #define afe_prompt_trace  kal_prompt_trace

static kal_char * _aszAfeState[] =
{
   "AFE_STATE_OFF",
   "AFE_STATE_ON",
   "AFE_STATE_IDLE_OFF",
   "AFE_STATE_CLASS_D_OFF"
};

#else
   #define afe_prompt_trace(...) 
#endif

#if (defined(MT6595) || defined(MT6752))
//#define MDAFE_DVT_CASE1 1
//#define MDAFE_DVT_CASE2 1
//#define MDAFE_DVT_CASE3 1

//#define APAUDSYS_DVT_8K 1
//#define APAUDSYS_DVT_16K 1
//#define APAUDSYS_DVT_32K 1

//#define USE_ASRC 1

//#define APAUDSYS_DVT_RF2 1
#endif
/*
============================================================================================================
------------------------------------------------------------------------------------------------------------
||                        Chapter: Definitions by Features (Common Feature for all chips)
------------------------------------------------------------------------------------------------------------
============================================================================================================
*/

#if defined(MT6280) || defined(MT6589) || defined(MT6572) || defined(MT6582) || defined(MT6290) || defined(MT6592) || defined(MT6571)
#define MDAFE_2IN_1OUT
#else // denali is here
#define MDAFE_6IN_2OUT
#endif

//=============================================================================================
//                  Section: Feature Power Down Control Registers Settings
//=============================================================================================
#define PDN_CON2_VAFE       0x0100
#define PDN_CON2_AAFE       PDN_CON2_VAFE

/*
============================================================================================================
------------------------------------------------------------------------------------------------------------
||                        Chapter: Definitions by Features (Feature for specific chips)
------------------------------------------------------------------------------------------------------------
============================================================================================================
*/


/*
============================================================================================================
------------------------------------------------------------------------------------------------------------
||                        Chapter: DEFINITION OF DATA TYPES
------------------------------------------------------------------------------------------------------------
============================================================================================================
*/

//=============================================================================================
//                  Section: Macros
//=============================================================================================
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
#define AFE_READ16(Reg)                        (*((volatile uint16*)(Reg)))
#define AFE_WRITE16(Reg, value)                 (*((volatile uint16*)(Reg)) = (value))
#define AFE_SET_BIT16(Reg, Bit)                  AFE_WRITE16(Reg, AFE_READ16(Reg) | (Bit))
#define AFE_CLR_BIT16(Reg, Bit)                  AFE_WRITE16(Reg, AFE_READ16(Reg) & (~(Bit)))

#define AFE_READ32(Reg)                        (*((volatile uint32*)(Reg)))
#define AFE_WRITE32(Reg, value)                 (*((volatile uint32*)(Reg)) = (value))
#define AFE_SET_BIT32(Reg, Bit)                  AFE_WRITE32(Reg, AFE_READ32(Reg) | (Bit))
#define AFE_CLR_BIT32(Reg, Bit)                  AFE_WRITE32(Reg, AFE_READ32(Reg) & (~(Bit)))
#else
// TODO: c2k [regAccess]

static uint16 regGet16WithLog(volatile uint16 *regAddr);
static uint32 regGet32WithLog(volatile uint32 *regAddr);

#define AFE_READ16(Reg)                        (regGet16WithLog(Reg))
#define AFE_WRITE16(Reg, Value)                MonTrace(MON_CP_HWD_SPH_REG_WRITE16_TRACE_ID, 2, Reg, Value)
#define AFE_SET_BIT16(Reg, Bit)                MonTrace(MON_CP_HWD_SPH_REG_SETBIT16_TRACE_ID, 2, Reg, Bit)
#define AFE_CLR_BIT16(Reg, Bit)                MonTrace(MON_CP_HWD_SPH_REG_CLRBIT16_TRACE_ID, 2, Reg, Bit)

#define AFE_READ32(Reg)                        (regGet32WithLog(Reg))
#define AFE_WRITE32(Reg, Value)                MonTrace(MON_CP_HWD_SPH_REG_WRITE32_TRACE_ID, 2, Reg, Value)
#define AFE_SET_BIT32(Reg, Bit)                MonTrace(MON_CP_HWD_SPH_REG_SETBIT32_TRACE_ID, 2, Reg, Bit)
#define AFE_CLR_BIT32(Reg, Bit)                MonTrace(MON_CP_HWD_SPH_REG_CLRBIT32_TRACE_ID, 2, Reg, Bit)
#endif

//=============================================================================================
//                  Section: External Variables and Functions
//=============================================================================================
extern AFE_STRUCT_T afe;
// extern const uint16 Ext_op_on_delay;
// extern const uint16 Ext_op_off_delay;
// extern boot_mode_type stack_query_boot_mode(void);


#if defined( NEW_BLOCK_FILTER_SUPPORT )
   extern volatile uint16 *g_Compen_Blk_Flt;
#endif

#if defined(__BT_SUPPORT__)
   extern const unsigned short default_bt_pcm_out_vol;
	extern const unsigned short default_bt_pcm_out_enh_ref_vol;
#endif
   
extern const unsigned char  L1SP_SPEAKER1;
extern const unsigned char  L1SP_SPEAKER2;
extern const unsigned char  L1SP_LOUD_SPEAKER;

// extern const uint16 digital_gain_table[17];

/*
============================================================================================================
------------------------------------------------------------------------------------------------------------
||                        Chapter: Function Prototypes
------------------------------------------------------------------------------------------------------------
============================================================================================================
*/
uint32 SaveAndSetIRQMask(void);
void RestoreIRQMask(uint32);

void AFE_Init( void );
void AFE_Init_status(bool flag);
#if 0 // def ANALOG_AFE_PATH_EXIST	
void SearchSpkFlag( int16 *v_lowest, int16 *a_lowest );
#else
void SearchPathWorkingFlag( int16 *v_lowest, int16 *a_lowest );
#endif

void AFE_SetRefresh( void );
void AFE_DELAY(uint16 delay);

void AFE_Chip_Event_Handler( void *data );
void AFE_Manager( void );


void AFE_Chip_Init( void );

#ifdef ANALOG_AFE_PATH_EXIST	
uint16 _converted_digital_gain(int8 digital_gain_index);
#else
uint16 _digitOnly_convert_digital_gain(int16 digitalGainDb);
void _digitOnly_update_digital_gain(int16 v_lowest, int16 a_lowest);
#endif

void AFE_SwitchExtAmplifier(bool enable);
// void AFE_DisableAmp(void);
void AFE_Initialize(void);
void AFE_RegisterStore(void);
// void AFE_DC_Calibration_WriteBack(void);
void _AfeRegisterBackupByChip(void);
void _AfeRegisterStoreByChip(void);

void AFE_SetRefresh( void );

void AFE_TurnOnLoopback( void );
void AFE_TurnOffLoopback( void );
uint8 AFE_GetOutputDevice( uint8 aud_func );

void AFE_SetBtFlag(bool on);

uint8 AFE_GetVoice8KMode(void);
void AFE_SetVoice8KMode(uint8 mode);
#endif //HWD_AFE_H
