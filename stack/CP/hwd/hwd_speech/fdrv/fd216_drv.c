/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

//#include "us_timer.h"
#include <time.h>


#include "fd216_drv.h"
#include "hwdcommon_def.h"
//#include "kal_public_api.h"
//#include "kal_general_types.h"
//#include "kal_trace.h"
#include "fd216_l1d_reg.h"
#include "fd216_l1d_reg_dsp.h"
#include "monapi.h"
#include "monids.h" 
#include "hwdspherr.h"
#include "hwdpsave.h"

#include "sddl/ddload.h"

/*
      extern kal_uint32 SaveAndsetIRQMask(void);
      extern void RestoreIRQMask(kal_uint32);
      // if( MODEM_TOPSM_IsMD2GPowerOnReady() == true ){ //To-do: ask C2K    // power, clk, reset要ready -->跟毛廣建確認
*/
extern HwdAudioDspPowerStateT HwdGetFd216PowerState(void);
//#define true         (kal_bool)(1==1)
//#define false        (kal_bool)(1==0)

typedef enum
{
   DSP_INIT_STAGE_ONE,
   DSP_INIT_STAGE_TWO,
   DSP_INIT_STAGE_THREE,
   DSP_INIT_STAGE_END
}  DSP_INIT_STAGE;

#define  DSP_ENA_DSP_EXCEPTION            0x0080

/* SHARE2_D2MCON DSP_IO_On */
#define  DSP_D2MCON_IO4_ON            0x0010
#define  DSP_D2MCON_IO5_ON            0x0020
#define  DSP_D2MCON_IO6_ON            0x0040

#define  DSP_WAIT_X_VALUE                 0x5555
#define  DSP_DUMMY_WR_TIMEOUT_US  1000000


#define  DSP_RESET_BEGIN                  0x0006
#define  DSP_RESET_END                    0x0007

/* Bitmap of DSP user */
#define  DSP_USER_MODEM       0x0002
#define  DSP_USER_AUDIO       0x0004

// #define EXT_assert_bypass( st, d1, d2, d3 )    EXT_assert( st, d1, d2, d3 )


   #define  DSP_WRITE(addr, data)      HW_WRITE(addr, (kal_uint16)(data) )
   #define  DSP_READ(addr)             HW_READ(addr)

   #define  HW_WRITE(ptr,data)         (*(ptr) = (data))
   #define  HW_READ(ptr)               (*(ptr))   


#define DSP_PATCH_COUNT   24
#define PATCH_IDX         2


#define  DSP_SLOW_IDLE_DIVIDER1            11  //?? ask james

#define  DSP_DEEP_CLK_DIVIDER            103     /* (240   *0.9)/((103+1)*2) = 1.0385 MHz */
#define  DSP_LOW_POWER_AUDIO_DIVIDER      12
#define  DSP_DEEP_CLK_DIVIDER1            0xC81//(((DSP_LOW_POWER_AUDIO_DIVIDER&0xff)<<8)|(DSP_DEEP_CLK_DIVIDER&0xff))

#define  DSP_RUNNING_VALUE                0x6666

/* Restart DSP API return value (ok message or fail error code) */
#define  DSP_RESTART_OK       0
#define  DSP_RESTART_FAIL     1



#define  L1D_DSP_PWDN()                                                      \
{  HW_WRITE(SHARE_PWDNCON ,0x8000);  }

/*
#define  FD216_L1D_DSP_GetFwVersion()                                               \
{  volatile uint16* pm_addr = DSP_PM_ADDR( 0, 0x0000 );                       \
   dsp.fw_version = DSP_READ( pm_addr );                                      \
}*/

struct dsp_mem_backup
{
      kal_uint16 *dst;
   DPRAMADDR  src;
         int  size;
};

struct dsp_mem_type
{
   struct dsp_mem_backup *mem;
                     int  num;
};

typedef  struct
{  
   volatile unsigned short*  addr;
   volatile unsigned short   data;
} sINITDATA;

/* Structure of DSP init control flow */
typedef struct
{
   uint16  is_dsp_first_init_done;
   uint16  is_dsp_normal_handshake;
   uint16  is_dsp_in_use;
   uint16  stage;
   
   volatile unsigned short* ptr_DP_DSP_STATUS_ ;                
   volatile unsigned short* ptr_DP_MCU_STATUS_ ;                
   volatile unsigned short* ptr_DP_MCU_STATUS_MTCMOS_;          
   volatile unsigned short* ptr_DP_Slow_Idle_Divider ;          
   volatile unsigned short* ptr_DSPCLK_CON  ;                   
   volatile unsigned short* ptr_DUMMY_RW_DPRAM1;
   volatile unsigned short* ptr_DUMMY_RW_DPRAM2;
   volatile unsigned int    ptr_IDMA_CM0;
   volatile unsigned int    ptr_IDMA_base;
   volatile unsigned int*   ptr_PATCH_EN;
   volatile unsigned short* ptr_PATCH_ENA;
   volatile unsigned int ptr_PATCH_PAGE_0;
   volatile unsigned int ptr_PATCH_PAGE_1;
   volatile unsigned int ptr_PATCH_base;
   volatile unsigned short* ptr_SHARE_D2MCON;
   volatile unsigned short* ptr_SHARE_DSP1CKR;  
} sDSP_Init_Ctrl;
static sDSP_Init_Ctrl dsp_init_ctrl = {0};

void FD216_L1D_DSP_WritePatchContent( void )
{
   // the md2G is default off, and delay the DSP patch writing from Application_init() to L1
   void FD216_idma_load( void );
   extern kal_uint32 MODEM_TOPSM_IsMD2GPowerOnReady( void );

   // (PWR_ON_SETTLE+1)T 32KHz
   //temp skip
   //WAIT_TIME_QB(100);
FD216_idma_load();
  /* if( MODEM_TOPSM_IsMD2GPowerOnReady() == true ){ //To-do: ask C2K    // power, clk, reset要ready -->跟毛廣建確認
      FD216_idma_load();
   } else {
      assert(0);
   }*/
}

kal_uint32  debug1;
kal_uint32  debug2;
kal_uint32  debug3;
kal_uint32  debug4;
kal_uint32  debug5;

      

void  FD216_L1D_DSP_WritePatchConfig( kal_uint32 is_patch_0_1 )
{  /* dynamic patch the DSP code */
   void  FD216_idma_get_patch_pointer( const kal_uint16 **, const kal_uint16 **ptr_p_page, const kal_uint16 **ptr_p_add, const kal_uint32 **ptr_p_val );
   const kal_uint16* p_ena;
   const kal_uint16* p_page;
   const kal_uint16* p_add;
   const kal_uint32* p_val;
   kal_uint32  dpatch_addr;
   kal_uint16  d16, patch_ena;
   int    i;
   
   //kal_prompt_trace(MOD_L1SP, "FD216_L1D_DSP_WritePatchConfig(1) %x", is_patch_0_1);
   

   FD216_idma_get_patch_pointer( &p_ena, &p_page, &p_add, &p_val );
   if( is_patch_0_1 ){
      debug1 = dpatch_addr= (kal_uint32)PATCH_base;

      for(i=0; i<PATCH_IDX; i++){ // i=0,1 for Master;  i=2,3 for Slave      
         //kal_prompt_trace(MOD_L1SP, "FD216_L1D_DSP_WritePatchConfig(2) %d", i);
         
         d16 = (kal_uint16) p_page[i];                 /* page */
         DSP_WRITE( (DPRAMADDR)dpatch_addr, d16 );
         //kal_prompt_trace(MOD_L1SP, "FD216_L1D_DSP_WritePatchConfig(3) %x %x", dpatch_addr, d16);
         dpatch_addr += 4;

         d16 = (kal_uint16) p_add[i];                  /* patch_address */
         DSP_WRITE( (DPRAMADDR)dpatch_addr, d16 );
         //kal_prompt_trace(MOD_L1SP, "FD216_L1D_DSP_WritePatchConfig(4) %x %x", dpatch_addr, d16);
         dpatch_addr += 4;

         d16 = (kal_uint16)(p_val[i]&0x00FFFF);        /* instruction low */
         DSP_WRITE( (DPRAMADDR)dpatch_addr, d16 );
         //kal_prompt_trace(MOD_L1SP, "FD216_L1D_DSP_WritePatchConfig(5) %x %x", dpatch_addr, d16);
         dpatch_addr += 4;

         d16 = (kal_uint16)(p_val[i]>>16);             /* instruction high */
         DSP_WRITE( (DPRAMADDR)dpatch_addr, d16 );
         //kal_prompt_trace(MOD_L1SP, "FD216_L1D_DSP_WritePatchConfig(6) %x %x", dpatch_addr, d16);
         dpatch_addr += 4;
      }

      /* MP0_EN        MP1_EN         SP0_EN        SP1_EN   */
      patch_ena = (p_ena[0]<<0) | (p_ena[1]<<1);

      HW_WRITE( PATCH_ENA, patch_ena );
      //kal_prompt_trace(MOD_L1SP, "FD216_L1D_DSP_WritePatchConfig(7) %x %x", PATCH_ENA, patch_ena);
   }else{
      kal_uint16 patchBegin = PATCH_IDX;
      kal_uint16 patchEnd   = DSP_PATCH_COUNT;

      debug2 = dpatch_addr = (kal_uint32)( PATCH_START_ADDR );//wego
      debug3 = patchBegin;
      debug4 = patchEnd;
      for(i=patchBegin; i<patchEnd; i++){
         //kal_prompt_trace(MOD_L1SP, "FD216_L1D_DSP_WritePatchConfig(8) %d", i);
         
         d16  = (kal_uint16)(p_val[i]>>16);
         d16 |= (kal_uint16)(p_ena[i] ? 0xA500 : 0x5A00 );  /* 0xa5(ena) or 0x5a(dis) + instruction high */
         DSP_WRITE( (DPRAMADDR)dpatch_addr, d16 );
         //kal_prompt_trace(MOD_L1SP, "FD216_L1D_DSP_WritePatchConfig(9) %x %x", dpatch_addr, d16);
         
         dpatch_addr += 2;

         d16 = (kal_uint16)(p_val[i]&0x00FFFF);             /* instruction low */
         DSP_WRITE( (DPRAMADDR)dpatch_addr, d16 );
         //kal_prompt_trace(MOD_L1SP, "FD216_L1D_DSP_WritePatchConfig(10) %x %x", dpatch_addr, d16);
         
         dpatch_addr += 2;

         d16 = (kal_uint16) p_add[i];                       /* patch_address */
         DSP_WRITE( (DPRAMADDR)dpatch_addr, d16 );
         //kal_prompt_trace(MOD_L1SP, "FD216_L1D_DSP_WritePatchConfig(11) %x %x", dpatch_addr, d16);
         
         dpatch_addr += 2;

         d16 = (kal_uint16) p_page[i];                      /* page */
         DSP_WRITE( (DPRAMADDR)dpatch_addr, d16 );
         //kal_prompt_trace(MOD_L1SP, "FD216_L1D_DSP_WritePatchConfig(12) %x %x", dpatch_addr, d16);
         
         dpatch_addr += 2;
      }
      //L1D_DSP_SetupMPU( dpatch_addr );                  /* Configure MPU */  ??
   } /* End of "if( is_patch_0_1 )" */
} /* End of "FD216_L1D_DSP_WritePatchConfig" */

unsigned long fd216_rm_tmr_ssta;
unsigned long fd216_dummy_wr_start_1, fd216_dummy_wr_start_2, fd216_dummy_wr_current_1, fd216_dummy_wr_current_2;
unsigned long fd216_dummy_wr_cnt_1 = 0;
unsigned long fd216_dummy_wr_cnt_2 = 0;



static void FD216_L1D_DSP_Dummy_WriteRead_DM( void )//確定FD216是否已經可以使用
{
   int dummy_r1 = 0, dummy_r2 = 0;
#if 0 
//   unsigned long d32;
   double dummy_wr_duration;
   time_t dummy_wr_start_local_1   = 0;
//   time_t dummy_wr_start_local_2   = 0;
   time_t dummy_wr_current_local_1 = 0;
#endif   
//   time_t dummy_wr_current_local_2 = 0;   
   /*unsigned long dummy_wr_duration, d32;
   unsigned long dummy_wr_start_local_1   = 0;
   unsigned long dummy_wr_start_local_2   = 0;
   unsigned long dummy_wr_current_local_1 = 0;
   unsigned long dummy_wr_current_local_2 = 0;*/

   /* set MODEM_CCF_CLK_CON bit[12] to 1 before reading the MODEM_fd216_rm_tmr_ssta status */
   /* MODEM_fd216_rm_tmr_ssta bit[3:0] == 1 => it means the power-on state                 */
   /* MODEM_fd216_rm_tmr_ssta bit[3:0] != 1 => it means the pause or settle state          */

//##### in order to pass the compiling, temporarily skip it
//   d32 = HW_READ( MODEM_CCF_CLK_CON );  //強制確認FD216的clk(毛廣建 ),  gating clk control 要記得做,要把流程投影片跟james確認
//   d32 |= 0x00001000;
//   HW_WRITE( MODEM_CCF_CLK_CON, d32 );

   //kal_prompt_trace(MOD_L1SP, "FD216_L1D_DSP_Dummy_WriteRead_DM(1) %x %x", MODEM_CCF_CLK_CON, d32);
   //fd216_rm_tmr_ssta = HW_READ( MODEM_RM_TMR_SSTA ) & 0x000F;

   /* Dummy write/read two dpram reg to make sure that dsp is not in reset mode. */
#if 0   
   dummy_wr_start_local_1 = time(NULL);
#endif

   while( (dummy_r1 != 0x5555 ) || (dummy_r2 != 0x6666 ) )
   {  DSP_WRITE( DUMMY_RW_DPRAM1, 0x5555 );  //DPRAM2_CPU_base SS可能不知   
                                             //#define DPRAM_CPU_base             (0x82200000)改成 (0x91200000)
                                             //#define DP2_DUMMY_1         (DPRAM_base(0x3f4e))改成 (DPRAM_base(0x50000))
                                             //#define DP2_DUMMY_2         (DPRAM_base(0x3f4e))改成 (DPRAM_base(0x50004))   DM
      DSP_WRITE( DUMMY_RW_DPRAM2, 0x6666 ); //避免bus沒醒, clk, pwr

      dummy_r1 = DSP_READ( DUMMY_RW_DPRAM1 ); 
      dummy_r2 = DSP_READ( DUMMY_RW_DPRAM2 );
      
      fd216_dummy_wr_cnt_1++;
#if 0
      dummy_wr_current_local_1 = time(NULL);//跟SS確認
      dummy_wr_duration  = difftime(dummy_wr_start_local_1, dummy_wr_current_local_1);//(dummy_wr_current_local_1>=dummy_wr_start_local_1) ? (dummy_wr_current_local_1-dummy_wr_start_local_1) : (dummy_wr_current_local_1+USCNT_WRAP-dummy_wr_start_local_1);
      if( dummy_wr_duration > 1 /*1 second*/ )//10us以上就不合理
      {  
         ASSERT(0, HWDSPH_ERR_FORCE_ASSERT, 0);
         //EXT_assert_bypass( 0, dummy_wr_current_local_1, dummy_wr_start_local_1, dummy_wr_duration );
         break;
      }
#endif
   }

#if 0
   fd216_dummy_wr_start_1   = dummy_wr_start_local_1;
   fd216_dummy_wr_current_1 = dummy_wr_current_local_1;

   /* Dummy write/read dpram reg twice to make sure that IDMA is ready */
   dummy_wr_start_local_2 = ust_get_current_time();
   while( (dummy_r1 != 0xAAAA ) )
   {
      DSP_WRITE( DUMMY_RW_DPRAM1, 0xAAAA );
      dummy_r1 = DSP_READ( DUMMY_RW_DPRAM1 );

      fd216_dummy_wr_cnt_2++;

      dummy_wr_current_local_2 = ust_get_current_time();
      dummy_wr_duration  = (dummy_wr_current_local_2>=dummy_wr_start_local_2) ? (dummy_wr_current_local_2-dummy_wr_start_local_2) : (dummy_wr_current_local_2+USCNT_WRAP-dummy_wr_start_local_2);
      if( dummy_wr_duration > DSP_DUMMY_WR_TIMEOUT_US )
      {  EXT_assert_bypass( 0, dummy_wr_current_local_2, dummy_wr_start_local_2, dummy_wr_duration );
         break;
      }
   }

   fd216_dummy_wr_start_2   = dummy_wr_start_local_2;
   fd216_dummy_wr_current_2 = dummy_wr_current_local_2;

   //L1D_TRC_DEBUG( 9160, dummy_wr_start_local_1, dummy_wr_current_local_2           );
   //L1D_TRC_DEBUG( 9160, fd216_rm_tmr_ssta,           (fd216_dummy_wr_cnt_2<<16)|fd216_dummy_wr_cnt_1 );

   fd216_dummy_wr_cnt_1 = 0;
   fd216_dummy_wr_cnt_2 = 0;
#endif
}

void FD216_L1D_FD216_DSP_Init( void )
{
 /*   This API is used for FD216 DSP initialization; these codes are     *
  * new-reg-arch FD216 specific, which is implemented in the file of     *
  * mcu\dsp_ram\dsp_init.c.                                              *
  *                                                                      *
  *   For cache-based FD216 DSP, this API is responsible of the          *
  * following initialization procedures.                                 *
  *   1. Parse DSP ROM image on EMI, and set the hardware registers of   *
  *      DSP cache.                                                      *
  *   2. Execute other DSP side initialization code (can't enable them   *
  *      for MT6256E2 due to possible glitch problem).                   *
  *   3. Release DSP boot mode by writing boot instruction.              *
  *                                                                      *
  *   For non-cache-based (traditional) FD216 DSP, this API is only      *
  * responsible for releasing DSP boot mode by writing boot instruction. */

   extern int FD216_fd216_dsp_init(void);
   FD216_fd216_dsp_init();
}

/*#include "fd216_l1d_reg.h"
#include "fd216_l1d_reg_dsp.h"
#define PATCH_PAGE(n)          (PATCH_base+((n)*0x10))
#define PATCH_ADDR(n)          (PATCH_PAGE(n)+0x04)
#define PATCH_INST_LOW(n)      (PATCH_PAGE(n)+0x08)
#define PATCH_INST_HIGH(n)     (PATCH_PAGE(n)+0x0c)
#define PATCH_EN               ((volatile kal_uint32 *)(PATCH_base+0x0100))*/

void  FD216_L1D_DSP_WakeUp( void )                                                                      
{                                                                      
   kal_uint16  d16;                                                                      
// merely store important address
dsp_init_ctrl.ptr_DP_DSP_STATUS_ = DP_DSP_STATUS_;      
dsp_init_ctrl.ptr_DP_MCU_STATUS_ = DP_MCU_STATUS_;      
dsp_init_ctrl.ptr_DP_MCU_STATUS_MTCMOS_ = DP_MCU_STATUS_MTCMOS_;
dsp_init_ctrl.ptr_DP_Slow_Idle_Divider = DP_Slow_Idle_Divider;
dsp_init_ctrl.ptr_DSPCLK_CON  = DSPCLK_CON;         
dsp_init_ctrl.ptr_DUMMY_RW_DPRAM1 = DUMMY_RW_DPRAM1;      
dsp_init_ctrl.ptr_DUMMY_RW_DPRAM2 = DUMMY_RW_DPRAM2;      
dsp_init_ctrl.ptr_IDMA_CM0 = IDMA_CM0;             
dsp_init_ctrl.ptr_IDMA_base = IDMA_base;            
dsp_init_ctrl.ptr_PATCH_EN = PATCH_EN;              
dsp_init_ctrl.ptr_PATCH_ENA = PATCH_ENA;            
dsp_init_ctrl.ptr_PATCH_PAGE_0 = PATCH_PAGE(0);         
dsp_init_ctrl.ptr_PATCH_PAGE_1 = PATCH_PAGE(1);         
dsp_init_ctrl.ptr_PATCH_base = PATCH_base;           
dsp_init_ctrl.ptr_SHARE_D2MCON = SHARE_D2MCON;         
dsp_init_ctrl.ptr_SHARE_DSP1CKR = SHARE_DSP1CKR;        


//
   d16 = HW_READ( DSPCLK_CON );      /* Set DSPCLK 104MHz */ //設定給AFE clock 用DSP的clock除過來的,一定要剛剛好match到,不然AFE會全亂掉                  
   d16 |= 0x7000;                                                                      
   HW_WRITE( DSPCLK_CON, d16 );                                                                            
   //MonTrace(MON_CP_HWD_GGG_MAILBOX_TRACE_ID, 3, 0x6666, ptr, data)                                                                      
   //MonTrace(MON_CP_HWD_GGG_MAILBOX_TRACE_ID, 4, 0x3333, 0x100, DSPCLK_CON, d16);                                                                         
 
/*MonTrace(MON_CP_HWD_GGG_MAILBOX_TRACE_ID, 4, 0x3333, 0x300, DP_DSP_STATUS_, 0);                 
MonTrace(MON_CP_HWD_GGG_MAILBOX_TRACE_ID, 4, 0x3333, 0x300, DP_MCU_STATUS_, 0);                 
MonTrace(MON_CP_HWD_GGG_MAILBOX_TRACE_ID, 4, 0x3333, 0x300, DP_MCU_STATUS_MTCMOS_, 0);          
MonTrace(MON_CP_HWD_GGG_MAILBOX_TRACE_ID, 4, 0x3333, 0x300, DP_Slow_Idle_Divider, 0);           
MonTrace(MON_CP_HWD_GGG_MAILBOX_TRACE_ID, 4, 0x3333, 0x300, DSPCLK_CON, 0);                     
MonTrace(MON_CP_HWD_GGG_MAILBOX_TRACE_ID, 4, 0x3333, 0x300, DUMMY_RW_DPRAM1, 0);
MonTrace(MON_CP_HWD_GGG_MAILBOX_TRACE_ID, 4, 0x3333, 0x300, DUMMY_RW_DPRAM2, 0);
MonTrace(MON_CP_HWD_GGG_MAILBOX_TRACE_ID, 4, 0x3333, 0x300, IDMA_CM0, 0);
MonTrace(MON_CP_HWD_GGG_MAILBOX_TRACE_ID, 4, 0x3333, 0x300, IDMA_base, 0);
MonTrace(MON_CP_HWD_GGG_MAILBOX_TRACE_ID, 4, 0x3333, 0x300, PATCH_EN, 0);
MonTrace(MON_CP_HWD_GGG_MAILBOX_TRACE_ID, 4, 0x3333, 0x300, PATCH_ENA, 0);
MonTrace(MON_CP_HWD_GGG_MAILBOX_TRACE_ID, 4, 0x3333, 0x300, PATCH_PAGE(0), 0);
MonTrace(MON_CP_HWD_GGG_MAILBOX_TRACE_ID, 4, 0x3333, 0x300, PATCH_PAGE(1), 0);
MonTrace(MON_CP_HWD_GGG_MAILBOX_TRACE_ID, 4, 0x3333, 0x300, PATCH_base, 0);
MonTrace(MON_CP_HWD_GGG_MAILBOX_TRACE_ID, 4, 0x3333, 0x300, SHARE_D2MCON, 0);
MonTrace(MON_CP_HWD_GGG_MAILBOX_TRACE_ID, 4, 0x3333, 0x300, SHARE_DSP1CKR, 0);*/


   FD216_L1D_DSP_WritePatchContent();
   
   FD216_L1D_DSP_WritePatchConfig( true );
   
   DSP_WRITE( SHARE_DSPCON, DSP_RESET_BEGIN ); //這個 SHARE_DSPCON 與 idma中 SHARE_DSPCTL base_address是相同的
// //kal_prompt_trace(MOD_L1SP, "FD216_L1D_DSP_WakeUp(1) %x %x", SHARE_DSPCON, DSP_RESET_BEGIN);
   
   DSP_WRITE( SHARE_DSPCON, DSP_RESET_END );
// //kal_prompt_trace(MOD_L1SP, "FD216_L1D_DSP_WakeUp(2) %x %x", SHARE_DSPCON, DSP_RESET_END);
   //add loop 0 assert
   FD216_L1D_DSP_Dummy_WriteRead_DM();

    // wego
   DSP_WRITE( DP_DSP_STATUS_,        0x0000);   ///#define DP_DSP_TASK_STATUS_BASE                   0x3a00 /* 0x3a00 */ 要改成 0x3800
   
   //kal_prompt_trace(MOD_L1SP, "FD216_L1D_DSP_WakeUp(3) %x %x", DP_DSP_STATUS_, 0x0000);
   //L1D_DSP_FIX_PIO_GLITCH_ENABLE();

    // wego
   DSP_WRITE( DP_MCU_STATUS_MTCMOS_, 0x0000);
   //kal_prompt_trace(MOD_L1SP, "FD216_L1D_DSP_WakeUp(4) %x %x", DP_MCU_STATUS_MTCMOS_, 0x0000);
   //Enable DSP memory clock gating
   //DSP_WRITE( SHARE_DSP2OPT, 0x2 );  //hardware保命用的,要問james要怎設定
   //C2K 用不到, 目前已經有預設值了,bit1 hardware always 1,所以有下沒下都一樣  未來的chip可能用得到, 開關特定hardware module
   //kal_prompt_trace(MOD_L1SP, "FD216_L1D_DSP_WakeUp(5) %x %x", SHARE_DSP2OPT, 0x2);
   //API provided by SP3 to initialize DSP cache and enable EM security protection
   FD216_L1D_FD216_DSP_Init();      
}


static const sINITDATA  FD216_DSP_REG_INIT_DATA[] =
{
   {  DP_Slow_Idle_Divider        , DSP_SLOW_IDLE_DIVIDER1 },
   {  DP_MCU_STATUS_               , DSP_RUNNING_VALUE },
   //{  DEBUG_assert_CTRL           , 0x1         },    //debug 用的 speech 不會用到
   {  0                           , 0                  },

};

const sINITDATA* FD216_L1D_DSP_GetRegInitTableAddr( void )
{  return FD216_DSP_REG_INIT_DATA;
}

static const sINITDATA  FD216_DSP_HW_INIT_DATA[] =
{
   {  SHARE_D2MCON                , 0x0000      },
   {  SHARE_DSP1CKR               , DSP_DEEP_CLK_DIVIDER1 }, // Host  DSP clock in deep sleep mode
   {  0                           , 0                  },   
};

const sINITDATA* FD216_L1D_DSP_GetHwInitTableAddr( void )
{  return FD216_DSP_HW_INIT_DATA;
}

typedef  struct
{
   kal_uint16         host_dsp_shareg[SHARE1_SIZE];    /* According to the SHERIFF REG size */
    
} sDSP_Backup;

static sDSP_Backup fd216_dsp_backup;

static struct dsp_mem_backup FD216_shareg_info[]=
{  
   { fd216_dsp_backup.host_dsp_shareg +0,                   SHARE1_BACKUP1_START, SHARE1_BACKUP1_SIZE },
   { fd216_dsp_backup.host_dsp_shareg +SHARE1_BACKUP1_SIZE, SHARE1_BACKUP2_START, SHARE1_BACKUP2_SIZE },
   //C2K 之後要改成 每使用FD216時候都要重新下, 0x0C81 : FD216 slow idle 要跑多快,設太小 就耗電, 設太大, 
   //serve IRQ 就慢一點,不會掉
};
/*
static struct dsp_mem_backup dpram_info[]=
{  {  fd216_dsp_backup.host_dsp_sheriff,
      DP_D2C_TASK0,
      DPRAM1_SIZE,
   },
   {  fd216_dsp_backup.slave_dsp_sheriff,
      DP2_D2C_TASK0,
      DPRAM2_SIZE,
   },
   {  fd216_dsp_backup.host_dsp_sheriff+DPRAM1_RESTORE2_OFFSET1,
      DPRAM1_RESTORE2_START1,
      DPRAM1_RESTORE2_SIZE1,
   },
   {  fd216_dsp_backup.host_dsp_sheriff+DPRAM1_RESTORE2_OFFSET2,
      DPRAM1_RESTORE2_START2,
      DPRAM1_RESTORE2_SIZE2,
   },
};*/

static struct dsp_mem_type FD216_mem_info[]=
{  
   { FD216_shareg_info, sizeof(FD216_shareg_info)/sizeof(struct dsp_mem_backup) },      
};

void  FD216_L1D_DSP_BackupDSP( void )
{
   kal_uint16 *addr1, *addr3;
   DPRAMADDR addr2;
   struct dsp_mem_type   *m;
   struct dsp_mem_backup *b, *b1;


   /* Backup DPRAM + Shareg */  //shareg : D2M enable
   for(m = FD216_mem_info; m<(FD216_mem_info+sizeof(FD216_mem_info)/sizeof(struct dsp_mem_type)); m++){  
      for(b=m->mem, b1=b+m->num; b<b1; b++){  
         addr1 = b->dst;
         addr2 = b->src;
         for(addr3=addr1+b->size; addr1<addr3; addr2++, addr1++){

            *addr1 = DSP_READ( addr2 );

            //kal_prompt_trace(MOD_L1SP, "FD216_L1D_DSP_BackupDSP(1) %x %x", addr1, addr2);
         }
      }
   }
}


void  FD216_L1D_DSP_Init( void )
{
   //kal_prompt_trace(MOD_L1SP, "FD216_L1D_DSP_Init(1) %x", DP_DSP_STATUS_);

   while( HW_READ(DP_DSP_STATUS_)!=0x5555 );

   HW_WRITE( DP_DSP_STATUS_, 0x0000 );
   /* Get MPU setting value before first MPU setting in FD216_L1D_DSP_WritePatchConfig( false ) */
   //   L1D_DSP_GetMpuSetting();
            
   FD216_L1D_DSP_WritePatchConfig( false );
   HW_WRITE( DP_MCU_STATUS_, 0x6666 );   
   //kal_prompt_trace(MOD_L1SP, "FD216_L1D_DSP_Init(2) %x", DP_MCU_STATUS_);

   while( HW_READ(DP_DSP_STATUS_)!=DSP_WAIT_X_VALUE ); 

/* Just Initialize SHERIF by default table : thomas chen */  //目前是powner on memory,只是電的來源跟patch ram , 所以patch ram也要量
   //james request time :
   //Wego request, register/memory 分類                  
   {  
      const sINITDATA *p;  
      for( p=FD216_L1D_DSP_GetRegInitTableAddr(); p->addr!=0; p++ ) { //sheriff只是DM中的一小塊, 要跟wego確認
         //kal_prompt_trace(MOD_L1SP, "FD216_L1D_DSP_Init(3) %x %x", p->addr, p->data);
         DSP_WRITE( p->addr, p->data );  
      }
      for( p=FD216_L1D_DSP_GetHwInitTableAddr(); p->addr!=0; p++ ) { //sheriff只是DM中的一小塊, 要跟wego確認
         //kal_prompt_trace(MOD_L1SP, "FD216_L1D_DSP_Init(3) %x %x", p->addr, p->data);
         HW_WRITE( p->addr, p->data );  
      }      
   }   
   //james request time
  
   /* Initialize patch content pointer */
   //L1D_DSP_GetPatchContentPointer();   自己去確認 ??
   //if( L1D_IN_META_MODE() ) //跟wego確認 & James
   //{  L1D_DSP_DisableDspSlowIdle();  }   

   {  
      kal_uint16  d16;
      /* enable DSP exception interrupt */  //檢查一下 還有哪些D2M interrupt 被enable  (23 modem專用) 4567 

      d16  = HW_READ( SHARE_D2MCON );
      d16 |= DSP_ENA_DSP_EXCEPTION;

	  /* DSP-to-MCU Interrupt Control Register On*/
	  d16 |= DSP_D2MCON_IO4_ON;
  	  d16 |= DSP_D2MCON_IO5_ON;
  	  d16 |= DSP_D2MCON_IO6_ON;	  
      HW_WRITE( SHARE_D2MCON, d16 );
      //kal_prompt_trace(MOD_L1SP, "FD216_L1D_DSP_Init(4) %x %x", SHARE_D2MCON, d16);
   }
   //FD216 沒有ICE接口
   
   //FD216_L1D_DSP_GetFwVersion();  //wego 
   
   /* After first time MCU/DSP hand shaking, backup (DPRAM + SHARE_Reg). */
   
   FD216_L1D_DSP_BackupDSP();   
   dsp_init_ctrl.stage = DSP_INIT_STAGE_END; /* DSP init procedure has finished. */   
   dsp_init_ctrl.is_dsp_in_use           = 0;//DSP_USER_MODEM;  
   dsp_init_ctrl.is_dsp_normal_handshake = 0;                  /* Before DSP first time power down, MCU/DSP use 1st-type(0x6666/0x5555) handshake. */   
   dsp_init_ctrl.is_dsp_first_init_done  = 1;                  /* DSP first time hand shaking was done. */   
}




void FD216_L1D_DSP_NoNeedDSP( kal_uint16 dsp_user) //audio & modem
{   

   //uint32 _savedMask;
   
   /*-------------------- mask IRQ --------------------*/
   //_savedMask = SaveAndSetIRQMask();
   SysIntDisable(SYS_IRQ_INT);
   if( dsp_user == DSP_USER_MODEM )
   {
      /* 2G modem does not need to backup again since it has been backup (DPRAM+SHARE_Reg) in L1D_Sleep_Prepare() */
   
      /* Claim that MODEM no more need dsp. */
      dsp_init_ctrl.is_dsp_in_use &= (~DSP_USER_MODEM);
   }
   else
   {
      /* Backup (DPRAM+SHARE_Reg) after dsp_user finish use DSP. */
      //FD216_L1D_DSP_BackupDSP(); 
      /* Claim that this dsp_user no more need dsp. */
      dsp_init_ctrl.is_dsp_in_use &= (~dsp_user);
   }
   
   /* If all dsp_user claim that they do not need DSP, reset DSP init stage parameter. */
#if 0   
   if( !(dsp_init_ctrl.is_dsp_in_use) )
   {
      /* DSP init stage parameter must the first or last one. */
      /* To avoid to reset DSP init stage parameter during L1D_DSP_RestartDSP API is still processing. */
      ASSERT( (dsp_init_ctrl.stage == DSP_INIT_STAGE_END) || (dsp_init_ctrl.stage == DSP_INIT_STAGE_ONE), HWDSPH_ERR_FORCE_ASSERT, 0);
      /* Reset DSP init stage parameter. */
      dsp_init_ctrl.stage = DSP_INIT_STAGE_ONE;
      /* After DSP first time power down, MCU/DSP use normal (0x8888/0x7777) handshake. */
      dsp_init_ctrl.is_dsp_normal_handshake = 1;
   }
#endif   
   /*------------------- un-mask IRQ -------------------*/
   //RestoreIRQMask(_savedMask);  
   SysIntEnable(SYS_IRQ_INT); 
}

void  FD216_L1D_DSP_RestoreDSP( kal_uint32 is_dpram, kal_uint32 is_phase2 )
{
   kal_uint16 *addr1, *addr3;
   DPRAMADDR addr2;
   struct dsp_mem_type   *m;
   struct dsp_mem_backup *b, *b1;

   m = FD216_mem_info + (is_dpram ? 1 : 0 );
   b = m->mem; b1= b+m->num;

   /* Restore DPRAM / Shareg */
   for( ; b<b1; b++)
   {  addr1 = b->dst;
      addr2 = b->src;
      for(addr3=addr1+b->size; addr1<addr3; addr2++, addr1++){
         DSP_WRITE( addr2, *addr1 );
         //kal_prompt_trace(MOD_L1SP, "FD216_L1D_DSP_RestoreDSP(1) %x %x", addr2, addr1);
      }
   }
}




void FD216_L1D_DSP_EnableDspSlowIdle( void )
{
   DSP_WRITE( DP_Slow_Idle_Divider,  DSP_SLOW_IDLE_DIVIDER1 );
}

void FD216_L1D_DSP_DisableDspSlowIdle( void )
{
   DSP_WRITE( DP_Slow_Idle_Divider,  0xFFFF );
}

unsigned short L1D_Audio_ChkDspInitDone()
{
   return dsp_init_ctrl.is_dsp_first_init_done;
}




unsigned short  FD216_L1D_DSP_RestartDSP( kal_uint16 dsp_user )// audio & modem
{

      //uint32 _savedMask;

      if( !(dsp_init_ctrl.is_dsp_first_init_done) )
      {
         return DSP_RESTART_FAIL; /* return error code */
      }      
      //_savedMask = SaveAndsetIRQMask();
      SysIntDisable(SYS_IRQ_INT);
      
      dsp_init_ctrl.is_dsp_in_use |= dsp_user;


      if( HWD_Audio_Fd216_NOT_INITIALIZED ==  HwdGetFd216PowerState()){
         dsp_init_ctrl.stage = DSP_INIT_STAGE_ONE;//Workaround @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@YS Hsieh
         dsp_init_ctrl.is_dsp_normal_handshake = 1;
      }
	  
      if( dsp_init_ctrl.stage == DSP_INIT_STAGE_ONE )
      {
         FD216_L1D_DSP_RestoreDSP( false, false );
   
         /* Reset DSP and unlock reset DSP */
         DSP_WRITE( SHARE_DSPCON,  DSP_RESET_BEGIN );
         DSP_WRITE( SHARE_DSPCON,  DSP_RESET_END   ); 
         dsp_init_ctrl.stage = DSP_INIT_STAGE_TWO;
      }
      //RestoreIRQMask(_savedMask);
      SysIntEnable(SYS_IRQ_INT);
      
      FD216_L1D_DSP_Dummy_WriteRead_DM();  

      //_savedMask = SaveAndsetIRQMask();
      SysIntDisable(SYS_IRQ_INT);
      
      if( dsp_init_ctrl.stage == DSP_INIT_STAGE_TWO )
      {
         //DSP_WRITE( SHARE_DSP2OPT, 0x2 );//問JAME是否可以拿掉
   
         /* Restore Patch Config in patch config */
         FD216_L1D_DSP_WritePatchConfig( true );
         dsp_init_ctrl.stage = DSP_INIT_STAGE_THREE;
      }
      //RestoreIRQMask(_savedMask);
      SysIntEnable(SYS_IRQ_INT);
      
//不斷電 CM schedule, DM sherif
      //_savedMask = SaveAndsetIRQMask();
      SysIntDisable(SYS_IRQ_INT);
      if( dsp_init_ctrl.stage == DSP_INIT_STAGE_THREE )
      {      
         FD216_L1D_DSP_WritePatchConfig( false );
         
         DSP_WRITE( DP_DSP_STATUS_MTCMOS_,  0x0000 );
         //kal_prompt_trace(MOD_L1SP, "FD216_L1D_DSP_RestartDSP(1) %x %x", DP_DSP_STATUS_MTCMOS_, 0x0000);
         /* DSP first boot indicator: not first boot */
         DSP_WRITE( DP_MCU_STATUS_MTCMOS_,  0x7777 );
   
         /* API provided by SP3 to initialize DSP cache and enable EM security protection */
         FD216_L1D_FD216_DSP_Init();
      }
      //RestoreIRQMask(_savedMask); 
      SysIntEnable(SYS_IRQ_INT);
      
      if( dsp_init_ctrl.is_dsp_normal_handshake )
      {
         /* Wait for (0x7777/0x8888) MCU/DSP hand shaking finish after DSP first time power down. */

         while( 0x8888 != DSP_READ( DP_DSP_STATUS_MTCMOS_ ) );

      }
      else
      {  //ASSERT(0, HWDSPH_ERR_FORCE_ASSERT, 0);
         /* Wait for (0x6666/0x5555) MCU/DSP hand shaking finish before DSP first time power down. */
         while( 0x5555 != DSP_READ( DP_DSP_STATUS_ ) );
      }

   //assert_bypass( dsp_init_ctrl.stage == DSP_INIT_STAGE_END );
   return 0;
}

void L1D_Audio_NoNeedDSP()
{
   FD216_L1D_DSP_NoNeedDSP( DSP_USER_AUDIO );
}
/* ==========================================================================*/

unsigned short L1D_Audio_RestartDSP()
{
   return FD216_L1D_DSP_RestartDSP( DSP_USER_AUDIO );
}

void L1D_DSP_EnableDspSlowIdle( void )
{  
   /* To prevent DSP from entering slow idle in Meta mode */
// if( !L1D_IN_META_MODE() )
   {
      DSP_WRITE( DP_Slow_Idle_Divider,  DSP_SLOW_IDLE_DIVIDER1 );
   }

}

/* To prevent DSP from entering slow idle in Meta mode */
void L1D_DSP_DisableDspSlowIdle( void )
{
   DSP_WRITE( DP_Slow_Idle_Divider,  0xFFFF );
}

static  uint8  dsp_not_pdn = 0;
void FD216_L1D_PauseDSP( void )
{
   if( !dsp_not_pdn ) /* if DSP report crash by sending D2M interrupt to MCU */
      L1D_DSP_PWDN(); /* do not power down DSP, otherwise DSP debug info is lost*/
//#if IS_CACHE_DSP_SUPPORT || IS_DSP_ARCHITECTURE_V4_SUPPORT
//   DSP_WRITE( DP_MCU_STATUS, 0 );           /* freeze DSP */
//#else
//   DSP_WRITE( DP_D2C_TASK1, 0 );   James         /* freeze DSP */
//#endif
   //L1D_WIN_DisableAllEvents( DIS_ALL_IRQ ); /* disable all TDMA events */
   //L1D_DumpDSPRegister();
}


static const uint32 L1D_DspMemoryInfo[] =
{/* address         , length(*16 bit) byte */
   IDMA_DM0 +0x50000, 16*1024*2,  /*DSP1 DM Page5*/
   IDMA_DM0 +0x60000, 16*1024*2,  /*DSP2 DM Page6*/
   IDMA_DM0 +0x70000, 16*1024*2,  /*DSP2 DM Page7*/
   IDMA_DM0 +0x80000, 16*1024*2,  /*DSP2 DM Page8*/
   IDMA_DM0 +0x90000, 16*1024*2,  /*DSP2 DM Page9*/
   IDMA_PM0 +0x00000, 16*1024*2,  /*DSP2 PM Page0*/
   IDMA_PM0 +0x30000, 16*1024*2,  /*DSP2 PM Page3*/
   IDMA_PM0 +0x80000, 16*1024*2,  /*DSP2 PM Page8*/
   IDMA_PM0 +0x90000, 16*1024*2,  /*DSP2 PM Page9*/
};

void L1D_GetDspMemoryInfo( uint32 **info, uint16 *count )
{  *info  = (uint32*)L1D_DspMemoryInfo;
   *count = sizeof(L1D_DspMemoryInfo)/sizeof(uint32)/2;
}

#define  DSP_DSP_EXCEPTION_ID             0x003E
#define  DSP_MPU_VIOLATION                0x003F

void L1D_DSP2MCU_Interrupt( void )
{
   volatile uint16  data1, data2;
  /* At this point, although IRQMask has not done.
     The next D2M interrupt will not be triggered.
     Because D2M is level trigger, and the status has not been read-clear. */
   static kal_uint32 uReentry = 0;

   if( uReentry )
   {  ASSERT(0, HWDSPH_ERR_FORCE_ASSERT, 0);  }
   uReentry = 1;

   /* mask D2C interrupt and Ack D2M interrupt */
   //IRQMask(IRQ_DSP2CPU_CODE); /// 問C2K SS 要換哪個function
   SysIntDisable (SYS_IRQ_INT);
   
   //#ifdef L1D_TEST
   //l1dtest.L1D_DSP2MCU_HEAD();
   //l1dtest.L1D_DSP2MCU_HEAD2();
   //#endif

   data1 = DSP_READ( SHARE_D2MSTA );

   /** TASK2 ***************************/
   //data2 = DSP_READ( DP_D2C_TASK2 );
   //if( data1&DSP_ENA_DTX_INTERRUPT && data2==DSP_SILENCE_FRAME_ID )
   //{
   //   L1D_StopUplinkTCh();
   //   L1D_Trc_D2CStopUL();
   //}

   /** TASK3 ***************************/
   //data2 = DSP_READ( DP_D2C_TASK3 );
   //if( data1&DSP_ENA_FB_INTERRUPT && data2==DSP_FB_DETECTED_ID )
   //{
   //   L1D_FCChMachine_ForceTuneAFC();
   //   L1D_Trc_D2CFBFound();
   //}
   //else if( data1&DSP_ENA_TX_IQ_SWAP_INTERRUPT && data2==DSP_FB_TX_IQ_SWAP_ID )
   //{
   //   if(l1d_rf.d2c_txiqswap)     L1D_BFE_SetTxIQSwap();
   //   if(l1d_rf.d2c_txiqconfig)   L1D_BFE_SetTxIQ_Config();
   //}

   #ifndef L1D_TEST
   /** TASK4 ***************************/
   data2 = DSP_READ( DP_D2C_TASK4 );
   if( data1&0x10 && data2!=0x8888 )
   {  if( data2!=0x0000 )
      {
         void L1SP_D2C_LISR( uint16 itype );
         L1SP_D2C_LISR( data2 );
      }
   }

   /** TASK5 ***************************/
   data2 = DSP_READ( DP_D2C_TASK5 );
   if( data1&0x20 && data2!=0x0000 )
   {
      void L1SP_D2C_LISR( uint16 itype );
      L1SP_D2C_LISR( data2 );
   }
   #endif

   /** TASK6 ***************************/
   data2 = DSP_READ( DP_D2C_TASK6 );
   if( data1&0x40 && data2!=0x0000 )
   {
      void L1SP_D2C_LISR( uint16 itype );
      L1SP_D2C_LISR( data2 );
   }

   /** TASK7 ***************************/
   // IO(0x7) is used for DSP crash interrupt
   data2 = DSP_READ( DP_D2C_TASK7 );
   if( data1&0x80 )
   {
      if( data2==DSP_DSP_EXCEPTION_ID )  /* DSP 0xDD44 crash*/
      {
         //DisableIRQ();???/// 問C2K SS 要換哪個function
         //SysIntDisable (SYS_ALL_INT);
         DSP_WRITE( DP_MCU_STATUS_, 0 );           /* freeze DSP */
         //L1D_WIN_DisableAllEvents( DIS_ALL_IRQ ); /* disable all TDMA events */
         //dsp_not_pdn = 1;

      #ifndef L1D_TEST
         ASSERT(0, HWDSPH_ERR_FORCE_ASSERT, 0);;/// 問C2K SS 要換哪個function
      #else
         //L1DTest_assertFail(); /* In order to stop cosim case automatically */
      #endif
      }
      else if( data2==DSP_MPU_VIOLATION )//問wego
      {
         ASSERT(0, HWDSPH_ERR_FORCE_ASSERT, 0);//FREEZE_DSP_assert_bypass( 0, DP_MCU_STATUS_ );
      }
   }

   uReentry=0;

   /* unmask D2C interrupt */
   //IRQUnmask(IRQ_DSP2CPU_CODE);/// 問C2K SS 要換哪個function
   SysIntEnable (SYS_IRQ_INT);
   /* At this point, the next D2M interrupt is able to trigger MCU. */
}

void FD216_DRV_DSP_Init(void)
{
   FD216_L1D_DSP_WakeUp();	
   FD216_L1D_DSP_Init();
   DSP_DynamicDownload(DDID_SPH_C2K_SCH);
}


