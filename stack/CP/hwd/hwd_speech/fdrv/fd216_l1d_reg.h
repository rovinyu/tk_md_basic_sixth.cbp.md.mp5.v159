/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

#ifndef _L1D_REG_H_
#define _L1D_REG_H_
#include "reg_base.h"
//@ Clock Gate Register�n�O�o�[

//#include "driver/sleep_drv/internal/inc/MODEM_TOPSM_private.h"
#define IDMA_CM0                 (IDMA_base +0x000000)
#define IDMA_PM0                 (IDMA_base +0x100000)
#define IDMA_DM0                 (IDMA_base +0x200000)

#define SHARE_DSPCTL             SHARE_DSPCON
//#define SHARE_D2MCTL             SHARE_D2MCON

#define DSP_CM_ADDR(page,addr)   ((APBADDR32)(IDMA_CM0+((page)<<16)+((addr)<<2)))
#define DSP_PM_ADDR(page,addr)   ((DPRAMADDR)(IDMA_PM0+((page)<<16)+((addr)<<1)))
#define DSP_DM_ADDR(page,addr)   ((DPRAMADDR)(IDMA_DM0+((page)<<16)+((addr)<<1)))

//typedef volatile unsigned short* SRAMADDR;                              /* SRAM addr is 16 bits                          */
//typedef volatile unsigned short  SRAMDATA;                              /* SRAM data is 16 bits                          */
typedef volatile unsigned short* APBADDR;                               /* APB addr is 32 bits                           */
//typedef volatile unsigned short  APBDATA;                               /* APB data is 16 bits                           */
typedef volatile unsigned long*  APBADDR32;                             /* APB addr is 32 bits                           */
//typedef volatile unsigned long   APBDATA32;                             /* APB data is 16 bits                           */
typedef volatile unsigned short* DPRAMADDR;                             /* DPRAM addr is 16 bits                         */
//typedef volatile signed   short* DPRAMADDR_S;                           /* DPRAM addr is 16 bits                         */
typedef volatile unsigned short  DPRAMDATA;                             /* DPRAM data is 16 bits                         */


#define PATCH_ENA                ((APBADDR)(PATCH_base+0x100))          /* patch enable register                         */
#define PATCH_P(n)               ((APBADDR)(PATCH_base+0x000+(n)*4))    /* the page number of patched instruction n      */
#define PATCH_A(n)               ((APBADDR)(PATCH_base+0x004+(n)*4))    /*     the address of patched instruction n      */
#define PATCH_IL(n)              ((APBADDR)(PATCH_base+0x008+(n)*4))    /*    low  16 bits of patched instruction n      */
#define PATCH_IH(n)              ((APBADDR)(PATCH_base+0x00C+(n)*4))    /*    high  8 bits of patched instruction n      */

#define PATCH_PAGE(n)          (PATCH_base+((n)*0x10))
#define PATCH_ADDR(n)          (PATCH_PAGE(n)+0x04)
#define PATCH_INST_LOW(n)      (PATCH_PAGE(n)+0x08)
#define PATCH_INST_HIGH(n)     (PATCH_PAGE(n)+0x0c)
#define PATCH_EN               ((volatile kal_uint32 *)(PATCH_base+0x0100))


#define MODEM2G_TOPSM_base       SLP_2G_CTRL_base

//driver/sleep_drv/internal/inc/modem_topsm_private.h
#define MODEM2G_TOP_SleepMode_base               SLP_2G_CTRL_base
#define MODEM2G_TOPSM_CCF_CLK_CON                ((volatile kal_uint32*)(MODEM2G_TOP_SleepMode_base+0x200))
#define MODEM2G_TOPSM_RM_TMR_SSTA                ((volatile kal_uint32*)(MODEM2G_TOP_SleepMode_base+0x40))

#define MODEM_CCF_CLK_CON   MODEM2G_TOPSM_CCF_CLK_CON
#define MODEM_RM_TMR_SSTA   MODEM2G_TOPSM_RM_TMR_SSTA
//#define MODEM_CCF_CLK_CON        ((APBADDR32)(MODEM2G_TOPSM_base+0x0200))
//#define MODEM_RM_TMR_SSTA        ((APBADDR32)(MODEM2G_TOPSM_base+0x0040))       /* Resource Manager Timer Sleep Control State    */

#define DSPCLK_CON               ((APBADDR)(MD2GCONFG_base+0x040))      /* DSP Clock Control Register                    */
#endif
