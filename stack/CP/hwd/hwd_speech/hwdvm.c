/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2005
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*******************************************************************************
 *
 * Filename:
 * ---------
 * vm.c
 *
 * Project:
 * --------
 *   MAUI
 *
 * Description:
 * ------------
 *   VM recording/playback
 *
 * Author:
 * -------
 * Phil Hsieh
 *
 *==============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 * $Revision:   1.27  $
 * $Modtime:   Jul 08 2005 11:01:20  $
 * $Log:   //mtkvs01/vmdata/Maui_sw/archives/mcu/l1audio/vm.c-arc  $
 *
 * 06 17 2015 ys.hsieh
 * [SIXTH00003262] [Jade][Pre-BringUp] [C2K] [CCCI IT][MD3 Regression]4G??????speechlogmtc?????????EE(??crash)resetmodemNEEE(??crash)(1/5)
 *  VM data alignment
 *
 * 06 23 2014 fu-shing.ju
 * [MOLY00070156] Merge VoLTE to LR9.W1423.MD.LWTG.MP and MOLY trunk
 * Merge VoLTE to LR9.W1423.
 *
 * 04 09 2014 thomas.chen
 * [MOLY00062128] fix K2 build error
 * .
 *
 * 01 13 2014 thomas.chen
 * [MOLY00053440] [MT6582LTE][IMS][VOLTE] VoLTE Data Plan Feature In
 * .
 *
 * 01 13 2014 thomas.chen
 * [MOLY00053440] [MT6582LTE][IMS][VOLTE] VoLTE Data Plan Feature In
 * .
 *
 * 10 17 2013 thomas.chen
 * [MOLY00036751] [MT6290E1][MMDS_DC][Pre-IT][Bring Up][G+T]2G CS call hang on the calling screen
 * .
 *
 * 09 05 2013 thomas.chen
 * [MOLY00036751] [MT6290E1][MMDS_DC][Pre-IT][Bring Up][G+T]2G CS call hang on the calling screen
 * .
 *
 * 09 02 2013 sheila.chen
 * [MOLY00034583] [MT6290E1][W/G][Overnight][China][SZ]Fatal Error (0x1, 0x1fa6bf) - L1Audio
 * vm stop bug fix. HISR may come when stopping.
 *
 * 06 05 2013 thomas.chen
 * [MOLY00025181] Merge Back for MT6292(build error, and DHL for ELT)
 * .
 *
 * 05 07 2013 sheila.chen
 * [MOLY00008234] [MT6572/MT6582] Integration
 * remove unused code
 *
 * 05 06 2013 sheila.chen
 * [MOLY00021578] Vm data lost in long call.
 * add vm callback function when getting vm data even the buffer is full.
 *
 * 04 26 2013 scholar.chang
 * [MOLY00020926] [Speech]DVT test code check-in
 *
 * 04 08 2013 sheila.chen
 * [MOLY00013672] 【G610T】Modem assert，概率?高
 * Fix bug that using incorrect AM state to check idle/call VM record. This will fail under BT <--> receiver/loudspeaker switching.
 *
 * 02 05 2013 sheila.chen
 * [MOLY00008234] [MT6572/MT6582] Integration
 * Provide Dynamic EPL, and vm recording for loopback
 *
 * 01 14 2013 sheila.chen
 * [MOLY00008234] [MT6572/MT6582] Integration
 * vm buffer extend for idle
 *
 * 01 11 2013 sheila.chen
 * [MOLY00008700] [EE][FT][MT6589+Dual Talk][Beijing][1 round][6.4]_第21?_?屏_modem,md1:_A手机
 * fix build error
 *
 * 01 11 2013 sheila.chen
 * [MOLY00008700] [EE][FT][MT6589+Dual Talk][Beijing][1 round][6.4]_第21?_?屏_modem,md1:_A手机
 * vm buffer extend (build error fix)
 *
 * 01 11 2013 sheila.chen
 * [MOLY00008700] [EE][FT][MT6589+Dual Talk][Beijing][1 round][6.4]_第21?_?屏_modem,md1:_A手机
 * vm buffer extend
 *
 * 12 24 2012 jy.huang
 * [MOLY00007920] Trace Reduction
 * .
 *
 * 12 24 2012 sheila.chen
 * [MOLY00002890] Remove Phase out function
 * Remove unused VM page define
 *
 * 12 21 2012 wayne.wang
 * [MOLY00002694] [SAL] Speech abstration layer phased in
 * Add 3G network info into vm
 *
 * 12 05 2012 sheila.chen
 * [MOLY00002890] Remove Phase out function
 * phase out FO (vm)
 *
 * 11 29 2012 wayne.wang
 * [MOLY00002694] [SAL] Speech abstration layer phased in
 * .
 *
 * 11 21 2012 sheila.chen
 * [MOLY00000112] [MT6583] Pre-integration
 * Warming removal 3
 * 
 * Fix CTM potential bug
 *
 * 11 20 2012 sheila.chen
 * [MOLY00000112] [MT6583] Pre-integration
 * 1. Idel VM with EPL bug fix
 * 
 * 2. Warnming Remove
 * 
 * 3. Dynamic feed in the enhancment parameter
 *
 * 11 07 2012 sheila.chen
 * [MOLY00000112] [MT6583] Pre-integration
 * fix VM recording bugs
 * 
 * 10 22 2012 sheila.chen
 * [MOLY00000112] [MT6583] Pre-integration
 * DMNR coefficient setting
 * 
 * 10 17 2012 wayne.wang
 * [MOLY00002694] [SAL] Speech abstration layer phased in
 * SAL - epl plus the refmic logging
 * 
 * 10 16 2012 wayne.wang
 * [MOLY00002694] [SAL] Speech abstration layer phased in
 * SAL - vm and epl record
 * 
 * 09 26 2012 sheila.chen
 * [MOLY00000112] [MT6583] Pre-integration
 * Chip Rename to MT6589
 * Remove unused AFE functions
 * Phase in phone call usage
 * 
 * 09 18 2012 sheila.chen
 * [MOLY00000112] [MT6583] Pre-integration
 * SAL Interrupt Integration
 * 
 * 09 17 2012 sheila.chen
 * [MOLY00000112] [MT6583] Pre-integration
 * 83 phone call prepare, warning removal
 * 
 * 09 11 2012 sheila.chen
 * [MOLY00000112] [MT6583] Pre-integration
 * Phase in call PCM record and EPL record in the same time
 * 
 * 09 10 2012 sheila.chen
 * [MOLY00000112] [MT6583] Pre-integration
 * Phase-in VM/VOC basic
 * 
 * 08 27 2012 thomas.chen
 * [MOLY00001704] [Gemini2.0][R7R8][3G][Overnight][TWN/TWN]l1sm.c 4289 0x0 0x0 0x0 - (LISR)CTIRQ1
 * .
 * 
 * 08 27 2012 thomas.chen
 * [MOLY00002647] [6280_SQC][India FT][Delhi][E1 Dongle]Assert fail: vm.c 847 - L1Audio
 * .
 * 
 * 08 21 2012 sheila.chen
 * [MOLY00000112] [MT6583] Pre-integration
 * Phase in background sound and SAL
 * 
 * 08 21 2012 sheila.chen
 * [MOLY00000112] [MT6583] Pre-integration
 * Phase in background sound and SAL
 * 
 * 07 27 2012 sheila.chen
 * [MOLY00000112] [MT6583] Pre-integration
 * Fix build warning.
 *
 * 07 16 2012 kh.hung
 * [MOLY00000706] Remove compile warning
 * .
 *
 * 06 08 2012 kh.hung
 * [MAUI_03184031] Check-in for MODEM_DEV audio part
 * .
 *
 * 03 30 2012 thomas.chen
 * [MAUI_03149788] MT6280 DVT
 * .
 *
 * 03 15 2012 thomas.chen
 * [MAUI_03149788] MT6280 DVT
 * .
 *
 * 12 22 2011 li.yu
 * [MAUI_03086817] [New feature] VM Logging over Catcher
 * Modify vocTchPcmRecordHisr()
 *
 * 12 22 2011 li.yu
 * [MAUI_03086817] [New feature] VM Logging over Catcher
 * Modify vmRecord() & vmRecordHisr()
 * Add vocWriteVocToCatcher(), vocTchRecordHisr(), vocTchPcmRecordHisr() & voc4WayPcmRecord()
 *
 * 12 22 2011 thomas.chen
 * [MAUI_03103229] EPL address up for MT6255
 * .
 *
 * 12 06 2011 thomas.chen
 * [MAUI_03092859] Add refMIC pcm and AGC gain into VM log
 * .
 *
 * 11 29 2011 lanus.chao
 * [MAUI_03088720] Remove 6235 / 6251 / 6253 / 6268
 * .
 *
 * 11 10 2011 kh.hung
 * [MAUI_03037272] Check-in for MSBB
 * .
 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *==============================================================================
 *******************************************************************************/
// #if defined(__VM_CODEC_SUPPORT__)

#if 0
#include "kal_public_api.h"
#include "kal_general_types.h"
#include "string.h"
#include "reg_base.h"
#include "kal_trace.h"

#include "speech_def.h"
#include "common_def.h"
#include "l1audio.h"
#include "l1audio_trace.h"
#include "am.h"
#include "media.h"
#include "afe.h"
#include "l1sp_trc.h"
#include "l1audio_sph_trc.h"
#include "sal_exp.h"
#include "sal_def.h"
#include "vm.h"
#endif 

#include "monids.h"
#include "monapi.h"

#include "hwdaudioservice.h"
#include "hwdam.h"
#include "hwdvm.h"
#include "hwdspherr.h"
#include "hwdsph.h"
#include "hwdsal_def.h"
#include "hwdsal_exp.h"

#define VM_LOG_DEBUG
//#define VM_VR_RERECORD

#define VM_CTRL_UL         1
#define VM_CTRL_DL         1

#define VM_CTRL_SC_START   3
#define VM_CTRL_SD_START   6

#define VM_STATE_IDLE         0x0
#define VM_STATE_RECORD       0x1
#define VM_STATE_RECORD_PAUSE 0x2
#define VM_STATE_PLAY         0x4
#define VM_STATE_PLAY_PAUSE   0x8
#define VM_STATE_STOP         0x10 
#define VM_STATE_RECORD_STOP  0x20 // entering vmStop

#define VM_BASIC_VM_DATA_SIZE    37 //SyncWord(1), header(1), vm.control(1), counter(1), counter(1), 16*2
#define VM_2G_CTRL_DL_DEBUG_SIZE 44

#define VM_4WAYPCM_DEBUG_SIZE     642 //320 + 320 + 2
#define VM_REFMICPCM_DEBUG_SIZE   324 //160 + 160 + 2 + 2
#define VM_ECHOREF_PCM_DEBUG_SIZE 322 //320 + 2

#define VM_3G_DSP_DEBUG_SIZE     22 //DSP 15 + L1_info 2 + crc_result|DCH_On/Off 1 + s_value 1 + tpc_SIR_lta 1 + dpdch_SIR_lta 1 + TFCI_max_corr 1
#define VM_3G_MCU_DEBUG_SIZE     37 //buffer status 1 + UL 17 + DL 19
#define VM_3G_DEBUG_SIZE         ( VM_3G_DSP_DEBUG_SIZE + VM_3G_MCU_DEBUG_SIZE )
#define VM_3G324M_DSP_DEBUG_SIZE 22 //DSP 15 + L1_info 2 + crc_result|DCH_On/Off 1 + s_value 1 + tpc_SIR_lta 1 + dpdch_SIR_lta 1 + TFCI_max_corr 1
#define VM_3G324M_MCU_DEBUG_SIZE 37 //buffer status 1 + UL 17 + DL 19
#define VM_3G324M_DEBUG_SIZE     ( VM_3G324M_DSP_DEBUG_SIZE + VM_3G324M_MCU_DEBUG_SIZE )
#define VM_MAXIMUM_SAVE_SIZE     91 //VM_3G_MCU_DEBUG_SIZE + VM_BASIC_VM_DATA_SIZE

#if 1 // defined(__DUAL_MIC_SUPPORT__) || defined(__SMART_PHONE_MODEM__)	
#define SIX_PCM_SAVE_SIZE            ((VM_4WAYPCM_DEBUG_SIZE*2) + VM_REFMICPCM_DEBUG_SIZE + VM_ECHOREF_PCM_DEBUG_SIZE)
#endif

#define FIVE_PCM_SAVE_SIZE           ((VM_4WAYPCM_DEBUG_SIZE*2) + VM_ECHOREF_PCM_DEBUG_SIZE)

// VM_MAGIC_HEADER(1), header(2), vm_lost & pcm_Lost(1),vm_counter(1), PCM (2*2*160)
#define VM_BUFFER_SIZE           (513 + 3000)//#### the expected number of frame within 40ms UL,and that within 40ms DL
static uint16 vmRecBuf[VM_BUFFER_SIZE];

#define VM_MAGIC_HEADER 0xAA88

#include "hwdlinkapi.h"
#include "hwdsph.h"


/* ------------------------------------------------------------------------------ */
static struct{
   // mediaControl   *ctrl;   // Use for media, phase out. 
   uint16          type;
   uint16         control_1;//new vm format from MT6280
   uint16         control_2;//new vm format from MT6280
   // uint32         rb_data_len;  /* actual data size (in word) in ring buffer */ //playback used, phase out
   int16          *vmBuf;    /* point to buffer for VM                              */   
   int16          *pcmBuf;   /* point to buffer for 1st set UL-DL PCM data         */   
   uint16         debug_info; /* 0  : vm (speech codec data) only                   */
                              /* b1 : record 1st set of UL-DL PCM buffer for SPE    */
                              /* b2 : record 2nd set of UL-DL PCM buffer for SPE    */ 
   uint8          state; // record DSP  runnning status (including 2 hisr)
   uint8          vm_lost; 
   uint8          vm_lost_count;
   uint8          pcm_lost_count;                                                                  
   uint8          sc_mode;
   uint8          sd_mode;
   uint32         pcm_save_size;

	
	void (*vm_hdlr)(void); // callback function for vm logging
	bool isVocOn; // only use under call AM_IsSpeechOn()
	bool isVmLOn; // only use under call AM_IsSpeechOn()
	bool isIdleVmOn; // idle VM record, exclusive with "isVocOn and isVmLOn"

	// related to 'vmRecOutputBuf', which is cycular buffer use to buffer formatted vm data from dsp
	uint16 pOutputBufWrite; 
	uint16 pOutputBufRead;
	uint16 outputBufSize;

	uint16 audId;
} vm;

#if defined( VM_LOG_DEBUG )
uint8   vm_counter;
#endif

#define VM_PCM_1ST_SET_RECORD_FLAG  0x1 
#define VM_PCM_2ND_SET_RECORD_FLAG  0x2
#define VM_VM_RECORD_FLAG           0x4
#define VM_PCM_REFMIC_RECORD_FLAG   0x8
#define VM_PCM_BAND_FLAG_UL_PRE		0x0010
#define VM_PCM_BAND_FLAG_UL_POS		0x0020
#define VM_PCM_BAND_FLAG_DL_PRE		0x0040
#define VM_PCM_BAND_FLAG_DL_POS		0x0080
#define VM_PCM_BAND_FLAG_UL2_POS	0x0100
#define VM_PCM_BAND_FLAG_UL3_POS	0x0200
#define VM_PCM_BAND_FLAG_UL4_POS	0x0400
//For future use
//#define VM_PCM_BAND_FLAG_AUX1     0x0800

#define VM_PCM_ECHO_REF_RECORD_FLAG 0x1000
//#define VM_PCM_AUX1_RECORD_FLAG     0x2000

#define TCH_VM_INT_FLAG             0x1 
#define TCH_PCM_INT_FLAG            0x2 



#define VMREC_OUTPUT_BUF_SIZE (3400)

static uint16 vmRecOutputBuf[VMREC_OUTPUT_BUF_SIZE];
#if 1 // defined(__DUAL_MIC_SUPPORT__) || defined(__SMART_PHONE_MODEM__)	
static int16 eplInputBuf[SIX_PCM_SAVE_SIZE];
#else
static int16 eplInputBuf[FIVE_PCM_SAVE_SIZE];
#endif



#define VM_3G_NETWORK_INFO_LEN 7
uint16 vm3GNetworkInfo[VM_3G_NETWORK_INFO_LEN];

// TODO: time stamp function
// extern uint32 L1I_GetTimeStamp( void );


#define GET_VM_LENGTH(vmctrl_1, vmctrl_2) AM_GetSpeechPatternLength((vmctrl_1>> 1) & 0x3F) \
+ ((vmctrl_2 & 1)?(AM_GetSpeechPatternLength((vmctrl_2 >> 1) & 0x3F)):0) + 7

                                

#if defined( __UMTS_RAT__ )  || defined( __UMTS_TDD128_MODE__ )
#define _EXTRA_LOG_FOR_BIT_TRUE_
#endif

#if defined(_EXTRA_LOG_FOR_BIT_TRUE_)
extern bool g_bNeedExtraLog;
#endif

// #pragma arm section code="SECONDARY_ROCODE"


static void IDMA_ReadFromDSP(uint16 *dst, volatile uint16 *src, uint32 length)
{
   int32 i;
   for( i = length; i > 0; i-- )    
       *dst++ = *src++;
}





//use this function instead of Media_WriteDataDone to avoid media.waiting false usage
int32 vmRec_queryOutputBufFreeSpace( void )
{
   int32 count;

   count = vm.pOutputBufRead + vm.outputBufSize - vm.pOutputBufWrite -1; 

   if( count >= vm.outputBufSize )
      count -= vm.outputBufSize;
   return count;
}

void vmRec_getWriteBuffer( uint16 **buffer, uint32 *bufLen )
{
   int32 count;

	if( vm.pOutputBufRead > vm.pOutputBufWrite )
      count = vm.pOutputBufRead  - vm.pOutputBufWrite - 1;
   else if( vm.pOutputBufRead == 0 )
      count = vm.outputBufSize - vm.pOutputBufWrite - 1;
   else
      count = vm.outputBufSize - vm.pOutputBufWrite;

   *buffer = &vmRecOutputBuf[vm.pOutputBufWrite];
   *bufLen = (uint32)count;
}

// static void vmWriteDataDone( uint32 len )
static void vmRec_writeDataDone( uint32 len )
{
	vm.pOutputBufWrite += len;
   if( vm.pOutputBufWrite >= vm.outputBufSize)
      vm.pOutputBufWrite = vm.pOutputBufWrite-vm.outputBufSize;

	// kal_prompt_trace(MOD_L1SP, "vmRec_writeDataDone len=%d word", len);
}

static void vmRec_writeVmToOutputBuf( uint16 vmLen, uint16 *vmBuf ) // a frame of VM must be written to MB
{
	uint16 *toPtr;
	uint32 segment, I;

	
   vmRec_getWriteBuffer( &toPtr, &segment );
   MonTrace(MON_CP_HWD_SPH_VM_WRITE_TO_OUTBUF_TRACE_ID, 4, 1, toPtr, segment, vmLen);


   if(segment > vmLen)      
      segment = vmLen;

   for( I = segment ; I > 0 ; I-- ){
		*toPtr++ = *vmBuf++;		
   }
   
   vmRec_writeDataDone( segment );

	// second segment
   vmLen -= segment;	// unprocess length
   if(vmLen > 0 )
   {
      vmRec_getWriteBuffer( &toPtr, &segment );
	  MonTrace(MON_CP_HWD_SPH_VM_WRITE_TO_OUTBUF_TRACE_ID, 4, 2, toPtr, segment, vmLen);	  

		if(segment > vmLen)      
      		segment = vmLen;
		
		for( I = segment ; I > 0 ; I-- ){
			*toPtr++ = *vmBuf++;
		}
      vmRec_writeDataDone( segment );
   }
   return;
}

/**
	@buf1: pointer to pcm buf1, 
	@len1:length of buf1, unit is word(2byte)
	@buf2:pointer to pcm buf2
	@len2: length of buf2. unit is word(2byte)
*/
void VmRec_GetReadBufs(uint32 *add1, uint16 *len1, uint32 *add2, uint16 *len2)
{

	if(vm.pOutputBufWrite >= vm.pOutputBufRead){ // write > read, 1 buf segment
	
		*add1 = (uint32)(&vmRecOutputBuf[vm.pOutputBufRead]);		
		*len1 = (vm.pOutputBufWrite - vm.pOutputBufRead);		
		*add2 = 0;
		*len2 = 0;
	
	} else { //write < read, 2 buf segment
		*add1 = (uint32)(&vmRecOutputBuf[vm.pOutputBufRead]);
		*len1 = (vm.outputBufSize - vm.pOutputBufRead);
		*add2 = (uint32)(vmRecOutputBuf);
		*len2 = (vm.pOutputBufWrite);		
	}		
	//pcmRec.bufReadFrame = curWriteFrame;
}

/**
	@len: unit is word
*/
void VmRec_ReadDataDone(uint16 len)
{
	vm.pOutputBufRead += len;
   if( vm.pOutputBufRead >= vm.outputBufSize)
      vm.pOutputBufRead = vm.pOutputBufRead-vm.outputBufSize;
}

static void vocWriteVocToCatcher( uint32 vmLen, uint16 *vmBuf )
{
	ASSERT(0, HWDSPH_ERR_ENTER_VOC, 0);
	(void) vmLen;
	(void) vmBuf;
	/*
   uint32 len = vmLen*2;
   uint16 *buf = vmBuf;

   // kal_dev_trace(TRACE_GROUP_VM, RECORD_LENGTH, len);
   while(len > 1000)
   {
      tst_vc_response(TVCI_VM_LOGGING, (const uint8*)buf, 1000);
	  buf += 500;
	  len -= 1000;
   }
   tst_vc_response(TVCI_VM_LOGGING, (const uint8*)buf, len);
   */
}

void vmFormatter_vm(int16 *vmBuf)
{	
   volatile uint16   *addr;
   uint16   I, sc_mode, sd_mode, sc_len, sd_len;	
   uint32   vmLen = 0;
	Sal_VM_Frame vmfrm_t;
	
	SAL_VM_GetFrame2G(&vmfrm_t);
	
	sc_mode = vmfrm_t.enc_mode;
	sd_mode = vmfrm_t.dec_mode;

	/*wayne ??
	if( sc_mode>2 && sc_mode<11 ){
		I = *DSP_RX_TCH_S(0,17) >> 8;
	sd_mode = I & 0x1F;
		I = *DSP_TX_TCH_S(0,17) >> 8;
	sc_mode = I & 0x1F;
	}*/
#if defined( VM_LOG_DEBUG )
	/*for catcher log codec information*/
	if( vm.sc_mode != sc_mode || vm.sd_mode != sd_mode ){
		// TODO: c2k [trace]
		// L1Audio_Msg_SPEECH_CODEC( L1SP_Speech_Codec_Mode(sc_mode), L1SP_Speech_Codec_Mode(sd_mode) );
		vm.sc_mode = sc_mode;
		vm.sd_mode = sd_mode;
	}
#endif
	sc_len = AM_GetSpeechPatternLength(sc_mode);
	ASSERT( sc_len > 0, HWDSPH_ERR_FORCE_ASSERT, 0);
	sd_len = AM_GetSpeechPatternLength(sd_mode);
	ASSERT( sd_len > 0, HWDSPH_ERR_FORCE_ASSERT, 0);	
		
	/*if( sc_mode <= 2 ) {
		vm.control =((*DSP_RX_TCH_S(0,0) & 0x3E) << 10) |
						((*DSP_TX_TCH_S(0,0) & 2)<< 9 ) |
						(sd_mode << VM_CTRL_SD_START) |
						(sc_mode << VM_CTRL_SC_START) |
						(vm.control & 3);
	}
	else {
		I = *DSP_RX_TCH_S(0,0) >> 1;
		I = ((I & 0x10)>>1) | (I & 0x07);
		vm.control =(I << 12) |
						((*DSP_TX_TCH_S(0,0) & 3)<< 10 ) |
						(sd_mode << VM_CTRL_SD_START) |
						(sc_mode << VM_CTRL_SC_START) |
						(vm.control & 3);
	}*/
	vm.control_1 = (sc_mode << 1) | (vm.control_1 & 1);
	vm.control_2 = (sd_mode << 1) | (vm.control_2 & 1);

	if(sc_mode > 2){
	I = vmfrm_t.enc_hdr & 3;//bit0, bit1
	vm.control_1 = vm.control_1 | (I << 7); //Tx
	}
	else{
	I = (vmfrm_t.enc_hdr & 2);//sp_flag
	vm.control_1 = vm.control_1 | (I << 10); 
	//add 3G_Mode here, where is dsp's 3g mode indicator
	}

	if(sd_mode > 2){
	I = (vmfrm_t.dec_hdr & 14) >> 1;//bit1, bit2, bit3
	vm.control_2 = vm.control_2 | (I << 7);	// Rx
	}
	else{
	I = (vmfrm_t.dec_hdr & 0x3E);
	vm.control_2 = vm.control_2 | (I << 10);
	}

		{	
		// extern uint32 L1I_GetTimeStamp( void );
		uint32 J;

		*vmBuf++ = VM_MAGIC_HEADER;
		J = L1SP_GetState();
		I = (uint16)( ( (SAL_VM_DBGINFO_LEN + SAL_VM_ENH_DBGINFO_LEN + SAL_VM_DRV_DBGINFO_LEN) << 3 ) | J);				  
		*vmBuf++ = I;
		*vmBuf++ = VM_VM_RECORD_FLAG;
		J = 0; // L1I_GetTimeStamp();
		I = (uint16)(J & 0xFFFF);
		*vmBuf++ = I;
		I = (uint16)((J >> 16) + ((uint32)vm_counter << 8));
		*vmBuf++ = I;
		vm_counter++;
		}


	*vmBuf++ = vm.control_1;
	*vmBuf++ = vm.control_2;
	vmLen+=2;


	if( vm.control_1 & 1 ) {
		addr = vmfrm_t.enc_hb_addr;
		vmLen += sc_len;
		for( I = 0; I < sc_len; I++ )
			*vmBuf++ = *addr++;
	}

	if( vm.control_2 & 1 ) {
		addr = vmfrm_t.dec_hb_addr;
		vmLen += sc_len;
		for( I = 0; I < sd_len; I++ )
			*vmBuf++ = *addr++;

	 addr = vmfrm_t.dbgInfo_addr;  
	 for( I = 0; I < SAL_VM_DBGINFO_LEN; I++ )
				*vmBuf++ = *addr++;

	 addr = vmfrm_t.enh_dbgInfo_addr;  
	 for( I = 0; I < SAL_VM_ENH_DBGINFO_LEN; I++ )
				*vmBuf++ = *addr++;

	 for(I = 0; I < SAL_VM_DRV_DBGINFO_LEN; I++){
			    *vmBuf++ = 0;
		}

	}

	// kal_prompt_trace(MOD_L1SP, "vmLen: %d words", vmLen);
}


/*
#if defined(__UMTS_RAT__) || defined( __UMTS_TDD128_MODE__ )
   	void sp3g_vmRecordService( uint32 *l1_info, uint16 crc_result, uint16 buf_status, uint16 *ul_frame_data, uint16* dl_frame_data, uint8 dl_count );
#endif
*/
void vmRecordHisr( void )
{
   // kal_uint32 module_id = MOD_L1AUDIO_SPH_SRV;
   //volatile uint16   *addr;
   uint16   I; //, sc_mode, sd_mode, sc_len, sd_len;
   uint16   *vmBuf;
   // uint32   vmLen = 0;

	/*
#if defined(__UMTS_RAT__) || defined( __UMTS_TDD128_MODE__ )
   if( L1SP_GetState() == L1SP_STATE_3G_SPEECH_ON || L1SP_GetState() == L1SP_STATE_3G324M_SPEECH_ON )
	{
		sp3g_vmRecordService(0, 0, 0, 0, 0, 0);
		return;
	}
#endif
	*/
	// dsp status check
   if( vm.state != VM_STATE_RECORD )
      return;

	// application status check
	if( (false == vm.isIdleVmOn) && (false == vm.isVocOn) && (false == vm.isVmLOn))
		return;


#if defined(__MA_L1__)
   {
      uint8 rx_type, tx_type, BFI;
      uint16 rx_debug;
      rx_debug = *DSP_RX_TCH_S(0,0);
      tx_type = *DSP_RX_TCH_S(0,0) & 0x3;
      rx_type = (rx_debug >> 1) & 0x7;
      BFI = (rx_debug >> 5) & 0x1;   //speechBFI
      if( rx_type < 3 )//non-amr
         rx_type = 8 + (rx_type << 1) + BFI;
      BFI = (rx_debug >> 8) & 0xFF;  //BER
      L1Audio_Msg_SPEECH_FRAME( L1SP_Speech_TX_Type(tx_type), L1SP_Speech_RX_Type(rx_type), BFI);
      L1Audio_Msg_VM_DEBUG( DP2_3G_debug_info0, DP2_3G_debug_info1, DP2_3G_debug_info7 );
   }
#endif

   vmBuf = (uint16 *)vm.vmBuf;
   if( vmBuf[0] == VM_MAGIC_HEADER ){//already buffer VM
      {
               I = (uint16)( vmBuf[1] >> 3 ) + AM_GetSpeechPatternLength((vmBuf[5]>> 1) & 0x3F) 
                                + ((vm.control_2 & 1)?(AM_GetSpeechPatternLength((vmBuf[6] >> 1) & 0x3F)):0) 
                                + 7; //####
		}
		      
      if( vmRec_queryOutputBufFreeSpace() < I ) {
			MonFault(MON_HWDSPH_FAULT_UNIT, HWDSPH_ERR_VMREC_HISR_VM_DATA_LOST_DUE_TO_BUF_FULL, vmRec_queryOutputBufFreeSpace(), MON_CONTINUE);
			MonFault(MON_HWDSPH_FAULT_UNIT, HWDSPH_ERR_VMREC_HISR_VM_DATA_LOST_DUE_TO_BUF_FULL, I, MON_CONTINUE);
			vm.vm_hdlr();
         return;
      }
      else{
         vmRec_writeVmToOutputBuf(I, vmBuf );
			vm.vm_hdlr();
			
         vm.vm_lost_count = 0;
         //*(uint32 *)vm.vmBuf = 0;
         // Jade C2K change compiler, only support 2 byte alignment
        *(int8 *)vm.vmBuf = 0;//reset
		*((int8 *)vm.vmBuf+1) = 0;//reset
		*((int8 *)vm.vmBuf+2) = 0;//reset
		*((int8 *)vm.vmBuf+3) = 0;//reset

      }
   }
	vmFormatter_vm(vm.vmBuf);
}





#if 1 // defined(__DUAL_MIC_SUPPORT__) || defined(__SMART_PHONE_MODEM__)	
static void vmRefMic_AGC_PcmRecord( bool isToVoc, volatile uint16 *addr, uint16 len )
{
   uint32 I, tmp = 0;
   uint16 *buf, *mBuf;
   uint16 logsize;

	// kal_prompt_trace(MOD_L1SP, "vmRefMic_AGC_PcmRecord");
	
   logsize = len + 2 + 2;
   
   buf = (uint16 *)vm.pcmBuf; 
   *buf++ = (vm.vm_lost_count << 8) + vm.pcm_lost_count;
   *buf++ = vm_counter ;   
   
   {//RefMic
      IDMA_ReadFromDSP((uint16*)buf, addr, len);
      buf += len;
	  *buf++ = SAL_AGC_GetSWGain(0);
	  *buf = SAL_AGC_GetSWGain(1);

   }

   buf = (uint16 *)vm.pcmBuf;

	if(isToVoc){
		vocWriteVocToCatcher(logsize, buf);   
		
	} else { // normal vm process
	
	   // Media_GetWriteBuffer( &mBuf, &tmp );
	   vmRec_getWriteBuffer( &mBuf, &tmp );
	   if( tmp > logsize )
	      tmp = logsize;
	   for( I = tmp ; I > 0 ; I-- )
	      *mBuf++ = *buf++;
		
	   vmRec_writeDataDone(tmp);
	   tmp = logsize - tmp;
	   if( (int32)tmp > 0 ){
	      // Media_GetWriteBuffer( &mBuf, &I );
	      vmRec_getWriteBuffer( &mBuf, &I );
	      for( I = tmp ; I > 0 ; I--) {
	         *mBuf++ = *buf++;				
	      }
	      vmRec_writeDataDone(tmp);
	   }
	}
}

#endif

static void vmEchoRefPcmRecord( bool isToVoc, volatile uint16 *addr, uint16 len )
{
   uint32 I, tmp = 0;
   uint16 *buf, *mBuf;
   uint16 logsize;

	// kal_prompt_trace(MOD_L1SP, "vmRefMic_AGC_PcmRecord");
	
   logsize = len + 2;
   
   buf = (uint16 *)vm.pcmBuf;
   *buf++ = (vm.vm_lost_count << 8) + vm.pcm_lost_count;
   *buf++ = vm_counter ;
   
   IDMA_ReadFromDSP((uint16*)buf, addr, len);
   buf = (uint16 *)vm.pcmBuf;
   
	if(isToVoc){
		vocWriteVocToCatcher(logsize, buf);   
		
	} else { // normal vm process
	
	   // Media_GetWriteBuffer( &mBuf, &tmp );
	   vmRec_getWriteBuffer( &mBuf, &tmp );
	   if( tmp > logsize )
	      tmp = logsize;
	   for( I = tmp ; I > 0 ; I-- )
	      *mBuf++ = *buf++;
		
	   vmRec_writeDataDone(tmp);
	   tmp = logsize - tmp;
	   if( (int32)tmp > 0 ){
	      // Media_GetWriteBuffer( &mBuf, &I );
	      vmRec_getWriteBuffer( &mBuf, &I );
	      for( I = tmp ; I > 0 ; I--) {
	         *mBuf++ = *buf++;				
	      }
	      vmRec_writeDataDone(tmp);
	   }
	}
}

static void vm4WayPcmRecord(volatile uint16 *ul_addr, uint16 ul_len, volatile uint16 *dl_addr, uint16 dl_len)
{
   uint32 I;
	uint32 tmp = 0;
   uint16 *buf, *mBuf;
   uint16 logsize;


   logsize = ul_len + dl_len + 2;
	

   buf = (uint16 *)vm.pcmBuf; 
   // header for record 1st set of UL-DL PCM data 
   *buf++ = (vm.vm_lost_count << 8) + vm.pcm_lost_count;
   *buf++ = vm_counter ;
   
   // Uplink
   IDMA_ReadFromDSP((uint16*)buf, ul_addr, ul_len);
   buf+= ul_len;
   
   // Downlink 
   IDMA_ReadFromDSP((uint16*)buf, dl_addr, dl_len);
   
   buf = (uint16 *)vm.pcmBuf;
   vmRec_getWriteBuffer( &mBuf, &tmp );
   MonTrace(MON_CP_HWD_SPH_VM_WRITE_TO_OUTBUF_TRACE_ID, 4, 1, mBuf, tmp, logsize);
   if( tmp > logsize )
      tmp = logsize;
   for( I = tmp ; I > 0 ; I-- ) {
      *mBuf++ = *buf++;		
   }
   vmRec_writeDataDone(tmp);
   
   tmp = logsize - tmp; // tmp is size did not write to Buffer
   if( (int32)tmp > 0 ){
      vmRec_getWriteBuffer( &mBuf, &I );
	  MonTrace(MON_CP_HWD_SPH_VM_WRITE_TO_OUTBUF_TRACE_ID, 4, 2, mBuf, I, tmp);
   
		if(I>=tmp){
			for( I = tmp ; I > 0 ; I--) {
      	   		*mBuf++ = *buf++;      	   
			}
			vmRec_writeDataDone( tmp );
		} else {
			MonFault(MON_HWDSPH_FAULT_UNIT, HWDSPH_ERR_VMREC_HISR_VM_DATA_LOST_DUE_TO_BUF_FULL, I, MON_CONTINUE); // free size
			MonFault(MON_HWDSPH_FAULT_UNIT, HWDSPH_ERR_VMREC_HISR_VM_DATA_LOST_DUE_TO_BUF_FULL, tmp, MON_CONTINUE); // vm size			
		}
   }   
}

#if 0 // no VoC
static void voc4WayPcmRecord( volatile uint16* ul_addr, uint16 ul_len, volatile uint16* dl_addr, uint16 dl_len)
{
   uint16 *buf;

   buf = (uint16 *)vm.pcmBuf; 
   // header for record 1st set of UL-DL PCM data 
   *buf++ = (vm.vm_lost_count << 8) + vm.pcm_lost_count;
   *buf++ = vm_counter ;

	// make it interleave	
   // Uplink
   IDMA_ReadFromDSP((uint16*)buf, ul_addr, ul_len);
   buf+= ul_len;
   
   // Downlink 
   IDMA_ReadFromDSP((uint16*)buf, dl_addr, dl_len);
   
   buf = (uint16 *)vm.pcmBuf;

   vocWriteVocToCatcher(ul_len + dl_len + 2, buf);
}
#endif 

void vmTchPcmRecordHisr(void *param)
{
   uint32 I, tmp = 0;
   // uint16 dummy;
   uint16 *buf; //, *mBuf;
   Sal_EPL_Frame eplfrm_t;
   uint16 pcmsize;
		
   // check pre-on and codec close
   if( spc2k.bCodRdy==false && L1SP_GetState() != L1SP_STATE_2G_SPEECH_ON)
      return;

   // dsp status check
   if( vm.state != VM_STATE_RECORD )
      return;

   // application status check
	if( (false == vm.isIdleVmOn) && (false == vm.isVocOn) && (false == vm.isVmLOn))
		return;

	SAL_EPL_GetFrame(&eplfrm_t);

	pcmsize = 0;
   if(vm.debug_info & VM_PCM_1ST_SET_RECORD_FLAG)
   		pcmsize += eplfrm_t.ul_pre_len + eplfrm_t.dl_pre_len;
   if(vm.debug_info & VM_PCM_2ND_SET_RECORD_FLAG)
   		pcmsize += eplfrm_t.ul_pos_len + eplfrm_t.dl_pos_len;
   if(vm.debug_info & VM_PCM_REFMIC_RECORD_FLAG)
   		pcmsize += eplfrm_t.ul2_pos_len;
   if(vm.debug_info & VM_PCM_ECHO_REF_RECORD_FLAG)
   	  pcmsize += eplfrm_t.ul4_pos_len;
   	
	
   // handing the total size I (vm + pcm buffer) in word(2byte)
   buf = (uint16 *)vm.vmBuf;
   if( buf[0] == VM_MAGIC_HEADER ){//already buffer VM, (normal case)
      tmp = ( buf[1] >> 3 ) + pcmsize; // debug size (vm buf + pcm buf)
      {
         I = tmp + GET_VM_LENGTH(vm.control_1, vm.control_2);
      }      
	
   } else { // case when previous vmbufer is missing! Save the pcm data with empty vm 
      tmp = pcmsize;  //debug size(only PCM)
      I = pcmsize + 3 + 2;// plus sync word and format, and 2 timestamp
	 // TODO:	
   }

   buf[2] &= 0xf80f;//clean the epl band flag
   if(eplfrm_t.ul_pre_len == 320)
   		buf[2] |= VM_PCM_BAND_FLAG_UL_PRE;
   if(eplfrm_t.ul_pos_len == 320)
   		buf[2] |= VM_PCM_BAND_FLAG_UL_POS;
   if(eplfrm_t.dl_pre_len == 320)
   		buf[2] |= VM_PCM_BAND_FLAG_DL_PRE;
   if(eplfrm_t.dl_pos_len == 320)
   		buf[2] |= VM_PCM_BAND_FLAG_DL_POS;
   if(eplfrm_t.ul2_pos_len == 320)
   		buf[2] |= VM_PCM_BAND_FLAG_UL2_POS;
   if(eplfrm_t.ul3_pos_len == 320)
   		buf[2] |= VM_PCM_BAND_FLAG_UL3_POS;
	 if(eplfrm_t.ul4_pos_len == 320)
   		buf[2] |= VM_PCM_BAND_FLAG_UL4_POS;

	//when vm logging and VOC are both on, only send the data to vm logging to reduce log size
	// if(vm.isVmLOn || ((!vm.isVmLOn) && (!vm.isVocOn) && (VM_STATE_RECORD == vm.state))){	 // Vm Logging  or idle VM record
	if(vm.isVmLOn || vm.isIdleVmOn) { // Vm Logging  or idle VM record
			
		if( vmRec_queryOutputBufFreeSpace() < I )
	   { // drop this log and add counter
	      vm.pcm_lost_count++; 
			MonFault(MON_HWDSPH_FAULT_UNIT, HWDSPH_ERR_VMREC_HISR_VM_DATA_LOST_DUE_TO_BUF_FULL, vmRec_queryOutputBufFreeSpace(), MON_CONTINUE);
			MonFault(MON_HWDSPH_FAULT_UNIT, HWDSPH_ERR_VMREC_HISR_VM_DATA_LOST_DUE_TO_BUF_FULL, I, MON_CONTINUE);
			vm.vm_hdlr();
	      return;       
	   } else {
	      buf[0] = VM_MAGIC_HEADER;
	      buf[1] = (uint16)(tmp << 3) | ( L1SP_GetState()) ;	               
	      buf[2] |= ( vm.debug_info & (VM_PCM_REFMIC_RECORD_FLAG + VM_PCM_1ST_SET_RECORD_FLAG + VM_PCM_2ND_SET_RECORD_FLAG + VM_PCM_ECHO_REF_RECORD_FLAG) );              
	   }
		// vm buf 
		I -= pcmsize; // I is vm buffer size, 
		vmRec_writeVmToOutputBuf(I, buf );
		
		/*	
	   Media_GetWriteBuffer( &mBuf, &tmp );
	   I -= vm.pcm_save_size;
	   if( tmp > I )
	      tmp = I;
	   for( dummy = (uint16)tmp ; dummy > 0 ; dummy-- )
	      *mBuf++ = *buf++;
	   vmWriteDataDone( tmp );
	   I -= tmp;
	   if( (int32)I > 0 ){
	      Media_GetWriteBuffer( &mBuf, &tmp );
	      for( dummy = (uint16)I ; dummy > 0 ; dummy-- )
	         *mBuf++ = *buf++;
	      vmWriteDataDone( I );
	   } */
		//pcm bufs
	   if(vm.debug_info & VM_PCM_1ST_SET_RECORD_FLAG)
	   {
	      vm4WayPcmRecord(eplfrm_t.ul_pre_buf, eplfrm_t.ul_pre_len, eplfrm_t.dl_pre_buf, eplfrm_t.dl_pre_len);			
		  MonTrace(MON_CP_HWD_SPH_VM_EPL_DSPBUF_INFO_TRACE_ID, 6, 
		  	VM_PCM_BAND_FLAG_UL_PRE, eplfrm_t.ul_pre_buf, eplfrm_t.ul_pre_len, 
		  	VM_PCM_BAND_FLAG_DL_PRE, eplfrm_t.dl_pre_buf, eplfrm_t.dl_pre_len);
	   }  
	   

	   if(vm.debug_info & VM_PCM_2ND_SET_RECORD_FLAG)
	   {
	      vm4WayPcmRecord(eplfrm_t.ul_pos_buf, eplfrm_t.ul_pos_len, eplfrm_t.dl_pos_buf, eplfrm_t.dl_pos_len);			
		  MonTrace(MON_CP_HWD_SPH_VM_EPL_DSPBUF_INFO_TRACE_ID, 6, 
		  	VM_PCM_BAND_FLAG_UL_POS, eplfrm_t.ul_pos_buf, eplfrm_t.ul_pos_len, 
		  	VM_PCM_BAND_FLAG_DL_POS, eplfrm_t.dl_pos_buf, eplfrm_t.dl_pos_len);
	   }     
#if 1 // defined(__DUAL_MIC_SUPPORT__) || defined(__SMART_PHONE_MODEM__)	
	   if(vm.debug_info & VM_PCM_REFMIC_RECORD_FLAG)
	   {
	      vmRefMic_AGC_PcmRecord(false, eplfrm_t.ul2_pos_buf, eplfrm_t.ul2_pos_len);
		  MonTrace(MON_CP_HWD_SPH_VM_EPL_DSPBUF_INFO_TRACE_ID, 6,
		  	VM_PCM_BAND_FLAG_UL2_POS, eplfrm_t.ul2_pos_buf, eplfrm_t.ul2_pos_len, 
		  	0, 0, 0);
	   }
#endif

     if(vm.debug_info & VM_PCM_ECHO_REF_RECORD_FLAG)
	   {      
	      vmEchoRefPcmRecord(false, eplfrm_t.ul4_pos_buf, eplfrm_t.ul4_pos_len);	
	      MonTrace(MON_CP_HWD_SPH_VM_EPL_DSPBUF_INFO_TRACE_ID, 6,
		  	VM_PCM_ECHO_REF_RECORD_FLAG, eplfrm_t.ul4_pos_buf, eplfrm_t.ul4_pos_len, 
		  	0, 0, 0);		
	   }
		
		vm.vm_hdlr();
	} else { // VOC,

		ASSERT(0, HWDSPH_ERR_ENTER_VOC, 0);
#if 0		
		// fill vm buffer header 
		buf[0] = VM_MAGIC_HEADER;
		buf[1] = (uint16)(tmp << 3) | ( L1SP_GetState()) ;								
		buf[2] |= ( vm.debug_info & (VM_PCM_REFMIC_RECORD_FLAG + VM_PCM_1ST_SET_RECORD_FLAG + VM_PCM_2ND_SET_RECORD_FLAG + VM_PCM_ECHO_REF_RECORD_FLAG) ); 				 

		I -= pcmsize;
		vocWriteVocToCatcher(I, buf);
		if(vm.debug_info & VM_PCM_1ST_SET_RECORD_FLAG) {
			voc4WayPcmRecord(eplfrm_t.ul_pre_buf, eplfrm_t.ul_pre_len, eplfrm_t.dl_pre_buf, eplfrm_t.dl_pre_len);
		}

		if(vm.debug_info & VM_PCM_2ND_SET_RECORD_FLAG) {
			voc4WayPcmRecord(eplfrm_t.ul_pos_buf, eplfrm_t.ul_pos_len, eplfrm_t.dl_pos_buf, eplfrm_t.dl_pos_len);			
		}

#if 1 // defined(__DUAL_MIC_SUPPORT__) || defined(__SMART_PHONE_MODEM__)	
		if(vm.debug_info & VM_PCM_REFMIC_RECORD_FLAG)
		{
      	vmRefMic_AGC_PcmRecord(true, eplfrm_t.ul2_pos_buf, eplfrm_t.ul2_pos_len);
		}
#endif
		if(vm.debug_info & VM_PCM_ECHO_REF_RECORD_FLAG)
		{
      	vmEchoRefPcmRecord(true, eplfrm_t.ul4_pos_buf, eplfrm_t.ul4_pos_len);
		}
#endif 
		
	}
  // Jade C2K change compiler, only support 2 byte alignment
	*(int8 *)vm.vmBuf = 0;//reset
	*((int8 *)vm.vmBuf+1) = 0;//reset
	*((int8 *)vm.vmBuf+2) = 0;//reset
	*((int8 *)vm.vmBuf+3) = 0;//reset
   vm.pcm_lost_count = 0;
	buf[2] = 0;
}

void vmFormatter_amr(int16 *vmBuf)
{	
   volatile uint16 *addr;
   uint16 I, sc_mode, sd_mode, sc_len, sd_len ; 	
   uint32 J;
   Sal_VM_Frame vmfrm_t;
	/*I = DP_3G_DL_RX_MODE_REPORT >> 8;
	sd_mode = I & 0x1F;
	I = DP_3G_UL_TX_MODE_REPORT >> 8;
	sc_mode = I & 0x1F;*/
	SAL_VM_GetFrame3G(&vmfrm_t);
	sd_mode = vmfrm_t.dec_mode;
	sc_mode = vmfrm_t.enc_mode;

	sc_len = AM_GetSpeechPatternLength(sc_mode);
	ASSERT( sc_len > 0, HWDSPH_ERR_FORCE_ASSERT, 0);
	sd_len = AM_GetSpeechPatternLength(sd_mode);
	ASSERT( sd_len > 0, HWDSPH_ERR_FORCE_ASSERT, 0);	

	I = vmfrm_t.enc_hdr;
	I = I & 0x03;
	vm.control_1 = I << 7 | (sc_mode << 1) | (vm.control_1 & 1);
	if(SAL_3G_Mode())
		vm.control_1 = vm.control_1 | 0x1000;


	I = vmfrm_t.dec_hdr;
	I = I & 0x0e;
	vm.control_2 = I << 6 | (sd_mode << 1) | (vm.control_2 & 1);

	//kal_dev_trace( TRACE_INFO, VM_CONTROL, vm.control );
	// record syn_word for VM_LOG_DEBUG 
		
	*vmBuf++ = VM_MAGIC_HEADER;
	J = L1SP_GetState();
	I = (uint16)( ( (SAL_VM_DBGINFO_LEN + SAL_VM_ENH_DBGINFO_LEN + SAL_VM_DRV_DBGINFO_LEN)<< 3 ) | J);
	*vmBuf++ = I;
	*vmBuf++ = VM_VM_RECORD_FLAG ;			 //VM
	J = 0; // TODO: L1I_GetTimeStamp();
#if defined(_EXTRA_LOG_FOR_BIT_TRUE_)
	if( g_bNeedExtraLog ){
		// TODO: c2k [trace]
		 ; // kal_dev_trace( TRACE_INFO, VM_SP3G_VM_L1T, J );
	}
#endif
	I = (uint16)(J & 0xFFFF);
	*vmBuf++ = I;
	I = (uint16)((J >> 16) + ((uint32)vm_counter << 8));
	*vmBuf++ = I;
	vm_counter++;

	// record vm control value 	
	*vmBuf++ = vm.control_1;
	*vmBuf++ = vm.control_2;

	// record UL data 	
	addr = vmfrm_t.enc_hb_addr;
	for( I = 0; I < sc_len; I++ )
		*vmBuf++ = *addr++;
		
	// record DL data 	
	addr = vmfrm_t.dec_hb_addr;
	for( I = 0; I < sd_len; I++ )
		*vmBuf++ = *addr++;

	addr = vmfrm_t.dbgInfo_addr;  
	 for( I = 0; I < SAL_VM_DBGINFO_LEN; I++ )
		*vmBuf++ = *addr++;

	addr = vmfrm_t.enh_dbgInfo_addr;  
	 for( I = 0; I < SAL_VM_ENH_DBGINFO_LEN; I++ )
		*vmBuf++ = *addr++;

	for(I = 0; I < SAL_VM_DRV_DBGINFO_LEN; I++){
		if(I < VM_3G_NETWORK_INFO_LEN)
		*vmBuf++ = vm3GNetworkInfo[I];
		else
		*vmBuf++ = 0;
		}
	 
}

uint32 SP4G_GetCodecMode(void);
void vmFormatter_4g(int16 *vmBuf)
{
      vmFormatter_amr((int16 *)vmBuf);
}

void vmFormatter_c2k(int16 *vmBuf)
{
   volatile uint16 *addr;
   uint16 I, sc_mode, sd_mode, sc_len, sd_len ; 	
   uint32 J;
   Sal_VM_Frame vmfrm_t;
   MonSysTimeT    SysTime;
	/*I = DP_3G_DL_RX_MODE_REPORT >> 8;
	sd_mode = I & 0x1F;
	I = DP_3G_UL_TX_MODE_REPORT >> 8;
	sc_mode = I & 0x1F;*/
	SAL_VM_GetFrameC2K(&vmfrm_t);
	sd_mode = vmfrm_t.dec_mode;
	sc_mode = vmfrm_t.enc_mode;

	sc_len = AM_GetSpeechPatternLength(sc_mode);
	ASSERT( sc_len > 0, HWDSPH_ERR_FORCE_ASSERT, 0);
	sd_len = AM_GetSpeechPatternLength(sd_mode);
	ASSERT( sd_len > 0, HWDSPH_ERR_FORCE_ASSERT, 0);	

	I = vmfrm_t.enc_hdr; //Enc Rate
	//I = I & 0x03; 
	vm.control_1 = I << 7 | (sc_mode << 1) | (vm.control_1 & 1);
	//if(SAL_3G_Mode())
		vm.control_1 = vm.control_1 | 0x2000; //Call Mode: C2K


	I = vmfrm_t.dec_hdr; //Dec Rate
	//I = I & 0x0e;
	vm.control_2 = I << 7 | (sd_mode << 1) | (vm.control_2 & 1);

	//kal_dev_trace( TRACE_INFO, VM_CONTROL, vm.control );
	// record syn_word for VM_LOG_DEBUG 
		
	*vmBuf++ = VM_MAGIC_HEADER;
	J = L1SP_GetState();
	I = (uint16)( ( (SAL_VM_DBGINFO_LEN + SAL_VM_ENH_DBGINFO_LEN + SAL_VM_DRV_DBGINFO_LEN)<< 3 ) | J);
	*vmBuf++ = I;
	*vmBuf++ = VM_VM_RECORD_FLAG ;			 //VM

	//J = 0; // TODO: L1I_GetTimeStamp();
	SysFrameCountGet(&SysTime);
	
	J = ((uint32)SysTime.SysTime[0]) & 0x00FFFFFF; // index 0 for system time 1x, sysdefs.h 
#if defined(_EXTRA_LOG_FOR_BIT_TRUE_)
	if( g_bNeedExtraLog ){
		// TODO: c2k [trace]
		 ; // kal_dev_trace( TRACE_INFO, VM_SP3G_VM_L1T, J );
	}
#endif
	I = (uint16)(J & 0xFFFF);
	*vmBuf++ = I;
	I = (uint16)((J >> 16) + ((uint32)vm_counter << 8));
	*vmBuf++ = I;
	vm_counter++;

	// record vm control value 	
	*vmBuf++ = vm.control_1;
	*vmBuf++ = vm.control_2;

	// record UL data 	
	addr = vmfrm_t.enc_hb_addr;
	for( I = 0; I < sc_len; I++ )
		*vmBuf++ = *addr++;
		
	// record DL data 	
	addr = vmfrm_t.dec_hb_addr;
	for( I = 0; I < sd_len; I++ )
		*vmBuf++ = *addr++;

	addr = vmfrm_t.dbgInfo_addr;  
	 for( I = 0; I < SAL_VM_DBGINFO_LEN; I++ )
		*vmBuf++ = *addr++;

	addr = vmfrm_t.enh_dbgInfo_addr;  
	 for( I = 0; I < SAL_VM_ENH_DBGINFO_LEN; I++ )
		*vmBuf++ = *addr++;

	for(I = 0; I < SAL_VM_DRV_DBGINFO_LEN; I++){
		if(I < VM_3G_NETWORK_INFO_LEN)
		*vmBuf++ = vm3GNetworkInfo[I];
		else
		*vmBuf++ = 0;
		}
	 

    
}


void vmTchRecordHisr(void *param)
{
	
#if defined(__UMTS_RAT__) || defined( __UMTS_TDD128_MODE__ ) // when 3G
	bool is3G;	
#endif // defined(__UMTS_RAT__) || defined( __UMTS_TDD128_MODE__ )
   uint8    sp_state = L1SP_GetState();	
   uint16   *vmBuf;

	// check pre-on and codec close
	if( spc2k.bCodRdy==false && L1SP_GetState() != L1SP_STATE_2G_SPEECH_ON)
		return;

	// check dsp status	
   if( vm.state != VM_STATE_RECORD )
      return;

	// check application status
	if( (false == vm.isIdleVmOn) && (false == vm.isVocOn) && (false == vm.isVmLOn))
		return;

	// REMIND!! 
	// For MT6280: when vm under 3g call, vm interrupt is not this one. Need DSP's patch 
	// For MT6589: when vm under 3g call, vm interrupt is this one.
	/*
    if( L1SP_GetState() != L1SP_STATE_2G_SPEECH_ON )
      return;
	*/

	/* REMOVE
   if(vm.isVmLOn){  // vm logging
		// TODO: 
		if(vmRec_queryOutputBufFreeSpace() < VM_MAXIMUM_SAVE_SIZE)		
	   {
	      vm.vm_lost = 1; 
	      vm.vm_lost_count ++;
	      // mediaDataNotification();
	      return;       
	   }
	   
	    vm.vm_lost = 0;
		
	    vmRecordHisr();
	    mediaIncRecordTime();  //for update record time
	    mediaDataNotification();        
   }
   */


	
	//check is under 3g or not
#if defined(__UMTS_RAT__) || defined( __UMTS_TDD128_MODE__ ) // when 3G
	is3G = false;
	is3G = ( L1SP_GetState() == L1SP_STATE_3G_SPEECH_ON || L1SP_GetState() == L1SP_STATE_3G324M_SPEECH_ON );	
#endif

	// just logging
#if defined(__MA_L1__)
	if(!is3G) {
		uint8 rx_type, tx_type, BFI;
		uint16 rx_debug;
		rx_debug = *DSP_RX_TCH_S(0,0);
		tx_type = *DSP_RX_TCH_S(0,0) & 0x3;
		rx_type = (rx_debug >> 1) & 0x7;
		BFI = (rx_debug >> 5) & 0x1;	 //speechBFI
		if( rx_type < 3 )//non-amr
			rx_type = 8 + (rx_type << 1) + BFI;
		BFI = (rx_debug >> 8) & 0xFF;  //BER
		L1Audio_Msg_SPEECH_FRAME( L1SP_Speech_TX_Type(tx_type), L1SP_Speech_RX_Type(rx_type), BFI);
		L1Audio_Msg_VM_DEBUG( DP2_3G_debug_info0, DP2_3G_debug_info1, DP2_3G_debug_info7 );
	}
#endif

	// process previous buffer
	vmBuf = (uint16 *)vm.vmBuf;
   if( vmBuf[0] == VM_MAGIC_HEADER ){//already buffer VM
      uint16 I;
      {
         I = (uint16)( vmBuf[1] >> 3 ) + AM_GetSpeechPatternLength((vmBuf[5]>> 1) & 0x3F) 
               + ((vm.control_2 & 1)?(AM_GetSpeechPatternLength((vmBuf[6] >> 1) & 0x3F)):0) 
               + 7; //####
      }
      //write data
      if( vmRec_queryOutputBufFreeSpace() < I ){
         vm.vm_lost = 1;
         vm.vm_lost_count++;
			MonFault(MON_HWDSPH_FAULT_UNIT, HWDSPH_ERR_VMREC_HISR_VM_DATA_LOST_DUE_TO_BUF_FULL, vmRec_queryOutputBufFreeSpace(), MON_CONTINUE);
			MonFault(MON_HWDSPH_FAULT_UNIT, HWDSPH_ERR_VMREC_HISR_VM_DATA_LOST_DUE_TO_BUF_FULL, I, MON_CONTINUE);
			
         vm.vm_hdlr();
         return;
      }
      else{
         //when vm logging and VOC are both on, only send the data to vm logging to reduce log size   
         if(vm.isVmLOn){				
            vmRec_writeVmToOutputBuf(I, vmBuf );
            vm.vm_hdlr();	   
         } else {
            vocWriteVocToCatcher( (uint32)I, vmBuf );
         }
      }

      vm.vm_lost_count = 0;
      //*(uint32 *)vm.vmBuf = 0;
      // Jade C2K change compiler, only support 2 byte alignment
      *(int8 *)vm.vmBuf = 0;//reset
	  *((int8 *)vm.vmBuf+1) = 0;//reset
	  *((int8 *)vm.vmBuf+2) = 0;//reset
	  *((int8 *)vm.vmBuf+3) = 0;//reset
      vmBuf[2] = 0;
   }

	// formatting!!!
	switch(sp_state){
	case L1SP_STATE_C2K_SPEECH_ON:
	    vmFormatter_c2k(vm.vmBuf);
	    break;	
	case L1SP_STATE_4G_SPEECH_ON:
	   vmFormatter_4g(vm.vmBuf);
	   break;
	case L1SP_STATE_3G_SPEECH_ON:   
	case L1SP_STATE_3G324M_SPEECH_ON:   
	   vmFormatter_amr(vm.vmBuf);
	   break;
	default:
	   vmFormatter_vm(vm.vmBuf);
	   break;      
	}
}


extern void UL1D_RXBRP_GET_INFO_FOR_SPEECH_VM(int16* tpc_SIR_lta, int16* dpdch_SIR_lta, int16* TFCI_max_corr);
void vmSet3GNetworkInfo( uint32 *l1_info, uint16 crc_result, uint16 buf_status, uint8 dl_count )
{
	  int16 tpc_SIR_lta, dpdch_SIR_lta, TFCI_max_corr, I;
      uint32 l1Info, s_value;
      if( l1_info == NULL ){
         l1Info = 0;
         tpc_SIR_lta = 0;
         dpdch_SIR_lta = 0;
         TFCI_max_corr = 0;
         s_value = 0;
      }else{
         l1Info = l1_info[1];
         tpc_SIR_lta = l1_info[4];
         dpdch_SIR_lta = l1_info[5];
         TFCI_max_corr = l1_info[6];
         s_value = l1_info[3];
      }
      I = (uint16)(l1Info & 0xFFFF);
      vm3GNetworkInfo[4] = I;
	  
      I = (uint16)(l1Info >> 16);
	  vm3GNetworkInfo[5] = I;

	  vm3GNetworkInfo[3] = crc_result;

      
      vm3GNetworkInfo[6] = (uint16)((s_value >= 32767)? 32767 : s_value);//s_value
#if defined( __MTK_UL1_FDD__ )// only MTK 3G has this function
      if(dl_count == 0){
         UL1D_RXBRP_GET_INFO_FOR_SPEECH_VM( &tpc_SIR_lta,  &dpdch_SIR_lta, &TFCI_max_corr);
      }
#endif
      vm3GNetworkInfo[0] = (uint16)tpc_SIR_lta;
      vm3GNetworkInfo[1] = (uint16)dpdch_SIR_lta;
      vm3GNetworkInfo[2] = (uint16)TFCI_max_corr;
}

/*
void vocTchRecordHisr(void *param)
{
   if( vm.state != VM_STATE_RECORD )
      return;
   
   if( L1SP_GetState() != L1SP_STATE_2G_SPEECH_ON )
      return;
         
   vm.vm_lost = 0;
   vmRecordHisr();    
}
*/

// #pragma arm section

extern void SP3G_Start_VM( void ); 
extern void SP3G_Stop_VM( void ); 
extern bool SP3G_VM_Status( void ); 

void vmSpeechOnHandler( void *state ) 
{
   if(vm.debug_info & (VM_PCM_1ST_SET_RECORD_FLAG + VM_PCM_2ND_SET_RECORD_FLAG))
   {     
      AM_PCM8K_RecordOn(AM_PCM8KREC_APP_TYPE_VMEPL);
   }
#if defined(__UMTS_RAT__) || defined( __UMTS_TDD128_MODE__ )
   //SP3G_Start_VM();marked by wayne 20120808
#endif
   L1Audio_HookHisrHandler( DP_D2C_VM_REC_INT, vmTchRecordHisr, 0 );
   AM_VMRecordOn( (uint16)vm.type );
}

void vmSpeechOffHandler( void *state ) 
{      
   if(vm.debug_info & (VM_PCM_1ST_SET_RECORD_FLAG + VM_PCM_2ND_SET_RECORD_FLAG))
   {      
      AM_PCM8K_RecordOff(false, AM_PCM8KREC_APP_TYPE_VMEPL);      
   }
#if defined(__UMTS_RAT__) || defined( __UMTS_TDD128_MODE__ )
   if(SP3G_VM_Status())
		SP3G_Stop_VM();
#endif
   // *DP_SC_MUTE = 0x0000; // do inside SAL
   L1Audio_UnhookHisrHandler( DP_D2C_VM_REC_INT );
   AM_VMRecordOff();
}

void VMREC_ConfigEpl(void)
{
	// only support phone call VM record dynamic change from AP side
	if( vm.isVocOn || vm.isVmLOn ){
		uint16 debugInfo = L1Audio_GetDebugInfo(VM_DEBUG_INFO);
		// uint32 module_id = MOD_L1AUDIO_SPH_SRV;
#if defined(__ENABLE_SPEECH_DVT__)
		debugInfo = 3; 
#endif // defined(__ENABLE_SPEECH_DVT__)


		MonTrace(MON_CP_HWD_SPH_VM_DEBUG_INFO_TRACE_ID, 1, debugInfo);
			
		// TODO: SP1's request, use inject string to enable EPL. (maybe via ETS) 
		/*
	  if(!tst_trace_check_ps_filter_off(TRACE_GROUP_EPL, &module_id, 0x2)) 			
	  {
			debugInfo |= (VM_PCM_1ST_SET_RECORD_FLAG + VM_PCM_2ND_SET_RECORD_FLAG);
		}

     if(!tst_trace_check_ps_filter_off(TRACE_GROUP_REFMIC, &module_id, 0x2))	
	  {
			debugInfo |= VM_PCM_REFMIC_RECORD_FLAG;
		}
		if(!tst_trace_check_ps_filter_off(TRACE_GROUP_ECHOREF, &module_id, 0x2))	
	  {
			debugInfo |= VM_PCM_ECHO_REF_RECORD_FLAG;
		}
		*/
		
		if( ((debugInfo & (VM_PCM_1ST_SET_RECORD_FLAG + VM_PCM_2ND_SET_RECORD_FLAG))!=0) // config to open
			&& (vm.debug_info & (VM_PCM_1ST_SET_RECORD_FLAG + VM_PCM_2ND_SET_RECORD_FLAG))==0) // did not open yet
		{//EPL open
			debugInfo |= VM_PCM_ECHO_REF_RECORD_FLAG;		
#if 1 // defined(__DUAL_MIC_SUPPORT__) || defined(__SMART_PHONE_MODEM__)	
			debugInfo |= VM_PCM_REFMIC_RECORD_FLAG;
	      if(debugInfo & VM_PCM_REFMIC_RECORD_FLAG){
	         vm.pcm_save_size = SIX_PCM_SAVE_SIZE;
	      }else
#endif
	      {
	         vm.pcm_save_size = FIVE_PCM_SAVE_SIZE;
	      }

			// enable EPL
			memset(eplInputBuf, 0 , sizeof(int16)*vm.pcm_save_size);
			vm.pcmBuf = eplInputBuf; //(int16*)get_ctrl_buffer( vm.pcm_save_size*sizeof(uint16) );
			vm.pcm_lost_count = 0; 
			vm.vm_lost  = 1; // to force save VM first 
			vm.vm_lost_count = 0;
			L1Audio_HookHisrHandler( DP_D2C_VMEPL_REC_INT, (L1Audio_EventHandler)vmTchPcmRecordHisr, 0 ); //
	      AM_PCM8K_RecordOn(AM_PCM8KREC_APP_TYPE_VMEPL);
			vm.debug_info = debugInfo;
		}
	}
		
}

/**
	@vm_hdlr: handler
	@type: using in Idle VM record
	@isVoc: is call from vm over catcher.
*/
// void vmRecord( mediaControl *ctrl, uint16 type )
void VMREC_Start(void (*vm_hdlr)(void), uint16 type, bool isVoc)
{	
   // if( AM_IsSpeechOn() ) {
   if( AM_IsAmInSpeechState() ) {
				
		//decide the open
	   bool isAlreadyOn = (vm.isVocOn || vm.isVmLOn);
		
		MonTrace(MON_CP_HWD_SPH_VM_CTRL_TRACE_ID, 7, VM_STATE_IDLE, VM_STATE_RECORD, true, vm.isVmLOn, vm.isVocOn, vm.isIdleVmOn, isVoc);		

		ASSERT((!vm.isIdleVmOn), HWDSPH_ERR_VM_RE_ENTRY, 0 );
		
		if(isVoc){
			ASSERT(!vm.isVocOn, HWDSPH_ERR_VOC_RE_ENTRY, vm.isVmLOn ); // prevent re-entry
			vm.isVocOn = true;
			// vm.vm_hdlr = NULL; 
		} else { // normal vm 
			ASSERT(!vm.isVmLOn, HWDSPH_ERR_VM_RE_ENTRY, vm.isVocOn ); // prevent re-entry
			// L1SP_Register_Service(vmSpeechOnHandler, vmSpeechOffHandler);
			vm.vm_hdlr = vm_hdlr; 
			vm.isVmLOn = true;			
		}

		
		//already on		
	   if(isAlreadyOn){		
			VMREC_ConfigEpl();
			return;
	   } else {

			// Initials
			// lock sleep mode
			vm.audId = L1Audio_GetAudioID();
			L1Audio_SetFlag( vm.audId );/*Be careful.Before Locking SleepMode, to access DSP sherrif tasks much time. So access DSP must be after SetFlag*/

		   // vm.ctrl = ctrl;
		   
		   vm.type = type;
		   
			//vm buffer initital
		   vm.vmBuf = (int16 *)vmRecBuf; // get_ctrl_buffer( VM_BUFFER_SIZE );//####
		   memset( vm.vmBuf, 0, VM_BUFFER_SIZE );

			// output buffer initial 
			vm.pOutputBufWrite = 0;
			vm.pOutputBufRead = 0;
			vm.outputBufSize = VMREC_OUTPUT_BUF_SIZE; 
			memset( vmRecOutputBuf, 0, VMREC_OUTPUT_BUF_SIZE*sizeof(uint16) );		

				
			// control setup
	      //vm.control = 0x0003;
			vm.control_1 = 0x0001;
			vm.control_2 = 0x0001;

			VMREC_ConfigEpl();
			
		  
#if defined(__UMTS_RAT__) || defined( __UMTS_TDD128_MODE__ )
	      //SP3G_Start_VM();marked by wayne 20120808
#if defined(_EXTRA_LOG_FOR_BIT_TRUE_) && !defined(__UMTS_TDD128_MODE__)
	//TDD load need to reduce trace
	      g_bNeedExtraLog = TRUE;
#endif
#endif
			L1Audio_HookHisrHandler( DP_D2C_VM_REC_INT, (L1Audio_EventHandler)vmTchRecordHisr, 0 ); 
			
			// enalbe normal VM record
	      AM_VMRecordOn( (uint16)type );
			
			vm.state = VM_STATE_RECORD;

	   }
   }
   else {
		ASSERT((!vm.isVocOn) && (!vm.isVmLOn), HWDSPH_ERR_VM_RE_ENTRY, 0 );
		ASSERT(vm.state != VM_STATE_RECORD, HWDSPH_ERR_VM_RE_ENTRY, 0 );

		MonTrace(MON_CP_HWD_SPH_VM_CTRL_TRACE_ID, 7, VM_STATE_IDLE, VM_STATE_RECORD, false, vm.isVmLOn, vm.isVocOn, vm.isIdleVmOn, -1);
		
		
		// Initials
		// lock sleep mode
		vm.audId = L1Audio_GetAudioID();
		L1Audio_SetFlag( vm.audId );/*Be careful.Before Locking SleepMode, to access DSP sherrif tasks much time. So access DSP must be after SetFlag*/

		// setup handler
	   vm.vm_hdlr = vm_hdlr; 
	   vm.type = type;
		vm.isIdleVmOn = true;

		//vm buffer initital
	   vm.vmBuf = (int16 *)vmRecBuf; // get_ctrl_buffer( VM_BUFFER_SIZE );
	   memset( vm.vmBuf, 0, VM_BUFFER_SIZE );

		// output buffer initial 
		vm.pOutputBufWrite = 0;
		vm.pOutputBufRead = 0;
		vm.outputBufSize = VMREC_OUTPUT_BUF_SIZE; 
		memset( vmRecOutputBuf, 0, VMREC_OUTPUT_BUF_SIZE*sizeof(uint16) );	

		
      // for idle-mode recording , will use DP_D2C_SE_DONE , through mediaHisr -> vmRecordHisr
      // vm.control = 0x0001;
		vm.control_1 = 0x0001;
		vm.control_2 = 0x0000;
      // *DP_SC_MUTE = 0x0002; // do inside SAL

		// Read the debug info
		vm.debug_info = L1Audio_GetDebugInfo(VM_DEBUG_INFO); 
		MonTrace(MON_CP_HWD_SPH_VM_DEBUG_INFO_TRACE_ID, 1, vm.debug_info);
		
		L1Audio_HookHisrHandler( DP_D2C_VM_REC_INT, (L1Audio_EventHandler)vmRecordHisr, 0 );
      AM_VMRecordOn( (uint16)type );

		if(vm.debug_info & (VM_PCM_1ST_SET_RECORD_FLAG + VM_PCM_2ND_SET_RECORD_FLAG))
		{//EPL open
		
#if 1 // defined(__DUAL_MIC_SUPPORT__) || defined(__SMART_PHONE_MODEM__)	
	      if(vm.debug_info & VM_PCM_REFMIC_RECORD_FLAG){
	         vm.pcm_save_size = SIX_PCM_SAVE_SIZE;
	      }else
#endif
	      {
	         vm.pcm_save_size = FIVE_PCM_SAVE_SIZE;
	      }

			// enable EPL
			memset(eplInputBuf, 0 , sizeof(int16)*vm.pcm_save_size);
			vm.pcmBuf = eplInputBuf; // (int16*)get_ctrl_buffer( vm.pcm_save_size*sizeof(uint16) );
			vm.pcm_lost_count = 0; 
			vm.vm_lost  = 1; // to force save VM first 
			vm.vm_lost_count = 0;
			L1Audio_HookHisrHandler( DP_D2C_VMEPL_REC_INT, (L1Audio_EventHandler)vmTchPcmRecordHisr, 0 );
         AM_PCM8K_RecordOn(AM_PCM8KREC_APP_TYPE_VMEPL);
		}

	   vm.state = VM_STATE_RECORD;
   }
}


/**
	@isVoc: is call from voice over cater. true is yes; false is no.
*/
void VMREC_Stop( bool isVoc)
{
			

	// check voc & vm are both closed. 
	if(AM_IsAmInSpeechState()){	
		MonTrace(MON_CP_HWD_SPH_VM_CTRL_TRACE_ID, 7, VM_STATE_RECORD, VM_STATE_STOP, true, vm.isVmLOn, vm.isVocOn, vm.isIdleVmOn, isVoc);
	
		if(isVoc){
			ASSERT(vm.isVocOn == true, HWDSPH_ERR_FORCE_ASSERT, 0 );
			vm.isVocOn = false; 
			if(vm.isVmLOn){ //still another need vm 				
				return;
			}
		} else { //record vm from media
			ASSERT(vm.isVmLOn == true, HWDSPH_ERR_FORCE_ASSERT, 0 );
			vm.isVmLOn = false;
			if(vm.isVocOn){ //still another need vm			
				return;
			}
		}
   }else {
		MonTrace(MON_CP_HWD_SPH_VM_CTRL_TRACE_ID, 7, VM_STATE_RECORD, VM_STATE_STOP, false, vm.isVmLOn, vm.isVocOn, vm.isIdleVmOn, -1);
	  	
		ASSERT(vm.isIdleVmOn == true, HWDSPH_ERR_FORCE_ASSERT, 0 );
		vm.isIdleVmOn = false;
   }

	if(VM_STATE_RECORD == vm.state)
   {
      vm.state = VM_STATE_RECORD_STOP;
      if(vm.debug_info & (VM_PCM_1ST_SET_RECORD_FLAG + VM_PCM_2ND_SET_RECORD_FLAG))  
      {    
         AM_PCM8K_RecordOff(false, AM_PCM8KREC_APP_TYPE_VMEPL);
         L1Audio_UnhookHisrHandler( DP_D2C_VMEPL_REC_INT );
         vm.pcmBuf = NULL; // free_ctrl_buffer(vm.pcmBuf);
         
      }
      L1Audio_UnhookHisrHandler(DP_D2C_VM_REC_INT); // vm record unhook
      // L1SP_UnRegister_Service();
   }else {
   	return;
	}

	AM_VMRecordOff();
	if( vm.vmBuf != NULL ){
      vm.vmBuf = NULL; // free_ctrl_buffer( vm.vmBuf );
      vm.vmBuf = NULL;
   }

   vm.state = VM_STATE_STOP;

	vm.debug_info = 0;
	// output buffer clean 
	vm.pOutputBufWrite = 0;
	vm.pOutputBufRead = 0;
	vm.outputBufSize = 0; 
	memset( vmRecOutputBuf, 0, VMREC_OUTPUT_BUF_SIZE*sizeof(uint16) );	


	// release sleep mode
   L1Audio_ClearFlag( vm.audId );
   L1Audio_FreeAudioID( vm.audId );
   
}

/*
void vmStop(void)
{
	
   if(vm.control & 0x02)
   {
      vm.state = VM_STATE_RECORD_STOP;
      if(vm.debug_info & (VM_PCM_1ST_SET_RECORD_FLAG + VM_PCM_2ND_SET_RECORD_FLAG))  
      {    
         AM_PCM8K_RecordOff(false, AM_PCM8KREC_APP_TYPE_VMEPL);
         L1Audio_UnhookHisrHandler( DP_D2C_VMEPL_REC_INT );
         vm.pcmBuf = NULL; free_ctrl_buffer(vm.pcmBuf);
         
      }
      L1Audio_UnhookHisrHandler(DP_D2C_VM_REC_INT);
      L1SP_UnRegister_Service();
   }   

   switch(vm.state) {
      case VM_STATE_RECORD_PAUSE:
         break;
      case VM_STATE_RECORD:
      case VM_STATE_RECORD_STOP:
         AM_VMRecordOff();
         *DP_SC_MUTE = 0x0000;
#if defined(__UMTS_RAT__) || defined( __UMTS_TDD128_MODE__ )
         if(SP3G_VM_Status())
            SP3G_Stop_VM();
#if defined(_EXTRA_LOG_FOR_BIT_TRUE_)
         g_bNeedExtraLog = KAL_FALSE;
#endif
#endif
         break;
      case VM_STATE_PLAY:
      case VM_STATE_PLAY_PAUSE:
         AM_VMPlaybackOff(vm.ctrl->param.vm);
         break;
      default:
         ASSERT(KAL_FALSE);
   }

   if( vm.vmBuf != NULL ){
      free_ctrl_buffer( vm.vmBuf );
      vm.vmBuf = NULL;
   }

   vm.state = VM_STATE_STOP;
}
*/
/*
void vmPause(mediaControl *ctrl)
{
   switch(vm.state) {
      case VM_STATE_RECORD:
         if((vm.debug_info & (VM_PCM_1ST_SET_RECORD_FLAG + VM_PCM_2ND_SET_RECORD_FLAG)) && AM_IsSpeechOn())               
            AM_PCM8K_RecordOff(false, AM_PCM8KREC_APP_TYPE_VMEPL);
         *DP_SC_MUTE = 0x0000;
#if defined(__UMTS_RAT__) || defined( __UMTS_TDD128_MODE__ )
         if(SP3G_VM_Status())
            SP3G_Stop_VM();
#if defined(_EXTRA_LOG_FOR_BIT_TRUE_)
         g_bNeedExtraLog = KAL_FALSE;
#endif
#endif
         AM_VMRecordOff();
         vm.state = VM_STATE_RECORD_PAUSE;
         break;
      case VM_STATE_PLAY:
         AM_VMPlaybackOff(vm.ctrl->param.vm);
         vm.state = VM_STATE_PLAY_PAUSE;
         break;
      default:
         ASSERT(KAL_FALSE);
   }

}

void vmResume(mediaControl *ctrl)
{
   switch(vm.state) {
      case VM_STATE_RECORD_PAUSE:
         if( AM_IsSpeechOn() ) {
            vm.control = 0x0003;
            if(vm.debug_info & (VM_PCM_1ST_SET_RECORD_FLAG + VM_PCM_2ND_SET_RECORD_FLAG))  
            {
               AM_PCM8K_RecordOn(AM_PCM8KREC_APP_TYPE_VMEPL);
            }
         }
         else {
            vm.control = 0x0001;
            *DP_SC_MUTE = 0x0002;
         }
#if defined(__UMTS_RAT__) || defined( __UMTS_TDD128_MODE__ )
         SP3G_Start_VM();
#if defined(_EXTRA_LOG_FOR_BIT_TRUE_)
         g_bNeedExtraLog = KAL_TRUE;
#endif
#endif
         AM_VMRecordOn( (uint16)vm.type );
         vm.state = VM_STATE_RECORD;
         break;
      case VM_STATE_PLAY_PAUSE:
         vm.state = VM_STATE_PLAY;
         vm.control = 0;
         vm_playback();
         break;
      default:
         ASSERT(KAL_FALSE);
   }
}
*/
/* ------------------------------------------------------------------------------ */
// #endif
