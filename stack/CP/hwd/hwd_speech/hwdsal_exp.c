/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

#include "hwdsal_impl.h"
#include "hwdsal_def.h"
#include "hwdsal_exp.h"
#include "monapi.h"
#include "monids.h"
#include "monvtst.h"
#include "hwdlinkapi.h"


// must to be configured right before sal app on. Normal, loudspk, and earphone can share the same device config for dsp, so switching among these 3 devices can discard the sal app reset.
void SAL_Set_Device(uint32 dev) 
{
	//ASSERT(dev < SAL_DEV_MAX_NUM);
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(21), dev, SAL_UNKNOWN, SAL_UNKNOWN, SAL_UNKNOWN);
#ifndef SALI_DISABLE
	SALI_Set_Device(dev);
#endif

}


void SAL_2G_Call_Open(uint32 enc_mod, uint32 dec_mod, uint32 sub_channel)
{
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(0), enc_mod, dec_mod, sub_channel, SAL_UNKNOWN);
	//AM_DSP_SetSRCCtrl(0, 0);//SAL, SALI_Config_SRC
#ifndef SALI_DISABLE
	SALI_Config_SRC(SAL_APP_TYPE_2GCall, IsWBLink(enc_mod));

	//AM_WriteSpeechParameters( 0x0633, am.speech_mode, am.speech_mode );//SAL, SALI_Enable_Codec
	/*   if(IsWBLink(enc_mod))
      *DP_AUDIO_PAR |= (VOICE_WB_AMR_FLAG); 
   else
      *DP_AUDIO_PAR &= ~(VOICE_WB_AMR_FLAG);        */
	SALI_Enable_Codec(enc_mod, dec_mod);

	//AM_FillSilencePattern( DSP_TX_TCH_S0_0, 2, am.speech_mode );  //wayne, for the next 2g call 2g mdm originally, now dsp sph and mdm has handshaking, no need anymore
	SALI_Fill_Enc_Silence(SAL_APP_TYPE_2GCall, enc_mod);

	SALI_Set_2G_Sch_Delay(enc_mod, sub_channel);	
    //AM_Write_Speech_Delay( am.speech_mode,  am.sub_channel, L1D_Get_Win_TQ_WRAP() - TQ_WRAP_COUNT );
    

   /* bit14 :the data of Speech DSP is avaible
      bit14 on  : Modem always sends the SpeechGood frame from Speech DSP, and at that time Driver fills the silence pattern. It causes MD send the silence pattern(good frame) to network
      bit14 off : Modem always sends the SpeechBad  frame into network. It will cause the better performance for continuity 
   */
   /*
   *E_CTRL_HO &= ~0x4000; 
   *E_CTRL_HO |= 0x0004;   // Enable ramp up in TCH
	*/
	
	SALI_AppSpec_Control(SAL_APP_TYPE_2GCall, 1);


   SALI_VBI_Reset();
#endif	  
}

void SAL_2G_Handover(uint32 enc_mod, uint32 dec_mod, uint32 sub_channel)
{
		//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(2), enc_mod, dec_mod, sub_channel, SAL_UNKNOWN);
#ifndef SALI_DISABLE		
		SALI_Config_SRC(SAL_APP_TYPE_2GCall, IsWBLink(enc_mod));
	
		SALI_Set_2G_Handover();
		
		SALI_Enable_Codec(enc_mod, dec_mod);

        SALI_Set_2G_Sch_Delay(enc_mod, sub_channel);

		//for vm logging druing 2g ho
		SALI_Fill_Enc_Silence(SAL_APP_TYPE_2GCall, enc_mod);//wayne, for vm logging se frame during ho
        SALI_VM_Upd_Enc_Used(enc_mod);//wayne, for vm logging old se codec during ho, use DP_Encoder_Used_Mode from mt6280
        //L1Audio_Msg_AM_Handover( enc_mod );
		#if defined(__MA_L1__)
        L1Audio_Msg_SPEECH_CODEC( L1SP_Speech_Codec_Mode(enc_mod), L1SP_Speech_Codec_Mode(enc_mod) );
		#endif

        SALI_VBI_Reset();
#endif
}

void SAL_3G_Call_Open(uint32 enc_mod, uint32 dec_mod, uint32 dtx, uint32 delR, uint32 delW, uint32 delM)
{
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(3), enc_mod, dec_mod, dtx, SAL_UNKNOWN);		 
	//AM_DSP_SetSRCCtrl(0, 0);//SAL, SAL_ConfigSRC(device, codec);
	
	SALI_Config_SRC(SAL_APP_TYPE_3GCall, IsWBLink(enc_mod));

	
	// *DP_3G_STATE = 1;//SAL, Dsp_Set3GMode(true);
	SALI_Set_3G(true);

	/*
	if( SP3G_IsDTXOn() )
            sc_flags = 0x637;
         else
            sc_flags = 0x633;
         AM_WriteSpeechParameters( sc_flags, am.speech_mode, am.speech_mode ); // DTX always on*/
	SALI_Enable_Codec(enc_mod, dec_mod);//does 3g also use these two sherrif to assgin codec?? difference between MT6280 and MT6583
	SALI_3G_SetDtx(dtx);
	
	//AM_FillSilencePattern( DP_3G_UL_TX_TYPE, 2, am.speech_mode );  
	SALI_Fill_Enc_Silence(SAL_APP_TYPE_3GCall, enc_mod);

	//AM_Write_Idle_Delay(6);	 
    SALI_Set_Sch_Delay(delR, delW, delM, 0, SAL_UNKNOWN);

	SALI_AppSpec_Control(SAL_APP_TYPE_3GCall, 1);

        
   	SALI_VBI_Reset();
	  
}

void SAL_4G_Call_Open_temp(uint32 enc_mod, uint32 dec_mod, uint32 dtx, uint32 delR, uint32 delW, uint32 delM)
{
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(3), enc_mod, dec_mod, dtx, SAL_UNKNOWN);		 
	//AM_DSP_SetSRCCtrl(0, 0);//SAL, SAL_ConfigSRC(device, codec);
	
	SALI_Config_SRC(SAL_APP_TYPE_4GCall, IsWBLink(enc_mod));

	
	// *DP_3G_STATE = 1;//SAL, Dsp_Set3GMode(true);
	SALI_Set_3G(true);

	/*
	if( SP3G_IsDTXOn() )
            sc_flags = 0x637;
         else
            sc_flags = 0x633;
         AM_WriteSpeechParameters( sc_flags, am.speech_mode, am.speech_mode ); // DTX always on*/
	SALI_Enable_Codec(enc_mod, dec_mod);//does 3g also use these two sherrif to assgin codec?? difference between MT6280 and MT6583
	SALI_3G_SetDtx(dtx);
	
	//AM_FillSilencePattern( DP_3G_UL_TX_TYPE, 2, am.speech_mode );  
	SALI_Fill_Enc_Silence(SAL_APP_TYPE_3GCall, enc_mod);

	//AM_Write_Idle_Delay(6);	 
    SALI_Set_Sch_Delay(delR, delW, delM, 0, SAL_UNKNOWN);

	SALI_AppSpec_Control(SAL_APP_TYPE_4GCall, 1);

        
   	SALI_VBI_Reset();
	  
}

void SAL_3G_SetDtx(bool on)
{
	SALI_3G_SetDtx(on);
}

void SAL_3G_Upd_Enc_Cod(uint32 enc_mod)
{
	SALI_Upd_3G_Enc_Rate(enc_mod);
}

void SAL_3G_Upd_Dec_Cod(uint32 dec_mod)
{	
	SALI_Upd_3G_Dec_Rate(dec_mod);
}

void SAL_3G_Set_TxType(uint32 tx_type)
{
	SALI_Set_3G_TxType(tx_type);
}

void SAL_3G_Set_RxType(uint32 rx_type)
{
	SALI_Set_3G_RxType(rx_type);
}

void SAL_2G_Call_Close(uint32 dsp_end)
{
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(1), dsp_end, SAL_UNKNOWN, SAL_UNKNOWN, SAL_UNKNOWN);
#ifndef SALI_DISABLE
	if(dsp_end)
	SALI_VBI_End();

	SALI_Disable_Codec();
   //*DP_SC_MUTE &= ~0x0002; wayne, come back, why??
   
 	SALI_AppSpec_Control(SAL_APP_TYPE_2GCall, 0);
#endif
}

void SAL_3G_Call_Close(uint32 dsp_end)
{
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(4), dsp_end, SAL_UNKNOWN, SAL_UNKNOWN, SAL_UNKNOWN);
	
	if(dsp_end)
	SALI_VBI_End();

	SALI_Disable_Codec();	
   
   //*DP_SC_MUTE &= ~0x0002;

   	SALI_Set_3G_RxType(SAL_3G_RX_NO_DATA);
	SALI_Set_3G(false);

	SALI_AppSpec_Control(SAL_APP_TYPE_3GCall, 0);

}

void SAL_4G_Call_Close_temp(uint32 dsp_end)
{
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(4), dsp_end, SAL_UNKNOWN, SAL_UNKNOWN, SAL_UNKNOWN);
	
	if(dsp_end)
	SALI_VBI_End();

	SALI_Disable_Codec();	
   
   //*DP_SC_MUTE &= ~0x0002;

   	SALI_Set_3G_RxType(SAL_3G_RX_NO_DATA);
	SALI_Set_3G(false);

	SALI_AppSpec_Control(SAL_APP_TYPE_4GCall, 0);

}

//fsju
void SAL_4G_G_Codec_Call_Close(uint32 dsp_end)
{
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(30), dsp_end, SAL_UNKNOWN, SAL_UNKNOWN, SAL_UNKNOWN);

	SALI_Disable_Update_Codec_Now();

	if(dsp_end)
	SALI_VBI_End();

	SALI_Disable_Codec();	
   
   //*DP_SC_MUTE &= ~0x0002;

   	SALI_Set_3G_RxType(SAL_3G_RX_NO_DATA);
	SALI_Set_3G(false);

	SALI_AppSpec_Control(SAL_APP_TYPE_3GCall, 0);

}

//fsju
void SAL_4G_G_Codec_Call_Open(uint32 enc_mod, uint32 dec_mod, uint32 dtx, uint32 delR, uint32 delW, uint32 delM)
{
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(29), enc_mod, dec_mod, dtx, SAL_UNKNOWN);		 
	//AM_DSP_SetSRCCtrl(0, 0);//SAL, SAL_ConfigSRC(device, codec);
	
	SALI_Config_SRC(SAL_APP_TYPE_3GCall, IsWBLink(enc_mod));

	
	// *DP_3G_STATE = 1;//SAL, Dsp_Set3GMode(true);
	SALI_Set_3G(true);

	/*
	if( SP3G_IsDTXOn() )
            sc_flags = 0x637;
         else
            sc_flags = 0x633;
         AM_WriteSpeechParameters( sc_flags, am.speech_mode, am.speech_mode ); // DTX always on*/
	SALI_Enable_Codec(enc_mod, dec_mod);//does 3g also use these two sherrif to assgin codec?? difference between MT6280 and MT6583
	SALI_3G_SetDtx(dtx);
	
	//AM_FillSilencePattern( DP_3G_UL_TX_TYPE, 2, am.speech_mode );  
	SALI_Fill_Enc_Silence(SAL_APP_TYPE_3GCall, enc_mod);

	//AM_Write_Idle_Delay(6);	 
    SALI_Set_Sch_Delay(delR, delW, delM, 0, SAL_UNKNOWN);

	SALI_AppSpec_Control(SAL_APP_TYPE_3GCall, 1);

        
   	SALI_VBI_Reset();

    //for G-serial codec, Just only disable AMR
	SALI_Disable_Codec();
	SALI_Enable_Update_Codec_Now();

}

bool SAL_3G_Mode()
{
	return SALI_Check_3G();
}

void SAL_App_Open(uint32 delR, uint32 delW, uint32 delM)
{
	//SALI_Set_Sch_Delay(delR, delW, delM, 0, 0);
	//SALI_VBI_Reset();
}

void SAL_App_Close(uint32 dsp_end)
{
	//if(dsp_end)
		//SALI_VBI_End();
}

void SAL_Bgsnd_SetInit()
{
#ifndef SALI_DISABLE
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(5), 1, SAL_UNKNOWN, SAL_UNKNOWN, SAL_UNKNOWN);
	SALI_Bgsnd_Switch(true);
#endif
}

bool SAL_Bgsnd_IsRunning()
{
#ifndef SALI_DISABLE
	return SALI_Bgsnd_IsRunning();
#endif
}

bool SAL_Bgsnd_IsIdle()
{
#ifndef SALI_DISABLE
	return SALI_Bgsnd_IsOFF();
#endif
}

void SAL_Bgsnd_SetFinal()
{
#ifndef SALI_DISABLE
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(5), 0, SAL_UNKNOWN, SAL_UNKNOWN, SAL_UNKNOWN);
	SALI_Bgsnd_Switch(false);
#endif
}

void SAL_Bgsnd_Config(uint32 ulgain, uint32 dlgain, uint32 ulmix, uint32 dlmix)
{
#ifndef SALI_DISABLE
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(6), ulgain, dlgain, ulmix, dlmix);
	SALI_Bgsnd_Config(ulgain, dlgain, ulmix, dlmix);
#endif
}

volatile uint16* SAL_Bgsnd_GetBuf_DL()
{
#ifndef SALI_DISABLE
	return SALI_Bgsnd_GetBuf(1);
#endif
}

uint32 SAL_Bgsnd_GetDataLen_DL()
{
#ifndef SALI_DISABLE
	return SALI_Bgsnd_GetDataLen(1);
#endif
}

volatile uint16* SAL_Bgsnd_GetBuf_UL()
{
#ifndef SALI_DISABLE
	return SALI_Bgsnd_GetBuf(0);
#endif
}

uint32 SAL_Bgsnd_GetDataLen_UL()
{
#ifndef SALI_DISABLE
	return SALI_Bgsnd_GetDataLen(0);
#endif
}

void SAL_Dsp_Sph_Init()
{
#ifndef SALI_DISABLE
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(19), SAL_UNKNOWN, SAL_UNKNOWN, SAL_UNKNOWN, SAL_UNKNOWN);
	SALI_Init();
//	SALI_Fill_Enc_Silence(SAL_APP_TYPE_2GCall, enc_mod);
#endif
}

uint16 tmp_SAL_Sidetone_GetFltCoefAddr[SAL_FLTCOEFLEN_SIDETONE];
volatile uint16* SAL_Sidetone_GetFltCoefAddr()
{
#ifndef SALI_DISABLE
	return SALI_GetFltCoefAddr(SALI_FLTCOEF_TYPE_SIDETONE);
#else
	return tmp_SAL_Sidetone_GetFltCoefAddr;
#endif
}

uint16 tmp_SAL_Notch_GetFltCoefAddr[SAL_PARAMETERLEN_NOTCH];
volatile uint16* SAL_Notch_GetFltCoefAddr()
{
#ifndef SALI_DISABLE
	return SALI_GetFltCoefAddr(SALI_FLTCOEF_TYPE_NOTCH);
#else
   return tmp_SAL_Notch_GetFltCoefAddr;	
#endif
}

uint16 SAL_Sidetone_GetAdaptiveGain()
{
#ifndef SALI_DISABLE
	return SALI_Sidetone_GetAdaptiveGain();
#endif
}

void SAL_Sidetone_SetGain(uint16 val)
{
#ifndef SALI_DISABLE
	SALI_Sidetone_SetGain(val);
#endif
}

void SAL_Sidetone_Enable(bool ena)
{
#ifndef SALI_DISABLE
	SALI_Sidetone_Enable(ena);
#endif
}

uint16 SAL_DGain_Get_DL()
{
#ifndef SALI_DISABLE
	return SALI_DGain_Get(1);
#endif
}

void SAL_DGain_Set_DL(uint16 val)
{
#ifndef SALI_DISABLE
	SALI_DGain_Set(1, val);
#endif
}

uint16 SAL_DGain_Get_UL()
{
#ifndef SALI_DISABLE
	return SALI_DGain_Get(0);
#endif
}

void SAL_DGain_Set_UL(uint16 val)
{
#ifndef SALI_DISABLE
	SALI_DGain_Set(0, val);
#endif
}

uint16 tmp_SAL_BKF_GetFltCoefAddr_NB_UL[SAL_FLTCOEFLEN_BKF_NB];
volatile uint16* SAL_BKF_GetFltCoefAddr_NB_UL()
{
#ifndef SALI_DISABLE
	volatile uint16 *addr;
	addr = SALI_GetFltCoefAddr(SALI_FLTCOEF_TYPE_BKF_NB_UL);
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(10), ((int)addr)>>16, (int)addr, SAL_UNKNOWN, SAL_UNKNOWN);
	return addr;
#else
   return tmp_SAL_BKF_GetFltCoefAddr_NB_UL;	
#endif
}

uint16 tmp_SAL_BKF_GetFltCoefAddr_NB_DL[SAL_FLTCOEFLEN_BKF_NB];
volatile uint16* SAL_BKF_GetFltCoefAddr_NB_DL()
{
#ifndef SALI_DISABLE
	volatile uint16 *addr;
	addr = SALI_GetFltCoefAddr(SALI_FLTCOEF_TYPE_BKF_NB_DL);
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(10), SAL_UNKNOWN, ((int)addr)>>16, (int)addr, SAL_UNKNOWN);
	return addr;
#else
   return tmp_SAL_BKF_GetFltCoefAddr_NB_DL;	
#endif
}
uint16 tmp_BKF_GetFltCoefAddr_WB_UL[SAL_FLTCOEFLEN_BKF_WB];
volatile uint16* SAL_BKF_GetFltCoefAddr_WB_UL()
{
#ifndef SALI_DISABLE
	volatile uint16 *addr;
	addr = SALI_GetFltCoefAddr(SALI_FLTCOEF_TYPE_BKF_WB_UL);
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(10), SAL_UNKNOWN, SAL_UNKNOWN, ((int)addr)>>16, (int)addr);
	return addr;
#else
   return tmp_BKF_GetFltCoefAddr_WB_UL;	
#endif
}

uint16 tmp_SAL_BKF_GetFltCoefAddr_WB_DL[SAL_FLTCOEFLEN_BKF_WB];
volatile uint16* SAL_BKF_GetFltCoefAddr_WB_DL()
{
#ifndef SALI_DISABLE
	volatile uint16 *addr;
	addr = SALI_GetFltCoefAddr(SALI_FLTCOEF_TYPE_BKF_WB_DL);
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(10), (int)addr, SAL_UNKNOWN, SAL_UNKNOWN, ((int)addr)>>16);
	return addr;
#else
   return tmp_SAL_BKF_GetFltCoefAddr_WB_DL;	
#endif
}

volatile uint16* SAL_FS_GetFltCoefAddr()
{
	volatile uint16 *addr;
	addr = SALI_GetFltCoefAddr(SALI_FLTCOEF_TYPE_FS);
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(36), ((int)addr)>>16, (int)addr, SAL_UNKNOWN, SAL_UNKNOWN);
	return addr;
}

void SAL_BKF_Switch(uint32 ul, uint32 dl)
{
#ifndef SALI_DISABLE
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(7), ul, dl, SAL_UNKNOWN, SAL_UNKNOWN);
	SALI_BKF_Switch(ul, dl);
#endif
}
uint16 tmp_SAL_SRC_GetFltCoefAddr[SAL_FLTCOEFLEN_SRC];
volatile uint16* SAL_SRC_GetFltCoefAddr()
{
#ifndef SALI_DISABLE
	return SALI_GetFltCoefAddr(SALI_FLTCOEF_TYPE_SRC);
#else
   return tmp_SAL_SRC_GetFltCoefAddr;	
#endif
}

uint16 tmp_SAL_AGC_GetFltCoefAddr[SAL_FLTCOEFLEN_AGC];
volatile uint16* SAL_AGC_GetFltCoefAddr()
{
#ifndef SALI_DISABLE
	return SALI_GetFltCoefAddr(SALI_FLTCOEF_TYPE_AGC);
#else
	return tmp_SAL_AGC_GetFltCoefAddr;
#endif
}

void SAL_ENH_SetULPath(bool on)
{
#ifndef SALI_DISABLE
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(8), 0, on, SAL_UNKNOWN, SAL_UNKNOWN);
	SALI_ENH_PathEnable(0, on);
#endif
}

void SAL_ENH_SetDLPath(bool on)
{
#ifndef SALI_DISABLE
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(8), 1, on, SAL_UNKNOWN, SAL_UNKNOWN);
	SALI_ENH_PathEnable(1, on);
#endif
}


void SAL_ENH_SetInit(uint32 enh_mod)
{
#ifndef SALI_DISABLE
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(9), enh_mod, 1, SAL_UNKNOWN, SAL_UNKNOWN);
	SALI_ENH_Switch(enh_mod, true);
#endif
}

void SAL_ENH_SetOff(uint32 enh_mod)
{
#ifndef SALI_DISABLE
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(9), enh_mod, 0, SAL_UNKNOWN, SAL_UNKNOWN);
	SALI_ENH_Switch(enh_mod, false);
#endif
}

bool SAL_ENH_IsReady(uint32 enh_mod)
{
#ifndef SALI_DISABLE
	return SALI_ENH_IsReady(enh_mod);
#else
	return true;
#endif
}

bool SAL_ENH_IsIdle(uint32 enh_mod)
{
#ifndef SALI_DISABLE
	return SALI_ENH_IsIdle(enh_mod);
#else
	return true;
#endif
}

void SAL_ENH_Dynamic_Ctrl(bool on, Sal_Enh_Dynamic_t fea)
{
#ifndef SALI_DISABLE
	SALI_ENH_Dynamic_Ctrl(on, fea);
#endif
}

void SAL_ENH_Gain_Set_DL(uint16 val)
{
#ifndef SALI_DISABLE
	SALI_ENH_Gain_Set_DL(val);
#endif
}

void SAL_ENH_Dynamic_State_Par_Init(void)
{
	SALI_ENH_Dynamic_State_Par_Init();
	MonTrace (MON_CP_SPH_SAL_ENH_DYNAMIC_STATE_PAR_INIT, 1, *DSP_SPH_ENH_DYNAMIC_STATE);
}

void SAL_ENH_Dynamic_State_Set(Sal_Enh_Dyn_Sta_t sta)
{
	SALI_ENH_Dynamic_State_Set(sta);
}

bool SAL_ENH_Dynamic_State_Check(Sal_Enh_Dyn_Sta_t sta)
{
	return SALI_ENH_Dynamic_State_Check(sta);
}

void SAL_ENH_Flag_Par_Set(Sal_Enh_Flag_Par_t par)
{
	SALI_ENH_Flag_Par_Set(par);
}


uint16 tmp_SAL_CommonPar_GetBuf[SAL_PARAMETERLEN_COMMON];
volatile uint16* SAL_CommonPar_GetBuf()
{
#ifndef SALI_DISABLE
	volatile uint16 *addr;
	addr = SALI_Parameter_GetAddr(SALI_PARAMETER_TYPE_COMMON);
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(12), ((int)addr)>>16, (int)addr, SAL_UNKNOWN, SAL_UNKNOWN);
	return addr;
#else
   return tmp_SAL_CommonPar_GetBuf;	
#endif
}
uint16 tmp_SAL_ModePar_GetBuf_NB[SAL_PARAMETERLEN_MODE_NB];
volatile uint16* SAL_ModePar_GetBuf_NB()
{
#ifndef SALI_DISABLE
	volatile uint16 *addr;
	addr = SALI_Parameter_GetAddr(SALI_PARAMETER_TYPE_MODE_NB);
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(11), ((int)addr)>>16, (int)addr, SAL_UNKNOWN, SAL_UNKNOWN);
	return addr;
#else
	return tmp_SAL_ModePar_GetBuf_NB;
#endif
}


volatile uint16* SAL_ModePar_GetBuf2_NB()
{
	volatile uint16 *addr;
	addr = SALI_Parameter_GetAddr(SALI_PARAMETER_TYPE_MODE2_NB);
	MonTrace (MON_CP_SPH_SAL_SPH_PAR_MODE2_NB_ADDR, 1, addr);
	return addr;
}

volatile uint16* SAL_ModePar_GetBuf3_NB()
{
	volatile uint16 *addr;
	addr = SALI_Parameter_GetAddr(SALI_PARAMETER_TYPE_MODE3_NB);
	MonTrace (MON_CP_SPH_SAL_SPH_PAR_MODE3_NB_ADDR, 1, addr);
	return addr;
}

uint16 tmp_SAL_ModePar_GetBuf_WB[SAL_PARAMETERLEN_MODE_WB];
volatile uint16* SAL_ModePar_GetBuf_WB()
{
#ifndef SALI_DISABLE
	volatile uint16 *addr;
	addr = SALI_Parameter_GetAddr(SALI_PARAMETER_TYPE_MODE_WB);
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(11), SAL_UNKNOWN, SAL_UNKNOWN, ((int)addr)>>16, (int)addr);
	return addr;
#else
   return tmp_SAL_ModePar_GetBuf_WB;
#endif
}

volatile uint16* SAL_ModePar_GetBuf2_WB()
{
	volatile uint16 *addr;
	addr = SALI_Parameter_GetAddr(SALI_PARAMETER_TYPE_MODE2_WB);
	MonTrace (MON_CP_SPH_SAL_SPH_PAR_MODE2_WB_ADDR, 1, addr);
	return addr;
}

volatile uint16* SAL_ModePar_GetBuf3_WB()
{
	volatile uint16 *addr;
	addr = SALI_Parameter_GetAddr(SALI_PARAMETER_TYPE_MODE3_WB);
	MonTrace (MON_CP_SPH_SAL_SPH_PAR_MODE3_WB_ADDR, 1, addr);
	return addr;
}

uint16 tmp_SAL_DMNR_GetFltCoefAddr_NB[SAL_FLTCOEFLEN_DMNR_NB];
volatile uint16* SAL_DMNR_GetFltCoefAddr_NB()
{
#ifndef SALI_DISABLE
	volatile uint16 *addr;
	addr = SALI_GetFltCoefAddr(SALI_FLTCOEF_TYPE_DMNR_NB);
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(13), ((int)addr)>>16, (int)addr, SAL_UNKNOWN, SAL_UNKNOWN);
	return addr;
#else
	return tmp_SAL_DMNR_GetFltCoefAddr_NB;
#endif
}

uint16 tmp_SAL_DMNR_GetFltCoefAddr_WB[SAL_FLTCOEFLEN_DMNR_WB];
volatile uint16* SAL_DMNR_GetFltCoefAddr_WB()
{
#ifndef SALI_DISABLE
	volatile uint16 *addr;
	addr = SALI_GetFltCoefAddr(SALI_FLTCOEF_TYPE_DMNR_WB);
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(13), SAL_UNKNOWN, SAL_UNKNOWN, ((int)addr)>>16, (int)addr);
	return addr;
#else
   return tmp_SAL_DMNR_GetFltCoefAddr_WB;	
#endif
}

volatile uint16* SAL_DMNR_LSPK_GetFltCoefAddr_NB()
{
	volatile uint16 *addr;
	//bool bLSPK;
	//bLSPK = true;
	
	addr = SALI_GetFltCoefAddr(SALI_FLTCOEF_TYPE_DMNR_LSPK_NB);
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(13), ((int)addr)>>16, (int)addr, bLSPK, SAL_UNKNOWN);
	return addr;
}


volatile uint16* SAL_DMNR_LSPK_GetFltCoefAddr_WB()
{
	volatile uint16 *addr;
	//bool bLSPK;
	//bLSPK = false;	

	addr = SALI_GetFltCoefAddr(SALI_FLTCOEF_TYPE_DMNR_LSPK_WB);
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(13), ((int)addr)>>16, (int)addr, bLSPK, SAL_UNKNOWN);
	return addr;
}

volatile uint16* SAL_ECHO_REF_SWBT_GetAddr()
{
	volatile uint16 *addr;
	
	addr = SALI_GetFltCoefAddr(SALI_FLTCOEF_TYPE_ECHO_REF_SW_BT);
	return addr;
}

uint16 SAL_3G_GetValue(uint16 type)
{
	return SALI_3G_GetValue(type);
}

volatile uint16* SAL_3G_GetAddr(uint16 type)
{
	return SALI_3G_GetAddr(type);
}

bool SAL_3G_IsULReady()
{
	return SALI_3G_CheckDataSync(0);
}

void SAL_3G_SetULEmpty()
{
	SALI_3G_SetDataStatus(0, 0);
}

bool SAL_3G_IsDLEmpty()
{
	return SALI_3G_CheckDataSync(1);
}

void SAL_3G_SetDLReady()
{
	SALI_3G_SetDataStatus(1, 1);
}


bool SAL_DSPINT_Resolve(uint16 iid, uint32* sph_int)
{
#ifndef SALI_DISABLE
	return SALI_Int_Resolve(iid, sph_int);
#else
	return true;
#endif

}

void SAL_PcmEx_Config(Sal_PCMEx_Config_t* cfg)
{
#ifndef SALI_DISABLE
	//ASSERT_REBOOT(cfg != 0);
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(14), cfg->swi, cfg->type, cfg->idle, cfg->band);
	SALI_PcmEx_Config(cfg);
#endif
}

void SAL_PcmEx_SetStateUL(Sal_PcmEx_Type_t type, Sal_PcmEx_State_t sta)
{
#ifndef SALI_DISABLE
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(15), 0, type, sta, SAL_UNKNOWN);
	SALI_PcmEx_SetState(0, type, sta);
#endif
}

void SAL_PcmEx_SetStateDL(Sal_PcmEx_Type_t type, Sal_PcmEx_State_t sta)
{
#ifndef SALI_DISABLE
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(15), 1, type, sta, SAL_UNKNOWN);
	SALI_PcmEx_SetState(1, type, sta);
#endif
}

bool SAL_PcmEx_CheckStateUL(Sal_PcmEx_Type_t type, Sal_PcmEx_State_t sta)
{
#ifndef SALI_DISABLE
	return SALI_PcmEx_CheckState(0, type, sta);
#else
	return true;
#endif
}

bool SAL_PcmEx_CheckStateDL(Sal_PcmEx_Type_t type, Sal_PcmEx_State_t sta)
{
#ifndef SALI_DISABLE
	return SALI_PcmEx_CheckState(1, type, sta);
#else
	return true;
#endif
}

bool SAL_PcmEx_IsCtrlClean(Sal_PcmEx_Type_t type)
{
#ifndef SALI_DISABLE
	return SALI_PcmEx_IsCtrlClean(type);
#else
	return true;
#endif
}

void SAL_PcmEx_SetCtrlClean(Sal_PcmEx_Type_t type)//dangerous operation
{
	//SALI_PcmEx_SetCtrlClean(type);
}

volatile uint16* SAL_PcmEx_GetBuf(Sal_PcmEx_BufId_t id)
{
#ifndef SALI_DISABLE
	return SALI_PcmEx_GetBuf(id);
#endif
}

uint16 SAL_PcmEx_GetBufLen(Sal_PcmEx_BufId_t id)
{
#ifndef SALI_DISABLE
	return SALI_PcmEx_GetBufLen(id);
#endif
}

uint16 SAL_AGC_GetSWGain(uint8 mic) // 0 -> mic1, 1 -> mic2
{
#ifndef SALI_DISABLE
	return SALI_AGC_GetSWGain(mic);
#endif
}

void SAL_AGC_SetGain(uint16 val)
{
#ifndef SALI_DISABLE
	SALI_AGC_SetGain(val);
#endif
}

void SAL_VM_Config(Sal_VM_Config_t *cfg)
{
#ifndef SALI_DISABLE
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(20), cfg->swi, cfg->idle, cfg->codec, SAL_UNKNOWN);
	SALI_VM_Config(cfg);
#endif
}


void SAL_VM_GetFrame2G(Sal_VM_Frame *vmfrm)
{
#ifndef SALI_DISABLE
	//ASSERT_REBOOT(vmfrm != 0);
	SALI_VM_GetFrame(0, vmfrm);
#endif
}

void SAL_VM_GetFrame3G(Sal_VM_Frame *vmfrm)
{
	//ASSERT_REBOOT(vmfrm != 0);
	SALI_VM_GetFrame(1, vmfrm);
}

void SAL_VM_GetFrameC2K(Sal_VM_Frame *vmfrm)
{
	//ASSERT_REBOOT(vmfrm != 0);
	SALI_VM_GetFrame(2, vmfrm);
}

void SAL_EPL_GetFrame(Sal_EPL_Frame *eplfrm)
{
#ifndef SALI_DISABLE
	//ASSERT_REBOOT(eplfrm != 0);
	SALI_EPL_GetFrame(eplfrm);
#endif
}

void SAL_LBK_Codec(bool on)
{
#ifndef SALI_DISABLE
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(16), on, SAL_UNKNOWN, SAL_UNKNOWN, SAL_UNKNOWN);
	SALI_LBK_Codec(on);
#endif
}

void SAL_BT_Config(Sal_BT_Config *cfg)
{
#ifndef SALI_DISABLE
	//ASSERT_REBOOT(cfg != 0);
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(17), cfg->feed_cfg, cfg->mode, cfg->linear_ctrl, (cfg->linear_ul_gain<<8)+cfg->linear_reverse);
	SALI_BT_Config(cfg);
#endif
}

//return if DSP is being in bt-mode
bool SAL_BT_IsEnable()
{
#ifndef SALI_DISABLE
	return SALI_BT_IsEnable();
#else
	return false;
#endif
}

void SAL_Mute_Ctrl(Sal_Mute_Point_t ctrl, bool mute)
{
#ifndef SALI_DISABLE
	SALI_Mute_Ctrl(ctrl, mute);
#endif
}

//true means the dsp point is muted
bool SAL_Mute_Check(Sal_Mute_Point_t ctrl)
{
#ifndef SALI_DISABLE
	return SALI_Mute_Check(ctrl);
#else
	return false;
#endif
}

void SAL_CTM_Switch(bool on)
{
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(18), on, SAL_UNKNOWN, SAL_UNKNOWN, SAL_UNKNOWN);
	SALI_CTM_Switch(on);
}

void SAL_CTM_SetGain(uint16 limit, uint16 update)
{
	SALI_CTM_SetGain(limit, update);
}

uint16 SAL_CTM_GetValue(uint16 type)
{
	return SALI_CTM_GetValue(type);
}

bool SAL_CTM_IsIdle()
{
	return SALI_CTM_IsIdle();
}

bool SAL_CTM_IsOff()
{
	return SALI_CTM_IsOff();
}

void SAL_AGC1_Config(Sal_AGC_Config_t *cfg)
{
#ifndef SALI_DISABLE
	//ASSERT_REBOOT(cfg != 0);
	SALI_AGC1_Config(cfg);
#endif
}

void SAL_AGC2_Config(Sal_AGC_Config_t *cfg)
{
#ifndef SALI_DISABLE
	//ASSERT_REBOOT(cfg != 0);
	SALI_AGC2_Config(cfg);
#endif
}

void SAL_AGC3_Config(Sal_AGC_Config_t *cfg)
{
#ifndef SALI_DISABLE
	//ASSERT_REBOOT(cfg != 0);
	SALI_AGC3_Config(cfg);
#endif
}

void SAL_AGC4_Config(Sal_AGC_Config_t *cfg)
{
#ifndef SALI_DISABLE
	//ASSERT_REBOOT(cfg != 0);
	SALI_AGC4_Config(cfg);
#endif
}

void SAL_NotchFilter_Enable(bool ena, bool thirdStageIIR)
{
#ifndef SALI_DISABLE
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(22), ena, thirdStageIIR, SAL_UNKNOWN, SAL_UNKNOWN);
	SALI_NotchFilter_Enable(ena, thirdStageIIR);
#endif
}
void SAL_8K_Resync(bool ul, int16 ul_offset, bool dl, int16 dl_offset)
{
#ifndef SALI_DISABLE
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(23), ul, ul_offset, dl, dl_offset);
	SALI_8K_Resync(ul, ul_offset, dl, dl_offset);
#endif
}

void SAL_GetWarnMsg(uint16 *ulcode, uint16 *dlcode)
{
#ifndef SALI_DISABLE
	SALI_GetWarnMsg(ulcode, dlcode);
#endif
}

void SAL_2G_SMR_Switch(bool on)
{
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(25), on, SAL_UNKNOWN, SAL_UNKNOWN, SAL_UNKNOWN);
	SALI_2G_SMR_Switch(on);
}

void SAL_VOLTE_SetInfo(Sal_VOLTE_info_t info, uint16 val)
{
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(24), info, val, SAL_UNKNOWN, SAL_UNKNOWN);
	SALI_VOLTE_SetInfo(info, val);
}

uint16 tmp_SAL_ENH_GetInternalParAddr[200];
volatile uint16* SAL_ENH_GetInternalParAddr()
{
#ifndef SALI_DISABLE
	volatile uint16 *addr;
	addr = SALI_Parameter_GetAddr(SALI_PARAMETER_TYPE_MODE_INTERNAL);
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(26), ((int)addr)>>16, (int)addr, SAL_UNKNOWN, SAL_UNKNOWN);
	return addr;
#else
	return tmp_SAL_ENH_GetInternalParAddr;
#endif
}

void SAL_3G_SetULUnsync()
{
	SALI_3G_SetULUnsync();
}

void SAL_PCM_Router_Open(Sal_PCM_Router_t *pcmrt)
{
	uint16 value1 = 0;
	uint16 value2 = 0;
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(27), pcmrt->dev_band, pcmrt->cod_band, SAL_UNKNOWN, SAL_UNKNOWN);
	value1 |= SAL_BIT0;       //enable PCM router
	if(0 == pcmrt->dev_band) // NB
	{
		value1 |= SAL_BIT1;     // NB: (value |= SAL_BIT1), WB: (value &= ~SAL_BIT1)
	}
	
	//if((0 == pcmrt->dev_band) && (1 == pcmrt->cod_band))
	//{
	//	value2 |= (SPH_SFE_SRC_PCM_RT_SE28K_DS + SPH_SFE_SRC_PCM_RT_8K2SD_US);
	//}
	//else if((1 == pcmrt->dev_band) && (0 == pcmrt->cod_band))
	//{
	//	value2 |= (SPH_SFE_SRC_PCM_RT_SE28K_US + SPH_SFE_SRC_PCM_RT_8K2SD_DS);
	//}	
	
	if(1 == pcmrt->cod_band)
	{
		*DSP_SPH_8K_CTRL |= SAL_BIT3;
	}
	SALI_Set_Sch_Delay(pcmrt->delR, pcmrt->delW, pcmrt->delM_DL, 0, pcmrt->delM_UL);
	*DSP_SPH_PCM_ROUTER_CTRL = value1;
	*DSP_SPH_SFE_CTRL |= value2;
	SALI_Config_SRC(SAL_APP_TYPE_PCM_Router, pcmrt->cod_band == SAL_PCM_WIDEBAND);
	SALI_AppSpec_Control(SAL_APP_TYPE_PCM_Router, 1);
	SALI_VBI_Reset();
}

void SAL_PCM_Router_Close(uint32 dsp_end)
{
	uint16 value = 0;
	
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(28), SAL_UNKNOWN, SAL_UNKNOWN, SAL_UNKNOWN, SAL_UNKNOWN);
	*DSP_SPH_PCM_ROUTER_CTRL = value;
	if(dsp_end)
	SALI_VBI_End();
	SALI_AppSpec_Control(SAL_APP_TYPE_PCM_Router, 0);
}


void SAL_C2K_Call_Open(uint16 u2SO, bool bDtx, uint16 u2DelR, uint16 u2DelW, uint16 u2DelM)
{
//	#define overflow_ctrl_addr ((volatile uint16*)0x91CA0028)
//	*overflow_ctrl_addr = 1;
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(3), enc_mod, dec_mod, dtx, SAL_UNKNOWN);
	MonTrace (MON_CP_SPH_SAL_CALL_OPEN, 5, u2SO, bDtx, u2DelR, u2DelW, u2DelM);
#ifndef SALI_DISABLE
	SALI_Config_SRC(SAL_APP_TYPE_C2KCall, SALI_C2K_IsSOWB(u2SO));
	//u2SO = SALI_SO_DSP_MAP(u2SO);
	SALI_Enable_Codec(u2SO, u2SO);
	SALI_C2K_SetDtx(bDtx);
	SALI_Set_Sch_Delay(u2DelR, u2DelW, u2DelM, 0, SAL_UNKNOWN);
	SALI_AppSpec_Control(SAL_APP_TYPE_C2KCall, 1);
	SALI_VBI_Reset();	
#endif
} 

void SAL_C2K_Call_Close(bool bDsp_end)
{
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(4), dsp_end, SAL_UNKNOWN, SAL_UNKNOWN, SAL_UNKNOWN);
	MonTrace (MON_CP_SPH_SAL_CALL_CLOSE, 1, bDsp_end);
#ifndef SALI_DISABLE
	if(bDsp_end)
	{
		SALI_VBI_End();
	}
	SALI_Disable_Codec();
	SALI_AppSpec_Control(SAL_APP_TYPE_C2KCall, 0);
#endif
}

bool SAL_C2K_IsULReady()
{
	return SALI_C2K_CheckDataSync(0);
}

void SAL_C2K_SetULEmpty()
{
	SALI_C2K_SetDataStatus(0, 0);
}

bool SAL_C2K_IsDLEmpty()
{
	return SALI_C2K_CheckDataSync(1);
}

void SAL_C2K_SetDLReady()
{
	SALI_C2K_SetDataStatus(1, 1);
}

void SAL_C2K_SetULUnsync()
{
	SALI_C2K_SetULUnsync();
}

void SAL_C2K_SetValue(uint16 u2ValId, uint16 u2Val)
{
	MonTrace (MON_CP_SPH_SAL_SET_VALUE, 2, u2ValId, u2Val);
#ifndef SALI_DISABLE
	SALI_C2K_SetValue(u2ValId, u2Val);
#endif
}

void SAL_C2K_TTY_Switch(bool bOn)
{
	MonTrace (MON_CP_SPH_SAL_TTY_SWITCH, 1, bOn);
	SALI_C2K_TTY_Switch(bOn);
}

void SAL_C2K_SetTTYStatus(bool bUL, bool bDL)
{
	MonTrace (MON_CP_SPH_SAL_SET_TTY_STATUS_UL, 1, bUL);
	MonTrace (MON_CP_SPH_SAL_SET_TTY_STATUS_DL, 1, bDL);
	SALI_C2K_SetTTYStatus(bUL, bDL);
}

void SAL_C2K_COD_Feature_Switch(uint16 u2CodFea, bool bOn)
{
	MonTrace (MON_CP_SPH_SAL_FEATURE_SWITCH, 2, u2CodFea, bOn);
	SALI_C2K_COD_Feature_Switch(u2CodFea, bOn);
}

volatile uint16* SAL_C2K_GetAddr(uint16 type)
{
  MonTrace (MON_CP_SPH_SAL_GET_ADDR, 1, SALI_C2K_GetAddr(type));
	return SALI_C2K_GetAddr(type);
}

uint16 SAL_C2K_GetValue(uint16 type)
{
	MonTrace (MON_CP_SPH_SAL_GET_VALUE, 1, SALI_C2K_GetValue(type));
	return SALI_C2K_GetValue(type);
}

uint16 tmp_SAL_C2K_SetCodState;
void SAL_C2K_SetCodState(uint16 u2Sta)
{
	MonTrace (MON_CP_SPH_SAL_SET_COD_STATE, 1, u2Sta); 
	
#ifndef SALI_DISABLE
	SALI_C2K_SetCodState(u2Sta);
#else	
   if(1==u2Sta){
      tmp_SAL_C2K_SetCodState = 2;
   }else
   if(3==u2Sta){
      tmp_SAL_C2K_SetCodState = 0;
   }      
#endif
}

uint16 SAL_C2K_GetCodState()
{
#ifndef SALI_DISABLE
	return SALI_C2K_GetCodState();
#else
   return tmp_SAL_C2K_SetCodState;	
#endif
}

void SAL_SCH_Feature_Switch(uint16 u2SchFea, bool bOn)
{
	MonTrace (MON_CP_SPH_SAL_FEATURE_SWITCH, 2, u2SchFea, bOn);
}


void SAL_OpenDSP_Ctrl(bool on)
{
	//L1Audio_Msg_SAL_PROC(SAL_PROC_NAME(37), on, SAL_UNKNOWN, SAL_UNKNOWN, SAL_UNKNOWN);
	SALI_OpenDSP_Ctrl(on);
}
void SAL_OpenDSP_State_Switch(bool on)
{
	SALI_OpenDSP_State_Switch(on);
}

bool SAL_OpenDSP_IsRunning()
{
	return SALI_OpenDSP_IsRunning();
}

bool SAL_OpenDSP_IsOFF()
{
	return SALI_OpenDSP_IsOFF();
}
void SAL_OpenDSP_Handshake_MD(bool UL, bool DL)
{
	SALI_OpenDSP_Handshake_MD(UL, DL);
}

void SAL_OpenDSP_Timeout_Thres(void)
{
	SALI_OpenDSP_Timeout_Thres();
}

void SAL_OpenDSP_NXP_Echo_Ref_Switch(bool on)
{
	SALI_OpenDSP_NXP_Echo_Ref_Switch(on);
}
