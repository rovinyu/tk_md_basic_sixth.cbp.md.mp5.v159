/*******************************************************************************
*  Modification Notice:
*  --------------------------
*  This software is modified by MediaTek Inc. and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/

/*******************************************************************************
 *
 * Filename:
 * ---------
 * hwdtxdfe.h
 *
 * Project:
 * --------
 *   MTK OrionC/OrionPlus Project
 *
 * Description:
 * ------------
 *   This file contains the DDPC Loop Related Functions written in C.
 *
 * Author:
 * -------
 * 
 *
 *==============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 * $Revision:$
 * $Modtime:$
 * $Log:$
 *
 *
 *
 *
 *
 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *==============================================================================
 ******************************************************************************/
#ifndef HWDTXDFE_H
#define HWDTXDFE_H

#include "hwddefs.h"
#include "hwdapi.h"
#include "hwdrfapi.h"
#ifdef MTK_PLT_DENALI
#include "hwdtxdfedenali.h"
#endif

#if (SYS_BOARD >= SB_JADE)
#include "hwdtxdfejade.h"
#endif


/*****************************************************************************
  STRUCT/ENUM DEFINES:
*****************************************************************************/


/*****************************************************************************
  MACRO DEFINES:
*****************************************************************************/

/*****************************************************************************
  GLOBAL FUNCTION DEFINES:
*****************************************************************************/
int16 HwdTxDfeTxDcOrigResultTranToFinlResult(int32 dc_orig_result);
int16 HwdTxDfeDetDcOrigResultTranToFinlResult(int32 dc_orig_result);
int16 HwdTxDfeTxIqGainOrigResultTranToFinlResult(int32 iq_gain_orig_result);
int16 HwdTxDfeTxIqPhaseOrigResultTranToFinlResult(int32 iq_phase_orig_result);
int16 HwdTxDfeDetIqGainOrigResultTranToFinlResult(int32 iq_gain_orig_result);
int16 HwdTxDfeDetIqPhaseOrigResultTranToFinlResult(int32 iq_phase_orig_result);
void HwdTxAgcCfgDfeSlot(TxMode ddpcMode, HwdRfTxAgcParamT *rfTxAgcParamP, int16 targetGain);
void HwdTxAgcCfgDfeHalfSlot(TxMode ddpcMode, HwdRfTxAgcParamT *rfTxAgcParamP, int16 targetGain);
void HwdTxDfeInit(void);
void HwdTxAgcCfgDfeImmed(HwdRfTxAgcImmedParamT *rfTxAgcParamP);
#if (defined MTK_DEV_DUMP_REG)
void HwdTxDfeRegLogRdAll(void);
#endif
/*****************************************************************************
  Global Varible
*****************************************************************************/


/*************************************************************************
      End of the file
*************************************************************************/
#endif

