/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*******************************************************************************
 *
 * Filename:
 * ---------
 * hwddrdi.h
 *
 * Project:
 * --------
 * C2K
 *
 * Description:
 * ------------
 * Header file containing typedefs and definitions pertaining
 * to the RF infrastructure.
 *
 * Author:
 * -------
 * 
 *
 *==============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 *
 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *==============================================================================
 *******************************************************************************/

#ifndef _HWDDRDI_H_
#define _HWDDRDI_H_

#include "sysdefs.h"
#include "nvram_enums.h"
#include "hwdnv.h"
#include "dbmdrdi.h"

#define HWD_DRDI_IDX_VALID_FLAG         (1 << 31)
#define DRDI_IDX_IS_VALID(iDX)          ((iDX) & HWD_DRDI_IDX_VALID_FLAG ? TRUE: FALSE)
#define DRDI_GET_IDX(iDX)               ((iDX) & (~HWD_DRDI_IDX_VALID_FLAG))

#define ERR_INVALID_SET_IDX                 (0x11000000)
#define DrdiAssert(eXPR, pARAM) \
    MonAssert(eXPR, MON_HWD_FAULT_UNIT, HWD_ERR_DRDI, (pARAM), MON_HALT)
#define DrdiCheck(eXPR, cODE, pARAM) \
    MonAssert(eXPR, MON_HWD_FAULT_UNIT, HWD_ERR_DRDI, (cODE) | (pARAM), MON_CONTINUE)

#define NVRAM_EF_HWD_DRDI_PARAM_SIZE                                sizeof(HwdDrdiParamT)
#define NVRAM_EF_HWD_DRDI_PARAM2_SIZE                               sizeof(HwdDrdiParam2T)
#define NVRAM_EF_HWD_DRDI_REMAP_TABLE_SIZE                          sizeof(HwdDrdiRemapTblT)


typedef struct
{
    uint32 c2kDrdiEnable;
    uint32 c2kDrdiDebugEnable;
    uint32 c2kDrdiTotalSetNumber;
    uint32 c2kDrdiRealTotalSetNumber;
} HwdDrdiParamT;

typedef struct
{
    uint32 c2kDrdiSetIndex;
} HwdDrdiParam2T;

typedef struct
{
    uint16 tblPtr[DRDI_TOTAL_SET_NUM];
} HwdDrdiRemapTblT;

typedef PACKED_PREFIX struct
{
    ExeRspMsgT  RspInfo;             /* cmd/rsp info for acknowlegment */
} PACKED_POSTFIX HwdDrdiGetCfgMsgT;

typedef enum
{
    DRDI_STATUS_HWD,
    DRDI_STATUS_INITDONE,
    DRDI_STATUS_CHECKDONE,
    DRDI_STATUS_TIMEOUT,
} HwdDrdiStatusSrcT;


typedef PACKED_PREFIX struct
{
    uint8 src;
    uint8 en;
    uint8 valid;
    uint32 setIdx;
} PACKED_POSTFIX HwdDrdiCfgMsgT;

extern /* const */ HwdDrdiParamT HwdDrdiParam;
extern /* const */ HwdDrdiParam2T HwdDrdiParam2;
extern HwdDrdiRemapTblT DRDI_REMAP_TABLE;

extern HwdDrdiParamT HwdDrdiParamInput;
extern HwdDrdiParam2T HwdDrdiParam2Input;
extern HwdDrdiRemapTblT HwdDrdiRemapTblInput;

extern void* HwdRfCustCustomDataSetNPtr[][DBM_MAX_SEG_RF_CUST_DB];
extern uint32 HwdRfCustCustomDataSetNSize[][DBM_MAX_SEG_RF_CUST_DB];
extern void* HwdRfCalCustomDataSetNPtr[][DBM_MAX_SEG_RF_CAL_DB];
extern uint32 HwdRfCalCustomDataSetNSize[][DBM_MAX_SEG_RF_CAL_DB];
extern void* HwdMipiCustomDataSetNPtr[][DBM_MAX_SEG_MIPI_DB];
extern uint32 HwdMipiCustomDataSetNSize[][DBM_MAX_SEG_MIPI_DB];

extern const uint32 DrdiRealSetNum;
extern uint32 DrdiRealSetNuminput;

/* DBM needed method */
extern void HwdDrdiPreInitParam(void);
extern void HwdDrdiInitParam(void);

/* App needed method */
extern bool HwdDrdiEnable(void);
extern bool HwdDrdiGetSetIndex(bool needRealIdx, uint32 *idxPtr);
extern uint32 HwdDrdiRemap(bool useDefaultRemap, uint32 setIdx);
extern void HwdDrdiGeneralProcess(void *MsgP);
extern void HwdDrdiPostProcess(const HwdDbmSegProcessInfoT * procPtr);
extern void HwdDrdiValidCheck(const void **d);
extern void HwdDrdiInitNvram(const void **d);
extern void HwdDrdiInitNvramDone(const void **d);
extern void HwdDrdiGetStatus(HwdDrdiGetCfgMsgT *msgPtr, uint8 src);
extern void HwdDrdiSendStatueToEts(uint8 src);

#endif /* _HWDDRDI_H_ */

