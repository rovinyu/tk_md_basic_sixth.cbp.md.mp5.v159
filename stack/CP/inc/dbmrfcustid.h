/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*******************************************************************************
 *
 * Filename:
 * ---------
 * dbmrfcustid.h
 *
 * Project:
 * --------
 * C2K
 *
 * Description:
 * ------------
 * Header file containing typedefs and definitions pertaining
 * to the RF infrastructure.
 *
 * Author:
 * -------
 * 
 *
 *==============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 *
 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *==============================================================================
 *******************************************************************************/

#if(!defined(MTK_PLT_RF_ORIONC) || defined(MTK_DEV_HW_SIM_RF))
#define DBM_HWD_RF_CUST_DATA_SIZE       (sizeof(HwdRfCustomDataT))
#define DBM_HWD_RF_CUST_BPI_MASK_SIZE   (sizeof(HwdRfCustBpiMaskT))
#define DBM_HWD_RF_CUST_BPI_DATA_SIZE   (sizeof(HwdRfCustBpiDataT))
#define DBM_HWD_RF_CUST_TAS_SIZE        (sizeof(HwdRfCustTasT))
#else
#define DBM_HWD_RF_CUST_DATA_SIZE       (980)
#define DBM_HWD_RF_CUST_BPI_MASK_SIZE   DBM_HWD_RF_CUST_DATA_SIZE
#define DBM_HWD_RF_CUST_BPI_DATA_SIZE   DBM_HWD_RF_CUST_DATA_SIZE
#define DBM_HWD_RF_CUST_TAS_SIZE        (100)
#endif
#define DBM_HWD_RF_CUST_LAST_SIZE       (1)
#ifdef __DYNAMIC_ANTENNA_TUNING__
#define DBM_HWD_DAT_FE_ROUTE_DATABASE_SIZE     (sizeof(C2K_CUSTOM_DAT_FE_ROUTE_DATABASE_T))
#define DBM_HWD_DAT_FE_BPI_DATABASE_SIZE       (sizeof(C2K_CUSTOM_DAT_FE_DATABASE_T))
#endif
DBM_RF_CUST_ITEM(HWD_RF_CUST_DATA, DBM_HWD_RF_CUST_DATA_SIZE, hwdRfCustomData, &hwdRfCustDataInput, HwdRfCustDataProcess,HWD_RF_BAND_UNDEFINED)
#if !defined(MTK_PLT_RF_ORIONC) || defined(MTK_DEV_HW_SIM_RF)
DBM_RF_CUST_ITEM(HWD_RF_CUST_BPI_MASK, DBM_HWD_RF_CUST_BPI_MASK_SIZE, hwdRfCustomBpiMask, &hwdRfCustBpiMaskInput, HwdRfCustBpiMaskProcess,HWD_RF_BAND_UNDEFINED)
DBM_RF_CUST_ITEM(HWD_RF_CUST_BPI_DATA, DBM_HWD_RF_CUST_BPI_DATA_SIZE, hwdRfCustomBpiData, &hwdRfCustBpiDataInput, HwdRfCustBpiDataProcess,HWD_RF_BAND_UNDEFINED)
#endif
DBM_RF_CUST_ITEM(HWD_RF_CUST_TAS, DBM_HWD_RF_CUST_TAS_SIZE, hwdRfCustomTas, &hwdRfCustTasInput, HwdRfCustTasProcess,HWD_RF_BAND_UNDEFINED)
#ifdef __DYNAMIC_ANTENNA_TUNING__
DBM_RF_CUST_ITEM(HWD_DAT_FE_ROUTE_DATABASE, DBM_HWD_DAT_FE_ROUTE_DATABASE_SIZE, hwdRfCustomDatRouteDataBase, &hwdRfCustomDatRouteDataBaseInput, HwdRfCustDatFeRouteDataProcess,HWD_RF_BAND_UNDEFINED)
DBM_RF_CUST_ITEM(HWD_DAT_FE_BPI_DATABASE, DBM_HWD_DAT_FE_BPI_DATABASE_SIZE, hwdRfCustomDatBpiDataBase, &hwdRfCustomDatBpiDataBaseInput, HwdRfCustDatBpiDataProcess,HWD_RF_BAND_UNDEFINED)
#endif
#ifdef __SAR_TX_POWER_BACKOFF_SUPPORT__
DBM_RF_CUST_ITEM(HWD_SAR_TX_PWR_OFFSET_BAND_A, sizeof(HwdSarTxPowerOffsetT), hwdTxPowerOffsetSar_BandA, NULL,HwdCalibSarTxPowerOffsetDataProcess, HWD_RF_BAND_A)
DBM_RF_CUST_ITEM(HWD_SAR_TX_PWR_OFFSET_BAND_B, sizeof(HwdSarTxPowerOffsetT), hwdTxPowerOffsetSar_BandB, NULL,HwdCalibSarTxPowerOffsetDataProcess, HWD_RF_BAND_B)
DBM_RF_CUST_ITEM(HWD_SAR_TX_PWR_OFFSET_BAND_C, sizeof(HwdSarTxPowerOffsetT), hwdTxPowerOffsetSar_BandC, NULL,HwdCalibSarTxPowerOffsetDataProcess, HWD_RF_BAND_C)
DBM_RF_CUST_ITEM(HWD_SAR_TX_PWR_OFFSET_BAND_D, sizeof(HwdSarTxPowerOffsetT), hwdTxPowerOffsetSar_BandD, NULL,HwdCalibSarTxPowerOffsetDataProcess, HWD_RF_BAND_D)
DBM_RF_CUST_ITEM(HWD_SAR_TX_PWR_OFFSET_BAND_E, sizeof(HwdSarTxPowerOffsetT), hwdTxPowerOffsetSar_BandE, NULL,HwdCalibSarTxPowerOffsetDataProcess, HWD_RF_BAND_E)
#endif
#ifdef __TX_POWER_OFFSET_SUPPORT__
DBM_RF_CUST_ITEM(HWD_TX_PWR_OFFSET_BAND_A, sizeof(HwdTxPowerOffsetT), hwdTxPowerOffset_BandA, NULL,HwdCalibTxPowerOffsetDataProcess, HWD_RF_BAND_A)
DBM_RF_CUST_ITEM(HWD_TX_PWR_OFFSET_BAND_B, sizeof(HwdTxPowerOffsetT), hwdTxPowerOffset_BandB, NULL,HwdCalibTxPowerOffsetDataProcess, HWD_RF_BAND_B)
DBM_RF_CUST_ITEM(HWD_TX_PWR_OFFSET_BAND_C, sizeof(HwdTxPowerOffsetT), hwdTxPowerOffset_BandC, NULL,HwdCalibTxPowerOffsetDataProcess, HWD_RF_BAND_C)
DBM_RF_CUST_ITEM(HWD_TX_PWR_OFFSET_BAND_D, sizeof(HwdTxPowerOffsetT), hwdTxPowerOffset_BandD, NULL,HwdCalibTxPowerOffsetDataProcess, HWD_RF_BAND_D)
DBM_RF_CUST_ITEM(HWD_TX_PWR_OFFSET_BAND_E, sizeof(HwdTxPowerOffsetT), hwdTxPowerOffset_BandE, NULL,HwdCalibTxPowerOffsetDataProcess, HWD_RF_BAND_E)
#endif
DBM_RF_CUST_ITEM(HWD_RF_CUST_LAST, DBM_HWD_RF_CUST_LAST_SIZE, dummyCustData, &dummyCustDataInput, HwdRfCustLastProcess,HWD_RF_BAND_UNDEFINED)
