/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.
*
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
*
* Copyright (c) 2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef _CSS_DEFS_H_

#define _CSS_DEFS_H_  1
#include "sysdefs.h"
#include "exeapi.h"
/* ============================================================ */
/* =========================== SSPR =========================== */
/* ============================================================ */

#define MAX_DO_DEFAULTBAND_SIZE 5

/* New System Selection */

/* #define PRL_MAX_SIZE 8192 */
#define PRL_MAX_SIZE        16384
#define ERI_MAX_SIZE        4096  /* Assuming 30 ERI Table entries */
#define MAX_ERI_ENTRIES     30
#define MAX_ERI_TEXT_LENGTH 32

/* Amala K. */
/* TEMPORARY UNTIL DBM CHANGES MADE FOR MAX_MRU_RECORDS=10 */
#define MAX_MRU_RECORDS     10
#ifdef MTK_DEV_C2K_IRAT
#define MAX_PS_REG_RAL_RECORDS     10
#endif

/* Number of positive and negative sids in the NAM. */
#define MAX_POSITIVE_SIDS   20
#define MAX_NEGATIVE_SIDS   10

/* Roaming types to be used for registration in register.c */

/*===========================================================================*/
/******************************** Local Defines ******************************/
/*===========================================================================*/

/* maximum size based on NAM (non-PRL) available number of channels           */
#define MAX_PCS_CHANS                   48 /* all PCS blocks have maximum = 48        */
#define MAX_CELL_CDMA_CHANS              4 /* cell CDMA from NAM PRI A/B, SEC A/B = 4 */
#define MAX_CELL_CUSTOM_CDMA_CHANS      32 /* cell CDMA custom channels maximum = 32  */
#define MAX_MRU_CHANS                   MAX_MRU_RECORDS
#ifdef MTK_DEV_C2K_IRAT
#define MAX_SIB8_CHANS                  100
#endif
#ifdef MTK_CBP
#define MAX_1X_MAX_ACCESS_FAIL_AVOID_CHAN_NUM   4
#define MAX_DO_MAX_ACCESS_FAIL_AVOID_CHAN_NUM   4
#define ALL_ENTRIES_IN_AVOID_LIST               0xFE
#define INVALID_INDEX_IN_AVOID_LIST             0xFF
#define MAX_1X_MAX_REG_REJ_AVOID_CHAN_NUM       5
#define MAX_1X_MAX_RELEASE_REJ_AVOID_CHAN_NUM   5
#endif

#define MAX_CHANNEL_LIST_SIZE       200
#define MAX_SYSTEM_LIST_SIZE        (90)
#define MAX_EXT_SYSTEM_LIST_SIZE    (90)

#define MAX_SMALL_CHANNEL_LIST_SIZE     20

#define MAX_SMALL_SYSTEM_LIST_SIZE      20
#define MAX_SMALL_EXT_SYSTEM_LIST_SIZE  20

#define ACQUISITION_INDEX_NOT_KNOWN     512

/* Default Band is a requirement from Verizon. Hence the
 * definition below is specific to Verizon Project(s).
 */

#define ACQUISITION_INDEX_NOT_KNOWN_DEFAULT_BAND    513

  /* CDMA Blocks */
#define A_BLOCK      0
#define B_BLOCK      1
#define C_BLOCK      2
#define D_BLOCK      3
#define E_BLOCK      4
#define F_BLOCK      5
#define G_BLOCK      6
#define H_BLOCK      7
#define I_BLOCK      8
#define J_BLOCK      9
#define K_BLOCK      10
#define L_BLOCK      11
#define M_BLOCK      12
#define N_BLOCK      13
#define PSB_BLOCK    27 /* Public Safety Broadband block */

#define UNDEF_BLK    254
#define ALL_BLOCKS   255

/* maximum channels & blocks in PRL acq records */
#ifdef MTK_CBP
#define MAX_CUSTOM_CHANS    128  /* AR_GENERIC_ACQ_REC_HRPD with 121 band/channel combinations in Sprint card */
#else
#define MAX_CUSTOM_CHANS    31
#endif
#define MAX_BLOCKS          14

#define BC0_BLOCK_CHANNEL_INCREMENT      0  /* Use PriA & PriB in the Nam */
#define BC1_BLOCK_CHANNEL_INCREMENT      25
#define BC2_BLOCK_CHANNEL_INCREMENT      0  /* Discrete channels */
#define BC3_BLOCK_CHANNEL_INCREMENT      0  /* Discrete channels */
#define BC4_BLOCK_CHANNEL_INCREMENT      25
#define BC5_BLOCK_CHANNEL_INCREMENT      50
#define BC6_BLOCK_CHANNEL_INCREMENT      25
#define BC7_BLOCK_CHANNEL_INCREMENT      25
#define BC8_BLOCK_CHANNEL_INCREMENT      25
#define BC9_BLOCK_CHANNEL_INCREMENT      25
#define BC10_BLOCK_CHANNEL_INCREMENT     50
#define BC11_BLOCK_CHANNEL_INCREMENT     50
#define BC12_BLOCK_CHANNEL_INCREMENT     25
#define BC13_BLOCK_CHANNEL_INCREMENT     25
#define BC14_BLOCK_CHANNEL_INCREMENT     25
#define BC15_BLOCK_CHANNEL_INCREMENT     25
#define BC16_BLOCK_CHANNEL_INCREMENT     25
#define BC17_BLOCK_CHANNEL_INCREMENT      0
#define BC18_BLOCK_CHANNEL_INCREMENT     25
#define BC19_BLOCK_CHANNEL_INCREMENT     25
#define BC20_BLOCK_CHANNEL_INCREMENT     25

#define CSS_1X_EMERGENCY_CALLBACK_MODE_TIME 300000L

typedef struct
{
    UINT8  blockId;
    UINT16 FirstCh;
    UINT8  NumCh;
} _PrefCh;

typedef struct
{
    UINT8   numBlocks;
    UINT8   increment;
    _PrefCh PrefCh[MAX_BLOCKS];
} _BandBlockChannel;

/* Roaming Indicators */
#define ROAMING_IND_ON                      0
#define ROAMING_IND_OFF                     1
#define ROAMING_IND_FLASHING                2
#define ROAMING_IND_OUT_OF_NEIGHBORHOOD     3
#define ROAMING_IND_OUT_OF_BUILDING         4
#define ROAMING_IND_PREFERRED_SYSTEM        5
#define ROAMING_IND_AVAILABLE_SYSTEM        6
#define ROAMING_IND_ALLIANCE_PARTNER        7
#define ROAMING_IND_PREMIUM_PARTNER         8
#define ROAMING_IND_FULL_SERV_FUNC          9
#define ROAMING_IND_PART_SERV_FUNC          10
#define ROAMING_IND_BANNER_ON               11
#define ROAMING_IND_BANNER_OFF              12
#define ROAM_IND_UNKNOWN                    255

#define NON_STANDARD_ENHANCED_ROAMING_IND_START  64

/* This will allow the user to switch or not to switch between geos during More Pref Scan.
 * By default, the MS will switch between geos. */
#define GEO_SWITCHING_SUPPORTED

/* E-PRL Defines */
#define OPERATIONAL_SID_0      15904
#define OPERATIONAL_SID_1      15905

/* Maximum time to wait for ESPM to validate System Selection */
#define CSS_1X_EPRL_ESPM_WAIT_TIME 12000L

/* Arbitrary maximum number of times SelectChannel() can be called recursively before
 * SVSM decides its in an infinite loop and throws MonFault
 * CSS_ERR_SVSM_SELECT_CHANNEL_TOO_MANY_RECURSIONS */
#define CSS_1X_SVSM_SELECT_CHANNEL_RECURSION_LIMIT 15
/*===========================================================================*/
/*************************** Local Data Structures ***************************/
/*===========================================================================*/

typedef NV_PACKED_PREFIX struct
{
    SysCdmaBandT DOBand;
    uint16 DOChannel;
} NV_PACKED_POSTFIX CTDoDefaultBandDataT;

typedef PACKED_PREFIX struct _CHANNEL_DESC {
    SysBandChannelT chan;

    /* Association Tag, valid only for DO system, and assume they are valid only for
     * associated DO scan list system */
    bool Pn_Association;
    bool Data_Association;
} PACKED_POSTFIX  CHANNEL_DESC, PACKED_POSTFIX *pCHANNEL_DESC;

typedef enum
{
    MRU,      /* Most recently used scan list  */
    MPL,      /* Most preferred scan list      */
    CRSL,     /* Channel release scan list     */
    PRL,      /* PRL scan list                 */
    DPCL,     /* Default band scan list        */
    RSL,      /* Redirection scan list         */
    OSL,      /* OTASP scan list               */
    GSL,      /* GEO scan list                 */
    ADOSL,    /* Associated DO scan list       */
    SRSL,     /* Silent retry scan list        */
    DEFAULTBAND, /* unused */
    SRRescan,    /* unused */
    ASMPL,    /* DO Asso More Pref scan list   */
    ADOLOSL,  /* DO Lost Asso scan list        */
    PRLOSL,   /* DO Lost PRL MRU scan list     */
    GLOSL,    /* DO Lost GEO scan list         */
    EMERGENCY,/* Emergency call scan list      */
    TEMPSL,
    AVOIDL,   /* Avoidance list                */
    HSRSL,    /* hVoLte silent redial scan list*/
    IGSL,     /* Idle GEO scan list            */
#ifdef MTK_CBP
    PSREGRAL, /* Recently acquired list        */
#endif
    RSSI,     /* Rssi Scan list*/
    CSS_LIST_END
} CssLists;

typedef struct
{
    CssLists        ListType;
    UINT16          ChanListNumChans;
    UINT16          ChanListIndex;
    CHANNEL_DESC    ChanList[MAX_CHANNEL_LIST_SIZE];
    UINT16          AcqIndex[MAX_CHANNEL_LIST_SIZE];
} ChannelList, *pChannelList;

typedef struct
{
    CssLists        ListType;
    UINT16          ChanListNumChans;
    UINT16          ChanListIndex;
    CHANNEL_DESC    ChanList[MAX_SMALL_CHANNEL_LIST_SIZE];
    UINT16          AcqIndex[MAX_SMALL_CHANNEL_LIST_SIZE];
} SmallChannelList, *pSmallChannelList;

#ifdef MTK_DEV_C2K_IRAT
typedef struct
{
    CssLists        ListType;
    UINT16          ChanListNumChans;
    UINT16          ChanListIndex;
    CHANNEL_DESC    ChanList[MAX_SMALL_CHANNEL_LIST_SIZE];
    UINT16          AcqIndex[MAX_SMALL_CHANNEL_LIST_SIZE];
    UINT16          IratPri[MAX_SMALL_CHANNEL_LIST_SIZE];
} SmallRalChannelList, *pSmallRalChannelList;
#endif

#define CSS_1X_TIMER_START  0
#define CSS_DO_TIMER_START 16
#ifdef MTK_DEV_C2K_IRAT
#define EHRPD_TIMER_START 40

typedef enum
{
    NOT_KNOWN,
    MOST_PREFERRED,
    PREFERRED,
    NEGATIVE,
    NON_SYSTEM_TABLE,
    SECOND_PREFERRED /* for LTE&DO interworing, second highest priority network in GEO */
} SystemPreference;
#else
typedef enum
{
    NOT_KNOWN,
    MOST_PREFERRED,
    PREFERRED,
    NEGATIVE,
    NON_SYSTEM_TABLE
} SystemPreference;
#endif /* MTK_DEV_C2K_IRAT */

typedef struct
{
    SysBandChannelT chan;
    int16           rssiValue;
} ChannelRssiValueT;

typedef struct
{
    UINT16              ChanListNumChans;
    ChannelRssiValueT   ChannelRssiValue[MAX_CHANNEL_LIST_SIZE];
} RssiResultT;

typedef enum
{
    VR_NON_SYS_TABLE,
    VR_ACCEPTED,
    VR_KEEP_SCAN,
    VR_NEGATIVE
}ValidateResult;

typedef enum
{
    CSS_CPSM_START_SCAN,
    CSS_CPSM_OOSA_SLEEP,
    CSS_CPSM_IN_SERVICE,
    CSS_CPSM_PMSS_SCAN,
    NUM_CSS_CPSM_EVT
}CssCpsmEvt;

typedef struct
{
    bool RspPended;
    ExeRspMsgT RspMsgInfo;
}CssSvcStatusRspInfoT;

typedef enum
{
    PSW_UIM_NOTIFY,
    PSW_WHAT_CHANGED,
    PSW_NOUIM_INIT,
    PSW_ILLEG_UPDATE,
    PSW_UIM_UPDATE,
    PSW_UIM_RESPONSE,
    PSW_UIM_GETPRL,
    PSW_UIM_PRL_RESPONSE,
    PSW_UIM_STORE_ESN,
    PSW_UIM_INIT,
    PSW_UIM_UIMNAMREAD,
    PSW_NAM_CH_BY_ETS,
    PSW_NAM_DATA_READ,
    PSW_NAM_DATA_WRITE,
    PSW_NAM_DATA_REQ,
    PSW_UIM_DATA_REQ,
    CSS_UIM_NOTIFY,
    CSS_UIM_INIT,
    CSS_UIM_UIMNAMREAD,
    PSW_UIM_ESN_STORE,
    PSW_UIM_OTA_SEC_API_RESPONSE,
    PSW_UIM_OTA_PARS_RELEASE,
    PSW_UIM_OTA_PARS_IN_SVC,
    PSW_UIM_OTA_PARS_IN_SVC_WAIT,
    PSW_UIM_OTA_PARS_CONFIRM,
    PSW_UIM_OTA_PARS_RCV_OUST,
    PSW_UIM_OTA_PARS_UIM_RESPONSE
} UimFunction;

/**********************************************************************/
/******************* CUSTOM SYSTEM SELECTION **************************/
/**********************************************************************/

/* ============================================================ */
/* =================== OUT OF SERVICE AREA ==================== */
/* ============================================================ */

#define MAX_OOSA_PHASES             10
#define CSS_OOSA_MAX_SLEEP_SECS     510

/* Scan duration is specified in seconds, stored in units of milliseconds */
#define CP_SELECTION_TIME_SCALE     1000

#endif
