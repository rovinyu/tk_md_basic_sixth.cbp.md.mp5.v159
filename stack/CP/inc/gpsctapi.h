/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef GPSCTAPI_H
#define GPSCTAPI_H
/*****************************************************************************
 
  FILE NAME:  gpsctapi.h

  DESCRIPTION : GPSCT task interface
 
      This include file provides system wide global type declarations and 
      constants
      **NOTE: This file is SHARED/INCLUDED IN THE PARTHUS CODE****
      *******SO, ANY/ALL OF THE CP RELATED TYPES SHOULD BE********
      *******GENERIC C TYPES..............................********
 
  HISTORY     :
      See Log at end of file

*****************************************************************************/

/*#include "sysdefs.h" 
#include "exeapi.h" */

/*------------------------------------------------------------------------
* EXE Interfaces - Definition of Signals and Mailboxes
*------------------------------------------------------------------------*/

/* GPSCT command mailbox id */
/*Satya: I have to do this because i cannot include any*/
/*Satya: references to CP related "defs" like EXE_MA...*/
/*Satya: Otherwise, i will have to include exeapi.h    */
/*Satya: which would mean that sysdefs.h would be included */
/*Satya: which would mean that the standard C types would be*/
/*Satya: redefined..Ex: Parthus uses U8 rather than uint8*/
/*Satya: This would mean that the sysdefs.h should not be */
/*Satya: compiled in with any of the Parthus code..etc..  */
/*Satya: Hence, i have commented out the following code   */
/*#define GPSCT_MAILBOX         EXE_MAILBOX_1_ID*/
#define GPSCT_MAILBOX         0x00


/*----------------------------------------------------------------------------
 Defines Constants used in this file
----------------------------------------------------------------------------*/

typedef PACKED_PREFIX struct
{
	unsigned short PilotPN;
	signed short  SystemTimeOffset;
} PACKED_POSTFIX  GpsCtGpsEndGpsAckMsgT;

typedef PACKED_PREFIX struct
{
	unsigned short Enable;
} PACKED_POSTFIX  GpsCtGpsHwEnableMsgT;

/* Define GPSCT Task msg Ids */
typedef enum 
{
   /* from PSW */
   GPSCT_36BIT_NWK_CDMA_TIME_MSG,
   GPSCT_PSEUDORANGE_MSMT_REQ_MSG,
   GPSCT_SENSITIVITY_ASSIST_MSG,
   GPSCT_SESSION_END_REQ_MSG,
					   
   /* from L1D */
   GPSCT_GPS_INIT_GPS_ACK_MSG,
   GPSCT_GPS_CONT_GPS_MSG,
   GPSCT_GPS_END_GPS_ACK_MSG,
   GPSCT_GPS_START_AUTONOMOUS,

   GPSCT_GPS_HW_ENABLE_MSG,

   GPSCT_NUM_MSGS_ID
} GpsCtMsgIdT;


#define GPSCT_PRM_MAX_SIZE  	   3
#define GPSCT_MAX_ASSIST_SIZE	 113
#define GPSCT_PRM_MAX_RSP_SIZE	 256

typedef struct
{
   unsigned char     IdleOrTraffic;  /* TRUE = Idle, FALSE = Traffic */
   unsigned char     SystemTimeOffsetIncl;
   unsigned short   ReqMsmtRecLen;  /* Request PRM Rec Length */
   unsigned char    ReqPseudorangesRec[GPSCT_PRM_MAX_SIZE];  /* Req PRM Buffer */
   unsigned short   ProvAcqAssistRecLen;  /* Prov GPS Acquisition Assist Rec Length */
   unsigned char    ProvAcqAssistRec[GPSCT_MAX_ASSIST_SIZE];  /* Prov GPS Acquisition Assist Buffer */
} GpsCtPseudorangeMsmtReqMsgT;

#define GPSCT_MAX_NUM_SA_NAV_BYTES 128
#define GPSCT_MAX_NUM_SA_SV         16
#define GPSCT_MAX_NUM_SA_DATA_RECS  3
typedef struct
{
   unsigned char   NavMsgBits[GPSCT_MAX_NUM_SA_NAV_BYTES];
   unsigned char   NumSvDr;
   unsigned char   SvNumArray[GPSCT_MAX_NUM_SA_SV];
} GpsCtSensAssistDataRecT;

typedef struct          
{
   unsigned short          RefBitNum;
   unsigned char           NumDataRecs;  /* num of data recs */
   unsigned short          DataRecSizeInBits;  /* size of each data rec in bits; range 0 to 1020 */  
   GpsCtSensAssistDataRecT SADataRecArray[GPSCT_MAX_NUM_SA_DATA_RECS];
} GpsCtSensAssistMsgT;    

/* GPSCT_SESSION_END_REQ_MSG:
 *    Sent by PSW to end a GPS Session. PSW passes on if it reset
 *    the MS to System Determination (TRUE) or not (FALSE). PDE 
 *    responds with a PSW_GPSCT_SESSION_END_RSP_MSG.
 */
typedef PACKED_PREFIX struct
{
	unsigned char		SystemDeter; /* TRUE=MS reset to SYS DETER */
} PACKED_POSTFIX  GpsCtSessionEndReqMsgT;

typedef PACKED_PREFIX struct
{
	unsigned char RecLen;
	unsigned char ProvPseudorangeRec[GPSCT_PRM_MAX_RSP_SIZE];
} PACKED_POSTFIX  GpsCtPseudorangeMsmtRspMsgT;

/* Satya: This structure mirrors the structure that is used in the */
/* Parthus DB to hold AA information.				   */

 typedef struct
 {
      unsigned char TOA;         /* Time of applicability*/
      unsigned char NumSV;       /* Number of satellites for which data is available*/
      unsigned char AzElIncl;     /* Azimuth and elevation angle included? TRUE/FALSE*/
      unsigned char SV[9];       /* SV PRN number (0 means no data available)*/
      signed short Dopp[9];     /* Doppler value. Units Hz*/
      char DoppRate[9];  /* Doppler rate of change. Units (1/64) Hz/s*/
      unsigned short DoppSR[9];  /* Doppler search range. Units Hz.*/
      signed short Code[9];     /* Code phase [range 0..1022]. Units C/A chips*/
      unsigned char CodePhInt[9];
      unsigned char GpsBitNum[9];
      unsigned short CodeSR[9];  /* Code search range. Units C/A chips [range 0..512]*/
      unsigned char Azim[9];     /* Azimuth. Units 11.25 degrees*/
      unsigned char Elev[9];     /* Elevation. Units 11.25 degrees*/
 } GpsCtNAAcqAss;

/* Satya: This is the structure that mirrors the PRM response */

typedef struct
{
   unsigned char PRN_num;  /* Satellite PRN number */
   unsigned char CNO;      /* Satellite C/No [dB-Hz] (range 0 to 63 dB-Hz) */
   signed short Dopp;      /* Measured Doppler frequency [0.2 Hz] (range +/-6553.6) */
   unsigned short Code_whole; /* Satellite Code phase measurement - whole chips */
                    /*   [C/A chips] (range 0..1022) */
   unsigned short Code_fract; /* Satellite Code phase measurement - fractional chips */
                    /*   [2^-10 C/A chips] (range 0..1023) */
   unsigned char Mul_Path_Ind; /* Multipath indicator (range 0..3) */
                    /*   (see TIA/EIA/IS-801 Table 3.2.4.2-7) */
   unsigned char Range_RMS_Exp;/* Pseudorange RMS error: Exponent (range 0..7) */
                    /*   (see TIA/EIA/IS-801 Table 3.2.4.2-8) */
   unsigned char Range_RMS_Man;/* Pseudorange RMS error: Mantissa (range 0..7) */
                    /*   (see TIA/EIA/IS-801 Table 3.2.4.2-8) */
   signed short Code_AA_diff; /* Code (Meas - AcqAid) difference [C/A chips] (range +/-512) */
   signed short Dopp_AA_diff; /* Doppler (Meas -AcqAid) difference [Hz] */
}GpsCtIS801SVData;

typedef struct
{
 unsigned char NumValidMeas; /* Number of Valid Meas avail [0..9] */
 unsigned int Time_Ref; /* Time of Validity [ms] % 14400000 */
 unsigned char TimeRefSrc; /* Time reference source [see IS801 std] */
 GpsCtIS801SVData SV_Data[9]; /* SV PRM data NUM_CH*/
}GpsCtPRMeas;

#endif

/*****************************************************************************
* $Log: gpsctapi.h $
* Revision 1.4  2006/03/30 11:04:26  ckackman
* GPS autonomous message definition for gps control task.
* Revision 1.3  2005/04/26 18:05:30  ssriniva
* Changed __packed to PACKED, to take care of 
* OTTS build problems.
* Revision 1.2  2005/04/22 12:11:32  ssriniva
* Gps Merge. Refer release notes.
*****************************************************************************/
