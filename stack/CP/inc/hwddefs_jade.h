/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*******************************************************************************
 *
 * Filename:
 * ---------
 * hwddefs_jade.h
 *
 * Project:
 * --------
 * C2K
 *
 * Description:
 * ------------
 * Header file containing typedefs and definitions pertaining
 * to the RF infrastructure.
 *
 * Author:
 * -------
 * 
 *
 *==============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 *
 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *==============================================================================
 *******************************************************************************/

/******************************************
    BSIs register define
******************************************/
#define MIPI0_BASE                     (0x0b820000)

#define MIPI0_RESET                    (MIPI0_BASE + 0x4*0x0)
#define MIPI0_CTRL                     (MIPI0_BASE + 0x4*0x1)
#define MIPI0_IMMED_CMD_CTRL           (MIPI0_BASE + 0x4*0x2)
#define MIPI0_IMMED_CMD_DATA           (MIPI0_BASE + 0x4*0x3)
#define MIPI0_IMMED_TRIG               (MIPI0_BASE + 0x4*0x4)
#define MIPI0_IMMED_RDATA_LSB          (MIPI0_BASE + 0x4*0x5)
#define MIPI0_IMMED_RDATA_MSB          (MIPI0_BASE + 0x4*0x6)
#define MIPI0_DO1ST_CMD_CTRL1          (MIPI0_BASE + 0x4*0x7)
#define MIPI0_DO1ST_CMD_DATA1          (MIPI0_BASE + 0x4*0x8)
#define MIPI0_DO2ND_CMD_CTRL1          (MIPI0_BASE + 0x4*0x9)
#define MIPI0_DO2ND_CMD_DATA1          (MIPI0_BASE + 0x4*0xa)
#define MIPI0_DO1ST_CMD_CTRL2          (MIPI0_BASE + 0x4*0xb)
#define MIPI0_DO1ST_CMD_DATA2          (MIPI0_BASE + 0x4*0xc)
#define MIPI0_DO2ND_CMD_CTRL2          (MIPI0_BASE + 0x4*0xd)
#define MIPI0_DO2ND_CMD_DATA2          (MIPI0_BASE + 0x4*0xe)
#define MIPI0_TXGATEON_CMD_CTRL        (MIPI0_BASE + 0x4*0xf)
#define MIPI0_TXGATEON_CMD_DATA        (MIPI0_BASE + 0x4*0x10)
#define MIPI0_TXGATEOFF_CMD_CTRL       (MIPI0_BASE + 0x4*0x11)
#define MIPI0_TXGATEOFF_CMD_DATA       (MIPI0_BASE + 0x4*0x12)
#define MIPI0_TXGATE_MASK              (MIPI0_BASE + 0x4*0x13)
#define MIPI0_INT_STATUS               (MIPI0_BASE + 0x4*0x14)
#define MIPI0_INT_MASK                 (MIPI0_BASE + 0x4*0x15)
#define MIPI0_IMMED_BUF_STATUS         (MIPI0_BASE + 0x4*0x16)
#define MIPI0_IMMED_RDBUF_STATUS       (MIPI0_BASE + 0x4*0x17)
#define MIPI0_DLY_BUF1_DO1ST_STATUS    (MIPI0_BASE + 0x4*0x18)
#define MIPI0_DLY_BUF1_DO2ND_STATUS    (MIPI0_BASE + 0x4*0x19)
#define MIPI0_DLY_BUF2_DO1ST_STATUS    (MIPI0_BASE + 0x4*0x1a)
#define MIPI0_DLY_BUF2_DO2ND_STATUS    (MIPI0_BASE + 0x4*0x1b)
#define MIPI0_TGON_BUF_STATUS          (MIPI0_BASE + 0x4*0x1c)
#define MIPI0_TGOFF_BUF_STATUS         (MIPI0_BASE + 0x4*0x1d)

#define BSI_REG_BASE(bSInAME)                   (bSInAME)

#define BSI_REG_RESET(bSInAME)                  (MIPI0_RESET                 + (bSInAME) * 0x2000)
#define BSI_REG_CTRL(bSInAME)                   (MIPI0_CTRL                  + (bSInAME) * 0x2000)
#define BSI_REG_IMMED_CMD_CTRL(bSInAME)         (MIPI0_IMMED_CMD_CTRL        + (bSInAME) * 0x2000)
#define BSI_REG_IMMED_CMD_DATA(bSInAME)         (MIPI0_IMMED_CMD_DATA        + (bSInAME) * 0x2000)
#define BSI_REG_IMMED_TRIG(bSInAME)             (MIPI0_IMMED_TRIG            + (bSInAME) * 0x2000)
#define BSI_REG_IMMED_RDATA_LSB(bSInAME)        (MIPI0_IMMED_RDATA_LSB       + (bSInAME) * 0x2000)
#define BSI_REG_IMMED_RDATA_MSB(bSInAME)        (MIPI0_IMMED_RDATA_MSB       + (bSInAME) * 0x2000)
#define BSI_REG_DO1ST_CMD_CTRL1(bSInAME)        (MIPI0_DO1ST_CMD_CTRL1       + (bSInAME) * 0x2000)
#define BSI_REG_DO1ST_CMD_DATA1(bSInAME)        (MIPI0_DO1ST_CMD_DATA1       + (bSInAME) * 0x2000)
#define BSI_REG_DO2ND_CMD_CTRL1(bSInAME)        (MIPI0_DO2ND_CMD_CTRL1       + (bSInAME) * 0x2000)
#define BSI_REG_DO2ND_CMD_DATA1(bSInAME)        (MIPI0_DO2ND_CMD_DATA1       + (bSInAME) * 0x2000)
#define BSI_REG_DO1ST_CMD_CTRL2(bSInAME)        (MIPI0_DO1ST_CMD_CTRL2       + (bSInAME) * 0x2000)
#define BSI_REG_DO1ST_CMD_DATA2(bSInAME)        (MIPI0_DO1ST_CMD_DATA2       + (bSInAME) * 0x2000)
#define BSI_REG_DO2ND_CMD_CTRL2(bSInAME)        (MIPI0_DO2ND_CMD_CTRL2       + (bSInAME) * 0x2000)
#define BSI_REG_DO2ND_CMD_DATA2(bSInAME)        (MIPI0_DO2ND_CMD_DATA2       + (bSInAME) * 0x2000)
#define BSI_REG_TXGATEON_CMD_CTRL(bSInAME)      (MIPI0_TXGATEON_CMD_CTRL     + (bSInAME) * 0x2000)
#define BSI_REG_TXGATEON_CMD_DATA(bSInAME)      (MIPI0_TXGATEON_CMD_DATA     + (bSInAME) * 0x2000)
#define BSI_REG_TXGATEOFF_CMD_CTRL(bSInAME)     (MIPI0_TXGATEOFF_CMD_CTRL    + (bSInAME) * 0x2000)
#define BSI_REG_TXGATEOFF_CMD_DATA(bSInAME)     (MIPI0_TXGATEOFF_CMD_DATA    + (bSInAME) * 0x2000)
#define BSI_REG_TXGATE_MASK(bSInAME)            (MIPI0_TXGATE_MASK           + (bSInAME) * 0x2000)
#define BSI_REG_INT_STATUS(bSInAME)             (MIPI0_INT_STATUS            + (bSInAME) * 0x2000)
#define BSI_REG_INT_MASK(bSInAME)               (MIPI0_INT_MASK              + (bSInAME) * 0x2000)
#define BSI_REG_IMMED_BUF_STATUS(bSInAME)       (MIPI0_IMMED_BUF_STATUS      + (bSInAME) * 0x2000)
#define BSI_REG_IMMED_RDBUF_STATUS(bSInAME)     (MIPI0_IMMED_RDBUF_STATUS    + (bSInAME) * 0x2000)
#define BSI_REG_DLY_BUF1_DO1ST_STATUS(bSInAME)  (MIPI0_DLY_BUF1_DO1ST_STATUS + (bSInAME) * 0x2000)
#define BSI_REG_DLY_BUF1_DO2ND_STATUS(bSInAME)  (MIPI0_DLY_BUF1_DO2ND_STATUS + (bSInAME) * 0x2000)
#define BSI_REG_DLY_BUF2_DO1ST_STATUS(bSInAME)  (MIPI0_DLY_BUF2_DO1ST_STATUS + (bSInAME) * 0x2000)
#define BSI_REG_DLY_BUF2_DO2ND_STATUS(bSInAME)  (MIPI0_DLY_BUF2_DO2ND_STATUS + (bSInAME) * 0x2000)
#define BSI_REG_TGON_BUF_STATUS(bSInAME)        (MIPI0_TGON_BUF_STATUS       + (bSInAME) * 0x2000)
#define BSI_REG_TGOFF_BUF_STATUS(bSInAME)       (MIPI0_TGOFF_BUF_STATUS      + (bSInAME) * 0x2000)

/******************************************
    BPI register define
******************************************/
#define TX_TXON_BASE                   (0x0b850000)

#define TX_TXON1XDO_MODE_SEL0          (TX_TXON_BASE + 0x4*0x0)
#define TX_TXON1XDO_MODE_SEL1          (TX_TXON_BASE + 0x4*0x1)
#define TX_TXON1XDO_MODE_SEL2          (TX_TXON_BASE + 0x4*0x2)
#define TX_TXON1XDO_MODE_SEL3          (TX_TXON_BASE + 0x4*0x3)
#define TX_CP_DSPM0                    (TX_TXON_BASE + 0x4*0x4)
#define TX_CP_DSPM1                    (TX_TXON_BASE + 0x4*0x5)
#define TX_CP_DSPM2                    (TX_TXON_BASE + 0x4*0x6)
#define TX_CP_DSPM3                    (TX_TXON_BASE + 0x4*0x7)
#define TX_ON_DIN0                     (TX_TXON_BASE + 0x4*0x8)
#define TX_ON_DIN1                     (TX_TXON_BASE + 0x4*0x9)
#define TX_ON_DIN2                     (TX_TXON_BASE + 0x4*0xa)
#define TX_ON_DIN3                     (TX_TXON_BASE + 0x4*0xb)
#define TX_ON_DLY_DIN00                (TX_TXON_BASE + 0x4*0xc)
#define TX_ON_DLY_DIN01                (TX_TXON_BASE + 0x4*0xd)
#define TX_ON_DLY_DIN02                (TX_TXON_BASE + 0x4*0xe)
#define TX_ON_DLY_DIN03                (TX_TXON_BASE + 0x4*0xf)
#define TX_ON_DLY_DIN10                (TX_TXON_BASE + 0x4*0x10)
#define TX_ON_DLY_DIN11                (TX_TXON_BASE + 0x4*0x11)
#define TX_ON_DLY_DIN12                (TX_TXON_BASE + 0x4*0x12)
#define TX_ON_DLY_DIN13                (TX_TXON_BASE + 0x4*0x13)
#define TX_ON_TYPE0                    (TX_TXON_BASE + 0x4*0x14)
#define TX_ON_TYPE1                    (TX_TXON_BASE + 0x4*0x15)
#define TX_ON_TYPE2                    (TX_TXON_BASE + 0x4*0x16)
#define TX_ON_TYPE3                    (TX_TXON_BASE + 0x4*0x17)
#define TX_ON_POL0                     (TX_TXON_BASE + 0x4*0x18)
#define TX_ON_POL1                     (TX_TXON_BASE + 0x4*0x19)
#define TX_ON_POL2                     (TX_TXON_BASE + 0x4*0x1a)
#define TX_ON_POL3                     (TX_TXON_BASE + 0x4*0x1b)
#define TX_ON_EN0                      (TX_TXON_BASE + 0x4*0x1c)
#define TX_ON_EN1                      (TX_TXON_BASE + 0x4*0x1d)
#define TX_ON_EN2                      (TX_TXON_BASE + 0x4*0x1e)
#define TX_ON_EN3                      (TX_TXON_BASE + 0x4*0x1f)
#define TX_ON_OVERRIDE_SEL0            (TX_TXON_BASE + 0x4*0x20)
#define TX_ON_OVERRIDE_SEL1            (TX_TXON_BASE + 0x4*0x21)
#define TX_ON_OVERRIDE_SEL2            (TX_TXON_BASE + 0x4*0x22)
#define TX_ON_OVERRIDE_SEL3            (TX_TXON_BASE + 0x4*0x23)
#define TX_ON_SETUP0                   (TX_TXON_BASE + 0x4*0x24)
#define TX_ON_SETUP1                   (TX_TXON_BASE + 0x4*0x25)
#define TX_ON_SETUP2                   (TX_TXON_BASE + 0x4*0x26)
#define TX_ON_SETUP3                   (TX_TXON_BASE + 0x4*0x27)
#define TX_ON_SETUP4                   (TX_TXON_BASE + 0x4*0x28)
#define TX_ON_SETUP5                   (TX_TXON_BASE + 0x4*0x29)
#define TX_ON_SETUP6                   (TX_TXON_BASE + 0x4*0x2a)
#define TX_ON_SETUP7                   (TX_TXON_BASE + 0x4*0x2b)
#define TX_ON_SETUP8                   (TX_TXON_BASE + 0x4*0x2c)
#define TX_ON_SETUP9                   (TX_TXON_BASE + 0x4*0x2d)
#define TX_ON_SETUP10                  (TX_TXON_BASE + 0x4*0x2e)
#define TX_ON_SETUP11                  (TX_TXON_BASE + 0x4*0x2f)
#define TX_ON_SETUP12                  (TX_TXON_BASE + 0x4*0x30)
#define TX_ON_SETUP13                  (TX_TXON_BASE + 0x4*0x31)
#define TX_ON_SETUP14                  (TX_TXON_BASE + 0x4*0x32)
#define TX_ON_SETUP15                  (TX_TXON_BASE + 0x4*0x33)
#define TX_ON_SETUP16                  (TX_TXON_BASE + 0x4*0x34)
#define TX_ON_SETUP17                  (TX_TXON_BASE + 0x4*0x35)
#define TX_ON_SETUP18                  (TX_TXON_BASE + 0x4*0x36)
#define TX_ON_SETUP19                  (TX_TXON_BASE + 0x4*0x37)
#define TX_ON_SETUP20                  (TX_TXON_BASE + 0x4*0x38)
#define TX_ON_SETUP21                  (TX_TXON_BASE + 0x4*0x39)
#define TX_ON_SETUP22                  (TX_TXON_BASE + 0x4*0x3a)
#define TX_ON_SETUP23                  (TX_TXON_BASE + 0x4*0x3b)
#define TX_ON_SETUP24                  (TX_TXON_BASE + 0x4*0x3c)
#define TX_ON_SETUP25                  (TX_TXON_BASE + 0x4*0x3d)
#define TX_ON_SETUP26                  (TX_TXON_BASE + 0x4*0x3e)
#define TX_ON_HOLD0                    (TX_TXON_BASE + 0x4*0x3f)
#define TX_ON_HOLD1                    (TX_TXON_BASE + 0x4*0x40)
#define TX_ON_HOLD2                    (TX_TXON_BASE + 0x4*0x41)
#define TX_ON_HOLD3                    (TX_TXON_BASE + 0x4*0x42)
#define TX_ON_HOLD4                    (TX_TXON_BASE + 0x4*0x43)
#define TX_ON_HOLD5                    (TX_TXON_BASE + 0x4*0x44)
#define TX_ON_HOLD6                    (TX_TXON_BASE + 0x4*0x45)
#define TX_ON_HOLD7                    (TX_TXON_BASE + 0x4*0x46)
#define TX_ON_HOLD8                    (TX_TXON_BASE + 0x4*0x47)
#define TX_ON_HOLD9                    (TX_TXON_BASE + 0x4*0x48)
#define TX_ON_HOLD10                   (TX_TXON_BASE + 0x4*0x49)
#define TX_ON_HOLD11                   (TX_TXON_BASE + 0x4*0x4a)
#define TX_ON_HOLD12                   (TX_TXON_BASE + 0x4*0x4b)
#define TX_ON_HOLD13                   (TX_TXON_BASE + 0x4*0x4c)
#define TX_ON_HOLD14                   (TX_TXON_BASE + 0x4*0x4d)
#define TX_ON_HOLD15                   (TX_TXON_BASE + 0x4*0x4e)
#define TX_ON_HOLD16                   (TX_TXON_BASE + 0x4*0x4f)
#define TX_ON_HOLD17                   (TX_TXON_BASE + 0x4*0x50)
#define TX_ON_HOLD18                   (TX_TXON_BASE + 0x4*0x51)
#define TX_ON_HOLD19                   (TX_TXON_BASE + 0x4*0x52)
#define TX_ON_HOLD20                   (TX_TXON_BASE + 0x4*0x53)
#define TX_ON_HOLD21                   (TX_TXON_BASE + 0x4*0x54)
#define TX_ON_HOLD22                   (TX_TXON_BASE + 0x4*0x55)
#define TX_ON_HOLD23                   (TX_TXON_BASE + 0x4*0x56)
#define TX_ON_HOLD24                   (TX_TXON_BASE + 0x4*0x57)
#define TX_ON_HOLD25                   (TX_TXON_BASE + 0x4*0x58)
#define TX_ON_HOLD26                   (TX_TXON_BASE + 0x4*0x59)
#define TX_CP_GRP_DLY                  (TX_TXON_BASE + 0x188)


/******************************************
    Delay loader register define
******************************************/
#define HWD_DLY_LDR_BASE                        (0x0b840000)
#define HWD_TXONCMP0_1ST                        (HWD_DLY_LDR_BASE + 0x800)
#define HWD_TXONCMP0_2ND                        (HWD_DLY_LDR_BASE + 0x804)
#define HWD_TXONCMPINT_1ST_A                    (HWD_DLY_LDR_BASE + 0x87c)
#define HWD_TXONCMPINT_2ND_A                    (HWD_DLY_LDR_BASE + 0x880)
#define HWD_TXONCMPINT_1ST_M                    (HWD_DLY_LDR_BASE + 0x848)
#define HWD_TXONCMPINT_2ND_M                    (HWD_DLY_LDR_BASE + 0x84c)
#define HWD_PDMCMP3_1ST                         (HWD_DLY_LDR_BASE + 0x894)
#define HWD_PDMCMP3_2ND                         (HWD_DLY_LDR_BASE + 0x898)
#define HWD_FINEGAINOLCMP_1ST                   (HWD_DLY_LDR_BASE + 0x904)
#define HWD_FINEGAINOLCMP_2ND                   (HWD_DLY_LDR_BASE + 0x908)
#define HWD_FINEGAINCLINICMP_1ST                (HWD_DLY_LDR_BASE + 0x914)
#define HWD_FINEGAINCLINICMP_2ND                (HWD_DLY_LDR_BASE + 0x918)
#define HWD_PMICCMP_1ST                         (HWD_DLY_LDR_BASE + 0x92c)
#define HWD_PMICCMP_2ND                         (HWD_DLY_LDR_BASE + 0x930)
#define HWD_BPI_CMP0_1ST                        (HWD_DLY_LDR_BASE + 0x97c)
#define HWD_BPI_CMP0_2ND                        (HWD_DLY_LDR_BASE + 0x980)
#define HWD_BPI_CMP1_1ST                        (HWD_DLY_LDR_BASE + 0x984)
#define HWD_BPI_CMP1_2ND                        (HWD_DLY_LDR_BASE + 0x988)
#define HWD_BPI_CMP2_1ST                        (HWD_DLY_LDR_BASE + 0x98c)
#define HWD_BPI_CMP2_2ND                        (HWD_DLY_LDR_BASE + 0x990)
#define HWD_BPI_CMP3_1ST                        (HWD_DLY_LDR_BASE + 0x994)
#define HWD_BPI_CMP3_2ND                        (HWD_DLY_LDR_BASE + 0x998)
#define HWD_BPI_CMP4_1ST                        (HWD_DLY_LDR_BASE + 0x99c)
#define HWD_BPI_CMP4_2ND                        (HWD_DLY_LDR_BASE + 0x9a0)
#define HWD_BPI_CMP5_1ST                        (HWD_DLY_LDR_BASE + 0x9a4)
#define HWD_BPI_CMP5_2ND                        (HWD_DLY_LDR_BASE + 0x9a8)
#define HWD_BPI_CMP6_1ST                        (HWD_DLY_LDR_BASE + 0x9ac)
#define HWD_BPI_CMP6_2ND                        (HWD_DLY_LDR_BASE + 0x9b0)
#define HWD_BPI_CMP7_1ST                        (HWD_DLY_LDR_BASE + 0x9b4)
#define HWD_BPI_CMP7_2ND                        (HWD_DLY_LDR_BASE + 0x9b8)
#define HWD_BPI_CMP8_1ST                        (HWD_DLY_LDR_BASE + 0x9bc)
#define HWD_BPI_CMP8_2ND                        (HWD_DLY_LDR_BASE + 0x9c0)
#define HWD_BPI_CMP9_1ST                        (HWD_DLY_LDR_BASE + 0x9c4)
#define HWD_BPI_CMP9_2ND                        (HWD_DLY_LDR_BASE + 0x9c8)
#define HWD_BPI_CMP10_1ST                       (HWD_DLY_LDR_BASE + 0x9cc)
#define HWD_BPI_CMP10_2ND                       (HWD_DLY_LDR_BASE + 0x9d0)
#define HWD_BPI_CMP11_1ST                       (HWD_DLY_LDR_BASE + 0x9d4)
#define HWD_BPI_CMP11_2ND                       (HWD_DLY_LDR_BASE + 0x9d8)
#define HWD_BPI_CMP12_1ST                       (HWD_DLY_LDR_BASE + 0x9dc)
#define HWD_BPI_CMP12_2ND                       (HWD_DLY_LDR_BASE + 0x9e0)
#define HWD_BPI_CMP13_1ST                       (HWD_DLY_LDR_BASE + 0x9e4)
#define HWD_BPI_CMP13_2ND                       (HWD_DLY_LDR_BASE + 0x9e8)
#define HWD_BPI_CMP14_1ST                       (HWD_DLY_LDR_BASE + 0x9ec)
#define HWD_BPI_CMP14_2ND                       (HWD_DLY_LDR_BASE + 0x9f0)
#define HWD_BPI_CMP15_1ST                       (HWD_DLY_LDR_BASE + 0x9f4)
#define HWD_BPI_CMP15_2ND                       (HWD_DLY_LDR_BASE + 0x9f8)
#define HWD_BPI_CMP16_1ST                       (HWD_DLY_LDR_BASE + 0x9fc)
#define HWD_BPI_CMP16_2ND                       (HWD_DLY_LDR_BASE + 0xa00)
#define HWD_BPI_CMP17_1ST                       (HWD_DLY_LDR_BASE + 0xa04)
#define HWD_BPI_CMP17_2ND                       (HWD_DLY_LDR_BASE + 0xa08)
#define HWD_BPI_CMP18_1ST                       (HWD_DLY_LDR_BASE + 0xa0c)
#define HWD_BPI_CMP18_2ND                       (HWD_DLY_LDR_BASE + 0xa10)
#define HWD_BPI_CMP19_1ST                       (HWD_DLY_LDR_BASE + 0xa14)
#define HWD_BPI_CMP19_2ND                       (HWD_DLY_LDR_BASE + 0xa18)
#define HWD_BPI_CMP20_1ST                       (HWD_DLY_LDR_BASE + 0xa1c)
#define HWD_BPI_CMP20_2ND                       (HWD_DLY_LDR_BASE + 0xa20)
#define HWD_BPI_CMP21_1ST                       (HWD_DLY_LDR_BASE + 0xa24)
#define HWD_BPI_CMP21_2ND                       (HWD_DLY_LDR_BASE + 0xa28)
#define HWD_BPI_CMP22_1ST                       (HWD_DLY_LDR_BASE + 0xa2c)
#define HWD_BPI_CMP22_2ND                       (HWD_DLY_LDR_BASE + 0xa30)
#define HWD_BPI_CMP23_1ST                       (HWD_DLY_LDR_BASE + 0xa34)
#define HWD_BPI_CMP23_2ND                       (HWD_DLY_LDR_BASE + 0xa38)
#define HWD_BPI_CMP24_1ST                       (HWD_DLY_LDR_BASE + 0xa3c)
#define HWD_BPI_CMP24_2ND                       (HWD_DLY_LDR_BASE + 0xa40)
#define HWD_BPI_CMP25_1ST                       (HWD_DLY_LDR_BASE + 0xa44)
#define HWD_BPI_CMP25_2ND                       (HWD_DLY_LDR_BASE + 0xa48)
#define HWD_BPI_CMP26_1ST                       (HWD_DLY_LDR_BASE + 0xa4c)
#define HWD_BPI_CMP26_2ND                       (HWD_DLY_LDR_BASE + 0xa50)
#define HWD_BPI_CMP27_1ST                       (HWD_DLY_LDR_BASE + 0xa54)
#define HWD_BPI_CMP27_2ND                       (HWD_DLY_LDR_BASE + 0xa58)
#define HWD_BPI_CMP28_1ST                       (HWD_DLY_LDR_BASE + 0xa5c)
#define HWD_BPI_CMP28_2ND                       (HWD_DLY_LDR_BASE + 0xa60)
#define HWD_BPI_CMP29_1ST                       (HWD_DLY_LDR_BASE + 0xa64)
#define HWD_BPI_CMP29_2ND                       (HWD_DLY_LDR_BASE + 0xa68)
#define HWD_BPI_CMP30_1ST                       (HWD_DLY_LDR_BASE + 0xa6c)
#define HWD_BPI_CMP30_2ND                       (HWD_DLY_LDR_BASE + 0xa70)
#define HWD_BPICFT_CMP0_1ST                     (HWD_DLY_LDR_BASE + 0xa74)
#define HWD_BPICFT_CMP0_2ND                     (HWD_DLY_LDR_BASE + 0xa78)
#define HWD_BPICFT_CMP1_1ST                     (HWD_DLY_LDR_BASE + 0xa7c)
#define HWD_BPICFT_CMP1_2ND                     (HWD_DLY_LDR_BASE + 0xa80)
#define HWD_BPI_CMP32_1ST                       (HWD_DLY_LDR_BASE + 0xa84)
#define HWD_BPI_CMP32_2ND                       (HWD_DLY_LDR_BASE + 0xa88)
#define HWD_BPI_CMP33_1ST                       (HWD_DLY_LDR_BASE + 0xa8c)
#define HWD_BPI_CMP33_2ND                       (HWD_DLY_LDR_BASE + 0xa90)
#define HWD_BPI_CMP34_1ST                       (HWD_DLY_LDR_BASE + 0xa94)
#define HWD_BPI_CMP34_2ND                       (HWD_DLY_LDR_BASE + 0xa98)
#define HWD_BPI_CMP35_1ST                       (HWD_DLY_LDR_BASE + 0xa9c)
#define HWD_BPI_CMP35_2ND                       (HWD_DLY_LDR_BASE + 0xaa0)
#define HWD_BPI_CMP36_1ST                       (HWD_DLY_LDR_BASE + 0xaa4)
#define HWD_BPI_CMP36_2ND                       (HWD_DLY_LDR_BASE + 0xaa8)
#define HWD_BPI_CMP37_1ST                       (HWD_DLY_LDR_BASE + 0xaac)
#define HWD_BPI_CMP37_2ND                       (HWD_DLY_LDR_BASE + 0xab0)
#define HWD_BPI_CMP38_1ST                       (HWD_DLY_LDR_BASE + 0xab4)
#define HWD_BPI_CMP38_2ND                       (HWD_DLY_LDR_BASE + 0xab8)
#define HWD_BPI_CMP39_1ST                       (HWD_DLY_LDR_BASE + 0xabc)
#define HWD_BPI_CMP39_2ND                       (HWD_DLY_LDR_BASE + 0xac0)
#define HWD_BPI_CMP40_1ST                       (HWD_DLY_LDR_BASE + 0xac4)
#define HWD_BPI_CMP40_2ND                       (HWD_DLY_LDR_BASE + 0xac8)
#define HWD_BPI_CMP41_1ST                       (HWD_DLY_LDR_BASE + 0xacc)
#define HWD_BPI_CMP41_2ND                       (HWD_DLY_LDR_BASE + 0xad0)
#define HWD_BPI_CMP42_1ST                       (HWD_DLY_LDR_BASE + 0xad4)
#define HWD_BPI_CMP42_2ND                       (HWD_DLY_LDR_BASE + 0xad8)
#define HWD_BPI_CMP43_1ST                       (HWD_DLY_LDR_BASE + 0xadc)
#define HWD_BPI_CMP43_2ND                       (HWD_DLY_LDR_BASE + 0xae0)
#define HWD_BPI_CMP44_1ST                       (HWD_DLY_LDR_BASE + 0xae4)
#define HWD_BPI_CMP44_2ND                       (HWD_DLY_LDR_BASE + 0xae8)
#define HWD_BPI_CMP45_1ST                       (HWD_DLY_LDR_BASE + 0xaec)
#define HWD_BPI_CMP45_2ND                       (HWD_DLY_LDR_BASE + 0xaf0)
#define HWD_BPI_CMP46_1ST                       (HWD_DLY_LDR_BASE + 0xaf4)
#define HWD_BPI_CMP46_2ND                       (HWD_DLY_LDR_BASE + 0xaf8)
#define HWD_BPI_CMP47_1ST                       (HWD_DLY_LDR_BASE + 0xafc)
#define HWD_BPI_CMP47_2ND                       (HWD_DLY_LDR_BASE + 0xb00)

#define HWD_MIPI01_CMP_1ST                      (HWD_DLY_LDR_BASE + 0xb38)
#define HWD_MIPI01_CMP_2ND                      (HWD_DLY_LDR_BASE + 0xb3c)
#define HWD_MIPI02_CMP_1ST                      (HWD_DLY_LDR_BASE + 0xb40)
#define HWD_MIPI02_CMP_2ND                      (HWD_DLY_LDR_BASE + 0xb44)
#define HWD_MIPI11_CMP_1ST                      (HWD_DLY_LDR_BASE + 0xb48)
#define HWD_MIPI11_CMP_2ND                      (HWD_DLY_LDR_BASE + 0xb4c)
#define HWD_MIPI12_CMP_1ST                      (HWD_DLY_LDR_BASE + 0xb50)
#define HWD_MIPI12_CMP_2ND                      (HWD_DLY_LDR_BASE + 0xb54)
#define HWD_MIPI21_CMP_1ST                      (HWD_DLY_LDR_BASE + 0xb58)
#define HWD_MIPI21_CMP_2ND                      (HWD_DLY_LDR_BASE + 0xb5c)
#define HWD_MIPI22_CMP_1ST                      (HWD_DLY_LDR_BASE + 0xb60)
#define HWD_MIPI22_CMP_2ND                      (HWD_DLY_LDR_BASE + 0xb64)
#define HWD_MIPI31_CMP_1ST                      (HWD_DLY_LDR_BASE + 0xb68)
#define HWD_MIPI31_CMP_2ND                      (HWD_DLY_LDR_BASE + 0xb6c)
#define HWD_MIPI32_CMP_1ST                      (HWD_DLY_LDR_BASE + 0xb70)
#define HWD_MIPI32_CMP_2ND                      (HWD_DLY_LDR_BASE + 0xb74)
#define HWD_BSI11_CMP_1ST                       (HWD_DLY_LDR_BASE + 0xb30)
#define HWD_BSI11_CMP_2ND                       (HWD_DLY_LDR_BASE + 0xb34)
#define HWD_BSI12_CMP_1ST                       (HWD_DLY_LDR_BASE + 0xbac)
#define HWD_BSI12_CMP_2ND                       (HWD_DLY_LDR_BASE + 0xbb0)

#define HWD_TXNCO_CMP_1ST                       (HWD_DLY_LDR_BASE + 0xb84)
#define HWD_TXNCO_CMP_2ND                       (HWD_DLY_LDR_BASE + 0xb88)

#define HWD_DDPC_CMP_1ST                        (HWD_DLY_LDR_BASE + 0xb8c)
#define HWD_DDPC_CMP_2ND                        (HWD_DLY_LDR_BASE + 0xb90)

#define HWD_DISTXONCP                           (HWD_DLY_LDR_BASE + 0x8d4)
#define HWD_DISPDMCP                            (HWD_DLY_LDR_BASE + 0x844)
#define HWD_DISTXCP                             (HWD_DLY_LDR_BASE + 0x910)
#define HWD_DISPMICCP                           (HWD_DLY_LDR_BASE + 0x938)
#define HWD_DISABPI0CP                          (HWD_DLY_LDR_BASE + 0xb20)
#define HWD_DISABPI1CP                          (HWD_DLY_LDR_BASE + 0xb24)
#define HWD_DISABPICFTCP                        (HWD_DLY_LDR_BASE + 0xb28)
#define HWD_DISABPI2CP                          (HWD_DLY_LDR_BASE + 0xb2c)
#define HWD_DISABSIMIPICP                       (HWD_DLY_LDR_BASE + 0xb80)
#define HWD_DISNCODDPCCP                        (HWD_DLY_LDR_BASE + 0xb98)

#define HWD_TXONDLYMASK                         (HWD_DLY_LDR_BASE + 0x860)
#define HWD_PDM3DLYMASK                         (HWD_DLY_LDR_BASE + 0x8bc)
#define HWD_TXMASK                              (HWD_DLY_LDR_BASE + 0x90c)
#define HWD_PMICMASK                            (HWD_DLY_LDR_BASE + 0x934)
#define HWD_BPI_MASK0                           (HWD_DLY_LDR_BASE + 0xb04)
#define HWD_BPI_MASK1                           (HWD_DLY_LDR_BASE + 0xb08)
#define HWD_BPI_MASK2                           (HWD_DLY_LDR_BASE + 0xb0c)
#define HWD_BPI_MASK3                           (HWD_DLY_LDR_BASE + 0xb10)
#define HWD_BPICFT_MASK                         (HWD_DLY_LDR_BASE + 0xb14)
#define HWD_BPI_MASK4                           (HWD_DLY_LDR_BASE + 0xb18)
#define HWD_BPI_MASK5                           (HWD_DLY_LDR_BASE + 0xb1c)
#define HWD_BSI1_MASK                           (HWD_DLY_LDR_BASE + 0xb78)
#define HWD_MIPI_MASK                           (HWD_DLY_LDR_BASE + 0xb7c)
#define HWD_NCODDPC_MASK                        (HWD_DLY_LDR_BASE + 0xb94)

#define HWD_TXONDLYMODE                         (HWD_DLY_LDR_BASE + 0x838)
#define HWD_TXONDLYMODEBPI0CP                   (HWD_DLY_LDR_BASE + 0xb9c)
#define HWD_TXONDLYMODEBPI1CP                   (HWD_DLY_LDR_BASE + 0xba0)
#define HWD_TXONDLYMODEBPICFTCP                 (HWD_DLY_LDR_BASE + 0xba4)
#define HWD_TXONDLYMODEBPI2CP                   (HWD_DLY_LDR_BASE + 0xba8)


/******************************************
    Interrupt register field definition
******************************************/
/* HWD_CP_ISR7_L & HWD_CP_ISR7_H - Interrupt bit positions */
#define HWD_INT_BSI1        (1 << 0)
#define HWD_INT_MIPI0       (1 << 1)
#define HWD_INT_MIPI1       (1 << 2)
#define HWD_INT_MIPI2       (1 << 3)
#define HWD_INT_MIPI3       (1 << 4)
#define HWD_INT_BSI_ALL     (HWD_INT_BSI1 | HWD_INT_MIPI0 | HWD_INT_MIPI1 \
                           | HWD_INT_MIPI2 | HWD_INT_MIPI3)
#define HWD_INT_BPI_CFT     (1 << 5)


/******************************************
    Interrupt register field definition
******************************************/
#define CONFLICT_TIME_LATCH_EN              (0x0b8501a8)
#define CONFLICT_STATUS                     (0x0b8501ac)
#define RF_ERROR_START_CNT_0_1X             (0x0b8501b0)
#define RF_ERROR_START_CNT_1_1X             (0x0b8501b4)
#define RF_ERROR_END_CNT_0_1X               (0x0b8501b8)
#define RF_ERROR_END_CNT_1_1X               (0x0b8501bc)
#define RF_ERROR_START_CNT_0_DO             (0x0b8501c0)
#define RF_ERROR_START_CNT_1_DO             (0x0b8501c4)
#define RF_ERROR_END_CNT_0_DO               (0x0b8501c8)
#define RF_ERROR_END_CNT_1_DO               (0x0b8501cc)

