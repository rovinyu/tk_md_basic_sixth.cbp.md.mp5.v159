/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
/*****************************************************************************

  FILE NAME: exeapi.h

  DESCRIPTION:

    This file contains function prototypes and type definitions
    for the Executive services. These services are based on
    Nucleus RTOS functions.

*****************************************************************************/

#ifndef EXEAPI_H
#define EXEAPI_H

#ifndef MTK_PLT_ON_PC
#include "cp_nucleus.h"
#endif
#include "sysdefs.h"

/*--------------------------------------------------------------------
* Define Exe constants
*--------------------------------------------------------------------*/

/* Number of Msec per timer tick */
#define EXE_TIMER_TICK         10

/*--------------------------------------------------------------------
* Define Exe macros
*--------------------------------------------------------------------*/

/* Cal timer ticks from Msec */
#define ExeCalMsec(NumMsec) (((NumMsec) / EXE_TIMER_TICK) + 1)

/*--------------------------------------------------------------------
* Define Exe typedefs
*--------------------------------------------------------------------*/

/*---------------------------------------------------------------------
* The enumerated type ExeTaskIdT is used by application tasks
* when they want to call a scheduler service and they need to
* know the task id of a particular task. This enumerated type
* also contains the number of overall tasks that the scheduler
* will deal with. The order of task ids is important and must
* match the order of task control blocks defined in ExeTaskCbT.
* New task ids should be added to the end of the list before
* the EXE_NUM_TASKS entry.
*---------------------------------------------------------------------*/

#include "exetaskid.h"

/* Remember to also update the "CP Task Id" enum in mon_cp_msg.txt and
 * "CP Thread Ids" enum in mon_spy.txt */

/*------------------------------------------------------------------------
* Define starting range for LISR & HISR thread Ids. If these defines are
* changed they must also be changed in exeapi.inc
*------------------------------------------------------------------------*/

#define EXE_LISR_START_THREAD_ID      0x48
#define EXE_HISR_START_THREAD_ID      0x90
/* Generic threads IDs  */
#define EXE_HISRP1_THREAD_ID          0xF7
#define EXE_HISRP0_THREAD_ID          0xF8
#define EXE_USR_THREAD_ID             0xF9
#define EXE_SYS_THREAD_ID             0xFA
#define EXE_ABORT_THREAD_ID           0xFB
#define EXE_UNDEF_THREAD_ID           0xFC
#define EXE_FIQ_THREAD_ID             0xFD
#define EXE_IRQ_THREAD_ID             0xFE
#define EXE_SCHEDULER_THREAD_ID       0xFF
#define EXE_SYS_EXCEPTION_THREAD_ID   EXE_FIQ_THREAD_ID

/* Remember to also update the "CP Thread Ids" enum in mon_spy.txt */

/*---------------------------------------------------------------------
* The enumerated type ExeLisrIdT is used by LISR to associated a thread
* ID with a LISR. If these defines are changed they must also be changed
* in exeapi.inc
*---------------------------------------------------------------------*/

typedef enum
{
   EXE_TIMER_LISR_THREAD_ID            = EXE_LISR_START_THREAD_ID,
   EXE_UART0_LISR_THREAD_ID            = EXE_LISR_START_THREAD_ID + 0x01,
   EXE_UART1_LISR_THREAD_ID            = EXE_LISR_START_THREAD_ID + 0x02,
   EXE_UART2_LISR_THREAD_ID            = EXE_LISR_START_THREAD_ID + 0x03,
   EXE_CTS_LISR_THREAD_ID              = EXE_LISR_START_THREAD_ID + 0x04,
   EXE_DSPM_CTL_MBOX_LISR_THREAD_ID    = EXE_LISR_START_THREAD_ID + 0x05,
   EXE_DSPM_DBUF_MBOX_LISR_THREAD_ID   = EXE_LISR_START_THREAD_ID + 0x06,
#ifdef MTK_PLT_AUDIO
   EXE_AUDIO_LISR_THREAD_ID            = EXE_LISR_START_THREAD_ID + 0x07,
#if defined(__CVSD_CODEC_SUPPORT__)
   EXE_BT_LISR_THREAD_ID               = EXE_LISR_START_THREAD_ID + 0x08,
#endif
#else
   EXE_DSPV_MBOX_LISR_THREAD_ID        = EXE_LISR_START_THREAD_ID + 0x07,
   EXE_DSPV_F_MBOX_LISR_THREAD_ID      = EXE_LISR_START_THREAD_ID + 0x08,
#endif
#if defined(SYS_OPTION_CCIRQ_PCCIF)||defined (SYS_OPTION_CCIRQ_UT_ON_D1)
   EXE_PCCIF_LISR_THREAD_ID            = EXE_LISR_START_THREAD_ID + 0x0A,
#endif
   EXE_RESYNC_LISR_THREAD_ID           = EXE_LISR_START_THREAD_ID + 0x0B,
   EXE_SLEEP_OVER_LISR_THREAD_ID       = EXE_LISR_START_THREAD_ID + 0x0C,
   EXE_SSTIMER_LISR_THREAD_ID          = EXE_LISR_START_THREAD_ID + 0x0D,
   EXE_RNGR_LISR_THREAD_ID             = EXE_LISR_START_THREAD_ID + 0x0F,
#ifdef SYS_OPTION_IOP_CCIF
   EXE_CCIFRX_LISR_THREAD_ID           = EXE_LISR_START_THREAD_ID + 0x10, 
#endif
   EXE_SYS_TIME_LISR_THREAD_ID         = EXE_LISR_START_THREAD_ID + 0x11,
   EXE_GPINT7_LISR_THREAD_ID           = EXE_LISR_START_THREAD_ID + 0x12,
   EXE_UIMRX_LISR_THREAD_ID            = EXE_LISR_START_THREAD_ID + 0x13,
   EXE_UIMTX_LISR_THREAD_ID            = EXE_LISR_START_THREAD_ID + 0x14,
   EXE_MUXPDU_LISR_THREAD_ID           = EXE_LISR_START_THREAD_ID + 0x15,
   EXE_MUXPDU_FIQISR_THREAD_ID         = EXE_LISR_START_THREAD_ID + 0x16,
   EXE_RINGER_LISR_THREAD_ID           = EXE_LISR_START_THREAD_ID + 0x19,
   EXE_RINGER_FIQISR_THREAD_ID         = EXE_LISR_START_THREAD_ID + 0x1A,
   EXE_GPINT3_LISR_THREAD_ID           = EXE_LISR_START_THREAD_ID + 0x1E,
   EXE_M2CFM_FIQISR_THREAD_ID          = EXE_LISR_START_THREAD_ID + 0x21,
   EXE_M2CFM_LISR_THREAD_ID            = EXE_LISR_START_THREAD_ID + 0x22,
#ifndef MTK_PLT_AUDIO
   EXE_SHARED_MEM_LISR_THREAD_ID       = EXE_LISR_START_THREAD_ID + 0x23,
#endif
#ifdef SYS_OPTION_EVDO
   EXE_DO_SRCH_LISR_THREAD_ID          = EXE_LISR_START_THREAD_ID + 0x24,
#endif
   EXE_SYS_DO_TIME_LISR_THREAD_ID      = EXE_LISR_START_THREAD_ID + 0x25,    /* FOR_DO_SYSTEM_TIME */
#ifdef SYS_OPTION_EVDO
   EXE_DO_SLOT_LISR_THREAD_ID          = EXE_LISR_START_THREAD_ID + 0x26,
   EXE_DO_HALF_SLOT_LISR_THREAD_ID     = EXE_LISR_START_THREAD_ID + 0x27,
   EXE_DO_FN_SLOTFOUND_LISR_THREAD_ID  = EXE_LISR_START_THREAD_ID + 0x28,
   EXE_DO_CELLSWRDY_LISR_THREAD_ID     = EXE_LISR_START_THREAD_ID + 0x29,
   EXE_DMA_MDM_RX_LISR_THREAD_ID       = EXE_LISR_START_THREAD_ID + 0x2A,
#endif
   EXE_D0_PPP_HA_DEC0_LISR_THREAD_ID   = EXE_LISR_START_THREAD_ID + 0x2B,
   EXE_D0_PPP_HA_DEC1_LISR_THREAD_ID   = EXE_LISR_START_THREAD_ID + 0x2C,
   EXE_D0_PPP_HA_ENC0_LISR_THREAD_ID   = EXE_LISR_START_THREAD_ID + 0x2D,
   EXE_D0_PPP_HA_ENC1_LISR_THREAD_ID   = EXE_LISR_START_THREAD_ID + 0x2E,
   EXE_GENERIC_DMA_LISR_THREAD_ID      = EXE_LISR_START_THREAD_ID + 0x2F,
#ifdef SYS_OPTION_EVDO
   EXE_DMA_MDM_TX_LISR_THREAD_ID       = EXE_LISR_START_THREAD_ID + 0x30,
#endif
   EXE_SDIO_S_LISR_THREAD_ID           = EXE_LISR_START_THREAD_ID + 0x31,
   
   EXE_GPINT_SRC1_LISR_THREAD_ID       = EXE_LISR_START_THREAD_ID + 0x32,
   EXE_GPINT_SRC3_LISR_THREAD_ID       = EXE_LISR_START_THREAD_ID + 0x33,
#ifdef SYS_OPTION_AP_INTERFACE
   EXE_GPIO_AP_RDY_LISR_THREAD_ID      = EXE_LISR_START_THREAD_ID + 0x34,
#endif


#ifdef SYS_OPTION_MPU
   EXE_MPU_INT1_LISR_THREAD_ID         = EXE_LISR_START_THREAD_ID + 0x35,
#endif

   EXE_SLEEP_OVER_DO_LISR_THREAD_ID    = EXE_LISR_START_THREAD_ID + 0x36,

   EXE_UIM2RX_LISR_THREAD_ID            = EXE_LISR_START_THREAD_ID + 0x3F,
   EXE_UIM2TX_LISR_THREAD_ID            = EXE_LISR_START_THREAD_ID + 0x40,

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
   EXE_PMICCTRLINTF_LISR_THREAD_ID      = EXE_LISR_START_THREAD_ID + 0x41,
#endif

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
   EXT_BPICFT_LISR_THREAD_ID           = EXE_LISR_START_THREAD_ID + 0x38,
   EXE_BSI_LISR_THREAD_ID              = EXE_LISR_START_THREAD_ID + 0x42,
#endif /* MTK_CBP */

#if defined(SYS_OPTION_CCIRQ_CCIRQ)
   EXE_CCIRQ_L1_LISR_THREAD_ID         = EXE_LISR_START_THREAD_ID + 0x43,
   EXE_CCIRQ_PS_LISR_THREAD_ID         = EXE_LISR_START_THREAD_ID + 0x44,
#endif /*SYS_OPTION_CCIRQ_CCIRQ*/

#if (SYS_BOARD >= SB_JADE)
   EXE_FRC1X_SYNC_LISR_THREAD_ID       = EXE_LISR_START_THREAD_ID + 0x45,
   EXE_FRCDO_SYNC_LISR_THREAD_ID       = EXE_LISR_START_THREAD_ID + 0x46,
#endif

#if defined (MTK_DEV_C2K_IRAT) && defined (MTK_DEV_C2K_SRLTE_L1)
   EXE_C2K_INT_LISR_THREAD_ID          = EXE_LISR_START_THREAD_ID + 0x47,
#endif

   /* MUST BE LAST IN LIST!! */
   EXE_LAST_LISR_THREAD_ID             = EXE_LISR_START_THREAD_ID + 0x48

 } ExeLisrIdT; /* Remember to also update the "CP Thread Ids" enum in mon_spy.txt */

/*---------------------------------------------------------------------
* The enumerated type ExeHisrIdT is used by application tasks when they
* create an HISR and a thread id needs to associated with the HISR.
* If these defines are changed they must also be changed in exeapi.inc
*---------------------------------------------------------------------*/

typedef enum
{
   EXE_TIMER_HISR_THREAD_ID            = EXE_HISR_START_THREAD_ID,
   EXE_UART0_HISR_THREAD_ID            = EXE_HISR_START_THREAD_ID + 0x01,
   EXE_UART1_HISR_THREAD_ID            = EXE_HISR_START_THREAD_ID + 0x02,
   EXE_UART2_HISR_THREAD_ID            = EXE_HISR_START_THREAD_ID + 0x03,
   EXE_DSPM_CTL_MBOX_HISR_THREAD_ID    = EXE_HISR_START_THREAD_ID + 0x04,
   EXE_DSPM_DBUF_MBOX_HISR_THREAD_ID   = EXE_HISR_START_THREAD_ID + 0x05,
#ifndef MTK_PLT_AUDIO
   EXE_DSPV_MBOX_HISR_THREAD_ID        = EXE_HISR_START_THREAD_ID + 0x06,
#endif
   EXE_SYS_TIME_HISR_THREAD_ID         = EXE_HISR_START_THREAD_ID + 0x07,
   EXE_CTS0_HISR_THREAD_ID             = EXE_HISR_START_THREAD_ID + 0x08,
   EXE_CTS1_HISR_THREAD_ID             = EXE_HISR_START_THREAD_ID + 0x09,
   EXE_CTS2_HISR_THREAD_ID             = EXE_HISR_START_THREAD_ID + 0x0A,
   EXE_CTS3_HISR_THREAD_ID             = EXE_HISR_START_THREAD_ID + 0x0B,
   EXE_CCIRQ_L1_HISR_THREAD_ID0        = EXE_HISR_START_THREAD_ID + 0x0C,
   EXE_SLEEP_OVER_HISR_1X_THREAD_ID    = EXE_HISR_START_THREAD_ID + 0x0D,
   EXE_SPAGE_RESYNC_HISR_THREAD_ID     = EXE_HISR_START_THREAD_ID + 0x0E,
   EXE_SSTIMER_HISR_THREAD_ID          = EXE_HISR_START_THREAD_ID + 0x0F,
   EXE_RNGR_HISR_THREAD_ID             = EXE_HISR_START_THREAD_ID + 0x10,
   EXE_CCIRQ_L1_HISR_THREAD_ID1        = EXE_HISR_START_THREAD_ID + 0x11,
   EXE_SYS_EVENT_HISR_THREAD_ID        = EXE_HISR_START_THREAD_ID + 0x12,
   EXE_MUXPDU_HISR_THREAD_ID           = EXE_HISR_START_THREAD_ID + 0x13,
   EXE_CCIRQ_L1_HISR_THREAD_ID2        = EXE_HISR_START_THREAD_ID + 0x14,
   EXE_RINGER_HISR_THREAD_ID           = EXE_HISR_START_THREAD_ID + 0x15,
   EXE_CCIRQ_L1_HISR_THREAD_ID3        = EXE_HISR_START_THREAD_ID + 0x16,
   EXE_CCIRQ_L1_HISR_THREAD_ID4        = EXE_HISR_START_THREAD_ID + 0x17,
   EXE_MMC_HISR_THREAD_ID              = EXE_HISR_START_THREAD_ID + 0x18,
   EXE_UIM_CMD_HISR_THREAD_ID          = EXE_HISR_START_THREAD_ID + 0x19,
   EXE_CCIRQ_L1_HISR_THREAD_ID5        = EXE_HISR_START_THREAD_ID + 0x1A,
   EXE_CCIRQ_L1_HISR_THREAD_ID6        = EXE_HISR_START_THREAD_ID + 0x1B,
   EXE_CCIRQ_L1_HISR_THREAD_ID7        = EXE_HISR_START_THREAD_ID + 0x1C,
#if defined (MTK_DEV_C2K_IRAT) && defined (MTK_DEV_C2K_SRLTE_L1)
   EXE_MPA_RELEASE_REQ_HISR_THREAD_ID       = EXE_HISR_START_THREAD_ID + 0x1D,
#endif
   EXE_M2CFM_HISR_THREAD_ID            = EXE_HISR_START_THREAD_ID + 0x1E,
   EXE_CCIRQ_PS_HISR_THREAD_ID0        = EXE_HISR_START_THREAD_ID + 0x1F,
#ifndef MTK_PLT_AUDIO
   EXE_SHARED_MEM_HISR_THREAD_ID       = EXE_HISR_START_THREAD_ID + 0x20,
#endif
#ifdef SYS_OPTION_EVDO
   EXE_DO_SRCH_HISR_THREAD_ID          = EXE_HISR_START_THREAD_ID + 0x21,
   EXE_SYS_DO_TIME_HISR_THREAD_ID      = EXE_HISR_START_THREAD_ID + 0x22, /* NOT USED! */
   EXE_RMC_TRACE_HISR_THREAD_ID        = EXE_HISR_START_THREAD_ID + 0x23, /* NOT USED! */
   EXE_DO_FN_SLOTFOUND_HISR_THREAD_ID  = EXE_HISR_START_THREAD_ID + 0x24,
   EXE_DO_CELLSWRDY_HISR_THREAD_ID     = EXE_HISR_START_THREAD_ID + 0x25,
   EXE_DMA_MDM_RX_HISR_THREAD_ID       = EXE_HISR_START_THREAD_ID + 0x26,
#endif
   EXE_D0_PPP_HA_DEC0_HISR_THREAD_ID   = EXE_HISR_START_THREAD_ID + 0x27,
   EXE_D0_PPP_HA_DEC1_HISR_THREAD_ID   = EXE_HISR_START_THREAD_ID + 0x28,
   EXE_D0_PPP_HA_ENC0_HISR_THREAD_ID   = EXE_HISR_START_THREAD_ID + 0x29,
   EXE_D0_PPP_HA_ENC1_HISR_THREAD_ID   = EXE_HISR_START_THREAD_ID + 0x2A,
   EXE_GENERIC_DMA_HISR_THREAD_ID      = EXE_HISR_START_THREAD_ID + 0x2B,
   EXE_CCIRQ_PS_HISR_THREAD_ID1        = EXE_HISR_START_THREAD_ID + 0x2C,
   EXE_CCIRQ_PS_HISR_THREAD_ID2        = EXE_HISR_START_THREAD_ID + 0x2D,
#if defined (MTK_DEV_C2K_IRAT) && defined (MTK_DEV_C2K_SRLTE_L1)
   EXE_RTBA_PROC_XL1_CMD_HISR_THREAD_ID     = EXE_HISR_START_THREAD_ID + 0x2E,
#endif
   EXE_CCIRQ_PS_HISR_THREAD_ID3        = EXE_HISR_START_THREAD_ID + 0x2F,
#if defined (MTK_DEV_C2K_IRAT) && defined (MTK_DEV_C2K_SRLTE_L1)
   EXE_C2K_INT_HISR_THREAD_ID          = EXE_HISR_START_THREAD_ID +0x30,
#endif
   EXE_MMAPPS_HISR_THREAD_ID           = EXE_HISR_START_THREAD_ID + 0x31,
#ifdef SYS_OPTION_EVDO
   EXE_MPA_PREEMPT_REQ_HISR_THREAD_ID  = EXE_HISR_START_THREAD_ID + 0x32,
   EXE_RCP_RMC_RF_DONE_HISR_THREAD_ID  = EXE_HISR_START_THREAD_ID + 0x33,
#endif
#ifdef SYS_OPTION_CCISM_2SCP
   EXE_CCISM_RX_HISR_THREAD_ID         = EXE_HISR_START_THREAD_ID +0x34,
#endif

   EXE_SDIO_S_HISR_THREAD_ID           = EXE_HISR_START_THREAD_ID + 0x35,
   
   EXE_GPINT0_HISR_THREAD_ID           = EXE_HISR_START_THREAD_ID + 0x36,
   EXE_GPINT1_HISR_THREAD_ID           = EXE_HISR_START_THREAD_ID + 0x37,
   EXE_GPINT2_HISR_THREAD_ID           = EXE_HISR_START_THREAD_ID + 0x38,
   EXE_GPINT3_HISR_THREAD_ID           = EXE_HISR_START_THREAD_ID + 0x39,
   EXE_GPINT4_HISR_THREAD_ID           = EXE_HISR_START_THREAD_ID + 0x3A,
   EXE_GPINT5_HISR_THREAD_ID           = EXE_HISR_START_THREAD_ID + 0x3B,
   EXE_GPINT6_HISR_THREAD_ID           = EXE_HISR_START_THREAD_ID + 0x3C,
   EXE_GPINT7_HISR_THREAD_ID           = EXE_HISR_START_THREAD_ID + 0x3D,
   EXE_GPINT8_HISR_THREAD_ID           = EXE_HISR_START_THREAD_ID + 0x3E,
   EXE_GPINT9_HISR_THREAD_ID           = EXE_HISR_START_THREAD_ID + 0x3F,
   EXE_GPINT10_HISR_THREAD_ID          = EXE_HISR_START_THREAD_ID + 0x40,
   EXE_GPINT11_HISR_THREAD_ID          = EXE_HISR_START_THREAD_ID + 0x41,
#ifdef SYS_OPTION_AP_INTERFACE
   EXE_GPIO_AP_RDY_HISR_THREAD_ID      = EXE_HISR_START_THREAD_ID + 0x42,
#endif   
   EXE_SBC_ENCODE_HISR_THREAD_ID       = EXE_HISR_START_THREAD_ID + 0x43,
   EXE_AVSYNC_HISR_THREAD_ID           = EXE_HISR_START_THREAD_ID + 0x44,
   EXE_MXSSPI_HISR_THREAD_ID           = EXE_HISR_START_THREAD_ID + 0x45,

   EXE_GPINT12_HISR_THREAD_ID          = EXE_HISR_START_THREAD_ID + 0x46,
   EXE_GPINT13_HISR_THREAD_ID          = EXE_HISR_START_THREAD_ID + 0x47,
   EXE_GPINT14_HISR_THREAD_ID          = EXE_HISR_START_THREAD_ID + 0x48,
   EXE_GPINT15_HISR_THREAD_ID          = EXE_HISR_START_THREAD_ID + 0x49,
#if defined (MTK_DEV_C2K_IRAT) && defined (MTK_DEV_C2K_SRLTE_L1)
   EXE_RTBA_PROC_EVL1_CMD_HISR_THREAD_ID    = EXE_HISR_START_THREAD_ID + 0x4A,
#endif
   EXE_DMA_HISR_THREAD_ID              = EXE_HISR_START_THREAD_ID + 0x4E,
   EXE_MP4_HISR_THREAD_ID              = EXE_HISR_START_THREAD_ID + 0x51,
   EXE_AVC_HISR_THREAD_ID              = EXE_HISR_START_THREAD_ID + 0x52,
   EXE_SLEEP_OVER_HISR_DO_THREAD_ID    = EXE_HISR_START_THREAD_ID + 0x53,

#ifdef SYS_OPTION_DUAL_CARDS          
   EXE_UIM2_CMD_HISR_THREAD_ID         = EXE_HISR_START_THREAD_ID + 0x57,
#endif
#if defined(MTK_PLT_AUDIO)
   EXE_SPHAUDIO_HISR_THREAD_ID         = EXE_HISR_START_THREAD_ID + 0x58,
#endif // defined(MTK_PLT_AUDIO)

#ifdef SYS_OPTION_CCIRQ_PCCIF
#ifdef SYS_OPTION_CCIRQ_PCCIF_UT_PRI
   EXE_PCCIF_HISR_THREAD_ID0           = EXE_HISR_START_THREAD_ID + 0x59,
   EXE_PCCIF_HISR_THREAD_ID1           = EXE_HISR_START_THREAD_ID + 0x5A,
#else
   EXE_PCCIF_HISR_THREAD_ID            = EXE_HISR_START_THREAD_ID + 0x59,
#endif
#endif
#ifdef SYS_OPTION_IOP_CCIF
   EXE_CCIF_REV_DATA_HISR_THREAD_ID    = EXE_HISR_START_THREAD_ID + 0x5B,
   EXE_CCIF_FWD_DMA_HISR_THREAD_ID     = EXE_HISR_START_THREAD_ID + 0x5C,
   EXE_CCIF_REV_DMA_HISR_THREAD_ID     = EXE_HISR_START_THREAD_ID + 0x5D,   
#endif

   /* MUST BE LAST IN LIST!! */
   EXE_LAST_HISR_THREAD_ID             = EXE_HISR_START_THREAD_ID + 0x5E /* NOTE: 0xEE&0xEF are reserved for HwdProfiling */
} ExeHisrIdT;

/*----------------------------------------------------------------------
* The following typedef defines the 3 event types (signal, mail and
* time) and the four mailboxes and twenty task signal flags. An application
* task can use these to determine which particular event, message or
* signal flag or timeout was set to cause their task to be scheduled
* after calling ExeWaitEvent.
*----------------------------------------------------------------------*/

typedef enum
{
   EXE_MESSAGE_TYPE  =       0x00000001,   /* Event types */
   EXE_SIGNAL_TYPE   =       0x00000002,
   EXE_TIMEOUT_TYPE  =       0x00000004,

   EXE_MAILBOX_1     =       0x00000008,   /* Mailbox types */
   EXE_MAILBOX_2     =       0x00000010,
   EXE_MAILBOX_3     =       0x00000020,
   EXE_MAILBOX_4     =       0x00000040,
   EXE_MAILBOX_5     =       0x00000080,
   EXE_MAILBOX_ALL   =       0x000000F8,   /* Combination of all 5 mailbox bits */

   EXE_SIGNAL_1      =       0x00000100,   /* Signal types */
   EXE_SIGNAL_2      =       0x00000200,
   EXE_SIGNAL_3      =       0x00000400,
   EXE_SIGNAL_4      =       0x00000800,
   EXE_SIGNAL_5      =       0x00001000,
   EXE_SIGNAL_6      =       0x00002000,
   EXE_SIGNAL_7      =       0x00004000,
   EXE_SIGNAL_8      =       0x00008000,
   EXE_SIGNAL_9      =       0x00010000,
   EXE_SIGNAL_10     =       0x00020000,
   EXE_SIGNAL_11     =       0x00040000,
   EXE_SIGNAL_12     =       0x00080000,
   EXE_SIGNAL_13     =       0x00100000,
   EXE_SIGNAL_14     =       0x00200000,
   EXE_SIGNAL_15     =       0x00400000,
   EXE_SIGNAL_16     =       0x00800000,
   EXE_SIGNAL_17     =       0x01000000,
   EXE_SIGNAL_18     =       0x02000000,
   EXE_SIGNAL_19     =       0x04000000,
   EXE_SIGNAL_20     =       0x08000000,
   EXE_SIGNAL_21     =       0x10000000,
   EXE_SIGNAL_22     =       0x20000000,
   EXE_SIGNAL_23     =       0x40000000,
   EXE_SIGNAL_24     =      (int32)0x80000000,     /* typecast to int to avoid compiler warnings */
   EXE_SIGNAL_ALL    =       (int32)0xFFFFFF00      /* Combination of all 24 signal bits */
} ExeEventWaitT;

#define EXE_TASK_NULL_ID    0
#define EXE_SIGNAL_NULL     0
/* Define Signal types of event wait */
#define EXE_SIGNAL_TRUE     (EXE_SIGNAL_ALL)
#define EXE_SIGNAL_FALSE    (~EXE_SIGNAL_TRUE & EXE_SIGNAL_TRUE)

/* Define timeout type of event wait */
#define EXE_TIMEOUT_FALSE  NU_SUSPEND

/* Define constant SemaphoreGet Timeout Values */
#define EXE_SEM_WAIT_FOREVER  NU_SUSPEND
#define EXE_SEM_NO_WAIT       0

/* Define SemaphoreGet Return Code Types
 *    The "ExeSemaphoreGet()" function will only return 1 of 2 codes: _SUCCESS or _TIMEOUT.
 *    It will signal a Nucleus error and NOT return to the caller if:
 *       a) the timeout value is EXE_SEM_WAIT_FOREVER and return value from Nucleus
 *          is not NU_SUCCESS, or
 *       b) the return value from Nucleus is anything other than NU_SUCCESS or NU_TIMEOUT.
 *    These 2 return codes MUST be checked against by the application tasks.
 *    Application tasks MUST call "ExeFaultSet(Status)" if "Status", the
 *       return code from "ExeSemaphoreGet()", is EXE_SEM_TIMEOUT and
 *       corrective actions cannot be taken.
 */
#ifdef MTK_PLT_ON_PC
#define EXE_SEM_SUCCESS       0     /*   0 */
#define EXE_SEM_TIMEOUT       -50     /* -50 */
#else
#define EXE_SEM_SUCCESS       NU_SUCCESS     /*   0 */
#define EXE_SEM_TIMEOUT       NU_TIMEOUT     /* -50 */
#endif

/* Define message types of event wait */
typedef enum
{
   EXE_MESSAGE_FALSE  = 0,
   EXE_MESSAGE_TRUE   = EXE_MESSAGE_TYPE,
   EXE_MESSAGE_MBOX_1 = EXE_MAILBOX_1,
   EXE_MESSAGE_MBOX_2 = EXE_MAILBOX_2,
   EXE_MESSAGE_MBOX_3 = EXE_MAILBOX_3,
   EXE_MESSAGE_MBOX_4 = EXE_MAILBOX_4,
   EXE_MESSAGE_MBOX_5 = EXE_MAILBOX_5
}ExeMessageT;

typedef uint32 ExeSignalT;

/*--------------------------------------------------------------------
* Define the typedefs used to create Nucleus type control blocks
* for semaphores, timers, fixed memory partitions and HISRs.
*--------------------------------------------------------------------*/

#ifdef MTK_PLT_ON_PC

typedef uint32  ExeSemaphoreT;
typedef uint32   ExeBufferT;
typedef uint32  ExeTimerT;
typedef uint32  ExeHisrT;

#else /* MTK_PLT_ON_PC */

typedef NU_SEMAPHORE      ExeSemaphoreT;
typedef NU_PARTITION_POOL ExeBufferT;
typedef NU_TIMER          ExeTimerT;
typedef NU_HISR           ExeHisrT;

#endif /* MTK_PLT_ON_PC */

/* Define HISR priorities */
typedef enum
{
   EXE_HISR_PRIO_0 = 0x00,
   EXE_HISR_PRIO_1 = 0x01
} ExeHisrPrioT;

/*---------------------------------------------------------------------
* The enumerated type ExeMailboxIdT is used by application tasks
* when they want to send mail to a particular mailbox owned by
* another task.
*---------------------------------------------------------------------*/

typedef enum
{
   EXE_MAILBOX_1_ID  = 0x00,
   EXE_MAILBOX_2_ID  = 0x01,
   EXE_MAILBOX_3_ID  = 0x02,
   EXE_MAILBOX_4_ID  = 0x03,
   EXE_MAILBOX_5_ID  = 0x04,
   EXE_NUM_MAILBOX
} ExeMailboxIdT;

/*---------------------------------------------------------------------
* The enumerated type ExePreemptionT is used by application tasks
* when they want to disable/enable the preemption posture of the
* currently executing task.
*---------------------------------------------------------------------*/

#ifdef MTK_PLT_ON_PC

typedef enum
{
   EXE_PREEMPTION_DISABLE = 0x08,
   EXE_PREEMPTION_ENABLE  = 0x00
} ExePreemptionT;

#else /* MTK_PLT_ON_PC */

typedef enum
{
   EXE_PREEMPTION_DISABLE = NU_NO_PREEMPT,
   EXE_PREEMPTION_ENABLE  = NU_PREEMPT
} ExePreemptionT;

#endif /* MTK_PLT_ON_PC */

/*---------------------------------------------------------------------
* The structure ExeRspMsgT is used in the command-response paradigm
* for many of the tasks.  It is defined in the EXE API to give a single
* definition for this structure.
*---------------------------------------------------------------------*/

typedef PACKED_PREFIX struct
{
    ExeTaskIdT     TaskId;
    ExeMailboxIdT  MailboxId;
    uint32         MsgId; 
} PACKED_POSTFIX  ExeRspMsgT;

/*Don't change the struct!!*/
typedef struct 
{
    uint32     MsgId;  
    uint32     MsgLength;  
    uint32     DataPtr;
    uint32     Msg32KCnt;
} ExeMsgDataT;

typedef PACKED_PREFIX struct
{
#ifdef MTK_PLT_ON_PC
   uint8 Name[0xFF];
#else /* MTK_PLT_ON_PC */
   uint8 Name[NU_MAX_NAME];
#endif /* MTK_PLT_ON_PC */
   uint32 Size;
   uint32 FreeSpace;
   uint8* StartAddr;
   uint16 MinAllocation;
#ifdef MTK_PLT_ON_PC
   uint8* MemPoolCbPtr;
#else /* MTK_PLT_ON_PC */
   NU_MEMORY_POOL* MemPoolCbPtr;
#endif /* MTK_PLT_ON_PC */
} PACKED_POSTFIX  ExeMemoryPoolInfoT;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*****************************************************************************

  FUNCTION NAME: ExeEventWait

  DESCRIPTION:

    This routine is called by a task running under control of the exe
    scheduler when the task wishes to suspend itself until one or more
    events have occurred. This routine suspends a task on an signal flag,
    OR message OR timeout. The task remains suspended until one of the
    specified events becomes true. If a task is waiting on an signal flag
    it will be started if any of the twenty signal flags are set. If a
    task is waiting on a message there is an option to wait on a message
    in a particular mailbox or a message in any of the task's mailboxes.

    CAUTION:

    All signal flags owned by a task are cleared by this call. So a
    task must process all signals returned in this call or they will
    be lost.

  PARAMETERS:

    INPUTS:

    TaskId   - Task ID of task that wants to wait on an event
    Signal   - Specify which signals to wait on
               EXE_SIGNAL_TRUE to specify a wait on all signals or
               EXE_SIGNAL_FALSE for no signals.
    Message  - EXE_MESSAGE_FALSE for don't care
               EXE_MESSAGE_TRUE to specify a wait on a message in any mailbox
               EXE_MESSAGE_MBOX_1 to only wait on messages in mailbox 1
               EXE_MESSAGE_MBOX_2 to only wait on messages in mailbox 2
               EXE_MESSAGE_MBOX_3 to only wait on messages in mailbox 3
               EXE_MESSAGE_MBOX_4 to only wait on messages in mailbox 4
               EXE_MESSAGE_MBOX_5 to only wait on messages in mailbox 5
    Timeout  - Timeout associated with this call. EXE_TIMEOUT_FALSE
               for no timeout.

  RETURNED VALUES:

    A bit mapped flag indicating which events were set to cause this task
    to run. Refer to ExeEventWaitT for the bit map assignments.

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.

  TASKING CHANGES:

    This routine will cause the calling task to suspend.

*****************************************************************************/
#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeEventWait(TaskId, Signal, Message, Timeout) \
        __ExeEventWait(TaskId, Signal, Message, Timeout, __MODULE__, __LINE__)

extern ExeEventWaitT __ExeEventWait(ExeTaskIdT TaskId, ExeEventWaitT Signal,
                                    ExeMessageT Message, uint32 Timeout,
                                    const char *Filename, unsigned Linenumber);

#else

extern ExeEventWaitT ExeEventWait(ExeTaskIdT TaskId, ExeEventWaitT Signal,
                                  ExeMessageT Message, uint32 Timeout);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************

  FUNCTION NAME: ExeTaskWait

  DESCRIPTION:

    This routine is called by a task running under control of the exe
    scheduler when the task wishes to suspend itself for a certain number
    of timer ticks.

  PARAMETERS:

    Ticks    - Number of ticks that the task will be suspended.

  RETURNED VALUES:

    None.

  TASKING CHANGES:

    This routine will cause the calling task to suspend.

*****************************************************************************/
extern void ExeTaskWait(uint32 Ticks);

/*****************************************************************************

  FUNCTION NAME: ExeSignalSet

  DESCRIPTION:

    This routine sets a signal flag of a particular task. Each task
    has twelve signal flags. The scheduler will clear the signal
    flag when the task is activated.

  PARAMETERS:

    INPUTS:

    TaskId    - The task id of the task whose signal flag is to be set.
    SignalFlg - The number of the signal flag to set. Each task has
                twenty signal flags defined by ExeSignalT

  RETURNED VALUES:

    None

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.

  TASKING CHANGES:

    This routine will suspend the calling task if the signal set causes
    a waiting higher priority task to be ready to run.

**************************************************************************/

#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeSignalSet(TaskId, SignalFlg) \
        __ExeSignalSet(TaskId, SignalFlg, __MODULE__, __LINE__)

extern void __ExeSignalSet(ExeTaskIdT TaskId, ExeSignalT SignalFlg,
                           const char *Filename, unsigned Linenumber);

#else

extern void ExeSignalSet(ExeTaskIdT TaskId, ExeSignalT SignalFlg);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************

  FUNCTION NAME: ExeMsgSend

  DESCRIPTION:

    This routine sends a message into a particular task's mailbox.
    The format of the message is defined by the receiver of the message.
    This message is added to the end of the message list belonging to the
    mailbox specified by the mboxid parameter.

  PARAMETERS:

    INPUTS:

    TaskId       - The task id of the owner of the mailbox
    MailboxId    - The mailbox id of the mailbox receiving the message
    MsgId        - The id of message
    MsgBufferP   - The pointer to the message data buffer. Null if no data
    MsgSize      - The size of the message data in bytes. Zero if no data

  RETURNED VALUES:

    None.

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.

  TASKING CHANGES:

    This routine will suspend the calling task if the message sent
    causes a waiting higher priority task to be ready to run.

*****************************************************************************/

#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeMsgSend(TaskId, MailboxId, MsgId, MsgBufferP, MsgSize) \
        __ExeMsgSend(TaskId, MailboxId, MsgId, MsgBufferP, MsgSize, FALSE, __MODULE__, __LINE__)

extern void __ExeMsgSend(ExeTaskIdT TaskId, ExeMailboxIdT MailboxId, uint32 MsgId,
                         void *MsgBufferP, uint32 MsgSize, bool ToFront ,
                         const char *Filename, unsigned Linenumber);

#else

#define ExeMsgSend(TaskId, MailboxId, MsgId, MsgBufferP, MsgSize) \
        __ExeMsgSend(TaskId, MailboxId, MsgId, MsgBufferP, MsgSize, FALSE)

extern void __ExeMsgSend(ExeTaskIdT TaskId, ExeMailboxIdT MailboxId, uint32 MsgId,
                       void *MsgBufferP, uint32 MsgSize, bool ToFront);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

#ifdef MTK_PLT_ON_PC
/*****************************************************************************

  FUNCTION NAME: ExeMsgLog

  DESCRIPTION:

    This routine log a user defined message (with any needed parameters) in ELT for test/debug purpose.
    The format of the message is defined by the receiver of the message.

  PARAMETERS:

    INPUTS:

    TaskId       - The task id of the defined message
    MailboxId    - The mailbox id of the mailbox receiving the message
    MsgId        - The id of message
    MsgBufferP   - The pointer to the message data buffer. Null if no data
    MsgSize      - The size of the message data in bytes. Zero if no data

  RETURNED VALUES:

    None.

  ERROR HANDLING:

    No error codes are returned.

  TASKING CHANGES:

    None.

*****************************************************************************/
extern void ExeMsgLog (ExeTaskIdT TaskId, ExeMailboxIdT MailboxId, uint32 MsgId,
                       void *MsgBufferP, uint32 MsgSize);
#endif

/*****************************************************************************

  FUNCTION NAME: ExeMsgSendToFront

  DESCRIPTION:

    This routine sends a message into a particular task's mailbox.
    The format of the message is defined by the receiver of the message.
    This message is added to the front of the message list belonging to the
    mailbox specified by the mboxid parameter.

  PARAMETERS:

    INPUTS:

    TaskId       - The task id of the owner of the mailbox
    MailboxId    - The mailbox id of the mailbox receiving the message
    MsgId        - The id of message
    MsgBufferP   - The pointer to the message data buffer. Null if no data
    MsgSize      - The size of the message data in bytes. Zero if no data

  RETURNED VALUES:

    None.

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.

  TASKING CHANGES:

    This routine will suspend the calling task if the message sent
    causes a waiting higher priority task to be ready to run.

*****************************************************************************/
#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeMsgSendToFront(TaskId, MailboxId, MsgId, MsgBufferP, MsgSize) \
        __ExeMsgSend(TaskId, MailboxId, MsgId, MsgBufferP, MsgSize, TRUE,  __MODULE__, __LINE__)


#else

#define ExeMsgSendToFront(TaskId, MailboxId, MsgId, MsgBufferP, MsgSize) \
        __ExeMsgSend(TaskId, MailboxId, MsgId, MsgBufferP, MsgSize, TRUE)

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************

  FUNCTION NAME: ExeMsgRead

  DESCRIPTION:

    This routine removes a message from the head of a message list
    belonging to the mailbox specified in this call. The MsgId variable
    is filled in with the appropriate data. This routine also returns a
    pointer to the data buffer associated with this message.

  CAUTION:

    Only one task should read from any given mailbox.

  PARAMETERS:

    INPUTS:

    TaskId     - The task id of the owner of the mailbox
    MailboxId  - The mailbox id of the mailbox to check the mail from.

    OUTPUTS:

    MsgIdP      - Message id variable is updated
    MsgBufferP  - Message data buffer ptr is updated, NULL if no data
    MsgSizeP    - Message data size variable is updated, zero if no data

  RETURNED VALUES:

    bool       - Returns status indicating if the mailbox was full/empty
                 TRUE = message read, FALSE mailbox empty.

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.

  TASKING CHANGES:

    None.

*****************************************************************************/

#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeMsgRead(TaskId, MailboxId, MsgIdP, MsgBufferP, MsgSizeP) \
        __ExeMsgRead(TaskId, MailboxId, MsgIdP, MsgBufferP, MsgSizeP, __MODULE__, __func__, __LINE__)

extern bool __ExeMsgRead(ExeTaskIdT TaskId, ExeMailboxIdT MailboxId, uint32 *MsgIdP,
                         void **MsgBufferP, uint32 *MsgSizeP,
                         const char *Filename, const char *FunctionName, unsigned Linenumber);

#else

extern bool ExeMsgRead(ExeTaskIdT TaskId, ExeMailboxIdT MailboxId, uint32 *MsgIdP,
                       void **MsgBufferP, uint32 *MsgSizeP);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************

  FUNCTION NAME: ExeMsgCheck

  DESCRIPTION:

    This routine checks a task's mailbox by returning the number of
    mail messages in the mailbox.

  CAUTION:

    Only one task should read from any given mailbox.

  PARAMETERS:

    INPUTS:

    TaskId     - The task id of the owner of the mailbox
    MailboxId  - The mailbox id of the mailbox to check the mail from.

  RETURNED VALUES:

    uint32     - Returns the number of mail messages in the mailbox

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.

  TASKING CHANGES:

    None.

*****************************************************************************/

extern uint32 ExeMsgCheck(ExeTaskIdT TaskId, ExeMailboxIdT MailboxId);

/*****************************************************************************

  FUNCTION NAME: ExeMsgBufferGet

  DESCRIPTION:

    Attempts to allocate a block of memory of the specified size from a
    semi-dynamic memory pool and returns the address of the allocated block.

  PARAMETERS:

    INPUTS:

    MsgBufferSize   - The size of a msg memory buffer (in bytes) needed.

  RETURNED VALUES:

    void * - A pointer to the memory buffer

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.

  TASKING CHANGES:

    None.

*****************************************************************************/

#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeMsgBufferGet(MsgBufferSize) \
        __ExeMsgBufferGet(MsgBufferSize, __MODULE__, __func__, __LINE__)

extern void * __ExeMsgBufferGet(uint32 MsgBufferSize,
                                const char *Filename, const char *FunctionName, unsigned Linenumber);
#else

extern void * ExeMsgBufferGet(uint32 MsgBufferSize);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************

  FUNCTION NAME: ExeMsgBufferFree

  DESCRIPTION:

    This routine deallocates a variable length block of memory back
    to the semi-dynamic memory pool. The memory being deallocated must have
    been allocated previously by a call to ExeMsgBufferGet.

  PARAMETERS:

    INPUTS:

    MsgBufferP   - A pointer to a msg memory buffer.

  RETURNED VALUES:

    No error codes are returned. This routine handles all Nucleus errors.

  TASKING CHANGES:

    None.

*****************************************************************************/

#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeMsgBufferFree(MsgBufferP) \
        __ExeMsgBufferFree(MsgBufferP, __MODULE__, __LINE__)

extern void __ExeMsgBufferFree(void *MsgBufferP,
                             const char *Filename, unsigned Linenumber);

#else

extern void ExeMsgBufferFree(void *MsgBufferP);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************

  FUNCTION NAME: ExeTimerCreate

  DESCRIPTION:

    This routine creates a an application timer. The specified expiration
    routine is executed each time the timer expires. Application expiration
    routines should avoid using task suspension calls since this can cause
    delays in other application timer requests. The application timer is
    created in the disable mode so that a call to ExeTimerStart() is need
    to start the timer.

  PARAMETERS:

    INPUTS:

    TimerCbP       - A pointer to a timer control block
    Routine        - Expiration routine to be called when timer expires.
    TimerId        - Timer ID passed into expiration routine
    InitialTime    - Specifies the initial number of timer ticks for the
                     timer expiration.
    RescheduleTime - Specifies the number of timer ticks for expiration
                     after the first expiration. If this parameter is zero,
                     the timer only expires once.

  RETURNED VALUES:

    None.

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.

  TASKING CHANGES:

    None.

*****************************************************************************/

#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeTimerCreate(TimerCbP, Routine, TimerId, InitialTime, RescheduledTime) \
        __ExeTimerCreate(TimerCbP, Routine, TimerId, InitialTime, RescheduledTime, __MODULE__, __LINE__)

extern void __ExeTimerCreate(ExeTimerT *TimerCbP, void (*Routine)(uint32), uint32 TimerId,
                             uint32 InitialTime, uint32 RescheduledTime,
                             const char *Filename, unsigned Linenumber);
#else

extern void ExeTimerCreate(ExeTimerT *TimerCbP, void (*Routine)(uint32), uint32 TimerId,
                           uint32 InitialTime, uint32 RescheduledTime);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************

  FUNCTION NAME: ExeTimerStart

  DESCRIPTION:

    This routine starts a previously created application timer.

  PARAMETERS:

    INPUTS:

    TimerCbP - A pointer to a timer control block

  RETURNED VALUES:

    None.

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.

  TASKING CHANGES:

    None.

******************************************************************************/

#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeTimerStart(TimerCbP) \
        __ExeTimerStart(TimerCbP, __MODULE__, __LINE__)

extern void __ExeTimerStart(ExeTimerT *TimerCbP,
                            const char *Filename, unsigned Linenumber);

#else

extern void ExeTimerStart(ExeTimerT *TimerCbP);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************

  FUNCTION NAME: ExeTimerStop

  DESCRIPTION:

    This routine stops a previously created application timer.

  CAUTIONS:

    Once a timer has been stopped it must be reset using the ExeTimerReset()
    routine before it is started again.

  PARAMETERS:

    INPUTS:

    TimerCbP - A pointer to a timer control block

  RETURNED VALUES:

    None.

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.

  TASKING CHANGES:

    None.

*****************************************************************************/

#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeTimerStop(TimerCbP) \
        __ExeTimerStop(TimerCbP, __MODULE__, __LINE__)

extern void __ExeTimerStop(ExeTimerT *TimerCbP,
                           const char *Filename, unsigned Linenumber);

#else

extern void ExeTimerStop(ExeTimerT *TimerCbP);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************

  FUNCTION NAME: ExeTimerReset

  DESCRIPTION:

    This routine resets a previously created application timer. The
    specified expiration routine is executed each time the timer expires.
    The application timer is placed in the disable mode so that a call to
    ExeTimerStart() is need to start the timer.

  PARAMETERS:

    INPUTS:

    TimerCbP       - A pointer to a timer control block
    Routine        - Expiration routine to be called when timer expires.
    InitialTime    - Specifies the initial number of timer ticks for the
                     timer expiration.
    RescheduleTime - Specifies the number of timer ticks for expiration
                     after the first expiration. If this parameter is zero,
                     the timer only expires once.

  RETURNED VALUES:

    None.

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.

  TASKING CHANGES:

    None.

*****************************************************************************/

#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeTimerReset(TimerCbP, Routine, InitialTime, RescheduledTime) \
        __ExeTimerReset(TimerCbP, Routine, InitialTime, RescheduledTime, __MODULE__, __LINE__)

extern void __ExeTimerReset(ExeTimerT *TimerCbP, void (*Routine)(uint32),
                            uint32 InitialTime, uint32 RescheduledTime,
                            const char *Filename, unsigned Linenumber);
#else

extern void ExeTimerReset(ExeTimerT *TimerCbP, void (*Routine)(uint32),
                          uint32 InitialTime, uint32 RescheduledTime);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************

  FUNCTION NAME: ExeTimerAdjust

  DESCRIPTION:

    This routine adjusts the active timer list by the number of timer ticks
    passed to this routine. The adjust time passed in to this routine is
    subtracted from the active timer list.

  PARAMETERS:

    INPUTS:

    AdjustTime  - Specifies the amount of time (in timer ticks) that the
                  active list of timers should be adjusted by.

  RETURNED VALUES:

    None.

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.

  TASKING CHANGES:

    None.

*****************************************************************************/

#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeTimerAdjust(AdjustTime) \
        __ExeTimerAdjust(AdjustTime, __MODULE__, __LINE__)

extern void __ExeTimerAdjust(uint32 AdjustTime,
                             const char *Filename, unsigned Linenumber);

#else

extern void ExeTimerAdjust(uint32 AdjustTime);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************

  FUNCTION NAME: ExeSemaphoreCreate

  DESCRIPTION:

    This routine creates a counting semaphore. Semaphore values range from 0
    to 4,294,967,294.

  PARAMETERS:

    INPUTS:

       SemaphoreCbP - A pointer to a semaphore control block
       InitialCount - Initial count of semaphore

  RETURNED VALUES:

    None.

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.

  TASKING CHANGES:

    None.

*****************************************************************************/

#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeSemaphoreCreate(SemaphoreCbP, InitialCount) \
        __ExeSemaphoreCreate(SemaphoreCbP, InitialCount, __MODULE__, __LINE__)

extern void __ExeSemaphoreCreate(ExeSemaphoreT *SemaphoreCbP, uint32 InitialCount,
                                 const char *Filename, unsigned Linenumber);

#else

extern void ExeSemaphoreCreate(ExeSemaphoreT *SemaphoreCbP, uint32 InitialCount);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************
 
  FUNCTION NAME: ExeSemaphoreDelete

  DESCRIPTION:

    This routine deleates a counting semaphore. Semaphore values range from 0 
    to 4,294,967,294.

  PARAMETERS:
      
    INPUTS:
       
       SemaphoreCbP - A pointer to a semaphore control block

  RETURNED VALUES:

    None.

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.
  
  TASKING CHANGES:
 
    None.

*****************************************************************************/
#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeSemaphoreDelete(SemaphoreCbP) \
         __ExeSemaphoreDelete(SemaphoreCbP, __MODULE__, __LINE__)

void __ExeSemaphoreDelete(ExeSemaphoreT *SemaphoreCbP,
                         const char *Filename, unsigned Linenumber);

#else

void ExeSemaphoreDelete(ExeSemaphoreT *SemaphoreCbP);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************

  FUNCTION NAME: ExeSemaphoreGet

  DESCRIPTION:

    This routine gets an instance of a specific semaphore. This translates
    into decrementing the semaphore's internal counter by one. If the
    semaphore counter is zero before this call, the service can not be
    immediately satisfied and the calling task will be suspended. If a
    timeout is specified and the calling task is suspended for a time
    greater then Timeout then this routine will return an error.

  PARAMETERS:

    INPUTS:

    SemaphoreCbP  - A pointer to a semaphore control block
    Timeout       - Timeout associated with this call.  Use EXE_SEM_WAIT_FOREVER
                    for no timeout.

  RETURNED VALUES:

    EXE_SEM_SUCCESS - SemaphoreGet was successful
    EXE_SEM_TIMEOUT - SemaphoreGet timed out

  ERROR HANDLING:

    This routine will signal a Nucleus error and NOT return to the caller if:

      a) the timeout value is EXE_SEM_WAIT_FOREVER and return value from Nucleus
            is not NU_SUCCESS, or
      b) the return value from Nucleus is neither NU_SUCCESS or NU_TIMEOUT.

  TASKING CHANGES:

    If the semaphore counter is zero (in locked state) before this call,
    the service can not be immediately satisfied and the calling task will
    be suspended.

*****************************************************************************/

#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeSemaphoreGet(SemaphoreCbP, Timeout) \
        __ExeSemaphoreGet(SemaphoreCbP, Timeout, __MODULE__, __LINE__)

extern int32 __ExeSemaphoreGet(ExeSemaphoreT *SemaphoreCbP, uint32 Timeout,
                              const char *Filename, unsigned Linenumber);

#else

extern int32 ExeSemaphoreGet(ExeSemaphoreT *SemaphoreCbP, uint32 Timeout);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************

  FUNCTION NAME: ExeSemaphoreGetWithTimeout

  DESCRIPTION:

    This routine gets an instance of a specific semaphore. This translates
    into decrementing the semaphore's internal counter by one. If the
    semaphore counter is zero before this call, the service can not be
    immediately satisfied and the calling task will be suspended. If a
    timeout is specified and the calling task is suspended for a time
    greater then Timeout then this routine will return an error.

    This routine differs from ExeSemaphoreGet in the fact that it returns
    on a timeout rather than declaring a critical fault and not returning.

  PARAMETERS:

    INPUTS:

    SemaphoreCbP  - A pointer to a semaphore control block
    Timeout       - Timeout associated with this call. EXE_TIMEOUT_FALSE
                    for no timeout.

  RETURNED VALUES:

    EXE_SEM_SUCCESS - SemaphoreGet was successful
    EXE_SEM_TIMEOUT - SemaphoreGet timed out

  ERROR HANDLING:

    This routine will signal a Nucleus error and NOT return to the caller if:

      a) the timeout value is EXE_SEM_WAIT_FOREVER and return value from Nucleus
            is not NU_SUCCESS, or
      b) the return value from Nucleus is neither NU_SUCCESS or NU_TIMEOUT.

  TASKING CHANGES:

    If the semaphore counter is zero (in locked state) before this call,
    the service can not be immediately satisfied and the calling task will
    be suspended.

*****************************************************************************/

#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeSemaphoreGetWithTimeout(SemaphoreCbP, Timeout) \
        __ExeSemaphoreGetWithTimeout(SemaphoreCbP, Timeout, __MODULE__, __LINE__)

extern int32 __ExeSemaphoreGetWithTimeout(ExeSemaphoreT *SemaphoreCbP, uint32 Timeout,
                              const char *Filename, unsigned Linenumber);

#else

extern int32 ExeSemaphoreGetWithTimeout(ExeSemaphoreT *SemaphoreCbP, uint32 Timeout);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************

  FUNCTION NAME: ExeSemaphoreRelease

  DESCRIPTION:

    This routine releases an instance of a specific semaphore. This
    translates into incrementing the semaphore's internal counter by one.

  PARAMETERS:

    INPUTS:

    SemaphoreCbP   -  A pointer to a semaphore control block

  RETURNED VALUES:

    None.

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.

  TASKING CHANGES:

    This routine will suspend the calling task if the released semaphore
    causes a waiting higher priority task to be ready to run.

*****************************************************************************/

#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeSemaphoreRelease(SemaphoreCbP) \
        __ExeSemaphoreRelease(SemaphoreCbP, __MODULE__, __LINE__)

extern void __ExeSemaphoreRelease(ExeSemaphoreT *SemaphoreCbP,
                                const char *Filename, unsigned Linenumber);

#else

extern void ExeSemaphoreRelease(ExeSemaphoreT *SemaphoreCbP);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************
 
  FUNCTION NAME: ExeGetSemaphoreCount

  DESCRIPTION:

    This routine returns the number of currently available semaphors in the pool 

  PARAMETERS:
      
    INPUTS:
    
    SemaphoreCbP   -  A pointer to a semaphore control block 

  RETURNED VALUES:

    None.

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.
  
  TASKING CHANGES:

    This routine will suspend the calling task if the released semaphore
    causes a waiting higher priority task to be ready to run.

*****************************************************************************/
#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeGetSemaphoreCount(SemaphoreCbP) \
        __ExeGetSemaphoreCount(SemaphoreCbP, __MODULE__, __LINE__)

uint32 __ExeGetSemaphoreCount(ExeSemaphoreT *SemaphoreCbP,
                         const char *Filename, unsigned Linenumber);

#else

uint32 ExeGetSemaphoreCount(ExeSemaphoreT *SemaphoreCbP);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************
 
  FUNCTION NAME: ExeMemoryPoolCreate

  DESCRIPTION:

    This routine creates a nucleas memory pool based on the 
    previousely allocated buffer.

  PARAMETERS:
      
    INPUTS:
    
    *pool - assigned nucleas pool ID
    *name - pool name
    *start_address - start address of the pre-allocated buffer
    pool_size - size of the buffer
    min_allocation - min size of the allocation from the pool

  RETURNED VALUES:

    None.

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.
  
  TASKING CHANGES:

    None.

*****************************************************************************/
#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeMemoryPoolCreate(pool, name, start_address, pool_size, min_allocation) \
        __ExeMemoryPoolCreate(pool, name, \
        start_address, pool_size, min_allocation, __MODULE__, __LINE__)

extern void __ExeMemoryPoolCreate(NU_MEMORY_POOL *pool, char *name, 
                           void *start_address, uint32 pool_size,
                           uint32 min_allocation,
                           const char *Filename, unsigned Linenumber); 

#else
extern void ExeMemoryPoolCreate(NU_MEMORY_POOL *pool, char *name, 
                        void *start_address, uint32 pool_size,
                        uint32 min_allocation);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************
 
  FUNCTION NAME: ExeMemoryPoolCreate

  DESCRIPTION:

    This routine deletes a nucleas memory pool 

  PARAMETERS:
      
    INPUTS:
    
    NU_MEMORY_POOL *pool - nucleas pool ID

  RETURNED VALUES:

    None.

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.
  
  TASKING CHANGES:

    None.

*****************************************************************************/

#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeMemoryPoolDelete(pool) \
        __ExeMemoryPoolDelete(pool, __MODULE__, __LINE__)

void __ExeMemoryPoolDelete(NU_MEMORY_POOL *pool,
                        const char *Filename, unsigned Linenumber);

#else

void ExeMemoryPoolDelete(NU_MEMORY_POOL *pool);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************
 
  FUNCTION NAME: ExeMallocNohalt

  DESCRIPTION:

    This routine allocates a buffer from any memory pool.

  PARAMETERS:
      
    INPUTS:
    
    pool    - pool ID; if pool==NULL use ExeSystemMemory pool
    size    - The size in bytes of the requested memory.
    suspend - suspend/not suspend option

  RETURNED VALUES:

    The pointer of the allocated buffer.

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.
  
  TASKING CHANGES:

    None.

*****************************************************************************/
#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeMallocNohalt(pool, size, suspend) \
        __ExeMallocNohalt(pool, size, suspend, __MODULE__, __LINE__)

extern void *__ExeMallocNohalt(NU_MEMORY_POOL *pool, uint32 size, uint32 suspend,
                         const char *Filename, unsigned Linenumber);
                   
#else
                   
extern void *ExeMallocNohalt(NU_MEMORY_POOL *pool, uint32 size, uint32 suspend);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************
 
  FUNCTION NAME: ExeMalloc

  DESCRIPTION:

    This routine allocates a buffer from any memory pool.

  PARAMETERS:
      
    INPUTS:
    
    pool    - pool ID; if pool==NULL use ExeSystemMemory pool
    size    - The size in bytes of the requested memory.
    suspend - suspend/not suspend option

  RETURNED VALUES:

    The pointer of the allocated buffer.

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.
  
  TASKING CHANGES:

    None.

*****************************************************************************/
#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeMalloc(pool, size, suspend) \
        __ExeMalloc(pool, size, suspend, __MODULE__, __LINE__)

extern void *__ExeMalloc(NU_MEMORY_POOL *pool, uint32 size, uint32 suspend,
                         const char *Filename, unsigned Linenumber);
                   
#else
                   
extern void *ExeMalloc(NU_MEMORY_POOL *pool, uint32 size, uint32 suspend);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************
 
  FUNCTION NAME: ExeFree

  DESCRIPTION:

    This routine returns a buffer to the system memory pool.

  PARAMETERS:
      
    INPUTS:
    
    ptr    - The pointer of the memory to be returned.

  RETURNED VALUES:

    None.

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.
  
  TASKING CHANGES:

    None.

*****************************************************************************/

#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeFree(ptr) \
        __ExeFree(ptr, __MODULE__, __LINE__)

extern void __ExeFree(void * ptr,
                              const char *Filename, unsigned Linenumber);

#else

extern void ExeFree(void * ptr);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************
 
  FUNCTION NAME: ExeGetFreePoolMemory

  DESCRIPTION:

    This routine returns the number of currently available semaphors in the pool 

  PARAMETERS:
      
    INPUTS:
    
    pool    - pool ID; if pool==NULL use ExeSystemMemory pool

  RETURNED VALUES:

    returns number of available bytes in the pool.

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.
  
  TASKING CHANGES:

    None

*****************************************************************************/
#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeGetFreePoolMemoryCount(pool) \
        __ExeGetFreePoolMemoryCount(pool, __MODULE__, __LINE__)

extern uint32 __ExeGetFreePoolMemoryCount(NU_MEMORY_POOL *pool,
                              const char *Filename, unsigned Linenumber);

#else

extern uint32 ExeGetFreePoolMemoryCount(NU_MEMORY_POOL *pool);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************

  FUNCTION NAME: ExeMemoryPoolBlockSizeAvailable

  DESCRIPTION:

    This routine returns the largest available free block size within the
    specified memory pool.

  PARAMETERS:

    INPUTS:

    *pool - Pointer to nucleus memory pool.

  RETURNED VALUES:

    Largest available free block size

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.

  TASKING CHANGES:

    None.

*****************************************************************************/

#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeMemoryPoolBlockSizeAvailable(pool) \
        __ExeMemoryPoolBlockSizeAvailable(pool, __MODULE__, __LINE__)

uint32 __ExeMemoryPoolBlockSizeAvailable(NU_MEMORY_POOL *pool, 
                   const char *Filename, unsigned Linenumber);
                   
#else
uint32 ExeMemoryPoolBlockSizeAvailable(NU_MEMORY_POOL *pool);

#endif


/*****************************************************************************

  FUNCTION NAME: ExeBufferCreate

  DESCRIPTION:

    This routine creates a buffer of fixed size memory records.

  PARAMETERS:

    INPUTS:

    BufferCbP  - A pointer to a buffer control block
    NumRec     - The number of records to create in the buffer.
    RecSize    - The size in bytes of the fixed memory records.

  RETURNED VALUES:

    None.

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.

  TASKING CHANGES:

    None.

*****************************************************************************/

#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeBufferCreate(BufferCbP, NumRec, RecSize) \
        __ExeBufferCreate(BufferCbP, NumRec, RecSize, __MODULE__, __LINE__)

extern void __ExeBufferCreate(ExeBufferT *BufferCbP, uint32 NumRec, uint32 RecSize,
                              const char *Filename, unsigned Linenumber);

#else

extern void ExeBufferCreate(ExeBufferT *BufferCbP, uint32 NumRec, uint32 RecSize);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************

  FUNCTION NAME: ExeBufferGet

  DESCRIPTION:

    This routine allocates a fixed size memory record from a specific
    buffer. The buffer must have been created previously with a call
    to ExeBufferCreate().

  PARAMETERS:

    INPUTS:

    BufferCbP - A pointer to a memory buffer control block

  RETURNED VALUES:

    void *    - A void pointer to a fixed size memory record

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.

  TASKING CHANGES:

    None.

*****************************************************************************/

#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeBufferGet(BufferCbP) \
        __ExeBufferGet(BufferCbP, __MODULE__, __LINE__)

extern void * __ExeBufferGet(ExeBufferT *BufferCbP,
                             const char *Filename, unsigned Linenumber);

#else

extern void * ExeBufferGet(ExeBufferT *BufferCbP);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************

  FUNCTION NAME: ExeBufferFree

  DESCRIPTION:

    This routine deallocates a fixed size memory record back to a specific
    buffer. The buffer must have been created previously with a call
    to ExeBufferCreate().

  PARAMETERS:

    INPUTS:

    BufferP   -  A pointer to a fixed size memory buffer.

  RETURNED VALUES:

    None.

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.

  TASKING CHANGES:

    None.

*****************************************************************************/

#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeBufferFree(BufferP) \
        __ExeBufferFree(BufferP, __MODULE__, __LINE__)

extern void __ExeBufferFree(void *BufferP,
                            const char *Filename, unsigned Linenumber);
#else

extern void ExeBufferFree(void *BufferP);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************

  FUNCTION NAME: ExeHisrCreate

  DESCRIPTION:

    This routine creates a Nucleus high level ISR (HISR).

  PARAMETERS:

    INPUTS:

    HisrCbP       - A pointer to a HISR control block
    ThreadId      - HISR thread id
    Routine       - HISR routine to be called when HISR is activated
    Priority      - HISR priority

  RETURNED VALUES:

    None

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.

  TASKING CHANGES:

    None.

*****************************************************************************/

#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeHisrCreate(HisrCbP, ThreadId, Routine, Priority) \
        __ExeHisrCreate(HisrCbP, ThreadId, Routine, Priority, __MODULE__, __LINE__)

extern void __ExeHisrCreate(ExeHisrT *HisrCbP, ExeHisrIdT ThreadId, void (*Routine)(void),
                            ExeHisrPrioT Priority,
                            const char *Filename, unsigned Linenumber);

#else

extern void ExeHisrCreate(ExeHisrT *HisrCbP, ExeHisrIdT ThreadId, void (*Routine)(void),
                          ExeHisrPrioT Priority);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************

  FUNCTION NAME: ExeHisrActivate

  DESCRIPTION:

    This routine activates a previously created HISR.

  PARAMETERS:

    INPUTS:

    HisrCbP       - A pointer to a HISR control block

  RETURNED VALUES:

    None

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.

  TASKING CHANGES:

    None.

*****************************************************************************/

#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeHisrActivate(HisrCbP) \
        __ExeHisrActivate(HisrCbP, __MODULE__, __LINE__)

extern void __ExeHisrActivate(ExeHisrT *HisrCbP,
                              const char *Filename, unsigned Linenumber);

#else

extern void ExeHisrActivate(ExeHisrT *HisrCbP);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************

  FUNCTION NAME: ExePreemptionChange

  DESCRIPTION:

    This routine changes the preemption posture of the currently
    executing task to disabled or enabled. If the posture is set
    to disabled no other task can preempt the current task. This
    routine can only be used by tasks and can not be used by HISRs.

  PARAMETERS:

    INPUTS:

    Preemption  - Preemption posture to set
                  EXE_PREEMPT_DISABLED or EXE_PREEMPT_ENABLED

  RETURNED VALUES:

    None

  ERROR HANDLING:

    No error codes are returned.

  TASKING CHANGES:

    This routine will cause the calling task to suspend only when
    preemption is enabled and there is a higher priority task ready
    to run.

*****************************************************************************/

extern void ExePreemptionChange(ExePreemptionT Preemption);

/*****************************************************************************

  FUNCTION NAME: ExeMaxMsgBufferGet

  DESCRIPTION:

    This routine returns the largest message buffer that can be allocated.

  PARAMETERS:

    INPUTS:

    None

  RETURNED VALUES:

    uint32 - Maximum message buffer size.

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.

  TASKING CHANGES:

    None.

*****************************************************************************/

extern uint32 ExeMaxMsgBufferGet(void);

/*****************************************************************************

  FUNCTION NAME: ExeNucleusSystemError

  DESCRIPTION:

    This routine converts a nucleus error to a exe unit error and
    then calls MonFault.

  PARAMETERS:

    INPUTS: Error - nucleus error type

  RETURNED VALUES:

    none

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.

  TASKING CHANGES:

    None.

*****************************************************************************/

extern void ExeNucleusSystemError(uint32 NucleusError);

/*****************************************************************************

  FUNCTION NAME: ExeSystemClockGet

  DESCRIPTION:

    This routine returns the current system clock value.

  PARAMETERS:

    INPUTS:

    None

  RETURNED VALUES:

    uint32 - Current system clock.

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.

  TASKING CHANGES:

    None.

*****************************************************************************/

extern uint32 ExeSystemClockGet(void);

/*****************************************************************************

  FUNCTION NAME: ExeSystemClockSet

  DESCRIPTION:

    This routine sets the current system clock value.

  PARAMETERS:

    INPUTS:

    ClockValue - The value to which the system clock will be set.

  RETURNED VALUES:

    None

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.

  TASKING CHANGES:

    None.

*****************************************************************************/

extern void ExeSystemClockSet(uint32 ClockValue);

/*****************************************************************************
 
  FUNCTION NAME: ExeThreadIDGet

  DESCRIPTION:

    This routine returns the current thread ID(task id or hisr id).

  PARAMETERS:
      
    INPUTS:

    None

  RETURNED VALUES:

    uint32 - Current thread id .

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.
  
  TASKING CHANGES:

    None.

*****************************************************************************/
extern uint32 ExeThreadIDGet(void);

/*****************************************************************************
 
  FUNCTION NAME: ExeTimerGetRemainTime
 
  DESCRIPTION:
 
    This routine get the remaining time of application timer.
 
  PARAMETERS:
      
    INPUTS:
    
    TimerCbP - A pointer to a timer control block
 
  RETURNED VALUES:
 
    None.
 
  ERROR HANDLING:
 
    No error codes are returned. This routine handles all Nucleus errors.
  
  TASKING CHANGES:
 
    None.
 
******************************************************************************/

#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeTimerGetRemainTime(TimerCbP, RemainTime) \
        __ExeTimerGetRemainTime(TimerCbP, RemainTime, __MODULE__, __LINE__)

  extern void __ExeTimerGetRemainTime(ExeTimerT *TimerCbP, uint32 *RemainTime,
                                 const char *Filename, unsigned Linenumber);

#else

  extern void ExeTimerGetRemainTime(ExeTimerT *TimerCbP, uint32 *RemainTime);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************
 
  FUNCTION NAME: ExeTimerDelete

  DESCRIPTION:

    This routine deletes a previously created application timer. All timers
    created must be deleted.  Otherwise, Timers created that happen to
    be located at the same address of a previously created but never
    deleted timer, will fail upon creation..

  PARAMETERS:
      
    INPUTS:
    
    TimerCbP       - A pointer to a timer control block

  RETURNED VALUES:

    None.

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.
  
  TASKING CHANGES:

    None.

*****************************************************************************/

#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeTimerDelete(TimerCbP) \
        __ExeTimerDelete(TimerCbP, __MODULE__, __LINE__)

extern void __ExeTimerDelete(ExeTimerT *TimerCbP, 
                            const char *Filename, unsigned Linenumber);
#else

extern void ExeTimerDelete(ExeTimerT *TimerCbP);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

#ifdef SYS_DEBUG_MSG_BUFF_BUCKET_INFO
extern void ExeResetMsgBuffBuckets( void );
#endif

/*****************************************************************************
  FUNCTION NAME: ExeSpyMsgNotify

  DESCRIPTION:   Control the EXE spy timer

  PARAMETERS:    

  RETURNED VALUES: none
*****************************************************************************/
extern void ExeSpyMsgNotify(void);

/*****************************************************************************
 
  FUNCTION NAME: ExeInterruptDisable

  DESCRIPTION:

    This routine disables interrupts specified by the input bit mask.

    NOTE: This routine must be used in conjunction with ExeInterruptEnable.
    
  PARAMETERS:
      
    INPUTS:
    
    IntMask - Bit Mask of interrupts to disable (IRQ, FIQ, or both)

  RETURNED VALUES:

    None

  ERROR HANDLING:

    No error codes are returned.                                         
 
  TASKING CHANGES:

    None.

*****************************************************************************/
extern uint32 ExeInterruptDisable(uint32 IntMask);

/*****************************************************************************
 
  FUNCTION NAME: ExeInterruptEnable

  DESCRIPTION:

    This routine restores the interrupt status prior to the most recent
    call to ExeInterruptDisable.

    NOTE: This routine must be used in conjunction with ExeInterruptDisable.

  PARAMETERS:
      

  RETURNED VALUES:

    None

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.
 
  TASKING CHANGES:

    None.

*****************************************************************************/
extern void ExeInterruptEnable( uint32 psr );

/*****************************************************************************
 
  FUNCTION NAME: ExeFaultHalt

  DESCRIPTION:

    This routine sends all pending message queue information, message buffer
    statistics, and task control block information to ETS. Since this routine
    should only be called following a fatal crash, this routine will not
    return. Instead the processor is placed in an infinite loop.

  PARAMETERS:
 
    None.

  RETURNED VALUES:

    None.

  TASKING CHANGES:

    None.

*****************************************************************************/
extern void ExeFaultHalt(void);

/*****************************************************************************
 
  FUNCTION NAME: ExeGetMemPoolInfo

  DESCRIPTION:

    Get memery pool information by index.

  PARAMETERS:
 
    PoolPtr, The ExeMemoryPoolInfoT type data. use for save the result.

    Index,   the index number of memery pool, based on 0.

  RETURNED VALUES:

    The nums of all memery pool.

*****************************************************************************/
uint16 ExeGetMemPoolInfo(ExeMemoryPoolInfoT* PoolPtr, uint16 Index);

/*****************************************************************************
 
  FUNCTION NAME: ExeCreateEvtGrp

  DESCRIPTION:

    Create a exe events group.

  PARAMETERS:
 
    name, the name to create a exe event group.

  RETURNED VALUES:

    ExeEvtGrp,  The ExeEvtGrp type data. use for save the result..

*****************************************************************************/
#ifndef MTK_PLT_ON_PC

typedef enum {
    EXE_EVTGRP_SSECCUSS          = 0,
    EXE_EVTGRP_RETRIEVE_TIMEOUT  = 1,
    EXE_EVTGRP_EVTS_NOT_PRESENT  = 2
}ExeEvtGrp_Status;

typedef NU_EVENT_GROUP * ExeEvtGrp;

#define EXE_EVTGRP_CONSUME      NU_OR_CONSUME
#define EXE_EVTGRP_AND          NU_AND
#define EXE_EVTGRP_AND_CONSUME  NU_AND_CONSUME
#define EXE_EVTGRP_OR           NU_OR
#define EXE_EVTGRP_OR_CONSUME   NU_OR_CONSUME

#define EXE_NO_SUSPEND          NU_NO_SUSPEND
#define EXE_SUSPEND             NU_SUSPEND


#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeCreateEvtGrp(name) \
        __ExeCreateEvtGrp(name, __MODULE__, __LINE__)

extern ExeEvtGrp __ExeCreateEvtGrp(char * name,
                           const char *Filename, unsigned Linenumber);

#else
extern ExeEvtGrp ExeCreateEvtGrp(char * name);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************
 
  FUNCTION NAME: ExeDeleteEvtGrp

  DESCRIPTION:

    Delete a exe events group.

  PARAMETERS:
 
    EvtGrp, The ExeEvtGrp type data. use for save the result.

  RETURNED VALUES:

    None.

*****************************************************************************/

#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeDeleteEvtGrp(egrp) \
        __ExeDeleteEvtGrp(egrp, __MODULE__, __LINE__)

extern void __ExeDeleteEvtGrp(ExeEvtGrp EvtGrp,
                           const char *Filename, unsigned Linenumber);

#else
extern void ExeDeleteEvtGrp(ExeEvtGrp EvtGrp);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************
 
  FUNCTION NAME: ExeEvtGrp_Set_Evts

  DESCRIPTION:

    set events to a events group.

  PARAMETERS:
 
    EvtGrp, The ExeEvtGrp.
    events, the events need to set
    operation
    
  RETURNED VALUES:

    None.

*****************************************************************************/
#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeEvtGrp_Set_Evts(egrp, evts, op) \
        __ExeEvtGrp_Set_Evts(egrp, evts, op, __MODULE__, __LINE__)

extern void __ExeEvtGrp_Set_Evts(ExeEvtGrp EvtGrp, uint32 events, uint8 operation,
                                    const char *Filename, unsigned Linenumber);
#else
extern void ExeEvtGrp_Set_Evts(ExeEvtGrp EvtGrp, uint32 events, uint8 operation);
#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************
 
  FUNCTION NAME: ExeEvtGrp_Retrieve_Evts

  DESCRIPTION:

    Retrieve events from a events group.

  PARAMETERS:
 
    EvtGrp, The ExeEvtGrp.
    Need_Evts, the events need to wait
    operation
    
  RETURNED VALUES:

    None.

*****************************************************************************/
#ifdef SYS_DEBUG_FAULT_FILE_INFO
    
#define ExeEvtGrp_Retrieve_Evts(egrp, evts, op, revts, susp) \
            __ExeEvtGrp_Retrieve_Evts(egrp, evts, op, revts, susp, __MODULE__, __LINE__)
    
extern ExeEvtGrp_Status __ExeEvtGrp_Retrieve_Evts(ExeEvtGrp EvtGrp, uint32 Need_Evts, 
                                             uint8 operation, uint32* retrieved_evts, 
                                             uint32 suspend, const char *Filename, unsigned Linenumber);
#else
extern ExeEvtGrp_Status ExeEvtGrp_Retrieve_Evts(ExeEvtGrp EvtGrp, uint32 Need_Evts, 
                                             uint8 operation, uint32* retrieved_evts, 
                                             uint32 suspend);
#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************
 
  FUNCTION NAME: ExeEvtGrp_Retrieve_Evts_Timeout

  DESCRIPTION:

    Retrieve events from a events group .

  PARAMETERS:
 
    EvtGrp, The ExeEvtGrp.
    Need_Evts, the events need to wait
    operation
    
  RETURNED VALUES:

    None.

*****************************************************************************/
#ifdef SYS_DEBUG_FAULT_FILE_INFO
    
#define ExeEvtGrp_Retrieve_Evts_Timeout(egrp, evts, op, rents, timeo) \
            __ExeEvtGrp_Retrieve_Evts_Timeout(egrp, evts, op, rents, timeo, __MODULE__, __LINE__)
    
extern ExeEvtGrp_Status __ExeEvtGrp_Retrieve_Evts_Timeout(ExeEvtGrp EvtGrp, uint32 Need_Evts, 
                                             uint8 operation, uint32* retrieved_evts, 
                                             uint32 timeout, const char *Filename, unsigned Linenumber);
#else
extern ExeEvtGrp_Status ExeEvtGrp_Retrieve_Evts_Timeout(ExeEvtGrp EvtGrp, uint32 Need_Evts, 
                                             uint8 operation, uint32* retrieved_evts, 
                                             uint32 timeout);
#endif /* SYS_DEBUG_FAULT_FILE_INFO */


#endif /*! MTK_PLT_ON_PC */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
/**Log information: \main\4 2012-03-23 07:11:14 GMT yliu
** remove suspicious ^L character**/
/**Log information: \main\Trophy\Trophy_fwu_href22348\1 2014-01-08 06:16:55 GMT fwu
** HREF#22348. Merge ESPI related source code.**/
/**Log information: \main\Trophy\1 2014-01-09 06:34:26 GMT zlin
** HREF#22348, merge code.**/
