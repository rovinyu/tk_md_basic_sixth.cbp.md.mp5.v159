/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
/**************************************************************************************************
* %version: 2 %  %instance: HZPT_2 %   %date_created: Fri Mar 23 15:56:37 2007 %  %created_by: jingzhang %  %derived_by: jingzhang %
**************************************************************************************************/

/*****************************************************************************
* 
* FILE NAME   : valdispapi.h
*
* DESCRIPTION :
*
*   This is the interface include file for Display.
*
* HISTORY     :
*
*   See Log at end of file
*
*****************************************************************************/
#ifndef _VALDISPAPI_H_
#define _VALDISPAPI_H_

#include "sysdefs.h"


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/*--------------------------------------------------------------------
* Macro and data structure defines
*--------------------------------------------------------------------*/

typedef uint32 ValDispColorT;

typedef uint8 ValDispLcdIdT;

//don't modify following Macro, brew has referred to it
/* please do not modify the following lcd ids */
#define VAL_DISP_MAIN_LCD_ID        (ValDispLcdIdT)(0)
#define VAL_DISP_SUB_LCD_ID         (ValDispLcdIdT)(1)

#define VAL_DISP_INVALID_LCD_ID     (ValDispLcdIdT)(0xFF)
typedef enum
{
    VAL_DISP_LCD_OWNER_CBP,
    VAL_DISP_LCD_OWNER_OTHER,
    VAL_DISP_LCD_OWNER_TOTAL
}ValDispLcdOwnerT;

#define VAL_DISP_RGB_565(R, G, B)                   \
   ((ValDispColorT)((((uint16)(R) & 0x00F8) << 8)   \
                  + (((uint16)(G) & 0x00FC) << 3)   \
                  + (((uint16)(B) & 0x00F8) >> 3)))

#define VAL_DISP_RGB_WHITE_565  VAL_DISP_RGB_565(0xFF, 0xFF, 0xFF)
#define VAL_DISP_RGB_BLACK_565  VAL_DISP_RGB_565(0x0, 0x0, 0x0)

#define VAL_DISP_RGB_RED_565    VAL_DISP_RGB_565(0xFF, 0x0, 0x0)
#define VAL_DISP_RGB_GREEN_565  VAL_DISP_RGB_565(0x0, 0xFF, 0x0)
#define VAl_DISP_RGB_BLUE_565   VAL_DISP_RGB_565(0x0, 0x0, 0xFF)

#define VAL_DISP_INVALID_COLOR   (0xFFFFFFFF)

//don't modify following Enum, brew has referred to it
typedef enum
{
   VAL_DISP_FONT_SMALL_PLAIN,
   VAL_DISP_FONT_SMALL_BOLD, 
   VAL_DISP_FONT_NORMAL_PLAIN,
   VAL_DISP_FONT_NORMAL_BOLD,
   VAL_DISP_FONT_LARGE_PLAIN,
   VAL_DISP_FONT_LARGE_BOLD,
   VAL_DISP_FONT_SUPER_BOLD,
   VAL_DISP_FONT_SPECIAL_NORMAL,
   VAL_DISP_FONT_SPECIAL_LARGE,
   VAL_DISP_FONT_MICRO_BOLD,
   
   VAL_DISP_FONT_CUST_1,
   VAL_DISP_FONT_CUST_2,
   VAL_DISP_FONT_CUST_3,
   VAL_DISP_FONT_CUST_4,
   
   VAL_DISP_FONT_TOTAL
} ValDispFontT;

typedef struct
{
    uint16     StartCode;
    uint16     EndCode;
    uint16     StartIndex;
} ValFontIndexTableT;

//don't modify following Struct, brew has referred to it
typedef struct
{
   ValDispFontT     FontId;
   uint16           GlyphCount;
   uint8            Ascent;
   uint8            Descent;
   bool             FixedWidth;
   uint8            Width;  
   const uint32     *OffsetP; 
   uint16           IndexTableNum;
   const ValFontIndexTableT   *IndexTableP;
   const uint8      *BitmapP;
} ValDispFontInfoT;

//don't modify following Enum, brew has referred to it
typedef enum 
{
    VAL_DISP_FNT_CP_LATIN1 = 0,             /* Basic Latin plus Latin-1 */
    VAL_DISP_FNT_CP_UNICODE,                /* Unicode little mode */
    VAL_DISP_FNT_CP_UNICODE_BIG,            /* Unicode big mode */
    VAL_DISP_FNT_CP_TOTAL                   /* Total number of Codepages */
} ValDispFontCodepageT;

//don't modify following Enum, brew has referred to it
typedef enum
{
    VAL_DISP_RO_COPY,
    VAL_DISP_RO_INVERT,
    VAL_DISP_RO_AND,
    VAL_DISP_RO_OR,
    VAL_DISP_RO_XOR,
    VAL_DISP_RO_ANDNOT,
    VAL_DISP_RO_ORNOT,
    VAL_DISP_RO_TOTAL
} ValDispRasterOpT;  

//don't modify following Enum, brew has referred to it
typedef enum 
{
    VAL_DISP_MODE_TRANSPARENT,
    VAL_DISP_MODE_OPAQUE,
    VAL_DISP_MODE_TOTAL
} ValDispModeT;

//don't modify following Enum, brew has referred to it
typedef enum
{
    VAL_DISP_PEN_0 = 0,                   /* Zero pixel border */
    VAL_DISP_PEN_1 = 1,                   /* One pixel border */
    VAL_DISP_PEN_2 = 2,                   /* Two pixel border */
    VAL_DISP_PEN_3 = 3,                   /* Three pixel border */
    VAL_DISP_PEN_4 = 4,                   /* Four pixel border */
    VAL_DISP_PEN_5 = 5,                   /* Five pixel border */
    VAL_DISP_PEN_TOTAL                /* Total number of pens */
} ValDispPenT;

//don't modify following Enum, brew has referred to it
typedef enum 
{
    VAL_DISP_HALIGN_LEFT,
    VAL_DISP_HALIGN_CENTER,
    VAL_DISP_HALIGN_RIGHT
} ValDispHAlignT;

//don't modify following Enum, brew has referred to it
typedef enum 
{ 
    VAL_DISP_VALIGN_UP,         /* Aligns the point with the up side of the bounding rectangle */
    VAL_DISP_VALIGN_CENTER,     /* Aligns the point with the vertical center of the bounding rectangle */
    VAL_DISP_VALIGN_DOWN        /* Aligns the point with the down side of the bounding rectangle */
} ValDispVAlignT;               /* This data type is the alignment used for the text that is generated */

//don't modify following Enum, brew has referred to it
typedef enum
{
    VAL_DISP_TRANSFER_COPY,            /* Copy source to dest; used for opaque images */
    VAL_DISP_TRANSFER_TRANSPARENT,     /* Copy only data that is not the transparent color */
    VAL_DISP_TRANSFER_NON_TRANSPARENT, /* Set dest to specified color before drawing text */
    VAL_DISP_TRANSFER_COLOR_FILL       /* Set dest to specified color */
} ValDispTransferT;

//don't modify following Struct, brew has referred to it
typedef struct 
{
    int16 x, y;               
} ValDispPointT;

//don't modify following Struct, brew has referred to it
typedef struct
{
    int16 x, y;                /* Starting point */
    int16 dx, dy;              /* Width and height */ 
} ValDispRectT;

//don't modify following Enum, brew has referred to it
typedef enum
{
    VAL_DISP_BIT_COUNT_1 = 1,
    VAL_DISP_BIT_COUNT_2 = 2,
    VAL_DISP_BIT_COUNT_4 = 4,
    VAL_DISP_BIT_COUNT_8 = 8,
    VAL_DISP_BIT_COUNT_16 = 16,
    VAL_DISP_BIT_COUNT_18 = 18,
    VAL_DISP_BIT_COUNT_24 = 24,

    VAL_DISP_BIT_GFX_RGB666    = 0xF1,
    VAL_DISP_BIT_GFX_RGB888    = 0xF2,     
    VAL_DISP_BIT_GFX_ARGB1555  = 0xF3,	   
    VAL_DISP_BIT_GFX_ARGB4444  = 0xF4, 
    VAL_DISP_BIT_GFX_ARGB8888  = 0xF5,
    VAL_DISP_BIT_GFX_8BIT      = 0xF6,
    VAL_DISP_BIT_GFX_1BIT      = 0xF7,
    
    VAL_DISP_BIT_COUNT_MAX
} ValDispBitCountTypeT;

//don't modify following Enum, brew has referred to it
typedef enum
{
    VAL_DISP_OPAQUE_BIT_NONE = 0,
    VAL_DISP_OPAQUE_BIT_1 = 1,
    VAL_DISP_OPAQUE_BIT_4 = 4,
    VAL_DISP_OPAQUE_BIT_8 = 8,
    VAL_DISP_OPAQUE_BIT_TRANSCOLOR = 0xFF
} ValDispOpaqueBitT;

typedef enum
{
    VAL_DISP_FORMAT_BMP,
    VAL_DISP_FORMAT_WBMP,
    VAL_DISP_FORMAT_RGB16,
    VAL_DISP_FORMAT_COMPRESSED_RGB,
    VAL_DISP_NUM_FORMATS
} ValDispFormatT;

//don't modify following Struct, brew has referred to it
typedef struct
{
    uint16 Width;                   /* the width of the bitmap */
    uint16 Height;                  /* the height of the bitmap */
    ValDispBitCountTypeT BitCount;  /* the depth of color for pixels */
    ValDispOpaqueBitT OpaqueBit;    /* opaque bit type */
    uint16 TransColor;              /* transparent color */
    uint8* AlphaData;               /* point to alpha data */
    uint8*  DataBuf;                /* Points to bitmap buffer */
} ValDispBitmapT;

//! type of image source to decode-display
typedef enum
{
  VAL_DECODE_PARA_NONE = 0,
  VAL_DECODE_PARA_BITMAP,
  VAL_DECODE_PARA_FILE,
  VAL_DECODE_PARA_BUF,
  VAL_DISP_PARA_UNKNOWN
} ValDecodeParaTypeT;

//! image source parameter to display
typedef struct
{
  ValDecodeParaTypeT ParaType;      /*type of source image to display*/
  void* ParaP;                      /*source image parameter, can be cast into ValDispBitmapT,*/ 
                                    /*ValDecodeFileParaT or ValDecodeBufParaT type pointer*/
}ValDecodeParaT;

//don't modify following Struct, brew has referred to it
typedef struct
{
    ValDispLcdIdT           LcdId;
    ValDispBitmapT          Bitmap;
    ValDispBitmapT*         BmpP; 
    ValDispRectT            ClipRect;
    ValDispColorT           TextColor;
    ValDispColorT           BackColor;
    ValDispColorT           PenColor;
    ValDispColorT           BrushColor;
    ValDispModeT            BackMode;
    ValDispColorT           TransColor;
    ValDispPenT             PenType;
    ValDispRasterOpT        RO;
    ValDispFontT            Font;
    uint16                  FontWidth;
    uint16                  FontHeight;
    ValDispFontCodepageT    Codepage;
    bool DrawThrough;
    bool ForceSysFont;
} ValDispDevContextT;

typedef enum
{
    VAL_DISP_ERR_NONE,
    VAL_DISP_ERR_INVALID_PARAMETER,
    VAL_DISP_ERR_DRIVER_ERR,
    VAL_DISP_ERR_INVALID_LCD_ID,
    VAL_DISP_ERR_UNSOPPORTED_BITMAP,
    VAL_DISP_ERR_MAX
} ValDispErrorCodeT;

typedef enum
{
    VAL_DISP_CONTRAST_LEVEL_0 = 0,
    VAL_DISP_CONTRAST_LEVEL_1,
    VAL_DISP_CONTRAST_LEVEL_2,
    VAL_DISP_CONTRAST_LEVEL_3,
    VAL_DISP_CONTRAST_LEVEL_4,
    VAL_DISP_CONTRAST_LEVEL_5,
    VAL_DISP_CONTRAST_LEVEL_6,
    VAL_DISP_CONTRAST_LEVEL_7,
    VAL_DISP_CONTRAST_LEVEL_8,
    VAL_DISP_CONTRAST_LEVEL_MAX = VAL_DISP_CONTRAST_LEVEL_8
} ValDispContrastLevelT;

//don't modify following Struct, brew has referred to it
typedef struct
{
   char Vendor[20];    /* max 20 characters */
   char ModelId[20];   /* max 20 characters */
   int16 WidthInPixels;
   int16 HeightInPixels;
   ValDispBitCountTypeT BitCount;
   uint8* DataBufP;
} ValDispDeviceInfoT;

/* Used for Update the LCD
typedef enum
{
    VAL_DISP_UPDATE_ERROR = 0,
    VAL_DISP_UPDATE_DONE,
    VAL_DISP_UPDATE_OWNER_OTHER,
    VAL_DISP_UPDATE_NO_CHANGE
}ValDispUpdateFlagT;
*/
/* if use enum, there will be an error in edit window, no idea about that */
#define VAL_DISP_UPDATE_ERROR       0
#define VAL_DISP_UPDATE_DONE        1
#define VAL_DISP_UPDATE_OWNER_OTHER 2
#define VAL_DISP_UPDATE_NO_CHANGE   3


/*--------------------------------------------------------------------
* YL:The following functions are to set/get direction of screen rotation
*--------------------------------------------------------------------*/
typedef enum
{
  SCREEN_NORMAL,	
  SCREEN_ROTATE_ANTI_CLOCKWISE_90 /* 90 degree anti-clockwise rotation */
}ScreenRotateDirT;
ScreenRotateDirT ValSetScreenRotateDir(ScreenRotateDirT screenRotateDir);
ScreenRotateDirT ValGetScreenRotateDir(void);

/*--------------------------------------------------------------------
* YL:The following functions are added to support screen rotation.
*--------------------------------------------------------------------*/
bool IsDynamicScreenRotate(void);
void ConvertDpToLp(uint16 *PointX, uint16 *PointY, ScreenRotateDirT screenDirection);
void ConvertLpToDp(int16 *PointX, int16 *PointY, ScreenRotateDirT screenDirection);
void ConvertRect(int16 *x, int16 *y, int16 *dx, int16 *dy, ScreenRotateDirT screenRotateDir);
int32 ValGetScreenWidth(ValDispBitmapT *BmpP, ScreenRotateDirT screenDirection);
int32 ValGetScreenHeight(ValDispBitmapT *BmpP, ScreenRotateDirT screenDirection);
void ValGetIncrement(ValDispBitmapT *BmpP,int32 *xIncrement, int32 *yIncrement,ScreenRotateDirT screenDirection);
void ValAdjustBmpSize(ScreenRotateDirT screenRotateDir,ValDispDevContextT* dcP);
/*--------------------------------------------------------------------
* The following functions are to set/get properties in /out of DC
*--------------------------------------------------------------------*/
//don't modify following Function, brew has referred to it
ValDispErrorCodeT  ValDispInitDC(ValDispDevContextT* dcP, ValDispLcdIdT LcdId, ValDispBitmapT *BitmapP);

ValDispColorT ValDispSetTextColor(ValDispDevContextT* dcP, ValDispColorT Color);
ValDispColorT ValDispGetTextColor(ValDispDevContextT* dcP);

ValDispColorT ValDispSetBackColor(ValDispDevContextT* dcP, ValDispColorT Color);
ValDispColorT ValDispGetBackColor(ValDispDevContextT* dcP);

ValDispModeT ValDispSetBackMode(ValDispDevContextT* dcP, ValDispModeT Mode);
ValDispModeT ValDispGetBackMode(ValDispDevContextT* dcP);

ValDispColorT ValDispSetTransColor(ValDispDevContextT* dcP, ValDispColorT Color);
ValDispColorT ValDispGetTransColor(ValDispDevContextT* dcP);

ValDispPenT ValDispSetPenType(ValDispDevContextT* dcP, ValDispPenT Pen);
ValDispPenT ValDispGetPenType(ValDispDevContextT* dcP);

ValDispColorT ValDispSetPenColor(ValDispDevContextT* dcP, ValDispColorT Color);
ValDispColorT ValDispGetPenColor(ValDispDevContextT* dcP);

ValDispColorT ValDispSetBrushColor(ValDispDevContextT* dcP, ValDispColorT Color);
ValDispColorT ValDispGetBrushColor(ValDispDevContextT* dcP);

ValDispRasterOpT ValDispSetROP(ValDispDevContextT* dcP, ValDispRasterOpT RO);
ValDispRasterOpT ValDispGetROP(ValDispDevContextT* dcP);

ValDispFontT ValDispSetFont(ValDispDevContextT* dcP, ValDispFontT Font);
ValDispFontT ValDispGetFont(ValDispDevContextT* dcP);
void    ValDispSetFontSize(ValDispDevContextT* dcP, uint16 FontHeight, uint16 FontWidth);
uint16  ValDispGetFontHeight(ValDispDevContextT* dcP);

ValDispFontCodepageT ValDispSetFontCodepage(ValDispDevContextT* dcP, ValDispFontCodepageT Codepage);
ValDispFontCodepageT ValDispGetFontCodepage(ValDispDevContextT* dcP);
   
//don't modify following Function, brew has referred to it
ValDispRectT ValDispSetClipRect(ValDispDevContextT* dcP, ValDispRectT* RectP);
ValDispRectT ValDispGetClipRect(ValDispDevContextT* dcP);     
    
bool ValDispSetDrawThrough(ValDispDevContextT* dcP, bool DrawThrough);
bool ValDispGetDrawThrough(ValDispDevContextT* dcP);

bool ValDispSetForceSysFont(ValDispDevContextT* dcP, bool ForceSysFont);
bool ValDispGetForceSysFont(ValDispDevContextT* dcP);

/*--------------------------------------------------------------------
* The following functions are to draw and bitblt.
*--------------------------------------------------------------------*/
ValDispColorT ValDispSetPixelColor(ValDispDevContextT* dcP, int16 x, int16 y, ValDispColorT Color);
ValDispColorT ValDispGetPixelColor(ValDispDevContextT* dcP, int16 x, int16 y);

void ValDispDrawLine(ValDispDevContextT* dcP, ValDispPointT P0, ValDispPointT P1);
void ValDispDrawRect(ValDispDevContextT* dcP, ValDispRectT* RectP, bool Filled);
void ValDispEraseRect(ValDispDevContextT* dcP, ValDispRectT* RectP);
void ValDispClearScreen(ValDispDevContextT* dcP);
void ValDispObscureScreen(ValDispDevContextT* dcP);
 
void ValDispBitBlt(ValDispDevContextT* dcP, ValDispRectT* DstRectP, ValDispBitmapT* BmpSrcP, int16 xSrc, int16 ySrc);
void ValDispBitBltTransparent(ValDispDevContextT* dcP, ValDispRectT* DstRectP, ValDispBitmapT* BmpSrcP, int16 xSrc, int16 ySrc);
void ValDispBitBlt_Ext(ValDispDevContextT* dcP, ValDispRectT* DstRectP, ValDecodeParaT* DecodeParaP, int16 xSrc, int16 ySrc);
void ValDispBitBltTransparent_Ext(ValDispDevContextT* dcP, ValDispRectT* DstRectP, ValDecodeParaT* DecodeParaP, int16 xSrc, int16 ySrc);


/*--------------------------------------------------------------------
* The following functions are for font and text
*--------------------------------------------------------------------*/
void ValDispFontRegister(ValDispFontT Font, ValDispFontInfoT* FontInfoP, bool Unicode);
void ValDispFontUnRegister(ValDispFontT Font, ValDispFontInfoT* FontInfoP, bool Unicode); 
//don't modify following Function, brew has referred to it     
void ValDispGetFontInfo(ValDispDevContextT *dcP, uint16* AscentP, uint16* DescentP);
void ValDispInitFontInfo(void);
//don't modify following Function, brew has referred to it
void ValDispMeasureText(ValDispDevContextT* dcP, uint8* TextP, int16 Chars, int16 MaxWidth, int16* CharFitsP, int16* PixelsP);   
uint16 ValDispGetTextSize(ValDispDevContextT* dcP, uint8* TextP, uint16 Count); 	

void ValDispGetFontCharBitmap(ValDispFontT Font,uint16 CharCode, ValDispBitmapT *BitmapP, 
                                bool OnlyWidth, bool ForceSysFont, uint16 FontHeight, uint16 FontWidth);

void ValDispTextOut(ValDispDevContextT* dcP, int16 x, int16 y, uint8* TextP, uint16 Length);
void ValDispTextOut_Ex(ValDispDevContextT *dcP, int16 x, int16 y, uint8 *TextP, uint16 Length, ScreenRotateDirT screenDirection);
//don't modify following Function, brew has referred to it
void ValDispDrawText(ValDispDevContextT* dcP, uint8* TextP, uint16 Count, ValDispRectT* RectP, ValDispHAlignT Align);  
void ValDispDrawText_Ex(ValDispDevContextT* dcP, uint8* TextP, uint16 Count, ValDispRectT* RectP, ValDispHAlignT Align, ScreenRotateDirT screenDirection);  
/*--------------------------------------------------------------------
* the following functions are related to lcd
*--------------------------------------------------------------------*/
void ValDispSetLcdOwner(ValDispLcdIdT LcdId, ValDispLcdOwnerT LcdOwner);
void ValDispContrastSet(ValDispLcdIdT LcdId, ValDispContrastLevelT Level);
void ValDispDisplayOn(ValDispLcdIdT LcdId, bool OnOff);
void ValDispLcdDevInfoGet(ValDispLcdIdT LcdId, ValDispDeviceInfoT* LcdInfoP);

//don't modify following Function, brew has referred to it
uint8 ValDispUpdateDisplayRect(ValDispLcdIdT LcdId, ValDispBitmapT* BmpP, ValDispRectT *RectP);
uint8 ValDispRefreshDisplayRect(ValDispLcdIdT LcdId, ValDispBitmapT* BmpP, ValDispRectT *RectP);
uint8 ValDispBufferUpdateDisplayRect(ValDispLcdIdT LcdId, ValDispBitmapT* BmpP, ValDispRectT *RectP);
uint8 ValDispUpdateDisplayRect_Ext(ValDispLcdIdT LcdId, ValDecodeParaT* DecodeParaP, ValDispRectT* RectP);
uint8 ValDispRefreshDisplayRect_Ext(ValDispLcdIdT LcdId, ValDecodeParaT* DecodeParaP, ValDispRectT* RectP);

typedef enum
{
   GFX_SRC0_IS_RGB565=0,		
   GFX_SRC0_IS_RGB666,	   
   GFX_SRC0_IS_RGB888,	   
   GFX_SRC0_IS_ARGB1555,	   
   GFX_SRC0_IS_ARGB4444,	   
   GFX_SRC0_IS_ARGB8888,	   
   GFX_SRC0_IS_8BIT_EXPAND,	
   GFX_SRC0_IS_1BIT_EXPAND	
}VAL_GFX_SRC0_FMT;

typedef enum
{
   GFX_SRC1_IS_RGB565=0,
   GFX_SRC1_IS_RGB888	
}VAL_GFX_SRC1_FMT;


typedef enum
{
   GFX_DEST_IS_RGB565=0,
   GFX_DEST_IS_RGB888	
}VAL_GFX_DEST_FMT;

void GFXBitBltBmpBC8ToBC16(ValDispDevContextT *dcP, int16 xDst, int16 yDst, int16 dx, int16 dy, 
                         ValDispBitmapT *SrcBmpP, int16 xSrc, int16 ySrc, bool Transparent);
void GFXBitBltBmpBC16ToBC16(ValDispDevContextT *dcP, int16 xDst, int16 yDst, int16 dx, int16 dy, 
                         ValDispBitmapT *SrcBmpP, int16 xSrc, int16 ySrc, bool Transparent);
void GFXBitBltBmpOtherToBC16(ValDispDevContextT *dcP, int16 xDst, int16 yDst, int16 dx, int16 dy, 
                         ValDispBitmapT *SrcBmpP, int16 xSrc, int16 ySrc, bool Transparent);                         

int GFXBitBltBase(uint32 *Src0DataStartP,uint32 *Src1DataStartP, uint32 *DstDataStartP,
                   VAL_GFX_SRC0_FMT Src0Fmt,VAL_GFX_SRC1_FMT Src1Fmt,VAL_GFX_DEST_FMT DstFmt,
                   bool AlphaEn,uint8 AlphaData,uint32* ColortableP,
                   int16 BitBWidth,int16 BitBHeight,
                   int16 Src0offset,int16 Src1offset,int16 Dstoffset);
int GFXBitBltBaseTransparent(uint32 *Src0DataStartP,uint32 *Src1DataStartP, uint32 *DstDataStartP,
                   VAL_GFX_SRC0_FMT Src0Fmt,VAL_GFX_SRC1_FMT Src1Fmt,VAL_GFX_DEST_FMT DstFmt,
                   uint32 Src0TransColor,uint32* ColortableP,
                   int16 BitBWidth,int16 BitBHeight,
                   int16 Src0offset,int16 Src1offset,int16 Dstoffset);                   
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif  /* #ifndef _VALDISPAPI_H_ */ 
/*****************************************************************************
 * $Log: valdispapi.h $
 * Revision 1.4  2008/01/31 15:41:04  zlin
 * Modify #define VAl_DISP_RGB_BLUE_565   DISP_RGB_565(0x0, 0x0, 0x3F) to #define VAl_DISP_RGB_BLUE_565   DISP_RGB_565(0x0, 0x0, 0x1F).
 * Revision 1.3  2008/01/31 10:45:38  zlin
 * Modify VAL_DISP_RGB_GREEN_565  DISP_RGB_565(0x0, 0x1F, 0x0) to VAL_DISP_RGB_GREEN_565  DISP_RGB_565(0x0, 0x3F, 0x0).
 * Revision 1.2  2008/01/02 16:47:21  dwang
 * Merge ui_dev 3.2.0
 * Revision 1.1  2007/10/29 10:53:05  binye
 * Initial revision
 * Revision 1.2  2007/10/12 09:39:23  dsu
 * code merge
 * Revision 1.8  2007/01/08 14:22:33  xyliu
 * Modify the functions about updating the lcd, add the return flag to indicate wheather the LCD is update really.
 * Revision 1.7  2007/01/04 17:03:34  xyliu
 * Modify the ValDispFontCodepageT for UI.
 * Revision 1.6  2006/12/31 10:54:00  xyliu
 * New Codepages.
 * Revision 1.5  2006/12/21 11:20:22  hzhang
 * add data type ValDispVAlignT
 * Revision 1.4  2006/12/11 09:49:20  zzhang
 * Merge from the old baseline.
 * Revision 1.7  2006/12/04 21:48:42  xyliu
 * Add the definition of function ValDispInitFontInfo().
 * Revision 1.6  2006/12/04 09:46:24  xyliu
 * Change a parameter of the function ValDispGetFontInfo.
 * Revision 1.5  2006/12/04 09:32:28  xyliu
 * Change two APIs' names.
 * Revision 1.4  2006/11/30 19:31:35  xyliu
 * Add text font for customer.
 * Revision 1.3  2006/11/30 17:04:05  xyliu
 * Change several parameters' name of functions, let them same with the parameters' name in valdisp*.c.
 * Revision 1.2  2006/11/10 10:23:09  hzhang
 * new VAL DISPLAY APIs
 *****************************************************************************/
/**Log information: E:\ClearCase\VTUI2_5X\HZREF\cp\inc\valdispapi.h@@\main\vtui2_5x\2 2008-05-26 02:49:50 GMT wjzhang
** HREF#308**/
/**Log information: \main\vtui2_5x\3 2008-07-02 08:44:29 GMT wjzhang
** HREF#769**/
/**Log information: \main\vtui2_5x\4 2008-09-03 06:41:41 GMT wjzhang
** HREF#875**/
/**Log information: \main\vtui2_5x\5 2008-10-20 09:52:33 GMT mchang
** HREF#2583**/
/**Log information: \main\vtui2_5x\6 2008-10-30 09:40:23 GMT cwu
** HREF#2698 **/
/**Log information: \main\vtui2_5x\7 2008-12-17 11:22:31 GMT yliu
** HREF#3992**/
/**Log information: \main\vtui2_5x\vtui2_5x_jinqi_href5146\1 2009-02-10 03:27:30 GMT jinqi
** HREF#5146**/
/**Log information: \main\vtui2_5x\8 2009-02-10 07:22:42 GMT yliu
** HREF#5146**/
/**Log information: \main\vtui2_5x\vtui2_5x_lfzhu_href6339\1 2009-04-14 07:45:27 GMT lfzhu
** HREF#6339**/
/**Log information: \main\vtui2_5x\9 2009-04-26 09:28:30 GMT jinqi
** HREF#6339**/
/**Log information: \main\helios_dev\helios_dev_jinqi_href6918\1 2009-05-14 01:38:10 GMT jinqi
** HREF#6918**/
/**Log information: \main\helios_dev\3 2009-05-18 12:00:11 GMT yliu
** HREF#6918 merge**/
/**Log information: \main\helios_dev\helios_dev_wjzhang_href7364\1 2009-06-22 02:00:20 GMT wjzhang
** HREF#7364,add VAL_DISP_FONT_MICRO_BOLD �ֿ�**/
/**Log information: \main\helios_dev\4 2009-06-22 02:42:28 GMT jinqi
** HREF#7364**/
/**Log information: \main\helios_dev\helios_dev_yubai_href8132\1 2009-09-01 02:08:48 GMT yubai
** HREF#8132**/
/**Log information: \main\helios_dev\5 2009-09-01 08:30:21 GMT yubai
** HREF#8132**/
/**Log information: \main\helios_dev\touchflow\touchflow_cwu_href8098\1 2009-09-17 06:28:27 GMT cwu
** HREF#8098 **/
/**Log information: \main\helios_dev\touchflow\1 2009-10-19 07:47:18 GMT dwang
** HREF#8098**/
/**Log information: \main\VTUI3\VTUI3_yangli_href10287\1 2010-01-18 07:51:49 GMT yangli
** HREF#10287**/
/**Log information: \main\VTUI3\2 2010-01-19 08:37:30 GMT jinqi
** HREF#10287**/
/**Log information: \main\VTUI3\VTUI3_yangli_href11067\1 2010-03-24 07:46:58 GMT yangli
** HREF#11067**/
/**Log information: \main\VTUI3\3 2010-03-25 05:48:45 GMT wjzhang
** merge for jinqi, href#11067**/
/**Log information: \main\VTUI3\VTUI3_yangli_href11334\1 2010-04-13 11:42:17 GMT yangli
** HREF#11334 **/
/**Log information: \main\VTUI3\VTUI3_yangli_href11334\2 2010-04-14 08:40:11 GMT yangli
** HREF#11334**/
/**Log information: \main\VTUI3\4 2010-04-14 09:03:03 GMT jinqi
** VTUI3_yangli_href11334**/
/**Log information: \main\CBP7FeaturePhone\CBP7FeaturePhone_nicholaszhao_href17384\1 2011-07-04 08:22:10 GMT nicholaszhao
** HREF#17384**/
/**Log information: \main\CBP7FeaturePhone\CBP7FeaturePhone_nicholaszhao_href17384\2 2011-07-08 06:26:05 GMT nicholaszhao
** HREF#17384**/
/**Log information: \main\CBP7FeaturePhone\9 2011-07-12 09:40:35 GMT marszhang
** HREF#17384**/
/**Log information: \main\VTUI3\VTUI3_0_5_0\CBP8_UI\1 2012-02-28 08:55:11 GMT gfzhu
** modify for vector font,  can be setted font size by application. **/
/**Log information: \main\5 2012-03-08 08:18:30 GMT gfzhu
** Changed for LCD size (320*480) , sphone ui, vector font , menu ctrl from CBP8.0**/
/**Log information: \main\6 2012-04-23 03:24:49 GMT wjzhang
** support GFX 2D Grahpic Accelerator**/
