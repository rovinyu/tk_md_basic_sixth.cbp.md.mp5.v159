/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.
*
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
*
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
/*****************************************************************************
*
* FILE NAME   : do_clcapi.h
*
* DESCRIPTION : API definition for CLC (Connection Layer Control) task.
*
* HISTORY     :
*****************************************************************************/
#ifndef _DO_CLCAPI_H_
#define _DO_CLCAPI_H_

/*----------------------------------------------------------------------------
 Include Files
----------------------------------------------------------------------------*/
#include "do_msgdefs.h"
#include "do_fcpapi.h"
#include "syscommon.h"
#include "do_rcpapi.h"
#include "valapi.h"

#ifdef MTK_DEV_C2K_IRAT
#ifdef MTK_DEV_C2K_SRLTE
#include "iratapi.h"
#endif
#endif


/*----------------------------------------------------------------------------
 Global Defines and Macros
----------------------------------------------------------------------------*/
#define MAX_NUM_STANDBY_MEAS_C2K_BAND  (1)

/*----------------------------------------------------------------------------
 Mailbox IDs
----------------------------------------------------------------------------*/
#define CLC_CMD_MAILBOX         EXE_MAILBOX_1_ID
/*
#define CLC_OTAMSG_MAILBOX      EXE_MAILBOX_2_ID
*/
#define CLC_TEST_MAILBOX        EXE_MAILBOX_2_ID

/*----------------------------------------------------------------------------
     Command Message IDs, for CLC task, for CLC_CMD_MAILBOX, EXE_MAILBOX_1_ID
     The messages IDs for components shall also be put in here.
----------------------------------------------------------------------------*/
typedef enum /*_Message_ID_define*/
{
    /* CLC task segment*/
    CLC_xxx_CMD_MSG  = CLC_CMD_MSGID_START,
    CLC_TIMER_EXPIRED_MSG,
    DO_PARM_SET_MSG,
    DO_PARM_GET_MSG,
    CLC_DBM_DO_DATA_WRITE_ACK_MSG,
    CLC_DBM_DO_DATA_READ_MSG,
    CLC_GET_PSW_PARM_RSP_MSG,

#if defined (MTK_PLT_ON_PC_UT) && (MTK_DEV_C2K_IRAT) && defined(MTK_DEV_C2K_SRLTE)
    ERRC_CLC_LTE_SCAN_C2K_ACTIVE_PARAMS_IND,
#endif
    CLC_TASK_CMD_MSGID_LAST,

    /* ALMP segment */
    ALMP_PSW_POWER_MSG = CLC_CMD_MSGID_ALMP_START,
    ALMP_INSP_NTWK_ACQD_MSG,
    ALMP_IDP_CONN_OPENED_MSG,
    ALMP_IDP_CONN_FAILED_MSG,
    ALMP_IDP_PAGE_REQ_MSG,
    ALMP_IDP_FASTCONN_INITIATED_MSG,
    ALMP_CSP_CONN_CLOSED_MSG,
    ALMP_OMP_AN_REDIRECT_MSG,
    ALMP_OMP_SUPERVSN_FAIL_MSG,
    ALMP_CCM_SUPERVSN_FAIL_MSG,
    ALMP_ACM_SUPERVSN_FAIL_MSG,
    ALMP_RTM_SUPERVSN_FAIL_MSG,
    ALMP_FTM_SUPERVSN_FAIL_MSG,
    ALMP_RUP_CONN_LOST_MSG,
    ALMP_RUP_NTWK_LOST_MSG,
    ALMP_RUP_ASSGN_REJ_MSG,
    ALMP_RMC_NTWK_LOST_MSG,
    ALMP_CSS_RE_INIT_MSG,
    ALMP_HLP_REL_CONN_MSG,
    ALMP_HLP_CLOSE_CONN_MSG,
    ALMP_HLP_OPEN_CONN_MSG,
    ALMP_FTAP_OPEN_CONN_MSG,
    ALMP_FTAP_REL_CONN_MSG,
    ALMP_FTAP_CLOSE_CONN_MSG,
    ALMP_RTAP_OPEN_CONN_MSG,
    ALMP_RTAP_REL_CONN_MSG,
    ALMP_RTAP_CLOSE_CONN_MSG,

    ALMP_SCP_OPEN_CONN_MSG,
    ALMP_SCP_REL_CONN_MSG,
    ALMP_SCP_CLOSE_CONN_MSG,
    ALMP_OMP_UPDATE_CMD_MSG,
    ALMP_SMP_CLOSE_CONN_MSG,

    ALMP_ETS_OPEN_CONN_MSG,
    ALMP_ETS_REL_CONN_MSG,
    ALMP_ETS_CLOSE_CONN_MSG,
    ALMP_SCP_RECONFIGURED_MSG,

    ALMP_CSS_SYS_ACQ_REQ_MSG,
    ALMP_CSS_OOSA_WAKEUP_REQ_MSG,
    ALMP_HSC_NTWK_LOST_MSG,
    ALMP_DSAR_QUEUE_FULL_MSG,
    ALMP_IDP_THAW_MSG,
    ALMP_IDP_ACCESS_FAIL_MSG,
    ALMP_CSS_SYSTEM_VALIDATED_MSG,
    ALMP_SMP_POWERDOWN_CNF_MSG,
    ALMP_IRATM_TO_LTE_MEAS_CTRL_CNF,
#ifdef MTK_CBP
    ALMP_NOTIFY_SRV_STATUS_TO_UPPER_LAYER_MSG,
#endif
    CLC_CMD_MSGID_ALMP_LAST,

    /* INSP segment */
    INSP_ALMP_ACTIVATE_MSG = CLC_CMD_MSGID_INSP_START,
    INSP_ALMP_DEACTIVATE_MSG,
    INSP_RMC_PILOT_ACQ_RSP_MSG,
    INSP_RMC_FREEZE_MSG,
    INSP_RMC_THAW_MSG,
    INSP_RMC_SYNC_OK_MSG,
    INSP_RMC_NETWORK_RESET_DONE_MSG,
    INSP_1XASSIST_DOACQ_MSG,
    INSP_CSS_FREQ_RSP_MSG,
    INSP_CSS_OOSA_SLEEP_REQ_MSG,
    INSP_CSS_OOSA_CONT_SLEEP_REQ_MSG,/*used for continuous sleep cmd after OOSA timer expired*/
    INSP_CSS_ACQ_ABORT_REQ_MSG,
    INSP_HSC_OOSA_WAKEUP_IND_MSG,
    INSP_RMC_RF_STATUS_MSG,
    INSP_HSC_SUSPEND_SLEEP_CNF_MSG,
    INSP_OMP_QC_REDIRECT,
    CLC_CMD_MSGID_INSP_LAST,

    /* IDP segment */
    IDP_ALMP_ACTIVATE_MSG = CLC_CMD_MSGID_IDP_START,
    IDP_ALMP_DEACTIVATE_MSG,
    IDP_ALMP_CLOSE_MSG,
    IDP_ALMP_OPEN_CONN_MSG,                  /*_MsgStruct IdpAlmpOpenConnMsgT*/
    IDP_RUP_CONNECTION_INITIATED_MSG,
    IDP_RUP_CONNECTION_OPENED_MSG,
    IDP_RUP_IHO_RESULT_MSG,
    IDP_HSC_FREEZE_MSG,
    IDP_HSC_THAW_MSG,
    IDP_HSC_WAKEUP_IND_MSG,
    IDP_HSC_RESYNC_IND_MSG,
    IDP_HSC_RESYNC_DENIED_MSG,
    IDP_RMC_DOHWRFREQUEST_FAIL_MSG,
    IDP_ACM_TX_STARTED_MSG,
    IDP_ACM_TX_ENDED_MSG,
    IDP_CCM_SLEEP_CAPSULE_DONE_MSG,
    IDP_CSS_SEARCH_STARTED_MSG,
    IDP_CSS_SEARCH_ENDED_MSG,
    IDP_SMP_SESSION_OPENED_MSG,
    IDP_SMP_SESSION_CLOSED_MSG,
    IDP_OMP_UPDATED_MSG,   /*_MsgStruct IdpOmpUpdatedMsgT*/
    IDP_CSP_SUSPEND_PERIOD_STATUS_MSG,  /*_MsgStruct IdpCspSuspendPeriodStatusMsgT*/
    IDP_CSP_CONNECTION_CLOSED_MSG,
    IDP_AMP_SESSIONSEED_MSG,                /*_MsgStruct IdpAmpSessionSeedMsgT*/
    IDP_CCM_OFFSET_MSG,                         /*_MsgStruct IdpCcmOffsetMsgT*/
    IDP_SLOTTED_MODE_SET_MSG,            /*_MsgStruct IdpSlottedModeSetMsgT*/
    IDP_1XPS_PAGINGMASK_SET_MSG,
    IDP_SCP_RECONFIGURED_MSG,
    IDP_SCP_COMMITTED_MSG,
    IDP_RMC_DEACTIVATE_IND_MSG,
    IDP_ETS_HANDOFF_MSG,
    IDP_ALMP_SYSINFO_UPDATE_MSG,
    IDP_DO_SYS_STATUS_GET_MSG,
    IDP_DSAR_ACCESS_STARTED_MSG,
    IDP_DSAR_ACCESS_ENDED_MSG,
    IDP_PREF_CC_CYCLE_SET_MSG,
    IDP_EXTENDED_SLOTCYCLE_SET_MSG,
    IDP_RUP_CHANNEL_CHANGED_IND_MSG,
    IDP_RUP_FORCE_IHO_RSP_MSG,

    IDP_IRAT_MEAS_REQ,
    IDP_IRAT_HANDOFF_REQ,
    IDP_OMP_OTHERRAT_UPDATED_MSG,
    IDP_CSS_MEAS_SENT_IND,
    IDP_SMP_SESSION_STATUS_MSG,
    IDP_IRAT_DO_IDLE_LONG_SLEEP_REQ,
    IDP_IRAT_DO_IDLE_LONG_SLEEP_WAKEUP_REQ,
    IDP_HSC_TX_AVAILABLE_IND,
    IDP_RUP_DFS_END_MSG,
    IDP_DFS_TIMER_SET_MSG,
    IDP_RMC_FREEZE_MSG,
    IDP_RMC_THAW_MSG,
    IDP_CSS_RSVAS_VIRTUAL_SUSPEND_REQ_MSG,
    IDP_CSS_RSVAS_RESUME_REQ_MSG,
    IDP_PSW_RSVAS_SERVICE_OCCUPY_CNF_MSG,
    IDP_PSW_RSVAS_SERVICE_OCCUPY_IND_MSG,
    IDP_VAL_PDN_SETUP_STATUS_NOTIFY_IND_MSG,
    IDP_PSW_1XRTT_PAGE_POSITION_CHANGE_IND_MSG,
    CLC_CMD_MSGID_IDP_LAST,

    /* CSP segment */
    CSP_ALMP_ACTIVATE_MSG = CLC_CMD_MSGID_CSP_START,
    CSP_ALMP_DEACTIVATE_MSG,
    CSP_ALMP_CLOSE_CONN_MSG,
    CLC_CMD_MSGID_CSP_LAST,

    /* OMP segment */
    OMP_IDP_ACTIVATE_MSG = CLC_CMD_MSGID_OMP_START,
    OMP_CSP_ACTIVATE_MSG,
    OMP_IDP_DEACTIVATE_MSG,
    OMP_ALMP_DEACTIVATE_MSG,
    OMP_RUP_IDLE_HANDOFF_MSG,
    OMP_CSP_CONN_CLOSED_MSG,
    OMP_SCP_RECONFIGURED_MSG,
    OMP_SCP_COMMITTED_MSG,
    OMP_ACM_SYS_PARMS_UPD_REQ,
    OMP_IRAT_MCC_REQ,
    OMP_CSS_SYSTEM_VALIDATED_MSG,
    CLC_CMD_MSGID_OMP_LAST,
/*IRAT*/
    /* IRATM segment */
    IRATM_OMP_OTHERRAT_UPDATED_MSG = CLC_CMD_MSGID_IRATM_START,
    IRATM_RUP_IDLE_HANDOFF_MSG,
    IRATM_RMC_CCELL_MEASUREMENT_CNF,
    IRATM_RMC_SYNC_MSG_ACQ_START_IND,
    IRATM_RMC_SYNC_MSG_ACQ_TMR_OUT,
    IRATM_RMC_CCELL_MEAS_IND,
    IRATM_RMC_ACQ_CGI_CNF,
    IRATM_RMC_CGI_ACQ_START_IND,
    IRATM_RMC_CGI_ACQ_TMR_OUT,
    IRATM_RMC_PILOT_ACQ_FAILED_IND,
    IRATM_RMC_SET_RAT_CNF,
    IRATM_CSS_SET_RAT_REQ,
    IRATM_CSS_SUSPEND_REQ,
    IRATM_CSS_VIRTUAL_SUSPEND_REQ,
    IRATM_RUP_CUR_SECTOR_MEAS_CNF,
    IRATM_RUP_CUR_SECTOR_SIG_CHANGED_IND,
    IRATM_CSS_TO_LTE_MEAS_CTRL_REQ,
    IRATM_CSS_PLMN_LIST_UPDATE_REQ,
    IRATM_CSS_TO_LTE_RESEL_FAIL_INFO,
    IRATM_CSS_WAKEUP_CMP_RSP,
    IRATM_ALMP_TO_LTE_MEAS_CTRL_REQ,
    IRATM_TIMER_EXPIRED_CMD_MSG,
    IRATM_HSC_FREEZE_MSG,
    IRATM_HSC_THAW_MSG,
    IRATM_RMC_RX_STOP_IND,
    IRATM_CSS_SUSPEND_RESUME_REQ,
    IRATM_CSS_VIRTUAL_SUSPEND_RESUME_REQ,
    IRATM_CSS_POWER_CTRL_REQ,
#ifdef MTK_PLT_ON_PC_UT
    CAS_EAS_LTE_MEASUREMENT_CNF,
    CAS_EAS_LTE_MEASUREMENT_IND,
    CAS_EAS_EVALUATE_ECELL_CNF,
    CAS_EAS_EVALUATE_ECELL_STOP_CNF,
    CAS_EAS_ACTIVATE_ECELL_CNF,
    CAS_EAS_LTE_MEASUREMENT_REQ,
    CAS_EAS_LTE_HPS_QUALIFY_CELL_IND,
    CAS_EAS_EVALUATE_ECELL_REQ,
    CAS_EAS_EVALUATE_ECELL_STOP_REQ,
    CAS_EAS_ACTIVATE_ECELL_REQ,
    CAS_EAS_C2K_POWER_ON_IND,
    CAS_EAS_C2K_POWER_OFF_IND,
    EAS_CAS_CONFIG_CCELL_MEAS_REQ,
    EAS_CAS_REPORT_CGI_REQ,
    EAS_CAS_ACTIVATE_CCELL_REQ,
    EAS_CAS_PARAM_UPDATE_IND,
    EAS_CAS_CONFIG_CCELL_MEAS_CNF,
    EAS_CAS_CCELL_MEAS_IND,
    EAS_CAS_REPORT_CGI_CNF,
    EAS_CAS_ACTIVATE_CCELL_CNF,
    EAS_CAS_LTE_POWER_ON_IND,
    EAS_CAS_LTE_POWER_OFF_IND,
#endif
    CLC_CMD_MSGID_IRATM_LAST,
#ifdef MTK_PLT_ON_PC_UT
    UT_CLC_DSAF_FWD_MSG,
#endif
    CLC_CMD_MSGID_LAST
} ClcCmdMsgT;

/*----------------------------------------------------------------------------
     OTA Message IDs, for CLC_OTAMSG_MAILBOX
----------------------------------------------------------------------------*/
typedef enum
{
    ALMP_DSA_OTA_MSG = CLC_OTAMSG_MSGID_START,

    CLC_OTA_MSG_MSGID_LAST
} ClcOTAMsgT;

typedef enum
{
    CLC_CMD_INITACQ_ETS,
    CLC_CMD_CONNREQ_ETS,
    CLC_CMD_CONNREL_ETS,
    CLC_ETS_GET_STATUS_MSG,
    CLC_CMD_POWERDOWN_ETS,
    CLC_CMD_POWERUP_ETS,
    CLC_SETUP_CONNECTION_ETS,
    CLC_REL_CONNECTION_ETS,
    CLC_CMD_ALU_NW_TEST_ETS,
#if defined (MTK_DEV_OPTIMIZE_EVL1)
    CLC_CMD_NST_ETS,  /* NST: start NST */
#endif
#if defined (MTK_DEV_C2K_SRLTE)
    CLC_CMD_OTHERRATNBL_ETS,
#endif
    CLC_CMD_NUM
} ClcTstMsgT;

typedef enum
{
   CLC_NETWORK_ERROR_EVDO_CO_NO_SERVICE,
   CLC_NETWORK_ERROR_EVDO_CO_ACCESS_FAILURE,
   CLC_NETWORK_ERROR_EVDO_CO_REDIRECTION,
   CLC_NETWORK_ERROR_EVDO_CO_NOT_PREFERRED,
   CLC_NETWORK_ERROR_EVDO_CO_MODE_HANDOFF,
   CLC_NETWORK_ERROR_EVDO_CO_IN_PROGRESS,
   CLC_NETWORK_ERROR_EVDO_CO_SETUP_TIMEOUT,
   CLC_NETWORK_ERROR_EVDO_CO_SESSION_NOT_OPEN,
   CLC_NETWORK_ERROR_EVDO_RELEASE_NO_REASON,
   CLC_NETWORK_ERROR_EVDO_PROTOCOL_FAILURE,
   CLC_NETWORK_ERROR_EVDO_DENY_NO_REASON,
   CLC_NETWORK_ERROR_EVDO_DENY_NETWORK_BUSY,
   CLC_NETWORK_ERROR_EVDO_DENY_AUTHENTICATION,
   CLC_NETWORK_ERROR_EVDO_REDIRECT_TO_1X,
   CLC_NETWORK_ERROR_EVDO_FADE,
   CLC_NETWORK_ERROR_EVDO_USER_DISCONNECTED
#if defined (MTK_DEV_C2K_IRAT) && defined (MTK_DEV_C2K_SRLTE)
   ,
   CLC_NETWORK_ERROR_EVDO_RSVAS_OCCUPY_FAIL
#endif
}ClcConnFailT;

typedef enum
{
   CLC_CONNDENY_BUSY_GENERAL,
   CLC_CONNDENY_NOTCA_NORTCACK,
   CLC_CONNDENY_AUTH_BILLING,
   CLC_UATI_FAILURE,
   CLC_NW_CLOSE_SESSION,
   CLC_OTHER_MSG_EXC_FAILURE,
   CLC_AVOID_CH,
   CLC_ERR_NUM
}ClcErrCauseT;

typedef enum
{
   CLC_DATA,
   CLC_SESSION
}ClcConnTypeT;

/*----------------------------------------------------------------------------
     define signals used by CLC task
----------------------------------------------------------------------------*/

#define CLC_xx1_SIGNAL                  EXE_SIGNAL_2
#define CLC_xxx2_SIGNAL                 EXE_SIGNAL_3
#define CLC_xxx3_SIGNAL                 EXE_SIGNAL_4



/*----------------------------------------------------------------------------
     Message Formats structure
----------------------------------------------------------------------------*/
/*CLC_TIMER_EXPIRED_MSG*/
typedef struct
{
   uint32   TimerId;
} ClcTimerExpiredMsgT;

typedef struct
{
   uint8 ConnCloseReason;
}CspAlmpCloseConnMsg;

/* IDP_HSC_RESYNC_DENIED_MSG */
/* This is also used for ETS command, but since it is a uint32, so removal of PACKED is ok*/
typedef struct
{
   uint32 PrevWakeFrameLower32;
} IdpHscResyncDeniedMsgT;

typedef  struct
{
  bool bIdleHO;
}IdpRupIHOResultMsg;

typedef enum
{
  AT_PWROFF, /*Same as AT_INACTIVE, but can be used to check powerup */
  AT_INACTIVE,
  AT_PILOTACQ,
  AT_SYNC,
  AT_IDLE,
  AT_ACCESS,
  AT_CONNECTED
}EEDOATStateEnumT;

typedef enum
{
   POWER_DOWN,
   POWER_UP
}PowerStateT;

typedef PACKED_PREFIX struct
{
   EEDOATStateEnumT  ATState;
   uint8     AlmpState;
   uint8     InspState;
   uint8     IdpState;
   uint8     CspState;
   uint8     RupState;
   uint8     OmpState;
   bool      bHybridOn;
} PACKED_POSTFIX  MonSpyL3StateT;

#if defined (MTK_DEV_OPTIMIZE_EVL1)
typedef PACKED_PREFIX struct
{
   bool   NstEvdoRev0;  /* EVDO Rev.0 NST */
   bool   NstEvdoRevA;  /* EVDO Rev.A NST */
} PACKED_POSTFIX ClcNstTstMsgT;
#endif

#if defined (MTK_DEV_C2K_SRLTE)
typedef PACKED_PREFIX struct
{
   bool  MeasOn;
   bool  priorityIncl;
   uint8 servingPriority;
   uint8 threshserving;
   bool  perEarfcnParaIncl;
   uint8 rxLevMinEUTRACommon;
   uint8 pEMaxCommon;
   bool  pLMNIDIncl;
   //Eutran freq info
   uint8 numEutraFreq;
   //freq 0
   uint16 earfcn0;
   uint8 priority0;
   uint8 threshX0;
   //freq 1
   uint16 earfcn1;
   uint8 priority1;
   uint8 threshX1;
   //freq 2
   uint16 earfcn2;
   uint8 priority2;
   uint8 threshX2;
}PACKED_POSTFIX ClcOtherRatNblMsgT;
#endif

typedef PACKED_PREFIX struct
{
   uint8             OvhFlag;
   uint8             SysType;
   uint8             BandClass;
   uint16            Channel;
   uint16            PilotPN;
   uint16            Interval;
   uint8             RmcRFMode;
   uint8             RFPaths;
   uint8             C3Enabled;
   uint8             SessionAutoMode;
   bool              CssEnabled;
} PACKED_POSTFIX ClcInitTstMsgT;

typedef PACKED_PREFIX struct
{
   uint8 BandClass;
   uint16 Channel;
   uint16 PilotPN;
} PACKED_POSTFIX IdpEtsHandoffMsgT;

/* CLC_HSC_OOSA_WAKEUP_IND_MSG */
typedef  struct
{
  OosaWakeupTypeT  OosaWakeupType;
} ClcHscOosaWakeupIndMsgT;

#ifdef CCM_ACM_DSA_TEST
typedef enum
{
    REVA_TEST_DATA_RATE_0,
    REVA_TEST_DATA_RATE_1,
    REVA_TEST_DATA_RATE_2,
    REVA_TEST_DATA_RATE_3,
    REVA_TEST_DATA_RATE_4,
    REVA_TEST_DATA_RATE_5,
    REVA_TEST_DATA_RATE_6,
    REVA_TEST_DATA_RATE_7,
    REVA_TEST_DATA_RATE_8,
    REVA_TEST_DATA_RATE_9,
    REVA_TEST_DATA_RATE_10,
    REVA_TEST_DATA_RATE_11,
    REVA_TEST_DATA_RATE_12,
    REVA_TEST_DATA_RATE_12_TEST,
    REVA_TEST_DATA_RATE_CIRCULAR,
    REVA_TEST_DATA_RATE_QOS
} RevATestDataRateT;

typedef enum
{
    REV0_TEST_DATA_RATE_REAL,
    REV0_TEST_DATA_RATE_0,
    REV0_TEST_DATA_RATE_1,
    REV0_TEST_DATA_RATE_2,
    REV0_TEST_DATA_RATE_3,
    REV0_TEST_DATA_RATE_4,
    REV0_TEST_DATA_RATE_5,
    REV0_TEST_DATA_RATE_5_TEST,
    REV0_TEST_DATA_RATE_CIRCULAR

} Rev0TestDataRateT;

typedef enum
{
    REVA_TEST_MAX_SUB_PKT_NUM_REAL,
    REVA_TEST_MAX_SUB_PKT_NUM_0,
    REVA_TEST_MAX_SUB_PKT_NUM_1,
    REVA_TEST_MAX_SUB_PKT_NUM_2,
    REVA_TEST_MAX_SUB_PKT_NUM_3
} RevATestMaxNumSubPktT;

#if defined MTK_PLT_ON_PC
#pragma pack( push, saved_packing, 1 )
#endif

typedef PACKED_PREFIX struct
{
   uint8  AutoCallSetup;
   uint8  RevProtocol;
   RevATestDataRateT  RevATestDataRate;
   uint8  RevATestDRCValue;
   Rev0TestDataRateT Rev0TestDataRate;
   uint8  TestRABInfo; /* 0 : real mbp, 1 : alternate, 2: all-up, 3: all-down */
   uint8  CloseLoopEnable; /* 0 : disable, 1: enable */
   uint8  HwAckEnable; /* 0 : disable, 1: enable */
   uint8  RevATestHarq; /* 1 : ack, 0: nak */
   RevATestMaxNumSubPktT  RevATestMaxNumSubPkt; /* Real,0~3 */
/* temprorary use RTM_TM_LOLAT mode for Lucent AN and RTM_TM_HICAP mode for Agilent now */ /* need to implement real mode later */
   uint8  RevATestTransmitMode; /* 0 : RTM_TM_HICAP, 1 : RTM_TM_LOLAT, 2 : Real */
   uint8  TxDelayLoadingTestMode; /* 1 : enable */
   uint8  RTM3FixedQueueTestMode; /* 1 : enable */
   uint16 RPCTestTriggerSubframeCount;
} PACKED_POSTFIX ClcConnReqTstMsgT;
#if defined MTK_PLT_ON_PC
#pragma pack( pop, saved_packing )
#endif

#endif

typedef struct
{
  uint8  TxStatus;  /* 0-success, 1-failed, 2-aborted */
}IdpAcmTxEndedMsgT;

typedef PACKED_PREFIX struct
{
   ClcConnFailT connFail;
} PACKED_POSTFIX AlmpIdpConnFailMsgT;

typedef PACKED_PREFIX struct
{
    PowerStateT state;
    bool bNetworkLostInd;
} PACKED_POSTFIX IdpAlmpDeactiveMsgT;

typedef struct {
   bool bByPassSessionStatusForSleep;
} IdpSmpSessionStatusMsgT;

typedef struct {
   bool bDueSysTimeFault;
} AlmpCcmSupvFailMsgT;

typedef PACKED_PREFIX struct
{
   uint16 PilotPN;
} PACKED_POSTFIX OmpRupIdleHandoffMsgT;

typedef enum
{
   DO_PARM_INIT_MODE,
   DO_PARM_SET_HYBRID_MODE,
   DO_PARM_GET_HYBRID_PREF_MODE,
   DO_PREF_CC_CYCLE_SET_MODE,
   DO_PREF_CC_CYCLE_GET_MODE,
   DO_STR_CONFIGURATION_SET_MODE,
   DO_STR_CONFIGURATION_GET_MODE,
   DO_FORCE_REL0_NEG_SET_MODE,
   DO_FORCE_REL0_NEG_GET_MODE,
   DO_NOT_DISTURB_SET_MODE,
   DO_NOT_DISTURB_GET_MODE,
   DO_FT_MAC_DRC_GATING_SET_MODE,
   DO_FT_MAC_DRC_GATING_GET_MODE,
   DO_LUP_UNSOLICITED_SET_MODE,
   DO_LUP_UNSOLICITED_GET_MODE,
   DO_RX_DIVERSITY_CTRL_SET_MODE,
   DO_RX_DIVERSITY_CTRL_GET_MODE,
   DO_SUBTYPE_CONFIG_DATA_SET_MODE,
   DO_SUBTYPE_CONFIG_DATA_GET_MODE,
   DO_SUBTYPE_CONFIG_DATA_DISABLE_SET_MODE,
   DO_EXTENDED_SLOT_CYCLE_SET_MODE,
   DO_EXTENDED_SLOT_CYCLE_GET_MODE,
   DO_PERIODICAL_SEARCH_CYCLE_SET_MODE,
   DO_PERIODICAL_SEARCH_CYCLE_GET_MODE,
   DO_CT_DEFAULTBAND_SET_MODE,
   DO_CT_DEFAULTBAND_GET_MODE,
   DO_PARM_SET_PREF_MODE,
   DO_PARM_GET_PREF_MODE,
   DO_PARM_GET_HYBRID_MODE,
   DO_PARM_SET_HYBRID_PREF_MODE,
   DO_PARM_SET_eHRPD_MODE,
   DO_PARM_GET_eHRPD_MODE,
   DO_PARM_SET_DFS_IDLE_INTERVAL, /* Time between DFS searches in Idle non-slotted mode */
   DO_PARM_GET_DFS_IDLE_INTERVAL,
   DO_PARM_SET_DFS_CONN_INTERVAL, /* Time between DFS searches in Connected state */
   DO_PARM_GET_DFS_CONN_INTERVAL,
   DO_PARM_SET_DFS_IDLE_NUM_CHAN, /* Number of DFS channels to search per DO slot */
   DO_PARM_GET_DFS_IDLE_NUM_CHAN,
   DO_PARM_SET_DFS_CONN_NUM_CHAN, /* Number of DFS channels to search per Conn Timeout */
   DO_PARM_GET_DFS_CONN_NUM_CHAN,
   DO_PARM_SET_DFS_IDLE_ECIO_THRESH, /* Active Pilot strength thresh to start DFS */
   DO_PARM_GET_DFS_IDLE_ECIO_THRESH,
   DO_PARM_SET_DFS_CONN_ECIO_THRESH, /* Average Active Pilot strengths to start DFS */
   DO_PARM_GET_DFS_CONN_ECIO_THRESH,
   DO_PARM_SET_MANUAL_AVOID_SID,
   DO_PARM_GET_MANUAL_AVOID_SID,
   DO_PARM_GET_SHDR_HYBRID_MODE,
   DO_PARM_ALL_ID_MODE
} DoParmOperationId;

typedef enum
{
    DO_PARM_OPERATION_SUCCESS,
    DO_PARM_OPERATION_FAIL_READ_NOT_ALLOWED,
    DO_PARM_OPERATION_FAIL_WRITE_NOT_ALLOWED,
    DO_PARM_OPERATION_NOT_ALLOWED_IN_CLC_STATE,
    DO_PARM_OPERATION_FAIL_INVALID_PTR,
    DO_PARM_OPERATION_FAIL_INVALID_LENGTH,
    DO_PARM_OPERATION_GENERAL_FAILURE,
    DO_PARM_OPERATION_NO_CHANGE_IN_VALUE,
    DO_PARM_OPERATION_FAIL_VALUE_OUT_OF_RANGE,
    DO_PARM_OPERATION_INTERFACE_NOT_SUPPORTED,
    DO_PARM_OPERATION_RESULT_END_LIST
} DoParmAccessResultCode;

typedef enum
{
  DO_PARM_MIN_VALUE,
  DO_PARM_MAX_VALUE,
  DO_PARM_DEFAULT_VALUE,
  DO_PARM_CUSTOM_VALUE,
  DO_PARM_OP_TYPE_LIST_END
} DoParmOperationType;


typedef PACKED_PREFIX struct
{
   uint32   HybridMode;
   uint32   PrefMode;
   bool     needSaveDbm;
} PACKED_POSTFIX  DoHybridPrefModeData_APIStruct;

typedef PACKED_PREFIX struct
{
   uint32   HybridMode;
   uint32   PrefMode;
   uint16   hscMpaOpModeHwdConfig;
   uint16   hscMpaOpMode;
} PACKED_POSTFIX  DoSHDRHybridModeData_APIStruct;


typedef PACKED_PREFIX struct
{
   uint32   HybridMode;
} PACKED_POSTFIX  DoHybridModeData_APIStruct;

typedef PACKED_PREFIX struct
{
   bool Disable;
} PACKED_POSTFIX  DoeHRPDModeData_APIStruct;

typedef PACKED_PREFIX struct
{
   uint32   PrefMode;
} PACKED_POSTFIX  DoPrefModeData_APIStruct;

typedef PACKED_PREFIX struct
{
   bool     PrefCCCycleEnable;
   uint32   PrefCCCycle;
} PACKED_POSTFIX  DoPrefCCCycleData_APIStruct;

typedef PACKED_PREFIX struct
{
   uint16   Stream0Configuration;
   uint16   Stream1Configuration;
   uint16   Stream2Configuration;
   uint16   Stream3Configuration;
} PACKED_POSTFIX  DoStreamConfiguration_APIStruct;

typedef PACKED_PREFIX struct
{
   bool     ScpForceRel0Config;
} PACKED_POSTFIX  DoForceRel0NegoData_APIStruct;

typedef PACKED_PREFIX struct
{
   uint8    NotDisturb;
} PACKED_POSTFIX  DoNotDisturbData_APIStruct;

typedef PACKED_PREFIX struct
{
   uint8    FtMacDrcGating;
} PACKED_POSTFIX  DoFtMacDrcGatingData_APIStruct;

typedef PACKED_PREFIX struct
{
   bool     LUPUnsolicitedEnable;
 } PACKED_POSTFIX  DoLUPUnsolicitedData_APIStruct;

typedef PACKED_PREFIX struct
{
   uint8    RxDiversityCtrl;
} PACKED_POSTFIX  DoRxDiversityCtrlData_APIStruct;

#define MAX_ATTR_SIZE 5

typedef PACKED_PREFIX struct
{
   uint16 attrId;
   uint8  numValue;
   uint16 value[MAX_ATTR_SIZE];
} PACKED_POSTFIX DoSubTypeConfigData_APIStruct;

typedef PACKED_PREFIX struct
{
   uint16 attrId;
} PACKED_POSTFIX DoSubTypeConfigDataGet_APIStruct;

typedef PACKED_PREFIX struct
{
   uint16 attrId;
} PACKED_POSTFIX DoSubTypeConfigDataDisable_APIStruct;

typedef PACKED_PREFIX struct
{
   UINT8 interval; /* Units of Seconds */
} PACKED_POSTFIX  DoDfsIdleIntervalData_APIStruct;

typedef PACKED_PREFIX struct
{
   UINT8 interval; /* Units of Seconds */
} PACKED_POSTFIX  DoDfsConnIntervalData_APIStruct;

typedef PACKED_PREFIX struct
{
   UINT8 NumChan; /* Number of DFS Channels to search per DO slot */
} PACKED_POSTFIX  DoDfsIdleNumChanData_APIStruct;

typedef PACKED_PREFIX struct
{
   UINT8 NumChan; /* Number of DFS Channels to search per Conn Timeout */
} PACKED_POSTFIX  DoDfsConnNumChanData_APIStruct;

typedef PACKED_PREFIX struct
{
   UINT16 Thresh; /* In units of 0.125 dB */
} PACKED_POSTFIX  DoDfsIdleEcIoThreshData_APIStruct;

typedef PACKED_PREFIX struct
{
   UINT16 Thresh; /* In units of 0.125 dB */
} PACKED_POSTFIX  DoDfsConnEcIoThreshData_APIStruct;

typedef PACKED_PREFIX struct {
   uint16 T_Dfs_TimerMSC;
   uint8 T_Dfs_NumChan;
} PACKED_POSTFIX  IdpDfsTimerSetMsgT;

typedef struct
{
   DoParmOperationId ParmId;
   bool InProgress;
   bool RspNeeded;
   ExeRspMsgT  RspInfo;
   RegIdT      RegId;
} ClcDoDbmRspType;

typedef PACKED_PREFIX struct
{
   uint8    RxDiversityCtrl;
} PACKED_POSTFIX ClcDoRxDiversityCtrlMsgT;

typedef PACKED_PREFIX struct
{
   uint16 T_Dual_Idle;
   uint8 T_HRPD_ExtendedSC;
   uint8 T_1x_ExtendedSC;
} PACKED_POSTFIX DoExtendedSlotCycleData_APIStruct;

typedef PACKED_PREFIX struct
{
   uint16 T_HRPD_Init;
   uint16 T_1x_Init;
} PACKED_POSTFIX DoPeriodicalSearchCycleData_APIStruct;

typedef PACKED_PREFIX struct
{
   CTDoDefaultBandDataT DOSystem[MAX_DO_DEFAULTBAND_SIZE];
} PACKED_POSTFIX DoDefaultBandData_APIStruct;

typedef PACKED_PREFIX struct
{
   bool  NetworkMode;
} PACKED_POSTFIX DoNetworOrRelayMode_APIStruct;

typedef PACKED_PREFIX struct {
   uint16 T_Dual_Idle;
   uint8 T_HRPD_ExtendedSC;
} PACKED_POSTFIX  IdpExtendedSlotCycleSetMsgT;

typedef PACKED_PREFIX struct {
   EEDOATStateEnumT State;
} PACKED_POSTFIX ValClcATStatusMsgT;

typedef struct {
   bool bActiveDueCCMSupvFail;
   bool bTrafficIsSetup;
} IdpAlmpActiveMsgT;

typedef struct {
   bool bSuccess;
} IdpRupForceIHORspMsgT;

typedef struct {
   bool bHONeed;  /*indicated if HO to different channel or not*/
} IdpRupDfsEndMsgT;

typedef struct
{
   uint8    CdmaBand;                /* CDMA band classe to perform init acq */
   uint16   FreqChan;                /* Frequency channel to perform init acq */
} IdpRupChannelChangedIndMsgT;

#ifdef MTK_DEV_C2K_IRAT
typedef PACKED_PREFIX struct
{
  uint16 PnCgi;        /* cell to acquire its sector ID. 0xffff means not specified */
  uint8 Band;          /* CDMA bandclass */
  uint16 Channel;      /* CDMA channel number */
  uint8 SearchWinSize; /* CDMA pilot search window size. 0xff means not provided */
  uint8 NumPn;         /* number of PN included. value 0 means meas done */
  uint16 Pn[1];        /* CDMA pilot PN list to measure */
} PACKED_POSTFIX  IdpIratMeasReqT;

typedef PACKED_PREFIX struct
{
  uint8 Band;          /* target band/channel: if same as current, IDP need not swicth channel. otherwise, switch to this channel */
  uint16 Channel;
  uint8 NumPn;         /* number of PN included. value 0 means not provided by IRAT(switch freq only) */
  uint16 Pn[1];        /* CDMA pilot PN list. This list need to send to RUP to act as neighbor pilot set */
} PACKED_POSTFIX  IdpIratHandoffReqT;

typedef struct
{
   bool bOtherRATUpdated;
}IdpOmpOtherRatUpdatedMsgT;

#if defined (MTK_DEV_C2K_SRLTE_L1) || defined (MTK_DEV_C2K_SRLTE)
/* IDP_VAL_PDN_SETUP_STATUS_NOTIFY_IND_MSG */
typedef struct
{
    bool bPdnSetupIsInprogress;/* TRUE, measn PDN setup signal interactive procedure is inprogress; */
                               /* FALSE,measn PDN setup signal interactive is ended. */
}IdpValPdnSetupStatusNotifyIndMsgT;
#endif /* (MTK_DEV_C2K_SRLTE_L1) || defined (MTK_DEV_C2K_SRLTE) */
#endif /* MTK_DEV_C2K_IRAT */

typedef struct
{
   uint32 nSleepTime;
} IdpIratDOIdleLongSleepReqT;

typedef struct
{
   bool bDueTmrExp;    /* FALSE - normal case, TRUE - due to DSAR timer expired for waiting for Tx available, IDP need to cancel previous wakeup request to HSC */
} IdpDsarAccessEndedMsgT;

/*RAT*/
#ifdef MTK_DEV_C2K_IRAT
#if defined (MTK_DEV_C2K_SRLTE_L1) || defined (MTK_DEV_C2K_SRLTE)
typedef struct
{
    uint16  pn;
    int16   phase;
    uint16  strength;
}CurSectorMeasResultT;

/*IRATM_RMC_CCELL_MEASUREMENT_CNF*/
typedef struct
{
   uint8    tid;           /* Transaction id */
   cas_meas_type_enum         measType;
} IratmRmcCcellMeasurementCnfT;

/*IRATM_RMC_CCELL_MEAS_IND*/
typedef struct
{
    uint8                      tid;                               /* tid is used by EAS to filter the measurement report from CAS, range: 0..255,
                                                                     the CCELL_MEAS_IND corresponding to this CCELL_MEAS_REQ should return the same tid */
    cas_meas_type_enum         measType;                         /* indicate the measurement type of this CCELL_MEAS_REQ
                                                                     - if 'RESEL' type is selected, the set of sectors indicated in this primitive should be a subset of the previously measured sectors
                                                                     - if 'CONNECTED' type is selected, it means EAS is in connected mode and requests CAS to do measurement
                                                                     (in order to distinguish from HP/LP/RESEL modes which used in idle mode)
                                                                     - when this primitive is used to stop measurement, EAS should choose correct type to inform CAS which type of measurement should be stopped */

    uint8                      bandNum;                          /* indicate the number of measurement bands, range: 1..MAX_NUM_MEAS_C2K_BAND_INC */
    cas_meas_band_rslt_struct  bandList[MAX_NUM_STANDBY_MEAS_C2K_BAND];  /* indicate the band measurement result */
} IratmRmcCcellMeasIndT;

/*IRATM_RUP_CUR_SECTOR_MEAS_CNF*/
typedef struct
{
    uint8  Band;
    uint16 Channel;
    CurSectorMeasResultT Pilot;
}IratmRupCurSectorMeasCnfT;


/*IRATM_RMC_ACQ_CGI_CNF*/
typedef struct
{
    uint8  tid;
    uint8  Band;
    uint16 Channel;
    cas_meas_sector_rslt_struct Pilot;
}IratmRmcAcqCgiCnfT;


/*IRATM_ALMP_TO_LTE_MEAS_CTRL_REQ*/
typedef struct
{
    bool    isMeasStart;       /*Indicates whether to lte measurement can be started or
                                 should be stopped*/
}IratmAlmpToLteMeasCtrlReqT;

/*IRATM_CSS_TO_LTE_MEAS_CTRL_REQ*/
typedef struct
{
    bool isMeasStart;    /*Indicates whether to lte measurement can be started or should be stopped*/
    bool abortMeas;      /*Indicate if the mesurement will be aborted
                           TRUE: abort measurement and isMeasStart is set to FALSE*/

    /*Only valid when isMeasStart == TRUE;
    Transfer the remaining time and frequency list of deprioritization info to C2K,C2K will take these EUTRA frequencies as lowest priority when C2K is
    active RAT until the deprioritization timer expiry.*/
    eas_deprioritization_info_struct easDeprioritizationInfo;

}IratmCssToLteMeasCtrlReqT;

/*IRATM_CSS_TO_LTE_RESEL_FAIL_INFO*/
typedef struct
{
    uint32 Frequency;
    uint16 Pci;
    ts_eval_activate_fail_handle activateFailHandle;
}IratmCssToLteReselFailInfoT;

/*IRATM_CSS_SET_RAT_REQ*/
typedef struct
{
    srlteRatTypeT ratType;
    bool          needCnf;
}IratmCssSetRatReqT;


/*IRATM_CSS_POWER_CTRL_REQ*/
typedef struct
{
    bool powerStatus; /*0: power off; 1: power on */
}IratmCssPowerCtrlReqT;


/*IRATM_CSS_PLMN_LIST_UPDATE_REQ */
typedef struct
{
    gmss_css_sim_plmn_info_ind_struct PlmnInfo;
}IratmCssPlmnListUpdateReqT;


/*IRATM_RUP_CUR_SECTOR_SIG_CHANGED_IND*/
typedef struct
{
  bool  bLessThanThreshold;           /*Indicate if pilot strength less than threshhold*/
  uint8 Band;                        /* CDMA bandclass */
  uint16 Channel;                    /* CDMA channel number */
  CurSectorMeasResultT Pilot;         /* CDMA pilot measure result */
} IratmRupCurSectorSigChangedIndT; /* content same as xxxCurSysMeasRspT */

/* RMC_IRATM_CCELL_MEAS_REQ */
typedef struct
{
    uint8                  tid;          /* tid is used by EAS to filter the measurement report from CAS, range: 0..255
                                                the CCELL_MEAS_IND corresponding to this CCELL_MEAS_REQ should return the same tid */
    cas_meas_type_enum     measType;    /* indicate the measurement type of this CCELL_MEAS_REQ
                                                - if 'RESEL' type is selected, the set of sectors indicated in this primitive should be a subset of the previously measured sectors
                                                - if 'CONNECTED' type is selected, it means EAS is in connected mode and requests CAS to do measurement
                                                  (in order to distinguish from HP/LP/RESEL modes which used in idle mode)
                                                - when this primitive is used to stop measurement, EAS should choose correct type to inform CAS which type of measurement should be stopped */
    uint8                  measPeriod;  /* indicate the absolute measurement period used by C2K to report the measurement results to LTE
                                                this field is only valid if 'meas_type' = HP_MODE, represent in 'seconds' */
    uint8                  bandNum;     /* indicate the number of measurement bands, range: 0..MAX_NUM_MEAS_C2K_BAND_INC
                                                if band_num=0, CAS should stop the measurement specified in the 'meas_type' */
    cas_meas_band_info_struct  bandList[MAX_NUM_MEAS_C2K_BAND_INC];  /* indicate the measurement information */

}RmcIratmCcellMeasReqT;
#endif
#endif
/*RAT*/
extern MonSpyL3StateT  gL3StateSpy;
extern uint8 MacJammerInd;

#if defined (MTK_DEV_OPTIMIZE_EVL1)
extern bool  NstEvdoRev0; /* EVDO Rev.0 NST */
extern bool  NstEvdoRevA; /* EVDO Rev.A NST */
#define MTK_DEV_NST_DO_REV0   (NstEvdoRev0)
#define MTK_DEV_NST_DO_REVA   (NstEvdoRevA)
#define MTK_DEV_NST_DO        (NstEvdoRev0 || NstEvdoRevA)
#endif

#if defined (MTK_DEV_C2K_SRLTE)
extern ClcOtherRatNblMsgT OtherRatNblMsg;
extern eas_cas_param_update_ind_struct easCasParamUpdateIndMsg;
extern RmcIratmCcellMeasReqT rmcIratmCcellMeasReq;
#endif

/* insp.c */
extern void ProcessInspFreezeMsg (void);
extern void ProcessInspThawMsg (void);
extern bool AlmpSetATState(EEDOATStateEnumT State);

#if defined(MTK_DEV_C2K_IRAT) && defined(MTK_DEV_C2K_SRLTE)
extern bool IdpCheckIfFastConnIsInProgress(void);
extern void ProcessIratmAlmpToLteMeasCtrlReq(void* pMsg);
extern void IratmSetOmpUpdateFlag (bool flag);
extern void IratmSetModeChangeIndex (bool flag);
extern bool IratmGetModeChangeIndex (void);
extern bool IratmJudgeIfActiveRatState (void);
#endif

/*Do parms*/
void ClcDoDataWriteAck(DbmWriteRspMsgT * MsgDataPtr );
void ClcDoParmSetMsg(void *MsgDataPtr);
void ClcDoParmGetMsg(void *MsgDataPtr);
void ClcDoDataInit(void);
void ClcDoDataReadMsg(void *MsgDataPtr);
void ClcDoDataValUpdateReqMsg(void *MsgDataPtr);
void ClcOmpGetOvhdInfo(ValEvdoOvhdInfoT*  pInfo);
void IdpGetActiveChannelInfo(RmcIdpRxActivateMsgT* pChan, uint32* pUATI);
bool IdpSessionSeedForChHashCheck(uint32 SessionSeedToUse);
uint8 IdpGetCurCcmOffset(void);
bool ClcDsaValidMsg(uint8 Type,uint8 MsgId);
bool ClcGetRevState(void);

#ifdef MTK_DEV_C2K_IRAT
void ClcOmpSetMccInfo(void);
#ifdef MTK_DEV_C2K_SRLTE
void IdpSendSignalProtectStatusInd(bool bSignalNeedProtect);
#endif  /* MTK_DEV_C2K_SRLTE */
#endif  /* MTK_DEV_C2K_IRAT */
/*****************************************************************************
* End of File
*****************************************************************************/
#endif
