/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef CP_NUCLEUS_H
#define CP_NUCLEUS_H

/*****************************************************************************
 
  FILE NAME:  cp_nucleus.h

  DESCRIPTION:

    This file extends the Nucleus nucleus.h file.

*****************************************************************************/

#include "nucleus.h"            /* Include the base file */



#ifdef __cplusplus
extern  "C" {                               /* C declarations in C++     */
#endif  /* __cplusplus */

#undef PLUS_1_11
#undef PLUS_1_13
#undef PLUS_VERSION_COMP

#undef NU_System_Clock_Frequency
#undef NU_PLUS_Tick_Rate
#undef NU_PLUS_Ticks_Per_Second
#undef NU_HW_Ticks_Per_Second
#undef NU_HW_Ticks_Per_SW_Tick
#undef NU_COUNT_DOWN
#undef NU_Retrieve_Hardware_Clock


#ifndef         NU_SOURCE_FILE

#define         NU_Adjust_Active_Timers         TMF_Adjust_Active_Timers
VOID            NU_Adjust_Active_Timers(UNSIGNED adj_time);

#endif  /* NU_SOURCE_FILE */

#ifdef  __cplusplus
}                                           /* End of C declarations     */
#endif  /* __cplusplus */

#endif    /* CP_NUCLEUS_H */

/*****************************************************************************
* $Log: cp_nucleus.h $
* Revision 1.2  2004/03/25 11:45:47  fpeng
* Updated from 6.0 CP 2.5.0
* Revision 1.1  2003/05/12 15:26:12  fpeng
* Initial revision
* Revision 1.5  2002/06/04 08:07:09  mshaver
* Added VIA Technologies copyright notice.
* Revision 1.4  2002/05/13 15:17:47  mshaver
* Added rcs log.
*****************************************************************************/

