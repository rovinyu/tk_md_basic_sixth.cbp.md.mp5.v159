/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef DBMAPI_H
#define DBMAPI_H
/*****************************************************************************
 
  FILE NAME:  dbmapi.h

  DESCRIPTION:

    This file contains all constants and typedefs needed to interface
    with the DBM unit via the Exe mail service routines.

*****************************************************************************/

#include "sysapi.h"
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
#include "dbmrfcust.h"
#if (SYS_BOARD >= SB_JADE)
#include "dbmmipi.h"
#include "dbmpoc.h"
#include "dbmdrdi.h"
#endif
#endif /* MTK_CBP */

#if defined(SYS_FLASH_LESS_SUPPORT) && defined(MTK_DEV_VERSION_CONTROL)
#include "rfsimageinfo.h"
#endif
/*------------------------------------------------------------------------
* Define constants used in DBM API
*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
* EXE Interfaces - Definition of Signals and Mailboxes
*------------------------------------------------------------------------*/

/* DBM command mailbox id */
#define DBM_MAILBOX         EXE_MAILBOX_1_ID

/* DBM flash marker size in bytes. Marker used to distinguish active
   flash data bases */
#define DBM_MARKER_SIZE     2

/**************************************************/
/* Define Data Base 1 (CP) Constants              */
/**************************************************/
#define DBM_CP_FLASH_OFFSET 0  /* The CP data base starts at this offset 
                                  from the flash sector address */

/* Define data base Constants. These constants must be updated when adding
** new segments to this data base - NOTE: In order optimize cache memory allocation
** for customer builds that may not support CP features, these constants are
** preprocessor compile option dependant. */

#ifdef MTK_DEV_VERSION_CONTROL
/* Define CP segment sizes in bytes */
typedef enum
{
   DBM_PSW_NAM1_SIZE          = 548,
   DBM_PSW_MS_CAP_DB_SIZE     = 130,
   DBM_UICC_SIZE              = 20,  /* Previous Misc UI. Now place holder available for reuse */
   DBM_PSW_NAM2_SIZE          = 548, /* Same size as DBM_PSW_NAM1_SIZE */
   DBM_SECURE_DATA_SEG_SIZE   = 448, /* To match Size 3 Msg Buffer Size */
   DBM_DO_DATA_SEG_SIZE       = 350,
   DBM_UI_MISC_SIZE           = 128,
   DBM_HLP_IPCOUNTERS_SIZE    = 128
} DbmCPSegmentSizeT;

/*Define CP Cust segment sizes in bytes*/
typedef enum
{
   DBM_PSW_MRU1_SIZE          = 30,
   DBM_PSW_MRU2_SIZE          = 30,  /* Same size as DBM_PSW_MRU1_SIZE */
   DBM_DO_MRU_SIZE            = 30,
   DBM_CSS_1X_SIZE            = 256, /* storage for CSS 1X parameters */
   DBM_CSS_DO_SIZE            = 256, /* storage for CSS DO parameters */
   DBM_CSS_MISC_SIZE          = 128, /* storage for CSS Misc parameters */
   DBM_PSW_MISC_SIZE          = 20,  /* Additional bytes allocated for any future requirements */
   DBM_CUSTOMIZE_SIZE         = 6,   /*used for customized feature*/
   DBM_HSPD_SEG_SIZE          = 2880,
   DBM_HSPD_SECURE_SEG_SIZE   = 968  /* Encryption of data requires total to be a multiple of 4 */
#ifdef CBP7_EHRPD
   ,DBM_EHRPD_SIZE            = 554  /*current used 352 for eHRPD, reserved 48 bytes for future extension*/
#endif
}DbmCPCustSegmentSizeT;

typedef enum 
{
/*CP segment defines*/
   DBM_PSW_NAM1_SEG = 0,
   DBM_PSW_MS_CAP_DB_SEG,
   DBM_UICC_SEG,
   DBM_PSW_NAM2_SEG,
   DBM_SECURE_DATA_SEG, 
   DBM_DO_DATA_SEG,
   DBM_UI_MISC_SEG,
   DBM_HLP_IPCOUNTERS_SEG,
   DBM_MAX_SEG_CP_DB,

/*CP Cust segment defines*/
   DBM_PSW_MRU1_CUST_SEG = DBM_MAX_SEG_CP_DB,
   DBM_PSW_MRU2_CUST_SEG,
   DBM_DO_MRU_CUST_SEG,
   DBM_CSS_1X_CUST_SEG,
   DBM_CSS_DO_CUST_SEG,
   DBM_CSS_MISC_CUST_SEG,
   DBM_PSW_MISC_CUST_SEG,
   DBM_CUSTOMIZE_CUST_SEG,
   DBM_HSPD_CUST_SEG,
   DBM_HSPD_SECURE_CUST_SEG,   
#ifdef CBP7_EHRPD
   DBM_EHRPD_CUST_SEG,
#endif 
   DBM_MAX_SEG_CP_CUST_DB
   
} DbmCPSegmentT;

#define DBM_CUST_SEG_BASE DBM_MAX_SEG_CP_DB
#define DBM_CUST_SEG_NUM (DBM_MAX_SEG_CP_CUST_DB - DBM_CUST_SEG_BASE)

/* Define CP data base segment sizes */

#define DBM_CP_SEGMENT_SIZES \
   DBM_PSW_NAM1_SIZE, \
   DBM_PSW_MS_CAP_DB_SIZE, \
   DBM_UICC_SIZE, \
   DBM_PSW_NAM2_SIZE, \
   DBM_SECURE_DATA_SEG_SIZE, \
   DBM_DO_DATA_SEG_SIZE,\
   DBM_UI_MISC_SIZE,\
   DBM_HLP_IPCOUNTERS_SIZE



/* Define CP cust data base segment sizes */
#ifdef CBP7_EHRPD
#define DBM_CP_CUST_SEGMENT_SIZES \
   DBM_PSW_MRU1_SIZE, \
   DBM_PSW_MRU2_SIZE, \
   DBM_DO_MRU_SIZE,   \
   DBM_CSS_1X_SIZE,\
   DBM_CSS_DO_SIZE,\
   DBM_CSS_MISC_SIZE,\
   DBM_PSW_MISC_SIZE, \
   DBM_CUSTOMIZE_SIZE,\
   DBM_HSPD_SEG_SIZE, \
   DBM_HSPD_SECURE_SEG_SIZE,\
   DBM_EHRPD_SIZE
#else
#define DBM_CP_CUST_SEGMENT_SIZES \
   DBM_PSW_MRU1_SIZE, \
   DBM_PSW_MRU2_SIZE, \
   DBM_DO_MRU_SIZE,   \
   DBM_CSS_1X_SIZE,\
   DBM_CSS_DO_SIZE,\
   DBM_CSS_MISC_SIZE,\
   DBM_PSW_MISC_SIZE, \
   DBM_CUSTOMIZE_SIZE,\
   DBM_HSPD_SEG_SIZE, \
   DBM_HSPD_SECURE_SEG_SIZE
#endif
#else /*#ifdef MTK_DEV_VERSION_CONTROL*/
/* Define CP segment sizes in bytes */
typedef enum
{
   DBM_PSW_NAM1_SIZE          = 548,
   DBM_PSW_MRU1_SIZE          = 30,
   DBM_PSW_MS_CAP_DB_SIZE     = 130,
   DBM_UICC_SIZE              = 20,  /* Previous Misc UI. Now place holder available for reuse */
   DBM_PSW_MISC_SIZE          = 20,  /* Additional bytes allocated for any future requirements */
   DBM_PSW_NAM2_SIZE          = 548, /* Same size as DBM_PSW_NAM1_SIZE */
   DBM_PSW_MRU2_SIZE          = 30,  /* Same size as DBM_PSW_MRU1_SIZE */
   DBM_SERVICE_HISTORY_SIZE   = 41,
   DBM_DBM_TIMESTAMP_SIZE     = 20,
   DBM_SECURE_DATA_SEG_SIZE   = 448, /* To match Size 3 Msg Buffer Size */
   DBM_HSPD_SEG_SIZE          = 2880,
   DBM_HSPD_SECURE_SEG_SIZE   = 968, /* Encryption of data requires total to be a multiple of 4 */
   DBM_DO_DATA_SEG_SIZE       = 350,
   DBM_DO_MRU_SIZE            = 30,
   DBM_EHRPD_SIZE             = 554, /*current used 352 for eHRPD, reserved 48 bytes for future extension*/
   DBM_HLP_IPCOUNTERS_SIZE    = 128,
   DBM_MSC_WMCMSMODE_SIZE     = 30,
   DBM_CUSTOMIZE_SIZE         = 6,   /*used for customized feature*/
   DBM_GPIO_IOCIOD_SIZE       = 24,
   DBM_AUDIO_MICG_SIZE        = 12, 
   DBM_UI_MISC_SIZE           = 128,
   DBM_CSS_1X_SIZE            = 256, /* storage for CSS 1X parameters */
   DBM_CSS_DO_SIZE            = 256, /* storage for CSS DO parameters */
   DBM_CSS_MISC_SIZE          = 128  /* storage for CSS Misc parameters */
} DbmCPSegmentSizeT;

 
/* Define all segment constants utilizing segments
   ** designated for use with Dual NAM implementation */

/* Define CP data base segment names */
typedef enum 
{
   DBM_PSW_NAM1_SEG,
   DBM_PSW_MRU1_SEG,
   DBM_PSW_MS_CAP_DB_SEG,
   DBM_UICC_SEG,
   DBM_PSW_MISC_SEG,
   DBM_PSW_NAM2_SEG,
   DBM_PSW_MRU2_SEG,
   DBM_SERVICE_HISTORY_SEG,
   DBM_DBM_TIMESTAMP_SEG,
   DBM_SECURE_DATA_SEG,
   DBM_HSPD_SEG,
   DBM_HSPD_SECURE_SEG,   
   DBM_DO_DATA_SEG,
   DBM_DO_MRU_SEG,
   DBM_EHRPD_SEG,
   DBM_HLP_IPCOUNTERS_SEG,
   DBM_MSC_WMCMSMODE_SEG,
   DBM_CUSTOMIZE_SEG,
   DBM_GPIO_IOCIOD_SEG,
   DBM_AUDIO_MICG_SEG,
   DBM_UI_MISC_SEG,
   DBM_CSS_1X_SEG,
   DBM_CSS_DO_SEG,
   DBM_CSS_MISC_SEG,
   DBM_MAX_SEG_CP_DB
} DbmCPSegmentT;

/* Define CP data base segment sizes */
#define DBM_CP_SEGMENT_SIZES \
   DBM_PSW_NAM1_SIZE, \
   DBM_PSW_MRU1_SIZE, \
   DBM_PSW_MS_CAP_DB_SIZE, \
   DBM_UICC_SIZE, \
   DBM_PSW_MISC_SIZE, \
   DBM_PSW_NAM2_SIZE, \
   DBM_PSW_MRU2_SIZE, \
   DBM_SERVICE_HISTORY_SIZE, \
   DBM_DBM_TIMESTAMP_SIZE, \
   DBM_SECURE_DATA_SEG_SIZE, \
   DBM_HSPD_SEG_SIZE, \
   DBM_HSPD_SECURE_SEG_SIZE,\
   DBM_DO_DATA_SEG_SIZE,\
   DBM_DO_MRU_SIZE, \
   DBM_EHRPD_SIZE, \
   DBM_HLP_IPCOUNTERS_SIZE,\
   DBM_MSC_WMCMSMODE_SIZE,\
   DBM_CUSTOMIZE_SIZE,\
   DBM_GPIO_IOCIOD_SIZE,\
   DBM_AUDIO_MICG_SIZE,\
   DBM_UI_MISC_SIZE,\
   DBM_CSS_1X_SIZE,\
   DBM_CSS_DO_SIZE,\
   DBM_CSS_MISC_SIZE
/* Define segment indexes into CP data base cache */
typedef enum
{
   DBM_CP_MARKER_INDEX        = 0,
   DBM_PSW_NAM1_INDEX         = DBM_CP_MARKER_INDEX + DBM_MARKER_SIZE,
   DBM_PSW_MRU1_INDEX         = DBM_PSW_NAM1_INDEX + DBM_PSW_NAM1_SIZE,
   DBM_PSW_MS_CAP_DB_INDEX    = DBM_PSW_MRU1_INDEX + DBM_PSW_MRU1_SIZE,
   DBM_UICC_INDEX             = DBM_PSW_MS_CAP_DB_INDEX + DBM_PSW_MS_CAP_DB_SIZE,
   DBM_PSW_MISC_INDEX         = DBM_UICC_INDEX + DBM_UICC_SIZE,
   DBM_PSW_NAM2_INDEX         = DBM_PSW_MISC_INDEX + DBM_PSW_MISC_SIZE,
   DBM_PSW_MRU2_INDEX         = DBM_PSW_NAM2_INDEX + DBM_PSW_NAM2_SIZE,
   DBM_SERVICE_HISTORY_INDEX  = DBM_PSW_MRU2_INDEX + DBM_PSW_MRU2_SIZE,
   DBM_DBM_TIMESTAMP_INDEX    = DBM_SERVICE_HISTORY_INDEX + DBM_SERVICE_HISTORY_SIZE,
   DBM_SECURE_DATA_SEG_INDEX  = DBM_DBM_TIMESTAMP_INDEX + DBM_DBM_TIMESTAMP_SIZE,
   DBM_HSPD_SEG_INDEX         = DBM_SECURE_DATA_SEG_INDEX + DBM_SECURE_DATA_SEG_SIZE,
   DBM_HSPD_SECURE_SEG_INDEX  = DBM_HSPD_SEG_INDEX+ DBM_HSPD_SEG_SIZE, 
   DBM_DO_DATA_SEG_INDEX = DBM_HSPD_SECURE_SEG_INDEX + DBM_HSPD_SECURE_SEG_SIZE,
   DBM_DO_MRU_INDEX           = DBM_DO_DATA_SEG_INDEX + DBM_DO_DATA_SEG_SIZE,  
   DBM_EHRPD_INDEX            = DBM_DO_MRU_INDEX + DBM_DO_MRU_SIZE,
   DBM_HLP_IPCOUNTERS_INDEX   = DBM_EHRPD_INDEX + DBM_EHRPD_SIZE,
   DBM_MSC_WMCMSMODE_INDEX    = DBM_HLP_IPCOUNTERS_INDEX + DBM_HLP_IPCOUNTERS_SIZE,
   DBM_CUSTOMIZE_INDEX        = DBM_MSC_WMCMSMODE_INDEX + DBM_MSC_WMCMSMODE_SIZE,
   DBM_GPIO_IOCIOD_INDEX      = DBM_CUSTOMIZE_INDEX + DBM_CUSTOMIZE_SIZE,
   DBM_AUDIO_MICG_INDEX       = DBM_GPIO_IOCIOD_INDEX + DBM_GPIO_IOCIOD_SIZE,
   DBM_UI_MISC_INDEX          = DBM_AUDIO_MICG_INDEX + DBM_AUDIO_MICG_SIZE,
   DBM_CSS_1X_INDEX           = DBM_UI_MISC_INDEX + DBM_UI_MISC_SIZE,
   DBM_CSS_DO_INDEX           = DBM_CSS_1X_INDEX + DBM_CSS_1X_SIZE,
   DBM_CSS_MISC_INDEX         = DBM_CSS_DO_INDEX + DBM_CSS_DO_SIZE,
   DBM_CP_CACHE_SIZE          = DBM_CSS_MISC_INDEX + DBM_CSS_MISC_SIZE
} DbmCPSegmentIndexT;

/* Define CP data base segment indexes */
#define DBM_CP_SEGMENT_INDEXES \
   DBM_PSW_NAM1_INDEX, \
   DBM_PSW_MRU1_INDEX, \
   DBM_PSW_MS_CAP_DB_INDEX, \
   DBM_UICC_INDEX, \
   DBM_PSW_MISC_INDEX, \
   DBM_PSW_NAM2_INDEX, \
   DBM_PSW_MRU2_INDEX, \
   DBM_SERVICE_HISTORY_INDEX, \
   DBM_DBM_TIMESTAMP_INDEX, \
   DBM_SECURE_DATA_SEG_INDEX, \
   DBM_HSPD_SEG_INDEX, \
   DBM_HSPD_SECURE_SEG_INDEX, \
   DBM_DO_DATA_SEG_INDEX,\
   DBM_DO_MRU_INDEX, \
   DBM_EHRPD_INDEX, \
   DBM_HLP_IPCOUNTERS_INDEX,\
   DBM_MSC_WMCMSMODE_INDEX,\
   DBM_CUSTOMIZE_INDEX,\
   DBM_GPIO_IOCIOD_INDEX,\
   DBM_AUDIO_MICG_INDEX,\
   DBM_UI_MISC_INDEX,\
   DBM_CSS_1X_INDEX,\
   DBM_CSS_DO_INDEX,\
   DBM_CSS_MISC_INDEX

#endif

/**************************************************/
/* Define Data Base 2 (RF) Constants              */
/**************************************************/
#define DBM_RF_FLASH_OFFSET 8192  /* The RF data base starts at this offset 
                                     from the flash sector address */

/* Define RF data base segment names */
/* 
 * IMPORTANT NOTE:
 * When RF data base segments are changed please use the following rules:
 * 1. New segments must be added to the bottom of the enum list.
 * 2. Existing segment sizes may be modified provided the segment name is unchanged.
 * 3. Existing segment names may be changed provided there is concurrence with 
 *    the APPS group and the impact to the ETS ICD is minimal.
 * 4. If as a result of a change an existing segment is no longer used then
 *    add a comment next to the segment to indicate so.
*/
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
/* For backward compatibily */
#define DBM_MAX_SEG_HWD_RF_CUST         DBM_MAX_SEG_RF_CUST_DB
#define DBM_MAX_SEG_RF_DB               DBM_MAX_SEG_RF_CAL_DB
#define DBM_MAX_SEG_RF_DB_SEG           DBM_MAX_SEG_RF_CAL_DB
typedef DbmRfCalSegmentT DbmRFSegmentT;
#else /* MTK_CBP */
#define DBM_RF_ID_SEG
/* typedef enum  DbmRFSegmentT; */
#include "dbmrfid.h"
#undef DBM_RF_ID_SEG

/* Define data base Constants. These constants must be updated
   when adding new segments to this data base */

#define DBM_RF_ID_SIZE
/* Define RF segment sizes in bytes ----  typedef enum DbmRFSegmentSizeT;*/
#include "dbmrfid.h"
#undef DBM_RF_ID_SIZE
#endif /* MTK_CBP */

#define    DBM_RF_MARKER_INDEX                   0

/**************************************************/
/* Define Data Base 3 - SYS Constants             */
/**************************************************/

/* Define SYS file marker and size */
#define DBM_SYS_MARKER        "VIA3"
#define DBM_SYS_MARKER_SIZE   4 

/* Define SYS data segment names */
typedef enum
{
   DBM_SYS_RF_BANDS_SUPPORTED_TABLE_SEG,
   DBM_SYS_RESERVED_FOR_NEW_BANDS_SEG,
   DBM_MAX_SEG_SYS_DB
} DbmSysSegmentT;

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
#undef DBM_MAX_SEG_SYS_DB
#define DBM_MAX_SEG_SYS_DB DBM_MAX_SEG_RF_CUST_DB
#endif

/* Define SYS segment sizes in bytes */
typedef enum
{
   DBM_SYS_RF_BANDS_SUPPORTED_TABLE_SIZE  = 126,  // SYS_BAND_CLASS_MAX * sizeof(HwdSupportedCDMABandT) - 6 * 21 bands
   DBM_SYS_RESERVED_FOR_NEW_BANDS_SIZE    = 114   // Reserved for new band classes - 6 * 19 bands
} DbmSysSegmentSizeT;

#if defined(MTK_DEV_VERSION_CONTROL) && defined(MTK_DEV_RF_CUSTOMIZE)


#else
/* Define SYS data file segment sizes */
#define DBM_SYS_SEGMENT_SIZES \
   DBM_SYS_RF_BANDS_SUPPORTED_TABLE_SIZE, \
   DBM_SYS_RESERVED_FOR_NEW_BANDS_SIZE

/* Define segment indexes into SYS data file */
typedef enum
{
   DBM_SYS_MARKER_INDEX                   = 0,

   /* FIRST */
   DBM_SYS_RF_BANDS_SUPPORTED_TABLE_INDEX = DBM_SYS_MARKER_INDEX                    + DBM_SYS_MARKER_SIZE,
   DBM_SYS_RESERVED_FOR_NEW_BANDS_INDEX   = DBM_SYS_RF_BANDS_SUPPORTED_TABLE_INDEX  + DBM_SYS_RF_BANDS_SUPPORTED_TABLE_SIZE,
   DBM_SYS_CACHE_SIZE                     = DBM_SYS_RESERVED_FOR_NEW_BANDS_INDEX    + DBM_SYS_RESERVED_FOR_NEW_BANDS_SIZE
} DbmSysSegmentIndexT;

/* Define Band Class Support database segment indexes */
#define DBM_SYS_SEGMENT_INDEXES \
   DBM_SYS_RF_BANDS_SUPPORTED_TABLE_INDEX, \
   DBM_SYS_RESERVED_FOR_NEW_BANDS_INDEX, \
   DBM_SYS_CACHE_SIZE
#endif

/*------------------------------------------------------------------------
* Define DBM message interface constants and structures
*------------------------------------------------------------------------*/

/* Define DBM msg command Ids */
typedef enum 
{
   DBM_CLEAR_MSG = 0,
   DBM_READ_MSG,
   DBM_WRITE_MSG,
   DBM_CACHE_MSG,
   DBM_FLUSH_MSG,
   DBM_READ_BLK_MSG,
   DBM_WRITE_BLK_MSG,
   DBM_REG_MSG,
   DBM_DE_REG_MSG,
   DBM_RF_DB_EXISTS_MSG,
   DBM_WRITE_BLK_PTR_MSG,
   DBM_FILE_BACKUP_MSG
#if defined(SYS_FLASH_LESS_SUPPORT) && defined(MTK_DEV_VERSION_CONTROL)
   ,
   DBM_GET_NV_FILE_VERSION_MSG,
   DBM_GET_RF_FILE_CALIBRATE_STATE_MSG,
   DBM_SET_RF_FILE_CALIBRATE_STATE_MSG
#endif
#ifdef MTK_DEV_CCCI_FS
   ,
   DBM_READ_LID_MSG,
   DBM_WRITE_LID_MSG,
   DBM_ETS_READ_LID_MSG,
   DBM_ETS_WRITE_LID_MSG
#endif
} DbmMsgIdT;

/* Define DBM CP, RF, and RF Band Class Support data base number */
typedef enum 
{
   DBM_CP_DATA_BASE  = 0,
   DBM_RF_DATA_BASE,
   DBM_SYS_DATA_BASE
#if defined(MTK_DEV_VERSION_CONTROL) && defined(MTK_DEV_RF_CUSTOMIZE)
   ,
   DBM_RF_CAL_BASE  = DBM_RF_DATA_BASE,
   DBM_RF_CUST_BASE = DBM_SYS_DATA_BASE
#endif
#ifdef MTK_DEV_VERSION_CONTROL
   ,
   DBM_CP_CUST_DATA_BASE
#endif
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
#if (SYS_BOARD >= SB_JADE)
   ,
   DBM_MIPI_DATA_BASE,
   DBM_POC_DATA_BASE
#endif
#endif
} DbmDataBaseIdT;

/* Define DBM ack types */
typedef enum
{
   DBM_ACK_TYPE = 0x00,
   DBM_NACK_TYPE
} DbmAckTypeT;

/* Define data base address based on segment and offset */
typedef PACKED_PREFIX struct 
{
   uint16        Segment; 
   uint16        Offset;  
} PACKED_POSTFIX  DbmAddressT;

/* Define DBM msg header format */
typedef PACKED_PREFIX struct 
{
   ExeRspMsgT     RspInfo;    
   DbmDataBaseIdT DataBaseId;
} PACKED_POSTFIX  DbmMsgHeaderT;

/* Define DBM clear msg command */
/* Response msg is just msg id  */
typedef PACKED_PREFIX struct 
{
   ExeRspMsgT     RspInfo;    
   DbmDataBaseIdT DataBaseId;
} PACKED_POSTFIX  DbmClearMsgT;

/* Define DBM clear msg response */
typedef PACKED_PREFIX struct 
{
   DbmDataBaseIdT  DataBaseId;
   DbmAckTypeT AckType;
} PACKED_POSTFIX  DbmClearRspMsgT;

/* Define DBM read msg command */
typedef PACKED_PREFIX struct 
{
   ExeRspMsgT     RspInfo;    
   DbmDataBaseIdT DataBaseId;
   DbmAddressT    Address;    
   uint16         NumBytes;  /* Size of data to read in bytes */
} PACKED_POSTFIX  DbmReadMsgT;

/* Define DBM read msg response */
typedef PACKED_PREFIX struct 
{
    DbmDataBaseIdT  DataBaseId;
    DbmAddressT     Address;    
    uint16          NumBytes;  /* Size of data read in bytes */
#ifdef MTK_DEV_CCCI_FS
    /*Make sure Data[1] address is the 4-byte*/
    ATTRIB_ALIGNED(4)
#endif
#if defined(MTK_PLT_ON_PC) && defined(MTK_PLT_ON_PC_UT)
    uint8           Data[255];
#else
    uint8           Data[1];
#endif
} PACKED_POSTFIX  DbmReadRspMsgT;

/* Define DBM write msg command */
typedef PACKED_PREFIX struct 
{
   ExeRspMsgT     RspInfo;    
   DbmDataBaseIdT DataBaseId;
   DbmAddressT    Address;    
   uint16         NumBytes;  /* Size of data to write in bytes */
   bool           WriteThru;
#ifdef MTK_DEV_CCCI_FS
   /*Make sure Data[1] address is the 4-byte*/
   ATTRIB_ALIGNED(4)
#endif
   uint8          Data[1];
} PACKED_POSTFIX  DbmWriteMsgT;

/* Define DBM write msg response */
typedef PACKED_PREFIX struct 
{
   DbmDataBaseIdT  DataBaseId;
   DbmAckTypeT     AckType;
} PACKED_POSTFIX  DbmWriteRspMsgT;

/* Define DBM cache msg command */
/* Response msg is just msg id  */
typedef PACKED_PREFIX struct 
{
   ExeRspMsgT     RspInfo;    
   DbmDataBaseIdT DataBaseId;
} PACKED_POSTFIX  DbmCacheMsgT;

/* Define DBM flush msg command */
/* Response msg is just msg id  */
typedef PACKED_PREFIX struct 
{
   ExeRspMsgT     RspInfo;    
   DbmDataBaseIdT DataBaseId;
} PACKED_POSTFIX  DbmFlushMsgT;

/* Define DBM Rf Db Exists query command */
typedef PACKED_PREFIX struct
{
    ExeRspMsgT    RspInfo;
} PACKED_POSTFIX  DbmRfDbExistsMsgT;

/* Define DBM Rf Db Exists response */
typedef PACKED_PREFIX struct 
{
   bool  RfDbExists;
} PACKED_POSTFIX  DbmRfDbExistsRspMsgT;

#if defined(SYS_FLASH_LESS_SUPPORT) && defined(MTK_DEV_VERSION_CONTROL)
/* Define DBM Get NV File Version command */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT    RspInfo;
   char          FileName [RFS_FILE_NAME_LEN];
} PACKED_POSTFIX  DbmGetNvFileVersionMsgT;

/* Define DBM Get NV File Version response */
typedef PACKED_PREFIX struct 
{
   bool      Result;
   FileVerT  NvFileVer;
} PACKED_POSTFIX  DbmGetNvFileVersionRspMsgT;

/* Define DBM Get RF File Calibrate State command*/
typedef PACKED_PREFIX struct
{
   ExeRspMsgT    RspInfo;
} PACKED_POSTFIX DbmGetRfFileCalStateMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT    RspInfo;
   bool          state;
} PACKED_POSTFIX DbmSetRfFileCalStateMsgT;

typedef enum
{
   RF_FILE_IS_CALIBRATED = 0,
   RF_FILE_IS_NOT_CALIBRATED,
   RF_FILE_VERSION_IS_NOT_EXIST
}DbmGetRfCalStateEnumT;

/* Define DBM Get RF File Calibrate State response*/
typedef PACKED_PREFIX struct
{
   DbmGetRfCalStateEnumT State;
   FileVerT RfFileVer;
} PACKED_POSTFIX DbmGetRfFileCalStateRspMsgT;

typedef DbmGetRfFileCalStateRspMsgT RfFileCalStateT;
#endif
/* Define DBM Block data base id's */
typedef enum
{
  DBM_PRL1_DATA_BASE = 1,
  DBM_DS_DATA_BASE,
#ifdef MTK_DEV_VERSION_CONTROL
  DBM_DS_CUST_DATA_BASE,
#else
  DBM_VREC1_DATA_BASE,
  DBM_VREC2_DATA_BASE,
  DBM_VMEM1_DATA_BASE,
  DBM_VMEM2_DATA_BASE,
#endif
  DBM_PRL2_DATA_BASE,
  DBM_ERI1_DATA_BASE,
  DBM_ERI2_DATA_BASE,
#ifndef MTK_DEV_VERSION_CONTROL
  DBM_SLC_DATA_BASE,
#endif
  DBM_DMUPUBKEY_DATA_BASE,
  DBM_DMUPUBKEY2_DATA_BASE,
  DBM_DMUPUBKEY_ORGID_DATA_BASE,
  DBM_EXT_GPS_DATA_BASE,
  DBM_PGPS_PDA_DATA_BASE,
  DBM_GPS_HASH_DATA_BASE,
  DBM_SILENT_LOGGING_DB,
  DBM_UICC_DATA_BASE,
  DBM_ENUM_END_MARKER

} DbmBlkDataBaseIdT;

/* Define DBM program msg types */
typedef enum
{
   DBM_PROG_INIT_TYPE = 0x00,
   DBM_PROG_DATA_TYPE,
   DBM_PROG_DWNLD_DONE_TYPE
} DbmProgTypeT;

/* Define DBM block write msg command */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT         RspInfo;
   uint32             SeqNum;    
   DbmProgTypeT       MsgType; 
   DbmBlkDataBaseIdT  DataBaseId;
   uint32             Offset;
   uint16             Reserved;
   uint16             NumBytes;
#ifdef MTK_DEV_CCCI_FS
      /*Make sure Data[1] address is the 4-byte*/
      ATTRIB_ALIGNED(4)
#endif

   uint8              Data[1];
} PACKED_POSTFIX  DbmBlkWriteMsgT;

/* Define DBM block write msg response */
typedef PACKED_PREFIX struct
{
  uint32              SeqNum;
  DbmBlkDataBaseIdT   DataBaseId;
  DbmAckTypeT         AckType;
} PACKED_POSTFIX  DbmBlkWriteRspMsgT;

/* Define DBM write block pointer message data structure */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT         RspInfo;    
   DbmBlkDataBaseIdT  DataBaseId;
   uint32             DataBaseSize;
   uint32             Offset;
   uint32             NumBytes;
   uint8              *Data;
} PACKED_POSTFIX  DbmWriteBlkPtrMsgT;

/* Define DBM write block pointer message response data structure */
typedef PACKED_PREFIX struct
{
  DbmBlkDataBaseIdT   DataBaseId;
  DbmAckTypeT         AckType;
  uint32              NumBytes;
} PACKED_POSTFIX  DbmWriteBlkPtrRspMsgT;

/* Define DBM block read msg command */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT         RspInfo;    
   DbmBlkDataBaseIdT  DataBaseId;
   uint32             Offset;
   uint16             NumBytes;
   uint8             *DataP;
} PACKED_POSTFIX  DbmBlkReadMsgT;

/* Define DBM block read msg response */
typedef PACKED_PREFIX struct
{
   DbmBlkDataBaseIdT  DataBaseId;
   uint32             DataBaseSize;
   uint32             Offset;
   uint16             NumBytes;
   uint8             *Data;
} PACKED_POSTFIX  DbmBlkReadRspMsgT;

/* Define the DBM Reg Msg Data structure */
typedef PACKED_PREFIX struct 
{
   DbmReadMsgT   ReadInfo;  /* Size of data to read in bytes */
} PACKED_POSTFIX  DbmRegMsgT;

/* Define the DBM De-Reg Msg Data structure */
typedef PACKED_PREFIX struct
{
   ExeTaskIdT     TaskId;
   DbmDataBaseIdT DataBaseId;
   DbmAddressT    Address;

} PACKED_POSTFIX DbmDeRegMsgT;

#ifdef MTK_DEV_CCCI_FS
typedef PACKED_PREFIX struct
{
   ExeRspMsgT         RspInfo;
   uint16             LID;
   uint16             RecIdx;
   uint16             RecAmount;
   uint8*             UserDataP;
} PACKED_POSTFIX DbmReadLidMsgT;

typedef PACKED_PREFIX struct
{
   uint16             LID;
   uint16             RecIdx;
   uint16             RecAmount;
   uint16             RecSize;  /* Size of LID Record */
   uint16             NumBytes; /*Total read length*/
   /*Make sure Data[1] address is the 4-byte*/
   ATTRIB_ALIGNED(4)
   uint8              Data[1];  /*If UserDataP is NULL ,DBM should transfer data in message*/
}PACKED_POSTFIX DbmReadLidRspMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT        RspInfo;
   uint16            LID;
   uint16            RecIdx;
   uint16            NumBytes;  /*Number bytes of data to write*/
   uint8*            UserDataP;
      /*Make sure Data[1] address is the 4-byte*/
   ATTRIB_ALIGNED(4)
   uint8             Data[1]; /*If UserDataP is NULL ,user should transfer data in message*/
} PACKED_POSTFIX DbmWriteLidMsgT;

typedef PACKED_PREFIX struct
{
   uint16            LID;
   uint16            RecIdx;
   DbmAckTypeT       AckType;
} PACKED_POSTFIX DbmWriteLidRspMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT        RspInfo;
   uint16            LID;
   uint16            RecIdx;
   uint16            Offset;
   uint16            ReadSize;
} PACKED_POSTFIX DbmEtsReadLidMsgT;

typedef PACKED_PREFIX struct
{
   uint16            LID;
   uint16            RecIdx;
   uint16            NumBytes;  /* Size of data read in bytes */
   uint8             Data[1];
} PACKED_POSTFIX DbmEtsReadLidRspMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT        RspInfo;
   uint16            LID;
   uint16            RecIdx;
   uint16            Offset;
   uint16            WriteSize;
   uint8             Data[1];
}PACKED_POSTFIX DbmEtsWriteLidMsgT;

typedef PACKED_PREFIX struct
{
   uint16            LID;
   uint16            RecIdx;
   DbmAckTypeT       AckType;
} PACKED_POSTFIX DbmEtsWriteLidRspMsgT;
#endif
/*------------------------------------------------------------------------
 *  Define Global Function Prototypes
 *------------------------------------------------------------------------*/

#endif
