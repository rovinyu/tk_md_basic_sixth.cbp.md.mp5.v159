/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef _HWDPSAVE_H_
#define _HWDPSAVE_H_
/*****************************************************************************
* 
* FILE NAME   : hwdpsave.h
*
* DESCRIPTION : Hardware Driver power saving modes Interface. 
*
* HISTORY     :
*     See Log at end of file
*
*****************************************************************************/
              
/*----------------------------------------------------------------------------
 Include Files
----------------------------------------------------------------------------*/
#include "hwdapi.h"
#include "hwdaudioapi.h"

#ifdef MTK_DEV_DENALI_SRLTE_PRE_IT
extern HwdRfMpaEnumT  RtbaGetRfHWState(SysAirInterfaceT Mode, uint8 IcPath);
#endif

/*----------------------------------------------------------------------------
 Global Defines and Macros
----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 Global Typedefs
----------------------------------------------------------------------------*/

#if (defined(MTK_CBP)&& (!defined(MTK_PLT_ON_PC))) && defined(MTK_PLT_AUDIO)
/*----------------------------------------------------------------------------
 Global Data
----------------------------------------------------------------------------*/
/* FD216 states */
typedef enum
{
	HWD_Audio_Fd216_NOT_INITIALIZED,   /* Fd216 code/apps not downloaded */
	HWD_Audio_Fd216_CODE_DOWNLOADING,  /* Fd216 code being downloaded */
	HWD_Audio_Fd216_APP_DOWNLOADING,   /* Fd216 code has been downloading, downloading 1st app */
	HWD_Audio_Fd216_AUDIO_INIT, 	   /* Fd216 code is performing audio initialization */
	HWD_Audio_Fd216_READY			   /* Fd216 code and first app downloaded, Fd216 is ready */
} HwdAudioDspPowerStateT;
#endif


/*----------------------------------------------------------------------------
 Global Function Prototypes
----------------------------------------------------------------------------*/

extern HwdPwrSaveModesT HwdPwrSaveGetMode(SysAirInterfaceT Interface);
extern void HwdPwrSaveDisable(bool Disable);

extern void HwdPwrSaveModeSet(HwdPwrSaveModesT PwrSaveMode);

extern void HwdPwrSavePdmValue(uint32 PdmNumber, uint16 PdmValue);
extern void HwdPwrSaveDspMClkCtrl(bool HwState);
extern void HwdPwrSaveDspVClkCtrl(bool HwState);
extern bool HwdPwrSaveDspmClkState(void);
extern void HwdPwrSaveL1dDspmEnable(bool HwState);
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
extern bool HwdPwrSaveDspFd216ClkState(void);
#else
extern bool HwdPwrSaveDspvClkState(void);
#endif
extern void HwdPwrSaveDspvDnldCtrl(bool HwState);
extern void HwdPwrSaveDspvPcmModeCtrl(bool HwState);
extern void HwdPwrSaveDspvPcmFrameCtrl(bool HwState);
extern void HwdPwrSaveDspvJtagCtrl(bool HwState);
extern void HwdPwrSaveDspmJtagCtrl(bool HwState);
extern void HwdPwrOffBoard( void );
extern void HwdPwrSaveGpsUartClkCtrl(bool HwState);
extern void HwdPwrSaveGuimClk9Ctrl(bool HwState);
extern void HwdPwrSaveRxDivEnable(bool Enable);
extern void HwdPwrSaveEtsDspMCtrl(bool HwState);

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
extern void  HwdPwrSaveEtsFd216Ctrl(bool HwState);
#else
extern void HwdPwrSaveEtsDspVCtrl(bool HwState);
#endif

extern void HwdPwrSaveMonDspMCtrl(bool HwState);
extern void HwdPwrSaveMonDspVCtrl(bool HwState);
extern void HwdPwrSaveHwdDspMCtrl(bool HwState);
extern void HwdPwrSaveFndoClkCtrl(bool HwState);
extern void HwdPwrSaveProtectDoPreempt(bool Enable);
extern uint8 HwdPwrSaveGetRxDivMode(uint8 Interface);
extern uint8 HwdPwrSaveGetRxMainMode(uint8 Interface);
extern uint8 HwdPwrSaveGetTxMainMode(uint8 Interface);
extern uint8 HwdPwrSaveGetRfDivRx(uint8 Interface);
extern uint8 HwdPwrSaveGetRfMainRx(uint8 Interface);
extern uint8 HwdPwrSaveGetRfMainTx(uint8 Interface);
extern bool  HwdPwrSaveGetDspMClkState(void);

#ifdef MTK_CBP
extern uint8 HwdPwrSaveGetDspMClkCtrlState(void);
#endif
extern void HwdPwrSaveRfPathCtrl(SysAirInterfaceT Interface, uint32 Path, bool HwState);
extern void HwdSetEvdoSchClockFreq(uint16 Divider);
extern void HwdPwrSaveGtxClk9Ctrl(bool HwState);
extern bool HwdPwrSaveIsExtAmpUsed (HwdAudioModeT AudioMode);
extern HwdAudioVDacInputModeT HwdPwrSaveAudioInputModeGet (void);
extern void HwdPwrSaveTestRxAdcCtrl(SysAirInterfaceT Interface, bool HwState);
extern void HwdPwrSaveSet1xDoOnly(bool Is1xOnly);
extern void HwdPwrSaveGpsClkCtrl(bool HwState);
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
extern bool HwdPwrSaveGetRfPathMode(HwdRfMpaEnumT rfPath, SysAirInterfaceT *ModeP);
#ifdef MTK_DEV_RF_TEST
extern void HwdPwrSaveSetRfMainRx(uint8 Interface, HwdRfMpaEnumT path);
#endif /* MTK_DEV_RF_TEST */
#endif

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
extern void HwdPwrSaveDfeRxMCtrl(bool HwState);
extern void HwdPwrSaveDfeRxDCtrl(bool HwState);
#endif

#if (SYS_BOARD >= SB_JADE)
extern void HwdPwrSaveTxDetPathCtrl(bool HwState);
extern void HwdPwrSaveTopSMInit(void);
#endif

#if (SYS_BOARD >= SB_JADE)
extern void HwdPwrSaveForceOnClk();
#endif 


/*****************************************************************************
* $Log: hwdpsave.h $
*****************************************************************************/

/*****************************************************************************
* End of File
*****************************************************************************/
#endif

/**Log information: \main\5 2012-03-14 07:08:56 GMT hbi
** HREF#0000: update SD review code (Audio)**/
/**Log information: \main\Trophy\Trophy_ylxiao_href22207\1 2013-05-17 02:21:49 GMT ylxiao
** HREF#22207**/
/**Log information: \main\Trophy\1 2013-05-17 02:56:52 GMT hzhang
** HREF#22207 to remove compiling warning.**/
