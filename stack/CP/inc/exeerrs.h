/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 1998-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
/*****************************************************************************
 
  FILE NAME:  exeerrs.h

  DESCRIPTION:

    This file contains the fault codes for the EXE software unit.

*****************************************************************************/

#ifndef EXEERRS_H
#define EXEERRS_H

#include "exeapi.h"
#include "sysdefs.h"


#define EXE_MSG_PTR_LOG_FUNCTION_NAME_LENGTH 16

#define EXE_CONTEXT_LOG_ITEM_SIZE 100
#define EXE_EVENT_HISTORY_ITEM_SIZE 40

/*------------------------------------------------------------------------
*  The following definitions are fault ids for MonFault routine.
*-----------------------------------------------------------------------*/

typedef enum 
{
   EXE_MSG_BUFF_MEM_EMPTY_ERR        = 0x00,
   EXE_MSG_BUFF_MEM_SIZE_ERR         = 0x01,
   EXE_NUCLEUS_ERR                   = 0x02,
   EXE_ILLEGAL_LISR_OPERATION_ERR    = 0x03,
   EXE_MSG_BUFF_OVERWRITE_ERR        = 0x04,
   EXE_PART_MEM_EMPTY_ERR            = 0x05,
   EXE_STACK_OVERFLOW_ERR            = 0x06,
   EXE_TIMER_CREATION_ERR            = 0x07,
   EXE_UNHANDLED_INT_ERR             = 0x08,
   EXE_MAIL_QUEUE_FULL_ERR           = 0x09,
   EXE_INVALID_TIMER_EXPIRED_ERR     = 0x0A,
   EXE_MSG_BUFF_FREE_REDUNDANT_ERR   = 0x0B,
   EXE_MSG_BUFF_INVALID_FREE_ERR     = 0x0C,
   EXE_ILLEGAL_HISR_OPERATION_ERR    = 0x0D,
   EXE_MSG_BUFF_SENT_ERR             = 0x0E,
   EXE_MSG_BUFF_HWM_ERR              = 0x0F,
   EXE_TIMER_RESET_ERR               = 0x10,   
   EXE_MSG_BUFF_GET_CORRUPTED_PTR    = 0x11,   
   EXE_MSG_BUFF_FREE_CORRUPTED_PTR   = 0x12,   
   EXE_MSG_BUFF_SEND_CORRUPTED_PTR   = 0x13,   
   EXE_MSG_BUFF_READ_CORRUPTED_PTR   = 0x14,

   /*No halt error type.*/
   EXE_GET_SEMAPHORE_IN_HISR         = 0x15,
   EXE_EVENT_WAIT_IN_HISR            = 0x16,
   EXE_NU_SLEEP_IN_HISR              = 0x17,
   EXE_INT_IS_DISABLED_IN_MON_IDLE   = 0x18,
   EXE_INT_DISABLE_TIME_TOO_LONG     = 0x19,
   EXE_PARAM_NULL_ERROR              = 0x1A,
   EXE_HEAP_FREE_END_MARKE_ERROR     = 0x1B,
   EXE_HEAP_FREE_START_MARKE_ERROR   = 0x1C,
   EXE_INVALID_SEMAPHORE_DELETE_ERROR= 0x1D,
   EXE_INVALID_SEMAPHORE_GET_ERROR   = 0x1E,
   EXE_INVALID_SEMAPHORE_RELEASE_ERROR=0x1F,
   EXE_KEYPAD_FORCE_DUMP_ERR         = 0x20,
   EXE_INT_MANAGE_STACK_PTR_ERR      = 0x21,

   EXE_ERRORS_MAX /* Note, the MAX cannot be greater than 0xFF, if it does get, there will be a need to modify the ETS files */
} ExeErrsT;

typedef enum
{
   EXE_FAULT_TYPE_1 = 0,
   EXE_FAULT_TYPE_2,
   EXE_FAULT_TYPE_3, 
   EXE_FAULT_TYPE_4
} ExeFaultTypeT;

typedef PACKED_PREFIX struct
{
   uint8          Id;
   uint8          Priority;
   uint8          Status;
   uint8          MboxMsgs[EXE_NUM_MAILBOX];
} PACKED_POSTFIX  ExeTaskStatusT;

/* Define EXE Fault type 1 msg structure */
typedef PACKED_PREFIX struct
{
   ExeErrsT       ExeError;
   MonSysTimeT    SysTime;
   uint8          SrcTaskId;
   int32          NucleusError;
} PACKED_POSTFIX  ExeFaultType1T;

/* Define EXE Fault type 2 msg structure */
typedef PACKED_PREFIX struct
{
   ExeErrsT       ExeError;
   MonSysTimeT    SysTime;
   uint8          SrcTaskId;
   uint16          Buff1Alloc;
   uint16          Buff2Alloc;
   uint16          Buff3Alloc;
   uint16          Buff4Alloc;
   uint16         MsgBuffSize;
} PACKED_POSTFIX  ExeFaultType2T;

/* Define EXE Fault type 3 msg structure */
typedef PACKED_PREFIX struct
{
   ExeErrsT       ExeError;
   MonSysTimeT    SysTime;
   uint8          SrcTaskId;
   uint16         Buff1Alloc;
   uint16         Buff2Alloc;
   uint16         Buff3Alloc;
   uint16         Buff4Alloc;
   uint8          DestTaskId;
   uint8          MboxId;
   uint32         MsgId;
   uint16         MsgSize;
} PACKED_POSTFIX  ExeFaultType3T;


typedef PACKED_PREFIX struct
{
   ExeErrsT       ExeError;
   MonSysTimeT    SysTime;
   uint8          SrcTaskId;
   uint8*         MsgPtr;
} PACKED_POSTFIX  ExeFaultType4T;

/*
 * Debug queue data definition - this is used when a MON_HALT occurs.
 * The ExeFault() function will call NU_Read_From_Queue() in nu_quc.c
 * to retrieve all messages from all tasks' queues and pump out their
 * parameters to ETS.
 */

/* The queue data MsgParams[] array is limited to 30 so that it will fit
   into the largest ExeMsgBuffer and can easily be sent from one task to
   another (ie, from VAL to UI) */
#define EXE_TASK_MAIL_QUEUE_MAX_SIZE    30

typedef PACKED_PREFIX struct
{
   uint32   MsgId;
   uint32   MsgPtr;
   uint32   MsgLen;
   uint32   TimeMs;
} PACKED_POSTFIX  ExeDebugMsgParamT;

typedef PACKED_PREFIX struct
{
   uint8                TaskId;
   uint8                QueNum;
   uint16               NumMsgs;
   ExeDebugMsgParamT    MsgParams[EXE_TASK_MAIL_QUEUE_MAX_SIZE];
} PACKED_POSTFIX  ExeDebugQueueDataT;

/*
 * Debug message buffer pointer statistics definition -
 * this is used when a MON_HALT occurs.
 * The ExeFault() function will pump out all tasks's buffer pointer statistics to ETS.
 */
typedef PACKED_PREFIX struct
{
   void         *BuffPtr;
   uint8        BuffType;
   uint8        BuffState;
   uint8        TaskAlloc;
   uint8        TaskSentTo;
   uint32       MsgId;
   uint8        FunctionName[EXE_MSG_PTR_LOG_FUNCTION_NAME_LENGTH];
   uint16       LineNumber;
} PACKED_POSTFIX  ExeMsgBuffPtrStatsLogT;

/*
 * Debug TCB definition - this is used when a MON_HALT occurs.
 * The ExeFault() function will pump out all tasks's TCBs to ETS.
 */
typedef struct
{
   uint32               TaskId;
   NU_TASK              TaskCb;
} ExeDebugTcbDataT;

/* There are 7 Arm modes, but we don't use User mode */
typedef enum
{
   EXE_SUP_MODE = 0,
   EXE_FIQ_MODE,
   EXE_IRQ_MODE,
   EXE_UNDEF_MODE,
   EXE_ABT_MODE,
   EXE_SYS_MODE,
   EXE_ARM_MODE_MAX
} ExeARMModeT;

typedef PACKED_PREFIX struct
{
   uint8               ThreadNum;
   uint8               TaskId;
   uint8               tc_name[NU_MAX_NAME];  /* Task name              */
   uint8               Pri;   
   uint8               Status;
   uint32              PC;
} PACKED_POSTFIX  ExeTaskStatPart1ItemT;

typedef PACKED_PREFIX struct
{
   uint32        TaskInRunning;
   uint32        PcPointer;
   ExeTaskStatPart1ItemT  TaskStates[1];
} PACKED_POSTFIX  ExeTaskStatusPart1T;

typedef PACKED_PREFIX struct
{
   uint8          Task;
   uint32         TimeMs;
   uint8          Event;
   uint32         Arg1;
   uint16         Arg2;
   uint8          Status;
   uint8          MboxMsgs[EXE_NUM_MAILBOX];
} PACKED_POSTFIX  ExeTaskStatusPart2T;

/*
 * Event types used by thread event data, system event history and semaphore history data structures
 */
typedef enum
{
   DBG_EVENT_RESERVED      = 0x00,
   DBG_EVENT_WAIT          = 0x01,
   DBG_EVENT_GOT           = 0x02,
   DBG_TASK_WAIT           = 0x03,
   DBG_TASK_WAKEUP         = 0x04,
   DBG_SIG_SET             = 0x05,
   DBG_MSG_SEND            = 0x06,
   DBG_MSG_READ            = 0x07,
   DBG_MSG_READ_SUCCESS    = 0x08,
   DBG_MSG_READ_TIMED_OUT  = 0x09,
   DBG_GETTING_SEM         = 0x0A,
   DBG_OBTAINED_SEM        = 0x0B,
   DBG_RELEASED_SEM        = 0x0C,
   DBG_SEMGET_TIMED_OUT    = 0x0D,
   DBG_MALLOC_SUCSESS      = 0x0E,
   DBG_MALLOC_FAIL         = 0x0F,
   DBG_MSG_BUF_GET         = 0x10,
   DBG_MSG_BUF_FREE        = 0x11,
   DBG_EXE_FREE            = 0x12,
   DBG_THREAD_SWITCH       = 0x13,
   DBG_CREATE_SEM          = 0x14,
   DBG_ENTER_SLEEP         = 0x15,
   DBG_WAKE_UP             = 0x16,
   DBG_LIGHT_SLEEP         = 0x17,
   DBG_START_TIMER         = 0x18,
   DBG_CREATE_TIMER        = 0x19,
   DBG_DELETE_TIMER        = 0x1A,
   DBG_STOP_TIMER          = 0x1B,
   DBG_RESET_TIMER         = 0x1C,
   DBG_SUSPEND_TASK        = 0x1D,
   DBG_RESUME_TASK         = 0x1E,
   DBG_CHG_PRIORITY        = 0x1F,
   DBG_SLEEP_EV_0          = 0x20,
   DBG_SLEEP_EV_1          = 0x21,
   DBG_EVENT_MAX_ID
} ExeDebugEventTypeT;

#ifdef MTK_DEV_SHARED_DBG
typedef enum {
   DBG_SHARED_EVT_WAIT = 60,
   DBG_SHARED_EVT_GOT = 61,
   DBG_SHARED_TASK_WAIT = 62,
   DBG_SHARED_TASK_WKUP = 63,
   DBG_SHARED_SIG_SET = 64,
   DBG_SHARED_SEND = 65,
   DBG_SHARED_SLEEP = 66,
   DBG_SHARED_WAKUP = 67,
   DBG_SHARED_LOCKI = 68,
   DBG_SHARED_LOCKF = 69,
   DBG_SHARED_LOCKIF = 70,
   DBG_SHARED_UNLOCK = 71,
   DBG_SHARED_SLEEP_EV_0 = 72,
   DBG_SHARED_SLEEP_EV_1 = 73,
   DBG_SHARED_INVALID = 100
}ExeDbgEvtSharedTypeT;
#endif
 
typedef PACKED_PREFIX struct 
{
   uint32     TimeMs;
   uint32      Arg1;
   uint16      Arg2;
   uint8       Event;  /* ExeDebugEventTypeT */
} PACKED_POSTFIX ExeDbgTaskEventItemT;

typedef PACKED_PREFIX struct 
{
   uint32                   TaskId;
   uint8                    Index;
   ExeDbgTaskEventItemT Items[1];
} PACKED_POSTFIX ExeDbgTaskEventLogT;

typedef PACKED_PREFIX struct 
{
   uint32      TimeMs;
   uint32      Arg1;
   uint16      Arg2;
   uint8       Event;  /* ExeDebugEventTypeT */
   uint8       ThreadId;
} PACKED_POSTFIX ExeDbgSystemEventItemT;

typedef PACKED_PREFIX struct 
{
   uint8                    Index;
   ExeDbgSystemEventItemT Items[1];
} PACKED_POSTFIX ExeDbgSystemEventLogT;

typedef PACKED_PREFIX struct
{
   uint8          Id;
   uint32        StackSize;
   int32         CurUsePercent;
   int32         MaxUsePercent;
} PACKED_POSTFIX  ExeTaskStackStatusT;


typedef PACKED_PREFIX struct
{
  uint16 TaskId;
  uint32 StackData[1];
} PACKED_POSTFIX  ExeStackDataT;


typedef PACKED_PREFIX struct
{
   uint16  Year;
   uint16  Month;
   uint16  Date;
   uint16  Hour;
   uint16  Minute;
   uint16  Second;
   uint32  SysTime;
   uint32  Time32kcnt;
} PACKED_POSTFIX  ExeHaltTimeT;

typedef PACKED_PREFIX struct
{
   uint32 TimeMs;
   uint8  ThreadId;
} PACKED_POSTFIX  ExeContextHistoryItemT;

typedef PACKED_PREFIX struct
{
   uint16                 Index;
   ExeContextHistoryItemT LogArry[1];
} PACKED_POSTFIX  ExeContextHistoryEtsLogT;


/*
 * Debug Performance Data (profile spy) definition used when a halt condition is detected.
 */
/* Max number of thread ids that can be displayed on the Fault-logging version of Performance Data spy */
#define  MAX_FAULT_SPY_RECORDS   50

/* Fault-logging version of Performance Data spy structure definitions */
typedef PACKED_PREFIX struct
{
   uint16      ThreadId;
   uint16      Average;
   uint32      Total;
} PACKED_POSTFIX  FaultThreadStatsT;

typedef PACKED_PREFIX struct
{
   uint32      TotalTime;
   FaultThreadStatsT OverallData[MAX_FAULT_SPY_RECORDS];
} PACKED_POSTFIX  ExeDebugProfileSpyT;




typedef PACKED_PREFIX struct
{
    uint8               TaskId;
    uint8                tc_name[NU_MAX_NAME];  /* Task name              */
    uint8        tc_status;             /* Task status            */
    uint8        tc_priority;           /* Task priority          */
    uint32            tc_scheduled;          /* Task scheduled count   */
    void               *tc_stack_start;        /* Stack starting address */
    void               *tc_stack_end;          /* Stack ending address   */
    void               *tc_stack_pointer;      /* Task stack pointer     */
    uint32            tc_stack_size;         /* Task stack's size      */
    uint32            tc_stack_minimum;      /* Minimum stack size     */
    void                (*tc_entry)(UNSIGNED, VOID *);
    uint32            tc_app_reserved_1;     /* Application reserved   */
} PACKED_POSTFIX  ExeDebugTcbDataLogT;

typedef PACKED_PREFIX struct
{
    uint32  LogSize;
    uint32  FlashLogSpace;
    uint32  SramLogSpace;
    uint32  SramTotalSpace;
    uint32  SramStartAddr;
} PACKED_POSTFIX  ExeDebugLogSizeT;

#endif /* EXEERRS_H */

/*****************************************************************************
*****************************************************************************/

