/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2005-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef VALSOUNDAPI_H
#define VALSOUNDAPI_H
/******************************************************************************
* 
* FILE NAME   : valsndapi.h
*
* DESCRIPTION :
*
*   This is the API header file for VAL sound module.
*
* HISTORY     :
*
*   See Log at end of file
*
******************************************************************************/

#ifdef  __cplusplus
extern "C" {
#endif

#include "sysdefs.h"
#include "sysKeydef.h"
#include "valkeypad.h"
#include "valfsiapi.h"
#include "valvmemo.h"

#define VAL_SOUND_CONTINUES          (0xFF) /* Play forever, used for both TONES and DTMF */
#define VAL_SOUND_BURST              (1)    /* For DTMF burst mode */
#define VAL_SOUND_PCM_FRAME_SIZE     320    /* PCM frame length in bytes */

#define VAL_RINGER_DURATION_FOREVER (0xFFFFFFFF)
#define VAL_VIBRATE_DURATION_FOREVER (0xFFFFFFFF)

/* Event ID's used for registered sound callbacks */ 
#define VAL_SND_END_MUSIC_EVENT          1  /* Register by calling ValSndRegister            */
#define VAL_SND_MUSIC_STATUS_EVENT       2  /* Register by calling ValMusicStatusRegister    */
#define VAL_VMEMO_RECORD_END_EVENT       3  /* Register by calling ValVmemoRegister          */
#define VAL_VMEMO_PLAYBACK_END_EVENT     4  /*     "                                         */
#define VAL_VMEMO_VOICE_DATA_EVENT       5  /* Register by calling ValVmemoVoiceDataRegister */
#define VAL_SND_MIC_PCM_DETECT_ERR_EVENT 6  /* Register by calling ValSndPcmDetectRegister   */
#define VAL_SND_PLAY_START_RSP_EVENT     7
#define VAL_SND_SRV_OPT_CONN_EVENT       8  /* Register by calling ValSndSrvOptConnRegister  */
 
#define SOUND_ITERATION_FOREVER  VAL_SOUND_CONTINUES
#define SOUND_DURATION_FOREVER  VAL_RINGER_DURATION_FOREVER

#define WEAK_VIBRATE_PLAY_MS    500  /*500ms*/
#define WEAK_VIBRATE_REST_MS    500
#define MEDIUM_VIBRATE_PLAY_MS  800  /*800ms*/
#define MEDIUM_VIBRATE_REST_MS  800
#define STRONG_VIBRATE_PLAY_MS  3000  /*3000ms = 1s*/
#define STRONG_VIBRATE_REST_MS  1000  /*1000ms = 1s*/
#define VIBRATE_MAX_MS          0x1000000

#define VAL_SND_FAILURE   (-1)    /* Generic SND (negative) failure return code */

#define VAL_SND_DEEP_SLEEP_FLAG MON_VAL_SLEEP_SND_FLAG

/*-----------------------------------------------------------------
 *	valaudio.c interface
 *----------------------------------------------------------------*/
/* volume, dtmf and playback mode are in valapi.h */

/* NOTE: Some tone sequences are predefined in valmidi.c and indexed here.
* If this list is changed, must also update MidiFileList in valmidi.c.
*/
typedef enum {
   VAL_AUDIO_TONE_NULL,	/* to stop playback */
   VAL_AUDIO_TONE_DIAL,
   VAL_AUDIO_TONE_RING_BACK,
   VAL_AUDIO_TONE_INTERCEPT,
   VAL_AUDIO_TONE_INTERCEPT_ABBR,
   VAL_AUDIO_TONE_NWK_CONGESTION,
   VAL_AUDIO_TONE_NWK_CONGESTION_ABBR,
   VAL_AUDIO_TONE_BUSY,
   VAL_AUDIO_TONE_CONFIRM,
   VAL_AUDIO_TONE_CALL_WAITING,
   VAL_AUDIO_TONE_ALERT,
   VAL_AUDIO_TONE_POWER_UP,
   VAL_AUDIO_TONE_SMS_ALERT,
   VAL_AUDIO_TONE_REORDER,
   VAL_AUDIO_TONE_ANSWER_HOLD,
   VAL_AUDIO_TONE_POWER_UP_FANCY,
   VAL_AUDIO_TONE_POWER_OFF,
   VAL_AUDIO_TONE_BEEP,
   VAL_AUDIO_TONE_EARBEEP,
   VAL_AUDIO_TONE_EXTERNAL_POWER,
   VAL_AUDIO_TONE_SERVICE_ALERT,
   VAL_AUDIO_TONE_DTACO_TONE,
   VAL_AUDIO_TONE_OTASP_ALERT,
   VAL_AUDIO_TONE_PIP,
   VAL_AUDIO_TONE_LOWPOWER_ALERT,
   VAL_AUDIO_TONE_ALARMCLOCK_ALERT,

   VAL_AUDIO_NUM_TONES
} ValSndToneIdT;

/* dtmf id; used to index entries in dtmf tone table  */
typedef enum {		
   VAL_SOUND_DTMF_0 = 0,
   VAL_SOUND_DTMF_1,
   VAL_SOUND_DTMF_2,
   VAL_SOUND_DTMF_3,
   VAL_SOUND_DTMF_4,
   VAL_SOUND_DTMF_5,
   VAL_SOUND_DTMF_6,
   VAL_SOUND_DTMF_7,
   VAL_SOUND_DTMF_8,
   VAL_SOUND_DTMF_9,
   VAL_SOUND_DTMF_STAR,
   VAL_SOUND_DTMF_POUND,
   VAL_SOUND_DTMF_A,
   VAL_SOUND_DTMF_B,
   VAL_SOUND_DTMF_C,
   VAL_SOUND_DTMF_D,
   /* single tone id; used to index entries in single tone keys table  */
   VAL_SOUND_SINGLE_LEFT,
   VAL_SOUND_SINGLE_RIGHT,
   VAL_SOUND_SINGLE_UP,
   VAL_SOUND_SINGLE_DOWN,
   VAL_SOUND_SINGLE_CLEAR,
   VAL_SOUND_SINGLE_SOFT_LEFT,
   VAL_SOUND_SINGLE_SOFT_RIGHT,
   VAL_SOUND_SINGLE_SELECT,
   VAL_SOUND_SINGLE_SEND,
   VAL_SOUND_SINGLE_END,
   VAL_SOUND_SINGLE_POWER,
   VAL_SOUND_SINGLE_CAMERA,
   VAL_SOUND_SINGLE_VOLUME_UP,
   VAL_SOUND_SINGLE_VOLUME_DOWN,
   VAL_SOUND_SINGLE_RECORD,

   /* "Stop DTMF" should always be last */
   VAL_SOUND_DTMF_STOP,
   VAL_SOUND_DTMF_NUM
} ValSndDtmfIdT;
 
/* Tone structure for user-defined tones: tone sequences would be an array of this struct */
typedef struct
{
    uint16  Duration;  /* # of 20 msec frames  */
    uint16  Freq[2];   /* Dual Tone Frequencies in Hz; 
                          for single tone, set 2nd freq to zero */
} ValSndToneDataT;

typedef PACKED_PREFIX struct {
   bool           Play;   /* If TRUE, DTMF is played; if FALSE, DTMF is stopped */
   ValSndDtmfIdT  DtmfId;
} PACKED_POSTFIX  ValDtmfToneGenMsgT;

/* Midi Tone message (used by ETS) */
typedef PACKED_PREFIX struct
{
    uint8  ToneIdx;               /* Use ValSndToneIdT */
    bool   UseDefaultIterations;  /* TRUE if using default # iterations */
    uint8  NumUserIterations;     /* Used only if UseDefaultIterations is FALSE */
} PACKED_POSTFIX  ValMidiToneMsgT;

typedef enum
{
  VAL_KEY_SOUND_OFF = 0,
  VAL_KEY_SOUND_SHORT,
  VAL_KEY_SOUND_LONG
} ValSndKeySoundT;

typedef struct 
{
   void * pData;
   uint32 Iteration;
   uint32 Duration;
} ValSoundMsgT;

typedef enum
{
   VAL_SOUND_PATH_CALL_SPEAKER  = 1,
   VAL_SOUND_PATH_RINGER_SPEAKER = 2,
  
   VAL_SOUND_PATH_MICROPHONE = 4

} ValSoundPathT;

typedef enum
{
   VAL_SOUND_DEVICE_HANDSET         = 0,
   VAL_SOUND_DEVICE_MONO_HEADSET    = 1,
   VAL_SOUND_DEVICE_STEREO_LDSPKR   = 2,
   VAL_SOUND_DEVICE_EDAI_HANDSET    = 3,
   VAL_SOUND_DEVICE_MONO_LDSPKR     = 4,
   VAL_SOUND_DEVICE_STEREO_HEADSET  = 5,
   VAL_SOUND_DEVICE_LLINE_HANDSET   = 6,
   VAL_SOUND_DEVICE_TTY             = 7,      /* TTY 2-way using Hifi DAC headset */
   VAL_SOUND_DEVICE_TTY_VCO         = 8,      /* TTY voice carry-over, uses Hifi DAC */
   VAL_SOUND_DEVICE_TTY_HCO         = 9,      /* TTY hearing carry-over, voice to Hifi DAC handset */
   VAL_SOUND_DEVICE_I2S_HANDSET     = 10,     /* I2S handset output, bypass HiFi DAC  */
   VAL_SOUND_DEVICE_PC              = 11,     /* 1X Voice to/from PC (USB)  */
   VAL_SOUND_DEVICE_BLUETOOTH_ON_CP = 12,     /* Bluetooth A2DP interface running on CP */
   VAL_SOUND_DEVICE_EDAI_HEADSET    = 13,
   VAL_SOUND_DEVICE_EDAI_LDSPKR     = 14,
   VAL_SOUND_DEVICE_I2S_HEADSET     = 15,     /* I2S headset output, bypass Hifi DAC  */
   VAL_SOUND_DEVICE_I2S_LDSPKR      = 16,     /* I2S loudspeaker output, bypass Hifi DAC */
   VAL_SOUND_DEVICE_TTY_I2S         = 17,     /* TTY 2-way using I2S headset */
   VAL_SOUND_DEVICE_TTY_VCO_I2S     = 18,     /* TTY voice carry-over, uses I2S */
   VAL_SOUND_DEVICE_TTY_HCO_I2S_HANDSET = 19, /* TTY hearing carry-over, voice to I2S handset */
   VAL_SOUND_DEVICE_TTY_HCO_I2S_LDSPKR  = 20, /* TTY hearing carry-over, voice to I2S ldspkr  */
   VAL_SOUND_DEVICE_LDSPKR_HEADSET  = 21,     /* Both loudspeaker and headset simultaneously  */
   VAL_SOUND_DEVICE_VADC_IN_I2S_OUT = 22,     /* VADC MIC In, I2S Out (to AP) - 
                                                        to be used only for voice memo record */
   VAL_SOUND_DEVICE_EDAI_BLUETOOTH        = 23, /* EDAI for bluetooth */
   VAL_SOUND_DEVICE_EDAI_NO_MIC_HEADSET   = 24, /* EDAI headset w/out MIC, uses EDAI handset MIC */
   VAL_SOUND_DEVICE_EDAI_HAC              = 25, /* EDAI Hearing Aid Compatibility */
   VAL_SOUND_DEVICE_EDAI_TTY              = 26, /* TTY 2-way using EDAI headset */
   VAL_SOUND_DEVICE_EDAI_TTY_VCO          = 27, /* TTY Voice Carry-Over using EDAI */
   VAL_SOUND_DEVICE_EDAI_TTY_HCO          = 28, /* TTY Hearing Carry-Over using EDAI */
   VAL_SOUND_DEVICE_I2S_BLUETOOTH         = 29, /* I2S bluetooth output, bypass Hifi DAC */
   VAL_SOUND_DEVICE_I2S_NO_MIC_HEADSET    = 30, /* I2S headset with I2S handset MIC */
   VAL_SOUND_DEVICE_NO_MIC_STEREO_HEADSET = 31, /* Hifi Stereo Headset with handset MIC */
   VAL_SOUND_DEVICE_DUALMIC_ON = 32, /* turn on dual mic */
   VAL_SOUND_DEVICE_DUALMIC_OFF = 33, /* turn off dual mic */
   VAL_SOUND_DEVICE_NUM
} ValSoundDeviceT;

typedef enum
{
   VAL_SOUND_VOLUME_INCREASE = -3, /* HWD_AUDIO_VOL_UP,*/
   VAL_SOUND_VOLUME_DECREASE = -2, /* HWD_AUDIO_VOL_DOWN,*/
   VAL_SOUND_VOLUME_UNMUTE   = -1, /* HWD_AUDIO_VOL_UNMUTE,*/
   VAL_SOUND_VOLUME_MUTE     = 0,  /* HWD_AUDIO_VOL_MUTE,*/
   VAL_SOUND_VOLUME_LEVEL_1  = 1,  /* HWD_AUDIO_VOL_1 */
   VAL_SOUND_VOLUME_LEVEL_2,       /* HWD_AUDIO_VOL_2 */
   VAL_SOUND_VOLUME_LEVEL_3,       /* HWD_AUDIO_VOL_3 */
   VAL_SOUND_VOLUME_LEVEL_4,       /* HWD_AUDIO_VOL_4 */
   VAL_SOUND_VOLUME_LEVEL_5,       /* HWD_AUDIO_VOL_5 */
   VAL_SOUND_VOLUME_LEVEL_6,       /* HWD_AUDIO_VOL_6 */
   VAL_SOUND_VOLUME_LEVEL_7,       /* HWD_AUDIO_VOL_7 */
   VAL_SOUND_VOLUME_LEVEL_8,       /* HWD_AUDIO_VOL_8 */
   VAL_SOUND_VOLUME_LEVEL_9,       /* HWD_AUDIO_VOL_9 */
   VAL_SOUND_VOLUME_LEVEL_10,      /* HWD_AUDIO_VOL_10 */
   VAL_SOUND_VOLUME_DEFAULT  = VAL_SOUND_VOLUME_LEVEL_3,
   VAL_SOUND_VOLUME_MAX      = VAL_SOUND_VOLUME_LEVEL_10,
   VAL_SOUND_VOLUME_CURRENT
} ValSoundVolumeT;

typedef enum
{
   VAL_SOUND_FORMAT_TONE = 0,
   VAL_SOUND_FORMAT_KEY_TONE,
   VAL_SOUND_FORMAT_MIDI_MELODY,
   VAL_SOUND_FORMAT_MP3_MELODY,
   VAL_SOUND_FORMAT_IMELODY,
   VAL_SOUND_FORMAT_PCM,
   VAL_SOUND_FORMAT_CMF,
   VAL_SOUND_FORMAT_AAC_MELODY,
   VAL_SOUND_FORMAT_VOCODER,
   VAL_SOUND_FORMAT_FM,
   VAL_SOUND_FORMAT_MP4_AAC_MELODY,   /* AAC audio in MP4 file */
   VAL_SOUND_FORMAT_MP4_MP3_MELODY,   /* MP3 audio in MP4 file; for S/W MP3 only */
   VAL_SOUND_FORMAT_WMA,
   VAL_SOUND_FORMAT_WAV_MELODY, 
   VAL_SOUND_FORMAT_I2S_INPUT,
   VAL_SOUND_FORMAT_ASF_WMA_MELODY,  /* WMA audio in WMV/ASF file */  
   VAL_SOUND_FORMAT_WAV_ENCODER,     /* Record WAV format file    */
   VAL_SOUND_FORMAT_NUM
} ValSoundFormatT;

typedef enum
{
   VAL_SOUND_STOP_IMMEDIATE = 0,
   VAL_SOUND_STOP_DECAY
} ValSoundStopT;

typedef enum
{
   VAL_SOUND_KEY_VOLUME = 0,
   VAL_SOUND_TONE_VOLUME,
   VAL_SOUND_RING_VOLUME,
   VAL_SOUND_MUSIC_VOLUME,
   VAL_SOUND_VOICE_VOLUME,
   VAL_SOUND_MIC_VOLUME,
   VAL_SOUND_VOLUME_NUM
} ValSoundVolumeModesT;

typedef enum
{
   VAL_SOUND_VOICE_DEVICE = 0,
   VAL_SOUND_RINGER_DEVICE,
   VAL_SOUND_MUSIC_DEVICE,
   VAL_SOUND_DEV_MODE_NUM
} ValSoundDeviceModesT;

typedef enum
{
    VAL_SOUND_CHAN_MUTE_NONE = 0,
    VAL_SOUND_CHAN_MUTE_RIGHT,
    VAL_SOUND_CHAN_MUTE_LEFT
} ValSoundChanMuteT;

/* Data type for ValMusicPlayEX() ==>>>> */

#define VAL_SND_MUSIC_PLAYID_INVALID   0
#define VAL_SND_INVALID_STARTTIME         0xFFFFFFFF 
#define VAL_SND_INVALID_STARTOFFSET     0xFFFFFFFF

/* ValMusicPlayParamT:
 *    Data type for passing parameters to play music and feedback status when music ending.   
 */

typedef struct
{
     /* DataPtr:
       * Pointer to music data; should be NULL if FileNameP provided 
       */
      void  *DataPtr;      

     /* File:
       * File handle of  music file; should be FSI_INVALID_FILE_HANDLE if invalid 
       */
      ValFsiHandleT  File;

     /* FileNameP:
       * file name to be played; should be NULL if DataPtr provided 
       */
      char  *FileNameP; 

     /* Size:
       * Size of file or data buffer in bytes 
       */
      uint32 Size;      

     /* Format:
       * Music format is MIDI, MP3, AAC, AMR, WAV and QCP.
       * With values defined by ValSoundFormatT enum type.
       */
      ValSoundFormatT  Format;     
	                                             
     /* Iterations:
       * Number of times to repeat playing music. 0 or 1 means playing once. 
       */
      uint8    Iterations;  

     /* Duration:
       * Maximum time duration in unit of seconds to play music. This parameter is
       * usually used to implment alarm clock alert sound. Set to  
       * VAL_RINGER_DURATION_FOREVER if not used. */
      uint32  Duration;   

     /* BaseOffset 
       * File offset where 1st frame of music starts (for MP3, this
       * is after the container data); if unknown, set to zero
       */
       uint32  BaseOffset;
				 
     /* StartTime:
       * Starting time in millisecond to play music. A valid value is [0..total_time] of music file.
       */
      uint32  StartTime; 

     /* StartByteOffset:
       * Byte offset to play music file or data buffer. This parameter will be set to 0, or value
       * feedbacked by VAL VAL_SND_END_MUSIC_EVENT in ValSndEventMsgT struct. 
       */
      uint32  StartByteOffset; 
                                             
     /* Ringer:
       * TRUE - phone operate.
       */
      bool  Ringer;      
} ValMusicPlayParamT;

/*
 * ValMusicPlayIdT is used to identify the correlation of calling ValMusicPlayEx and VAL_SND_END_MUSIC_EVENT.
 */
typedef uint32 ValMusicPlayIdT;

/* data for VAL_SND_END_MUSIC_EVENT */
typedef struct
{
   ValSoundFormatT   MusicPlayed;  /* Midi or MP3 */
   ValAppStatusT     ReturnCode;   /* pass/fail reason */
   void              *DataPtr;     /* data buffer used for music 
                                      if applicable, otherwise - NULL */
   ValFsiHandleT     FileHandle;   /* file handle used for music 
                                      if applicable, otherwise - (-1) */

   ValMusicPlayIdT MusicPlayId; /* Associate this END_MUSIC_EVENT to the PlayId that is returned by the ValMusicPlayEx.*/
   uint32  MusicElapseTime; /* Elapse time in millisecond when music stop. A valid value is [0..total_time] of music file.*/
   uint32  MusicStopOffset; /* Byte offset when music stop. */
   uint8  StopEndFlag;   /*'1' indicate caller use stop api to end current playback, '0' and other values indicate a internal end*/
} ValSndEventMsgT;

/* data for VAL_SND_MUSIC_STATUS_EVENT */
typedef enum
{
    VAL_SND_MUSIC_RUNNING,
    VAL_SND_MUSIC_SUSPENDED,
    VAL_SND_MUSIC_FAST_FWD,
    VAL_SND_MUSIC_REWIND,
    VAL_SND_MUSIC_DONE,
    VAL_SND_MUSIC_FRAME_SYNC_NOT_FOUND,
    VAL_SND_MUSIC_LOSS_OF_FRAME_SYNC,
    VAL_SND_MUSIC_TICK_UPDATE  /*once per second*/
} ValSndMusicStatusT;

typedef struct
{
    ValSndMusicStatusT Status;
    uint32             CurrByteOffset;
    uint32             FirstFrameOffsetBytes;
    uint32             BitRate;
    uint16             AvgBytesPerSecond;
    uint16             SamplesPerSecond;
    uint16             LossOfFrameSyncErrCnt;
    uint32             MusicPlayId;  /*0 indicate invalid*/
    uint32             ElapsedTime; /*uint of millisecond*/
} ValMusicStatusEventMsgT;

/*Audio format enum, defined base on Sunplus556A*/
typedef enum
{
	VAL_AUDIO_FORMAT_NONE = 0,
	VAL_AUDIO_FORMAT_AMR,
	VAL_AUDIO_FORMAT_AAC,
	VAL_AUDIO_FORMAT_PCM,
	VAL_AUDIO_FORMAT_ADPCM,
	VAL_AUDIO_FORMAT_MP3,
	VAL_AUDIO_FORMAT_WMA,
	VAL_AUDIO_FORMAT_MIDI,
	VAL_AUDIO_FORMAT_WAV,
	VAL_AUDIO_FORMAT_QCP,
	
	VAL_AUDIO_FORMAT_NUM
}ValAudioFormatT;

typedef struct 
{
    uint32 FstHdrOffset;
    uint32 audioTotalSecond; 
    uint32 avgBps;	/*unit is KHz*/
    ValAudioFormatT audioFormat;   
    uint16 isSeekable;  /*0: unsupport position set funciton; 1: support position set funciton*/
    uint16 isVBR;  /*0: CBR; 1: VBR*/
    uint8  *TitleP; /*the max string lenth should be 256*/
    uint8  *ArtistP; /*the max string lenth should be 256*/
    uint8  *AlbumP; /*the max string lenth should be 256*/
} ValMusicInfoT;

typedef enum
{
   VAL_SOUND_VIBRATE_WEAK = 0,
   VAL_SOUND_VIBRATE_MEDIUM,
   VAL_SOUND_VIBRATE_STRONG,
   VAL_SOUND_VIBRATE_NUM
} ValSoundVibrateT;

/*EQ enum, should have the same define as HwdMusicEQT*/
typedef enum
{
	VAL_MUSIC_EQ_NONE=0,
	VAL_MUSIC_EQ_DBB,
	VAL_MUSIC_EQ_ROCK,
	VAL_MUSIC_EQ_JAZZ,
	VAL_MUSIC_EQ_POP,
	VAL_MUSIC_EQ_LIVE,
	VAL_MUSIC_EQ_MAX
} ValMusicEQT;

/* VAL_SND_SOUND_STOP_MSG */
typedef PACKED_PREFIX struct
{
    ValSoundStopT     StopMode;
} PACKED_POSTFIX  ValSoundStopMsgT;

typedef enum
{
    VAL_MIDI_PARSER_ACCURATE,
    VAL_MIDI_PARSER_FAST
} ValMidiParserT;

/* VAL audio sampling rates, used in ValMusicPlay API */
typedef enum
{
    _VAL_SAMP_RATE_8000,
    _VAL_SAMP_RATE_11025,  
    _VAL_SAMP_RATE_12000,
    _VAL_SAMP_RATE_16000,
    _VAL_SAMP_RATE_22050,  
    _VAL_SAMP_RATE_24000,
    _VAL_SAMP_RATE_32000,
    _VAL_SAMP_RATE_44100,  
    _VAL_SAMP_RATE_48000,
    _VAL_NUM_SAMPLING_RATES
} ValSndSamplingRatesT;

typedef enum
{
    SOUND_FORMAT_TONE = 0,
    SOUND_FORMAT_DTMF,
    SOUND_FORMAT_MIDI,
    SOUND_FORMAT_FREQ,
    SOUND_FORMAT_USERDATA,
    SOUND_FORMAT_TONE_SEQ,  /*tone sequence*/
    SOUND_FORMAT_NUM
} SoundFormatT;

typedef enum
{
    VAL_VOLUME_CURRENT = VAL_SOUND_VOLUME_CURRENT,
    VAL_VOLUME_MUTE    = VAL_SOUND_VOLUME_MUTE,
    VAL_VOLUME_LEVEL_1 = VAL_SOUND_VOLUME_LEVEL_1,
    VAL_VOLUME_LEVEL_2 = VAL_SOUND_VOLUME_LEVEL_3,
    VAL_VOLUME_LEVEL_3 = VAL_SOUND_VOLUME_LEVEL_5,
    VAL_VOLUME_LEVEL_4 = VAL_SOUND_VOLUME_LEVEL_7,
    VAL_VOLUME_LEVEL_5 = VAL_SOUND_VOLUME_LEVEL_8,
    VAL_VOLUME_LEVEL_MAX = VAL_SOUND_VOLUME_LEVEL_8,
    VAL_VOLUME_NUM = VAL_VOLUME_LEVEL_MAX
} ValVolumeT;

typedef struct
{
    SoundFormatT      Format;

    ValVolumeT        Volume;

    uint8             Iteration;
    uint32            Duration;

    uint32            MidiFileId;   /* valid when the Format indicate to play midi file */
    uint32            DtmfId;       /* valid when the Format indicate to play DTMF. */
    uint16            DtmfOnLength;
    uint32            ToneId;       /* valid when the Format indicate to play Tone/Single-Tone. */
    uint16            ToneFreq;     /* valid if the format indicate to play with specified frequency. */
    ValSoundFormatT   UserDataType; /*valid if the format indicate to play with user data.*/
    const uint8*      UserDataP;
    uint32            UserDataLen;

    /*following items are added for SOUND_FORMAT_TONE_SEQ format*/
    ValSndToneDataT*  ToneSeqP;
    uint16            NumTonesInSeq;
} PlayStructT;

/* Audio Sidetone parameters */
typedef PACKED_PREFIX struct
{
    uint16 SidetoneMultiplier;
    bool   SidetoneEnable [VAL_SOUND_DEVICE_NUM];
} PACKED_POSTFIX  ValAudioSidetoneParamsT;


/*===================================*/
/*-- Audio Tuning API definitions -- */
/*===================================*/

/**** NOTE that the following enumerations, definitions and structures 
 **** used for audio tuning should NOT be changed since they must remain
 **** compatible with the Audio Tuning Utility.
 */

#define VAL_SND_TUNE_PATHNAME_LEN  90

/* Audio tuning modes and parameters that can be read/updated */
typedef enum
{
    TUNE_SPKR_VOL = 0,
    TUNE_MIC_VOL,

    TUNE_SIDETONE,
    TUNE_MIC_BIF_MODE,
    TUNE_MIC_ANS_MODE,
    TUNE_SPKR_FIF_MODE,
    TUNE_ACP_MODE,
    TUNE_AEC_MODE,

    TUNE_ACP_PARAMS,
    TUNE_AEC_PARAMS,
    TUNE_MIC_ANS_PARAMS,
    TUNE_TTY_PARAMS,
    TUNE_MIC_FIF_PARAMS,    // Read only, no updates
    TUNE_MIC_BIF_PARAMS,
    TUNE_SPKR_FIF_PARAMS,
    TUNE_RINGER_PARAMS,     // Read only, no updates

    NUM_TUNE_PARAM_TYPES
} ValSndTuneParamTypesT;

/* Audio tuning status/error codes */
typedef enum
{
    VAL_SND_TUNE_STATUS_OK = 0,
    VAL_SND_TUNE_PARAM_TYPE_INVAL,
    VAL_SND_TUNE_DATA_SIZE_INVAL,
    VAL_SND_TUNE_FILE_GET_LEN_FAIL,
    VAL_SND_TUNE_FILE_OPEN_FAIL,
    VAL_SND_TUNE_FILE_READ_FAIL,
    VAL_SND_TUNE_FILE_WRITE_FAIL,
    VAL_SND_TUNE_FILE_DOES_NOT_EXIST,
    VAL_SND_TUNE_MALLOC_FAILED,
    VAL_SND_TUNE_FILE_FORMAT_INVALID,
    VAL_SND_TUNE_PARAM_UPDATE_NOT_ALLOWED,
    VAL_SND_TUNE_FILE_LEN_MISMATCH,
    VAL_SND_TUNE_MODE_INVAL
} ValSndTuneStatusT;
   
/* Source of data to initialize the parameter table */
typedef enum
{
    INIT_DEFAULTS = 0,
    INIT_FROM_FILE
} ValSndTuneInitTypesT;

/* Parameter types used by the DSPv */
typedef enum
{
    DSPV_DEFAULT_PARMS = 0,
    DSPV_HANDSET_PARMS,
    DSPV_HANDSET_NC_PARMS,
    DSPV_HEADSET_PARMS,
    DSPV_HANDSFREE_PARMS,
    DSPV_LDSPKR_PARMS,
    DSPV_BLUETOOTH_PARMS,
    NUM_DSPV_PARM_TYPES
} ValSndDspvParamTypesT;

/* These defines are purposely hard-coded since these are the definitions 
 * used by the Audio Tuning Utility.
 */
#define VAL_NUM_SPKR_VOL_LEVELS  5
#define VAL_NUM_AUDIO_MODES      30

#ifdef SYS_OPTION_VARIABLE_VOL_INCR
#define VAL_TUNE_SPKR_VOL_SIZE  (VAL_SOUND_VOLUME_MAX * VAL_NUM_SPKR_VOL_LEVELS)
#else   
#define VAL_TUNE_SPKR_VOL_SIZE  (2 * VAL_NUM_SPKR_VOL_LEVELS)
#endif

/* This union contains all audio parameters for both the READ and UPDATE messages.
 *
 * IMPORTANT: For any data arrays that are indexed by VAL_NUM_AUDIO_MODES,
 *            note that the mode order is as defined in the HWD drivers.
 *            Specifically, refer to the the HwdAudioModeT enum in hwdaudioapi.h.
 */
typedef PACKED_PREFIX union
{
    uint16                    SpkrVol[VAL_NUM_AUDIO_MODES][VAL_TUNE_SPKR_VOL_SIZE];
    uint16                    MicVol[VAL_NUM_AUDIO_MODES];
    ValAudioSidetoneParamsT   Sidetone;
    uint16                    MicBifMode[VAL_NUM_AUDIO_MODES];      /* 1 = enable, 0 = disable */
    bool                      MicAnsMode[VAL_NUM_AUDIO_MODES];
    uint16                    SpkrFifMode[VAL_NUM_AUDIO_MODES];     /* 1 = enable, 0 = disable */
    IpcDsvSetAudioAcpModeMsgT AcpMode[VAL_NUM_AUDIO_MODES];
    uint16                    AecMode[VAL_NUM_AUDIO_MODES];         /* 1 = enable, 0 = disable */

    IpcDsvSendAudioAcpCfgParamsMsgT     AcpParams[NUM_DSPV_PARM_TYPES];
    IpcDsvSendAudioAecMainCfgParamsMsgT AecParams[NUM_DSPV_PARM_TYPES]; /* No HANDSET_NC,
                                                                           no HANDSFREE  */
    IpcDsvSendMicAnsCfgParamsMsgT   MicAnsParams;
    IpcDsvSendAudioTtyCfgParamsMsgT TtyParams;
    IpcDsvSendMicFifCfgParamsMsgT   MicFifParams;
    IpcDsvSendMicBifCfgParamsMsgT   MicBifParams [NUM_DSPV_PARM_TYPES];
    IpcDsvSendSpkrFifCfgParamsMsgT  SpkrFifParams [NUM_DSPV_PARM_TYPES]; /* No HANDSET_NC */
    IpcDsvRngrConfigMsgT            RingerParams;
} PACKED_POSTFIX  ValSndTuneDataT;

/* Command/response message structures to Init, Store, Read and Update audio tuning parms */
typedef PACKED_PREFIX struct
{
    ExeRspMsgT           RspInfo;
    ValSndTuneInitTypesT DataSource;
    char                 PathName [VAL_SND_TUNE_PATHNAME_LEN];
} PACKED_POSTFIX  ValSndInitTuneParamsMsgT;

typedef PACKED_PREFIX struct
{
    ValSndTuneStatusT Status;
} PACKED_POSTFIX  ValSndInitTuneParamsRspMsgT;

typedef PACKED_PREFIX struct
{
    ExeRspMsgT RspInfo;
    char       PathName [VAL_SND_TUNE_PATHNAME_LEN];
} PACKED_POSTFIX  ValSndStoreTuneFileMsgT;

typedef PACKED_PREFIX struct
{
    ValSndTuneStatusT Status;
} PACKED_POSTFIX  ValSndStoreTuneFileRspMsgT;

typedef PACKED_PREFIX struct
{
    ExeRspMsgT            RspInfo;
    ValSndTuneParamTypesT ParamType;
    bool                  ReadDefaults;
} PACKED_POSTFIX  ValSndReadTuneParamsMsgT;

typedef PACKED_PREFIX struct
{
    ValSndTuneParamTypesT ParamType;
    ValSndTuneStatusT     Status;
    uint16                VersionNum;
    uint8                 Mode;   /* Refer to HwdAudioModeT in hwdaudioapi.h */
                                  /* Mode used only for Spkr Vol (msg split, too large) */
    uint16                Size;
    ValSndTuneDataT       Data;
} PACKED_POSTFIX  ValSndReadTuneParamsRspMsgT;

typedef PACKED_PREFIX struct 
{
    ExeRspMsgT            RspInfo;
    ValSndTuneParamTypesT ParamType;
    uint8                 Mode;   /* Refer to HwdAudioModeT in hwdaudioapi.h */
                                  /* Mode used only for Spkr Vol (msg split, too large) */
    uint16                Size;
    ValSndTuneDataT       Data;
} PACKED_POSTFIX  ValSndUpdateTuneParamsMsgT;

typedef PACKED_PREFIX struct
{
    ValSndTuneParamTypesT ParamType;
    ValSndTuneStatusT     Status;
    uint8                 Mode;   /* Refer to HwdAudioModeT in hwdaudioapi.h */
                                  /* Mode used only for Spkr Vol (msg split, too large) */
} PACKED_POSTFIX  ValSndUpdateTuneParamsRspMsgT;

/* VAL API's to Init, Read, Update and Store audio tuning parameters */
extern void ValSndInitTuneParams (ValSndInitTuneParamsMsgT *MsgP);
extern void ValSndReadTuneParams (ValSndReadTuneParamsMsgT *MsgP);
extern void ValSndUpdateTuneParams (ValSndUpdateTuneParamsMsgT *MsgP);
extern void ValSndStoreTuneFile (ValSndStoreTuneFileMsgT *MsgP);

/*------------------------------------------------------------------------
 * Global function prototypes
 *------------------------------------------------------------------------*/
/*****************************************************************************

   FUNCTION:      ValSetVolume
   DESCRIPTION:   sets current volume
   PARAMETERS:    ValSoundVolumeModesT VolMode,
                  ValSoundVolumeT Volume
   RETURNS:       none

   NOTE:          This function should be called once per VolMode
                  during UI initialization; later it can be called again
                  only if user changes the volume settings
                  
*****************************************************************************/
void ValSetVolume(ValSoundVolumeModesT VolMode, ValSoundVolumeT Volume);

/*****************************************************************************

   FUNCTION:      ValSoundChanMute
   
   DESCRIPTION:   Mutes only the right channel or only the left channel.
                  This applies only to stereo devices that are not currently muted.
                  
                  The indicated channel stays muted until this function unmutes it
                  with VAL_SOUND_CHAN_MUTE_NONE parameter.
                  
   PARAMETERS:    ChanMute - indicates which channel to mute
   
   RETURNS:       none

*****************************************************************************/
void ValSoundChanMute (ValSoundChanMuteT ChanMute);

/*****************************************************************************

   FUNCTION:      ValSetLowVolume
   DESCRIPTION:   Set the music/ringer volume to constant (lower) volume.
   PARAMETERS:    bool LowFlag
   RETURNS:       none

   NOTE:          This function is used to set ringer/music volume to a constant "low" value
                  for special applications:
                  
   		          *** ATTENTION: The default is to perform regular music/ringer volume adjustments.
                      When volume is set to "low" volume, it is automatically reset after the 
                      current inger/music play has completed (if active), or for the next 
                      ringer/music play only.
                      THAT IS, IF APP NEEDS TO PLAY RINGER/MUSIC WITH LOW VOLUME, THIS 
                      FUNCTION SHOULD BE CALLED EVERY TIME.
                  
*****************************************************************************/
void ValSetLowVolume(ValSoundVolumeModesT VolMode, bool LowFlag);

/*****************************************************************************
 
   FUNCTION:		ValGetVolume
   DESCRIPTION:	returns current volume
   PARAMETERS:		ValSoundVolumeModesT VolMode
   RETURNS:		   ValSoundVolumeT

*****************************************************************************/
ValSoundVolumeT ValGetVolume(ValSoundVolumeModesT VolMode);

/*****************************************************************************
 
   FUNCTION:		ValTonePlay
   
   DESCRIPTION:	    Plays a Comfort TONE or a DTMF 
   
   PARAMETERS:      SoundId    - Tone selection;
                                   - use ValSndToneIdT if Format param is VAL_SOUND_FORMAT_TONE
                                   - use ValSndDtmfIdT if Format param is VAL_SOUND_FORMAT_KEY_TONE
                                 
                    Format     - VAL_SOUND_FORMAT_TONE (comfort tone) or 
                                     VAL_SOUND_FORMAT_KEY_TONE (DTMF)
                                     
                    Iterations - for VAL_SOUND_FORMAT_KEY_TONE: 
                                     VAL_SOUND_CONTINUES for continuous DTMF
                                     VAL_SOUND_BURST for burst DTMF
                                     
                    NumIterations - for VAL_SOUND_FORMAT_TONE:
                                     0:     uses standard tone iterations 
                                     other: customized times by User
                                  - for VAL_SOUND_FORMAT_KEY_TONE
                                     >1:    the time(ms) length to play
                                     0,1:   normal

   RETURNS:		   error code
                      VAL_APP_OK = 0,
                      VAL_APP_CONFLICT,
                      VAL_APP_FAILED_ERROR,
                      VAL_APP_WRONG_PLAY_FORMAT
                      VAL_APP_TONE_ID_INVALID

*****************************************************************************/
ValAppStatusT ValTonePlay( uint8           SoundId,
                           ValSoundFormatT Format,
                           uint8           Iterations, 
                           uint16          NumIterations);

/*****************************************************************************

   FUNCTION:      ValUserTonePlay
   
   DESCRIPTION:   Play a user-defined tone sequence of single or dual tones.
   
   PARAMETERS:    ToneDataP     - sequence of tones to play, specifying duration and
                                  frequency for each tone (see below)
                  NumTonesInSeq - number of tones in the sequence
                  Iterations    - number of times to repeat tone

       Example of Tone Data: 
            This tone sequence plays 3 single tones in succession as follows:
                play 250 Hz tone for 200 msec (20 msec/frame X 10 frames)
           then PAUSE for the next 200 msec (ie. frequency of zero is "silence")
           then play 350 Hz tone for 1 second
           then stop.
     
           static ValSndToneDataT ToneSample [3] =
           {
               { 10, 250, 0 },
               { 10,   0, 0 },
               { 50, 350, 0 }
           };
   
   RETURNS:       VAL_APP_OK              - tone play successful
                  VAL_APP_TONE_ID_INVALID - indicates that tone parameters are invalid
                  VAL_APP_CONFLICT        - other audio is active

*****************************************************************************/
ValAppStatusT ValUserTonePlay (ValSndToneDataT *ToneDataP, 
                               uint16           NumTonesInSeq, 
                               uint16           Iterations);

/*****************************************************************************
   FUNCTION:     ValMusicPlayEx

   DESCRIPTION:  Play music in music mode or ringer mode.

   PARAMETERS:      
                ValMusicPlayExParam    - struct passing parameters to start playing music.

                MusicPlayId - For API caller (e.g. UI/JAVA/APPs) to keep track of music play status.
                                    Set to VAL_SND_MUSIC_PLAYID_INVALID if the API caller wants ValMusicPlayEx API
                                  to generate an ID. MusicPlayId will be assigned to ValSndEventMsgT.MusicPlayId for 
                                  VAL_SND_END_MUSIC_EVENT callback

   Precondition: Struct pointer MusicPlayExParam cannot be NULL. Caller has to create ValMusicPlayParamT
                struct type variable and pass its pointer here.

   Postcondition: *MusicPlayId holds value that will be assigned to ValSndEventMsgT.MusicPlayId
                along with VAL_SND_END_MUSIC_EVENT callback.
   
   RETURNS:      error code
                 VAL_APP_OK = 0,
                 VAL_APP_FILE_FORMAT_INVALID,
                 VAL_APP_FILE_IO_ERROR,
                 VAL_APP_FAILED_ERROR

*****************************************************************************/
uint16 ValMusicPlayEx(ValMusicPlayParamT *MusicPlayExParam, ValMusicPlayIdT *MusicPlayId);

/*****************************************************************************

   FUNCTION:     ValMusicPlay

   DESCRIPTION:  Play MIDI, IMelody, MP3, AAC, VOCODER, FM or PCM data

   PARAMETERS:  DataPtr     - Pointer to music data; should be NULL
                              if File Handle provided
                 File       - File handle
                 Size       - Size of file in bytes
                        *** - For I2S_INPUT format only, this field is the SAMPLING RATE
                              (in which case use ValSndSamplingRatesT enum)
                 FileNameP  - pointer to a music file 
                 Format     - Music format is MIDI or MP3 or IMelody or AAC
                 Iterations - Number of times to play music data
                 Duration   - Either VAL_RINGER_DURATION_FOREVER or duration of play in msec
                 BaseOffset - File offset where 1st frame of music starts (for MP3, this
                              is after the container data); if unknown, set to zero
                 StartingOffset - Number of millisecond after the Base Offset where to 
                                  start playing the music
                 Ringer     - if TRUE plays melody as a ringer (via RINGER device path)
                              otherwise plays melody via MUSIC device

          *** NOTE: The StartingOffset parameter is in bytes for all music types
          ***       except for MIDI, since most music files are sequential and the
          ***       HWD music drivers handle offset in bytes.
          ***       However, since MIDI is not a SEQUENTIAL file, the StartingOffset
          ***       parameter for MIDI is in milliseconds.
          
   RETURNS:  error code:
                 VAL_APP_OK = 0,
                 VAL_APP_WRONG_PLAY_FORMAT,
                 VAL_APP_CONFLICT,
                 VAL_APP_FAILED_ERROR

*****************************************************************************/
uint16 ValMusicPlay(void           *DataPtr, 
                    ValFsiHandleT   File, 
                    uint32          Size, 
                    char 	       *FileNameP,
                    ValSoundFormatT Format, 
                    uint8           Iterations,
                    uint32	        Duration,
                    uint32          BaseOffset,
                    uint32          StartingOffset,
                    bool            Ringer);

/*****************************************************************************
 
   FUNCTION:    ValSoundPlayAudio
   DESCRIPTION:	an API function to play any audio type; add for modem porting;
   PARAMETERS:	*pData - pointer to the PlayStructT
                *PlayId -

   RETURNS:		bool - return

*****************************************************************************/
void ValSoundPlayAudio (PlayStructT *pData, uint32 *PlayId);

/*****************************************************************************
 
   FUNCTION:     ValMusicFastFwd
   
   DESCRIPTION:  Fast Forward an active MMApps music file
   
   PARAMETERS:   PlayTime    - Segment of skipped melody to play (in tenth seconds);
                               must be less than AdvanceTime. If zero, then will
                               Fast Forward "AdvanceTime" and start playing.
                                
                 AdvanceTime - Time (in seconds) to advance for each play segment.
                               If PlayTime is zero, the file position is moved
                               this number of seconds and play continues.
                 
   RETURNS:      None

*****************************************************************************/
void ValMusicFastFwd( uint16 PlayTime, uint16 AdvanceTime );

/*****************************************************************************
 
   FUNCTION:     ValMusicRewind
   
   DESCRIPTION:  Rewind an active MMApps music file
   
  PARAMETERS:     PlayTime   - Segment of skipped melody to play (in tenth seconds);
                               must be less than RewindTime. If zero, then will
                               Rewind "AdvanceTime" and start playing.
                                
                  RewindTime - Time (in seconds) to rewind for each play segment.
                               If PlayTime is zero, the file position is moved
                               this number of seconds and play continues.
                 
   RETURNS:      None

*****************************************************************************/
void ValMusicRewind( uint16 PlayTime, uint16 AdvanceTime );

/*****************************************************************************
 
   FUNCTION:     ValMusicSuspend
   
   DESCRIPTION:  Suspend an active MMApps music file
   
   PARAMETERS:   None.
                 
   RETURNS:      None

*****************************************************************************/
uint16 ValMusicSuspend( void );

/*****************************************************************************
 
   FUNCTION:     ValMusicResume
   
   DESCRIPTION:  Resume an active MMApps music file
   
   PARAMETERS:   None.
                 
   RETURNS:      None

*****************************************************************************/
uint16 ValMusicResume( void );
           
/*****************************************************************************
 
   FUNCTION:     ValMusicGetTotalPlayTime
   
   DESCRIPTION:  Get the total play time of a music file.
   
   PARAMETERS:   FileNameP - Pointer to music file name; only used if DataP is NULL
                 DataP     - Pointer to music data buffer; if NULL then FileNameP is used
                 DataBytes - The total number of bytes in the DataP buffer;
                             not used for file
                 Format    - Music format; 
                 
   RETURNS:      Total Play Time in seconds or VAL_SND_FAILURE if error

*****************************************************************************/
int32 ValMusicGetTotalPlayTime (char           *FileNameP,
                                void           *DataP, 
                                uint32          DataBytes, 
                                ValSoundFormatT Format);

/*****************************************************************************
  FUNCTION NAME: ValMusicElapseTimeGet
  
  DESCRIPTION:

       Get elapsed time for active Music.
       Add more music types later.

  PARAMETERS:

       pSec

  RETURNED VALUES:

      VAL_APP_OK or VAL_APP_FAILED_ERROR.

*****************************************************************************/
uint16 ValMusicElapseTimeGet(uint32 *pSec);

/*****************************************************************************
  
  FUNCTION NAME: ValMusicPositionSet
  
  DESCRIPTION:

       Sets the position as indicated for an active music file and continues playing.

  PARAMETERS:

       Seconds from start of file to continue playing.

  RETURNED VALUES:

       VAL_APP_OK or VAL_APP_FAILED_ERROR

*****************************************************************************/
uint16 ValMusicPositionSet(uint32 Second);

/*****************************************************************************
  
  FUNCTION NAME: ValMidiSetConfigParms
  
  DESCRIPTION:

       Sets the MIDI configuration parameter(s).
       
       AlgorithmSelect: 
              Selects whether MIDI parsing is to use an ACCURATE algorithm or 
              a FAST algorithm.
              
              This parameter applies to the calculation of total MIDI play time.
              
              VAL_MIDI_PARSER_ACCURATE - Use accurate algorithm to calculate 
                     the MIDI play time with 100% accuracy. This algorithm 
                     sacrifices execution time for accuracy and therefore may
                     be significantly slower.
                     
              VAL_MIDI_PARSER_FAST - Use fast algorithm to calculate the MIDI play
                     time. This algorithm sacrifices accuracy for execution time and
                     therefore may be significantly faster.
                     
              ** This parameter is defaulted to the FAST algorithm. There is no known
                 reason that this mode will result in any problems with accuracy.       

  PARAMETERS:

       AlgorithmSelect: VAL_MIDI_PARSER_ACCURATE or VAL_MIDI_PARSER_FAST.

  RETURNED VALUES:
  
       None.

*****************************************************************************/
void ValMidiSetConfigParms (ValMidiParserT AlgorithmSelect);

/*****************************************************************************
 
   FUNCTION:		ValSoundStop
   DESCRIPTION:	stops playing any audio
   PARAMETERS:		ValSoundStopT StopType
   RETURNS:		   None

*****************************************************************************/
void ValSoundStop( ValSoundStopT StopType );


/*****************************************************************************
 
   FUNCTION:		ValSoundStopAll
   DESCRIPTION:	    Stops any audio that is playing as well as any pending sound.
   PARAMETERS:		ValSoundStopT StopType
   RETURNS:		    None

*****************************************************************************/
void ValSoundStopAll (ValSoundStopT StopType);

/*****************************************************************************
 
	FUNCTION:		ValSetKeyVolumeAdjustMode
	DESCRIPTION:	setups volume adjust mode
	PARAMETERS:		bool On 
	RETURNS:		   None

*****************************************************************************/
void ValSetKeyVolumeAdjustMode( bool On );

/*****************************************************************************
 
	FUNCTION:		ValSetKeyVolume
	DESCRIPTION:	setups key volume
	PARAMETERS:		ValSoundVolumeT Volume 
	RETURNS:		   None

*****************************************************************************/
void ValSetKeyVolume( ValSoundVolumeT Volume );

/*****************************************************************************
 
	FUNCTION:		ValSndEnableContKey
	DESCRIPTION:	switchs short/continuous key mode
	PARAMETERS:		bool On 
	RETURNS:		   None

*****************************************************************************/
void ValSndEnableContKey( bool On );

/*****************************************************************************
 
	FUNCTION:		ValSndSilenceKey
	DESCRIPTION:	key lock control
	PARAMETERS:		bool Lock 
	RETURNS:		   None

*****************************************************************************/
void ValSndSilenceKey( bool On );

/*****************************************************************************
 
	FUNCTION:		ValGetSndSilenceKey
	DESCRIPTION:	Return state of key lock control
	PARAMETERS:		None 
	RETURNS:		SilenceKey status TRUE/FALSE

*****************************************************************************/
bool ValGetSndSilenceKey(void);

/*****************************************************************************

  FUNCTION NAME: ValSndRegister
  DESCRIPTION:   This routine is used to registers a callback to notify the 
                 caller when the sound event has happened. 
  PARAMETERS:    EventFuncP  - the function pointer
  RETURNS:       Register Identifier

*****************************************************************************/
RegIdT ValSndRegister( ValEventFunc EventFuncP );

/*****************************************************************************
 
  FUNCTION NAME: ValSndUnRegister
  DESCRIPTION:   This routine unregisters a callback 
  PARAMETERS:    RegId 
  RETURNS:       None.
 
*****************************************************************************/
void ValSndUnRegister( RegIdT RegId );

/*****************************************************************************

  FUNCTION NAME: ValMusicStatusRegister
  DESCRIPTION:   This routine is used to register a callback for receiving
                 periodic music status information.
  PARAMETERS:    EventFuncP  - the function pointer
  RETURNS:       Register Identifier

*****************************************************************************/
RegIdT ValMusicStatusRegister(ValEventFunc EventFuncP);

/*****************************************************************************
 
  FUNCTION NAME: ValMusicStatusUnregister
  DESCRIPTION:   This routine unregisters the Music Status callback 
  PARAMETERS:    RegId 
  RETURNS:       None.
 
*****************************************************************************/
void ValMusicStatusUnregister(RegIdT RegId);

/*****************************************************************************

  FUNCTION NAME: ValSndSrvOptConnRegister
  DESCRIPTION:   This routine is used to register a callback for receiving
                 notification when a voice service option is connected.
  PARAMETERS:    EventFuncP  - the function pointer
  RETURNS:       Register Identifier

*****************************************************************************/
RegIdT ValSndSrvOptConnRegister(ValEventFunc EventFuncP);

/*****************************************************************************
 
  FUNCTION NAME: ValSndSrvOptConnUnregister
  DESCRIPTION:   This routine unregisters the Srv Opt Connect callback 
  PARAMETERS:    RegId 
  RETURNS:       None.
 
*****************************************************************************/
void ValSndSrvOptConnUnregister(RegIdT RegId);

/*****************************************************************************

  FUNCTION NAME: ValSndPcmDetectRegister
  DESCRIPTION:   This routine is used to registers a callback to notify the 
                 caller when the PCM Detect event has happened. 
  PARAMETERS:    EventFuncP  - the function pointer
  RETURNS:       Register Identifier

*****************************************************************************/
RegIdT ValSndPcmDetectRegister(ValEventFunc EventFuncP);

/*****************************************************************************
 
  FUNCTION NAME: ValSndPcmDetectUnRegister
  DESCRIPTION:   This routine unregisters the PCM Detect event 
  PARAMETERS:    RegId 
  RETURNS:       None.
 
*****************************************************************************/
void ValSndPcmDetectUnRegister(RegIdT RegId);

/*****************************************************************************

  FUNCTION NAME: ValPcmProcessingEnable
  
  DESCRIPTION:   This routine is used to enable transfer of PCM frames from the
                 DSPv to CP for the CP to do extra speech enhancement processing.
                 
  PARAMETERS:    TaskId - Task ID requesting PCM frames for speech processing
                 MboxId - Mailbox ID to send PCM frames for speech processing
                 MsgId  - Message ID to use for sending PCM frames
                 
  RETURNS:       None

*****************************************************************************/
void ValPcmProcessingEnable (ExeTaskIdT TaskId, ExeMailboxIdT MboxId, uint32 MsgId);

/*****************************************************************************

  FUNCTION NAME: ValPcmProcessingDisable
  
  DESCRIPTION:   This routine disables the transfer of PCM frames from the
                 DSPv for CP speech enhancement processing.
                 
  PARAMETERS:    None
                 
  RETURNS:       None

*****************************************************************************/
void ValPcmProcessingDisable (void);

/*****************************************************************************

  FUNCTION NAME: ValPcmFrameInput
  
  DESCRIPTION:   This routine is used to process PCM Frames from the DSPv.
                 Processing involves sending the frame to the registered user to
                 perform speech enhancment processing.
                 
                 Once the speech processing is done, frames are sent back to DSPv.
                 
  PARAMETERS:    SpeechDataP - pointer to speech buffer to be processed
                 
  RETURNS:       TRUE if speech buffer processed, FALSE otherwise (PCM I/F disabled)

*****************************************************************************/
bool ValPcmFrameInput (uint16 *SpeechDataP);

/*****************************************************************************

  FUNCTION NAME: ValPcmFrameOutput
  
  DESCRIPTION:   This routine is used to take PCM Frames after CP's speech
                 enhancement processing and send them back to the DSPv.
                 
  PARAMETERS:    SpeechDataP - pointer to processed speech buffer
                 
  RETURNS:       None

*****************************************************************************/
void ValPcmFrameOutput (uint16 *SpeechDataP);

/*****************************************************************************

  FUNCTION NAME: ValPcmFrameOutputResp
  
  DESCRIPTION:   This routine is called when the DSPv sends an ACK in response
                 to a PCM output frame.
                 
                 PCM output frame flow control allows only 1 outstanding frame. 
                 
  PARAMETERS:    None
                 
  RETURNS:       None

*****************************************************************************/
void ValPcmFrameOutputResp (void);

/*****************************************************************************
 
   FUNCTION:      ValSndGetStatus
   DESCRIPTION:   returns current status
   PARAMETERS:    None
   RETURNS:       ValSoundFormatT

*****************************************************************************/
ValSoundFormatT ValSndGetStatus( void );

/*****************************************************************************
 
   FUNCTION:		ValSndIsMusicPlaying
   DESCRIPTION:	    Returns TRUE if music currently playing, FALSE otherwise
   PARAMETERS:		None
   RETURNS:		    TRUE/FALSE

*****************************************************************************/
bool ValSndIsMusicPlaying(void);

/*****************************************************************************
 
   FUNCTION:	  ValSoundSetDevice
   
   DESCRIPTION:	  Sets a new Device Type for a particular audio mode.
   
   PARAMETERS:	  DevMode - Mode for which device is being set (MUSIC, RINGER or VOICE)
                  Device  - Type of device being set
                    
   RETURNS:       bool - TRUE is successful, 
                         FALSE if audio device is invalid or not supported for the indicated mode,
                               or if audio mode is invalid.
   
                  NOTE:  
                  This function should be called once per DevMode
                  during UI initialization; later it can be called again
                  if the device for some mode got changed (e.g., user
                  inserts headset). It shouldn't me called every time
                  before playing MP3/AAC/Midi file or tone!

*****************************************************************************/
bool ValSoundSetDevice (ValSoundDeviceModesT DevMode, ValSoundDeviceT Device);

/*****************************************************************************
 
   FUNCTION:      ValAudioModeGet
   DESCRIPTION:   get a current Device
   PARAMETERS:    
   RETURNS:       ValSoundDeviceT

*****************************************************************************/
ValSoundDeviceT ValSoundDeviceGet(void);

/*****************************************************************************
 
	FUNCTION:		ValSndVibrateStart
    
	DESCRIPTION:	Begins vibration with the specified vibrate/rest times.
    
	PARAMETERS:     VibrateTime - Vibrate On time (msec) for each iteration
                    RestTime    - Vibrate Off time (msec) for each iteration
                    TotDuration - Total vibrate time (msec); 
                                  use VAL_VIBRATE_FOREVER to vibrate forever
	
    RETURNS:        None

*****************************************************************************/
void ValSndVibrateStart (uint32 VibrateTime, uint32 RestTime, int32 TotDuration);

/*****************************************************************************
 
	FUNCTION:		ValSndVibrateStop
    
	DESCRIPTION:	Stops vibration.
    
	PARAMETERS:     None
	
    RETURNS:        None

*****************************************************************************/
void ValSndVibrateStop (void);

/*****************************************************************************
 
	FUNCTION:		ValSndFlipCloseSilent
    
	DESCRIPTION:	Used when ending call by closing flip, and audio needs to
                    be silenced while call is disconnecting to prevent MIC/Spkr 
                    feedback with closed phone.
                    Must be called by UI after flip detect and before initiating
                    the call disconnect.
                    
	PARAMETERS:		None.
    
	RETURNS:		None.

*****************************************************************************/
void ValSndFlipCloseSilent (void);

/*****************************************************************************

   FUNCTION:      ValSndKeyProcess
   DESCRIPTION:   control function used to sound any DTMF or single tone key
                  when it's pressed or stop it if it's released
   PARAMETERS:    KpKeyIdT Key, 
                  bool IsPressed
   RETURNS:       None

*****************************************************************************/
void ValSndKeyProcess( SysKeyIdT Key, bool IsPressed );

/*****************************************************************************

   FUNCTION:      ValMusicInfoGet
   DESCRIPTION:   get input file's music infomation	
   PARAMETERS:    char *FileNameP
  			      ValMusicInfoT *MusicInfoP
   RETURNS:       VAL_APP_OK, VAL_APP_FAILED_ERROR

*****************************************************************************/
uint16 ValMusicInfoGet(char *FileNameP, ValMusicInfoT *MusicInfoP);

/*****************************************************************************

   FUNCTION:     ValMusicRecord

   DESCRIPTION:  Record MUSIC to a file.

   PARAMETERS:   FileNameP    - Filename ptr to record to
                 Format       - Music format to record, currently only WAVE is supported
                 SamplingRate - Sampling rate for recording
                 NumChan      - 1 chan for MONO, 2 chans for stereo

   RETURNS:  error code
                 VAL_APP_OK                    = successfully started recording
                 VAL_APP_WRONG_RECORD_FORMAT   = Record format not supported
                 VAP_APP_SAMPLING_RATE_INVALID = Invalid input sampling rate
                 VAL_APP_NUM_CHANS_INVALID     = Invalid # channels, must be 1 (mono) or 2 (stereo)
                 VAL_APP_CONFLICT              = Music or voice already active
                 VAL_APP_FILE_IO_ERROR         = Filename not provided or could not open/write to file
                 VAL_APP_FAILED_ERROR          = DSPv not compatible or ...
                 VAL_APP_MUSIC_NOT_SUPPORTED_ERR  = Both MUSIC and VMEMO must be enabled
                 VAL_APP_VMEMO_RECORD_DEVICE_FULL = No room on recording device
                 VAL_APP_MALLOC_ERROR             = Not enough memory to allocate working buffers

*****************************************************************************/
ValAppStatusT ValMusicRecord (char *FileNameP, ValSoundFormatT Format, ValSndSamplingRatesT SamplingRate, uint16 NumChan);

/*****************************************************************************
 
   FUNCTION:      PlayVibrate
   DESCRIPTION:	  begins vibration for certain duration and intensity
   PARAMETERS:
                  ValSoundVibrateT Vibrate, 
                  uint32 Duration, unit of millisecond
   RETURNS:       uint32 - return code

*****************************************************************************/
void ValSoundStartVibrate(ValSoundVibrateT Vibrate, uint32 Duration);

/*****************************************************************************

   FUNCTION:     ValSoundStopVibrate
   
   DESCRIPTION:	 Stops vibration.
   
   PARAMETERS:   None

   RETURNS:      None

*****************************************************************************/
void ValSoundStopVibrate(void);

/*****************************************************************************
 
   FUNCTION:		ValSndIsMusicPending
   DESCRIPTION:	    Returns TRUE if music currently Pending, FALSE otherwise
   PARAMETERS:		None
   RETURNS:		    TRUE/FALSE

*****************************************************************************/
bool ValSndIsMusicPending(void);

/*****************************************************************************

   FUNCTION:      ValMusicEQset
   DESCRIPTION:   set the current playing music EQ mode
   PARAMETERS:    ValMusicEQT EQMode
   RETURNS:       VAL_APP_OK, VAL_APP_FAILED_ERROR

*****************************************************************************/
uint16 ValMusicEQset(ValMusicEQT EQMode);

/*****************************************************************************

   FUNCTION:     ValSndApiEx_MusicPlayId_Create

   DESCRIPTION:  Create a MusicPlayId. 

   PARAMETERS:   None
                 
   RETURNS:      MusicPlayId

*****************************************************************************/
ValMusicPlayIdT ValSndApiEx_MusicPlayId_Create(void);

/*****************************************************************************

   FUNCTION:     ValSndApiEx_MusicPlayId_Retrieve

   DESCRIPTION:  Retrieve MusicPlayID from the queue. 

   PARAMETERS:   None
                 
   RETURNS:      MusicPlayId
                 VAL_SND_MUSIC_PLAYID_INVALID, if the queue is empty.

*****************************************************************************/
ValMusicPlayIdT ValSndApiEx_MusicPlayId_Retrieve( void );

/*****************************************************************************

   FUNCTION:     ValSndApiEx_MusicPlayId_Current

   DESCRIPTION:  get  the MusicPlayID by reading MusicPlayId_Queue head. 

   PARAMETERS:   None
                 
   RETURNS:      MusicPlayId
                 VAL_SND_MUSIC_PLAYID_INVALID, if the queue is empty.

*****************************************************************************/
ValMusicPlayIdT ValSndApiEx_MusicPlayId_Current( void );


/*****************************************************************************

   FUNCTION:      ValSndGetIteration
   DESCRIPTION:   Get Iterations of music
   PARAMETERS:    none
   RETURNS:       current Iteration

*****************************************************************************/
void ValSndPlaylistActive(bool Active);

/*****************************************************************************
 
   FUNCTION:      ValSndGetDevice
   DESCRIPTION:   get a Device from  DevMode
   PARAMETERS:    DevMode 
   RETURNS:       ValSoundDeviceT

*****************************************************************************/
ValSoundDeviceT ValSndGetDevice(ValSoundDeviceModesT DevMode);

/*****************************************************************************
 
	FUNCTION:		ValSndMicPcmDetectErr
    
	DESCRIPTION:	When MIC PCM Detect fault is reported by the DSPv,
                    this function passes it on to its registered users.
                    
	PARAMETERS:		None.
    
	RETURNS:		None.

*****************************************************************************/
void ValSndMicPcmDetectErr (void);

/*****************************************************************************

FUNCTION:		ValVoiceLoopBack
   
DESCRIPTION:	enable/disabele voice loopback function
                   
PARAMETERS:		Enable: TRUE - enable, FALSE -disable.
   
RETURNS:		None.

*****************************************************************************/
void ValVoiceLoopBack(bool Enable);

#ifdef  __cplusplus
}
#endif

#endif

/**Log information: \main\Trophy\Trophy_xding_href22331\1 2013-12-10 07:18:09 GMT xding
** HREF#22331, 合并MMC相关功能到Trophy baseline上**/
/**Log information: \main\Trophy\1 2013-12-10 08:33:48 GMT jzwang
** href#22331:Merge MMC latest implementation from Qilian branch.**/
