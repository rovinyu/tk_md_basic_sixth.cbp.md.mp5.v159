/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*******************************************************************************
 *
 * Filename:
 * ---------
 * hwdmipipublic.h
 *
 * Project:
 * --------
 * C2K
 *
 * Description:
 * ------------
 * Header file containing typedefs and definitions pertaining
 * to the MIPI driver.
 *
 * Author:
 * -------
 * 
 *
 *==============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 *
 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *==============================================================================
 *******************************************************************************/


#ifndef HWD_MIPI_PUBLIC_H
#define HWD_MIPI_PUBLIC_H

#ifdef __cpluscplus
extern "C" {
#endif

/*----------------------------------------------------------------------------
 Include Files
----------------------------------------------------------------------------*/
#include "hwdrfapi.h"
#include "dbmapi.h"
//#include "md3_kal_types.h"
#include "hwdnv.h"
#include "hwdmipidefs.h"
#ifdef __DYNAMIC_ANTENNA_TUNING__
#include "hwdtaspublic.h"
#endif
/*----------------------------------------------------------------------------
 Defines and Macros
----------------------------------------------------------------------------*/
#define USID_NULL                      0x0000
#define USID_REG_W                     0x0001
#define USID_REG_W_EXT                 0x0002

#define REG_0_W                   0x0001
#define REG_W                     0x0002
#define REG_W_EXT_1ST             0x0003
#define REG_W_EXT_BYTE            0x0004
#define REG_W_EXT_END             0x0005
#define IMM_BSI_WAIT              0x0006
#define REG_W_EXT_ONLY_ONEBYTE    0x0007

/* port select */
#define MIPI_PORT0                0x0000
#define MIPI_PORT1                0x0001
#define MIPI_PORT2                0x0002
#define MIPI_PORT3                0x0003
#define MIPI_PORT_MAX             0x0004

/* event type */
#define MIPI_TRX_ON         0x0001
#define MIPI_TRX_OFF        0x0002
#define MIPI_TPC_SET        0x0003 
#define MIPI_TX_GATE_ON     0x0004
#define MIPI_TX_GATE_OFF    0x0005
#define MIPI_RX_DIV_ON      0x0006
#define MIPI_RX_DIV_OFF     0x0007
#define MIPI_ETM_TX_ON      0x0008
#define MIPI_ETM_TX_OFF     0x0009
#define MIPI_ETM_TPC_SET    0x000A
#define MIPI_TPC_HPUE_SET   0x000B
#define MIPI_EVENT_NULL     0x0000

/* element type */
#define MIPI_NULL                 0x0000
#define MIPI_ASM                  0x0001
#define MIPI_ANT                  0x0002
#define MIPI_PA                   0x0003
#define MIPI_PA_SEC               0x0004
#define MIPI_DAT                  0x0005
#define MIPI_HPUE                 0x0005
#define MIPI_END_PATTERN          0xFFFF


#ifdef __DYNAMIC_ANTENNA_TUNING__
#define C2K_DAT_FE_CAT_MAX_NUM        (2)
#define C2K_MIPI_DAT_EVENT_NUM        (8)
#define C2K_MIPI_DAT_DATA_NUM         (20)
#define C2K_MIPI_DAT_ROUTE_A_NUM      (C2K_DAT_MAX_CAT_A_ROUTE_NUM)
#define C2K_MIPI_DAT_ROUTE_B_NUM      (C2K_DAT_MAX_CAT_B_ROUTE_NUM)
#define C2K_MIPI_DAT_PER_ROUTE_B_NUM   (C2K_DAT_CAT_B_ROUTE_NUM)
#define C2K_DAT_NO_SPLIT_BAND         (0xFF)
#endif

/* TPC PA SECTION DATA PATTERN */
#define MIPI_PA_SECTION_DATA0     0x0
#define MIPI_PA_SECTION_DATA1     0x1
#define MIPI_PA_SECTION_DATA2     0x2
#define MIPI_PA_SECTION_DATA3     0x3
#define MIPI_PA_SECTION_DATA4     0x4

/* Maximum numbers */
#define MIPI_SUPPORT_BAND_MAX_NUM    HWD_RF_BAND_MAX

/* Dummy timing transformation, used by PC tool to ease parsing */
#define M_US(uS)                (uS)

/* Dummy PA USID and register address */
#define MIPI_PA_SECTION_USID      0x0
#define MIPI_PA_SECTION_ADDRESS   0x0

/* Expand MIPI subband frequency to Hz */
#define M_100KHZ_TO_HZ(KhZ100)      (((uint32)(KhZ100)) * 100*1000)

/* kHz to Hz */
#define M_KHZ_TO_HZ(kHZ)          ((uint32)(kHZ) * 1000)

/*----------------------------------------------------------------------------
 Typedefs
----------------------------------------------------------------------------*/
typedef kal_uint8  USID_T;             //4 bits valid
typedef kal_uint8  PRODUCT_ID_T;       //8 bits valid
typedef kal_uint16 MANUFACTORY_ID_T;   //10 bits valid
typedef kal_uint8  MIPI_PORT_T;

typedef struct
{
   kal_uint16            mipi_addr;      // Port where data to send
   kal_uint32            mipi_data;      // mipi data
}MIPI_ADDR_DATA_T;                  // expanded by sub-freq

typedef struct
{
    kal_uint16            mipi_elm_type;  // mipi element type
    kal_uint16            mipi_port_sel;  // 0:for Port0, 1:for Port1 
    kal_uint16            mipi_data_seq;  // data write sequence format
    kal_uint16            mipi_usid;      // usid
    MIPI_ADDR_DATA_T      mipi_addr_data; // mipi address & data
    kal_uint32            mipi_wait_time; // mipi wait time
}MIPI_IMM_DATA_TABLE_T;

typedef struct
{
   kal_uint16            usid_procedure;
   MIPI_PORT_T           mipi_port_sel;
   USID_T                current_usid;
   PRODUCT_ID_T          product_id;
   MANUFACTORY_ID_T      manufactory_id;
   USID_T                new_usid;
}MIPI_USID_CHANGE_T;


typedef struct
{
   kal_uint16 mipi_data_st;//mipi data start index
   kal_uint16 mipi_data_sp;//mipi data stop index
}MIPI_DATA_STSP;

typedef struct
{
    kal_uint16            mipi_elm_type;  // mipi element type
    MIPI_DATA_STSP        mipi_data_stsp;
    kal_uint16            mipi_evt_type;     //event type
    kal_uint32            mipi_evt_offset;   //event offset
}MIPI_EVENT_TABLE_T;

typedef struct
{
   kal_uint16 addr;
   kal_uint32 data;
}MIPI_ADDR_DATA_EXPAND_TABLE_T;

typedef struct
{
   kal_uint16 mipi_subband_freq;
   MIPI_ADDR_DATA_EXPAND_TABLE_T mipi_data; // mipi data
}MIPI_DATA_EXPAND_TABLE_T;       //expanded by sub-freq

typedef struct
{
    kal_uint16 mipi_elm_type;                                     //mipi element type
    kal_uint16 mipi_port_sel;                                     //0:for Port0, 1:for Port1 
    kal_uint16 mipi_data_seq;                                     // data write sequence format
    kal_uint16 mipi_usid;                                         //mipi USID   
    MIPI_DATA_EXPAND_TABLE_T mipi_subband_data[MIPI_SUBBAND_NUM]; // mipi data
}MIPI_DATA_SUBBAND_TABLE_T;

typedef struct
{
   kal_uint16 mipi_elm_type;                                     //mipi element type
   kal_uint16 mipi_port_sel;                                     //0:for Port0, 1:for Port1 
   kal_uint16 mipi_data_seq;                                     // data write sequence format
   kal_uint16 mipi_usid;                                         //mipi USID      
   kal_uint16 addr;
   kal_uint32 data;
}MIPI_DATA_TABLE_T;

typedef struct
{
    MIPI_ADDR_DATA_EXPAND_TABLE_T mipi_event_data[MIPI_TPC_DATA_MAX_NUM];
}MIPI_TPC_SUBBAND_OCT_TABLE_T;

typedef struct 
{
    kal_uint16 mipi_subband_freq;
    kal_uint16 mipi_usid;
    MIPI_TPC_SUBBAND_OCT_TABLE_T mipi_pa_oct_table[MIPI_TPC_OCT_NUM];
}MIPI_TPC_SECTION_TABLE_T;


typedef struct
{
    bool mipiEnable;
}HwdMipiCustParamT;

typedef enum 
{
    MIPI_EXT_BYTE2 = 0, /* Byte 1 is included in the 1st CW */
    MIPI_EXT_BYTE3,
    MIPI_EXT_BYTE4,
    MIPI_EXT_LEFT_BYTE_MAX_NUM
} MipiExtByteIndexT;

#ifdef __DYNAMIC_ANTENNA_TUNING__
typedef struct
{
   MIPI_EVENT_TABLE_T mipiDatEvent[C2K_MIPI_DAT_EVENT_NUM];
}C2K_MIPI_DAT_Event_T;

typedef struct
{
   C2K_MIPI_DAT_Event_T mipiDatEventRoute[C2K_MIPI_DAT_ROUTE_A_NUM];
}C2K_MIPI_DAT_Event_CatA_T;

typedef struct
{
   C2K_MIPI_DAT_Event_T  mipiDatEventRoute[C2K_MIPI_DAT_ROUTE_B_NUM];
}C2K_MIPI_DAT_Event_CatB_T;

typedef struct
{
   MIPI_DATA_SUBBAND_TABLE_T mipiDatData[C2K_MIPI_DAT_DATA_NUM]; 
}C2K_MIPI_DAT_SB_DATA_T;

typedef struct
{
   C2K_MIPI_DAT_SB_DATA_T mipiDatDataRoute[C2K_MIPI_DAT_ROUTE_A_NUM];
}C2K_MIPI_DAT_Data_CatA_T;

typedef struct
{
   C2K_MIPI_DAT_SB_DATA_T mipiDatDataRoute0[C2K_MIPI_DAT_PER_ROUTE_B_NUM];   
   C2K_MIPI_DAT_SB_DATA_T mipiDatDataRoute1[C2K_MIPI_DAT_PER_ROUTE_B_NUM];
   C2K_MIPI_DAT_SB_DATA_T mipiDatDataRoute2[C2K_MIPI_DAT_PER_ROUTE_B_NUM];
   C2K_MIPI_DAT_SB_DATA_T mipiDatDataRoute3[C2K_MIPI_DAT_PER_ROUTE_B_NUM];
}C2K_MIPI_DAT_Data_CatB_T;

typedef struct 
{
   MIPI_EVENT_TABLE_T               dat_mipi_event[C2K_DAT_FE_CAT_MAX_NUM*C2K_MIPI_DAT_EVENT_NUM];
   MIPI_DATA_SUBBAND_TABLE_T        dat_mipi_data[C2K_DAT_FE_CAT_MAX_NUM*C2K_MIPI_DAT_DATA_NUM];
}C2K_DAT_MIPI_TABLE;  

#endif
/*----------------------------------------------------------------------------
 Global Variables Declaration
----------------------------------------------------------------------------*/
/* Custom file tables */
#include "hwdnvcustdefs.h"
HWD_MIPI_CUSTOM_DATA_DECLARE(SetDefault);

/* Input data tables */
extern HwdMipiCustParamT HwdMipiCustParamInput[];
extern MIPI_IMM_DATA_TABLE_T HwdMipiInitialCwInputTbl[];
extern MIPI_IMM_DATA_TABLE_T HwdMipiSleepCwInputTbl[];
extern MIPI_USID_CHANGE_T HwdMipiUsidChangeInputTbl[];
extern MIPI_EVENT_TABLE_T HwdMipiRxEventInputTbl[][MIPI_RX_EVENT_MAX_NUM];
extern MIPI_DATA_SUBBAND_TABLE_T HwdMipiRxDataInputTbl[][MIPI_RX_DATA_MAX_NUM];
extern MIPI_EVENT_TABLE_T HwdMipiTxEventInputTbl[][MIPI_TX_EVENT_MAX_NUM];
extern MIPI_DATA_SUBBAND_TABLE_T HwdMipiTxDataInputTbl[][MIPI_TX_DATA_MAX_NUM];
extern MIPI_EVENT_TABLE_T HwdMipiTpcEventInputTbl[][MIPI_TPC_EVENT_MAX_NUM];
extern MIPI_DATA_TABLE_T HwdMipiTpcDataInputTbl[][MIPI_TPC_DATA_MAX_NUM];
extern MIPI_TPC_SECTION_TABLE_T HwdMipiPaSectionDataInputTbl1xRtt[][MIPI_PA_SECTION_DATA_MAX_NUM];
extern MIPI_TPC_SECTION_TABLE_T HwdMipiPaSectionDataInputTblEvdo[][MIPI_PA_SECTION_DATA_MAX_NUM];

HWD_ETM_CUSTOM_DATA_DECLARE(SetDefault);
extern MIPI_EVENT_TABLE_T HwdEtmTxEventInputTbl[][MIPI_ETM_TX_EVENT_MAX_NUM];
extern MIPI_DATA_SUBBAND_TABLE_T  HwdEtmTxDataInputTbl[][MIPI_ETM_TX_DATA_MAX_NUM];
extern MIPI_EVENT_TABLE_T HwdEtmTpcEventInputTbl[][MIPI_ETM_TPC_EVENT_MAX_NUM];
extern MIPI_DATA_TABLE_T  HwdEtmTpcDataInputTbl[][MIPI_ETM_TPC_DATA_MAX_NUM];
extern MIPI_TPC_SECTION_TABLE_T  HwdEtmPaSectionData1xttInputTbl[][MIPI_PA_SECTION_DATA_MAX_NUM];
extern MIPI_TPC_SECTION_TABLE_T  HwdEtmPaSectionDataEvdoInputTbl[][MIPI_PA_SECTION_DATA_MAX_NUM];

#ifdef __DYNAMIC_ANTENNA_TUNING__
HWD_DAT_CUSTOM_DATA_DECLARE(SetDefault) ;
extern C2K_CUSTOM_DAT_FE_ROUTE_DATABASE_T hwdRfCustomDatRouteDataBaseInput;
extern C2K_CUSTOM_DAT_FE_DATABASE_T hwdRfCustomDatBpiDataBaseInput;

extern C2K_MIPI_DAT_Data_CatB_T hwdRfCustomDatCatBMipiDataTableInput;
extern C2K_MIPI_DAT_Data_CatA_T hwdRfCustomDatCatAMipiDataTableInput;
extern C2K_MIPI_DAT_Event_CatB_T  hwdRfCustomDatCatBMipiEventTableInput;
extern C2K_MIPI_DAT_Event_CatA_T  hwdRfCustomDatCatAMipiEventTableInput;

extern HwdRfApSensorRelativeIndex HwdRfDatRelativeIndex ;
extern C2K_DAT_MIPI_TABLE	hwdRfCustomDatMipiData[C2K_DAT_MAX_FE_ROUTE_NUM][C2K_DAT_MAX_STATE_NUM];
extern HwdCustomDatBpiSetting hwdRfCustomDatBpiData[C2K_DAT_MAX_FE_ROUTE_NUM][C2K_DAT_MAX_STATE_NUM];

extern HwdRfApSensorRelativeIndex HwdRfSar1xRelativeIndex ;
extern HwdRfApSensorRelativeIndex HwdRfSarDoRelativeIndex ;
#endif
/*----------------------------------------------------------------------------
 Glocal Function Declaration
----------------------------------------------------------------------------*/
extern void HwdMipiGeneralProcess(void *MsgDataPtr);
#ifdef __DYNAMIC_ANTENNA_TUNING__
extern void HwdMipiDatGeneralProcess(void *MsgDataPtr);
#endif
extern void HwdMipiInitData(const HwdDbmSegProcessInfoT * procPtr);
extern bool HwdMipiCustEnable(void);
extern uint16 HwdMipiGetSubband(uint32 freqHz, uint32 *subbandFreqHz);
extern bool HwdMipiCheckSubband(uint32 *subbandFreqHz);

#ifdef __cpluscplus
}
#endif

#endif /* HWD_MIPI_PUBLIC_H */


