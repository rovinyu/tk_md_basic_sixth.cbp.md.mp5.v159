/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2004-2013 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
/********************************************************************************************
* 
* FILE NAME   : valat.h
*
* DESCRIPTION : Prototypes and definitions for VAL AT.
*
********************************************************************************************/
#ifndef  _VALAT_H_
#define _VALAT_H_

#include "sysdefs.h"
#include "valdbmapi.h"
#include "valuimapi.h"
#ifdef MTK_DEV_VERSION_CONTROL
#include "valattune.h"
#endif

/*-----------------------------------------------------------------
 *   VAL AT definitions
 *----------------------------------------------------------------*/
#define ATC_MAX_ATPARMS          15
#define ATC_MAX_NUM_LINE_IN_RSP  15
#define ATC_MAX_COMMAND_BODY_LEN 16

#ifdef MTK_DEV_VERSION_CONTROL
#define DS_DATA_BASE_MASK       0x01
#define DS_CUST_DATA_BASE_MASK  0x02

#define DEFAULT_FCC_VR  0
#define DEFAULT_FCC_BR  0
#define DEFAULT_FCC_WD  0
#define DEFAULT_FCC_LN  0
#define DEFAULT_FCC_DF  0
#define DEFAULT_FCC_EC  0
#define DEFAULT_FCC_BF  0
#define DEFAULT_FCC_ST  0

#define DEFAULT_FCR     0

#define DEFAULT_FFC_VRC 0
#define DEFAULT_FFC_DFC 0
#define DEFAULT_FFC_LNC 0
#define DEFAULT_FFC_WDC 0

#define DEFAULT_FRQ_PGL 0
#define DEFAULT_FRQ_CBL 0

#define DEFAULT_MS_CARRIER  CR_V34
#define DEFAULT_MS_AUTOMODE FALSE

#if (AT_DPD_AUTOBAUD_RATES == BR_NONE)
  #ifndef AT_DEFAULT_IPR
  #error "AT_DEFAULT_IPR not defined"
  #endif

  #if ((AT_DEFAULT_IPR == 0) || ((AT_DEFAULT_IPR & AT_DPD_FIXED_RATES) == 0))
  #error "AT_DEFAULT_IPR definition error"
  #endif

  #if (AT_DEFAULT_IPR == BR_300)
  #define IPR_DEFAULT 300
  #elif (AT_DEFAULT_IPR == BR_1200)
  #define IPR_DEFAULT 1200
  #elif (AT_DEFAULT_IPR == BR_2400)
  #define IPR_DEFAULT 2400
  #elif (AT_DEFAULT_IPR == BR_4800)
  #define IPR_DEFAULT 4800
  #elif (AT_DEFAULT_IPR == BR_9600)
  #define IPR_DEFAULT 9600
  #elif (AT_DEFAULT_IPR == BR_19200)
  #define IPR_DEFAULT 19200
  #elif (AT_DEFAULT_IPR == BR_38400)
  #define IPR_DEFAULT 38400
  #elif (AT_DEFAULT_IPR == BR_57600)
  #define IPR_DEFAULT 57600
  #elif (AT_DEFAULT_IPR == BR_115200)
  #define IPR_DEFAULT 115200UL
  #elif (AT_DEFAULT_IPR == BR_230400)
  #define IPR_DEFAULT 230400UL
  #else
  #error "AT_DEFAULT_IPR definition error"
  #endif

#else /* (AT_DPD_AUTOBAUD_RATES != BR_NONE) */
#define IPR_DEFAULT 0
#endif
#endif

#define ATC_MAX_CFG_STR_SIZE	 249 /* MAX_CFG_LEN in Ai_data.h */
#define ATC_MAX_FDL_STR_SIZE     95  /* Sect 4.4.3 IS-707-A.3 */
#define ATC_MAX_GCAP_STR_SIZE    64  /* Ai_cmd is using 65535 bytes!! */
#define ATC_MAX_FLI_STR_SIZE	 21  /* MAX_FLI_LEN in Ai_data.h */ 
#define ATC_MAX_FPA_STR_SIZE	 21  /* MAX_FPA_LEN in Ai_data.h */
#define ATC_MAX_FPI_STR_SIZE	 21  /* MAX_FPI_LEN in Ai_data.h */
#define ATC_MAX_FPW_STR_SIZE	 21  /* MAX_FPW_LEN in Ai_data.h */
#define ATC_MAX_FSA_STR_SIZE	 21  /* MAX_FSA_LEN in Ai_data.h */
#define ATC_MAX_GMI_STR_SIZE	 64  /* Sect 4.1.1 IS-131 says 2048 */
#define ATC_MAX_GMM_STR_SIZE	 64  /* Sect 4.1.2 IS-131 says 2048 */
#define ATC_MAX_GMR_STR_SIZE	 64  /* Sect 4.1.3 IS-131 says 2048 */
#define ATC_MAX_HWV_STR_SIZE	 64
#define ATC_MAX_GOI_STR_SIZE	 64  /* Sect 4.1.4 IS-131 says 2048 */
#define ATC_MAX_GSN_STR_SIZE	 64  /* Sect 4.1.5 IS-131 says 2048 */

#define ATC_MAX_FIF_LEN          90

#ifdef MTK_CBP
#define ATC_MAX_REG_TYPE_NUM        7
#endif


typedef enum 
{
  ATC_ABC_Unknown,
  ATC_ABC_800MHz,
  ATC_ABC_1900MHz,
  ATC_NUM_ABCs
} AtcBandClass;

typedef struct 
{
  char*  data;
  uint16 len;
} AtcDataParse;

typedef struct
{
   uint32        num32;
   char*         ptrCharStr;
   AtcDataParse	 buf;
   uint32        ipAddr;
#ifdef MTK_CBP //MTK_DEV_C2K_IRAT
   bool          entered;
#endif      
} AtcParmT;

typedef PACKED_PREFIX struct
{
   char*          ParmLine[ATC_MAX_NUM_LINE_IN_RSP];  
} PACKED_POSTFIX  AtcParmArrayT;

typedef PACKED_PREFIX struct
{
   char           cmdName[16];
   AtcParmT*      ParmList[ATC_MAX_ATPARMS];
   uint8          chan;
} AtcSendAtMsgT;

typedef PACKED_PREFIX struct
{
    char            cmdName[ATC_MAX_COMMAND_BODY_LEN];
    AtcParmArrayT   LinesOfParms;
    uint8           numOfValidLines;
    bool            rspStatus;
    bool            needBufferAck;
    bool            skipResultCode;
    char*           ErrResultNum;
    uint8           chan;	 
    uint16          LineLen[ATC_MAX_NUM_LINE_IN_RSP];
#ifdef MTK_CBP
    bool            isUrc;
#endif
} PACKED_POSTFIX  AtcSendAtRespMsgT;

#ifdef SYS_OPTION_HL
typedef NV_PACKED_PREFIX struct
{
   uint8              StrChar[ATC_MAX_CFG_STR_SIZE];  
} NV_PACKED_POSTFIX  AtcDbmCfgStrT;

typedef NV_PACKED_PREFIX struct
{
   uint8              StrChar[ATC_MAX_FDL_STR_SIZE];
} NV_PACKED_POSTFIX  AtcDbmFdlStrT;

typedef NV_PACKED_PREFIX struct 
{
   uint8              StrChar[ATC_MAX_FLI_STR_SIZE];
} NV_PACKED_POSTFIX  AtcDbmFliStrT;

typedef NV_PACKED_PREFIX struct 
{
   uint8              StrChar[ATC_MAX_FPA_STR_SIZE];
} NV_PACKED_POSTFIX  AtcDbmFpaStrT;

typedef NV_PACKED_PREFIX struct 
{
   uint8              StrChar[ATC_MAX_FPI_STR_SIZE];
} NV_PACKED_POSTFIX  AtcDbmFpiStrT;

typedef NV_PACKED_PREFIX struct 
{
   uint8              StrChar[ATC_MAX_FPW_STR_SIZE];
} NV_PACKED_POSTFIX  AtcDbmFpwStrT;

typedef NV_PACKED_PREFIX struct 
{
   uint8              StrChar[ATC_MAX_FSA_STR_SIZE];
} NV_PACKED_POSTFIX  AtcDbmFsaStrT;

typedef NV_PACKED_PREFIX struct 
{
   uint8              StrChar[ATC_MAX_GCAP_STR_SIZE];
} NV_PACKED_POSTFIX  AtcDbmGcapStrT;

typedef NV_PACKED_PREFIX struct 
{
   uint8              StrChar[ATC_MAX_GMI_STR_SIZE];
} NV_PACKED_POSTFIX  AtcDbmGmiStrT;

typedef NV_PACKED_PREFIX struct 
{
   uint8              StrChar[ATC_MAX_GMM_STR_SIZE];
} NV_PACKED_POSTFIX  AtcDbmGmmStrT;

typedef NV_PACKED_PREFIX struct 
{
   uint8              StrChar[ATC_MAX_GMR_STR_SIZE];
} NV_PACKED_POSTFIX  AtcDbmGmrStrT;

typedef NV_PACKED_PREFIX struct 
{
   uint8              StrChar[ATC_MAX_HWV_STR_SIZE];
} NV_PACKED_POSTFIX  AtcDbmHwvStrT;

typedef NV_PACKED_PREFIX struct 
{
   uint8              StrChar[ATC_MAX_GOI_STR_SIZE];
} NV_PACKED_POSTFIX  AtcDbmGoiStrT;

typedef NV_PACKED_PREFIX struct 
{
   uint8              StrChar[ATC_MAX_GSN_STR_SIZE];
} NV_PACKED_POSTFIX  AtcDbmGsnStrT;

typedef NV_PACKED_PREFIX struct
{
   uint8             Carrier[8];
} NV_PACKED_POSTFIX  AtcDbmMaT;

typedef NV_PACKED_PREFIX struct
{
  uint8 bcs;
  uint8 bcl;
} NV_PACKED_POSTFIX  AtcCbc;

typedef NV_PACKED_PREFIX struct
{
  uint8  direction;
  bool   compressionNegotiation;
  uint16 maxDict;
  uint8  maxString;
} NV_PACKED_POSTFIX  AtcDs;

typedef NV_PACKED_PREFIX struct
{
  uint8 sqm;
  uint8 fer;
} NV_PACKED_POSTFIX  AtcCsq;

typedef NV_PACKED_PREFIX struct
{
  AtcBandClass bandClass;
  char band;
  uint16 sid;
} NV_PACKED_POSTFIX  AtcCss;

typedef NV_PACKED_PREFIX struct
{
  uint8 breakSelection;
  bool timed;
  uint8 defaultLen;
} NV_PACKED_POSTFIX  AtcEb;

typedef NV_PACKED_PREFIX struct
{
  uint8 origRqst;
  uint8 origFbk;
  uint8 ansFbk;
} NV_PACKED_POSTFIX  AtcEs;

typedef NV_PACKED_PREFIX struct
{
  uint8 pendingTd;
  uint8 pendingRd;
  uint8 timer;
} NV_PACKED_POSTFIX  AtcEtbm;

typedef NV_PACKED_PREFIX struct
{
  bool sub;
  bool sep;
  bool pwd;
} NV_PACKED_POSTFIX  AtcFap;

typedef NV_PACKED_PREFIX struct
{
  bool vr;
  uint8 br;
  uint8 wd;
  uint8 ln;
  uint8 df;
  bool ec;
  bool bf;
  uint8 st;
} NV_PACKED_POSTFIX  AtcFcc;

typedef NV_PACKED_PREFIX struct
{
  uint8 rq;
  uint8 tq;
} NV_PACKED_POSTFIX  AtcFcq;

typedef NV_PACKED_PREFIX struct
{
  uint8 vrc;
  uint8 dfc;
  uint8 lnc;
  uint8 wdc;
} NV_PACKED_POSTFIX  AtcFfc;

typedef NV_PACKED_PREFIX struct
{
  bool rpr;
  bool tpr;
  bool idr;
  bool nsr;
} NV_PACKED_POSTFIX  AtcFnr;

typedef NV_PACKED_PREFIX struct
{
  uint8 buf[ATC_MAX_FIF_LEN];
  uint8 len;
} NV_PACKED_POSTFIX  AtcFif;

typedef NV_PACKED_PREFIX struct
{
  uint8 pgl;
  uint8 cbl;
} NV_PACKED_POSTFIX  AtcFrq;

typedef NV_PACKED_PREFIX struct
{
  uint8 format;
  uint8 parity;
} NV_PACKED_POSTFIX  AtcIcf;

typedef NV_PACKED_PREFIX struct
{
  uint8 dceByDte;
  uint8 dteByDce;
} NV_PACKED_POSTFIX  AtcIfc;

typedef NV_PACKED_PREFIX struct
{
  uint8 carrier;
  bool automode;
  uint16 minRate;
  uint16 maxRate;
  uint16 minRxRate;
  uint16 maxRxRate;
} NV_PACKED_POSTFIX  AtcMs;

typedef NV_PACKED_PREFIX struct
{
  uint8 mode;
  uint8 dfltAnsMode;
  bool fbkTimeEnable;
} NV_PACKED_POSTFIX  AtcMv18s;

typedef NV_PACKED_PREFIX struct 
{
    uint32          ValActivedBandMask;
} NV_PACKED_POSTFIX  AtcValActivedBandDataT;

#ifdef MTK_CBP
typedef NV_PACKED_PREFIX struct 
{
  bool  regTypeArray[ATC_MAX_REG_TYPE_NUM];
} NV_PACKED_POSTFIX  AtcValRegtypeDataT;

#endif

typedef NV_PACKED_PREFIX struct  
{
   bool             ParamSetFlag;
   uint8            C109Parameter;
   uint8            CADParameter;
   AtcCbc           CBCParameter;
   uint32 	        CBIPParameter;
   bool             CDRParameter;
   AtcDs            CDSParameter;
   uint8            CFCParameter;
   uint32           CMIPParameter;
   uint8            CMUXParameter;
   bool             CPERParameter;
   uint16           CPSParameter;
   bool             CPSRParameter;
   uint8            CQDParameter;
   bool             CRCParameter;
   uint8            CRMParameter;
   AtcCsq           CSQParameter;
   AtcCss           CSSParameter;
   uint8            CTAParameter;
   bool             CXTParameter;
   uint8            C108Parameter;
   uint8            DParameter;
   bool             DialTypeParameter;
   bool             DRParameter;
   AtcDs            DSParameter;
   bool             EParameter[AT_CHAN_NUM];
   AtcEb            EBParameter;
   uint8            EFCSParameter;
   bool             ERParameter;
   AtcEs            ESParameter;
   uint8            ESRParameter;
   AtcEtbm          ETBMParameter;
   bool             FAAParameter;
   AtcFap           FAPParameter;
   uint8            FBOParameter;
   bool             FBUParameter;
   AtcFcc           FCCParameter;
   uint8            FCLASSParameter;
   AtcFcq           FCQParameter;
   bool             FCRParameter;
   uint8            FCTParameter;
   bool             FEAParameter;
   AtcFfc           FFCParameter;
   uint8            FHSParameter;
   bool             FIEParameter;
   AtcFcc           FISParameter;
   uint8            FLOParameter;
   bool             FLPParameter;
   uint8            FMSParameter;
   AtcFnr           FNRParameter;
   AtcFif           FNSParameter;
   bool             FPPParameter;
   uint8            FPRParameter;
   uint8            FPSParameter;
   AtcFrq           FRQParameter;
   uint8            FRYParameter;
   bool             FSPParameter;
   bool             IBCParameter;
   AtcIcf           ICFParameter;
   AtcIfc           IFCParameter;
   bool             ILRRParameter;
   uint32           IPRParameter;
   uint8            LParameter;
   uint8            MParameter;
   bool             MRParameter;
   AtcMs            MSParameter;
   bool             MV18RParameter;
   AtcMv18s         MV18SParameter;
   bool             QParameter[AT_CHAN_NUM];
   uint8            S0Parameter;
   uint8            S3Parameter;
   uint8            S4Parameter;
   uint8            S5Parameter;
   uint8            S6Parameter;
   uint8            S7Parameter;
   uint8            S8Parameter;
   uint8            S9Parameter;
   uint8            S10Parameter;
   uint8            S11Parameter;
   bool             VParameter[AT_CHAN_NUM];
   uint8            XParameter;  
    uint8 	        DMUVParameter;
#ifndef MTK_DEV_VERSION_CONTROL
    uint8	        QCMIPParameter;
#endif
    bool            QCQNCParameter;
    uint8           QCMDRParameter;

    AtcDbmCfgStrT   CfgStr;
    AtcDbmFdlStrT	FdlStr;
    AtcDbmFliStrT	FliStr;
    AtcDbmFpaStrT	FpaStr;
    AtcDbmFpiStrT	FpiStr;
    AtcDbmFpwStrT	FpwStr;
    AtcDbmFsaStrT	FsaStr;
    AtcDbmGcapStrT	GcapStr;
    AtcDbmGmiStrT	GmiStr;
    AtcDbmGmmStrT	GmmStr;
    AtcDbmGmrStrT	GmrStr;
    AtcDbmGoiStrT	GoiStr;
    AtcDbmGsnStrT	GsnStr;
    AtcDbmMaT       MaTbl;
    AtcDbmHwvStrT	HwvStr;
    uint8           CMEEParameter;
#ifdef MTK_DEV_ENGINEER_MODE
    uint8           EctmMode;
#ifdef MTK_DEV_C2K_IRAT
    uint8           EclscMode;
#endif
#endif
#ifdef MTK_CBP
    bool            VPMode;  /* voice privacy mode */

    AtcValActivedBandDataT ActivedBand;

    AtcValRegtypeDataT     stRegTypeTbl;    /*saved value, if enable the registration type 
                                                                  RegTypeArray[0], timer based
                                                                  RegTypeArray[1], Power up
                                                                  RegTypeArray[2], Zoned based
                                                                  RegTypeArray[3], Power down
                                                                  RegTypeArray[4], Parameter change
                                                                  RegTypeArray[5], order
                                                                  RegTypeArray[6], distanc based
                                                                  TRUE,enalbe the reg type*/

    uint16          uPrefSo;
    uint8           uPrefRc;
    bool            bEvrcCap;
    bool            bEvrcbSupport;       /*True,the EvrcB(SO68) is enable, False, it is disable*/
    bool            bEvrcSupport;        /*True,the EVRC(SO3)  is enable, False, it is disable*/
	/*for mode switch optimization*/
	uint8           PrefMode;
#ifdef MTK_CBP_ENCRYPT_VOICE    
    bool            bCVCEnable;          /*True, Cipher Voice Call is enable,False, disable;*/
#endif
#endif




} NV_PACKED_POSTFIX  AtcDbmBinDataT;

#ifdef MTK_DEV_VERSION_CONTROL
typedef NV_PACKED_PREFIX struct  
{
    uint8           QCMIPParameter;
} NV_PACKED_POSTFIX  AtcCustDbmBinDataT;
#endif
#endif

/*-----------------------------------------------------------------
 *   VAL AT global variables 
 *----------------------------------------------------------------*/
extern AtcDbmBinDataT       AtcNvmData;
#ifdef MTK_DEV_VERSION_CONTROL
extern AtcCustDbmBinDataT   AtcCustNvmData;
extern uint8 AtcNvmDataReadStatus;
extern uint8 AtcNvmDataWriteStatus;
#endif

#endif
  /* _VALAT_H_ */
