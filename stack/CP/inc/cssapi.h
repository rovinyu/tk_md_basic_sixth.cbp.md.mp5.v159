/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.
*
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
*
* Copyright (c) 2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef _CSS_API_H_

#define _CSS_API_H_  1
#include "sysdefs.h"
#include "exeapi.h"
#include "cssdefs.h"
#ifdef MTK_DEV_C2K_IRAT
#include "hlpapi.h"
#include "do_ims.h"
#include "../val/valiratapi.h"
#include "iratapi.h"
#endif /* MTK_DEV_C2K_IRAT */

#define CSS_SWAP(A, B, TMP)  {(TMP) = (A); (A) = (B); (B) = (TMP);}

/*----------------------------------------------------------------------------
* EXE Interfaces - Definition of Signals and Mailboxes
*----------------------------------------------------------------------------*/
#define CSS_TASK_MAILBOX         EXE_MAILBOX_1_ID
#define CSS_TASK_MAILBOX2        EXE_MAILBOX_2_ID

#define CSS_CPSM_1X_SCAN_TIMEOUT EXE_SIGNAL_1
#define CSS_UIM_NAM_GET_TIMEOUT  EXE_SIGNAL_2

#define HANG_ON_TIME           30000L  /* 30   seconds */

/* Subnet field length, in the unit of byte*/
#define SUBNET_LENGTH          16

#define CP_HRPD_MAX_NUM_REDIRECT_CHANNELS  10 /* can be more, just taken for now */

#define IRAT_MSG_START         100

#ifdef MTK_DEV_C2K_IRAT
#define PDN_ADD_LEN                 5
#define INTERN_SIM_INDEX_OFFSET     0xA0
#endif /* MTK_DEV_C2K_IRAT */

#define MAX_MANUAL_AVOID_1XSYS          6
#define MAX_ERI_HOME_VALUES             12
#define MAX_ERI_INTERNATIONAL_VALUES    30
#ifdef MTK_DEV_C2K_IRAT
#define HYBRID_MODE_CHG_1X_ENABLE_MASK     0x01
#define HYBRID_MODE_CHG_1X_DISABLE_MASK    0x02
#define HYBRID_MODE_CHG_DO_ENABLE_MASK     0x04
#define HYBRID_MODE_CHG_DO_DISABLE_MASK    0x08
#define HYBRID_MODE_CHG_REQ_START_MASK     0x80
#endif

/*----------------------------------------------------------------------------
* Message IDs for signals and commands sent to L1D
*----------------------------------------------------------------------------*/

typedef enum  /*_Message_ID_define*/
{
    /* 1X Messages */
    CSS_1X_SELECT_REQ_MSG,           /* 0 */
    CSS_1X_VALIDATE_REQ_MSG,
    CSS_1X_OOSA_WAKEUP_IND_MSG,
    CSS_1X_ACQ_ABORT_RSP_MSG,
    CSS_1XDO_HANDOFF_IND_MSG,
    CSS_1X_REG_ACCEPTED_MSG,
    CSS_1X_E911MODE_SET_MSG,
    CSS_1X_NAM_UPDATED_MSG,
    CSS_1X_UPDATE_ACTIVE_NAM_MSG,
    CSS_1X_SET_RETURN_CHANNEL_MSG,
    CSS_1X_SET_CALL_PENDING_MSG,
    CSS_1X_SYS_PARMS_UPDATE_MSG,
    CSS_1X_REPORT_CP_EVENT_MSG,
    CSS_1X_CLEAR_MRU_MSG,
    CSS_1X_SAVE_LAST_SYS_IN_MRU_MSG,
    CSS_1X_SAVE_CHANNEL_MSG,
    CSS_1X_MPSS_TIMEOUT_MSG,        /* 16 */

    /* DO Messages*/
    CSS_DO_SELECT_REQ_MSG,          /* 17 */
    CSS_DO_VALIDATE_REQ_MSG,
    CSS_DO_REDIRECT_REQ_MSG,
    CSS_DO_OOSA_WAKEUP_IND_MSG,
    CSS_DO_CONN_START_IND_MSG,
    CSS_DO_CONN_RELEASE_IND_MSG,
    CSS_DO_ACQ_ABORT_RSP_MSG,
    CSS_DO1X_HANDOFF_IND_MSG,
    CSS_DO_MPSS_TIMEOUT_MSG,
    CSS_DO_REDIRECT_CHANNEL_LIST_MSG,
    CSS_DO_NTWK_ACQD_MSG,
    CSS_DO_CHAN_CHANGED_IND_MSG,
    CSS_DO_SESSION_OPENED_MSG,
    CSS_DO_SESSION_CLOSED_MSG,
    CSS_DO_CLEAR_MRU_MSG,
    CSS_DO_REPORT_CP_EVENT_MSG,
    CSS_CP_POWER_CTRL_MSG,
    CSS_HYBRID_MODE_SET_MSG,
    CSS_PERIODICAL_SEARCH_CYCLE_SET_MSG,
    CSS_DO_DEFAULT_BAND_SET_MSG,
    CSS_DO_ACM_APERSIS_IND_MSG,
    CSS_DO_AUTH_FAIL_IND,          /* 38 */

    /* Misc Messages */
    CSS_UIM_GET_PRL_RSP_MSG,       /* 39 */
    CSS_UIM_STATUS_NOTIFY_MSG,
    CSS_UIM_GET_NAM_DATA_MSG,
#ifndef MTK_CBP
    CSS_UIM_PRL_LENGTH_MSG,        /* 42 */

    CSS_SVC_STATUS_REQ_MSG,        /* 43 */
#else
    CSS_SVC_STATUS_REQ_MSG = 43,        /* 43 */
#endif
    CSS_FORCE_DOSCAN_REQ_MSG,
    CSS_OOSA_SET_PARMS_MSG,
    CSS_OOSA_GET_PARMS_MSG,
    CSS_OOSA_SET_ENABLE_MSG,
    CSS_OOSA_GET_ENABLE_MSG,
    CSS_OOSA_SET_NUM_PHASES_MSG,
    CSS_OOSA_GET_NUM_PHASES_MSG,
    CSS_OOSA_SET_CURRENT_STAGE_MSG,
    CSS_OOSA_GET_CURRENT_STAGE_MSG,  /* 52 */

    /* ETS Messages Handler */
    CSS_TEST_CFG_MSG,               /* 53 */
    CSS_CLEAR_MRU_MSG,
    CSS_GET_PRL_ID_MSG,
    CSS_GET_ERI_VERSION_MSG,        /* 56 */

    /* MailBox2 Nessages */
    CSS_DBM_GET_NAM_RSP_MSG,        /* 57 */
    CSS_DBM_GET_PRL_RSP_MSG,
    CSS_PRL_UPDATED_MSG,
    CSS_MMSS_FILE_UPDATED_MSG,      /*60*/
    CSS_DBM_MRU_WRITE_ACK_MSG,
    CSS_DBM_MRU_GET_RSP_MSG,
    CSS_DBM_DO_MRU_WRITE_ACK_MSG,
    CSS_DBM_DO_MRU_GET_RSP_MSG,
    CSS_DBM_DATA_ERI_TABLE_MSG,
    CSS_DBM_GET_MSCAPDB_RSP_MSG,    /* 66 */

    CSS_TEST_MANUAL_BAND_CHNL_MSG,  /* 67 */
    CSS_MARK_CURRENT_1X_SYSTEM_AS_NEGATIVE_MSG,
    CSS_1X_REDIR_IND_MSG,
    CSS_1X_OVERHEAD_CURRENT_MSG,
    CSS_TIMER_EXPIRED_MSG,
    CSS_UPDATE_SYS_SELECT_RSP_MSG,
    IRAT_L1D_TX_PWR_RSP,
    CSS_GET_MCC_MSG,
    CSS_DO_ACCESS_STATUS_IND,
    CSS_DO_ERR_IND,
    CSS_DATA_TCH_SETUP_STATUS_MSG,
    CSS_PSW_1X_REG_STATUS_IND,
    CSS_PSW_1X_DO_OVERLAP_IND,
    CSS_HLP_SESS_VERIF_FAIL_IND,
    CSS_1X_UPDATE_LAST_SAVED_ECIO_RXPWR_MSG,
    CSS_DBM_GET_1X_DATA_RSP_MSG,
    CSS_DBM_1X_DATA_WRITE_ACK_MSG,
    CSS_DBM_GET_DO_DATA_RSP_MSG,
    CSS_DBM_DO_DATA_WRITE_ACK_MSG,
    CSS_DBM_GET_MISC_DATA_RSP_MSG,
    CSS_DBM_MISC_DATA_WRITE_ACK_MSG,
    CSS_DBM_INIT_NVRAM_TO_DEFAULT_MSG, /* 88 */

    CSS_OOSA_SCANNING_TIMER_SET_MSG = 95,
    CSS_OOSA_SCANNING_TIMER_GET_MSG,

    /* Generic CSS Set/Get PARM interface */
    CSS_SET_PARM_MSG = 98,
    CSS_GET_PARM_MSG = 99,

    /* IRAT message */
    CSS_IRAT_MSG = IRAT_MSG_START,   /* 100 */

    /* HLP to IRAT */
    CSS_HLP_NETWORK_CONN_RSP,
    CSS_HLP_EPS_ACTIVATE_RSP,
    CSS_HLP_NETWORK_REL_RSP,
    CSS_HLP_NETWK_REL_IND,
    CSS_HLP_EPS_BEARER_SETUP_RSP,
    CSS_HLP_EPS_BEARER_DISCONN_RSP,
    CSS_HLP_EPS_BEARER_DISCONN_IND,
    CSS_HLP_EPS_BEARER_SETUP_IND,
    CSS_HLP_EPS_DEFAULT_BEARER_DISCONN_IND,
    CSS_HLP_PPP_OPEN_REQ_RCVD_IND, /*110*/

    /* IDP to IRAT */
    IRAT_IDP_HANDOFF_RSP,    /*111*/
    /* RUP to IRAT */
    IRAT_RUP_MEAS_REPORT,
    IRAT_RUP_CUR_SYS_MEAS_RSP,
    IRAT_RUP_CUR_SYS_WEAK_IND,

    /* msg between CSS and VAL, ValApi_Set_PS_Connection_Rsp */
    CSS_VAL_PPP_CONN_STATUS_IND,

    /* msg between CSS and VAL, ValApi_Irat_Cdma_DataState_Ind */
    CSS_VAL_DATA_STATE_IND,

    /* OMP to IRAT */
    IRAT_OMP_RAT_UPDATED_IND,
    IRAT_OMP_RAT_INFO_IND,
    IRAT_OMP_MCC_RSP,
    IRAT_OMP_MCC_CHANGE_IND,  /*120*/

    /* RTM to IRAT */
    IRAT_RCP_TX_PWR_RSP, /*121*/

    /* PSW to IRAT */
    IRAT_PSW_RAT_INFO_IND,
    IRAT_PSW_MCC_RSP,
    IRAT_PSW_MCC_CHANGE_IND,
    IRAT_PSW_CUR_SYS_WEAK_IND,
    IRAT_PSW_CUR_SYS_MEAS_RSP,

    IRAT_IDP_DO_IDLE_LONG_SLEEP_RSP,
    IRAT_IDP_DO_IDLE_LONG_SLEEP_WAKEUP_RSP, /*128*/
    IRAT_DO_IDLE_SLEEP_IND,

    /* RMC to IRAT */
    IRAT_RMC_DOMEAS_ABORT_IND,   /*130*/
    IRAT_RMC_MEAS_REPORT_MSG,
    IRAT_RMC_DO_EARLY_WAKE_RSP,
    IRAT_RMC_DO_INACTIVATE_RSP,
    IRAT_OMP_CELLID_IND,
    IRAT_PSW_CELLID_IND,

    /*IRAT*/
    CSS_IRAT_EVENT_MSG,

#if defined(MTK_PLT_ON_PC_UT) && defined(MTK_DEV_C2K_IRAT)
    GMSS_CSS_MCC_SEARCH_REQ,
    GMSS_CSS_SYS_ACQUIRE_REQ,
    GMSS_CSS_CS_REG_REQ,
    GMSS_CSS_PS_REG_REQ,
    GMSS_CSS_DEACTIVATE_REQ,
    GMSS_CSS_RAT_CHANGE_REQ,
    GMSS_CSS_PS_DEREG_REQ,
    GMSS_CSS_ACTIVE_SIM_INFO_REQ,
    GMSS_CSS_PS_DEREG_CNF,
    IRATUT_CPSDM_CSS_RAT_CHANGE_RSP,
    IRATUT_MMC_CDMA_EHRPD_DEFAULT_BEARER_REQ,
    IRATUT_MMC_CDMA_EHRPD_BEARER_DISCONNECT_REQ,
    IRATUT_MMC_CDMA_DATA_CONNECTION_REQ,
    IRATUT_CSS_CONFIGURE_REQ,
    EAS_CSS_SYS_SNIFFER_REQ,
    RSVAC_CSS_FREQUENCY_SCAN_ACCEPT_IND,
    RSVAC_CSS_FREQUENCY_SCAN_PREEMPT_IND,
    GMSS_CSS_SIM_PLMN_INFO_IND,
    IRATUT_CSS_CPSDM_RESEL_REQ,
    IRATUT_CSS_CPSDM_REDIRECT_REQ,
    IRATUT_CSS_CPSDM_RAT_CHANGE_CMP_REQ,
    IRATUT_CSS_CPSDM_RESEL_TO_LTE_RSP,
#endif /* MTK_PLT_ON_PC_UT && MTK_DEV_C2K_IRAT */

#ifdef MTK_DEV_C2K_IRAT
    /* IRATM to CSS */
    CSS_IRATM_WAKEUP_FOR_IRAT_MEAS_IND,
    CSS_IRATM_TO_LTE_MEAS_CTRL_CNF, /*138*/
    CSS_IRATM_TO_LTE_RESEL_IND,
    CSS_IRATM_SUSPEND_CNF,
    CSS_IRATM_VIRTUAL_SUSPEND_CNF,
    CSS_IRATM_PLMN_LIST_UPDATE_RSP,
	CSS_IRATM_SET_RAT_CNF,
    /* IDP to CSS */
    CSS_IDP_RSVAS_VIRTUAL_SUSPEND_CNF_MSG,
    /* INSP to CSS */
    CSS_INSP_RSVAS_SUSPEND_COMP_MSG,
    /* PSW to CSS */
    CSS_PSW_RSVAS_SUSPEND_REQ_MSG,
    /* CLC_CSS_RSVAS_SUSPEND_COMP_MSG, */
    CSS_PSW_RSVAS_RESUME_REQ_MSG,
    CSS_PSW_RSVAS_DO_VRTL_SUSP_REQ_MSG,
    CSS_PSW_RSVAS_DO_RESUME_REQ_MSG,
    CSS_PSW_RSVAS_VRTL_RESUME_REQ_MSG,
    CSS_PSW_RSVAS_EVENT_REPORT_MSG,
    CSS_IRATM_PARAM_UPDATE_IND,
    CSS_IRATM_POWER_CTRL_CNF,
    /* CSS_INSP_RSVAS_SUSPEND_COMPLETE_MSG, */
#if defined(MTK_PLT_ON_PC_UT)
    MRS_CAS_OCCUPY_LLA_CNF,
    MRS_CAS_RELEASE_LLA_CNF,
    MRS_CAS_GET_LLA_OCCUPY_RAT_CNF,
    MRS_CAS_LOWER_LAYER_AVAILABILITY_UPDATE_IND,
#endif
    /* SRLTE message */
    CSS_SRLTE_MSG ,   /* 154 */
#endif
    CSS_VAL_SIM_INFO_CHANGED_IND_MSG,
    CSS_VAL_ATTACH_STATUS_CHANGED_IND_MSG,
    /* IRAT */
#ifdef MTK_CBP
    CSS_VAL_DEEP_SLEEP_MODE_REQ_MSG,
#endif
#if defined(MTK_PLT_ON_PC_UT) && defined(MTK_DEV_C2K_IRAT)
    GMSS_CSS_MMSS_LIST_SYNC_REQ,
    GMSS_CSS_3GPP_NORMAL_SERVICE_REQ,
    GMSS_CSS_SET_RAT_MODE_REQ,
    GMSS_CSS_EMC_SESSION_START_REQ,
    GMSS_CSS_EMC_SESSION_STOP_REQ,
    GMSS_CSS_EMC_LOCAL_DETACH_REQ,
#endif
#ifdef MTK_CBP
    CSS_VAL_LTE_DISABLED_REQ_MSG,
#endif
    CSS_VAL_IRAT_POWER_CTRL_UT_REQ,
#ifdef MTK_DEV_C2K_IRAT
    CSS_CLC_MAJOR_OCCUPY_LLA_REQ,
    CSS_CLC_RELEASE_LLA_REQ,
#endif
#if defined (MTK_DEV_C2K_IRAT)
    CSS_VAL_UIM_PRL_READ_REQ,
#endif
#if defined(MTK_PLT_ON_PC_UT) && defined(MTK_DEV_C2K_IRAT)
    GMSS_CSS_MSPL_SEARCH_EXHAUSTED,
    GMSS_CSS_CS_RESUME_REQ,
    GMSS_CSS_SET_RAT_MODE_CNF,
    GMSS_CSS_C2K_SERVICE_IND,
#endif
#if defined(MTK_CBP) && defined(MTK_PLT_ON_PC_UT)
    CSS_SYS_UT_TIMER_START_REQ,
    CSS_SYS_UT_TIMER_STOP_REQ,
    CSS_SYS_UT_TIMER_EXPIRED_IND,
#endif
    CSS_VAL_RAT_MODE_CHG_CNF_MSG,
    CSS_L1D_RSSI_SCAN_CNF_MSG,
    CSS_CMD_MSGID_LAST
} CssCmdMsgT;

/*----------------------------------------------------------------------------
* Basic Types
*----------------------------------------------------------------------------*/
#ifndef MTK_DEV_C2K_IRAT
typedef enum
{
    OP_HYBRID,
    OP_1X_ONLY,
    OP_DO_ONLY,
#ifdef MTK_CBP
    OP_INVALID
#endif
}OperationModeT;
#endif

#ifdef MTK_CBP
typedef enum
{
    CSS_RAT_DO,
    CSS_RAT_1X,
    CSS_RAT_1X_DO
}CssRatModeT;
typedef struct
{
    ChannelList    *Rssi_ScanList_p;
}Css_L1d_Rssi_Scan_ReqT;

typedef struct
{
    RssiResultT    *RssiResult_p;
}Css_L1d_Rssi_Scan_CnfT;

#endif

typedef enum
{
    HDM_1XVOICE,
    HDM_DATAPREF,
    HDM_VOIP
}HybridPrefModeT;

#ifdef MTK_DEV_C2K_IRAT
typedef enum
{
    MSPL_POWER_ON_SEARCHING,
    MSPL_NORMAL_SERVICE,
    MSPL_SEARCH_EXHAUSTED
}MsplSearchStatusT;
#endif

/* CSS_NWK_RPT_CP_EVENT_MSG */
typedef enum
{
    CSS_EV_DISCONNECT = 1,
    CSS_EV_PAGE_RECEIVED,
    CSS_EV_STOPALERT,
    CSS_EV_ABBR_ALERT,
    CSS_EV_NOSVC,
    CSS_EV_CDMA_INSVC,
    CSS_EV_CPENABLED,
    CSS_EV_CPDISABLED,
    CSS_EV_PDOWNACK,
    CSS_EV_CONNANALOG,
    CSS_EV_CONNDIGITAL,
    CSS_EV_ASSIGNED,
    CSS_EV_ORIG_FAIL,
    CSS_EV_NDSS_ORIG_CANCEL,
    CSS_EV_INTERCEPT,
    CSS_EV_REORDER,
    CSS_EV_CC_RELEASE,
    CSS_EV_CC_RELEASE_SO_REJ,
    CSS_EV_DIALING_COMPLETE,
    CSS_EV_DILAING_CONTINUE,
    CSS_EV_MAINTENANCE,
    CSS_EV_VP_ON,
    CSS_EV_VP_OFF,
    CSS_EV_PSIST_FAIL,
    CSS_EV_TC_RELEASE_MS,
    CSS_EV_TC_RELEASE_PDOWN,
    CSS_EV_TC_RELEASE_DISABLE,
    CSS_EV_TC_RELEASE_BS,
    CSS_EV_TC_RELEASE_SO_REJECT,
    CSS_EV_TC_RELEASE_TIMEOUT,
    CSS_EV_TC_RELEASE_ACK_FAIL,
    CSS_EV_TC_RELEASE_FADE,
    CSS_EV_TC_RELEASE_LOCK,
    CSS_EV_PAGE_FAIL,
    CSS_EV_RETRY_TIMER_ACTIVE,
    CSS_EV_RETRY_TIMER_INACTIVE,
    CSS_EV_AMPS_INSVC,
    CSS_EV_ORIG_REJECTED,
    CSS_EV_ORIG_REJECTED_SO_NOT_SUPPORTED,
    CSS_EV_ORIG_BLOCKED_BY_IRAT,

    CSS_EV_PKT_ACTIVE = 50,
    CSS_EV_PKT_RELEASED,
    CSS_EV_PKT_DORMANT,

    CSS_EV_ORIG_USER_CANCEL = 60,
    CSS_EV_ORIG_ACCESS_FAIL,
    CSS_EV_ORIG_NO_SVC_FAIL,
    CSS_EV_ORIG_RETRY_ORDER,
    CSS_EV_SR_ENABLED,
    CSS_EV_SR_DISABLED,
    CSS_EV_ORIG_REQ_RECEIVED,
    CSS_EV_PSW_POWER_UP,
    CSS_EV_PSW_POWER_DOWN,

    CSS_EV_FLASHFAIL = 80,
    CSS_EV_ANALOG_PAGE_RECEIVED,
    CSS_EV_AMPS_IDLE,

    CSS_EV_ABBR_INTERCEPT,
    CSS_EV_ABBR_REORDER,
    CSS_EV_CONNCOMPLETE,
    CSS_EV_ENTER_OOSA,
    CSS_EV_FNM_RELEASE,
    CSS_EV_MSID_UPDATED,
    CSS_EV_BUSY_ORIG_FAIL,
    CSS_EV_LOCKED_ORIG_FAIL,
    CSS_EV_REG_MAX_PROBE,
    CSS_EV_DOCONN_1XPREEMPT,
    CSS_EV_ORIG_ACCT,
    CSS_EV_REG_SYS_LOST,
#ifdef MTK_CBP
    CSS_EV_SMS_CANCEL_BY_PWRDOWN,
#endif
#ifdef MTK_DEV_C2K_IRAT
    CSS_EV_RSVAS_SUSPEND_COMPLETE,
#endif
    CSS_EV_SERVICE_SUCCESS,
    CSS_RPT_CP_EVENT_INVALID
} Css1xRptCpEventT;

typedef enum
{
    CSS_EV_DO_POWERUP,
    CSS_EV_DO_POWERDOWN
}CssDoRptCpEventT;

typedef enum
{
    NON_HYBRID,
    HYBRID,
    HYBRID_MODE_ENUM_END
}PriHybridModeT;

typedef enum
{
    CDMA_THEN_ANALOG,
    CDMAonly,
    ANALOG_THEN_CDMA,
    ANALOG_ONLY,
    DETERMINE_MODE_AUTOMATICALLY,
    EMERGENCY_MODE,
    RETRICT_TO_HOME_ONLY,
    CDMA_ONLY,
    HDR_ONLY,
    HYBRID_ONLY,
    SHDR_ONLY,
    PREF_MODE_ENUM_END
}PrefModeT;

typedef enum
{
    SUCC,
    TCH_FAIL,
    CONN_DENY,
    RETRY_DELAY,
    THROTTLING
}CssTchSetupStatusT;
typedef enum
{
    RSSI_SCAN_PHASE_INACTIVE,      /* no rssi scan filter */
    RSSI_SCAN_PHASE_SCANNING,      /* rssi scan is running */
    RSSI_SCAN_PHASE_ABORTING,      /* aborting rssi scan */
    RSSI_SCAN_PHASE_I,             /* rssi scan filter phase 1*/
    RSSI_SCAN_PHASE_II,            /* rssi scan filter phase 2*/
    RSSI_SCAN_PHASE_NOT_NEEDED     /* rssi scan filter phase 2*/
} FilterByRssiScanStateT;


typedef PACKED_PREFIX struct _RECORD_INDEX {
    uint8 *pByteOffset;
    uint32 bitOffset;
    uint16 recNum;
    uint16 recSize;
} PACKED_POSTFIX  RECORD_INDEX, PACKED_POSTFIX *pRECORD_INDEX;

typedef PACKED_PREFIX struct
{
    uint16 CPCA, CSCA;    /* CDMA CELL PRI CH, SEC CH, respectively, Sys A */
    uint16 CPCB, CSCB;    /* CDMA CELL PRI CH, SEC CH, respectively, Sys B */

    uint8 MAX_SID_NID;
    uint8 STORED_POS_SID_NID;
    uint16 SID[MAX_POSITIVE_SIDS];
    uint16 NID[MAX_POSITIVE_SIDS];

    uint8 STORED_NEG_SID_NID;
    uint16 NEG_SID[MAX_NEGATIVE_SIDS];
    uint16 NEG_NID[MAX_NEGATIVE_SIDS];

    uint8 SystemSelect;

} PACKED_POSTFIX  NAM_PREFERENCES, PACKED_POSTFIX *pNAM_PREFERENCES;

typedef enum {
    PRL_1X95 = 1,
    RSVD,
    PRL_EXT
}PRL_PREV;

typedef enum
{
    NORMAL_PRL = 0,
    CONCATENATE_PRL =1
}PRL_TYPE;

typedef enum
{
    TEST_MODE,
    MOST_PREF_SYS_IN_PRL,
    PREF_SYS_IN_PRL,
    REDIR_PREF_SYS_IN_PRL,
    REDIR_SYS_NOT_IN_PRL,
    REDIR_SYS_NO_PRL,
    NEG_SYS_IN_PRL,
    NOT_IN_PRL_BUT_HOME_SYS_IN_NAM,
    NOT_IN_PRL_BUT_NEG_SYS_IN_NAM,
    NOT_IN_PRL_OR_NAM_OR_DEFAULT_BAND,
    NON_SYSTEM_TABLE_ENTRY,
    NO_PRL_HOME_SYS_IN_NAM,
    NO_PRL_NEG_SYS_IN_NAM,
    EPRL_GOTO_PCH,
    DEFAULT_BAND_CHANNEL_SYSTEM,
    SYSTEM_MARKED_AS_NEGATIVE,
    HOME_SYSTEM_AVOIDANCE,
    REDIRECTION_SYSTEM_AVOIDANCE,
    ACCESS_MAX_PROBES_SYSTEM_AVOIDANCE,
    SYS_REJECTED_DUE_TO_HOME_ONLY,
    SYS_REJECTED_DUE_TO_INTENATIONAL_ROAM,
    SYS_REJECTED_DUE_TO_DOMESTIC_ROAM,
    SYS_ACCEPTED_DUE_TO_TEST_MODE,
    SYS_TEMPORARILY_REJECTED_DUE_TO_ECIO,
#ifdef MTK_CBP
    IN_DEFAULT_BAND_FOR_1X95_VER_PRL,
    IN_MRU_FOR_1X95_VER_PRL,
    NOT_IN_MRU_OR_DEFAULT_BAND_FOR_1X95_VER_PRL,
    NEG_SYS_IN_1X95_VER_PRL,
    IN_DEFAULT_BAND_FOR_NO_PRL,
    SYSTEM_FOR_NO_PRL,
    MOST_PREF_SYS_IN_PRL_WITH_ASSO,
    PREF_SYS_IN_PRL_WITH_ASSO,
    NEG_SYS_IN_PRL_WITH_ASSO,
    IN_DEFAULT_BAND_NOT_IN_PRL_WITH_ASSO,
    NOT_IN_DEFAULT_BAND_OR_PRL_WITH_ASSO
#endif
} SELECT_VALIDATE_STATUS;

#ifdef MTK_CBP
typedef enum
{
    CSS_GV_1X_REG_ENABLED,
    CSS_GV_IRAT_DO_PS_REGISTERED,
    CSS_GV_IRAT_1X_PS_REGISTERED,
    CSS_GV_FORCING_DO_STACK_INTO_INIT,
    CSS_GV_SYS_TYPE_TRIG_FOR_PS,
    CSS_GV_RAW_SYS_TYPE_TRIG_FOR_PS,
    CSS_GV_IRAT_SUPPORTED_PS_TYPE,
    CSS_GV_IRAT_CURRENT_PS_TYPE
} CSS_GLOBAL_VAR_E;
#endif

#if defined(MTK_PLT_ON_PC_UT) && defined(MTK_DEV_C2K_IRAT)
typedef enum
{
    CSS_CONF_LTE_IS_TURNED_OFF,        /**< LTE is turned on or not */
    CSS_CONF_UIM_IS_DISABLE,           /**< UIM is disable or not */
    CSS_CONF_UIM_STATE,                /**< UIM state (no card, UICC, ..) */
    CSS_CONF_IRAT_MODE,                /**< C2K-LTE IRAT mode */
    CSS_CONF_4G_SWITCH_OFF_BY_AT,      /**< 4G is switch off by AT command or not */
    CSS_CONF_SIM_INDEX,                /**< SIM index of C2K modem */
    CSS_CONF_PS_DATA_ATTACHED,         /**< PS data attached */
    CSS_CONF_OPERATOR_SBP_ID,          /**< Opeartor SBP ID */
    CSS_CONF_ECTM_MODE,                /**< UUT test mode */
    CSS_CONF_REL_TO_IDLE,              /**< ReleaseToIdle is ture or false */
    CSS_CONF_IDLE_GEO_LIST_SBP,        /**< idle GEO list feature SBP */
    CSS_CONF_MRU_FIRST_ENTRY_OPER_SID_NID, /**< MRU first entry operational SID/NID */
    CSS_CONF_1X_REG_ENABELD, /**< 1x registration is allowed or not */
    CSS_CONF_DATA_HOLD_SBP,           /**< T_data_hold before data call feature SBP */
    CSS_CONF_PREF_ONLY,               /**< PREF_ONLY in PRL */
    CSS_CONF_SYSTEM_SELECT_TYPE,      /**< system select type */
    CSS_CONF_SBP_FEATURE_SET,         /**< set sbp feature */
    CSS_CONF_INTL_ERI_VALUES_SET,     /**< set intlEriVals*/
    CSS_CONF_CSS_NAM_ROAMING_SET,     /**< set _css_nam.RoamingSetting */
    CSS_CONF_ACTIVE_BAND_BITMASK,     /**< set active band bitmask */
    CSS_CONF_INVALID = 0xff           /* invalid access option, only for initialisation */
}css_configure_enum;
#endif

typedef PACKED_PREFIX struct {
    bool valid;
    uint16 PR_LIST_SIZEs;
    uint16 PR_LIST_IDs;
    PRL_PREV SSPR_P_REV;
    bool PREF_ONLYs;
    uint8 DEF_ROAM_INDs;
    uint16 NUM_ACQ_RECSs;
    uint16 NUM_COMMON_SUBNET_RECS;
    uint16 NUM_SYS_RECSs;
    RECORD_INDEX acquireTableStart;
    RECORD_INDEX commSubnetTableStart;
    RECORD_INDEX systemTableStart;
    uint16 PR_LIST_CRCs;
    uint16 NUM_GEO_AREASs;
} PACKED_POSTFIX  PRL_HEADER, PACKED_POSTFIX *pPRL_HEADER;

typedef struct {
    bool valid;
    uint32 PRL_Length;
    PRL_TYPE prl_type;
    uint16 IS683A_Length;
    uint16 IS683C_Length;
    uint16 PR_LIST_CRCs;
} PRL_INFO, *pPRL_INFO;

typedef PACKED_PREFIX struct
{
    ExeRspMsgT RspInfo;
} PACKED_POSTFIX  CssGetPrlMsgT;

typedef PACKED_PREFIX struct
{
    bool e911;
} PACKED_POSTFIX  Css1xE911ModeSetMsgT;

typedef PACKED_PREFIX struct _SYSTEM_INFO_1X {
    bool   foundSomeSys;
    uint16 sid;
    uint16 nid;
    uint16 oper_sid;
    uint16 oper_nid;
    uint16 roamInd;
    uint8  systemPref;
    uint16 acqIndex;
    CHANNEL_DESC system;
    uint16  pilotStrength;
    int16   rxPower;
    uint8   sys1xPriority; /* IRAT Priority: 0 (HOME), 1 (PREF), 2 (ANY), 255 (INVALID) */
} PACKED_POSTFIX  SYSTEM_INFO_1X, PACKED_POSTFIX *pSYSTEM_INFO_1X;

typedef PACKED_PREFIX struct
{
    SYSTEM_INFO_1X Sys;
} PACKED_POSTFIX  OtaspSelectParmsT;

typedef PACKED_PREFIX struct
{
    uint16 expectedSid;
    bool   ignoreCDMA;
    uint8  sysOrdering;
    uint8  maxRedirectDelay;  /* only applies to Global redirect */
} PACKED_POSTFIX  SvcRedirAnalog;


#define CSS_MAX_SERV_REDIR_NUM_CHANS 16 /* 4 bits */
typedef PACKED_PREFIX struct
{
    uint8  bandClass;
    uint16 expectedSid;
    uint16 expectedNid;
    uint8  numChans;
    uint16 cdmaChan[CSS_MAX_SERV_REDIR_NUM_CHANS];
} PACKED_POSTFIX  SvcRedirCDMA;

typedef PACKED_PREFIX union
{
    SvcRedirAnalog analog;
    SvcRedirCDMA   cdma;
} PACKED_POSTFIX  RedirType;

typedef PACKED_PREFIX struct
{
    bool   global;                   /* indicates a global redirection */
    uint16   redirectAccolc;
    bool   returnIfFail;
    bool   deleteTMSI;
    bool   exclPRevMs;
    bool   redirectType;
    uint8  recordType;
    RedirType  type;
} PACKED_POSTFIX  SvcRedirData1xT;

typedef PACKED_PREFIX struct
{
    SvcRedirData1xT RediretionData;
    bool SysRedirectedToValid;
} PACKED_POSTFIX  RedirectionSelectParmsT;

typedef PACKED_PREFIX struct{
    uint8 indication;
#if defined(MTK_DEV_C2K_IRAT) && defined(MTK_DEV_C2K_SRLTE)
    bool  isFromRsvac;
#endif
    PACKED_PREFIX union{
        OtaspSelectParmsT OtaParms;
        RedirectionSelectParmsT RedirectParms;
    } PACKED_POSTFIX u;
} PACKED_POSTFIX Css1xSelectReqMsgT;

typedef enum
{
    CSS_1X_SID_NID_VALIDATION,
    CSS_1X_MCC_MNC_VALIDATION,
    CSS_1X_SIDNID_MCCMNC_VALIDATION
} Css1xValidationType;

typedef PACKED_PREFIX struct {
    uint16 curChan;
    SysCdmaBandT curBand;
    uint16  pilotStrength;
    int16   rxPower;
    Css1xValidationType valType;
    uint16 sid;
    uint16 nid;
    uint16 mcc;  /* mcc */
    uint16 mnc;  /* mnc (i.e. imsi_11_12) */
} PACKED_POSTFIX  Css1xValidateReqT;

typedef PACKED_PREFIX struct {
    uint16  pilotStrength;
    int16   rxPower;
} PACKED_POSTFIX  Css1xUpdateLastSavedEcIoRxPwrT;

typedef PACKED_PREFIX struct
{
    bool pending;
    bool isVoice;
} PACKED_POSTFIX  Css1XSetCallPendingMsgT;

typedef PACKED_PREFIX struct
{
    UINT16  sid;
    UINT16  nid;
    UINT16 curChan;
    SysCdmaBandT curBand;
} PACKED_POSTFIX  Css1XSysParmsUpdateMsgT;

#ifdef MTK_CBP
typedef struct
{
    PRL_INFO PRLInfo;
    uint8    *DataP;
} CssGetPrlRspMsgT;
#else
typedef PACKED_PREFIX struct
{
    PRL_HEADER   PRLHeader;
    uint8        *DataP;
} PACKED_POSTFIX  CssGetPrlRspMsgT;
#endif


#ifdef MTK_CBP
typedef NV_PACKED_PREFIX struct
{
    bool    valid;
    uint16  sid;
    uint16  nid;
} NV_PACKED_POSTFIX cssSidNid;
#endif
typedef NV_PACKED_PREFIX struct
{
    SysCdmaBandT  CdmaBand[MAX_MRU_RECORDS];
    uint16        FreqChan[MAX_MRU_RECORDS];
} NV_PACKED_POSTFIX  CssDbmDataT;

typedef PACKED_PREFIX struct
{
    uint8 nam;
} PACKED_POSTFIX  CssUpdateActiveNamMsgT;

typedef PACKED_PREFIX struct
{
    bool NvmDataWriteThru;
} PACKED_POSTFIX  CssWriteMruMsgT;

typedef PACKED_PREFIX struct
{
    SysBandChannelT channel;
} PACKED_POSTFIX  CssSaveChannelMsgT;

typedef PACKED_PREFIX struct
{
    SysBandChannelT channel;
    BOOL withSyncCh;
} PACKED_POSTFIX  CssSetReturnChannelMsgT;

typedef PACKED_PREFIX struct
{
    ExeRspMsgT RspInfo;         /* Response routing information */
} PACKED_POSTFIX  CssEriVersionNumberMsgT;

typedef PACKED_PREFIX struct
{
    uint16 eri_version_number;
} PACKED_POSTFIX  CssEriVersionNumberRspMsgT;

typedef PACKED_PREFIX struct
{
    bool newSession;
    uint8 sysRev;
} PACKED_POSTFIX  CssDOSessionOpenedMsgT;

typedef PACKED_PREFIX struct
{
    uint16 P1;
    uint16 P2;
    uint16 P3;
    uint16 P4;
    uint16 P1Delay;
    uint16 P2Delay;
    uint16 P3Delay;
    uint16 P4Delay;
    uint16 P1ScanTime;
    uint16 P2ScanTime;
    uint16 P3ScanTime;
    uint16 P4ScanTime;
    uint16 P1Cycles;
    uint16 P2Cycles;
    uint16 P3Cycles;
} PACKED_POSTFIX  CssOutOfServiceAreaParamsMsgT;

typedef PACKED_PREFIX struct
{
  Css1xRptCpEventT event;
} PACKED_POSTFIX  CssRptCpEventMsgT;

/* DO messages structure*/

typedef PACKED_PREFIX struct{
    uint8 indication;
#ifdef MTK_DEV_C2K_IRAT
    bool isFromRsvac;
#endif
} PACKED_POSTFIX  CssDoSelectReqMsgT;

typedef PACKED_PREFIX struct {
  uint8 subnet[SUBNET_LENGTH];
  uint16 curChan;
  SysCdmaBandT curBand;
} PACKED_POSTFIX  CssDoValidateReqMsgT;

typedef PACKED_PREFIX struct {
  CHANNEL_DESC Channel;
} PACKED_POSTFIX  CssDoNtwkAcqdMsgT;

typedef PACKED_PREFIX struct {
  bool SessOpened;
} PACKED_POSTFIX  CssDoSessStatusIndT;

typedef PACKED_PREFIX struct {
  OperationModeT OpMode;
  HybridPrefModeT HybridPrefMode;
  UINT16 Mpss_Duration;
} PACKED_POSTFIX  CssTestCfgT;

/* CDMA Channel Record */
typedef PACKED_PREFIX struct
{
   uint8             SysType;
   uint8             BandClass;
   uint16            Channel;
} PACKED_POSTFIX  ChanRecT;

/* ALMP Redirect Message */
typedef PACKED_PREFIX struct
{
   uint8   NumChannel;       /* 8 bits */
   ChanRecT  Channels[CP_HRPD_MAX_NUM_REDIRECT_CHANNELS]; /* 24 bits/channel */
} PACKED_POSTFIX  SvcRedirDataDoT, PACKED_POSTFIX CssDoSvcRedirDataMsgT;

typedef PACKED_PREFIX struct {
  CHANNEL_DESC Channel;
} PACKED_POSTFIX  CssDoChanChangedIndMsgT;

typedef PACKED_PREFIX struct
{
  CssDoRptCpEventT event;
} PACKED_POSTFIX  CssDoRptCpEventMsgT;

typedef PACKED_PREFIX struct
{
  bool PwrUp;  /* TRUE-powerup; FALSE-powerdown */
  uint8 Sys;   /* 0-DO, 1-1X, 2:both 1X and DO */
} PACKED_POSTFIX  CssCpPowerCtrlMsgT;

#ifdef MTK_CBP
typedef PACKED struct
{
    bool mode;  /* TRUE: enter deep sleep mode; FALSE: exit deep sleep mode */
} CssValDeepSleepModeMsgT;

typedef PACKED_PREFIX struct
{
    bool  attachStatus;
} PACKED_POSTFIX  CssValAttachStatusChangedIndMsgT;

#endif

typedef PACKED_PREFIX struct {
   uint32   HybridMode;
   uint32   PrefMode;
} PACKED_POSTFIX  DoHybridPreModeSetMsgT;

typedef PACKED_PREFIX struct {
   uint16 T_HRPD_Init;
   uint16 T_1x_Init;
} PACKED_POSTFIX  CssPeriodicalSearchCycleSetMsgT;

typedef PACKED_PREFIX struct
{
  ExeRspMsgT  RspInfo;
} PACKED_POSTFIX  CssGetPrlIdMsgT;

typedef PACKED_PREFIX struct
{
  uint16 PRL_ID;
} PACKED_POSTFIX  CssGetPrlIdRspMsgT;

typedef PACKED_PREFIX struct {
   CTDoDefaultBandDataT DOSystem[MAX_DO_DEFAULTBAND_SIZE];
} PACKED_POSTFIX  DODefaultBandSetMsgT;

typedef PACKED_PREFIX struct {
   uint8 APersistence;
} PACKED_POSTFIX  CssACMAPersistenceIndMsgT;

typedef PACKED_PREFIX struct {
   bool status; /*FALSE(0): max access probes failure, TRUE(1): access succeed*/
} PACKED_POSTFIX  CssDOAccessStatusIndT;

typedef PACKED_PREFIX struct {
   uint16 Type; /*high 8 bits are direction if current DO connection is for DO data call (1-AT initiated),
                         low 8 bits are the connection type(for example, data call, SCP con)*/
} PACKED_POSTFIX  CssDoConnStartIndMsgT;

typedef PACKED_PREFIX struct {
   uint8 type;
   uint8 cause;
} PACKED_POSTFIX  CssDoErrIndT;

typedef enum
{
   CPSM_OOSA_SCAN_TIMER=0,
   CPSM_OOSA_SCAN_LIST
}CpsmOOSAScanMethod;

typedef enum
{
   CSS_OOSA_POWER_UP = 0,
   CSS_OOSA_SYSTEM_LOST
}CpsmOOSAStage;

typedef enum
{
   CSS_1X_SUB_MODULE = 0,
   CSS_DO_SUB_MODULE
}CssSubModule;

typedef enum
{
  CSS_OOSA_PHASE_SUCCESS = 0,
  CSS_OOSA_PHASE_FAIL_GENERAL_FAILURE,
  CSS_OOSA_PHASE_FAIL_PHASE_NOT_EXIST,
  CSS_OOSA_PHASE_FAIL_PHASE_INDEX_TOO_LARGE,
  CSS_OOSA_PHASE_FAIL_SLEEP_OUT_OF_RANGE
}CssOOSAPhaseRsp;

typedef PACKED_PREFIX struct
{
  CpsmOOSAScanMethod   scanMethod;
  uint32               scanTime;
  uint8                cycles;
  uint32               delay;
} PACKED_POSTFIX CpsmOOSAPhaseT;

typedef enum
{
    CSS_1X_MPSS_TIMER = CSS_1X_TIMER_START,
    CSS_1X_EPRL_ESPM_WAIT_TIMER,
    CSS_1X_SCANNING_TIMER,
    CSS_1X_EMERGENCY_CALLBACK_MODE_TIMER,
    CSS_1X_LOST_WHILE_DO_CONN_SCAN_TIMER,
    CSS_1X_EMERGENCY_SCAN_PREV_SYSTEM_TIMER,
    CSS_1X_HOMESYS_AVOIDANCE_TIMER,
    CSS_1X_HOMESYS_AVOIDANCE_END_TIMER,
    CSS_1X_REDIRECTION_AVOIDANCE_TIMER, /*143v1[1]  4.5.1 4.5.3*/
    CSS_1X_PS_DELAY_TIMER, /* Verizon Reqs-LTE Multi Mode Operations */
#ifdef MTK_CBP
    CSS_1X_BSR_SCAN_TIMER, /* sprint feature: control BSR in 10s feature
                              if Device is not able to scan all the channels during the first 10 secs,
                              the Device MUST wait 10 extra secs before continuing scanning the remainder of the channels.
                              the first 10 secs*/
    CSS_1X_BSR_EXTRA_WAIT_TIMER, /* wait 10 extra secs*/
    CSS_1X_BSR_DATA_HOLD_TIMER, /* sprint feature: device must wait T_data_hold before establishing a data call to allow BSR completion */
    CSS_1X_REG_REJ_AVOIDANCE_TIMER, /* when registration reject continuously, need to bar it for some time */
    CSS_1X_REG_REJ_AVOIDANCE_END_TIMER,
#endif
    /* now only 2 entries left for new 1X timers */


    CSS_1X_TIMER_END,

    CSS_DO_MPSS_TIMER = CSS_DO_TIMER_START,
    CSS_DO_REDIR_END_TIMER,
    CSS_DO_SCANNING_TIMER,
    CSS_DO_WTF_SESSION_OPEN_TIMER,
    CSS_DO_AVOIDANCE_TIMER,
    CSS_DO_SCAN_INTERRUPT_TIME_TIMER,
    CSS_DO_MPSS_HOLD_TIMER, /*143v1[1]  5.4.3 T_dbsr_hold*/
    CSS_DO_MPSS_CALL_TIMER, /*143v1[1]  5.4.5 T_dbsr_call*/
    CSS_DO_CONSECUTIVE_DO_LOST_TIMER, /*143v1[1]  5.5.2*/
    CSS_DO_AVOIDANCE_LOST_TIMER, /*143v1[1]  5.5.2*/
    CSS_DO_REDIRECTION_AVOIDANCE_TIMER, /*143v1[1]  5.6.2*/
    CSS_DO_ACM_PERSIS_TEST_FAIL_AVOIDANCE_TIMER, /*143v1[1]  5.7.1*/
    CSS_DO_MAXACCESSFAIL_AVOIDANCE_TIMER, /*143v1[1]  5.7.2*/
    CSS_DO_SESSIONEGO_TIMEOUT_AVOIDANCE_TIMER, /*143v1[1]  5.7.7*/
    CSS_DO_SESSIONEGO_FAIL_AVOIDANCE_TIMER, /*143v1[1]  5.7.7*/
    CSS_DO_CONNDENY_AUTH_AVOIDANCE_TIMER, /*143v1[1]  5.7.7*/
    CSS_DO_CONNDENY_GENERAL_AVOIDANCE_TIMER, /*143v1[1]  5.7.4*/
    CSS_DO_CH_AVOIDANCE_TICK_TIMER,
    CSS_DO_SCANNING_DURATION_TIMER, /* Per KDDI's req, AT shall set the timer when start to scan,
                                     * and stop the scan when the timer expires in the scenario of "1X&DO both OOSA" */
    /*now there is 5 entries left for adding new DO timers*/

#ifdef MTK_DEV_C2K_IRAT
    CSS_IRAT_MCC_SEARCH_GUARD_TIMER,
    CSS_IRAT_PS_REG_GUARD_TIMER,
    CSS_IRAT_SYS_ACQUIRE_GUARD_TIMER,
    CSS_IRAT_RESEL_REDIR_GUARD_TIMER,
    CSS_IRAT_DEREG_GUARD_TIMER,
    DO_REL_TIMER = EHRPD_TIMER_START,
    CSS_IRAT_PS_RAT_RPT_DELAY_TIMER,
    CSS_IRAT_TDO_TIMER,
    CSS_IRAT_DO_RESUME_TIMER,
    CSS_IRAT_SET_RAT_MODE_TIMER,
    CSS_IRAT_DEACTIVATE_TIMER,
#endif
    CSS_1X_RELEASE_REJ_AVOIDANCE_TIMER,   /* when registration reject continuously, need to bar it for some time */
    CSS_1X_RELEASE_REJ_AVOIDANCE_END_TIMER,
    CSS_DO_TIMER_END,

#ifdef MTK_CBP
    CSS_MAX_NUMBER_OF_TIMER_TYPES
#else
    CSS_MAX_NUMBER_OF_TIMERS
#endif
} CssTimerTypeE;

#ifdef MTK_CBP
#define CSS_MAX_NUMBER_OF_TIMER_IDS ((CSS_1X_TIMER_END) - (CSS_1X_TIMER_START) + \
                                     (CSS_DO_TIMER_END) - (CSS_DO_TIMER_START) + \
                                     (MAX_1X_MAX_ACCESS_FAIL_AVOID_CHAN_NUM) - 1 + \
                                     (MAX_DO_MAX_ACCESS_FAIL_AVOID_CHAN_NUM) - 1)
#endif

typedef enum {
   CSS_1X_T_BSR_1,
   CSS_1X_T_BSR_2,
   CSS_1X_T_BSR_NEWSYS,
   CSS_1X_T_BSR_REDIR,
   CSS_1X_T_BSR_CALL,
#ifdef MTK_CBP
   CSS_1X_T_BSR_HOLD,    /* Old MPSS_ADDITIONAL_TIME timer.*/
                         /* For Sprint,this timer will be started when BSR is expired and reselection is not possible due to HDR activity.*/
                         /*When the timer expires,UE looks for more preferred CDMA systems.*/
#endif
   CSS_1X_NUM_T_BSR,
   CSS_1X_NUM_T_BSR_MAX = 10 /* enum shall not exceed this value */
} Css1xBsrTimerTypeT;

typedef enum {
   CSS_DO_T_BSR_1,
   CSS_DO_T_BSR_2,
   CSS_DO_T_BSR_HYBRID,
   CSS_DO_T_BSR_REDIR,
   CSS_DO_T_BSR_CALL,
   CSS_DO_T_BSR_HOLD,
   CSS_DO_NUM_T_BSR,
   CSS_DO_NUM_T_BSR_MAX = 10 /* enum shall not exceed this value */
} CssDOBsrTimerTypeT;

typedef enum {
   CSS_1X_T_MAPE_HOME_AVOIDANCE,
   CSS_1X_T_REDIR_AVOIDANCE,
   CSS_1X_NUM_T_AVOIDANCE,
   CSS_1X_NUM_T_AVOIDANCE_MAX = 6  /* enum shall not exceed this value */
} Css1xAvoidanceTimerTypeT;

typedef enum {
   CSS_DO_T_SYS_LOST_AVOIDANCE,
   CSS_DO_T_REDIR_AVOIDANCE,
   CSS_DO_NUM_T_AVOIDANCE,
   CSS_DO_NUM_T_AVOIDANCE_MAX = 12  /* enum shall not exceed this value */
} CssDOAvoidanceTimerTypeT;


/*------------------------------------------------------*/
/* Begin CSS Set/Get Parm definitions                   */
/*------------------------------------------------------*/
/* Bitmap (on uint8) of all the CSS DBM segments */
typedef enum
{
  CSS_DBM_1X    = 0x01,
  CSS_DBM_DO    = 0x02,
  CSS_DBM_MISC  = 0x04,
  CSS_DBM_RSVD5 = 0x08,
  CSS_DBM_RSVD4 = 0x10,
  CSS_DBM_RSVD3 = 0x20,
  CSS_DBM_RSVD2 = 0x40,
  CSS_DBM_RSVD1 = 0x80,
  CSS_DBM_ALL   = 0xff
} CssDbmId;

typedef PACKED_PREFIX struct
{
  SysCdmaBandT band;
  uint16       channel;
} PACKED_POSTFIX MruBandChannelT;

typedef PACKED_PREFIX struct
{
   CssDbmId DbmId;
} PACKED_POSTFIX  CssDbmInitDBToDefaultMsgT;

typedef enum
{
    CSS_PARM_SET_1X_MPSS_PILOT_THRESH,
    CSS_PARM_GET_1X_MPSS_PILOT_THRESH,
    CSS_PARM_SET_DO_OOSA_SCANNING_TIMER,
    CSS_PARM_GET_DO_OOSA_SCANNING_TIMER,
    CSS_PARM_SET_1X_T_BSR_DURATION,
    CSS_PARM_GET_1X_T_BSR_DURATION,
    CSS_PARM_SET_1X_AVOIDANCE_DURATION,
    CSS_PARM_GET_1X_AVOIDANCE_DURATION,
    CSS_PARM_SET_DO_T_BSR_DURATION,
    CSS_PARM_GET_DO_T_BSR_DURATION,
    CSS_PARM_SET_1X_MANUAL_AVOID_SID,
    CSS_PARM_GET_1X_MANUAL_AVOID_SID,
    CSS_PARM_SET_MRU_CHANNEL,
    CSS_PARM_GET_MRU_CHANNEL,
    CSS_PARM_SET_LOCK_CHANNEL,
    CSS_PARM_GET_LOCK_CHANNEL,
    CSS_PARM_SET_1X_PS_DELAY_TIMER_DURATION,
    CSS_PARM_GET_1X_PS_DELAY_TIMER_DURATION,
#ifdef MTK_CBP
    CSS_PARM_SET_1X_DO_T_BSR_1_2_DURATION,
    CSS_PARM_GET_1X_DO_T_BSR_1_2_DURATION,
#endif
    /* IMPORTANT:
     * when adding new parameters to the list, add BOTH SET and GET,
     * even if one of them is not used/implemented */
    CSS_PARM_OPERATION_ID_END_LIST
} CssParmOperationId;

typedef enum
{
    CSS_PARM_OPERATION_SUCCESS,
    CSS_PARM_OPERATION_FAIL_PARM_ID_NOT_SUPPORTED,
    CSS_PARM_OPERATION_FAIL_READ_NOT_ALLOWED,
    CSS_PARM_OPERATION_FAIL_WRITE_NOT_ALLOWED,
    CSS_PARM_OPERATION_FAIL_INVALID_PTR,
    CSS_PARM_OPERATION_FAIL_INVALID_LENGTH,
    CSS_PARM_OPERATION_GENERAL_FAILURE,
    CSS_PARM_OPERATION_NO_CHANGE_IN_VALUE,
    CSS_PARM_OPERATION_FAIL_VALUE_OUT_OF_RANGE,
    CSS_PARM_OPERATION_FAIL_OP_TYPE_NOT_SUPPORTED,
    CSS_PARM_OPERATION_FAIL_DEFAULT_NOT_DEFINED,
    CSS_PARM_OPERATION_FAIL_DEFAULT_NOT_SUPPORTED_FOR_PARM,
    CSS_PARM_OPERATION_FAIL_MISSING_PARAMETER,
    CSS_PARM_OPERATION_RESULT_END_LIST
}CssParmAccessResultCode;

typedef enum
{
    CSS_PARM_MIN_VALUE,
    CSS_PARM_MAX_VALUE,
    CSS_PARM_DEFAULT_VALUE,
    CSS_PARM_CUSTOM_VALUE,
    CSS_PARM_OP_TYPE_LIST_END
} CssParmOperationType;


typedef PACKED_PREFIX struct
{
    UINT16 pilotThresh;
} PACKED_POSTFIX  Css1xMpssPilotStrengthThresh_APIStruct;

typedef PACKED_PREFIX struct
{
    uint32  duration;
} PACKED_POSTFIX  CssTimerDuration_APIStruct;

typedef PACKED_PREFIX struct
{
    Css1xBsrTimerTypeT   tbsr;
    uint32               duration;
} PACKED_POSTFIX  Css1xBsrTimerDuration_APIStruct;

typedef PACKED_PREFIX struct
{
    Css1xAvoidanceTimerTypeT   avoidance;
    uint32                     duration;
} PACKED_POSTFIX  Css1xAvoidanceTimerDuration_APIStruct;

typedef PACKED_PREFIX struct
{
    CssDOBsrTimerTypeT   tbsr;
    uint32               duration;
} PACKED_POSTFIX  CssDOBsrTimerDuration_APIStruct;

typedef PACKED_PREFIX struct
{
    uint16 Sid[MAX_MANUAL_AVOID_1XSYS];
} PACKED_POSTFIX Css1xManualAvoidSid_APIStruct;

typedef PACKED_PREFIX struct
{
    uint8           subModule;
    uint8           index;
    MruBandChannelT mruChannel;
} PACKED_POSTFIX CssSetMruChannel_APIStruct;

typedef PACKED_PREFIX struct
{
    uint8           subModule;
    uint8           index;
    uint8           num1x;
    uint8           numDO;
    MruBandChannelT mru[MAX_MRU_RECORDS + MAX_MRU_RECORDS]; /* to contain 1x and/or DO MRU */
} PACKED_POSTFIX CssGetMruChannel_APIStruct;

#ifdef MTK_DEV_C2K_IRAT
typedef PACKED_PREFIX struct
{
    uint8   sysType;
    Bool    b1xChnlLocked;
    uint16  rttChnl;
    Bool    bDoChnlLocked;
    uint16  doChnl;
} PACKED_POSTFIX CssSetChannleLockInfo_APIStruct;
#endif

/*------------------------------------------------------*/
/* End CSS Parm Set/Get definitions                     */
/*------------------------------------------------------*/

typedef PACKED_PREFIX struct
{
    ExeRspMsgT           rspInfo;
    CssSubModule         subModule;
    CpsmOOSAStage        stage;
    uint8                phase;
    CpsmOOSAScanMethod   scanMethod;
    uint32               scanTime;
    uint8                cycles;
    uint32               delay;
} PACKED_POSTFIX CssOOSASetPhaseParmsMsgT;

typedef PACKED_PREFIX struct
{
    CssOOSAPhaseRsp      result;
    CssSubModule         subModule;
    CpsmOOSAStage        stage;
    uint8                phase;
} PACKED_POSTFIX CssOOSASetPhaseParmsRspMsgT;

typedef PACKED_PREFIX struct
{
    ExeRspMsgT           rspInfo;
    CssSubModule         subModule;
    CpsmOOSAStage        stage;
    uint8                phase;
    CpsmOOSAScanMethod   scanMethod;
    uint32               scanTime;
    uint8                cycles;
    uint32               delay;
} PACKED_POSTFIX CssOOSAGetPhaseParmsMsgT;

typedef PACKED_PREFIX struct
{
    CssOOSAPhaseRsp      result;
    CssSubModule         subModule;
    CpsmOOSAStage        stage;
    uint8                phase;
    CpsmOOSAScanMethod   scanMethod;
    uint32               scanTime;
    uint8                cycles;
    uint32               delay;
} PACKED_POSTFIX CssOOSAGetPhaseParmsRspMsgT;

typedef PACKED_PREFIX struct
{
    ExeRspMsgT           rspInfo;
    CssSubModule         subModule;
    bool  Enable;
} PACKED_POSTFIX CpsmOOSASetEnable_APIStruct;

#ifdef MTK_DEV_C2K_IRAT
typedef PACKED_PREFIX struct
{
    css_mmss_updated_file_id_enum file_id;
} PACKED_POSTFIX CssMMSSFileUpdateMsgT;
#endif

typedef PACKED_PREFIX struct
{
    CssOOSAPhaseRsp      result;
    CssSubModule         subModule;
    bool  Enable;
} PACKED_POSTFIX CpsmOOSASetEnableRsp_APIStruct;

typedef PACKED_PREFIX struct
{
    ExeRspMsgT           rspInfo;
    CssSubModule         subModule;
} PACKED_POSTFIX CpsmOOSAGetEnable_APIStruct;

typedef PACKED_PREFIX struct
{
    CssOOSAPhaseRsp      result;
    CssSubModule         subModule;
    bool  Enable;
} PACKED_POSTFIX CpsmOOSAGetEnableRsp_APIStruct;

typedef PACKED_PREFIX struct
{
    ExeRspMsgT           rspInfo;
    CssSubModule         subModule;
    CpsmOOSAStage        stage;
    uint8                numPhases;
} PACKED_POSTFIX CpsmOOSASetNumPhases_APIStruct;

typedef PACKED_PREFIX struct
{
    CssOOSAPhaseRsp      result;
    CssSubModule         subModule;
    CpsmOOSAStage        stage;
    uint8                numPhases;
} PACKED_POSTFIX CpsmOOSASetNumPhasesRsp_APIStruct;

typedef PACKED_PREFIX struct
{
    ExeRspMsgT           rspInfo;
    CssSubModule         subModule;
    CpsmOOSAStage        stage;
} PACKED_POSTFIX CpsmOOSAGetNumPhases_APIStruct;

typedef PACKED_PREFIX struct
{
    CssOOSAPhaseRsp      result;
    CssSubModule         subModule;
    CpsmOOSAStage        stage;
    uint8                numPhases;
} PACKED_POSTFIX CpsmOOSAGetNumPhasesRsp_APIStruct;

typedef PACKED_PREFIX struct
{
    ExeRspMsgT           rspInfo;
    CssSubModule         subModule;
    CpsmOOSAStage        stage;
    uint8                phase;
    uint8                cycle;
} PACKED_POSTFIX CpsmOOSASetCurrentStage_APIStruct;

typedef PACKED_PREFIX struct
{
    CssOOSAPhaseRsp      result;
    CssSubModule         subModule;
    CpsmOOSAStage        stage;
    uint8                phase;
    uint8                cycle;
} PACKED_POSTFIX CpsmOOSASetCurrentStageRsp_APIStruct;

typedef PACKED_PREFIX struct
{
    ExeRspMsgT           rspInfo;
    CssSubModule         subModule;
} PACKED_POSTFIX CpsmOOSAGetCurrentStage_APIStruct;

typedef PACKED_PREFIX struct
{
    CssOOSAPhaseRsp      result;
    CssSubModule         subModule;
    CpsmOOSAStage        stage;
    uint8                phase;
    uint8                cycle;
} PACKED_POSTFIX CpsmOOSAGetCurrentStageRsp_APIStruct;

typedef PACKED_PREFIX struct
{
    CpsmOOSAStage        stage;
    uint8                phase;
    uint8                cycle;
} PACKED_POSTFIX CpsmOOSACurrentPhase;

typedef PACKED_PREFIX struct
{
    ExeRspMsgT           rspInfo;
    CssSubModule         System;
} PACKED_POSTFIX CpsmGetMccT;

typedef PACKED_PREFIX struct
{
    uint16                Mcc;
    CssSubModule          System;
} PACKED_POSTFIX CpsmGetMcccRspT;

typedef PACKED_PREFIX struct
{
    UINT8 APersistence;
    SysCdmaBandT abandonBand;
    UINT16 abandonChan;
} PACKED_POSTFIX DOAbandonInfoT;

typedef PACKED_PREFIX struct
{
    SYSTEM_INFO_1X sys;
    UINT8 Counter;
#ifdef MTK_CBP
    bool  Valid;
#endif
} PACKED_POSTFIX _1XHomeSysAvoidanceInfoT;

typedef enum
{
    CPSM_1X_OOSA_SLEEP_REQUEST = 0,
    CPSM_1X_OOSA_SCAN_TIME_SET,
    CPSM_1X_OOSA_SCAN_TIME_EXPIRE,
    CPSM_1X_OOSA_WAKEUP_INDICATOR,
    CPSM_1X_OOSA_ENTER_NEW_CYCLE,
    CPSM_1X_OOSA_NOT_ALLOWED_FOUND_NON_PRL,
    CPSM_1X_OOSA_NOT_ALLOWED_FOUND_LPS,
    CPSM_1X_OOSA_NOT_ALLOWED_BY_SVSM_STATE,
    CPSM_1X_OOSA_COUNTERS_RESET,
    CPSM_1X_OOSA_ENTER_STAGE
}CssOOSAEvent;

typedef PACKED_PREFIX struct
{
    uint8        CtrlMode; /* HWD_ENABLE = 0, HWD_DISABLE = 1, HWD_LOAD = 2 */
    uint8        System;   /* 1x/DO */
    uint8        Band;
    uint16       Channel;
} PACKED_POSTFIX  CssTstPllChannelMsgT;

typedef PACKED_PREFIX struct
{
    uint8  NetworkPreference;
} PACKED_POSTFIX  CssSvcStatusReqMsgT;

typedef enum
{
    CSS_1X_MARK_SYSTEM_AS_NEGATIVE,
    CSS_1X_CLEAR_MARKED_SYSTEM_LIST
} CssMarkSystemT;

typedef PACKED_PREFIX struct
{
    CssMarkSystemT action;
} PACKED_POSTFIX  CssMarkSystemMsgT;

typedef PACKED_PREFIX struct
{
    BOOL Redirection;
} PACKED_POSTFIX  Css1xRedirIndMsgT;

typedef PACKED_PREFIX struct
{
    UINT32 timer;
} PACKED_POSTFIX  CssTimerExpiredMsgT;

typedef struct
{
    int16 TxPwr;      /* in Q7 format */
} IratRcpTxPwrRspT, IratL1dTxPwrRspT;

typedef PACKED_PREFIX struct
{
    CssTchSetupStatusT status;
} PACKED_POSTFIX  CssDataTchSetupStatusMsgT;

typedef PACKED_PREFIX struct
{
    ExeRspMsgT            rspInfo;
    CssSubModule          subModule;
    uint32                duration;
} PACKED_POSTFIX CpsmOOSAScanTimeSetMsgT;

typedef PACKED_PREFIX struct
{
    CssOOSAPhaseRsp     result;
    CssSubModule        subModule;
    uint32              duration;
} PACKED_POSTFIX CpsmOOSAScanTimeSetRspMsgT;

typedef PACKED_PREFIX struct
{
    ExeRspMsgT          rspInfo;
    CssSubModule        subModule;
    uint32              duration;
} PACKED_POSTFIX CpsmOOSAScanTimeGetMsgT;

typedef PACKED_PREFIX struct
{
    CssOOSAPhaseRsp     result;
    CssSubModule        subModule;
    uint32              duration;
} PACKED_POSTFIX CpsmOOSAScanTimeGetRspMsgT;

typedef PACKED_PREFIX struct
{
    uint8 RegStatus;   /* 0-1X reg succeed. 1-1X reg fail. others - reserved */
} PACKED_POSTFIX  CssPsw1XRegStatusIndT;

typedef PACKED_PREFIX struct
{
    uint8 DOOverlap;  /* the value of the 3rd bit of BASE_ID in SPM of LGT 1X(sid=2190) */
} PACKED_POSTFIX  CssPsw1XDOOverlapIndT;

/*IRAT*/
#ifdef MTK_DEV_C2K_IRAT
/*CSS_IRATM_TO_LTE_RESEL_IND*/
typedef struct
{
    cas_eas_activate_ecell_req_struct  ReselToLteInfo;
} CssIratmToLteReselIndT;

/*CSS_IRATM_PARAM_UPDATE_IND*/
typedef struct
{
    kal_bool                  is_valid;       /* indicate if the CDMA2000 neighbor cell reselection parameter is valid */
    nbr_cell_list_c2k_struct  nbr_cell_list;  /* indicate the CDMA2000 neighbor cell reselection parameters */
} CssIratmParaUpdateIndT;

/*IRATM_CSS_PLMN_LIST_UPDATE_RSP*/
typedef struct
{
    gmss_css_sim_plmn_info_rsp_struct PlmnInfoRsp;
}CssIratmPlmnListUpdateRspT;

/*CSS_IRATM_POWER_CTRL_CNF*/
typedef struct
{
    bool powerStatus; /*0: power off; 1: power on */
}CssIratmPowerCtrlCnfT;
/*CSS_IRATM_MEAS_CTRL_CNF*/
typedef struct
{
    bool isMeasStart; /*0: measure off; 1: measure on */
}CssIratmMeasCtrlCnfT;

/* CSS_CLC_RELEASE_LLA_REQ */
typedef struct
{
    uint8 LLAReleaseOwner;
}CssClcReleaseLlaReqT;
#endif
/*IRAT*/
#ifdef MTK_DEV_C2K_IRAT

/* copy of AppImsNetwkConnRspMsgT */
typedef struct
{
    uint8          PdnId;
    uint8          BearerId;/*use 0xf0|PdnId to indicate default bearer*/
    AppOptStatusT  Status;
    IPAddrTypeT    AddrType;
    uint32         LocalIPAddr[PDN_ADD_LEN];/*the last byte is Ipv4 addr*/
    uint8          Pcscf6Num;
    uint32         PCSCF6Addr[MAX_SUPPORTED_ADDR_NUM][4];
    uint8          DNS6Num;
    uint32         DNS6Addr[MAX_SUPPORTED_ADDR_NUM][4];
    uint8          Pcscf4Num;
    uint32         PCSCF4Addr[MAX_SUPPORTED_ADDR_NUM];
    uint8          DNS4Num;
    uint32         DNS4Addr[MAX_SUPPORTED_ADDR_NUM];

    uint32         RouteAddr;
    uint8          IP6Interfaceid[8];
    uint8          SelBearerCtrlMode;
    PdnCfgErrorCodeT   ErrCode;
    uint8          ResContainerLen; /*length of Operation reserved PCO container FF00H, 0 means not available*/
    uint8          ResContainer[255]; /*content of operation reserved PCO container FF00H*/
    UINT32         DNSPriAddr;
    UINT32         DNSSecAddr;
#ifdef MTK_CBP //MTK_DEV_C2K_IRAT
    uint16         IPv4MTU;
#endif /* MTK_DEV_C2K_IRAT */
}CssHlpNetwkConnRspMsgT;

typedef struct
{
    AppOptStatusT   Status;
    uint8           PdnId;
    uint8           Bearer_Id;
    uint8           ErrCode;
} CssHlpEpsBearerSetupRspT;

typedef struct
{
    bool  NeedResetConn; /* This field shall be filled with value of same field in corresponding HLP_CSS_NETWORK_REL_REQ message */
} CssHlpNetwkRelRspMsgT;

typedef struct
{
    AppOptStatusT  Status;
    uint8       Pdn_Id;
    uint8       Bearer_Id;
} CssHlpEpsBearerDisconnRspT;

typedef struct
{
    uint8  Pdn_Id;
    uint8  Bearer_Id;
    uint8  Reason;
} CssHlpEpsBearerDisconnIndT;

typedef struct
{
    uint8  Pdn_Id;
    uint8  Bearer_Id;
    uint8  ReconnectionInd; /*1-Reconnect this APN not allowed*/
} CssHlpEpsDefBearerDisconnIndT;

typedef struct
{
    uint8          PdnId;
    uint8          Bearer_Id;
    uint8          Num;
    QoMPktFilterContentT   Tft[MAX_SUPPORTED_IPFLOW];
} CssHlpEpsBearerSetupIndT;

typedef struct
{
    IratPSTypeT psType;
} CssHlpPppOpenReqRcvdIndT;

typedef struct
{
    UINT8          status;
    UINT32         LocalIPAddr;
    UINT32         RemoteIPAddr;
    UINT32         PriDNSAddr;
    UINT32         SecDNSAddr;
} CssValPPPConnStatusIndT;

typedef enum
{
    ACTIVE_STATE,
    DORMANT_STATE,
    IDLING_STATE,
    SUSPEND_STATE,
    DISABLE_STATE,
    UNAVAILABLE_STATE,
    RELEASED_STATE,
    HO_1X_REQ_STATE,
    HO_EHRPD_REQ_STATE,
    RESET_STATE,
    EHRPD_RETRY_REQ_STATE
} CssDataStateIndT;

typedef struct
{
    CssDataStateIndT    state;
} CssValDataStateIndMsgT;

typedef struct
{
    uint8 CgiAvl;  /* 0 - can't acquire CGI(including PnCgi not specified case).  1 - CGI is available */
    uint8 Cgi[16];       /* sectorID from Sector Parameter Message */
    uint8 Band;          /* CDMA bandclass */
    uint16 Channel;      /* CDMA channel number */
    uint8 NumPn;         /* number of PN included */
    IratCdmaPilotMeasResultT Pilot[1]; /* CDMA pilot measure result */
} IratRupMeasReportT;

typedef struct
{
    uint8 Band;          /* CDMA bandclass */
    uint16 Channel;      /* CDMA channel number */
    IratCdmaPilotMeasResultT Pilot; /* CDMA pilot measure result */
} IratRupCurSysMeasRspT, IratPswCurSysMeasRspT;

typedef struct
{
    uint8 available; /* 0-unavailale(msg not include RatInfo data); 1-available */
    IratEUTRANMeasureInfoT RatInfo;
    uint8 ServingPriority; /* priority of current serving CDMA channel. 0xff means not provided */
    int8 ThreshServing;    /* CDMA serving pilot strength threshold trigger point for EUTRAN
                              neighbor selection. 0xff means not provided */
} IratOmpRATInfoIndT, IratPswRATInfoIndT;

typedef struct
{
    uint16 Mcc; /* 0xffff means not available */
    uint8 pRev; /* pRev of CDMA95 and 1X */
} IratOmpMCCRspT, IratPswMCCRspT;

#if defined(MTK_DEV_C2K_IRAT)
typedef struct
{
    uint16 Mcc; /* 0xffff means not available */
    BOOL   regenabled; /* 1x register enabled or not*/
} IratOmpMccChangeIndT, IratPswMccChangeIndT;
#endif

typedef struct
{
    uint8 Band;          /* CDMA bandclass */
    uint16 Channel;      /* CDMA channel number */
    IratCdmaPilotMeasResultT Pilot; /* CDMA pilot measure result */
} IratRupCurSysWeakIndT, IratPswCurSysWeakIndT; /* content same as xxxCurSysMeasRspT */

typedef struct
{
    bool bOtherRATUpdated;
}IratOmpRATUpdatedMsgT;

typedef struct
{
    uint16 sid;
    uint16 nid;
    uint16 baseId;
    uint16 mcc;
    uint16 mnc;
    uint8  pzid;
} IratPswCellIDIndT;

typedef struct
{
    uint8 sectorId[16];
    uint8 subnetLength;
} IratOmpCellIDIndT;


#ifdef MTK_DEV_C2K_IRAT

typedef struct
{
    IratEvent event;
    uint32    eventParameter;
} CssIratEventMsgT;

typedef enum
{
    RSVAS_CSS_EVENT_CS_OCCUPY,
    RSVAS_CSS_EVENT_CS_CANCEL,
    RSVAS_CSS_EVENT_CS_OCCUPY_IND,
    RSVAS_CSS_EVENT_CS_RELEASE,
    RSVAS_CSS_EVENT_PS_OCCUPY,
    RSVAS_CSS_EVENT_PS_CANCEL,
    RSVAS_CSS_EVENT_PS_OCCUPY_IND,
    RSVAS_CSS_EVENT_PS_RELEASE,
    RSVAS_CSS_EVENT_RECV_VIRTUAL_SUSPEND,
    RSVAS_CSS_EVENT_RECV_RESUME_IN_VIRTUAL_MODE
} CssRsvasEventT;

/* CSS_PSW_RSVAS_EVENT_REPORT_MSG */
typedef PACKED_PREFIX struct
{
    CssRsvasEventT  cssRsvasEvent;
    rsvas_sim_enum  servOwnerSimId;
    rsvas_sim_enum  currC2kSimId;
} PACKED_POSTFIX  CssRsvasEventReportMsgT;

/* CSS_PSW_RSVAS_SUSPEND_REQ_MSG */
typedef PACKED_PREFIX struct
{
    rsvas_sim_enum servOwnerSimId;
    rsvas_sim_enum currC2kSimId;
} PACKED_POSTFIX  CssPswRsvasSuspendReqMsgT;

/* CSS_PSW_RSVAS_DO_VRTL_SUSP_REQ_MSG */
typedef PACKED_PREFIX struct
{
    rsvas_sim_enum servOwnerSimId;
    rsvas_sim_enum currC2kSimId;
} PACKED_POSTFIX  CssPswRsvasDoVrtlSuspReqMsgT;

/* CSS_PSW_RSVAS_DO_RESUME_REQ_MSG */
typedef PACKED_PREFIX struct
{
    rsvas_sim_enum servOwnerSimId;
    rsvas_sim_enum currC2kSimId;
} PACKED_POSTFIX  CssPswRsvasDoResumeReqMsgT;

/* CSS_PSW_RSVAS_VRTL_RESUME_REQ_MSG */
typedef PACKED_PREFIX struct
{
    rsvas_sim_enum servOwnerSimId;
    rsvas_sim_enum currC2kSimId;
} PACKED_POSTFIX  CssPswRsvasVrtlResumeReqMsgT;

/* CSS_PSW_RSVAS_RESUME_REQ_MSG */
typedef PACKED_PREFIX struct
{
    rsvas_sim_enum servOwnerSimId;
    rsvas_sim_enum currC2kSimId;
} PACKED_POSTFIX  CssPswRsvasResumeReqMsgT;

enum rsvac_1x_states
{
    RSVAC_1X_NULL_ST,
    RSVAC_1X_WAITING_ST,
    RSVAC_1X_PREEMPTING_ST,
    RSVAC_1X_RUNNING_ST,
    RSVAC_1X_NUM_STATES
};
typedef enum rsvac_1x_states RSVAC_1X_STATES;

enum rsvac_do_states
{
    RSVAC_DO_NULL_ST,
    RSVAC_DO_WAITING_ST,
    RSVAC_DO_PREEMPTING_ST,
    RSVAC_DO_RUNNING_ST,
    RSVAC_DO_NUM_STATES
};
typedef enum rsvac_do_states RSVAC_DO_STATES;

typedef enum
{
    SYS_1xRTT,
    SYS_EVDO,
    SYS_BOTH
}C2K_SystemT;

typedef struct
{
    PACKED_PREFIX union{
        RSVAC_1X_STATES rsvac1xState;
        RSVAC_DO_STATES rsvacDoState;
    } PACKED_POSTFIX u;
    bool isCurrFullBand;
    bool isNewFreqScanRequest;
    uint8 currSysIndication;
    bool isWaitModifyResult;
}rsvacContext;
#endif /* MTK_DEV_C2K_SRLTE */
#if defined(MTK_PLT_ON_PC_UT) && defined(MTK_DEV_C2K_IRAT)
typedef struct
{
    css_configure_enum    configure_id;
    uint8                 value;
    uint32                valueArray[10];
}css_configure_req_struct;
#endif

#if defined(MTK_CBP) && defined(MTK_PLT_ON_PC_UT)
/* msg struct of msg: CSS_SYS_UT_TIMER_START_REQ */
typedef struct
{
    CssTimerTypeE   timerId;
    uint16          index;
    uint32          duration;
}css_ut_timer_start_req_struct;

/* msg struct of msg: CSS_SYS_UT_TIMER_STOP_REQ */
typedef struct
{
    CssTimerTypeE   timerId;
    uint16          index;
}css_ut_timer_stop_req_struct;

/* msg struct of msg: CSS_SYS_UT_TIMER_EXPIRED_MSG */
typedef struct
{
    CssTimerTypeE   type;
    uint16          index;
}css_ut_timer_expired_ind_struct;
#endif
#endif /* MTK_DEV_C2K_IRAT */


#ifdef MTK_DEV_C2K_IRAT
BOOL IRATGet1XPSEnabled(void);
BOOL IRATIsCsRegPendedForSilentRedial(void);
#endif
BOOL IRATGet1XRegEnabled(void);
void IRATSet1xRegEnabled(bool val);
#ifdef MTK_CBP
extern void MonCssGenericInfoSpy(const char* fmt, ...);
#endif

#if defined (MTK_DEV_C2K_IRAT) && defined (MTK_DEV_C2K_SRLTE_L1)
extern uint32 SysFrcCntGet(void);
#endif
extern void IopTraceToAPMainLog(const char* fmt, ...);
#ifdef MTK_DEV_LOG_SPLM
extern void IopEtsSplmTxMDLog(void*  pPayload, uint16 Payload_Len );
#endif
#endif

/**Log information: \main\CBP80\cbp80_yzhang_scbp10127\1 2012-08-03 06:45:34 GMT yzhang
** Sprint EHRPD requirement:IPCP,AUTH,DNS Server Addr in PCO of VSNCP**/
/**Log information: \main\Trophy\Trophy_xding_href22331\1 2013-12-10 07:16:05 GMT xding
** HREF#22331, 合并MMC相关功能到Trophy baseline上**/
/**Log information: \main\Trophy\1 2013-12-10 08:33:05 GMT jzwang
** href#22331:Merge MMC latest implementation from Qilian branch.**/
