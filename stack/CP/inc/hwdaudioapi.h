/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
/*****************************************************************************
 
  FILE NAME: hwdaudioapi.h

  DESCRIPTION:

  This file contains all the audio hardware driver subroutines prototypes

*****************************************************************************/

#ifndef _HWD_AUDIO_API_H_
#define _HWD_AUDIO_API_H_

#include "hwdapi.h"
#include "sysdefs.h"
#include "ipcapi.h"

#define HWD_VC_CP_SELECT		0x00
#define HWD_VC_DV_SELECT		0x01

#define HWD_VC_REGSEL			HWD_VC_CP_SELECT

#define HWD_AUDIO_DURATION_FOREVER 0xFFFF

	/*
	 *	Volume settings are used for several DSPV volume controls
	 *	Pay attention when modifying the number of settings, and think
	 *	first if your goal could not be better accomplished by altering
	 *	the volume-to-DSPV mapping in hwdaudio.c rather than changing
	 *	the number (or definitions) of volume steps.
	 */
typedef enum
{
   HWD_AUDIO_VOL_UNCHG  = -4,
   HWD_AUDIO_VOL_UP     = -3,
   HWD_AUDIO_VOL_DOWN   = -2,
   HWD_AUDIO_VOL_UNMUTE = -1,
   HWD_AUDIO_VOL_MUTE   = 0,	/* easier to cast form int... */
   HWD_AUDIO_VOL_1      = 1,
   HWD_AUDIO_VOL_2,
   HWD_AUDIO_VOL_3,
   HWD_AUDIO_VOL_4,
   HWD_AUDIO_VOL_5,
   HWD_AUDIO_VOL_6,
   HWD_AUDIO_VOL_7,
   HWD_AUDIO_VOL_8,
   HWD_AUDIO_VOL_9,
   HWD_AUDIO_VOL_10,
   HWD_AUDIO_VOL_DEFAULT= HWD_AUDIO_VOL_3,
   HWD_AUDIO_VOL_MAX    = HWD_AUDIO_VOL_10
} HwdAudioVolT;

    /* individual channel mute: left, right or neither chan (HWD_AUDIO_VOL_MUTE overrides this) */
typedef enum
{
    HWD_AUDIO_NO_CHAN_MUTED = 0,
    HWD_AUDIO_RIGHT_CHAN_MUTED,
    HWD_AUDIO_LEFT_CHAN_MUTED
} HwdAudioChanMuteT;

	/* audio mode definitions */
#define HWD_AUDIO_MODE_UNCHNG 0xFF /* mode unchanged */

typedef enum {
    HWD_AUDIO_HANDSET_MODE = 0,	   /* Hifi DAC (Diff) handset spkr and mic */
    HWD_AUDIO_MONO_HEADSET_MODE,   /* Hifi DAC (SE, left chan) mono headset with Aux Mic */
    HWD_AUDIO_STEREO_LDSPKR_MODE,  /* Hifi DAC, (SE Left & Right chan) with Ext AMP      */
    HWD_AUDIO_LLINE_HANDSET_MODE,  /* Hifi DAC, landline handset mode (for test), no LNA */
    HWD_AUDIO_MONO_LDSPKR_MODE,    /* Hifi DAC (Rng Diff) loudspeaker "handset speaker"  */
    HWD_AUDIO_STEREO_HEADSET_MODE, /* Hifi DAC (SE Left & Right chan) stereo headset     */
    HWD_AUDIO_NO_MIC_STEREO_HEADSET_MODE, /* Hifi DAC (SE R&L) stereo headset w/ handset MIC */

    HWD_AUDIO_EDAI_HANDSET_MODE,   /* Ext Digital codec, handset mode */
    HWD_AUDIO_EDAI_HEADSET_MODE,   /* Ext Digital codec, headset mode      */
    HWD_AUDIO_EDAI_LDSPKR_MODE,    /* Ext Digital codec, loudspeaker mode  */
    HWD_AUDIO_EDAI_BT_MODE,        /* Ext Digital codec, Bluetooth mode    */
    HWD_AUDIO_EDAI_NO_MIC_HEADSET_MODE,  /* Ext Digital codec, No-MIC headset with handset MIC */
    HWD_AUDIO_EDAI_HAC_MODE,        /* Ext Digital codec, Hearing Aid Compatibility mode   */
    HWD_AUDIO_EDAI_TTY_MODE,        /* Ext Digital codec, TTY mode */
    HWD_AUDIO_EDAI_TTY_VCO_MODE,    /* Ext Digital codec, TTY Voice Carry Over (TX voice)  */
    HWD_AUDIO_EDAI_TTY_HCO_MODE,    /* Ext Digital codec, TTY Hearing Carry Over (TX TTY)  */

    HWD_AUDIO_TTY_MODE,            /* TTY: 2-way TTY, use Hifi DAC Headset */
    HWD_AUDIO_TTY_VCO_MODE,        /* TTY: Voice Carry Over (TX voice), use Hifi DAC */
    HWD_AUDIO_TTY_HCO_MODE,        /* TTY: Hearing Carry Over (TX TTY), RX voice to Hifi handset */
    HWD_AUDIO_TTY_I2S_MODE,            /* TTY: 2-way TTY, output on I2S  */
    HWD_AUDIO_TTY_VCO_I2S_MODE,        /* TTY: Voice Carry Over (TX voice), output on I2S */
    HWD_AUDIO_TTY_HCO_I2S_HANDSET_MODE,/* TTY: Hearing Carry Over (TX TTY), RX voice to I2S Handset */
    HWD_AUDIO_TTY_HCO_I2S_LDSPKR_MODE, /* TTY: Hearing Carry Over (TX TTY), RX voice to I2S Ldspkr  */

    HWD_AUDIO_I2S_HANDSET_MODE,        /* I2S Handset, bypasses Hifi DAC */
    HWD_AUDIO_I2S_HEADSET_MODE,        /* I2S Headset, bypasses Hifi DAC */
    HWD_AUDIO_I2S_NO_MIC_HEADSET_MODE, /* I2S Headset with I2S Handset MIC, bypasses Hifi DAC */
    HWD_AUDIO_I2S_LDSPKR_MODE,         /* I2S Ldspkr,  bypasses Hifi DAC */
    HWD_AUDIO_I2S_BLUETOOTH_MODE,      /* I2S Bluetooth, bypasses Hifi DAC */

    HWD_AUDIO_PC_MODE,             /* 1X Voice to/from PC (USB)      */
    HWD_AUDIO_BLUETOOTH_ON_CP_MODE,/* Bluetooth on CP mode */
    HWD_AUDIO_LDSPKR_HEADSET_MODE, /* Both loudspeaker and headset simultaneously */
    HWD_AUDIO_VADC_IN_I2S_OUT_MODE,/* VADC MIC in, I2S out (to AP) - used for voice record only */

		/*
		 *	add new modes here, and modify tables in
		 *	hwdaudio.c to handle new setups and volume controls
		 *	for each new mode
		 */

    HWD_AUDIO_NUM_MODES		/* must be last; used for loops and arrays */
} HwdAudioModeT;

    /* volume mode definitions: each volume type requires different
     *                          volume messages and parameters */
typedef enum
{
    HWD_AUDIO_VOICE_VOL_MODE     = 0x01,
    HWD_AUDIO_MUSIC_VOL_MODE     = 0x02,  /* All music except MIDI */
    HWD_AUDIO_MIDI_VOL_MODE      = 0x04,
    HWD_AUDIO_DTMF_VOL_MODE      = 0x08,
    HWD_AUDIO_TONE_VOL_MODE      = 0x10,
    HWD_AUDIO_FM_VOL_MODE        = 0x40,
    HWD_AUDIO_VOC_MUSIC_VOL_MODE = 0x80
} HwdVolumeModeT;

	/* Audio devices id used to build logical paths (bitmask) */
#define	HWD_AUDIO_DEV_SPKR	0x01
#define	HWD_AUDIO_DEV_MIC	0x02

	/* available tone generators, per device (mic/spkr) */
typedef enum {
	HWD_AUDIO_VOICE_0 = 0,
	HWD_AUDIO_VOICE_1,
	HWD_AUDIO_VOICE_2,
	HWD_AUDIO_VOICE_3,
	HWD_AUDIO_VOICE_4,
	HWD_AUDIO_VOICE_5,

	HWD_AUDIO_VOICE_NUM	/* max is defined in ipcapi.h as IPC_NUM_TONE_GEN */
} HwdAudioVoiceT;
 
/* Power saving Hifi DAC input modes */
typedef enum
{
   HWD_AUDIO_VDAC_INPUT_DSPV,
   HWD_AUDIO_VDAC_INPUT_FM,
   HWD_AUDIO_VDAC_INPUT_I2S,
   HWD_AUDIO_VDAC_NUM_INPUTS
} HwdAudioVDacInputModeT;

/* tone data */
typedef struct {
	uint16			Freq;	/* tone frequency (Hz) */
	uint16			Dur;	/* tone duration (ms) */
	HwdAudioVoiceT	Voice;	/* tone generator involved */
	HwdAudioVolT	Vol;	/* tone volume (1...10) */
} HwdAudioToneT;

/* Hifi DAC sampling rates */
typedef enum
{
    DAC_SAMPLE_RATE_8K,
    DAC_SAMPLE_RATE_11p025K,  /* 11.025K */
    DAC_SAMPLE_RATE_12K,
    DAC_SAMPLE_RATE_16K,
    DAC_SAMPLE_RATE_22p05K,   /* 22.05K  */
    DAC_SAMPLE_RATE_24K,
    DAC_SAMPLE_RATE_32K,
    DAC_SAMPLE_RATE_44p1K,    /* 44.1K   */
    DAC_SAMPLE_RATE_48K,
    NUM_DAC_SAMPLING_RATES
} DacSamplingRatesT;

/* Definitions for audio loopback */
typedef enum
{
    HWD_AUDIO_LOOPBACK_DISABLED,
    HWD_AUDIO_HARDWARE_LPBK_EN,
    HWD_AUDIO_SOFTWARE_LPBK_EN
} HwdAudioLoopbackTypeT;


/*----------------------------------------------------------------------------
 Global Function Prototypes
----------------------------------------------------------------------------*/
/* Audio mode functions */
extern HwdAudioModeT HwdAudioModeGet( void );
extern void          HwdAudioModeSet( HwdAudioModeT Mode, HwdVolumeModeT VolMode, 
                                      ExeRspMsgT *RspInfo );
extern bool          HwdAudioIsModeSupported(HwdAudioModeT Mode);

/* Audio path on/off */
extern uint16        HwdAudioPathGet( void );
extern void 		 HwdAudioPathSet( bool Enabled, uint16 LogicalPath );

/* Volume API's */
extern void 		 HwdAudioVolumeSet( uint16 LogicalPath, HwdAudioVolT Volume );
extern void          HwdAudioToneVolSet( HwdAudioVolT Volume );
extern void 		 HwdAudioVolumeDataSet( uint16 LogicalPath, HwdAudioVolT Volume );
extern void          HwdAudioDisableSidetone (void);
extern void          HwdAudioToneVolumeLow (bool VolLow);
extern void          HwdAudioRingerVolumeHigh (bool VolHi);
extern void          HwdAudioRingerVolumeConstant (bool VolConstant);
extern HwdAudioVolT	 HwdAudioVolumeGet( uint16 LogicalPath );
extern void          HwdAudioMusicMute (bool Mute);
extern void          HwdAudioSetSpkrChanMute (HwdAudioChanMuteT ChanMute);
extern HwdAudioChanMuteT HwdAudioGetSpkrChanMute (void);

/* Misc */
extern HwdAudioLoopbackTypeT  HwdAudioGetLoopbackState (void);
extern void          HwdAudioFMEnable (bool Enable);
extern void          HwdAudioI2sEnable (bool Enable, DacSamplingRatesT SamplingRate);
extern uint16        HwdAudioGetExtDacPowerupTime (void);

extern void          HwdAudioL1DTstGetPhoneStatus( uint16 *TxMuted, uint16 *RxMuted );

extern void          HwdAudioVibrateStart (uint32 VibrateTime, 
                               uint32 RestTime, int32 TotDuration);
extern void          HwdAudioVibrateStop (void);

extern void          HwdExtAmpPause (uint16 HwState);

/* Audio Tuning prototypes */
extern void HwdAudioInitTuningParams (ExeRspMsgT *RspInfoP, uint8 DataSource, char *PathName);
extern void HwdAudioReadTuningParams (ExeRspMsgT *RspInfoP, uint8 ParamType, bool ReadDefaults);
extern void HwdAudioUpdateTuningParams (ExeRspMsgT *RspInfoP, uint8 ParamType, uint16 Size, uint8 Mode, uint8 *DataP);
extern void HwdAudioStoreTuningFile (ExeRspMsgT *RspInfoP, char *PathName);
extern void HwdAudioSetModeCfgReqd (void);

#endif /* _HWD_AUDIO_API_H_ */

