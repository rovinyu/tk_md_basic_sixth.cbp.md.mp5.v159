/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE. 
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/


#ifndef __CC_SYS_COMM_H__
#define __CC_SYS_COMM_H__

#include "../shared/cross_core_umoly/global_cc_sys_comm_id.h"
#include "../shared/cross_core_umoly/global_cc_rpc_op_id.h"
#include "../shared/cross_core_umoly/global_cc_sync_func_id.h"

typedef void (*CC_IRQ_SYS_CALLBACK_P)(uint32 para0, uint32 para1, uint32 para2);

extern int32 cc_sys_comm_channel_init(uint32 comm_id, CC_IRQ_SYS_CALLBACK_P funcp);
extern int32 cc_sys_comm_channel_deinit(uint32 comm_id);
extern int32 cc_sys_comm_tx(uint32 comm_id, uint32 para0, uint32 para1);
extern int32 cc_sys_comm_tx_polling(uint32 comm_id, uint32 para0, uint32 para1);

/*for CC IRQ MSG*/
#define GlMod_ID_T  uint32
extern int32 cc_irq_msg_send(GlMod_ID_T SrcModId, GlMod_ID_T DestModId, uint32 MsgId, void *MsgBufferP, uint32 MsgSize);

/* for CC IRQ RPC */
/* TX side */
extern int32 cc_irq_rpc_call(uint32 rpc_opid, uint32 input_para_struct_len, void *input_para_struct_addr, uint32 output_para_struct_len, void *output_para_struct_addr);
/* RX side */
typedef void (*CC_IRQ_RPC_CALLBACK_P)(uint32 rpc_buffer_index);
extern int32 cc_irq_rpc_register_callback(uint32 rpc_opid, CC_IRQ_RPC_CALLBACK_P funcp);
extern int32 cc_irq_rpc_get_input_param(uint32 rpc_buffer_index, uint32 input_para_struct_len, void *input_para_struct_addr, uint32 core_type);
extern int32 cc_irq_rpc_put_output_param(uint32 rpc_buffer_index, uint32 ret_value, uint32 output_para_struct_len, void *output_para_struct_addr, uint32 core_type);

/* for CC IRQ SYNC */
typedef void (*CC_IRQ_SYNC_USER_CALLBACK_P)();
/* function registered by user to execute before sending ping packet to MD1. */
extern int32 cc_irq_sync_register_tx_cb(uint32 func_id, CC_IRQ_SYNC_USER_CALLBACK_P funcp);
/* function registered by user to execute after receiving ack packet from MD1 */
extern int32 cc_irq_sync_register_rx_cb(uint32 func_id, CC_IRQ_SYNC_USER_CALLBACK_P funcp);
#endif /*__CC_SYS_COMM_H__*/

