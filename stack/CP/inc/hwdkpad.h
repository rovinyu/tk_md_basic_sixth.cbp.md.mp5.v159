/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
/**************************************************************************************************
* %version: 3 %  %instance: HZPT_2 %   %date_created: Fri Mar 23 16:08:24 2007 %  %created_by: jingzhang %  %derived_by: jingzhang %
**************************************************************************************************/

/*****************************************************************************
* 
* FILE NAME   :     hwdkpad.h
*
* DESCRIPTION :     all hardware keys defintion.
*
* HISTORY     :     See Log at end of file
*
*****************************************************************************/
#ifndef _HWD_KEYPAD_H_
#define _HWD_KEYPAD_H_

#include "sysdefs.h"
#include "sysKeydef.h"

/* define the max rows and cols for keypad, do not modify it */
#define HWD_KEYPAD_MAX_ROWS             (0x08)
#define HWD_KEYPAD_MAX_COLS             (0x08)

#define HWD_KPD_CTRL_6x4_EN             0x06        /* Enables 6x4 keypad          */
#define HWD_KPD_CTRL_6x5_EN             0x46        /* Enables 6x5 keypad          */
#define HWD_KPD_CTRL_8x8_EN             0x92        /* Enables 6x5 keypad          */

/* define hardware status for key */
typedef enum
{
   HWD_KEY_PRESS = 0,
   HWD_KEY_RELEASE,
   HWD_KEY_HOLD,
   HWD_KEY_STATUS_NUM
}HwdKeyStatusT;

typedef PACKED_PREFIX struct
{
   HwdKeyStatusT   Status;
   SysKeyIdT       KeyId;
} PACKED_POSTFIX HwdKeypadMsgT;

typedef void (*HwdKeypadCallbackFuncT)(HwdKeyStatusT Status, SysKeyIdT KeyId);

/*****************************************************************************
    FUNCTION NAME: HwdKeypadInit

    DESCRIPTION:
        This method is used to initialize keypad hardware(include extern keys out
        of keypad).

    PARAMETERS:
        Column          if it is 5, then the keypad in CBP is 6 x 5.
                        other value, the the keypad in CBP is 6 x 4.

        MultiKey        if it is TRUE, then multi key mode, otherwise single key
                        mode.
                        
        KeyHoldDuration Key hold time, unit is 100ms. if 0, there is no hold status.

        CallbackFuncP   Point to the callback function registered by val, it is 
                        used to process the keypad events.

    RETURNED VALUES:
        None.
*****************************************************************************/
void HwdKeypadInit(uint8 Column, bool MultiKey, uint8 KeyHoldDuration, HwdKeypadCallbackFuncT CallbackFuncP);   
              
/*****************************************************************************
    FUNCTION NAME: HwdKeypadGetFlipKeyStatus

    DESCRIPTION:
        This method is to get the hardware status of flip key.

    PARAMETERS:
        None.

    RETURNED VALUES:
        None.

    Caution:
        do not change the function name and prototype. it is refrenced  
        by valkeypad.c
*****************************************************************************/
HwdKeyStatusT HwdKeypadGetFlipKeyStatus(void);
/*****************************************************************************
    FUNCTION NAME: HwdKeypadGetPwrKeyStatus

    DESCRIPTION:
        This method is to get the hardware status of power key.

    PARAMETERS:
        None.

    RETURNED VALUES:
        None.

    Caution:
        do not change the function name and prototype. it is refrenced  
        by valkeypadcust.c
*****************************************************************************/
HwdKeyStatusT HwdKeypadGetPwrKeyStatus(void);

/*****************************************************************************
    FUNCTION NAME: HwdKeypadNotify

    DESCRIPTION:
        The method is to send message to hwd task.

    PARAMETERS:
        None

    RETURNED VALUES:
        None.
*****************************************************************************/
void HwdKeypadNotify(HwdKeyStatusT KeyStatus, SysKeyIdT KeyId); 

extern void HwdKeypadSetTtyState(bool On_Off);

extern void HwdKeypadInitCust(void);    /* Ext Key Init function defined in hwdkeypadcust.c */
extern void	HwdKeypadLisr(void);

/*****************************************************************************
    FUNCTION NAME: HwdKeypadMultikeyEn

    DESCRIPTION:
        The routine can enable or disable keypad driver support multi-key mode. After call the routine, after
         all key are released, then the new setting will work.

    PARAMETERS:
        En, TRUE or FALSE.

    RETURNED VALUES:
        None.
*****************************************************************************/
void HwdKeypadMultikeyEn(bool En);

#endif
/*****************************************************************************
* END OF FILE
*****************************************************************************/

/**Log information: \main\3 2012-03-12 08:13:19 GMT hzhang
** change keypad column for CBP8.0**/
