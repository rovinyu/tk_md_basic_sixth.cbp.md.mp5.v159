/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2006-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef  _DO_QOS_H_
#define  _DO_QOS_H_
/*****************************************************************************
* 
* FILE NAME   :   do_Qos.h
*
* DESCRIPTION :   This file contains public QOS definitions
*
* HISTORY     :
*     See Log at end of file
*
*****************************************************************************/

/*----------------------------------------------------------------------------
* Include Files
----------------------------------------------------------------------------*/
#include "sysdefs.h"
#include "do_rcpapi.h"
/*----------------------------------------------------------------------------
Defines And Macros
----------------------------------------------------------------------------*/
#define MAX_RESERVATION_LABEL          0x100
#define MAX_SUPPORTED_IPFLOW             2
#define MAX_SUPPORTED_QOS_BLOB_NUM 16
#define MAX_SUPPORTED_RESERVATION_NUM      15
#define MAX_QOS_PROFILE_NUM    16
#define MAX_QOSPROFILE_COUNT      10
#define MAX_CLIENT_NUM         6
#define MAX_TFT_NUM              12
#define DEFAULT_PPP_LABEL        0xff
#define DEFAULT_PPPFREE_LABEL    0xfe

typedef enum
{
   SPARE_TFT,
   CREATE_NEW_TFT,
   DELETE_TFT,
   ADD_PKTFILTER_TO_TFT,
   REPLACE_PKTFILTER,
   DELETE_PKTFILTER,
   QOS_CHECK,
   INITIAL_FLOW_REQ = 0x80,/*HSGW to UE*/
   QOS_CHECK_CONFIRM,
   HSGW_DEL_PKTFILTER,
   HSGW_REPLACE_PKTFILTER,
   UE_DEL_IDENTIFIED_PKTFILTER
   
}TftOperationCodeT;

typedef enum
{
  FLOW_BI,
  FLOW_REV,
  FLOW_FWD
}RlpFlowDirection;

typedef enum
{
  FLOW_PPPBESTEFORT,
  FLOW_PPPFREE,/*PPP Free Operation*/
  FLOW_SIP,
  FLOW_RTP_AUDIO_RS1, /*profileID=0x100*/
  FLOW_RTP_VIDEO_32K, /*profileID=0x301*/
  FLOW_RTP_AUDIO_RS2, /*profileID=0x101*/
  FLOW_RTP_VIDEO_48K, /*profileID=0x303*/
  FLOW_RTP_VIDEO_64K, /*profileID=0x305*/
  INVALID_FLOWTYPE  
}QoSFlowTypeT;

typedef enum
{
  ProtoNull,
  ProtoHDLC,
  ProtoIPv4,
  ProtoIPv6,
  ProtoROHC,
  ProtoIPv4andIPv6
}FlowProtoTypeT;

typedef enum
{
  QOS_OP_SUCCESS,
  QOS_OP_REJECT,
  QOS_OP_TIMEOUT,
  QOS_OP_FAIL
}QoSOpResultT;


typedef  PACKED_PREFIX struct
{
   uint8  ReservationLabel;
   uint8  Priority;
   uint8  QoSSetNum;
   uint8  QosAttrSetId[MAX_SUPPORTED_QOS_BLOB_NUM];/*7 bits,should not be zero*/
/*   uint8  Verbose;
   uint8  RlpNum;
   uint8  MacFlowId;*/
   QoSFlowTypeT          FlowType;
} PACKED_POSTFIX QosSubBlobT;

typedef  PACKED_PREFIX struct
{
   bool    bCurSupported;
   uint16  FlowProfileID;
/*   uint8   Verbose;
   uint8   TrafficClass;
   uint16  PeakRate;
   uint16  BucketSize;
   uint16  TokenRate;
   uint8   MaxLatency;
   uint8   MaxIPPacketLossRate;
   uint8   PacketSize;
   uint8   DelayVarSensitive;*/
} PACKED_POSTFIX QosAttributeT;

typedef  PACKED_PREFIX struct
{
  uint8 Num;
  QosAttributeT  QosAttribute[MAX_SUPPORTED_QOS_BLOB_NUM];
} PACKED_POSTFIX QosAttrProfileT;

typedef  PACKED_PREFIX struct
{
  bool    bIpV4;
  uint8   PrecedenceIndex;/*evaluation precedence index*/
  /*uint32  SrcAddress[4]; 
  uint32  SrcAddrMask[4];
  uint16  DstPortRange[2];
  uint16  SrcPortRange[2];*/
  uint16  SrcPortNum;/*For Forward flow, it's DstPortNum*/
  uint8   ProNumOrNHdr;
  uint32  Spi;/*ipsec security parameter index*/
  uint8   TrafficClass; /*for ipv4, it's tos*/
  uint8   TrafficMask;
  uint32  FlowLabel;/*valid for IPv6 only*/
} PACKED_POSTFIX QoMPktFilterContentT;

typedef  PACKED_PREFIX struct
{
  bool                  bUsed;
  QosSubBlobT           QosBlob;
/*  ProtoTypeT     ProtoType;
  uint16         PortNum;
  PayLoadTypeT   PayloadType;
  FlowProtoTypeT L2P;*//*higher Layer Protocol,0 NULL,1 Octet-based HDLC-like framing
                       2 Internet Protocol version4, 3 IP version 6, 4 RoHC, 5 IPv4 and IPv6*/
  QoMPktFilterContentT  PktFilter;
} PACKED_POSTFIX QosSubScriberProfileT;

typedef PACKED_PREFIX struct
{
  uint8   num;
  QosSubScriberProfileT   SubScribProfile[MAX_SUPPORTED_RESERVATION_NUM];
} PACKED_POSTFIX QosSubScriberTabelT;


typedef PACKED_PREFIX struct
{
   QosAttrProfileT             ATProfile;
   QosSubScriberTabelT    RevSubTable;
   QosSubScriberTabelT    FwdSubTable;
} PACKED_POSTFIX QoSProfileT;

typedef struct
{
  uint8   ProfileCount;
  uint8   ProfileLength[MAX_QOSPROFILE_COUNT];
  uint16  Profile[MAX_QOSPROFILE_COUNT][MAX_QOS_PROFILE_NUM];
}SupportedQosProfileT;

typedef struct
{
  uint8 ReservationLabel[MAX_RESERVATION_LABEL];
  uint8 DosAllowed[MAX_RESERVATION_LABEL];
  uint8 ReservationCount;
 /* FlowProtoTypeT  FlowProtocol;
  uint8 ResvState[MAX_RESERVATION_LABEL];
  uint8 RlpState;
  uint8 HighLayerProtocol;*/
}RlpFlowInfoT;

typedef  struct
{
  uint8   ResvLabel;
  QoMPktFilterContentT  Content;
}QoMPktFilterT;

typedef  struct
{
  uint8          Direction;
  uint8          PktFilterNum;
  QoMPktFilterT  FilterList[MAX_CLIENT_NUM];
}QoMTFTInfoT;

typedef struct
{
   QoSFlowTypeT         FlowType;
   uint8                  Priority;
   uint8                  ProfileNum;
   uint16                ProfileID[4];  
   QoMPktFilterContentT  PktFilter;
}HlpQoSFlowInfoT;

typedef struct
{
  QoSOpResultT  result;
}HlpQOMOpFlowRspMsgT;

typedef struct
{
  uint8 Label;
  uint8 Direction;
}HlpQOMOpFlowRelIndMsgT;

typedef struct
{
  uint8 Num;
  uint8 Label[MAX_CLIENT_NUM];
}HlpQOMFlowIndMsgT;


typedef void (*MsgDepatchT)(uint8* pData, uint16 size, uint8 PdnId);
/****************************************************************************/
void   QOMQosTableInit(void);
bool   QOMRlpUpdateFwdQosTable(uint8 ResvLabel, uint16 len, uint8* pProfile);
bool   QOMRlpUpdateRevQosTable(uint8 ResvLabel, uint16 len, uint8* pProfile);

void   QOMRlpUpdateQoSProfile(SupportedQosProfileT* pProfile);
uint8 QoMGetRevRlpNumByLabel(uint8 ResvLabel);
uint8 QoMGetFwdRlpNumByLabel(uint8 ResvLabel);
uint8 QoMGetFwdLabelByRlpFlow(uint8 nRlpFlow);
uint8 QoMGetRevLabelPriority(uint8 ResvLabel);
uint8 QoMGetResvLabelByMatchingTFT(uint8* startingAddr, uint16 frameType);
void  QOMRlpReservationOnRsp(QoSOpResultT Result);
void  QOMRlpReservationOffRsp(QoSOpResultT Result);
void  QOMRlpQoSNegoFail(void);
void  QOMRlpFwdQosRelease(uint8 Label);
void  QOMRlpRevQosRelease(uint8 Label);
void QOMSetRevLabelRlpNum(uint8 Label, uint8 RlpNum);
void QOMSetFwdLabelRlpNum(uint8 Label, uint8 RlpNum);

bool QOMSetQoSAttributeSet(uint8 Direction,QosAttrProfileT* pProfile);
bool QOMSetQoSProfile(QosAttrProfileT* pProfile);
bool QOMSetSubscribQoSProfile(uint8 Direction,QosSubScriberProfileT* pSubProfile);
bool QOMGetNegQoSProfile(uint8 Label, uint8 Direction, QosSubBlobT* pProfile);
bool HlpQOMFlowRequest(uint8 FlowType, uint8 Direction,uint8* pLabel);
#if 0
bool HlpQOMFlowRelease(uint8 Label, uint8 Direction);
#endif
bool HlpQOMReleaseQoS(uint8* pLabel, uint8* pDirection, uint8 Num, uint8 TftOpCode);
void HlpQOMFlowDeactivate(uint8 num,uint8 *pLabel, uint8 *pDirection);
bool QOMSetQoSTFT(QoMTFTInfoT* pTft);

void  QOMWriteDataToFile(void);
void QOMGetSubQosProfile(QoSProfileT* pQoSData);

void QOMEncReservationQoSRequestAttr(uint16* bitPos,uint8* pData);
void QOMClearSubscribQoSProfile(void);
MsgDepatchT QOMDepatchRecvdPkt(uint16 DstPort);
bool QOMCheckFwdTft(uint16 DstPortNum);
void QOMCloseUdpConnection(void);
void QOMActivate(bool flag);
QosSubScriberProfileT*  QOMGetSubScribeQoSByProfileId(uint16 ProfileID);
void QOMUpdateFwdSubScribTable(QoMPktFilterContentT *pTft, QosSubBlobT *pBlob, uint8 PdnId);
QoMPktFilterContentT*  QOMGetTftByLabel(uint8 Label);
void QOMSetFlowPortNum(uint8 ReservationLabel, uint16 PortNum);

#ifdef CBP7_EHRPD
void  HlpQOMSetRevFlowPath(uint8 RlpFlowNum, uint8 PathType);
uint8 HlpQOMGetRevFlowPath(uint8 RlpFlowNum);
void  HlpQOMSetFwdFlowPath(uint8 RlpFlowNum, uint8 PathType);
uint8 HlpQOMGetFwdFlowPath(uint8 RlpFlowNum);
void HlpQoMInitSubQoSTableWithPdnTable(void);

#endif

/*****************************************************************************
* End of File
*****************************************************************************/
#endif


