/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 1998-2011 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
/*****************************************************************************
 
  FILE NAME:  lmdmsg.h

  DESCRIPTION:

    This file contains the defenitions of all the LMD msg id's.

*****************************************************************************/

/*
** Messages of LmdCmdMsgIdT type
** should be sent to the LMD_CMD_MAILBOX.
*/



#ifndef MSG_ID_MISMATCH_DETECT


// regular msg id's enum
#define MSGID_SET(name, val) name = val
#define MSGID_NEXT(name) name
#define MSG_IDS typedef enum
#define LMD_MSG_IDS_NAME LmdCmdMsgT

extern const uint32 LmdCmdValidMsgIdList[];
extern uint32 LmdCmdValidMsgIdListSizeOf(void);


#else


#ifdef  HWD_MSG_ID_MISMATCH_DETECT
#define  LMDValidMsgIdList HwdLmdCmdValidMsgIdList
#else
#define  LMDValidMsgIdList LmdCmdValidMsgIdList
#endif

#undef MSGID_SET 
#undef MSGID_NEXT 
#undef MSG_IDS
#undef LMD_MSG_IDS_NAME  

 


#define MSGID_SET(name, val) name 
#define MSGID_NEXT(name) name
#define MSG_IDS const uint32 LMDValidMsgIdList[] =
#define LMD_MSG_IDS_NAME  
#endif




MSG_IDS
{
    /* PSW's commands for call processing*/
    MSGID_SET(LMD_ACCESS_PROBE_ABORT_MSG , 0),
    MSGID_NEXT(LMD_TRAFFIC_CHAN_START_MSG),
    MSGID_NEXT(LMD_TRAFFIC_CHAN_STOP_MSG),
    MSGID_NEXT(LMD_RESET_MSG),

    /* Service Configuration */
    MSGID_SET(LMD_SERVICE_CONFIGURATION_MSG , 0x10),
    MSGID_NEXT(LMD_SERVICE_CONFIG_NN_MSG),
    MSGID_NEXT(LMD_SERVICE_OPTION_CONTROL_MSG),
    MSGID_NEXT(LMD_CONVERSATION_SUBSTATE_CONN_MSG),
    MSGID_NEXT(LMD_CONVERSATION_SUBSTATE_DISC_MSG),
    MSGID_NEXT(LMD_LOOPBACK_SERVICE_OPTION_MSG),
    MSGID_NEXT(LMD_AUDIO_SSO_CONNECT_RSP_MSG),
    MSGID_NEXT(LMD_AUDIO_SSO_DISCONN_RSP_MSG),

    /* Markov/TDSO support */
    MSGID_SET(LMD_CLEAR_MARKOV_STATS_MSG ,0x20) ,   
    MSGID_NEXT(LMD_MARKOV_SERVICE_OPTION_MSG) ,     

    /* Power control message */
    MSGID_SET(LMD_PWR_CTL_PARMS_MSG , 0x30),
 
   /* Audio AGC and Handsfree messages */
    MSGID_SET(LMD_SPCH_DATA_ENC_AGC_MSG , 0x40),
    MSGID_NEXT(LMD_SPCH_DATA_ENC_HFREE_MSG),
    MSGID_NEXT(LMD_SPCH_DATA_DEC_HFREE_MSG),
#ifdef MTK_CBP_ENCRYPT_VOICE
	MSGID_NEXT(LMD_VOICE_ENCRYPT_DISABLE_SPCH_DATA_MSG),
	MSGID_NEXT(LMD_VOICE_ENCRYPT_ENABLE_SPCH_DATA_MSG),
#endif

    /* Voice Activity */
    /* Debug */

    /* For Data services. */
    MSGID_SET(LMD_RLP_REV_FUNDICATED_DATA_REQ_MSG , 0x50),
    MSGID_NEXT(LMD_RLP_REV_SCH_DATA_REQ_MSG),
    MSGID_NEXT(LMD_DCCH_REV_DATA_MSG),
    MSGID_NEXT(LMD_SCCH_REV_DATA_MSG),
    /* Data services:  Fwd SCH and SCCH Ack Response Msg */
    MSGID_NEXT(LMD_SCCH_FWD_DATA_RSP_MSG),

   /* The TDSO TX data msg */ 
    MSGID_SET(LMD_FCH_REV_TDSO_DATA_MSG ,0x60),
    MSGID_NEXT(LMD_DCCH_REV_TDSO_DATA_MSG), 
    MSGID_NEXT(LMD_SCH_REV_TDSO_DATA_MSG),  
    MSGID_NEXT(LMD_SCCH_REV_TDSO_DATA_MSG), 
                   
    /* SCH setup messages */
    MSGID_SET(LMD_FSCH_MSG ,0x70),
    MSGID_NEXT(LMD_RSCH_MSG),
    MSGID_NEXT(LMD_FSCH_BURST_END_MSG),
    MSGID_NEXT(LMD_RSCH_STOP_MSG),
    MSGID_NEXT(LMD_FSCH_PDU_ORDER_CTRL_MSG),

    /* Reverse channel messages */
    MSGID_SET(LMD_REVERSE_ACCESS_SIG_MSG , 0x80),
    MSGID_NEXT(LMD_EACH_SIG_MSG),
    MSGID_NEXT(LMD_REVERSE_TRAFFIC_SIG_MSG),
    MSGID_NEXT(LMD_TRANSMITTER_STATUS_MSG),
    MSGID_NEXT(LMD_COUNTER_SUSPEND_MSG),

    /* Mux PDU support */
    MSGID_SET(LMD_MUXPDU_TEST_MODE_MSG , 0x90),
    MSGID_NEXT(LMD_MUXPDU_BUFFER_STATUS_MSG),
    MSGID_NEXT(LMD_MUXPDU_SET_RAW_SPY_LEN_MSG),
    MSGID_NEXT(LMD_MUXPDU_PDCH_TEST_MODE_MSG),
    MSGID_NEXT(LMD_MUXPDU_SET_OP_MODE_MSG),
    MSGID_NEXT(LMD_MUXPDU_GET_OP_MODE_MSG),

    /* For Diagnostic Monitor support */
    /* Voice quality muting */


    /* Resource Control: FCH and DCCH only */
    MSGID_SET(LMD_RESOURCE_UPDATE_MSG , 0xC0),
    
    /* Sim Message */
    MSGID_SET(LMD_SIM_MSG , 0xF0),

    /* Statistics counter retrieve and set message */
    MSGID_NEXT(LMD_GET_STAT_CNT_MSG),
    MSGID_NEXT(LMD_SET_STAT_CNT_MSG),
    MSGID_NEXT(LMD_RESET_STAT_CNT_MSG),
    MSGID_NEXT(LMD_SET_FER_STATS_NUM_FRAMES_MSG),

	/* Debug Screen Info message */
    MSGID_NEXT(LMD_GET_DEBUG_SCRN_INFO_MSG),

    /* Speech Routing Disable Message */
    MSGID_NEXT(LMD_SPEECH_DISABLE_MSG),
    /* Instruct LMD to stop requesting data from RLP but continue
     * submitting R-Sch Tx data to DSPm.
     */
    MSGID_NEXT(LMD_RLP_STOP_REQ_MSG),
    
    MSGID_NEXT(LMD_CLEAR_FWD_SCH_DTX_STATS_MSG),
#ifdef MTK_PLT_ON_PC_UT     
    /* Added messages for MTK_PLT_ON_PC_UT */
    MSGID_NEXT(UT_LMD_FWD_FUNDICATED_DATA_IND_MSG),
    MSGID_NEXT(UT_LMD_FWD_SCH_DATA_IND_MSG),
    MSGID_NEXT(UT_LMD_REV_DATA_REQ_MSG),
#endif
    MSGID_NEXT(LMD_SPEECH_LOOPBACK_MODE_MSG),
    MSGID_NEXT(LMD_NUM_CMD_MSG_IDS)

   }LMD_MSG_IDS_NAME;
