/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*****************************************************************
  FILE NAME: fsmrfsapi.h

  DESCRIPTION:

    Definitions for remote file system.

 Copyright (c) 2011, VIA Telecom, Inc.

*******************************************************************/
#ifndef FSM_RFS_API_H
#define FSM_RFS_API_H

#ifdef MTK_DEV_CCCI_FS
#define RfsImageForceSync(action)
#else
#include "exedefs.h"
#include "monapi.h"
#include "hwdflash.h"
#include "iopets.h"

/*#define FSM_RFS_DBG*/
#define RFS_RF_DATA_BACKUP_BASE 99 /* PC tool will send  DB flush command use this data base ID to sync RF_IMG to AP NVRAM*/

typedef enum
{
  RFS_IPC_DOWNLOAD_IMAGE_PACKAGE_MSG = IOP_CP_FLASH_PROGRAM_ETS,   /*1202*/
  RFS_IPC_DOWNLOAD_IMAGE_REQ_MSG = 1280,
  RFS_IPC_DEL_IMAGE_FILE_REQ_MSG = 1281,
#ifdef MTK_DEV_VERSION_CONTROL
  /*Add new message id for sync fsm images*/
  RFS_IPC_SYNC_RFS_IMAGE_REQ_MSG = 1290,
#else
  RFS_IPC_SYNC_RFS_IMAGE_REQ_MSG = 1283,
#endif
  RFS_IPC_SYNC_CRASH_LOG_REQ_MSG = 1284,
  RFS_IPC_RFS_FILE_INVALID_MSG   = 1285,
  RFS_IPC_BOOTLOADER_START_UP_MSG= 1286,
  RFS_IPC_BOOTLOADER_FINISH_MSG  = 1287,
  RFS_IPC_BACKUP_RFS_IMAGE_MSG   = 1288,
  RFS_IPC_SYNC_RAM_DUMP_REQ_MSG  = 1289
}RfsIpcMsgIdT;


typedef enum
{
  RFS_SYNC_ACT_TIMER = 0,
  RFS_SYNC_ACT_POWRDOWN = 1,
  RFS_SYNC_ACT_FLUSH = 2
#ifdef MTK_DEV_VERSION_CONTROL
  ,
  RFS_SYNC_ACT_VERSION_MISMATCH = 3
#endif
}RfsImageSyncActionT;

/**********************************************************************
*   define for RFS task
***********************************************************************/

/* Deal with by RFS SYNC task */
#define IOP_TASK_READY_SIGNAL       EXE_SIGNAL_1
#define RFS_IMAGE_SYNC_SIGNAL       EXE_SIGNAL_2
#define RFS_HALT_LOG_ERASE_SIGNAL   EXE_SIGNAL_3

#ifdef MTK_DEV_VERSION_CONTROL
/*Its define must be accordant with AP*/
typedef enum
{
   CP_CODE_IMAGE_TYPE    = 1, /*same with old*/
   CRASH_LOG_IMAGE_TYPE  = 3, /*same with old*/
   FSM_RF_IMAGE_TYPE     = 4,
   FSM_CUST_IMAGE_TYPE   = 5,
   FSM_RW_IMAGE_TYPE     = 6
}RfsDownloadImageT;
typedef PACKED_PREFIX struct
{
   RfsDownloadImageT ImageType;
} PACKED_POSTFIX RfsDownloadImageReqMsgT;
#else
/* RFS_IPC_DOWNLOAD_IMAGE_REQ_MSG msg */
typedef PACKED_PREFIX struct 
{
   HwdFlashSectionTypeT ImageType;    
} PACKED_POSTFIX  RfsDownloadImageReqMsgT;
#endif

/* Define RFS_IPC_DOWNLOAD_IMAGE_REQ_MSG response msg */
typedef PACKED_PREFIX struct 
{
   HwdFlashSectionTypeT ImageType;    
   uint32 ImageSize;
} PACKED_POSTFIX  RfsDownloadImageReqRspMsgT;

/* RFS_IPC_DEL_IMAGE_FILE_REQ_MSG msg */
typedef PACKED_PREFIX struct 
{
   HwdFlashSectionTypeT ImageType;    
} PACKED_POSTFIX RfsEraseImageReqMsgT;

/* Define RFS_IPC_DEL_IMAGE_FILE_REQ_MSG response msg */
typedef PACKED_PREFIX struct 
{
   HwdFlashSectionTypeT ImageType;    
   bool Result;
} PACKED_POSTFIX RfsEraseImageRspMsgT;

typedef PACKED_PREFIX struct 
{
   uint32 ImageLen;    
   uint32 CheckSum;
} PACKED_POSTFIX RfsImageHeaderT;

/*message structure for RFS_IPC_SYNC_RFS_IMAGE_REQ_MSG = 1283 and RFS_IPC_SYNC_CRASH_LOG_REQ_MSG = 1284*/
typedef PACKED_PREFIX struct 
{
  uint32  SeqNum;   /*Ack should return seqnum*/
  uint32  Offset;   /* Offset in bytes from beginning of file */
  uint16  Length;   /* Length of the data field, in bytes;*/
  uint8   Data[1];  /* data buffer, length is defined by Length field */
} PACKED_POSTFIX RfsIpcUploadMsgT;

typedef PACKED_PREFIX struct
{
	uint32 SeqNum;
	uint32 Result;
} PACKED_POSTFIX RfsIpcUploadAckMsgT;


void RfsIpcMgrInit(void);
void RfsIpcWaitIOPRdy(void);
void RfsIpcSignalHandler(ExeEventWaitT RfsSignal);
bool RfsIpcDataTx(uint32 MsgId, void* CmdP, uint32 Len);

bool RfsSyncRxCmdMsg(uint32 MsgId, void* MsgDataP, uint32 MsgLen);

void RfsSyncImageInit(void);
uint32 RfsSyncImageCreate(void);

/* Theis routine should been call before phone powr down. */
void RfsImageForceSync(RfsImageSyncActionT Action);
void RfsSyncReqEraseHaltLog(ExeRspMsgT RspInfo);

void RfsFilesDirtyFlagSet(bool Value);

/*****************************************************************************
  FUNCTION NAME: RfsResetCBP

  DESCRIPTION:

    This routine jump to boot code then restart CP SW, don't run boot rom.

  PARAMETERS:

    none
    
  RETURNED VALUES:

    none

*****************************************************************************/
void RfsResetCBP(void);


/*****************************************************************************
  FUNCTION NAME: RfsImageBackup

  DESCRIPTION:

    This routine informs AP to backup current RFS image for RF calibration data backup.
    The routine will return until AP send the ACK for back up command.

  PARAMETERS:

    none
    
  RETURNED VALUES:

    none

*****************************************************************************/
void RfsImageBackup(void);
#endif

#endif

/*********************************************************************************/




/**Log information: \main\Thin_Modem\1 2012-02-14 07:48:00 GMT zlin
** HREF#0000, Add Tiantai Plus driver code.**/
/**Log information: \main\Trophy\Trophy_zlin_href22058\1 2013-03-22 08:37:14 GMT zlin
** HREF#22058, Add RFS image upload action feild.**/
/**Log information: \main\Trophy\1 2013-03-22 08:55:16 GMT hzhang
** HREF#22058 to eanble RAM dump to APand add RFS image upload action feild.**/
