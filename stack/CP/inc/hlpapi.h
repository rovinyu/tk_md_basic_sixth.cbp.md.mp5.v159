/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.
*
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
*
* Copyright (c) 1998-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef _HLPAPI_H_
#define _HLPAPI_H_
/*****************************************************************************
*
* FILE NAME   : hlpapi.h
*
* DESCRIPTION :
*
*     This include file provides system wide global type declarations and
*     constants
*
* HISTORY     :
*     See Log at end of file.
*
*****************************************************************************/

/*----------------------------------------------------------------------------
 Include Files
----------------------------------------------------------------------------*/
#include "sysdefs.h"
#include "exeapi.h"
#include "pswcustom.h"
#include "valatdata.h"
#include "cpbuf.h"
#include "iopapi.h"
#ifdef MTK_PLT_ON_PC_UT
#include "../hlp/hlput.h"
#endif /* MTK_PLT_ON_PC_UT */
#include "sbpapi.h"

#define EAP_AKA_PRIME 1

/*----------------------------------------------------------------------------
 Global Defines and Macros
----------------------------------------------------------------------------*/
#define HLP_MAILBOX_CMD                  EXE_MAILBOX_1_ID
#define HLP_MAILBOX2_CMD                 EXE_MAILBOX_2_ID
#define HLP_SEC_MAILBOX                  EXE_MESSAGE_MBOX_3
#define HLP_SEC_CMD                      EXE_MAILBOX_3_ID

#define HLP_STARTUP_SIGNAL               EXE_SIGNAL_1
#define HLP_FWD_DATA_IND_SIGNAL          EXE_SIGNAL_2
#define HLP_REV_DATA_IND_SIGNAL          EXE_SIGNAL_3
#define HLP_PPP_HA_DEC0_COMPLETE         EXE_SIGNAL_4
#define HLP_PPP_HA_ENC0_COMPLETE         EXE_SIGNAL_5
#define HLP_PPP_HA_DEC1_COMPLETE         EXE_SIGNAL_6
#define HLP_PPP_HA_ENC1_COMPLETE         EXE_SIGNAL_7
#define HLP_REV_DATA_DORM_SIGNAL         EXE_SIGNAL_8
#define HLP_DO_HO_1X_SIGNAL              EXE_SIGNAL_9
#ifdef CBP7_EHRPD
#define HLP_FWD_PKT_DATA_IND_SIGNAL      EXE_SIGNAL_10
#endif
/* defined for rtp input simulator, which will utilize the IOP
*  pcm voice channel */
#define HLP_RTP_IS_PACKET_IND_SIGNAL	 EXE_SIGNAL_11
#define HLP_SIP_IS_PACKET_IND_SIGNAL     EXE_SIGNAL_12

#define HLP_MAX_SIZE_DATA                240
#define HLP_MAX_USRID_LEN                72
#define HLP_MAX_PSWD_LEN                 16
#define HLP_MAX_NUM_REV_RLP              300
#define HLP_MAX_BWSR_DIG_LEN             (CP_MAX_CALLING_PARTY_NUMBER_SIZE +1)
#define HLP_MAX_SPY_LEN                  164
#define MAX_NUM_DO_INACTIVITY_TICKS      4500    /* 4500 x 6.67ms = 30 sec */
#define MAX_NUM_1X_INACTIVITY_TICKS      30      /* second */

#define HLP_AKA_KEY_LEN                  16 /*128 bits 3G TS 33.105.v.3.0(2000-03)*/
#define HLP_AKA_RAND_LEN                 16 /*128 bits 3G TS 33.105.v.3.0(2000-03)*/
#define HLP_AKA_FMK_LEN                  4  /*32 bits */
#define HLP_AKA_SQN_LEN                  6  /*48 bits 3G TS 33.105.v.3.0(2000-03)*/
#define HLP_AKA_AMF_LEN                  2  /*16 bits 3G TS 33.105.v.3.0(2000-03)*/
#define HLP_AKA_MACA_LEN                 8  /*64 bits 3G TS 33.105.v.3.0(2000-03)*/
#define HLP_AKA_MACS_LEN                 8  /*64 bits 3G TS 33.105.v.3.0(2000-03)*/
#define HLP_AKA_RES_LEN                  16 /*32<->128 bits 3G TS 33.105.v.3.0(2000-03)*/
#define HLP_AKA_CK_LEN                   16 /*128 bits 3G TS 33.105.v.3.0(2000-03)*/
#define HLP_AKA_IK_LEN                   16 /*128 bits 3G TS 33.105.v.3.0(2000-03)*/
#define HLP_AKA_AK_LEN                   6  /*48 bits 3G TS 33.105.v.3.0(2000-03)*/
#define HLP_AKA_AKS_LEN                  6  /*48 bits 3G TS 33.105.v.3.0(2000-03)*/
#define HLP_AKA_UAK_LEN                  16
#define HLP_AKA_AUTN_LEN                 16
#define HLP_AKA_AUTS_LEN                 16
#define HLP_AKA_OP_LEN                   16
#define HLP_AKA_SEQ_ARRAY_SIZE           32

#define EAP_AKA_NONCE_S_LEN              16
#define EAP_AKA_NONCE_MT_LEN             16
#define EAP_AKA_MAC_LEN                  16
#define EAP_AKA_K_AUT_LEN                32
#define EAP_AKA_K_ENCR_LEN               16
#define EAP_AKA_MSK_DATA_LEN             64
#define EAP_AKA_IV_LEN                   16

#define EAP_AKA_RAND_LEN                 16
#define EAP_AKA_AUTN_LEN                 16
#define EAP_AKA_RES_MAX_LEN              16
#define EAP_AKA_MK_LEN                   20
#define EAP_AKA_MSK_LEN                  64
#define EAP_AKA_AUTS_LEN                 14
#define EAP_AKA_IK_LEN                   16
#define EAP_AKA_CK_LEN                   16
#define EAP_AKA_SHA1_MAC_LEN             20

#define EAP_AKA_PSEUDONYM_LEN            128
#define EAP_AKA_REAUTH_ID_LEN            128
#define EAP_AKA_KC_LEN                   8

#if EAP_AKA_PRIME
#define EAP_AKA_K_RE_LEN                 32
#define EAP_AKA_EMSK_DATA_LEN            64
#define EAP_AKA_PRIME_MK_LEN             208
#endif

#define HLP_MAX_SOCKET                   10

#define HLP_MN_PASSWD_MAX_SIZE           HLP_MAX_PSWD_LEN
#define HLP_MN_NAI_MAX_SIZE              HLP_MAX_USRID_LEN
#define HLP_BETTER_SYSTEM_THRESH         20
#define MN_PASSWD_MAX_SIZE               HLP_MAX_PSWD_LEN
#define MN_NAI_MAX_SIZE                  HLP_MAX_USRID_LEN
#define MN_AUTH_MAX_SIZE                 3
#define HLP_NUM_DIAL_STRING_DIGITS       11

#ifdef CBP7_EHRPD
#define MAX_HLP_FLOW_SUPPORTED           8  /* 6 */ /*Must be same as MAX_RLP_FLOW_SUPPORTED*/
#define MAX_PROTPARMSPROFILE_COUNT       8
#define MAX_RLP_FRAMES_PER_IP_PKT        32
#define NUM_HLP_FWD_IP_DATA_IND_ENTRIES  16
#endif

#define HSPD_PRI

#define HLP_MAX_HSPD_PROFILES            8

#define HLP_HSPD_DB_PAD_SIZE             57
#define HLP_HSPD_PROFILE_PAD_SIZE        128
#define HLP_EHRPD_DB_PAD_SIZE            47

#ifdef CBP7_EHRPD
#define HLP_HSPD_SECURE_DB_PAD_SIZE      114
#else
#define HLP_HSPD_SECURE_DB_PAD_SIZE      130
#endif
#define HLP_HSPD_SECURE_PROFILE_PAD_SIZE 64

#define VAL_UIM_FILE_CHANGE_LIST_MAX     50
#define HLP_VZW_PCO_CODE                 0xFF00
#define HLP_VZW_PCO_CODE_STR             "FF00"
#define HLP_VZW_PCO_MCCMNC_LEN           6
#define HLP_VZW_PCO_MCCMNC_STR           "311480"
#define HLP_VZW_PCO_CODE_STR_LEN         4

#define HLP_MAX_APN_NUM                  10
#define HLP_UIM_USIM_ACL_TAG           0xDD
#define HLP_UIM_USIM_ACL_SEPERATOR     0x2E
#define HLP_CAM_TIMER_MAX_NUM          15  /*the number of HLP cam timer, see structure HlpCamTimer_Info*/
/***********************************************************************/
/* Global Typedefs                                                     */
/**********************************************************************/

typedef struct
{
    uint16 HlpFiveMinTmrID;
    uint16 HlpTchRelTmrID;
    uint16 HlpXoffRspTmrID;
    uint16 HlpWfrFastConnTmrID;
    uint16 HlpDataRetryTmrID;
    uint16 HlpFallbackTmrID;
    uint16 HlpDORetryTimerID;
    uint16 HlpDrmntRcntBackOffTimerID;
    uint16 HlpWaitForUmReleaseTimerID;
    uint16 HlpC109PulseTimerID;
    uint16 HlpPppConnectBackOffTimerID;
    uint16 HlpWfrRmConnectTimerID;
    uint16 HlpWfrIratReleaseTimerID;
    uint16 HlpWfrHandoffHystTimerID;
    uint16 HlpWfrFallback1XConnectTimerID;
    /*add items below, please keep the sequense above.*/
    /* ps: the printf enum in ETS database is definded to this order*/
	uint16 HlpRsvasAbortReqTimerID;
} HlpCamTimer_Info;

typedef enum
{
   HLP_REV_DATA_REQ_MSG = 0,         /* raw frameless data at reverse link from Rm   */
   HLP_RM_TX_DATA_RSP_MSG,           /* response from Uart for completion of data at reverse link. */
   HLP_RLP_IDLE_IND_MSG,             /* alias: is7074RlpInactivityInd        */
   HLP_RLP_REV_DATA_RSP_MSG,         /* alias: is7074RlpTxComplete           */
   HLP_RLP_OPEN_FAILED_MSG,          /* 1x RLP fail to open.                 */
   HLP_PPP_CONNECT_REQ_MSG,          /* start PPP connection(s).             */
   HLP_PPP_RELEASE_REQ_MSG,          /* release PPP connection               */
   HLP_PPP_STATUS_MSG,               /* indicates change of PPP status       */
   HLP_ANSWER_REQ_MSG,               /* To answer a circuit switched data call. */
   HLP_TIMER_EXPIRED_MSG,            /* alias: is7074TimerExpired            */
   HLP_SYTEM_QUERY_RSP_MSG,          /* System query response                */
   HLP_CONNECTION_IND_MSG,           /* TCH connection response              */
   HLP_CONNECTION_RELEASED_IND_MSG,  /* TCH connection released.             */
   HLP_CONNECTION_FAILED_IND_MSG,    /* TCH connection failed.               */
   HLP_OOSA_IND_MSG,                 /* In/Out of service indicator          */
   HLP_USERNAME_PASSWD_MSG,          /* use for SIP in onboard browser.      */
   HLP_DORMANT_RECONNECT_REQ_MSG,    /* to connect TCH.                      */
   HLP_POWER_REQ_MSG,                /* for powering down HLP                */
   HLP_UM_APP_INSTANCE_MSG,
   HLP_VOICE_CALL_RELEASE_MSG, /* possible backoff timer activiation upon end of voice call. */

   HLP_SOCKET_CREATE_MSG = 30,       /* create a TCP/UDP SAP for client.     */
   HLP_SOCKET_BIND_MSG,              /* bind address/port to a SAP that's created by a socket  */
   HLP_SOCKET_CONNECT_MSG,           /* connect a created socket             */
   HLP_SOCKET_CLOSE_MSG,             /* close a connected socket             */
   HLP_DORMANT_REQ_MSG,              /* alias: is7074BrowserDormantReq       */
   HLP_SOCKET_SEND_REQ_MSG,          /* raw data to be sent through TCP/UDP sockets at reverse link. */
   HLP_SYS_SEL_PREF_SET_MSG,         /* To override result of CSS.           */
   HLP_UPB_SEND_DATA_MSG,            /* alias: is7074BrowserSendDatagram     */
   HLP_UPB_RECV_DATA_RSP_MSG,
   HLP_TCPB_DATA_RECV_RSP_MSG,

   HLP_IPV6_SOCKET_CREATE_MSG,
   HLP_IPV6_SOCKET_BIND_MSG,
   HLP_IPV6_SOCKET_CONNECT_MSG,

   HLP_OTAPA_UPDATE_IND_MSG,         /* Notification from PE when OTAPA completed  */
   HLP_SID_NID_CHANGE_IND_MSG,      /* 1X SID/NID change notification             */
   HLP_PKT_ZONE_ID_CHANGE_IND_MSG,  /* 1X Packet ZONE ID change notification      */
   HLP_SUBNET_ID_CHANGE_IND_MSG,    /* DO Subnet ID change notification           */
   HLP_COLOR_CODE_CHANGE_IND_MSG,   /* DO ColorCode change notification           */
   HLP_1X_MT_CONN_IN_NULL_IND_MSG,  /* 1X MT Connected in NULL state notification */

   HLP_ICMP_PING_REQ_MSG,

   HLP_MIP_RRP_MSG = 50,             /* alias to MIP is7074 calls            */
   HLP_MIP_AGENT_ADV_MSG,            /* alias to MIP is7074 calls            */
   HLP_MIP_UM_PPP_STATUS_MSG,        /* indicate major milestone for MIP PPP progress. */

   HLP_MIP_TIMER_CALL_BACK_MSG,
   HLP_ALMP_CONN_OPENED_MSG,
   HLP_ALMP_CONN_FAILED_MSG,
   HLP_ALMP_CONN_RELEASED_MSG,
   HLP_ALMP_CONN_CLOSED_MSG,
   HLP_FCP_PAF_DATAIND_MSG,
   HLP_XOFF_RESP_RCVD_MSG,
   HLP_ACSTRM_XON_RESP_RCVD_MSG,

   HLP_SERVICE_STATUS_MSG,           /* Service status from PSW (1X) or ALMP (DO) */

/* test messages use only. */
   HLP_TEST_BROWSER_CONNECT_MSG = 70,
   HLP_TEST_PPP_OPEN_MSG,
   HLP_TEST_PPP_CLOSE_MSG,
   HLP_IP_COUNTERS_RESET_ETS,
   HLP_IP_COUNTERS_PEEK_ETS,

   HLP_PURE_ACK_TAG_RATE_GET_ETS,
   HLP_PURE_ACK_TAG_RATE_SET_ETS,

   HLP_THROTTLING_TMR_STATUS_GET_ETS,

   HLP_DBM_DATA_DMUPUBKEY_MSG = 80,
   HLP_DBM_DATA_DMUPUBKEYORGID_MSG,
   HLP_VAL_DMUV_SET_MSG,
   HLP_PSWS_DMU_KEYGEN_RSP_MSG,
   HLP_PSWS_DMU_KEYENC_RSP_MSG,
   HLP_VAL_MIP_PREFERMODE_SET_MSG,
   HLP_GET_MIP_KEYS_RSP_MSG,
   HLP_SOCKET_LISTEN_MSG,
   HLP_SOCKET_OPTION_MSG,
   HLP_SOCKET_SHUTDOWN_MSG,
   HLP_SOCKET_LINGER_MSG,
   HLP_DBM_HSPD_SEG_READ_MSG,
   HLP_HSPD_DATABASE_WRITE_ACK_MSG,

   HLP_DBM_EHRPD_SEG_READ_MSG,
   HLP_DBM_EHRPD_WRITE_ACK_MSG,

   HLP_VAL_CTA_UPDATE_REQ_MSG,

   HLP_DO_INACTIVITY_EXPR_MSG = 100,

   /*PRI*/
   HLP_PARM_SET_MSG = 110,
   HLP_PARM_GET_MSG,
   HLP_DBM_SECURE_DATA_READ_MSG,
   HLP_DBM_SECURE_DATA_WRITE_ACK_MSG,
   HLP_SECURE_DATA_INIT_MSG,

   HLP_DMU_SECURE_DATA_CHANGED_MSG,
   HLP_DBM_GET_ESN_MEID_RSP_MSG,
#ifdef MTK_CBP
   HLP_VAL_POWER_CYCLE_MSG,
#endif
   HLP_PSW_MDN_UPDATED_MSG = 120,
   HLP_UIM_SIP_CHAP_RSP_MSG,
   HLP_UIM_MIP_MNHA_AUTH_RSP_MSG,
   HLP_UIM_MIP_RRQ_HASH_RSP_MSG,
   HLP_UIM_MIP_MNAAA_AUTH_RSP_MSG,
   HLP_UIM_AKA_AUTH_RSP_MSG,
   HLP_UIM_ACCESS_CHAP_RESP_MSG,
   HLP_UIM_GET_3GPD_OPC_RSP_MSG,                   /* Get EFME3GPDOPC    */
   HLP_UIM_UPDATE_3GPD_OPC_RSP_MSG,                /* Update EFME3GPDOPC */
   HLP_UIM_GET_3GPD_OPM_RSP_MSG,                   /* Get EF3GPDOPM      */
   HLP_UIM_UPDATE_3GPD_OPM_RSP_MSG,      /*= 130*/ /* Update EF3GPDOPM   */
   HLP_UIM_GET_3GPD_SIPCAP_RSP_MSG,                /* Get EFSIPCAP       */
   HLP_UIM_GET_3GPD_MIPCAP_RSP_MSG,                /* Get EFMIPCAP       */
   HLP_UIM_GET_3GPD_SIPUPP_RSP_MSG,                /* Get EFSIPUPP       */
   HLP_UIM_GET_3GPD_MIPUPP_RSP_MSG,                /* Get EFMIPUPP       */
   HLP_UIM_GET_3GPD_SIPSP_RSP_MSG,                 /* Get EFSIPSP        */
   HLP_UIM_UPDATE_3GPD_SIPSP_RSP_MSG,              /* Update EFSIPSP     */
   HLP_UIM_GET_3GPD_MIPSP_RSP_MSG,                 /* Get EFMIPSP        */
   HLP_UIM_UPDATE_3GPD_MIPSP_RSP_MSG,              /* Update EFMIPSP     */
   HLP_UIM_GET_3GPD_SIPPAPSS_RSP_MSG,              /* Get SIPPAPSS       */
   HLP_UIM_UPDATE_3GPD_SIPPAPSS_RSP_MSG, /*= 140*/ /* Update SIPPAPSS    */
   HLP_DORMANCY_TIMERS_SET_MSG,
   HLP_DORETRY_TIMERS_SET_MSG,
   HLP_DO_HO_1X_REQ_MSG,
   HLP_UM_PPP_RENEGOTIATION_MSG,
   HLP_SET_NETWORK_OR_RELAY_MODE_MSG,
   HLP_SET_IPCOUNTES_RSP_MSG,
   HLP_DBM_IP_COUNTERS_READ_DB_MSG,
   HLP_SET_MIP_PASSWORD_MSG,
   HLP_UIM_NOTIFY_REGISTER_MSG,
   HLP_UIM_FILE_CHANGED_MSG,
   HLP_UIM_GET_HRPDCAP_RSP_MSG,
   HLP_UIM_GET_3GPDUPPEXT_RSP_MSG,
   HLP_UIM_GET_TCPCFG_RSP_MSG,
   HLP_UIM_GET_DGC_RSP_MSG,
   HLP_UIM_GET_MIPFLAGS_RSP_MSG,
   HLP_UIM_GET_IPV6CAP_RSP_MSG,
   HLP_UIM_GET_USIM_FILE_LENGTH_RSP_MSG,
   HLP_UIM_GET_USIM_UST_DATA_RSP_MSG,
   HLP_UIM_GET_USIM_EST_DATA_RSP_MSG,
   HLP_UIM_GET_USIM_ACL_DATA_RSP_MSG,

   IMS_APP_NETWK_CONN_REQ = 170,
   IMS_APP_NETWK_REL_REQ,
   IMS_APP_SETUP_REQ,
   IMS_APP_REL_REQ,
   IMS_APP_SIP_REGISTRATION_REQ,
   IMS_PPP_PDN_CONN_SET_UP_RSP,
   IMS_PPP_PDN_CONN_REL_RSP,
   IMS_PPP_PDN_REL_IND,
   IMS_IPv6_ADDR_CONFIG_RSP,
   IMS_CAM_PPP_UM_STATUS_IND,
   IMS_CAM_PPP_RM_STATUS_IND,

   IMS_QOM_FLOW_REQ_RSP = 200,
   IMS_QOM_FLOW_REL_RSP,
   IMS_QOM_FLOW_REL_IND,
   IMS_QOM_FLOW_NTWK_INIT,
   IMS_QOM_FLOW_REL_NTWK_INIT,
   IMS_RLP_EHRPD_ATTRIBUTE_MSG,
   HLP_IMS_TIMER_EXPIRED_MSG,
   IMS_APP_BEARER_REQ_MSG,
#ifdef CBP7_IMS
   /* RTP */
   HLP_RTP_LOCAL_SDP_MSG = 250,
   HLP_RTP_PEER_SDP_MSG,
   HLP_RTP_DEL_STREAM_MSG,
   HLP_RTP_CONFIG_STREAM_MSG,
   HLP_RTP_SET_LOCAL_MCAP_MSG,

   /* RTP RMI */
   HLP_RTPRMI_AUDIO_SSO_CONNECT_RSP_MSG,
   HLP_RTPRMI_AUDIO_SSO_DISCONNECT_RSP_MSG,
   HLP_RTPRMI_TX_SPCH_MSG,
   HLP_RTPRMI_FREE_MBUF_MSG,

   /* RTP Input Simulate */
   HLP_RTP_RTP_INPUT_MSG,
   HLP_RTP_RTCP_INPUT_MSG,
   HLP_RTP_INPUT_SOURCE_MSG,
   HLP_RTP_IS_START_MSG,
   HLP_RTP_IS_STOP_MSG,
   HLP_RTP_IP6_INPUT_MSG,
#endif
#ifdef CBP7_EHRPD
   /*Message Ids for EAKA*/
   HLP_SEC_AKAAUTH_RSP_MSG = 300,
   HLP_SEC_DERIVEMK_RSP_MSG,
   HLP_SEC_DERIVEREAUTHXKEY_RSP_MSG,
   HLP_SEC_DERIVEMSK_RSP_MSG,
   HLP_SEC_DERIVEMAC_RSP_MSG,
   HLP_SEC_DERIVEPMK_RSP_MSG,
   HLP_SEC_AESCRYPT_RSP_MSG,
   HLP_GET_USIM_IMSI_RSP_MSG,
   HLP_GET_ISIM_IMPU_RSP_MSG,
   HLP_GET_USIM_AD_RSP_MSG,
#endif

   HLP_RCP_RLPFLOW_PROTOCOL_INFO_MSG = 350,
   HLP_FCP_RLPROUTE_CHANGED_MSG,

#ifdef MTK_CBP //MTK_DEV_C2K_IRAT
   /*message between CSS and HLP*/
   HLP_DEFAULT_BEARER_CONN_REQ=400,
   HLP_CSS_NETWORK_REL_REQ,
   HLP_EPS_BEARER_SETUP_REQ,
   HLP_EPS_BEARER_DISCONN_REQ,
   HLP_DATA_CONNECTION_CMD,
   HLP_IRAT_SET_IPV6ADDR_CMD,
   HLP_VAL_UPDATE_PDN_INACTIVITY_TIMER_MSG,
   HLP_VAL_PCMT_PARA_MSG,
#endif /* MTK_DEV_C2K_IRAT */

   /* Message Ids for IP6 */
   HLP_IP6_UDP_CONNECTION_OPEN_MSG = 450,
   HLP_IP6_UDP_CONNECTION_CLOSE_MSG,
   HLP_IP6_TCP_CONNECTION_OPEN_MSG,
   HLP_IP6_TCP_CONNECTION_CLOSE_MSG,
   HLP_SET_PDN_INFO_MSG,
   HLP_GET_PDN_INFO_MSG,
   HLP_SEND_PING_MSG,

   HLP_PPP_UM_CONFIG_SET = 500,
   HLP_PPP_UM_CONFIG_GET,

   HLP_PPP_ROUTE_SET_MSG = 550,
   HLP_PPP_ROUTE_GET_MSG,

   #ifdef CBP7_IMS

   HLP_SMS_OVER_IMS_SET = 560,
   HLP_SMS_OVER_IMS_GET,

   /*SIP messages*/
   HLP_SIP_ACT_REQ = 600,
   HLP_SIP_DEACT_REQ,
   HLP_SIP_LOCAL_SDP_REQ,
   HLP_SIP_SET_REQ,
   HLP_SIP_REGISTRATION_REQ,
   HLP_SIP_UNREG_REQ,
   HLP_SIP_SESSION_SETUP_REQ,
   HLP_SIP_Session_MODIFY_REQ,
   HLP_SIP_Session_NET_MODIFY_REQ,
   HLP_SIP_SESSION_DISC_REQ,
   HLP_SIP_Session_Setup_Netwk_Req,   /* there is an incoming call        */
   HLP_SIP_Session_Setup_Netwk_Rsp,   /* our respone to the incoming call */
   HLP_SIP_User_Answer_Ind,
   HLP_SIP_HOLD_REQ,
   HLP_SIP_RETRIEVE_REQ,
   HLP_SIP_HOLD_RSP,
   HLP_SIP_RETRIEVE_RSP,
   HLP_SIP_CALLEEID_REL_REQ,
   HLP_SIP_SUBSCRIBE_REQ,

   /*SIP to IMS messages*/
   HLP_SIP_ACT_RSP,
   HLP_SIP_SET_RSP,
   HLP_SIP_SESSION_SETUP_RSP,
   HLP_SIP_Session_MODIFY_RSP,
   HLP_SIP_SESSION_DISC_RSP,
   HLP_SIP_REGISTRATION_FAIL_IND,
   HLP_SIP_REGISTRATION_SUCC_IND,
   HLP_SIP_UNREG_CNF,
   HLP_SIP_RINGBACK_IND,
   HLP_VoIP_Conn_SIP_Req,
   HLP_SIP_Ringing_Alert_Ind,
   HLP_SIP_SESSION_REL_IND,
   HLP_SIP_MODIFY_IND,
   HLP_SIP_MODIFY_CNF,
   HLP_SIP_HOLD_CNF,
   HLP_SIP_HOLD_IND,
   HLP_SIP_RETRIEVE_CNF,
   HLP_SIP_RETRIEVE_IND,
   HLP_SIP_PEER_SDP_IND,
   HLP_SIP_LOCAL_SDP_IND,
   HLP_SIP_DEREGISTERATION_RSP,
   HLP_SIP_DEACT_RSP,
   HLP_SIP_SESSION_SETUP_CNF,
   HLP_IMS_SMS_REQ,

   HLP_ETS_SEND_IMS_MSG,
   HLP_ETS_SET_IMS_STATE,
   HLP_ETS_SET_PDN_STATE,

   HLP_SIP_SET_SIGCOMP,
   HLP_SIP_SET_AKASQN_MSG,
#endif

#ifdef CBP7_TLS
   HLP_IP4_TCP_CONNECTION_OPEN_MSG,
#endif
   HLP_AN_AUTH_ALGO_SET_MSG = 800,

#ifdef MTK_PLT_ON_PC
   HLP_IP_DATA_SEND_REQ_MSG,
#ifdef MTK_PLT_ON_PC_UT
   HLP_UT_RCP_PPP_NWSIM_DATA_IND_MSG,
   HLP_UT_TURN_REQ_MSG,
   HLP_UT_DISC_PPP_REQ_MSG,
   HLP_UT_PPP_NEGO_CFG_MSG,
   HLP_UT_FWD_DATA_IND_MSG,
#ifdef MTK_DEV_C2K_IRAT
   HLP_UT_SET_IRAT_MSG,
#endif /* MTK_DEV_C2K_IRAT */
#endif /* MTK_PLT_ON_PC_UT */
#endif /* MTK_PLT_ON_PC */
#if defined (MTK_DEV_C2K_IRAT) && defined (MTK_DEV_C2K_SRLTE)
   HLP_PSW_RSVAS_ABORT_REQ_MSG,
#endif
   HLP_NUM_MSG_IDS
} HlpMsgIdT;

/***********************************************************************/
/* Enumeration Definitions                                             */
/***********************************************************************/
typedef enum
{
   HlpDataChRetOK,
   HlpDataChRetOK_Empty,
   HlpDataChRetFull,
   HlpDataChRetErr
} HlpDataChRetStatus;

typedef enum
{
   DO_NETWORK,
   RTT_NETWORK,
   NON_CONNECTED
} NetworkT;

typedef enum
{
   IN_SERVICE,
   OOSA,
   TO_ACQUIRE
} SvcStatusT;

typedef enum
{
   ASYNC_DATA,
   NETWORK_RM,
   PPP_ONLY,
   RELAY_RM,
   MAIN_CONN
} NspeAppT;

typedef enum
{
    DO_ONLY,       /* DO is active and is the only system that is allowed.  */
    DO_PREFERRED,  /* DO and 1X are both active, DO is preferred.       */
    RTT_ONLY,      /* 1X is active and is the only system that is allowed.  */
    RTT_PREFERRED,  /* DO and 1X are both active, 1X is preferred.       */
    NO_PREFERENCE,
#ifdef MTK_CBP
    HLP_NTW_PREF_NONE
#endif
} NetworkPrefT;

typedef enum
{
   CONNECTION_FAILED_IND,
   CONNECTION_CLOSED_IND,
   CONNECTION_OPENED_IND
} ConnectionRspT;

typedef enum
{
   Um,  /* Service Stream */
   Rm,
   Um_AccessStream
} pppEndPointT;

typedef enum
{
   DO_FOUND,    /* Only DO network is found */
   RTT_FOUND,   /* Only 1X network is found */
   BOTH,        /* Both network are found   */
   NEITHER      /* Nothing available        */
} NetworkRspT;

typedef enum
{
   HA_DEC0,
   HA_DEC1,
   HA_ENC0,
   HA_ENC1
} pppHaPortT;

typedef enum
{
   HLP_NETWORK_ERROR_EVDO_CO_NO_SERVICE,
   HLP_NETWORK_ERROR_EVDO_CO_ACCESS_FAILURE,
   HLP_NETWORK_ERROR_EVDO_CO_REDIRECTION,
   HLP_NETWORK_ERROR_EVDO_CO_NOT_PREFERRED,
   HLP_NETWORK_ERROR_EVDO_CO_MODE_HANDOFF,
   HLP_NETWORK_ERROR_EVDO_CO_IN_PROGRESS,
   HLP_NETWORK_ERROR_EVDO_CO_SETUP_TIMEOUT,
   HLP_NETWORK_ERROR_EVDO_CO_SESSION_NOT_OPEN,
   HLP_NETWORK_ERROR_EVDO_RELEASE_NO_REASON,
   HLP_NETWORK_ERROR_EVDO_PROTOCOL_FAILURE,
   HLP_NETWORK_ERROR_EVDO_DENY_NO_REASON,
   HLP_NETWORK_ERROR_EVDO_DENY_NETWORK_BUSY,
   HLP_NETWORK_ERROR_EVDO_DENY_AUTHENTICATION,
   HLP_NETWORK_ERROR_EVDO_REDIRECT_TO_1X,
   HLP_NETWORK_ERROR_EVDO_FADE,
   HLP_NETWORK_ERROR_EVDO_USER_DISCONNECTED
#if defined (MTK_DEV_C2K_IRAT) && defined (MTK_DEV_C2K_SRLTE_L1)
   ,
   HLP_NETWORK_ERROR_EVDO_RSVAS_SUSPEND
#endif
}HlpConnCloseReasonT;

typedef PACKED_PREFIX struct HlpPPPHaLLD
{
   uint32               sourceAddr;
   uint32               xferCount;
   uint32               configReg;
   uint32               nextInLLD;
} PACKED_POSTFIX  HlpPPPHaLLDT;

typedef enum
{
   IPSERVICE_TYPE_SIP_ONLY,
   IPSERVICE_TYPE_MIP_PREFER,
   IPSERVICE_TYPE_MIP_ONLY
} IPServiceTypeT;

typedef enum
{
   REG_TIMER_EXPIRED,
   DORMANT_RECONN,
   SID_NID_CHANGE,
   PKT_ZID_CHANGE,
   COLOR_CODE_CHANGE
} MipReRegReasonT;

typedef enum
{
    Check_T_Dormancy = 0x01,
    Check_T_HRPD_Search = 0x02,
    Check_T_Rapid_Dormancy = 0x04,
    Check_T_ResDef_Dormancy = 0x08,
    Check_Dormancy_All = 0x0f
} DormParmCheckT;

/***********************************************************************/
/* Message/Struct Definitions                                          */
/***********************************************************************/
typedef PACKED_PREFIX struct
{
   NetworkT    networkType;
   SvcStatusT  svcStatus;
} PACKED_POSTFIX  networkSvcT;

typedef PACKED_PREFIX struct
{
   networkSvcT ServiceStatus;
} PACKED_POSTFIX  HlpOosaIndMsgT;

typedef PACKED_PREFIX struct
{
   NetworkRspT  SysToBeConnected;
} PACKED_POSTFIX  HlpSystemQueryRspMsgT;

/* Same as ValPswCallEndReasonT defined in valpswapi.h */
typedef enum
{
  HLP_PSW_ORIG_FAIL=2,
  HLP_PSW_ORIG_CANCELED_NDSS,
  HLP_PSW_ORIG_CANCELED,
  HLP_PSW_INTERCEPT,
  HLP_PSW_REORDER,
  HLP_PSW_CC_RELEASE,
  HLP_PSW_CC_RELEASE_SO_REJ,
  HLP_PSW_FNM_RELEASE,
  HLP_PSW_DIALING_COMPLETE,
  HLP_PSW_DIALING_CONTINUE,
  HLP_PSW_MAINTENANCE,
  HLP_PSW_VP_ON,
  HLP_PSW_VP_OFF,
  HLP_PSW_PSIST_FAIL,
  HLP_PSW_TC_RELEASE_MS,
  HLP_PSW_TC_RELEASE_PDOWN,
  HLP_PSW_TC_RELEASE_DISABLE,
  HLP_PSW_TC_RELEASE_BS,
  HLP_PSW_TC_RELEASE_SO_REJECT,
  HLP_PSW_TC_RELEASE_TIMEOUT,
  HLP_PSW_TC_RELEASE_ACK_FAIL,
  HLP_PSW_TC_RELEASE_FADE,
  HLP_PSW_TC_RELEASE_LOCK,
  HLP_PSW_PAGE_FAIL,
  HLP_PSW_RETRY_TIMER_ACTIVE,
  HLP_PSW_RETRY_TIMER_INACTIVE,
  HLP_PSW_AMPS_INSVC,
  HLP_PSW_ORIG_REJECTED,
  HLP_PSW_EV_ORIG_RETRY_ORDER,
  HLP_PSW_SO_REDIRECT
} HlpPswCallEndReasonT;

typedef PACKED_PREFIX struct
{
   HlpPswCallEndReasonT reason;
} PACKED_POSTFIX  HlpConnectionFailedIndMsgT;

typedef PACKED_PREFIX struct
{
   ConnectionRspT ConnRsp;
} PACKED_POSTFIX  HlpConnectionRspMsgT;

typedef PACKED_PREFIX struct
{
   uint16 sid;
   uint16 nid;
} PACKED_POSTFIX  HlpSidNidChangIndMsgT;

typedef PACKED_PREFIX struct
{
   uint8 pid;
} PACKED_POSTFIX  HlpPktZoneIdChangIndMsgT;

typedef PACKED_PREFIX struct
{
   uint8 subnetId[16];
} PACKED_POSTFIX  HlpSubnetIdChangIndMsgT;

typedef PACKED_PREFIX struct
{
   uint8            CallNumber[HLP_MAX_BWSR_DIG_LEN]; /* ASCII chars, NULL terminated       */
   uint8            UserId[HLP_MAX_USRID_LEN];        /* Username use for SIP               */
   uint8            Pswd[HLP_MAX_PSWD_LEN];           /* Password use for SIP               */
} PACKED_POSTFIX  DialingInfoT;

typedef PACKED_PREFIX struct
{
   uint16           ServiceOption;
   NetworkPrefT     NetworkPref;
   NspeAppT         AppType;
   DialingInfoT     CallInfo;
   bool             ToUseMoIp;                        /* TRUE = MoIP; FALSE = SIP           */
   uint16           InactivityTmr;                    /* number of seconds for 1X RLP only  */
   bool             ReleaseTchWhenFallback;           /* MIP to SIP fallback, release TCH   */
} PACKED_POSTFIX  HlpPppConnectReqMsgT;

typedef PACKED_PREFIX struct
{
   NspeAppT  AppInstance;
   uint8         Action;
} PACKED_POSTFIX HlpUmAppInstanceMsgT;

typedef PACKED_PREFIX struct
{
  uint8   FileCount;
  uint16  FileId[VAL_UIM_FILE_CHANGE_LIST_MAX];
} PACKED_POSTFIX HlpUimFileChangeMsgT;

typedef struct {
   UINT32 ipaddress;
   UINT16 port;
} FilterMux;

typedef struct {
   FilterMux socket[4];
   uint16  currentSocketNum;
}MuxInfo;

typedef PACKED_PREFIX struct
{
   bool gotoDormant;
} PACKED_POSTFIX  HlpConnectionReleasedIndMsgT;

typedef PACKED_PREFIX struct
{
   bool               success;                            /* True = TCH connection and 1X RLP established. */
} PACKED_POSTFIX  HlpConnectionIndMsgT;

typedef PACKED_PREFIX struct
{
   bool             TcpType;   /* TRUE=TCP, FALSE=UDP */
   int16            socketId;  /* input socket Id, will be used in the response message */
   ExeRspMsgT       SocketCreateRspInfo;
   ExeRspMsgT       SocketConnRspInfo;
   ExeRspMsgT       SocketCloseRspInfo;
} PACKED_POSTFIX  HlpSocketCreateMsgT;

typedef PACKED_PREFIX struct
{
   uint32           IpAddr;
   uint16           Port;
} PACKED_POSTFIX HlpAppAddrT;

typedef PACKED_PREFIX struct
{
   uint8            Sap;
   HlpAppAddrT      ResourceAddr;
   ExeRspMsgT       SocketBindRspInfo;
} PACKED_POSTFIX  HlpSocketBindMsgT;

typedef PACKED_PREFIX struct
{
   uint8            Sap;
   bool             Passive;  /* FALSE=active, TURE=listen                   */
   HlpAppAddrT      DestAddr; /* IP and port number if active mode is chosen */
} PACKED_POSTFIX  HlpSocketConnectMsgT;

typedef enum
{
   HLP_IP6_ADDR_TYPE_PUBLIC,
   HLP_IP6_ADDR_TYPE_PUBLIC_TEMP,
   HLP_IP6_ADDR_TYPE_LOCAL
} HlpIp6AddrTypeE;

typedef PACKED_PREFIX struct
{
   uint8            Sap;
   UINT8 FlowId;
   HlpIp6AddrTypeE AddrType;
   UINT16 PeerAddrPort;
   UINT32 PeerAddrIp[4];
} PACKED_POSTFIX  HlpIp6SocketConnectMsgT;

typedef PACKED_PREFIX struct
{
   UINT8 Sap;
} PACKED_POSTFIX HlpIp6SocketConnectRspMsgT;

typedef PACKED_PREFIX struct
{
   uint8            Sap;
   bool             Graceful;
} PACKED_POSTFIX  HlpSocketCloseMsgT;

typedef PACKED_PREFIX struct
{
   uint8            Sap;   /*server's sap*/
   int16            SockListenfd;
   int16            sockfd[5];
   uint8            SocketNum;
   ExeRspMsgT       MmiSocketListenRspInfo;
} PACKED_POSTFIX  HlpSocketListenMsgT;

typedef PACKED_PREFIX struct
{
   uint8            Sap;
   UINT8            opt_type;           /* Specifies which option is being set */
   UINT16           size;
} PACKED_POSTFIX  HlpOptionRequestMsgT;

typedef PACKED_PREFIX struct
{
   uint8            Sap;
   int8             how;
} PACKED_POSTFIX  HlpSocketShutDownMsgT;

typedef PACKED_PREFIX struct
{
   uint8            Sap;
   bool             lingerOnOff;
   uint32           lingerTime;
} PACKED_POSTFIX HlpSocketLingerMsgT;


typedef PACKED_PREFIX struct
{
   uint32           IpAddr;        /* 32-bit IP address.               */
   uint16           UdpPort;       /* UDP port address.                */
} PACKED_POSTFIX  HlpDatagramAddrT;

typedef PACKED_PREFIX struct
{
   uint8*           DataBufP;      /* Point to the first byte          */
   uint16           DataLen;       /* size of data in byte             */
   uint8            Flags;         /* if IP_MOREDATA is set in the flag
                                      it indicates that datagram is not
                                      able to fit into allocated buffer
                                      Datagram is truncated to fit.    */
} PACKED_POSTFIX  HlpDatagramT;


typedef PACKED_PREFIX struct
{
#if defined(MTK_PLT_ON_PC) && defined(MTK_PLT_ON_PC_UT)
    uint8            data[255];
#endif
    uint8*           DataP;   /* point to the first byte of data.    */
    uint16           Size;    /* The size of data in byte.           */
    bool             Push;    /* TRUE, send immediately;             */
                              /* FALSE, TCP determine by itself.     */
    HlpDatagramAddrT To;      /* The address of receipent for UDP    */
    uint8            Sap;     /* Socket of which the data is going to
                                 be sent.                            */
} PACKED_POSTFIX  HlpSocketSendReqMsgT;

typedef PACKED_PREFIX struct
{
   uint8    Sap;
} PACKED_POSTFIX HlpUpbRecvDataRspMsgT;


typedef PACKED_PREFIX struct
{
   uint8    Sap;
} PACKED_POSTFIX  HlpTcpbRecvRspMsgT;

typedef PACKED_PREFIX struct
{
   uint32   SrcIp;
   uint32   DestIp;
   uint16   SrcPort;
   uint16   DestPort;
   uint16   DataLength;
   uint8   *DataPtr;
} PACKED_POSTFIX  HlpMipRrpMsgT;

typedef PACKED_PREFIX struct
{
   uint32   SrcIp;
   uint32   DestIp;
   uint16   DataLength;
   uint8   *DataPtr;
} PACKED_POSTFIX  HlpMipAgentAdvMsgT;

typedef PACKED_PREFIX struct
{
   uint8    Status;
   uint32   LocalIp;
   uint32   RemoteIp;
} PACKED_POSTFIX  HlpMipUmPppStatusMsgT;

typedef PACKED_PREFIX struct
{
   uint32           TimerId;
   MipReRegReasonT  Reason;
} PACKED_POSTFIX  HlpMipTimerCallBackMsgT;

typedef struct
{
   pppEndPointT  interface;
   uint8         status;
   uint32        localAddr;
   uint32        remoteAddr;
   uint32        priDNS;
   uint32        secDNS;
} HlpPppStatusMsgT;

typedef PACKED_PREFIX struct
{
   uint8            UserId[HLP_MAX_USRID_LEN];
   uint8            Pswd[HLP_MAX_PSWD_LEN];
} PACKED_POSTFIX  HlpUsernamePasswdMsgT;

typedef PACKED_PREFIX struct
{
   uint16           TimerID;
} PACKED_POSTFIX  HlpTimerExpiredMsgT;

typedef PACKED_PREFIX struct
{
   uint8*           DataP;   /* point to the first byte of data.    */
   uint16           Size;    /* The size of data in byte.           */
   bool             Push;    /* TRUE, send immediately;             */
                             /* FALSE, TCP determine by itself.     */
   uint8            Sap;     /* Socket of which the data is going to
                                be sent.                            */
} PACKED_POSTFIX  HlpTcpbDataSendReqMsgT;

typedef PACKED_PREFIX struct
{
   HlpDatagramT      SendData;    /* Datagram itself..                  */
   HlpDatagramAddrT  To;          /* The address of receipent.          */
   uint8             Sap;         /* SAP to TCB                         */
} PACKED_POSTFIX  HlpUpbSendDataMsgT;

typedef PACKED_PREFIX struct
{
   bool              NetworkMode;  /* True: Network Mode, +CRM=2        */
                                   /* False: Relay Mode,  +CRM=1        */
} PACKED_POSTFIX  HlpSetNetworkOrRelayModeMsgT;

#ifdef CBP7_EHRPD
typedef  struct {
   CpBufferT   *dataPtr;
   uint16   offset;
   uint16   dataLen;
} PafHlpFwdDataPktT;

typedef  struct
{
   uint8       nAppType;
   uint8       nRLPFlowId;
   uint8       nRoute;
   bool        bRohcEnabled;
   uint8       numRlpFrames;
   PafHlpFwdDataPktT   rlpFrames[MAX_RLP_FRAMES_PER_IP_PKT];
} HlpRlpFwdIPDataPktT;
#endif

#if 1//!defined (MTK_DEV_MEMORY_OPT)
#define NUM_HLP_FWD_DATA_IND_ENTRIES     600
#define NUM_HLP_REV_DATA_IND_ENTRIES     150
#else
#define NUM_HLP_FWD_DATA_IND_ENTRIES     200
#define NUM_HLP_REV_DATA_IND_ENTRIES     150
#endif

typedef IopDataCpBuffDescT HlpRlpFwdDataPktT;
typedef IopDataCpBuffDescT HlpRlpRevDataPktT;

typedef PACKED_PREFIX struct
{
   uint8   forceNetworkPref;
   uint8   forceCSSResp;
} PACKED_POSTFIX  HlpSysSelPrefSetMsgT;

typedef PACKED_PREFIX struct
{
   uint8     nDMUV;
} PACKED_POSTFIX  HlpValDMUVSetMsgT;

typedef PACKED_PREFIX struct
{
   HlpConnCloseReasonT ConnCloseReason;
} PACKED_POSTFIX  HlpConnStatusMsgT;

#ifdef HSPD_PRI

#define RSA_PUBLIC_KEY_BIT     (1<<0)
#define AUTH_SET_BIT           (1<<1)
#define PKOID_SET_BIT          (1<<2)

typedef enum
{
   RSA_PUBLIC_KEY_SET = RSA_PUBLIC_KEY_BIT,
   AUTH_SET           = AUTH_SET_BIT,
   SET_BOTH           = RSA_PUBLIC_KEY_BIT|AUTH_SET_BIT,
   PKOID_SET          = PKOID_SET_BIT
} RsaSetE;

typedef struct
{
   uint8 nPKOID;
   uint8 nActiveDmuRsaPublicKeyNo;
   uint8 MN_Authenticator[MN_AUTH_MAX_SIZE];
} HlpRSAPublicKeyOrgIdInfoT;

typedef enum
{
   /* Set All Params in a Profile */
   HL_PARM_SET_HSPD_SEG_PROFILE_DATA             = 0,
   HL_PARM_GET_HSPD_SEG_PROFILE_DATA,
   HL_PARM_SET_HSPD_SECURE_SEG_PROFILE_DATA,  /* no Gets for Secure Data */

   HL_PARM_SET_HSPD_SECURE_SEG_PROFILE_BYTE_DATA,

   HL_PARM_GET_HSPD_SECURE_SEG_PROFILE_DATA,

   /* Set All HSPD SegData together, that are not part of a Profile */
   HL_PARM_SET_HSPD_SEG_ALL_DATA,
   HL_PARM_GET_HSPD_SEG_ALL_DATA,

   /* Init HSPD Db to defaults */
   HL_PARM_SET_HSPD_SEG_INIT,   /* Init Hspd Data that are not part of a Profile */
   HL_PARM_SET_HSPD_SEG_PROFILE_INIT, /* Init a Profile, with ProfileId */

   /* Set Individual Params in HSPD SegData, that are not part of a Profile */
   HL_PARM_SET_ACTIVE_PROFILE,                   /* ActiveProfile */
   HL_PARM_GET_ACTIVE_PROFILE                    = 10,
   HL_PARM_SET_NUM_PROFILE,                      /* NumProfiles */
   HL_PARM_GET_NUM_PROFILE,
   HL_PARM_SET_RRP_TIMEOUT,                      /* RRPTimeout */
   HL_PARM_GET_RRP_TIMEOUT,
   HL_PARM_SET_NUM_REG_RETRIES,                  /* NumRegRetries */
   HL_PARM_GET_NUM_REG_RETRIES,
   HL_PARM_SET_REG_BACKOFF,                      /* RRA */
   HL_PARM_GET_REG_BACKOFF,
   HL_PARM_SET_IS801_IPADDR,                     /* Is801_IpAddr */
   HL_PARM_GET_IS801_IPADDR                      = 20,
   HL_PARM_SET_IS801_PORTNUM,                    /* Is801_PortNum */
   HL_PARM_GET_IS801_PORTNUM,
   HL_PARM_SET_IS801_USERID,                     /* Is801_UserId[HLP_MAX_USRID_LEN]*/
   HL_PARM_GET_IS801_USERID,
   HL_PARM_SET_IS801_PSWD,                       /* Is801_Pswd[HLP_MAX_PSWD_LEN] */
   HL_PARM_GET_IS801_PSWD,
   HL_PARM_SET_IS801_CALLEDNUMBER,               /* Is801_CalledNumber[HLP_MAX_BWSR_DIG_LEN] */
   HL_PARM_GET_IS801_CALLEDNUMBER,
   HL_PARM_SET_MN_HA_AUTH,                       /* 0-RFC2002, 1-RFC2002bis */
   HL_PARM_GET_MN_HA_AUTH                        = 30,
   HL_PARM_SET_MN_HA_AUTH_ALGO,                  /* MN_HA_Auth_Algo */
   HL_PARM_GET_MN_HA_AUTH_ALGO,
   HL_PARM_SET_MN_AAA_AUTH_ALGO,                 /* MN_AAA_Auth_Algo */
   HL_PARM_GET_MN_AAA_AUTH_ALGO,
   HL_PARM_SET_MN_AUTHENTICATOR,                 /* MN_Authenticator[MN_AUTH_MAX_SIZE] */
   HL_PARM_GET_MN_AUTHENTICATOR,
   HL_PARM_SET_ACTIVE_DMURSA_PK,                 /* ActiveDmuRsaPublicKey */
   HL_PARM_GET_ACTIVE_DMURSA_PK,
   HL_PARM_SET_NPKOID,                           /* nPKOID */
   HL_PARM_GET_NPKOID                            = 40,
   HL_PARM_SET_SIP_NAI,                          /* SIP_NAI[HLP_MN_NAI_MAX_SIZE] */
   HL_PARM_GET_SIP_NAI,
   HL_PARM_SET_SIP_DUN_NAI,                      /* SIP_DUN_NAI[HLP_MN_NAI_MAX_SIZE] */
   HL_PARM_GET_SIP_DUN_NAI,
   HL_PARM_SET_AN_NAI,                           /* AN_NAI[HLP_MN_NAI_MAX_SIZE] */
   HL_PARM_GET_AN_NAI,
   HL_PARM_SET_DORMANT_HANDOFF_OPTSET,           /* DormantHandoffOptSet */
   HL_PARM_GET_DORMANT_HANDOFF_OPTSET,
   HL_PARM_SET_DATA_SCRM_ENABLED,                /* DataScrmEnabled */
   HL_PARM_GET_DATA_SCRM_ENABLED                 = 50,
   HL_PARM_SET_DATA_TRTL_ENABLED,                /* DataTrtlEnabled */
   HL_PARM_GET_DATA_TRTL_ENABLED,
   HL_PARM_SET_PREFRC,                           /* PrefRc */
   HL_PARM_GET_PREFRC,
   HL_PARM_SET_DNS_PRI_IP_ADDR,
   HL_PARM_GET_DNS_PRI_IP_ADDR,
   HL_PARM_SET_DNS_SEC_IP_ADDR,
   HL_PARM_GET_DNS_SEC_IP_ADDR,
   HL_PARM_GET_DNS_IP_ADDR,
   HL_PARM_SET_DNS_IP_ADDR                       = 60,
   HL_PARM_SET_MIP_MODE,
   HL_PARM_GET_MIP_MODE,

   /* Set Individual Params in HSPD Secure SegData, that are not part of a Profile */
   HL_PARM_SET_SIP_STRING_PASSWORD,
   HL_PARM_GET_SIP_STRING_PASSWORD,
   HL_PARM_SET_AN_PASSWORD,
   HL_PARM_GET_AN_PASSWORD,
   HL_PARM_SET_AN_BINARY_PASSWORD,
   HL_PARM_SET_SIP_BINARY_PASSWORD,
   HL_PARM_SET_PACKET_DIAL_STRING,
   HL_PARM_GET_PACKET_DIAL_STRING                = 70,
   HL_PARM_SET_VJ_COMPRESSION_ENABLE,
   HL_PARM_GET_VJ_COMPRESSION_ENABLE,
   HL_PARM_SET_MIP_STRING_PASSWORD,
   HL_PARM_GET_MIP_STRING_PASSWORD,
   HL_PARM_SET_MIP_BINARY_PASSWORD,
   HL_PARM_GET_MIP_BINARY_PASSWORD,
   HL_PARM_SET_QNC_ENABLED,
   HL_PARM_GET_QNC_ENABLED,
   HL_PARM_SET_MDR_MODE_ENABLED,
   HL_PARM_GET_MDR_MODE_ENABLED                  = 80,
   HL_PARM_SECURE_PROFILE_DATA_INIT,
   HL_PARM_GET_SIP_BINARY_PASSWORD,
#ifdef CBP7_EHRPD
   HL_PARM_SET_AKA_PASSWORD                      = 83,
   HL_PARM_GET_AKA_PASSWORD,
   HL_PARM_SET_AKA_RROFILEDATA,
   HL_PARM_GET_AKA_PROFILEDATA,
#endif
   HL_PARM_GET_HSPD_LOCK                         = 87,
   HL_PARM_SET_HSPD_LOCK,
   HL_PARM_SET_HSPD_LOCK_CODE,
   HL_PARM_SET_DATA_AUTO_PACKET_DETECTION,
   HL_PARM_GET_DATA_AUTO_PACKET_DETECTION,
   HL_PARM_SET_DATA_SO,
   HL_PARM_GET_DATA_SO,
   HL_PARM_SET_MN_NAI,
   HL_PARM_GET_MN_NAI,
   HL_PARM_SET_MN_DUN_NAI,
   HL_PARM_GET_MN_DUN_NAI,
   HL_PARM_SET_MN_HA_SPI_ENABLE,
   HL_PARM_GET_MN_HA_SPI_ENABLE,
   HL_PARM_SET_MN_HA_SPI,
   HL_PARM_GET_MN_HA_SPI,
   HL_PARM_SET_MN_AAA_SPI_ENABLE,
   HL_PARM_GET_MN_AAA_SPI_ENABLE,
   HL_PARM_SET_MN_AAA_SPI,
   HL_PARM_GET_MN_AAA_SPI,
   HL_PARM_SET_MN_REVERSE_TUNNELING,
   HL_PARM_GET_MN_REVERSE_TUNNELING,
   HL_PARM_SET_MN_HOME_IP_ADDRESS,
   HL_PARM_GET_MN_HOME_IP_ADDRESS,
   HL_PARM_SET_HOME_AGENT_PRIMARY_IP_ADDRESS,
   HL_PARM_GET_HOME_AGENT_PRIMARY_IP_ADDRESS,
   HL_PARM_SET_HOME_AGENT_SECONDARY_IP_ADDRESS,
   HL_PARM_GET_HOME_AGENT_SECONDARY_IP_ADDRESS,

   HL_PARM_SET_HSPD_NETWORKMODE,
   HL_PARM_GET_HSPD_NETWORKMODE,

   HL_PARM_SET_HSPD_DORMANT_TIMER_MODE,
   HL_PARM_GET_HSPD_DORMANT_TIMER_MODE,

   HL_PARM_SET_MIP_DE_REG_RETRIES                = 118,
   HL_PARM_GET_MIP_DE_REG_RETRIES,
   HL_PARM_SET_MIP_RE_REG_ONLYIF_TRAFFIC,
   HL_PARM_GET_MIP_RE_REG_ONLYIF_TRAFFIC,
   HL_PARM_SET_MIP_NAI_ENABLE,
   HL_PARM_GET_MIP_NAI_ENABLE,

   HL_PARM_SET_DATA_DO_TO_1X_ENABLE,
   HL_PARM_GET_DATA_DO_TO_1X_ENABLE,

   HL_PARM_SET_DATA_PPP_UM_CONFIG                = 126,
   HL_PARM_GET_DATA_PPP_UM_CONFIG,
   HL_PARM_SET_DATA_PPP_RM_CONFIG,
   HL_PARM_GET_DATA_PPP_RM_CONFIG,
   HL_PARM_SET_DATA_TCP_CONFIG,
   HL_PARM_GET_DATA_TCP_CONFIG,

   HL_PARM_SET_DO_RETRY_TIMER                    = 132,
   HL_PARM_GET_DO_RETRY_TIMER,

#ifdef CBP7_EHRPD
   HL_PARM_SET_EHRPD_TEST_MODE                   = 148,
   HL_PARM_GET_EHRPD_TEST_MODE,
#endif

   HL_PARM_SET_PROFILE_VALID                     = 150,
   HL_PARM_GET_PROFILE_VALID,
   HL_PARM_VAL_AT_SET_HSPD_NETWORKMODE,
   HL_PARM_VAL_AT_SET_AN_NAI,
   HL_PARM_VAL_AT_GET_AN_NAI,
#ifdef IPV6_FEATURE_CHINATELECOM
  HL_PARM_SET_IPV6V4MODE = 160,
  HL_PARM_GET_IPV6V4MODE,
#endif

   HL_PARM_OPERATION_ID_END_LIST
} HlpParmOperationId;

typedef enum
{
   HL_PARM_MIN_VALUE,
   HL_PARM_MAX_VALUE,
   HL_PARM_DEFAULT_VALUE,
   HL_PARM_CUSTOM_VALUE,
   HL_PARM_OP_TYPE_LIST_END
} HlpParmOperationType;

typedef enum
{
   HL_PARM_OPERATION_SUCCESS,
   HL_PARM_OPERATION_FAIL_READ_NOT_ALLOWED,
   HL_PARM_OPERATION_FAIL_WRITE_NOT_ALLOWED,
   HL_PARM_OPERATION_NOT_ALLOWED_IN_HL_STATE,
   HL_PARM_OPERATION_FAIL_INVALID_PTR,
   HL_PARM_OPERATION_FAIL_INVALID_LENGTH,
   HL_PARM_OPERATION_GENERAL_FAILURE,
   HL_PARM_OPERATION_NO_CHANGE_IN_VALUE,
   HL_PARM_OPERATION_FAIL_VALUE_OUT_OF_RANGE,
   HL_PARM_OPERATION_PARAMETER_NOT_SUPPORTED,
   HL_PARM_OPERATION_INTERFACE_NOT_SUPPORTED,
   HL_PARM_OPERATION_FAIL_OP_TYPE_NOT_SUPPORTED,
   HL_PARM_OPERATION_FAIL_DEFAULT_NOT_DEFINED,
   HL_PARM_OPERATION_FAIL_DEFAULT_NOT_SUPPORTED_FOR_PARM,
   HL_PARM_OPERATION_RESULT_END_LIST
} HlpParmAccessResultCode;

typedef enum
{
   NOT_CONNECTED,
   DO_CONNECTING,
   DO_CONNECTED,
   RTT_CONNECTING,
   RTT_CONNECTED
} ConnStateT;


typedef PACKED_PREFIX struct
{
   ExeRspMsgT resp;
   uint8   SMS_Over_IP_Networks_Indication;
   uint8   SigComp;
   uint8   ImsDomain[72];
   uint8   AkaPassword[72];
   uint16  SipT1Timer_ims;
   uint16  SipTFTimer_ims;
   uint16  SipT2Timer_ims;
   uint16  SipPcscfPort;
} PACKED_POSTFIX HlpSmsImsEtsCfgSetT;

typedef PACKED_PREFIX struct
{
   uint8   SMS_Over_IP_Networks_Indication;
   uint8   SigComp;
   uint8   ImsDomain[72];
   uint8   AkaPassword[72];
   uint16  SipT1Timer_ims;
   uint16  SipTFTimer_ims;
   uint16  SipT2Timer_ims;
   uint16  SipPcscfPort;
} PACKED_POSTFIX HlpSmsImsEtsCfgGetT;

/* The following structures are to be obsolete
   but keep them for backword compatible now
typedef PACKED_PREFIX struct
{
   ExeRspMsgT resp;
   uint16  restartTimer;
   uint8   lcpReqTry;
   uint8   lcpNakTry;
   uint8   lcpTermTry;
   uint8   authRetry;
   uint8   ipcpReqTry;
   uint8   ipcpNakTry;
   uint8   ipcpTermTry;
   uint8   ipcpCompression;
} PACKED_POSTFIX HlpPppUmEtsCfgSetT;

typedef PACKED_PREFIX struct
{
   uint8 result;
} PACKED_POSTFIX HlpPppUmEtsCfgSetRespT;

typedef PACKED_PREFIX struct
{
   uint8   result;
   uint16  restartTimer;
   uint8   lcpReqTry;
   uint8   lcpNakTry;
   uint8   lcpTermTry;
   uint8   authRetry;
   uint8   ipcpReqTry;
   uint8   ipcpNakTry;
   uint8   ipcpTermTry;
   uint8   ipcpCompression;
} PACKED_POSTFIX HlpPppUmEtsCfgGetT;

typedef PACKED_PREFIX struct
{
   uint16  restartTimer;
   uint8   lcpReqTry;
   uint8   lcpNakTry;
   uint8   lcpTermTry;
   uint8   authRetry;
   uint8   ipcpReqTry;
   uint8   ipcpNakTry;
   uint8   ipcpTermTry;
   uint8   ipcpCompression;
} PACKED_POSTFIX HlpPppUmEtsCfgT;
   The above structures are to be obsolete
   but keep them for backword compatible now */

typedef NV_PACKED_PREFIX struct
{
   uint32  RxBytes;
   uint32  RxPackets;
   uint32  BadRxPackets;
   uint32  TxBytes;
   uint32  TxPackets;
   uint64  TotalRxBytes;
   uint64  TotalRxPackets;
   uint64  TotalBadRxPackets;
   uint64  TotalTxBytes;
   uint64  TotalTxPackets;
   uint8   Flag[4];
   uint8   Unused[10];
} NV_PACKED_POSTFIX IPCounterStruct;

typedef PACKED_PREFIX struct
{
   uint32  RxBytes;
   uint32  RxPackets;
   uint32  BadRxPackets;
   uint32  TxBytes;
   uint32  TxPackets;
   uint64  TotalRxBytes;
   uint64  TotalRxPackets;
   uint64  TotalBadRxPackets;
   uint64  TotalTxBytes;
   uint64  TotalTxPackets;
} PACKED_POSTFIX IPCounterT;

typedef PACKED_PREFIX struct
{
   uint8   lcpReqTries;
   uint8   lcpNakTries;
   uint16  lcpRestartTimer;
   uint8   lcpTermTries;
   uint16  lcpTermTimer;
   uint8   authRetries;
   uint16  authTimer;
   uint8   ncpReqTries;
   uint8   ncpNakTries;
   uint16  ncpReStrTimer;
   uint8   ncpTermTries;
   uint16  ncpTermTimer;
} PACKED_POSTFIX HlpPppUmCfg_APIStruct;

typedef NV_PACKED_PREFIX struct
{
   uint8   lcpReqTries;
   uint8   lcpNakTries;
   uint16  lcpRestartTimer;
   uint8   lcpTermTries;
   uint16  lcpTermTimer;
   uint8   authRetries;
   uint16  authTimer;
   uint8   ncpReqTries;
   uint8   ncpNakTries;
   uint16  ncpReStrTimer;
   uint8   ncpTermTries;
   uint16  ncpTermTimer;
   uint8   unused[8];
} NV_PACKED_POSTFIX HlpPppUmCfgT;

typedef PACKED_PREFIX struct
{
   bool    pppKeepAlive;
   bool    pppDetect;
   uint8   lcpReqTries;
   uint8   lcpNakTries;
} PACKED_POSTFIX HlpPppRmCfg_APIStruct;

typedef NV_PACKED_PREFIX struct
{
   bool    pppKeepAlive;
   bool    pppDetect;
   uint8   lcpReqTries;
   uint8   lcpNakTries;
   uint16  lcpRestartTimer;
   uint8   lcpTermTries;
   uint16  lcpTermTimer;
   uint8   ncpReqTries;
   uint8   ncpNakTries;
   uint16  ncpReStrTimer;
   uint8   ncpTermTries;
   uint16  ncpTermTimer;
   uint8   unused[3];
} NV_PACKED_POSTFIX HlpPppRmCfgT;

typedef PACKED_PREFIX struct
{
   uint16  tcpMtu;
} PACKED_POSTFIX HlpTcpCfgSet_APIStruct;

typedef PACKED_PREFIX struct
{
   uint16  tcpMtu;
   uint16  tcpWinSize_1XRTT;
   uint16  tcpWinSize_EVDO;
} PACKED_POSTFIX HlpTcpCfg_APIStruct;

typedef NV_PACKED_PREFIX struct
{
   uint16  tcpMtu;
   uint16  tcpWinSize_1XRTT;
   uint16  tcpWinSize_EVDO;
   uint8   unused[4];
} NV_PACKED_POSTFIX HlpTcpCfgT;

typedef PACKED_PREFIX struct{
   uint8 ProfileId;
   uint8 NAI[HLP_MN_NAI_MAX_SIZE];
} PACKED_POSTFIX  HlpHspdMNNAI_APIStruct;

typedef PACKED_PREFIX struct{
   uint8 ProfileId;
   uint8 MN_REV_TUNNELING;
} PACKED_POSTFIX  HlpHspdRevTunneling_APIStruct;

typedef PACKED_PREFIX struct{
   uint8  ProfileId;
   uint32 SPI;
} PACKED_POSTFIX  HlpHspdMNSPI_APIStruct;

typedef PACKED_PREFIX struct{
   uint8 ProfileId;
   bool  SPI_Enable;
} PACKED_POSTFIX  HlpHspdMNSPIEnable_APIStruct;

typedef PACKED_PREFIX struct{
   uint8 ProfileId;
   uint32 IPAddress;
} PACKED_POSTFIX  HlpHspdProfileIPAddress_APIStruct;

typedef PACKED_PREFIX struct{
   uint8 ProfileId;
} PACKED_POSTFIX  HlpHspdProfileParmGet_APIStruct;

typedef NV_PACKED_PREFIX struct{
   uint8    ProfileId;
   bool     Profile_Valid;
   uint32   MN_HOME_IP_ADDR;
   uint32   HA_PRI_IP_ADDR;
   uint32   HA_SEC_IP_ADDR;
   uint8    MN_NAI[HLP_MN_NAI_MAX_SIZE];
   uint8    MN_DUN_NAI[HLP_MN_NAI_MAX_SIZE];
   uint8    MN_REV_TUNNELING;
   bool     bMN_HA_SPI_Enable;
   uint32   MN_HA_SPI;
   bool     bMN_AAA_SPI_Enable;
   uint32   MN_AAA_SPI;
   uint8    Padding[HLP_HSPD_PROFILE_PAD_SIZE];
} NV_PACKED_POSTFIX  HlpHspdProfileData;

#if defined MTK_PLT_ON_PC
#pragma pack( push, saved_packing, 1 )
#endif
typedef NV_PACKED_PREFIX struct {
   uint8 NAI[HLP_MN_NAI_MAX_SIZE];
   uint8 AcessNetworkId[HLP_MN_NAI_MAX_SIZE];
   uint8 OP[HLP_AKA_OP_LEN];
   uint8 SQN[HLP_AKA_SEQ_ARRAY_SIZE][HLP_AKA_SQN_LEN];
} NV_PACKED_POSTFIX  HlpHspdAkaProfileDataT;
#if defined MTK_PLT_ON_PC
#pragma pack( pop, saved_packing )
#endif

typedef PACKED_PREFIX struct{
   uint8    ProfileId;
   bool     Profile_Valid;
   uint32   MN_HOME_IP_ADDR;
   uint32   HA_PRI_IP_ADDR;
   uint32   HA_SEC_IP_ADDR;
   uint8    MN_NAI[HLP_MN_NAI_MAX_SIZE];
   uint8    MN_DUN_NAI[HLP_MN_NAI_MAX_SIZE];
   uint8    MN_REV_TUNNELING;
   bool     bMN_HA_SPI_Enable;
   uint32   MN_HA_SPI;
   bool     bMN_AAA_SPI_Enable;
   uint32   MN_AAA_SPI;
} PACKED_POSTFIX  HlpProfileData_APIStruct;

typedef NV_PACKED_PREFIX struct {
   uint8    RRPTimeout;      /* RRP MIP Registration timeout        */
   uint8    NumRegRetries;   /* RRP MIP Registration Retry Attempts */
   uint16   RRA;             /* Pre Re-registration backoff */
   uint32   Is801_IpAddr;
   uint16   Is801_PortNum;
   uint8    Is801_UserId[HLP_MAX_USRID_LEN];
   uint8    Is801_Pswd[HLP_MAX_PSWD_LEN];
   uint8    Is801_CalledNumber[HLP_MAX_BWSR_DIG_LEN];

   uint8    MN_HA_AUTH;  /* 0-RFC2002, 1-RFC2002bis, 2-bypassHA */
   uint8    MN_HA_Auth_Algo;
   uint8    MN_AAA_Auth_Algo;

   uint8    MN_Authenticator[MN_AUTH_MAX_SIZE];
   uint8    ActiveDmuRsaPublicKey;   /* 0-TestKey, 1-CommercialKey */
   uint8    nPKOID;

   uint8    SIP_NAI[HLP_MN_NAI_MAX_SIZE];
   uint8    SIP_DUN_NAI[HLP_MN_NAI_MAX_SIZE];
   uint8    AN_NAI[HLP_MN_NAI_MAX_SIZE];

   uint8    DormantHandoffOptSet;  /* 0-Disabled, 1-Enabled */
   uint8    DataScrmEnabled;       /* Supp Chan Supported */
   uint8    DataTrtlEnabled;       /* Slow Down data at high CPU utilization */
   uint8    PrefRc;                /* Preferred Radio Cfg. Default-RC3 */
   uint32   DNS_PRI_IP_ADDR;
   uint32   DNS_SEC_IP_ADDR;
   uint8    MIPMode;

   uint32   ActiveProfile;
   uint8    NumProfiles;
   uint8    VJCompEnabled;
   bool     HspdLocked;
   uint16   LockCode;
   bool     HspdNetworkMode;
   uint16   T_Dormancy;
   uint16   T_HRPD_Search;
   uint16   T_DO_Retry;
   uint16   T_Rapid_Dormancy;
   uint16   T_ResDef_Dormancy;
   uint8    NumDeRegRetries;
   bool     MipReRegOnlyIfTraffic;
   bool     MipNaiEnabled;
   bool     DoTo1X_Enabled;

   HlpPppUmCfgT PppUmCfg;
   HlpPppRmCfgT PppRmCfg;
   HlpTcpCfgT   TcpCfg;
   uint8     Ipv6V4Mode;

   uint8    Padding[HLP_HSPD_DB_PAD_SIZE];
   HlpHspdProfileData ProfileData [HLP_MAX_HSPD_PROFILES];
} NV_PACKED_POSTFIX  HlpHspdSegData;

typedef NV_PACKED_PREFIX struct
{
   HlpHspdAkaProfileDataT AkaProfileData;
   uint32                 pcmt_val_ehrpd; /* PPP Partial Context Maintenance timer eHRPD: standalone eHRPD scenario. Unit: second. */
   uint32                 pcmt_val_irat;  /* PPP Partial Context Maintenance timer eHRPD: LTE IRAT transition scenario. Unit: second. */
} NV_PACKED_POSTFIX  HlpEHrpdSegDataT;

typedef NV_PACKED_PREFIX struct
{
   uint8     ProfileId;
   bool      Profile_Valid;
   uint8     MN_AAA_PASSWORD_LEN;
   uint8     MN_AAA_PASSWORD[MN_PASSWD_MAX_SIZE]; /* MIP AAA Shared Secret*/
   uint8     MN_HA_PASSWORD_LEN;
   uint8     MN_HA_PASSWORD[MN_PASSWD_MAX_SIZE]; /* MIP HA Shared Secret  */
   uint8     Padding[HLP_HSPD_SECURE_PROFILE_PAD_SIZE];
} NV_PACKED_POSTFIX  HlpSecureProfileData;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;         /* Response routing information */
   bool        MNAAAPasswordValid;
   uint8       lenMNAAAPassword;
   uint8       MN_AAA_PASSWORD[MN_PASSWD_MAX_SIZE]; /* MIP AAA Shared Secret*/
   bool        MNHAPasswordValid;
   uint8       lenMNHAPassword;
   uint8       MN_HA_PASSWORD[MN_PASSWD_MAX_SIZE]; /* MIP HA Shared Secret  */
} PACKED_POSTFIX  HlpSetMIPpasswordMsgT;

typedef NV_PACKED_PREFIX struct
{
   uint32    checkSum; /* this field is not used any more */
   uint8     SIP_PASSWORD_LEN;
   uint8     SIP_PASSWORD[HLP_MAX_PSWD_LEN]; /* Simple IP User Password   */
   uint8     AN_PASSWORD_LEN;
   uint8     AN_PASSWORD[HLP_MAX_PSWD_LEN]; /* AN Shared Secret*/
#ifdef CBP7_EHRPD
   uint8     AKA_PASSWORD[HLP_MAX_PSWD_LEN];
#endif
   uint8     Padding[HLP_HSPD_SECURE_DB_PAD_SIZE];
   HlpSecureProfileData   ProfileData [HLP_MAX_HSPD_PROFILES];
} NV_PACKED_POSTFIX  HlpHspdSecureSegData;

typedef PACKED_PREFIX struct
{
   uint8     ProfileId;
   bool      Profile_Valid;
   uint8     MN_AAA_PASSWORD_LEN;
   uint8     MN_AAA_PASSWORD[MN_PASSWD_MAX_SIZE]; /* MIP AAA Shared Secret*/
   uint8     MN_HA_PASSWORD_LEN;
   uint8     MN_HA_PASSWORD[MN_PASSWD_MAX_SIZE]; /* MIP HA Shared Secret  */
} PACKED_POSTFIX  HlpSecureProfileData_APIStruct;

typedef PACKED_PREFIX struct
{
   uint8   ProfileId;   /* Init the data to defaults, for this ProfileId */
} PACKED_POSTFIX  HlpInitHspdProfileData_APIStruct;

typedef PACKED_PREFIX struct
{
   uint8   ProfileId;   /* Init the data to defaults, for this ProfileId */
} PACKED_POSTFIX HlpGetHspdProfileData_APIStruct;


typedef PACKED_PREFIX struct
{

   /* HSPD Seg Data, that are not part of a profile */
   /*===============================================*/
   uint8    RRPTimeout;      /* RRP MIP Registration timeout        */
   uint8    NumRegRetries;   /* RRP MIP Registration Retry Attempts */
   uint16   RRA;             /* Pre Re-registration backoff         */
   uint32   Is801_IpAddr;
   uint16   Is801_PortNum;
   uint8    Is801_UserId[HLP_MAX_USRID_LEN];
   uint8    Is801_Pswd[HLP_MAX_PSWD_LEN];
   uint8    Is801_CalledNumber[HLP_MAX_BWSR_DIG_LEN];

   uint8    MN_HA_AUTH;  /* 0-RFC2002, 1-RFC2002bis, 2-bypassHA */
   uint8    MN_HA_Auth_Algo;
   uint8    MN_AAA_Auth_Algo;
   uint8    MN_Authenticator[MN_AUTH_MAX_SIZE];
   uint8    ActiveDmuRsaPublicKey;   /* 0-TestKey, 1-CommercialKey */
   uint8    nPKOID;

   uint8    SIP_NAI[HLP_MN_NAI_MAX_SIZE];
   uint8    SIP_DUN_NAI[HLP_MN_NAI_MAX_SIZE];
   uint8    AN_NAI[HLP_MN_NAI_MAX_SIZE];

   uint8    DormantHandoffOptSet;  /* 0-Disabled, 1-Enabled */
   uint8    DataScrmEnabled;  /* Supp Chan Supported */
   uint8    DataTrtlEnabled;   /* Slow Down data at high CPU utilization */
   uint8    PrefRc; /* Preferred Radio Cfg. Default-RC3 */
   uint32   DNS_PRI_IP_ADDR;
   uint32   DNS_SEC_IP_ADDR;
   uint8    MIPMode;

   uint32   ActiveProfile;
   uint8    NumProfiles;
   uint8    VJCompEnabled;
   /* HSPD Secure Seg Data, that are not part of a profile */
   /*======================================================*/
   /* Only Set applies, no Gets for Secure Data */
   uint8    SIP_PASSWORD[HLP_MAX_PSWD_LEN]; /* Simple IP User Password   */
   uint8    AN_PASSWORD[HLP_MAX_PSWD_LEN]; /* AN Shared Secret*/
} PACKED_POSTFIX  HlpHspdSegData_APIStruct;

typedef PACKED_PREFIX struct
{
  uint8 RRPTimeout;
} PACKED_POSTFIX  HlpRrpTimeout_APIStruct;

typedef PACKED_PREFIX struct
{
  uint8 NumRegRetries;
} PACKED_POSTFIX  HlpNumRegRetries_APIStruct;

typedef PACKED_PREFIX struct
{
  uint8 NumDeRegRetries;
} PACKED_POSTFIX  HlpDeRegRetries_APIStruct;

typedef PACKED_PREFIX struct
{
  bool ReRegOnlyIfTraffic;
} PACKED_POSTFIX  HlpReRegOnlyIfTraffic_APIStruct;

typedef PACKED_PREFIX struct
{
  bool MipNaiEnabled;
} PACKED_POSTFIX  HlpMipNaiEnable_APIStruct;

typedef PACKED_PREFIX struct
{
  uint16 RRA;
} PACKED_POSTFIX  HlpReRegBackoff_APIStruct;

typedef PACKED_PREFIX struct
{
   uint32   Is801_IpAddr;
} PACKED_POSTFIX  HlpIs801IpAddr_APIStruct;

typedef PACKED_PREFIX struct
{
   uint16   Is801_PortNum;
} PACKED_POSTFIX  HlpIs801PortNum_APIStruct;

typedef PACKED_PREFIX struct
{
   uint8    Is801_UserId[HLP_MAX_USRID_LEN];
} PACKED_POSTFIX  HlpIs801UserId_APIStruct;

typedef PACKED_PREFIX struct
{
   uint8    Is801_Pswd[HLP_MAX_PSWD_LEN];
} PACKED_POSTFIX  HlpIs801Pswd_APIStruct;

typedef PACKED_PREFIX struct
{
   uint8    Is801_CalledNumber[HLP_MAX_BWSR_DIG_LEN];
} PACKED_POSTFIX  HlpIs801CalledNumber_APIStruct;


typedef PACKED_PREFIX struct
{
   uint8    MN_HA_AUTH;  /* 0-RFC2002, 1-RFC2002bis, 2-bypassHA */
} PACKED_POSTFIX  HlpMNHAAUTH_APIStruct;
typedef PACKED_PREFIX struct
{
   uint8    MN_HA_Auth_Algo;
} PACKED_POSTFIX  HlpMNHAAuthAlgo_APIStruct;

typedef PACKED_PREFIX struct
{
   uint8    MN_AAA_Auth_Algo;
} PACKED_POSTFIX  HlpMNAAAAuthAlgo_APIStruct;

typedef PACKED_PREFIX struct
{
   uint8    MN_Authenticator[MN_AUTH_MAX_SIZE];
} PACKED_POSTFIX  HlpMNAuthenticator_APIStruct;

typedef PACKED_PREFIX struct
{
   uint8    ActiveDmuRsaPublicKey;   /* 0-TestKey, 1-CommercialKey */
} PACKED_POSTFIX  HlpActiveDmuRsaPublicKey_APIStruct;

typedef PACKED_PREFIX struct
{
   uint8    nPKOID;
} PACKED_POSTFIX  HlpPKOID_APIStruct;

typedef PACKED_PREFIX struct
{
   uint8    SIP_NAI[HLP_MN_NAI_MAX_SIZE];
} PACKED_POSTFIX  HlpSIPNAI_APIStruct;

typedef PACKED_PREFIX struct
{
   uint8    SIP_DUN_NAI[HLP_MN_NAI_MAX_SIZE];
} PACKED_POSTFIX  HlpSIPDUNNAI_APIStruct;

typedef PACKED_PREFIX struct
{
   uint8    AN_NAI[HLP_MN_NAI_MAX_SIZE];
} PACKED_POSTFIX  HlpANNAI_APIStruct;

typedef PACKED_PREFIX struct
{
   uint8    DormantHandoffOptSet;  /* 0-Disabled, 1-Enabled */
} PACKED_POSTFIX  HlpDormantHandoffOptSet_APIStruct;

typedef PACKED_PREFIX struct
{
   uint8    DataScrmEnabled;   /* Supp Chan Supported */
} PACKED_POSTFIX  HlpDataScrmEnabled_APIStruct;

typedef PACKED_PREFIX struct
{
   uint8    DataTrtlEnabled;   /* Slow Down data at high CPU utilization */
} PACKED_POSTFIX  HlpDataTrtlEnabled_APIStruct;

typedef PACKED_PREFIX struct
{
   uint8    PrefRc; /* Preferred Radio Cfg. Default-RC3 */
} PACKED_POSTFIX  HlpPrefRc_APIStruct;

typedef PACKED_PREFIX struct
{
   uint8    VJCompressionEnabled;
} PACKED_POSTFIX  HlpVJCompressionEnabled_APIStruct;

typedef PACKED_PREFIX struct
{
   uint8    DialString[HLP_NUM_DIAL_STRING_DIGITS];
} PACKED_POSTFIX  HlpDialString_APIStruct;

typedef PACKED_PREFIX struct
{
   bool     QNC_Enabled;
} PACKED_POSTFIX  HlpQncEnabled_APIStruct;

typedef PACKED_PREFIX struct
{
   uint8    MDR_Mode;
} PACKED_POSTFIX  HlpMdrMode_APIStruct;

typedef PACKED_PREFIX struct
{
   bool DoTo1X_Enabled;
} PACKED_POSTFIX   HlpDoTo1XEnabled_APIStruct;

typedef PACKED_PREFIX struct
{
   uint32   DNS_PRI_IP_ADDR;
} PACKED_POSTFIX  HlpDNSPRIIPADDR_APIStruct;

typedef PACKED_PREFIX struct
{
   uint32   DNS_SEC_IP_ADDR;
} PACKED_POSTFIX  HlpDNSSECIPADDR_APIStruct;

typedef PACKED_PREFIX struct
{
   HlpDNSPRIIPADDR_APIStruct PriIpAddr;
   HlpDNSSECIPADDR_APIStruct SecIpAddr;
} PACKED_POSTFIX  HlpDNSIPADDR_APIStruct;

typedef PACKED_PREFIX struct
{
  uint32 ActiveProfile;
} PACKED_POSTFIX  HlpActiveProfile_APIStruct;

typedef PACKED_PREFIX struct
{
  uint8    ProfileId;
  bool     Profile_Valid;
} PACKED_POSTFIX  HlpProfileValid_APIStruct;

typedef PACKED_PREFIX struct
{
   uint8    NumProfiles;
} PACKED_POSTFIX  HlpNumProfiles_APIStruct;

typedef PACKED_PREFIX struct
{
   uint8    MipMode;
} PACKED_POSTFIX  HlpMipMode_APIStruct;

typedef PACKED_PREFIX struct
{
   uint8    SIP_PASSWORD[HLP_MAX_PSWD_LEN];
} PACKED_POSTFIX  HlpSipPassword_APIStruct;

typedef PACKED_PREFIX struct
{
   uint8    AN_PASSWORD[HLP_MAX_PSWD_LEN];
} PACKED_POSTFIX  HlpAnPassword_APIStruct;

typedef PACKED_PREFIX struct
{
   uint8    AN_PASSWORD_LEN;
   uint8    AN_PASSWORD[HLP_MAX_PSWD_LEN];
} PACKED_POSTFIX  HlpAnBinaryPassword_APIStruct;

typedef PACKED_PREFIX struct
{
   uint8    ProfileId;
   uint8    MN_AAA_PASSWORD_LEN;
   uint8    MN_AAA_PASSWORD[HLP_MN_PASSWD_MAX_SIZE];
   uint8    MN_HA_PASSWORD_LEN;
   uint8    MN_HA_PASSWORD[HLP_MN_PASSWD_MAX_SIZE];
} PACKED_POSTFIX  HlpMIPPasswordHex_APIStruct;

typedef PACKED_PREFIX struct
{
   uint8    ProfileId;
   uint8    MN_HA_PASSWORD[HLP_MN_PASSWD_MAX_SIZE];
   uint8    MN_AAA_PASSWORD[HLP_MN_PASSWD_MAX_SIZE];
} PACKED_POSTFIX  HlpMIPPasswordString_APIStruct;

typedef PACKED_PREFIX struct
{
   uint8    SIP_PASSWORD_LEN;
   uint8    SIP_PASSWORD[HLP_MAX_PSWD_LEN];
} PACKED_POSTFIX  HlpSIPPasswordHex_APIStruct;

typedef PACKED_PREFIX struct
{
   uint8    ProfileId;   /* Init the data to defaults, for this ProfileId */
} PACKED_POSTFIX  HlpSecureSegInit_APIStruct;

typedef PACKED_PREFIX struct
{
    bool HspdLocked;
} PACKED_POSTFIX  HlpHspdLockGet_APIStruct;

typedef PACKED_PREFIX struct
{
    uint16  LockCode;
    bool    Lock;
} PACKED_POSTFIX  HlpHspdLockSet_APIStruct;

typedef PACKED_PREFIX struct
{
    uint16  CurrentLockCode;
    uint16  NewLockCode;
} PACKED_POSTFIX  HlpHspdLockCode_APIStruct;

typedef PACKED_PREFIX struct
{
    bool Enable; /*   TRUE: Scan the serial link for PPP packets
           Default is FALSE: Wait for ATD #777*/
} PACKED_POSTFIX  HlpDataAutoPacketDetection_APIStruct;

typedef PACKED_PREFIX struct
{
    bool HspdNetworkMode;
} PACKED_POSTFIX  HlpHspdNetworkMode_APIStruct;

typedef PACKED_PREFIX struct
{
   uint16 T_Dormancy;
   uint16 T_HRPD_Search;
   uint16 T_Rapid_Dormancy;
   uint16 T_ResDef_Dormancy;
#ifdef MTK_CBP
   /* refer to DormParmCheckT enum
    * bit3: T_ResDef_Dormancy; bit2: T_Rapid_Dormancy; bit1: T_HRPD_Search; bit0: T_Dormancy.
    * if 1, need check
    */
   DormParmCheckT Check_Flag;
#endif
} PACKED_POSTFIX HlpDormantTimerData_APIStruct;

typedef PACKED_PREFIX struct
{
   uint16 T_DO_Retry;
} PACKED_POSTFIX HlpDoRetryTimerData_APIStruct;

typedef PACKED_PREFIX struct {
   uint16 T_Dormancy;
} PACKED_POSTFIX HlpValCtaUpdateReqMsgT;

typedef enum
{
  HLP_DATA_SO_PRE701=0,
  HLP_DATA_SO_QP=1,
  HLP_DATA_SO_IS701=2
} HlpDataSOType;

typedef PACKED_PREFIX struct
{
  HlpDataSOType SOSetType;
} PACKED_POSTFIX  HlpDataSO_APIStruct;

typedef struct
{
   HlpParmOperationId ParmId;
   bool InProgress;
   bool RspNeeded;
   ExeRspMsgT  RspInfo;
   int16       RegId;
} HlpSecureRspType;

typedef struct {
   UINT8 N_DIGITS;
   UINT8 MDN[OTA_MAX_MDN_DIGITS];
} HlpPswMDNUpdatedMsgT;

#define MAX_CHAP_DATA_LEN 16

typedef PACKED_PREFIX struct{
   uint8 ChapId;
   uint8 ChapLen;
   uint8 ChapData[MAX_CHAP_DATA_LEN];
} PACKED_POSTFIX  HlpUimAccessChapTestMsgT;

typedef PACKED_PREFIX struct {
   uint16 T_Dormancy;
   uint16 T_HRPD_Search;
   uint16 T_Rapid_Dormancy;
   uint16 T_ResDef_Dormancy;
#ifdef MTK_CBP
   DormParmCheckT Check_Flag;
#endif
} PACKED_POSTFIX  HlpDormancyTimersSetMsgT;

typedef PACKED_PREFIX struct {
   uint16 T_DO_Retry;
} PACKED_POSTFIX  HlpDORetryTimersSetMsgT;

typedef HlpSecureRspType HlpSegRspType;
#endif


typedef enum
{
#ifdef MTK_CBP //MTK_DEV_C2K_IRAT
   InvalidIP=0,
#endif
   IPv4=1,
   IPv6,
   IPv4andIPv6,
}IPAddrTypeT;

#ifdef CBP7_IMS

/* BEGIN: RTP related */
typedef enum
{
   HLP_RTP_STREAM_TYPE_AUDIO,
   HLP_RTP_STREAM_TYPE_VIDEO
} HlpRtpStreamTypeE;

typedef enum
{
   HLP_RTP_STREAM_DIRECTION_INACTIVE,
   HLP_RTP_STREAM_DIRECTION_SENDONLY,
   HLP_RTP_STREAM_DIRECTION_RECVONLY,
   HLP_RTP_STREAM_DIRECTION_SENDRECV
} HlpRtpStreamDirectionE;

typedef enum
{
   HLP_RTP_ADDR_FAMILY_IPV4 = 2,
   HLP_RTP_ADDR_FAMILY_IPV6 = 28
} HlpRtpAddrFamilyE;

typedef PACKED_PREFIX struct
{
    uint8 SupportCodec1;       /* Whether peer support codec1 */
    uint8 Codec1LocalId;       /* Codec1's local identifier */
    uint8 Codec1PtNum;         /* Codec1's RTP payload type number */
    uint8 SupportCodec2;       /* Whether peer support codec2 */
    uint8 Codec2LocalId;       /* Codec2's local identifier */
    uint8 Codec2PtNum;         /* Codec2's RTP payload type number */
    uint8 SupportCodec3;       /* Whether peer support codec3 */
    uint8 Codec3LocalId;       /* Codec3's local identifier */
    uint8 Codec3PtNum;         /* Codec3's RTP payload type number */
    uint8 SupportCodec4	;      /* Whether peer support codec4 */
    uint8 Codec4LocalId	;      /* Codec4's local identifier */
    uint8 Codec4PtNum;         /* Codec4's RTP payload type number */
} PACKED_POSTFIX  MediaCapT;

typedef PACKED_PREFIX struct
{
    uint8 ListenEnable;
    uint8 OutputEnable;
    uint8 InputEnable;
    uint8 SendEnable;
} PACKED_POSTFIX  StreamCfgT;

typedef PACKED_PREFIX struct
{
   uint8 SockAddrFamily;
   uint8 SockFlowId;
   uint16 SockAddrPort;
   uint32 SockAddrIp[4];
} PACKED_POSTFIX  SockAddrT;

typedef PACKED_PREFIX struct{
   uint8 StreamId;
   uint8 StreamType;
   uint8 StreamDirection;
   SockAddrT LocalRtpAddr;
} PACKED_POSTFIX  HlpRtpLocalSdpMsgT;

typedef PACKED_PREFIX struct{
   uint8 StreamId;
   uint8 StreamType;
   uint8 StreamDirection;
   SockAddrT PeerRtpAddr;
   SockAddrT PeerRtcpAddr;
   MediaCapT PeerMCap;
} PACKED_POSTFIX  HlpRtpPeerSdpMsgT;

typedef PACKED_PREFIX struct{
   uint8 StreamId;
} PACKED_POSTFIX  HlpRtpDelStreamMsgT;

typedef PACKED_PREFIX struct{
   uint8 StreamId;
   StreamCfgT StreamConfig;
} PACKED_POSTFIX  HlpRtpConfigStreamMsgT;

typedef PACKED_PREFIX struct{
   uint8 StreamId;
   MediaCapT LocalMCap;
} PACKED_POSTFIX  HlpRtpSetLocalMCapMsgT;

typedef PACKED_PREFIX struct{
   int32 DataLen;
   uint8 DataBuf[1];
} PACKED_POSTFIX  HlpRtpRtpInputMsgT;

typedef PACKED_PREFIX struct{
   int32 DataLen;
   uint8 DataBuf[1];
} PACKED_POSTFIX  HlpRtpRtcpInputMsgT;

typedef PACKED_PREFIX struct{
   uint8 Src;
} PACKED_POSTFIX  HlpRtpInputSourceMsgT;

typedef PACKED_PREFIX struct{
   int32 DataLen;
   uint8 DataBuf[1];
} PACKED_POSTFIX  HlpRtpIp6InputMsgT;

/* END: RTP related */

/*Begin: SIP related*/
typedef PACKED_PREFIX struct
{
   SockAddrT LocalSipAddr;
} PACKED_POSTFIX  HlpSipActReq_t;

typedef struct
{
    uint8 Result;
    SockAddrT LocalSipAddr;
} HlpSipActCnf_t;

typedef struct
{
   uint8 Result;
   uint8 Filler [3];
} HlpSipDeactCnf_t, HlpSipSetCnf_t, HlpSipUnregisterCnf_t, HlpSipRegisterNotify_t;

typedef struct Host_t
{
   uint8 AddrType;
   uint8 Filler [3];
   uint8* h_name;
   SockAddrT h_Addr;
} Host_t;

typedef struct
{
   uint8 nat_type;            /*The type of nat*/
   uint8 Filler [3];
   Host_t Host;               /*Host ip:port or FQDN:port*/
   Host_t nat_mapped;         /*nat mapped ip:port*/
   Host_t sip_proxy;          /*first sip proxy*/
   Host_t sip_proxy2;         /*second sip proxy*/
   Host_t sip_registrar;      /*registrar*/
   uint8* username;           /*username*/
   uint8* password;           /*password*/
   Host_t pre_route;          /*pre route*/
   uint8  reservation_label;  /*reservation label */
   IPAddrTypeT AddrType;      /*IPv6 or IPv4*/
} HlpSipSetReq_t;

typedef struct
{
   int active;
} HlpSipDeactReq_t, HlpSipRegisterReq_t, HlpSipUnregisterReq_t;

typedef struct
{
   uint8 MediaType;           /*Audio, video or text*/
   uint8 Filler [3];
   MediaCapT   StreamMCap;    /*Stream media capability*/
} HlpSipSetLocalMCapReq_t;

typedef struct
{
   uint8 CI;                  /*Call identifier for current call, bit4 = 0 means MN defines it; bit4 = 1 means SIP.*/
   uint8 DI;                  /*Dialog identifier*/
   uint16 AudioCodecSet;
   uint16 VideoCodecSet;
   uint16 OtherCodecSet;
   uint16 AudioPortNum;       /*port number used in SDP*/
   uint16 VideoPortNum;       /*port number used in SDP*/
   uint16 CalleAddrLen;
   uint8 FirstByte[1];        /*Called party address.*/
} HlpSipSetupReq_t, HlpSipSetupInd_t;

typedef struct
{
   uint8 CI;                  /*Call identifier for current call, bit4 = 0 means MN defines it; bit4 = 1 means SIP.*/
   uint8 DI;                  /*Dialog identifier*/
   uint8 Result;
   uint8 Filler[1];

}HlpSipSetupCompleteInd_t, HlpSipRelInd_t, HlpSipHoldReq_t, HlpSipRetrieveReq_t;

typedef struct
{
   uint8 CI;                  /*Call identifier for current call, bit4 = 0 means MN defines it; bit4 = 1 means SIP.*/
   uint8 DI;                  /*Dialog identifier*/
   uint8 Result;
   uint8 Filler;

} HlpSipHoldCnf_t, HlpSipRetrieveCnf_t;

typedef struct
{
   uint8 CI;                  /*Call identifier for current call, bit4 = 0 means MN defines it; bit4 = 1 means SIP.*/
   uint8 DI;                  /*Dialog identifier*/
   uint8 SI;
   uint8 Filler;

} HlpSipSwitchSessionInd_t;

typedef struct
{
   uint8 CI;                  /* Call identifier*/
   uint8 DI;                  /* Dialog identifier*/
   uint8 SI;                  /* Session identifier (allocated by SIP), used to synchronize the video and audio stream.*/
   uint8 StreamSeqNum;        /* Sequence number of this stream (allocated by SIP)*/
   uint8 StreamEnable;        /* Stream's identifier (allocated by MN, and used by MN and RTP.*/
   uint8 MI;                  /* Stream's identifier (allocated by MN, and used by MN and RTP)*/
   uint8 StreamType;          /* Stream type, such as audio, video and text*/
   uint8 StreamDirection;     /* Stream direction, such as sendrecv, sendonly, recvonly, or inactive*/
   SockAddrT LocalAddr;       /* Stream local address, including IP address and port*/
}HlpSipLocalSDPInd_t;

typedef struct
{
   uint8 CI;                  /* Call identifier*/
   uint8 DI;                  /* Dialog identifier*/
   uint8 SI;                  /* Session identifier (allocated by SIP)*/
   uint8 StreamSeqNum;        /* Sequence number of this stream (allocated by SIP)*/
   uint16 version;
   uint8 MI;                  /* Stream's identifier (allocated by MN, and used by MN and RTP)*/
   uint8 StreamType;          /* Stream type, such as audio, video and text*/
   uint8 StreamDirection;     /* Stream direction, such as sendrecv, sendonly, recvonly, or inactive*/
   uint8 Filler[3];
   SockAddrT StreamAddr;      /* Stream address, including IP address and port*/
   SockAddrT StreamRtcpAddr;  /* Stream's  rtcp address (by default will be one above the rtp port)*/
   MediaCapT StreamMCap;      /* Stream media capability*/
} HlpSipPeerSDPInd_t;

typedef struct
{
   uint8 CI;                  /*Call identifier for current call, bit4 = 0 means MN defines it; bit4 = 1 means SIP.*/
   uint8 DI;                  /*Dialog identifier*/
   uint16 ResponseCode;       /*Response code to be sent by SIP*/
} HlpSipSetupCnf_t, HlpSipSetupRsp_t, HlpSipCallProcInd_t,
HlpSipModifyCnf_t, HlpSipModifyRsp_t;

typedef struct
{
   uint8 CI;                  /*Call identifier for current call, bit4 = 0 means MN defines it; bit4 = 1 means SIP.*/
   uint8 DI;                  /*Dialog identifier*/
   uint8 Ring;                /*TRUE or FALSE*/
   uint8 Filler;
} HlpSipCallProcReq_t;

typedef struct
{
   uint8 CI;                  /*Call identifier for current call, bit4 = 0 means MN defines it; bit4 = 1 means SIP.*/
   uint8 DI;                  /*Dialog identifier*/
   uint16 version;            /*modify version*/
   uint16 AudioCodecSet;
   uint16 VideoCodecSet;
   uint16 OtherCodecSet;
   uint8  Filler[2];
   uint32 StreamSet;
}HlpSipModifyReq_t;

typedef struct
{
   uint8 CI;                  /*Call identifier for current call, bit4 = 0 means MN defines it; bit4 = 1 means SIP.*/
   uint8 DI;                  /*Dialog identifier*/
   uint16 AudioCodecSet;
   uint16 VideoCodecSet;
   uint16 OtherCodecSet;
   uint32 StreamSet;
}HlpSipModifyInd_t, HlpSipHoldInd_t, HlpSipRetrieveInd_t;

typedef struct
{
   uint8 CI;                  /*Call identifier for current call, bit4 = 0 means MN defines it; bit4 = 1 means SIP.*/
   uint8 DI;                  /*Dialog identifier*/
   uint8 Cause;
   uint8 Filler;
}  HlpSipDiscReq_t;

typedef struct
{
   uint8 CI;
   uint8 Filler[3];
} HlpSipCalleeidRelInd_t;

typedef struct
{
   uint32 value;
} HlpSipSigcompSet_t;


typedef struct
{
   bool success;
}HlpSmsRdyMsg_t, HlpSmsSendRspMsg_t ;

typedef struct
{
   UINT16 id;
   UINT8 digits[32 ];
   UINT8 data[254];
   UINT8 length;
}HlpImsSmsMsg_t;

#define HLP_SIP_MI_INVALID_ID 0xFF

/*End: SIP related*/

#endif /* CBP7_IMS */

#ifdef CBP7_EHRPD
typedef PACKED_PREFIX struct
{
   uint8    AKA_PASSWORD[HLP_MAX_PSWD_LEN];
} PACKED_POSTFIX  HlpAkaPassword_APIStruct;

typedef PACKED_PREFIX struct
{
   HlpHspdAkaProfileDataT    AkaProfileData;
} PACKED_POSTFIX  HlpAkaProfileData_APIStruct;

typedef enum
{
   AKAAuth_Success,
   AKAAuth_AUTNFailure,
   AKAAuth_SQNSyncFailure,
   AKAAuth_RESInvalid,
   AKAAuth_CKInvalid,
   AKAAuth_IKInvalid,
   AKAAuth_UnknowFailure
#if EAP_AKA_PRIME
   ,
   AKAAuth_KDFIputInvalid,
   AKAAuth_KDFInvalid
#endif
} EapAkaAuthResultT;

typedef struct
{
   EapAkaAuthResultT authResult;
} HlpSecAkaAuthRspMsgT;
#endif

typedef struct
{
   bool bEncryptDecryptResult;
} HlpSecAesCryptRspMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;
   UINT8 FlowId;
   HlpIp6AddrTypeE AddrType;
   UINT16 LocalAddrPort;
   UINT16 PeerAddrPort;
   UINT32 PeerAddrIp[4];
} PACKED_POSTFIX  HlpIp6TcpConnectionOpenMsgT;

typedef PACKED_PREFIX struct
{
   UINT8 Sap;
} PACKED_POSTFIX  HlpTcpConnectionOpenRspMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;
   UINT16 LocalAddrPort;
   UINT16 PeerAddrPort;
   UINT32 PeerAddrIp;
} PACKED_POSTFIX  HlpIp4TcpConnectionOpenMsgT;

#ifdef CBP7_EHRPD
typedef struct
{
   bool eEHRPD;
} HlpImsEHRPDAttriMsgT;

/* IP6 related Begin */

typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;
   UINT8 FlowId;
   HlpIp6AddrTypeE AddrType;
   UINT16 LocalAddrPort;
   UINT16 PeerAddrPort;
   UINT32 PeerAddrIp[4];
   INT32  PayloadSize;
} PACKED_POSTFIX  HlpIp6UdpConnectionOpenMsgT;

typedef PACKED_PREFIX struct
{
   UINT8 Sap;
} PACKED_POSTFIX  HlpIp6UdpConnectionOpenRspMsgT;

typedef PACKED_PREFIX struct
{
   UINT8 Sap;
} PACKED_POSTFIX  HlpIp6UdpConnectionCloseMsgT;

typedef PACKED_PREFIX struct
{
   UINT8 Sap;
} PACKED_POSTFIX  HlpIp6TcpConnectionCloseMsgT;

/* IP6 related End */

typedef  struct
{
   uint16 MaxCID;
   bool LargeCIDs;
   bool FeedbackForIncluded;
   uint8 FeedbackFor;
   uint16 MRRU;
   uint8 ProfileCount;
   uint16 Profiles[MAX_PROTPARMSPROFILE_COUNT];
} FwdRohcProtocolParmsT;

typedef  struct
{
   uint16 MaxCID;
   bool LargeCIDs;
   bool FeedbackForIncluded;
   uint8 FeedbackFor;
   uint16 MRRU;
   bool TimerBasedCompression;
   uint8 ProfileCount;
   uint16 Profiles[MAX_PROTPARMSPROFILE_COUNT];
} RevRohcProtocolParmsT;

typedef enum
{
   PI_NULL     = 0x00,         /* NULL */
   PI_HDLC     = 0x01,         /* Octet-based HDLC-like framing */
   PI_IPV4     = 0x02,         /* IP version4 */
   PI_IPV6     = 0x03,         /* IP version6 */
   PI_ROHC     = 0x04,         /* ROHC */
   PI_IPV4V6   = 0x05,         /* IP version4 and Version6 */
   PI_HDLC_ALT = 0x06,         /* Octet-based HDLC-like framing  with AltPPP*/
   PI_EHRPD    = 0x07,         /* Octet based HDLC like framing supporting multiple PDNs*/
   PI_PDNMUX   = 0x08,         /* PDN-Mux (One octet PDN-ID prefix) for Internet Protocol (IP) PDNs*/
   PI_RESERVED
}  HlpProtocolIdentifierT;


typedef struct
{
   bool bRevRohcOnRlpFlow;
   HlpProtocolIdentifierT  RevProtcolID;
   RevRohcProtocolParmsT   RevRohcParms;
   bool bFwdRohcOnRlpFlow;
   HlpProtocolIdentifierT  FwdProtcolID;
   FwdRohcProtocolParmsT   FwdRohcParms;
} HlpRlpFlowProtocolInfoT;

typedef struct
{
   HlpRlpFlowProtocolInfoT RlpFlowProtocolInfo[MAX_HLP_FLOW_SUPPORTED];
} HlpRlpFlowProtocolInfoMsgT;

typedef struct
{
   uint8  curRoute;
}  HlpRlpRouteChangedMsgT;
#endif

typedef enum
{
   HLP_PPP_ROUTE_DEFAULT,
   HLP_PPP_ROUTE_NDIS
} HlpPppRouteTypeE;

typedef PACKED_PREFIX struct
{
   uint8 RouteType;
} PACKED_POSTFIX  HlpPppRouteTypeSetMsgT;

typedef enum
{
   PATH_HDLC,
   PATH_IP,
   PATH_ROHC,
   PATH_IP_WITH_PDNMUX
} HlpRlpDataPathT;

typedef struct
{
   NetworkT   network;
   SvcStatusT svcStatus;
} HlpSvcStatusMsgT;

typedef enum
{
    CONN_SUSPEND, /* keep full PPP context and only releases the AI connection */
    CONN_RESUME, /* for grammatical symmetry (no actual action now???) */
    CONN_DISABLE, /* keep partial PPP context and no AI interaction (AI connction
                   * is supposed to be released before) */
    CONN_RELEASE, /* release the PPP context and try to notify network */
    CONN_RESET /* release the PPP context and doesn't try to notify network */
} ConnCmdT;

typedef PACKED_PREFIX struct
{
  ConnCmdT   dataCmd;
} PACKED_POSTFIX  HlpDataConnectionCmdT;

typedef enum
{
   PowerUp,
   HybridRegistration,
   RTTOnlyRegistration,
   TriggerPDRegistration,
   DefaultAttach,
   SetIPv6Address,
   DedicateAttach,
   DetachAll,
   SendPING_IPv4,
   SendPING_IPv6
} ActionT;

typedef PACKED_PREFIX struct
{
  ActionT    myAction;
  uint32     Ipv4Src;
  uint32     Ipv4Dst;
  uint32     Ipv6Src[4];
  uint32     Ipv6Dst[4];
  uint8      PDNid;
  uint8      PDNtype;
  uint8      AttachType;
  uint8      APNstring[16];
} PACKED_POSTFIX  HlpTestBrowserConnectMsgT;

typedef struct
{
   bool bPowerUp;
} HlpPowerReqMsgT;


typedef enum
{
   HLP_DOANAUTH_CHINATELECOM,
   HLP_DOANAUTH_INDONESIASMART,
   HLP_DOANAUTH_CARRIER_MAX
} HlpDOANAuthT;

typedef struct
{
   HlpDOANAuthT   DOANAuthAlgo;
} HlpDOANAuthAlgoSetMsgT;

typedef struct
{
  uint32  SrcAddr;
  uint32  DstAddress;
  uint32  Length;
  uint8   Data[520];
}HlpValPingReqMsgT;

typedef PACKED_PREFIX struct
{
  uint8     Apn[100]; /* APN of PDN which need update its inactivity timer */
} PACKED_POSTFIX  HlpValUpdatePdnInactivityTimerT;

typedef enum
{
    HLP_VAL_SET_PCMT_MODE = 0,
    HLP_VAL_READ_PCMT_MODE,
    HLP_VAL_PCMT_INVALID_MODE = 255
}HlpValPcmtModeT;

typedef PACKED_PREFIX struct
{
    ExeRspMsgT      RspInfo;
    HlpValPcmtModeT mode;
    bool            pcmtIratSet;
    uint32          pcmtIratValue;
    bool            pcmtEhrpdSet;
    uint32          pcmtEhrpdValue;
} PACKED_POSTFIX  HlpValPcmtParaMsgT;

typedef enum
{
   NO_SILENTLY_RETRY, /*don't retry by modem, directly notify AP*/
   SILENTLY_AFRESH_CONN,/*re-establish connection*/
   SILENTLY_AFRESH_PS,/*re-establish connection and re-nego ppp*/
} HlpValRecoverFlag;


/*---------------------------------------------------------------
*  Declare global variables
*----------------------------------------------------------------*/
extern bool           PppFilterIpMux;

extern MuxInfo        FilterMuxInfo;
extern IPCounterStruct IPCounters;

/*---------------------------------------------------------------
*  Declare global function prototypes
*----------------------------------------------------------------*/

extern void PppHaDecode0Lisr (void);
extern void PppHaDecode1Lisr (void);
extern void PppHaEncode0Lisr (void);
extern void PppHaEncode1Lisr (void);
extern uint8 PppGetANAuthState(void);

extern void  HlpSetInternetPdnId(UINT8 PdnId);

/*****************************************************************************
* $Log: hlwapi.h $
* Revision 1.8  2006/01/03 10:11:47  wavis
* Merging in VAL.
* Revision 1.7.1.2  2005/11/07 14:52:30  wavis
* Merging in VAL/FSM.
* Revision 1.7  2005/05/04 12:42:07  dstecker
* Changed interface from RLP to HLW to contain multiple frames to decrease context switching.
* Revision 1.6  2005/04/15 18:43:45  chinh
* - Added new message id and structures.
* Revision 1.5  2005/03/18 10:04:17  dstecker
* Modifications for the 4.05 merge
* Revision 1.4  2004/07/08 17:11:20  blee
* Revision 1.2  2004/03/25 11:45:51  fpeng
* Updated from 6.0 CP 2.5.0
* Revision 1.3  2004/02/10 11:44:50  cmastro
* merge with 4.0 8.05.00
* Revision 1.13  2004/01/21 09:51:27  hhong
* Added number of socket open/close for both TCP/UDP test and operation interface.
* Revision 1.12  2003/12/11 15:12:33  hhong
* Added HLW_UM_ABORT_REQ_MSG.
* Revision 1.11  2003/08/19 18:18:59  hhong
* Created HLW_UM_PKT_ZONE_IND_MSG.
* Revision 1.10  2003/06/10 10:53:59  hhong
* Added HLW_RM_TX_RSP to allow IOP to ack HLW independently.
* Revision 1.9  2003/03/13 19:18:38  hhong
* Added various API to support onboard browser (TCP/UDP) application.
* Revision 1.8  2002/09/09 10:54:43  etarolli
* Added HL timer call back msg definition
* Revision 1.7  2002/06/04 08:07:10  mshaver
* Added VIA Technologies copyright notice.
* Revision 1.6  2002/02/27 20:47:19  mclee
* Changed around the #includes to solve a problem where .h files were including each other in a circular manner and thus would not compile.
* Revision 1.5  2002/02/27 10:54:16  etarolli
* Added 3 message definitions to support MIP functionality
* Revision 1.4  2002/01/29 17:17:50  hhong
* To add response info. to HlwTcpbConnReqMsgT.
* Revision 1.3  2001/09/21 14:27:53  hhong
* [Correct] To promote UINT8 to UINT16 on the inactiivity time out
* Revision 1.2  2001/09/21 14:17:46  hhong
* To promote inactivity timeout from UINT8 to UINT16
* Revision 1.1  2001/04/26 11:59:36  yfang
* Initial revision
* Revision 1.9  2001/01/23 21:03:30  hhong
* Add mechanism to support Network Layer Rm Interface Protocol Option for packet data.
* Revision 1.8  2001/01/13 10:33:05  hhong
* Added messages handlers for the support of Packet Data Network Layer Rm Interface.
* Revision 1.7  2000/11/07 09:21:13  yfang
* 1) Support TCP browser
* 2) Support FAX
* Revision 1.6  2000/10/09 23:48:33Z  cmastro
* added include files and definition of HLW_MAX_BWSR_DIG_LEN in terms of  CP_MAX_CALLING_PARTY_NUMBER_SIZE.
* Let's see if it sticks this time.
* Revision 1.5  2000/09/11 21:46:46Z  hhong
* Add HLW_TEST_BROWSER_CONNECT_MSG and its message
* handler for testing purpose from ETS menu.
* Revision 1.4  2000/05/19 01:41:15Z  hhong
* Revision 1.2.4.1.1.2  2000/05/18 18:32:13Z  hhong
* To update to Isotel's release 2.30
* Revision 1.2.4.1.1.1  2000/05/11 18:22:51Z  hhong
* Duplicate revision
* Revision 1.2.4.1  2000/05/11 18:22:51Z  hhong
* A duplicated version of 1.2.2.2
* Revision 1.2.2.2  2000/03/27 23:20:33Z  hhong
* Make max browser digit length to be 32 explicitly.
* Revision 1.2.2.1  1999/12/02 18:33:09Z  hhong
* Duplicate revision
* Revision 1.2  1999/12/02 18:33:09Z  cdma
* Updated comments for CBP3.0
*****************************************************************************/

/*****************************************************************************
* End of File
*****************************************************************************/
#endif




/**Log information: \main\CBP80\cbp80_xding_scbp10176\1 2012-08-30 06:36:04 GMT xding
** add an PRI item under HSPD menu for retry time on KDDI 2-19**/
/**Log information: \main\SMART\1 2013-04-22 09:36:00 GMT mwang
** HREF#22164: Support Indonesia smart network DO AN auth algo.**/
/**Log information: \main\Trophy\Trophy_wzhou_href22221_fix4\1 2013-06-25 01:55:50 GMT wzhou
** href#22221: merge PDSN ping and UI callback functionality.**/
/**Log information: \main\Trophy\2 2013-06-25 01:45:44 GMT jzwang
** href#22221_fix4**/
/**Log information: \main\Trophy\Trophy_yzhang_href22363\1 2014-01-15 03:57:27 GMT yzhang
** HREF#22363:Merge CT IPv6 req implementation.**/
/**Log information: \main\Trophy\3 2014-01-15 05:10:04 GMT jzwang
** href#22363**/
