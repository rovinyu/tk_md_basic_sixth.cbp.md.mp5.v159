/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef _HWDAPPSAPI_H_
#define _HWDAPPSAPI_H_
/*****************************************************************************
*
* FILE NAME   : HwdAppsApi.h
*
* DESCRIPTION : Defines the Multimedia Applications global definitions.
*
* HISTORY     :
*     See Log at end of file
*
*****************************************************************************/

#include "valapi.h"
#include "valdispapi.h"
#include "hwdaudioapi.h"

/*---------------------------------------------------------------
*  Global Definitions
*---------------------------------------------------------------*/
/* Apps device types */
typedef enum
{
   HWD_APPS_AUDIO_VOICE_DEVICE,
   HWD_APPS_AUDIO_MIDI_DEVICE,
   HWD_APPS_AUDIO_MMAPPS_DEVICE,
   HWD_APPS_AUDIO_VOC_MUSIC_DEVICE,
   HWD_APPS_AUDIO_DEV_MODE_NUM
} HwdAppsAudioDevModesT;

/* MMApps music types */       
typedef enum
{
    HWD_MMAPPS_MP3_MUSIC,
    HWD_MMAPPS_AAC_MUSIC,
    HWD_MMAPPS_MP3_IN_MP4_MUSIC,
    HWD_MMAPPS_AAC_IN_MP4_MUSIC,
    HWD_MMAPPS_WMA_MUSIC,
    HWD_MMAPPS_WAV_MUSIC,
    HWD_MMAPPS_WAV_ENCODER,
    HWD_MMAPPS_NUM_MUSIC_TYPES
} HwdMMAppsMusicTypeT;

/* HWD_MMAPPS_CHAN_DATA_MSG */
typedef PACKED_PREFIX struct
{
   uint16      ChannelId;
   uint16      DataSize;         /* Size in WORDS of data     */
   uint16      *DataP;           /* Use IpcDspAppChanDataT    */
} PACKED_POSTFIX  HwdMMAppsChanDataMsgT;

/* DSPv states */
typedef enum
{
    HWD_MMAPPS_DSPV_NOT_INITIALIZED,   /* DSPv code/apps not downloaded */
    HWD_MMAPPS_DSPV_CODE_DOWNLOADING,  /* DSPv code being downloaded */
    HWD_MMAPPS_DSPV_APP_DOWNLOADING,   /* DSPv code has been downloading, downloading 1st app */
    HWD_MMAPPS_DSPV_AUDIO_INIT,        /* DSPv code is performing audio initialization */
    HWD_MMAPPS_DSPV_READY              /* DSPv code and first app downloaded, DSPv is ready */
} HwdMMAppsDspvStateT;

/* Callback definition for sound-over and Display-done */
typedef void (*HwdAppsSoundOverFuncT) (ValAppStatusT Status);
typedef uint8 *(*HwdMMAppsMusicGetBufFuncT) (uint32 BufSize);

/*---------------------------------------------------------------
*  Apps Audio Functions
*---------------------------------------------------------------*/
extern void   HwdAppsAudioModeSet( HwdAppsAudioDevModesT DevMode, HwdAudioModeT Mode, 
                                   ExeRspMsgT *RspMsgP );
extern void   HwdAppsAudioRegisterCB (HwdAppsSoundOverFuncT FuncP);

/*---------------------------------------------------------------
*  MIDI Functions
*---------------------------------------------------------------*/
extern uint8 HwdMidiSetupMelody (const uint8  *BuffPtr, 
                                 uint32        BuffSize, 
                                 uint32        FileSize,      
                                 ValFsiHandleT FileHandle,
                                 uint8         Iterations);
extern bool  HwdMidiPlayFromOffset (uint8  NextPlaySongIdx, 
                                    int8   NumIterations, 
                                    uint32 StartOffsetMsec);
extern bool  HwdMidiDtmf( ValSndDtmfIdT DtmfId, uint8 Mode );
extern void  HwdMidiTone( ValSndToneIdT ToneId );
extern void  HwdMidiStop (IpcDsvRngrCntrlModeEnum RngrCtrlMode);
extern void  HwdMidiSuspend (void);
extern void  HwdMidiResume (void);
extern uint32 HwdMidiGetTotalPlayTime (ValFsiHandleT FileHandle, uint32 FileSize, 
                                       void *BuffP, uint32 BuffLen, ValMidiParserT Parser);
extern uint32 HwdMidiGetElapsedTime (void);

/*---------------------------------------------------------------
*  iMelody Functions
*---------------------------------------------------------------*/
extern bool  HwdIMelodyPlay( uint8 NextPlaySongIndex, int8 NumIterations );
extern void  HwdIMelodyStop (void);

/*---------------------------------------------------------------
*  CMF Functions
*---------------------------------------------------------------*/
extern bool  HwdCMFPlay( uint8 NextPlaySongIndex, int8 NumIterations );
extern void  HwdCMFStop (void);

/*---------------------------------------------------------------
 *  MMApps Functions
 *---------------------------------------------------------------*/
extern bool HwdMMAppsMusicPlay (HwdMMAppsMusicTypeT MusicType, 
                                uint8              *DataP, 
                                ValFsiHandleT       FileHandle,
                                uint32              FileSize, 
                                uint8               NumIterations, 
                                uint32              BaseByteOffset, 
                                uint32              StartingByteOffset);

extern bool HwdMMAppsMusicRecord (HwdMMAppsMusicTypeT MusicType, 
                                  ValFsiHandleT       FileHandle,
                                  DacSamplingRatesT   SamplingRate, 
                                  uint8               NumChans);
extern void HwdMMAppsMusicStop (void);
extern void HwdMMAppsMusicFastFwd (uint16 PlayTime, uint16 AdvanceTime);
extern void HwdMMAppsMusicRewind (uint16 PlayTime, uint16 RewindTime);
extern void HwdMMAppsMusicSuspend (void);
extern void HwdMMAppsMusicResume (void);
extern void HwdMMAppsMusicStatusReg (ExeTaskIdT TaskId, ExeMailboxIdT Mailbox, uint32 MsgId);
extern void HwdMMAppsMusicGetBufRegCB (HwdMMAppsMusicGetBufFuncT FuncP);
extern uint32 HwdMMAppsMp3GetCurrFrame (void);
extern void HwdMMAppsMp3HeaderInfo (uint8 Version, uint8 Layer, bool VbrFlag, 
                                    uint32 TotPlayTime, uint32 NumFrames);

extern ExeHisrT HwdMMAppsHisrCb;
extern HwdMMAppsDspvStateT HwdMMAppsGetDspvState (void);
extern IpcDspAppIdT HwdMMAppsGetCurrApp (void);
extern bool   HwdMMAppsGetMusicStatusSemReq (void);
extern uint32 HwdMMAppsGetMusicRecordSeqCnt (void);

/*---------------------------------------------------------------
 *  VOCODER Functions
 *---------------------------------------------------------------*/
extern void HwdMMAppsVocoderDnld (IpcSpchSrvcOptT SrvcOpt, bool ForceDnld);
extern bool HwdMMAppsSetVocoder (IpcSpchSrvcOptT SrvcOpt);
extern IpcSpchSrvcOptT HwdMMAppsGetVocoder (void);
extern void HwdMMAppsResetVocoderApp (void);
extern bool HwdMMAppsIsResetInProg (void);
extern void HwdMMAppsAudioInitComplete (void);

/*---------------------------------------------------------------
 *  TONE Functions
 *---------------------------------------------------------------*/

/* TONE API functions */
extern void HwdTonePlay (ValSndToneIdT ToneId, uint8 NumIterations);
extern void HwdUserTonePlay (ValSndToneDataT *ToneDataP, uint16 NumTonesInSeq, uint16 Iterations);
extern void HwdTonePlayIterations (ValSndToneIdT ToneId, uint8 Iterations);
extern bool HwdToneDtmf (ValSndDtmfIdT DtmfId, uint8 Mode, uint16 Length);
extern void HwdToneStop (IpcToneModeT StopMode);

/* TONE Functions used by tasks other than HWD */
extern bool HwdToneIsActive (void);
extern void HwdToneSrvcOptDisconnecting (void);

/*---------------------------------------------------------------
*  External Micronas MP3 Functions 
*---------------------------------------------------------------*/
extern bool HwdExtMp3Play (void         *DataP, 
                           ValFsiHandleT FileHandle,
                           uint32        FileSize,
                           uint8         NumIterations,
                           uint32        BaseOffset,
                           uint32        StartingOffset);
extern void HwdExtMp3Stop (void);

#endif   /* _HWDAPPSAPI_H_  */


/**Log information: \main\5 2012-03-14 07:08:55 GMT hbi
** HREF#0000: update SD review code (Audio)**/
