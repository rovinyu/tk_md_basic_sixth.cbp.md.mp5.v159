/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*******************************************************************************
 * Filename:
 * ---------
 * nvram_data_items_id.h
 *
 * Project:
 * --------
 *   C2K JADE
 *
 * Description:
 * ------------
 *    This file defines logical data items ID stored in NVRAM.
 *
 *****************************************************************************/
#ifndef __NVRAM_DATA_ITEMS_ID_H__
#define __NVRAM_DATA_ITEMS_ID_H__

typedef enum
{
   /* System record, keep the system version */
   NVRAM_EF_SYS_LID,
   /* Branch record, keep the branch version */
   NVRAM_EF_BRANCH_VERNO_LID,
   /* Flavor record, keep the flavor version */
   NVRAM_EF_FLAVOR_VERNO_LID,

   NVRAM_EF_IMPT_COUNTER_LID,

   NVRAM_EF_EXT_SYS_VERNO_LID,
   
#ifdef MTK_NVRAM_CHECKSUM_LID_LIST
   NVRAM_EF_CHECKSUM_LID,
#endif

    /*L4 nvram LID*/
   NVRAM_EF_L4_START  = 0x100,
   NVRAM_EF_PSW_NAM1_LID = NVRAM_EF_L4_START,
   NVRAM_EF_PSW_MS_CAP_LID,                                                      /* 0x101*/
   NVRAM_EF_UICC_LID,                                                            /* 0x102*/
   NVRAM_EF_PSW_NAM2_LID,                                                        /* 0x103*/
   NVRAM_EF_SECURE_DATA_LID,                                                     /* 0x104*/
   NVRAM_EF_DO_DATA_LID,                                                         /* 0x105*/
   NVRAM_EF_UI_MISC_LID,                                                         /* 0x106*/
   NVRAM_EF_HLP_IPCOUNTERS_LID,                                                  /* 0x107*/
   NVRAM_EF_PSW_MRU1_LID,                                                        /* 0x108*/
   NVRAM_EF_PSW_MRU2_LID,                                                        /* 0x109*/
   NVRAM_EF_DO_MRU_LID,                                                          /* 0x10a*/
   NVRAM_EF_CSS_1X_LID,                                                          /* 0x10b*/
   NVRAM_EF_CSS_DO_LID,                                                          /* 0x10c*/
   NVRAM_EF_CSS_MISC_LID,                                                        /* 0x10d*/
   NVRAM_EF_PSW_MISC_LID,                                                        /* 0x10e*/
   NVRAM_EF_CUSTOMIZE_LID,                                                       /* 0x10f*/
   NVRAM_EF_HSPD_LID,                                                            /* 0x110*/
   NVRAM_EF_HSPD_SECURE_LID,                                                     /* 0x111*/
   NVRAM_EF_EHRPD_LID,                                                           /* 0x112*/
   
   /* database nvram LID*/
   NVRAM_EF_PRL1_LID,                                                            /* 0x113*/
   NVRAM_EF_PRL2_LID,                                                            /* 0x114*/
   NVRAM_EF_ATC_PARMS_LID,                                                       /* 0x115*/
   NVRAM_EF_ATC_CUST_PARMS_LID,                                                  /* 0x116*/
   NVRAM_EF_ERI1_LID,                                                            /* 0x117*/
   NVRAM_EF_ERI2_LID,                                                            /* 0x118*/
   NVRAM_EF_DMUPUBKEY1_LID,                                                      /* 0x119*/
   NVRAM_EF_DMUPUBKEY2_LID,                                                      /* 0x11a*/
   NVRAM_EF_DMUPUBKEY_ORG_LID,                                                   /* 0x11b*/
   NVRAM_EF_EXT_GPS_LID,                                                         /* 0x11c*/
   NVRAM_EF_PGPS_PDA_LID,                                                        /* 0x11d*/
   NVRAM_EF_GPS_HASH_LID,                                                        /* 0x11e*/
   NVRAM_EF_C2K_SBP_STATUS_CONFIG_LID,                                           /* 0x11f*/
   NVRAM_EF_C2K_SBP_DATA_CONFIG_LID,                                             /* 0x120*/

   /* Log FIlter Config. */ 
   NVRAM_EF_LOG_CONTROL_LID,                                                     /* 0x121*/   
   NVRAM_EF_CP_TRACE_FILTER_CONFIG_LID,                                          /* 0x122*/   
   NVRAM_EF_CP_SPY_FILTER_CONFIG_LID,                                            /* 0x123*/   
   NVRAM_EF_DSPM_TRACE_FILTER_CONFIG_LID,                                        /* 0x124*/   
   NVRAM_EF_DSPM_SPY_FILTER_CONFIG_LID,                                          /* 0x125*/   

   NVRAM_EF_UICC_CARD_DATA_LID,                                                  /* 0x126*/
   /* DAN DCN */
   NVRAM_EF_VAL_DANDCN_LID,                                                      /* 0x127*/      
   NVRAM_EF_SIDB_DATA_LID,                                                       /* 0x128*/

   NVRAM_EF_VAL_ACTIVED_BAND_DATA_LID,                                           /* 0x129*/
   NVRAM_EF_VAL_SMS_CBS_LID,                                                     /* 0X12A */
   NVRAM_EF_PSW_DBM_CRYPT_DATA_LID,                                              /* 0x12B*/

   /* NVRAM_EF_L4_END */

/* L1 and RF nvram LID */
   NVRAM_EF_L1_START = 0x200,
   NVRAM_EF_HWD_D1_AFC_PARMS_LID = NVRAM_EF_L1_START,
   NVRAM_EF_HWD_TEMPERATURE_PARMS_LID,                                           /* 0x201 */
   NVRAM_EF_HWD_BAND_A_TXAGC_LID,                                                /* 0x202 */
   NVRAM_EF_HWD_BAND_A_TXAGC_FREQ_ADJ_LID,                                       /* 0x203 */
   NVRAM_EF_HWD_BAND_A_TXAGC_TEMP_ADJ_LID,                                       /* 0x204 */
   NVRAM_EF_HWD_BAND_A_TX_HDET_LID,                                              /* 0x205 */
   NVRAM_EF_HWD_BAND_A_TX_HDET_FREQ_ADJ_LID,                                     /* 0x206 */
   NVRAM_EF_HWD_BAND_A_RXAGC_MULTIGAIN_FREQ_ADJ_LID,                             /* 0x207 */
   NVRAM_EF_HWD_BAND_A_RXAGC_TEMP_ADJ_LID,                                       /* 0x208 */
   NVRAM_EF_HWD_BAND_A_DIV_RXAGC_MULTIGAIN_FREQ_ADJ_LID,                         /* 0x209 */
   NVRAM_EF_HWD_BAND_A_DIV_RXAGC_TEMP_ADJ_LID,                                   /* 0x20a */
   NVRAM_EF_HWD_BAND_A_GPS_CAL_LID,                                              /* 0x20b */
   NVRAM_EF_HWD_BAND_B_TXAGC_LID,                                                /* 0x20c */
   NVRAM_EF_HWD_BAND_B_TXAGC_FREQ_ADJ_LID,                                       /* 0x20d */
   NVRAM_EF_HWD_BAND_B_TXAGC_TEMP_ADJ_LID,                                       /* 0x20e */
   NVRAM_EF_HWD_BAND_B_TX_HDET_LID,                                              /* 0x20f */
   NVRAM_EF_HWD_BAND_B_TX_HDET_FREQ_ADJ_LID,                                     /* 0x210 */
   NVRAM_EF_HWD_BAND_B_RXAGC_MULTIGAIN_FREQ_ADJ_LID,                             /* 0x211 */
   NVRAM_EF_HWD_BAND_B_RXAGC_TEMP_ADJ_LID,                                       /* 0x212 */
   NVRAM_EF_HWD_BAND_B_DIV_RXAGC_MULTIGAIN_FREQ_ADJ_LID,                         /* 0x213 */
   NVRAM_EF_HWD_BAND_B_DIV_RXAGC_TEMP_ADJ_LID,                                   /* 0x214 */
   NVRAM_EF_HWD_BAND_B_GPS_CAL_LID,                                              /* 0x215 */
   NVRAM_EF_HWD_BAND_C_TXAGC_LID,                                                /* 0x216 */
   NVRAM_EF_HWD_BAND_C_TXAGC_FREQ_ADJ_LID,                                       /* 0x217 */
   NVRAM_EF_HWD_BAND_C_TXAGC_TEMP_ADJ_LID,                                       /* 0x218 */
   NVRAM_EF_HWD_BAND_C_TX_HDET_LID,                                              /* 0x219 */
   NVRAM_EF_HWD_BAND_C_TX_HDET_FREQ_ADJ_LID,                                     /* 0x21a */
   NVRAM_EF_HWD_BAND_C_RXAGC_MULTIGAIN_FREQ_ADJ_LID,                             /* 0x21b */
   NVRAM_EF_HWD_BAND_C_RXAGC_TEMP_ADJ_LID,                                       /* 0x21c */
   NVRAM_EF_HWD_BAND_C_DIV_RXAGC_MULTIGAIN_FREQ_ADJ_LID,                         /* 0x21d */
   NVRAM_EF_HWD_BAND_C_DIV_RXAGC_TEMP_ADJ_LID,                                   /* 0x21e */
   NVRAM_EF_HWD_BAND_C_GPS_CAL_LID,                                              /* 0x21f */
   NVRAM_EF_HWD_GPS_CAL_LID,                                                     /* 0x220 */
   NVRAM_EF_HWD_BAND_D_GPS_CAL_LID,                                              /* 0x221 */
   NVRAM_EF_HWD_BAND_E_GPS_CAL_LID,                                              /* 0x222 */
   NVRAM_EF_HWD_RF_CUST_DATA_LID,                                                /* 0x223 */
   NVRAM_EF_HWD_RF_CUST_TAS_LID,                                                 /* 0x224 */
   NVRAM_EF_HWD_RF_CUST_LAST_LID,                                                /* 0x225 */
   
   /* LID for JADE RF*/
   NVRAM_EF_HWD_AFC_PARMS_LID,                                                   /* 0x226 */
   NVRAM_EF_HWD_TEMP_ADC_LID,                                                    /* 0x227 */
   NVRAM_EF_HWD_TX_TPC_LEVEL_BAND_A_LID,                                         /* 0x228 */
   NVRAM_EF_HWD_TX_TPC_LEVEL_BAND_B_LID,                                         /* 0x229 */
   NVRAM_EF_HWD_TX_TPC_LEVEL_BAND_C_LID,                                         /* 0x22a */
   NVRAM_EF_HWD_TX_TPC_LEVEL_BAND_D_LID,                                         /* 0x22b */
   NVRAM_EF_HWD_TX_TPC_LEVEL_BAND_E_LID,                                         /* 0x22c */
   NVRAM_EF_HWD_TX_PAGAIN_COMP_1XRTT_BAND_A_LID,                                 /* 0x22d */
   NVRAM_EF_HWD_TX_PAGAIN_COMP_1XRTT_BAND_B_LID,                                 /* 0x22e */
   NVRAM_EF_HWD_TX_PAGAIN_COMP_1XRTT_BAND_C_LID,                                 /* 0x22f */
   NVRAM_EF_HWD_TX_PAGAIN_COMP_1XRTT_BAND_D_LID,                                 /* 0x230 */
   NVRAM_EF_HWD_TX_PAGAIN_COMP_1XRTT_BAND_E_LID,                                 /* 0x231 */
   NVRAM_EF_HWD_TX_PAGAIN_COMP_EVDO_BAND_A_LID,                                  /* 0x232 */
   NVRAM_EF_HWD_TX_PAGAIN_COMP_EVDO_BAND_B_LID,                                  /* 0x233 */
   NVRAM_EF_HWD_TX_PAGAIN_COMP_EVDO_BAND_C_LID,                                  /* 0x234 */
   NVRAM_EF_HWD_TX_PAGAIN_COMP_EVDO_BAND_D_LID,                                  /* 0x235 */
   NVRAM_EF_HWD_TX_PAGAIN_COMP_EVDO_BAND_E_LID,                                  /* 0x236 */
   NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_BAND_A_LID,                                  /* 0x237 */
   NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_BAND_B_LID,                                  /* 0x238 */
   NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_BAND_C_LID,                                  /* 0x239 */
   NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_BAND_D_LID,                                  /* 0x23a */
   NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_BAND_E_LID,                                  /* 0x23b */
   NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_1XRTT_BAND_A_LID,                       /* 0x23c */
   NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_1XRTT_BAND_B_LID,                       /* 0x23d */
   NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_1XRTT_BAND_C_LID,                       /* 0x23e */
   NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_1XRTT_BAND_D_LID,                       /* 0x23f */
   NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_1XRTT_BAND_E_LID,                       /* 0x240 */
   NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_EVDO_BAND_A_LID,                        /* 0x241 */
   NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_EVDO_BAND_B_LID,                        /* 0x242 */
   NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_EVDO_BAND_C_LID,                        /* 0x243 */
   NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_EVDO_BAND_D_LID,                        /* 0x244 */
   NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_EVDO_BAND_E_LID,                        /* 0x245 */
   NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_HPM_BAND_A_LID,                                /* 0x246 */
   NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_HPM_BAND_B_LID,                                /* 0x247 */
   NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_HPM_BAND_C_LID,                                /* 0x248 */
   NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_HPM_BAND_D_LID,                                /* 0x249 */
   NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_HPM_BAND_E_LID,                                /* 0x24a */
   NVRAM_EF_HWD_DIV_RX_PATH_LOSS_HPM_BAND_A_LID,                                 /* 0x24b */
   NVRAM_EF_HWD_DIV_RX_PATH_LOSS_HPM_BAND_B_LID,                                 /* 0x24c */
   NVRAM_EF_HWD_DIV_RX_PATH_LOSS_HPM_BAND_C_LID,                                 /* 0x24d */
   NVRAM_EF_HWD_DIV_RX_PATH_LOSS_HPM_BAND_D_LID,                                 /* 0x24e */
   NVRAM_EF_HWD_DIV_RX_PATH_LOSS_HPM_BAND_E_LID,                                 /* 0x24f */
   NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_HPM_BAND_A_LID,                                /* 0x250 */
   NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_HPM_BAND_B_LID,                                /* 0x251 */
   NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_HPM_BAND_C_LID,                                /* 0x252 */
   NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_HPM_BAND_D_LID,                                /* 0x253 */
   NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_HPM_BAND_E_LID,                                /* 0x254 */
   NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_LPM_BAND_A_LID,                                /* 0x255 */
   NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_LPM_BAND_B_LID,                                /* 0x256 */
   NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_LPM_BAND_C_LID,                                /* 0x257 */
   NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_LPM_BAND_D_LID,                                /* 0x258 */
   NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_LPM_BAND_E_LID,                                /* 0x259 */
   NVRAM_EF_HWD_DIV_RX_PATH_LOSS_LPM_BAND_A_LID,                                 /* 0x25a */
   NVRAM_EF_HWD_DIV_RX_PATH_LOSS_LPM_BAND_B_LID,                                 /* 0x25b */
   NVRAM_EF_HWD_DIV_RX_PATH_LOSS_LPM_BAND_C_LID,                                 /* 0x25c */
   NVRAM_EF_HWD_DIV_RX_PATH_LOSS_LPM_BAND_D_LID,                                 /* 0x25d */
   NVRAM_EF_HWD_DIV_RX_PATH_LOSS_LPM_BAND_E_LID,                                 /* 0x25e */
   NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_LPM_BAND_A_LID,                                /* 0x25f */
   NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_LPM_BAND_B_LID,                                /* 0x260 */
   NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_LPM_BAND_C_LID,                                /* 0x261 */
   NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_LPM_BAND_D_LID,                                /* 0x262 */
   NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_LPM_BAND_E_LID,                                /* 0x263 */
   NVRAM_EF_HWD_GPS_RXAGC_TEMP_ADJ_LID,                                          /* 0x264 */
   NVRAM_EF_HWD_GPS_CHAR_DB_LID,                                                 /* 0x265 */
   NVRAM_EF_HWD_AFC_COMP_PARMS_LID,                                              /* 0x266 */


   /*MIPI LID*/
   NVRAM_EF_HWD_MIPI_PARAM_LID = 0x271,                                          /* 0x271*/
   NVRAM_EF_HWD_MIPI_INITIAL_CW_LID,                                             /* 0x272*/
   NVRAM_EF_HWD_MIPI_SLEEP_CW_LID,                                               /* 0x273*/
   NVRAM_EF_HWD_MIPI_USID_CHANGE_TABLE_LID,                                      /* 0x274*/
   NVRAM_EF_HWD_BAND_A_MIPI_RX_EVENT_LID,                                        /* 0x275*/
   NVRAM_EF_HWD_BAND_B_MIPI_RX_EVENT_LID,                                        /* 0x276*/
   NVRAM_EF_HWD_BAND_C_MIPI_RX_EVENT_LID,                                        /* 0x277*/
   NVRAM_EF_HWD_BAND_D_MIPI_RX_EVENT_LID,                                        /* 0x278*/
   NVRAM_EF_HWD_BAND_E_MIPI_RX_EVENT_LID,                                        /* 0x279*/
   NVRAM_EF_HWD_BAND_A_MIPI_RX_DATA_LID,                                         /* 0x27a*/
   NVRAM_EF_HWD_BAND_B_MIPI_RX_DATA_LID,                                         /* 0x27b*/
   NVRAM_EF_HWD_BAND_C_MIPI_RX_DATA_LID,                                         /* 0x27c*/
   NVRAM_EF_HWD_BAND_D_MIPI_RX_DATA_LID,                                         /* 0x27d*/
   NVRAM_EF_HWD_BAND_E_MIPI_RX_DATA_LID,                                         /* 0x27e*/
   NVRAM_EF_HWD_BAND_A_MIPI_TX_EVENT_LID,                                        /* 0x27f*/
   NVRAM_EF_HWD_BAND_B_MIPI_TX_EVENT_LID,                                        /* 0x280*/
   NVRAM_EF_HWD_BAND_C_MIPI_TX_EVENT_LID,                                        /* 0x281*/
   NVRAM_EF_HWD_BAND_D_MIPI_TX_EVENT_LID,                                        /* 0x282*/
   NVRAM_EF_HWD_BAND_E_MIPI_TX_EVENT_LID,                                        /* 0x283*/
   NVRAM_EF_HWD_BAND_A_MIPI_TX_DATA_LID,                                         /* 0x284*/
   NVRAM_EF_HWD_BAND_B_MIPI_TX_DATA_LID,                                         /* 0x285*/
   NVRAM_EF_HWD_BAND_C_MIPI_TX_DATA_LID,                                         /* 0x286*/
   NVRAM_EF_HWD_BAND_D_MIPI_TX_DATA_LID,                                         /* 0x287*/
   NVRAM_EF_HWD_BAND_E_MIPI_TX_DATA_LID,                                         /* 0x288*/
   NVRAM_EF_HWD_BAND_A_MIPI_TPC_EVENT_LID,                                       /* 0x289*/
   NVRAM_EF_HWD_BAND_B_MIPI_TPC_EVENT_LID,                                       /* 0x28a*/
   NVRAM_EF_HWD_BAND_C_MIPI_TPC_EVENT_LID,                                       /* 0x28b*/
   NVRAM_EF_HWD_BAND_D_MIPI_TPC_EVENT_LID,                                       /* 0x28c*/
   NVRAM_EF_HWD_BAND_E_MIPI_TPC_EVENT_LID,                                       /* 0x28d*/
   NVRAM_EF_HWD_BAND_A_MIPI_TPC_DATA_LID,                                        /* 0x28e*/
   NVRAM_EF_HWD_BAND_B_MIPI_TPC_DATA_LID,                                        /* 0x28f*/
   NVRAM_EF_HWD_BAND_C_MIPI_TPC_DATA_LID,                                        /* 0x290*/
   NVRAM_EF_HWD_BAND_D_MIPI_TPC_DATA_LID,                                        /* 0x291*/
   NVRAM_EF_HWD_BAND_E_MIPI_TPC_DATA_LID,                                        /* 0x292*/
   NVRAM_EF_HWD_BAND_A_MIPI_PA_SECTION_DATA_LID,                                 /* 0x293*/
   NVRAM_EF_HWD_BAND_B_MIPI_PA_SECTION_DATA_LID,                                 /* 0x294*/
   NVRAM_EF_HWD_BAND_C_MIPI_PA_SECTION_DATA_LID,                                 /* 0x295*/
   NVRAM_EF_HWD_BAND_D_MIPI_PA_SECTION_DATA_LID,                                 /* 0x296*/
   NVRAM_EF_HWD_BAND_E_MIPI_PA_SECTION_DATA_LID,                                 /* 0x297*/
   NVRAM_EF_HWD_RF_CUST_BPI_MASK_LID,                                            /* 0x298*/
   NVRAM_EF_HWD_RF_CUST_BPI_DATA_LID,                                            /* 0x299*/

   /*JADE POC LID*/
   NVRAM_EF_HWD_POC_STATE_LID,                                                   /* 0x29A*/
   NVRAM_EF_HWD_POC_BAND_A_LID,                                                  /* 0x29B*/
   NVRAM_EF_HWD_POC_BAND_B_LID,                                                  /* 0x29C*/
   NVRAM_EF_HWD_POC_BAND_C_LID,                                                  /* 0x29D*/
   NVRAM_EF_HWD_POC_BAND_D_LID,                                                  /* 0x29E*/
   NVRAM_EF_HWD_POC_BAND_E_LID,                                                  /* 0x29F*/

   /*MIPI new add LID*/
   NVRAM_EF_HWD_BAND_A_MIPI_PA_SECTION_DATA_1XRTT_LID,                           /* 0x2A0*/
   NVRAM_EF_HWD_BAND_B_MIPI_PA_SECTION_DATA_1XRTT_LID,                           /* 0x2A1*/
   NVRAM_EF_HWD_BAND_C_MIPI_PA_SECTION_DATA_1XRTT_LID,                           /* 0x2A2*/
   NVRAM_EF_HWD_BAND_D_MIPI_PA_SECTION_DATA_1XRTT_LID,                           /* 0x2A3*/
   NVRAM_EF_HWD_BAND_E_MIPI_PA_SECTION_DATA_1XRTT_LID,                           /* 0x2A4*/
   NVRAM_EF_HWD_BAND_A_MIPI_PA_SECTION_DATA_EVDO_LID,                            /* 0x2A5*/
   NVRAM_EF_HWD_BAND_B_MIPI_PA_SECTION_DATA_EVDO_LID,                            /* 0x2A6*/
   NVRAM_EF_HWD_BAND_C_MIPI_PA_SECTION_DATA_EVDO_LID,                            /* 0x2A7*/
   NVRAM_EF_HWD_BAND_D_MIPI_PA_SECTION_DATA_EVDO_LID,                            /* 0x2A8*/
   NVRAM_EF_HWD_BAND_E_MIPI_PA_SECTION_DATA_EVDO_LID,                            /* 0x2A9*/

   NVRAM_EF_HWD_RF_FILE_CAL_STATE_LID,                                           /* 0x2AA */

   /* DRDI LID */
   NVRAM_EF_HWD_DRDI_PARAM_LID = 0x300,                                          /* 0x300 */
   NVRAM_EF_HWD_DRDI_PARAM2_LID,                                                 /* 0x301 */
   NVRAM_EF_HWD_DRDI_REMAP_TABLE_LID,                                            /* 0x302 */

   /* A-GPS LID */
   NVRAM_EF_HWD_AGPS_GRP_DLY_BAND_A_LID = 0x305,                                 /* 0x305 */
   NVRAM_EF_HWD_AGPS_GRP_DLY_BAND_B_LID,                                         /* 0x306 */
   NVRAM_EF_HWD_AGPS_GRP_DLY_BAND_C_LID,                                         /* 0x307 */
   NVRAM_EF_HWD_AGPS_GRP_DLY_BAND_D_LID,                                         /* 0x308 */
   NVRAM_EF_HWD_AGPS_GRP_DLY_BAND_E_LID,                                         /* 0x309 */

   /* TX Power Backoff LID */
   NVRAM_EF_HWD_TX_PWR_BACKOFF_BAND_A_LID,                                       /* 0x30a */
   NVRAM_EF_HWD_TX_PWR_BACKOFF_BAND_B_LID,                                       /* 0x30b */
   NVRAM_EF_HWD_TX_PWR_BACKOFF_BAND_C_LID,                                       /* 0x30c */
   NVRAM_EF_HWD_TX_PWR_BACKOFF_BAND_D_LID,                                       /* 0x30d */
   NVRAM_EF_HWD_TX_PWR_BACKOFF_BAND_E_LID,                                       /* 0x30e */

   /* ETM LID */
   NVRAM_EF_HWD_BAND_A_MIPI_ETM_TX_EVENT_LID,                                    /* 0x30f */
   NVRAM_EF_HWD_BAND_B_MIPI_ETM_TX_EVENT_LID,                                    /* 0x310 */
   NVRAM_EF_HWD_BAND_C_MIPI_ETM_TX_EVENT_LID,                                    /* 0x311 */
   NVRAM_EF_HWD_BAND_D_MIPI_ETM_TX_EVENT_LID,                                    /* 0x312 */
   NVRAM_EF_HWD_BAND_E_MIPI_ETM_TX_EVENT_LID,                                    /* 0x313 */
   NVRAM_EF_HWD_BAND_A_MIPI_ETM_TX_DATA_LID,                                      /* 0x314*/
   NVRAM_EF_HWD_BAND_B_MIPI_ETM_TX_DATA_LID,                                      /* 0x315 */
   NVRAM_EF_HWD_BAND_C_MIPI_ETM_TX_DATA_LID,                                      /* 0x316 */
   NVRAM_EF_HWD_BAND_D_MIPI_ETM_TX_DATA_LID,                                      /* 0x317 */
   NVRAM_EF_HWD_BAND_E_MIPI_ETM_TX_DATA_LID,                                      /* 0x318 */
   NVRAM_EF_HWD_BAND_A_MIPI_ETM_TPC_EVENT_LID,                                    /* 0x319 */
   NVRAM_EF_HWD_BAND_B_MIPI_ETM_TPC_EVENT_LID,                                    /* 0x31a */
   NVRAM_EF_HWD_BAND_C_MIPI_ETM_TPC_EVENT_LID,                                    /* 0x31b */
   NVRAM_EF_HWD_BAND_D_MIPI_ETM_TPC_EVENT_LID,                                    /* 0x31c */
   NVRAM_EF_HWD_BAND_E_MIPI_ETM_TPC_EVENT_LID,                                    /* 0x31d */
   NVRAM_EF_HWD_BAND_A_MIPI_ETM_TPC_DATA_LID,                                     /* 0x31e */
   NVRAM_EF_HWD_BAND_B_MIPI_ETM_TPC_DATA_LID,                                     /* 0x31f*/
   NVRAM_EF_HWD_BAND_C_MIPI_ETM_TPC_DATA_LID,                                     /* 0x320*/
   NVRAM_EF_HWD_BAND_D_MIPI_ETM_TPC_DATA_LID,                                     /* 0x321*/
   NVRAM_EF_HWD_BAND_E_MIPI_ETM_TPC_DATA_LID,                                     /* 0x322*/
   NVRAM_EF_HWD_BAND_A_MIPI_ETM_PA_SECTION_DATA_1XRTT_LID,                        /* 0x323*/
   NVRAM_EF_HWD_BAND_B_MIPI_ETM_PA_SECTION_DATA_1XRTT_LID,                        /* 0x324*/
   NVRAM_EF_HWD_BAND_C_MIPI_ETM_PA_SECTION_DATA_1XRTT_LID,                        /* 0x325*/
   NVRAM_EF_HWD_BAND_D_MIPI_ETM_PA_SECTION_DATA_1XRTT_LID,                        /* 0x326*/
   NVRAM_EF_HWD_BAND_E_MIPI_ETM_PA_SECTION_DATA_1XRTT_LID,                        /* 0x327*/
   NVRAM_EF_HWD_BAND_A_MIPI_ETM_PA_SECTION_DATA_EVDO_LID,                         /* 0x328*/
   NVRAM_EF_HWD_BAND_B_MIPI_ETM_PA_SECTION_DATA_EVDO_LID,                         /* 0x329*/
   NVRAM_EF_HWD_BAND_C_MIPI_ETM_PA_SECTION_DATA_EVDO_LID,                         /* 0x330*/
   NVRAM_EF_HWD_BAND_D_MIPI_ETM_PA_SECTION_DATA_EVDO_LID,                         /* 0x331*/
   NVRAM_EF_HWD_BAND_E_MIPI_ETM_PA_SECTION_DATA_EVDO_LID,                         /* 0x332*/
   /* TX Power Offset LID */
   NVRAM_EF_HWD_TX_PWR_OFFSET_BAND_A_LID,                                       /* 0x333 */
   NVRAM_EF_HWD_TX_PWR_OFFSET_BAND_B_LID,                                       /* 0x334 */
   NVRAM_EF_HWD_TX_PWR_OFFSET_BAND_C_LID,                                       /* 0x335 */
   NVRAM_EF_HWD_TX_PWR_OFFSET_BAND_D_LID,                                       /* 0x336 */
   NVRAM_EF_HWD_TX_PWR_OFFSET_BAND_E_LID,                                       /* 0x337 */
   NVRAM_EF_HWD_DAT_FE_ROUTE_DATABASE_LID,							            /* 0x338 */
   NVRAM_EF_HWD_DAT_FE_BPI_DATABASE_LID,                                        /* 0x339 */
   NVRAM_EF_HWD_DAT_CAT_A_MIPI_EVENT_TABLE_LID,							        /* 0x33a */
   NVRAM_EF_HWD_DAT_CAT_A_MIPI_DATA_TABLE_LID,							        /* 0x33b */
   NVRAM_EF_HWD_DAT_CAT_B_MIPI_EVENT_TABLE_LID,							        /* 0x33c */  
   NVRAM_EF_HWD_DAT_CAT_B_MIPI_DATA_TABLE0_LID,							        /* 0x33d */
   NVRAM_EF_HWD_DAT_CAT_B_MIPI_DATA_TABLE1_LID, 								/* 0x33e */
   NVRAM_EF_HWD_DAT_CAT_B_MIPI_DATA_TABLE2_LID, 								/* 0x33f */
   NVRAM_EF_HWD_DAT_CAT_B_MIPI_DATA_TABLE3_LID, 								/* 0x340 */
   /* SAR LID */
   NVRAM_EF_HWD_SAR_TX_PWR_OFFSET_BAND_A_LID,									   /* 0x341 */
   NVRAM_EF_HWD_SAR_TX_PWR_OFFSET_BAND_B_LID,									   /* 0x342 */
   NVRAM_EF_HWD_SAR_TX_PWR_OFFSET_BAND_C_LID,									   /* 0x343 */
   NVRAM_EF_HWD_SAR_TX_PWR_OFFSET_BAND_D_LID,									   /* 0x344 */
   NVRAM_EF_HWD_SAR_TX_PWR_OFFSET_BAND_E_LID,									   /* 0x345 */

   /* 1XRTT L1D Nv Parameters LID */
   NVRAM_EF_HWD_1X_ACC_TX_POWER_OFFSET_LID = 0x400,                             /* 0x400 */

   NVRAM_EF_MAX_LID_NUM = 0xFFFF
}nvram_lid_core_enum;

#endif
