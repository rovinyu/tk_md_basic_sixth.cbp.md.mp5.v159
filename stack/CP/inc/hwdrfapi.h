/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef _HWDRFAPI_H_
#define _HWDRFAPI_H_
/*****************************************************************************
*
* FILE NAME   : hwdrfapi.h
*
* DESCRIPTION : Interface definition for external RF design
*
* HISTORY     :
*     See Log at end of file
*
*****************************************************************************/

/*----------------------------------------------------------------------------
 Include Files
----------------------------------------------------------------------------*/
#include "sysdefs.h"

#include "hwdapi.h"

/*----------------------------------------------------------------------------
 Global Defines and Macros
----------------------------------------------------------------------------*/
/* Check to see if SYS_OPTION_RF_HW is correctly defined for target build */
#if ((SYS_OPTION_RF_HW != SYS_RF_GRF_6413) && (SYS_OPTION_RF_HW != SYS_RF_FCI_7790) && (SYS_OPTION_RF_HW != SYS_RF_MTK_ORIONC)&& (SYS_OPTION_RF_HW != SYS_RF_MT6176))
   #error ERROR_RF_HW_OPTION_NOT_CORRECTLY_DEFINED
#endif

#define HWD_INT_PLL_SETTLE_MS                  1

/* #defines for PLL Lock Detect Circuitry */
#define HWD_PLL_POLARITY_ACTIVE_LOW   0x0000
#define HWD_PLL_POLARITY_ACTIVE_HIGH  0x0001
#define HWD_PLL_LOCK_ENABLE_MASK      0x0002

#define HWD_DEFAULT_PLL_LOCK_PULSE_THRESHOLD  7 /* Number of clk19 cycles for PLLU interrupt to occur */

/* The following global define is used during PCS to determine the threshold
** at which the RF solution switches to a high band Tx filter (on RF 2.7 this
** is controlled by GPIO_46 */
#define HWD_RF_PCS_HIGH_BAND_CHANNEL_THRESHOLD  600

/* #defines for RF register programming following deep sleep */
#define HWD_SLOTTED_RF_FORCE_LOADING   TRUE
#define HWD_SLOTTED_RF_NORMAL_LOADING  FALSE

#define HWD_CAL_IMD_FFT_SIZE       64

#define HWD_DO_TRAFFIC_PREEMPTION_DELAY   5   /* ms */


/* assignments for Main/Div antennas for 1x and DO */
typedef enum
{
   HWD_RF_MAIN_RX = 0,
   HWD_RF_DIV_RX,
   HWD_RF_SEC_RX,
   HWD_RF_MAX_NUM_OF_RX
}HwdRfRxEnumT;

typedef enum
{
#ifdef HWD_DBG_TXDAC_DIRECT
   HWD_RF_MAIN_TX = 0,
   HWD_RF_AUX_TX  = 1,
#else
   HWD_RF_MAIN_TX = 1,
   HWD_RF_AUX_TX  = 0,
#endif
   HWD_RF_MAX_NUM_OF_TX = 2
}HwdRfTxEnumT;

/*  matches this type!!
typedef enum
{
   MPA_RF_PATH_RX_BN1,
   MPA_RF_PATH_RX_BN2,
   MPA_RF_PATH_RX_BN3,
   MPA_RF_PATH_TX_BN1,
   MPA_RF_PATH_TX_BN2,
   
   MPA_RF_NO_PATH,
   MPA_RF_NUM_PATHS = MPA_RF_NO_PATH
} MpaRfPathT;
*/
typedef enum
{
   HWD_RF_MPA_RX1 = 0,
   HWD_RF_MPA_RX2,
   HWD_RF_MPA_RX3,
   HWD_RF_MPA_TX1,
   HWD_RF_MPA_TX2,
   HWD_RF_MPA_NO_PATH,
   HWD_RF_MPA_MAX_PATH_NUM = HWD_RF_MPA_NO_PATH
}HwdRfMpaEnumT;

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
typedef enum
{
   HWD_RF_HPM = 0,
   HWD_RF_LPM,
   HWD_RF_MAX_NUM_OF_PM
}HwdRfPwrModeT;
#endif

#define HWD_RF_MPA_RX1_BM  (1<<HWD_RF_MPA_RX1)
#define HWD_RF_MPA_RX2_BM  (1<<HWD_RF_MPA_RX2)
#define HWD_RF_MPA_RX3_BM  (1<<HWD_RF_MPA_RX3)
#define HWD_RF_MPA_TX1_BM  (1<<HWD_RF_MPA_TX1)
#define HWD_RF_MPA_TX2_BM  (1<<HWD_RF_MPA_TX2)
#define TX_MAX_POWERBACKOFF   M_Q5(5)
#define TX_MIN_POWERBACKOFF   M_Q5(-5)

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
typedef enum
{
   HWD_TX_PWR_BACK_OFF_HIGH_TEMP = 0,
   HWD_TX_PWR_BACK_OFF_LOW_TEMP,
   HWD_TX_PWR_BACK_OFF_HYST_NUM
}HwdRfPwrBackOffT;
#endif

/*----------------------------------------------------------------------------
     Delayed loading addresses
------------------------------------------------------------------------------*/
#define DEV_CF_TXON_CMPINT     0xE9CD
#define DEV_CF_TXON_CMP0       0xE9C8
#define DEV_CF_TXON_CMP1       0xE9C9
#define DEV_CF_TXON_CMP2       0xE9CA
#define DEV_CF_TXON_CMP3       0xE9CB
#define DEV_CF_TXON_CMP4       0xE9CC
#define DEV_CF_TXON_CMP5       0xE9DD
#define DEV_CF_TXON_CMP6       0xE9DE
#define DEV_CF_TXON_CMP7       0xE9D7
#define DEV_CF_TXON_CMPINT2    0xE9D8

#define DEV_CF_PDM_CMP0        0xE9D0
#define DEV_CF_PDM_CMP1        0xE9D1
#define DEV_CF_PDM_CMP2        0xE9D2
#define DEV_CF_PDM_CMP3        0xE9D3
#define DEV_CF_PDM_CMP4        0xE9D4
#define DEV_CF_PDM_CMP5        0xE9D5
#define DEV_CF_PDM_CMP6        0xE9D6

#define DEV_CF_PDM0_DATA       0xE943  /* PDM0 Data Register */
#define DEV_CF_PDM1_DATA       0xE944  /* PDM1 Data Register */
#define DEV_CF_PDM2_DATA       0xE945  /* PDM2 Data Register */
#define DEV_CF_PDM3_DATA       0xE946  /* PDM3 Data Register */
#define DEV_CF_PDM4_DATA       0xE947  /* PDM4 Data Register */
#define DEV_CF_PDM5_DATA       0xE94F  /* PDM5 Data Register */
#define DEV_CF_PDM6_DATA       0xE954  /* PDM6 Data Register */

/*----------------------------------------------------------------------------
    PA/LNA Configuration
------------------------------------------------------------------------------*/

#define RFON0_SELECT    0x0001
#define RFON1_SELECT    0x0002
#define RFON2_SELECT    0x0004
#define RFON3_SELECT    0x0008
#define RFON4_SELECT    0x0010
#define RFON5_SELECT    0x0020
#define RFON6_SELECT    0x0040
#define RFON7_SELECT    0x0080

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
/* NOTE: The following TXON select definitions must match the
** bit fields located in the DSPM DEV_TX_ON_DLYMODE_DIN register, 
** which is the same as the TXONDLYMODE register */
#define TXON_INT_M_SELECT    0x0001
#else
#define TXINT_SELECT    0x0001
#endif
#define TXON0_SELECT         0x0002
#define TXON1_SELECT         0x0004
#define TXON2_SELECT         0x0008
#define TXON3_SELECT         0x0010
#define TXON4_SELECT         0x0020
#define TXON5_SELECT         0x0040
#define TXON6_SELECT         0x0080
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
#define TXON7_SELECT         0x0100
#define TXON_INT_A_SELECT    0x0200
#endif

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
/* TXONDLYMASK register */
#define TXON0_MASK      (0x3 << (0 * 2))
#define TXON1_MASK      (0x3 << (1 * 2))
#define TXON2_MASK      (0x3 << (2 * 2))
#define TXON3_MASK      (0x3 << (3 * 2))
#define TXON4_MASK      (0x3 << (4 * 2))
#define TXON5_MASK      (0x3 << (5 * 2))
#define TXON6_MASK      (0x3 << (6 * 2))
#define TXINI_MAIN_MASK (0x3 << (7 * 2))
/* TXONDLYMASK2 register */
#define TXON7_MASK      (0x3 << (0 * 2))
#define TXINI_AUX_MASK  (0x3 << (1 * 2))
#endif

#define TXON7_SELECT    0x0100
#define TXINT2_SELECT   0x0200

#define HWD_HDET_RESET_RATE_ADJ  0xff

#if (SYS_ASIC <= SA_CBP82)
extern uint32 HwdPdmMaskRegAddr[];
extern uint32 HwdPdmRdBackRegAddr[];
extern uint32 HwdPdmDinRegAddr[];
#endif

/*----------------------------------------------------------------------------
 Global Typedefs
----------------------------------------------------------------------------*/
#if!defined(MTK_CBP) || defined(MTK_PLT_ON_PC)
typedef struct
{
    uint16 PaPdmAddress[4];
    uint16 PaPdmValue[4];
    uint16 PaPdmDlyMode[4];
}HwdGainControlUnitT;

typedef struct
{
   HwdGainControlUnitT PA1;
   HwdGainControlUnitT PA2;
   HwdGainControlUnitT PDM;
}HwdGainControlT;

typedef struct
{
    uint32 PaPdmSlotAddress;
    uint32 PaPdmHslotAddress;
    uint16 PaPdmSlotValue[4];
    uint16 PaPdmHslotValue[4];
    uint16 PaPdmDlyMode[4];
}HwdDoGainControlUnitT;

typedef struct
{
   HwdDoGainControlUnitT PA1;
   HwdDoGainControlUnitT PA2;
   HwdDoGainControlUnitT PDM;
}HwdDoGainControlT;


/* delay mode register control in do mode */
typedef enum
{
   HWD_DLYMODE_PA1 = 0,   
   HWD_DLYMODE_PA2,         
   HWD_DLYMODE_GATE,
   HWD_DLYMODE_PDM1,
   HWD_DLYMODE_PDM2,
   HWD_DLYMODE_RFSPI,

   HWD_DLYMODE_MAX_CTRL_UNITS

} HwdDelayModeCtrlUnitT;


/* delay mask register control in do mode */
typedef enum
{
   HWD_DLYMASK_PA1 = 0,   
   HWD_DLYMASK_PA2,         
   HWD_DLYMASK_GATE,
   HWD_DLYMASK_PDM1,
   HWD_DLYMASK_PDM2,
   HWD_DLYMASK_RFSPI,   
   
   HWD_DLYMASK_MAX_CTRL_UNITS

} HwdDelayMaskCtrlUnitT;

/* define gain control unit in do mode */
typedef enum
{
   HWD_DOTX_GAINCTRL_PA1 = 0,
   HWD_DOTX_GAINCTRL_PA2,
   HWD_DOTX_GAINCTRL_PDM,
   HWD_DOTX_GAINCTRL_SPI,
   
   HWD_DOTX_GAINCTRL_MAX_CTRL_UNITS 

} HwdDoGainCtrlUnitIdxT;
#endif

/* TX AGC PA gain state types */
typedef enum
{
   HWD_LOW_GAIN_STATE,
   HWD_MED_GAIN_STATE,
   HWD_HIGH_GAIN_STATE
} HwdAgcGainStateT;

/* Rx gain state type*/
typedef enum
{
   HWD_GAIN_STATE1,
   HWD_GAIN_STATE2,
   HWD_GAIN_STATE3,
   HWD_GAIN_STATE4,
   HWD_GAIN_STATE5,
   HWD_GAIN_STATE6,
   HWD_GAIN_STATE7,
   HWD_GAIN_STATE8,
   MAX_RX_GAIN_STATES
} HwdRxAgcGainStateT;

typedef enum
{
   MAIN_IMD_TABLE   = 0,
   MAIN_NOIMD_TABLE,
   DIV_IMD_TABLE,
   DIV_NOIMD_TABLE,
   SEC_IMD_TABLE,
   SEC_NOIMD_TABLE
} HwdCalMainDivTableSwitchT;

typedef enum
{
   MAIN_SLOT   = 0,
   MAIN_HALF_SLOT,
   DIV_SLOT,
   DIV_HALF_SLOT
} HwdRfTimingSelectT;

typedef enum
{
  HWD_AFC_VCTCXO,
  HWD_AFC_TSX,
  HWD_AFC_DCXO,
  HWD_AFC_TCXO,
  HWD_AFC_TYPE_NUM
}HwdRfCrystalT;

typedef struct
{
   int16 FreqMeasSign;              /* sign of the frequency measurement, function of
                                     * the baseband IQ conjuqation, related to
                                     * HWD_FREQ_MEAS_SIGN and HWD_PN_Q_INV.
                                     */
   int16 TxMdmDelay;                /* delay added to the Tx timing to line up Tx waveform
                                     * with Rx waveform at the antenna. Related to
                                     * HWD_TX_MODEM_DLY_Q3.
                                     */
   uint16 ClockSettleTimeSlpClkCnt; /* settle time for the internal clocks, in units of 32768kHz
                                     * sleep clock counts. This is the sum of the TCXO settle
                                     * time and the internal PLL settle time.
                                     */
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
   uint16 RxRfSettleDelaysMs;       /* delays necessary for RF power up, in ms.  */
#else
   uint16 RxRfSettleDelaysUs;       /* delays necessary for RF power up, in us.  */
#endif
#ifdef MTK_CBP
   uint16 RxVcoSettleTimeUs;        /* settle time of the RF Rx Vco ONLY, in us.  */
   uint16 RxPllSettleTimeUs;        /* settle time of the RF Rx PLLs =
                                       RxRfSettleDelaysUs+RxVcoSettleTimeUs+SoftwareOverhead, in us.  */
   uint16 RxPllBackoffTimeUs;       /* PLL backoff time used for slotted calculations
                                       includes PLL settling + overhead prior to Rx AGC start, in us.  */
   uint16 RxPllChannelSettle;       /* settle time for channel HO, in us */
#else
   uint16 RxVcoSettleTimeMs;        /* settle time of the RF Rx Vco ONLY, in ms.  */
   uint16 RxPllSettleTimeMs;        /* settle time of the RF Rx PLLs =
                                       RxRfSettleDelaysMs+RxVcoSettleTimeMs+SoftwareOverhead(3ms), in ms.  */
   uint16 RxPllBackoffTimeMs;       /* PLL backoff time used for slotted calculations
                                       includes PLL settling + overhead prior to Rx AGC start, in ms.  */
   uint16 RxPllChannelSettle;       /* settle time for channel HO, in ms */
#endif
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
   uint16 RxPllAmpsSettle;          /* settle time for Amps VCO tuning, in ms */
   uint16 TxPllSettleTimeMs;        /* settle time of the RF Tx PLL, in ms.  */
#else
   uint16 RxPllAmpsSettle;          /* settle time for Amps VCO tuning, in us */
   uint16 TxPllSettleTimeUs;        /* settle time of the RF Tx PLL, in us.  */
#endif
   uint16 MiniAcqTime;              /* time for mini acq - in us */
   uint16 ChannelNormTime;          /* Time required for channel normalization - in us*/
   uint32 RxPllBackoffTimeSymb;     /* Slotted Paging Backoff time in symbols.*/
   uint32 SlottedPagingRxAgcSettle; /* Slotted Paging RxAgc settle in symbols.*/
   uint32 SlottedPagingBackoffTime; /* Slotted Paging Backoff time from the pch in us if MTK_CBP enable, else in ms.*/
   bool   SlottedPagingRfForceLoad; /* Boolean to determine if RF registers need to be
                                     * to be re-initialized following deep sleep - setting
                                     * of this flag is dependant on RF solution.
                                     */
   uint16 TxFilterTuneValue;        /* cut-off frequency of the Tx Dac filter */

   int32  GpsRxGrpDelay;                                     

   uint32 FgrAllocOffset1;
   uint32 FgrAllocOffset2;
   uint16 FgrAllocEcioRelThresh1Q16;
   uint16 FgrAllocEcioRelThresh2Q16;

   uint16 R1Scale;
#ifdef MTK_CBP
   uint16 RxMiscAdjUs;          /* set margin for resync s/w operation */
#else
   uint16 RxEVDOMiscAdjUs;          /* For DO only to align Mini-Acq done at slot 8 and finger alloc done at slot A */
#endif
   bool   RxDiversityEnabled;

   uint16 SlottedPreemptionDelay;   /* Slotted preemption delay in ms  */
   uint16 PreemptionDelaySymb;      /* Slotted preemption delay in symbols  */
   uint8  GpsRfClkEdge;

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
   uint16 txPreBurstCalDurationTime; /** Tx pre-burst calibration duration time */
   int16  maxTxPower;                /** Max Tx ouput power, unit is 1/32dBm*/
   int16  minTxPower;                /** Min Tx ouput power, unit is 1/32dBm*/
   int16  txDacOutPwr;               /** TXDAC output power, unit is 1/32dBm*/
   uint16 rxAgcSettlingTime;         /** RF Rx AGC settling duration time, unit is 1/8 chip */
   uint16 rxDfePhaseJumpTime;        /** Rx DFE phase jump settling time, unit is 1/8 chip */
   uint16 rxDfeDcCalStartDlyTime;    /** The delay time of start RX DFE in RXDC cal, unit is 1/8 chip */
   uint16 lnaModeNum;                /** The LNA mode number */
   uint16 rxAgcTableSize;            /** The RF RXAGC table size*/
   uint16 txAgcTableSize;            /** The RF RXAGC table size*/
   int16  maxRxPower;                /** Max Rx input power of RXAGC table, unit is 1/32dBm*/
   int16  minRxPower;                /** Min Rx input power of RXAGC table, unit is 1/32dBm*/
   uint16 MainDoSlotTiming;          /** Digital gain latch time */
   uint16 MainDoHalfSlotTiming;      /** Digital gain latch time */
   uint16 DivDoSlotTiming;           /** Digital gain latch time */
   uint16 DivDoHalfSlotTiming;       /** Digital gain latch time */
   /**delay loader start offset = delayLoadWindow + txGlbDlyTime - txAgcSettlingTime */
   uint16 txGlbDlyTime;              /** The group delay time include the TXDFE/MS/RF delay time in chips*/
#ifdef MTK_DEV_RF_CUSTOMIZE
   uint16 txAgcSettlingTime;         /** RF Tx AGC settling duration time in chips*/
#endif /* MTK_DEV_RF_CUSTOMIZE */
   HwdRfCrystalT crystalType;
   bool   crystalInPmic;             /** TRUE: 26MHz from PMIC. FALSE: 26MHz from RF */
   bool   enable32kLess;             /** TRUE: with 32K less. FALSE: with 32K RTC */
   bool   coClockMode;               /** TRUE: coclock. FALSE: non-coclock */
   uint16 rfRxPathNum;               /** The number of RF device Rx path */
   int16  maxTxPowerOriginal;        /** Max Tx ouput power before backoff, unit is 1/32dBm*/
   int16  rxGlbDlyOffset;            /** The Rx path group delay time offset when swap from LPM to HPM, unit is 1/8 chip*/
#endif
   uint16 txMaxPwrCallbackAnt1[HWD_RF_BAND_MAX];       /** Max Tx ouput power call back, unit is 1/64dBm*/
   uint16 txMaxPwrCallbackAnt2[HWD_RF_BAND_MAX];       /** Max Tx ouput power call back, unit is 1/64dBm*/
} HwdRfConstantsT;

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
typedef PACKED_PREFIX struct
{
    uint8   TxBand;
    uint16  TxChannel;
    uint8   RxBand;
    uint16  RxChannel;
    int8    Temperature;
    uint16  TemperatureAdc;
    uint8   PaMode;
    uint8   LnaMode;

    int16   TxHFreqComp;        /* dB */
    int16   TxHTempComp;        /* dB */
    int16   TxHTotalComp;       /* dB */
    int16   TxMFreqComp;        /* dB */
    int16   TxMTempComp;        /* dB */
    int16   TxMTotalComp;       /* dB */
    int16   TxLFreqComp;        /* dB */
    int16   TxLTempComp;        /* dB */
    int16   TxLTotalComp;       /* dB */

    int16   PdetFerqComp;       /* dB */
    int16   PdetHCoupLoss;      /* dB */
    int16   PdetLCoupLoss;      /* dB */
    int16   PdetMCoupLoss;      /* dB */
    int16   PdetHTotal;         /* dB */
    int16   PdetLTotal;         /* dB */
    int16   PdetMTotal;         /* dB */

    int16   RxHTempComp;        /* dB */
    int16   RxHPathLoss;        /* dB */   
    int16   RxMTempComp;        /* dB */
    int16   RxMPathLoss;        /* dB */   
    int16   RxLTempComp;        /* dB */   
    int16   RxLPathLoss;        /* dB */ 
} PACKED_POSTFIX HwdRfCompDataSpyT;
#endif

/* types for internal (not ETS) message APIs */
typedef struct 
{
   SysCdmaBandT  PllBand;
   uint32        Channel;
   ExeTaskIdT    RspTaskId;
   ExeSignalT    RspSignal;
}HwdPllChannelSetMsgT;	 

typedef PACKED_PREFIX struct
{
   uint8   Interface;
   uint8   Band;
   uint16  Channel;
   uint8   RfUsed;
   int16   RxPwrM;
   uint8   RxGainStateM;
   int16   RxFreqAdjM[8];
   int16   RxTempAdjM;
   int16   RxPwrD;
   uint8   RxGainStateD;
   int16   RxFreqAdjD[8];
   int16   RxTempAdjD;
   int16   TxPwr;
   uint16  TxPdm;
   uint8   TxGainState;
   int16   TxFreqAdj[3];
   int16   TxTempAdj[3];
   int16   TxMaxPwr;
   int16   TxPwrDetAdj;
   int16   TempMeas;
   int16   BatteryMeas;
   int16   PwrDetMeas;
   uint8   AfcMode;
   int16   AfcErr;
   uint16  AfcPdm;
} PACKED_POSTFIX HwdRfCalDataStatusSpyT;           /* typedef for the spy */

/* IMD */
typedef enum
{
   BACK_TO_AUTOMATIC_TABLE = 0,
   CALIBRATE_IMD_TABLE,
   CALIBRATE_NOIMD_TABLE,
} HwdImdTableCalibT;

typedef PACKED_PREFIX struct
{
   SysAirInterfaceT     Mode;
   HwdImdTableCalibT    ImdMainTableCalibSwitch;
   HwdImdTableCalibT    ImdDivTableCalibSwitch;
   HwdImdTableCalibT    ImdSecTableCalibSwitch;
} PACKED_POSTFIX  HwdImdTableCalibConfigSetMsgT;

/***** NOTE: Assigned values should not be changed due to their use as offsets
             into PLL programming arrays. */


/*****************************************************************************
 Values for TxAmpsModulationType are:
     REV_CH_MODULATION_BASEBAND_IQ = 0
     REV_CH_MODULATION_VCO         = 1
     REV_CH_MODULATION_DIF_MODE    = 2
 These values are defined in ipcapi.h
 This structure is initialized in hwdinit.c
 *****************************************************************************/

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
/** PA mode enumeration */
typedef enum
{
   HWD_PA_MODE_HIGH = 0,
   HWD_PA_MODE_MID  = 1,
   HWD_PA_MODE_LOW  = 2,
   HWD_PA_MODE_NUM
}HwdRfPaModeEnumT;

/** LNA mode enumeration */
typedef enum
{
   HWD_LNA_MODE0, /**> LNA highest mode*/
   HWD_LNA_MODE1,
   HWD_LNA_MODE2,
   HWD_LNA_MODE3,
   HWD_LNA_MODE4,
   HWD_LNA_MODE5,
   HWD_LNA_MODE6,
   HWD_LNA_MODE7,
   HWD_LNA_MODE_NUM
}HwdRfLnaModeEnumT;

/** RFGPO level enumeration */
typedef enum
{
   HWD_RFGPO_LOW_LVL  = 0,
   HWD_RFGPO_HIGH_LVL = 1
}HwdRfGpoLvlEnumT;

/** Enumeration of TXAGC delay load signal */
typedef enum HwdTxAgcDlyLoadSigT_tag{
  /* 0 */ HWD_TXAGC_PA_VDD_SIG,
  /* 1 */ HWD_TXAGC_TXPGA_SIG,
  /* 2 */ HWD_TXAGC_PA_MODE_SIG,
  /* 3 */ HWD_TXAGC_GBB0_CL_SIG,
  /* 4 */ HWD_TXAGC_GBB0_OL_SIG,
  /* 5 */ HWD_TXAGC_GBB1_SIG,
  /* 6 */ HWD_TXAGC_PAHSE_JUMP_SIG,
  /* 7 */ HWD_TXAGC_PDET_RD_SIG,
  /* 8 */ HWD_TXAGC_PDET_ADC_SIG,
  /* 9 */ HWD_TXAGC_REF_DDPC_SIG,
  /* 10 */HWD_TXAGC_SIG_NUM
}HwdTxAgcDlyLoadSigT;

/** RF Debug Trace item */
typedef enum
{
  TXAGC_SET_DLYLDR_SLOT,       
  TXAGC_SET_DLYLDR_HALF_SLOT,
  AFC_PARAM,
  TEMP_SCHEDULE_DBG,
  DDPC_LOOP_TST_DBG,
  COMPENSATION_ADJUST_TST_DBG,
  PSRR_STATUS,
} HwdRfDbgTraceCtrlT;

/** PGA mode type enumeration */
typedef enum HwdPgaModeT_tag
{
   HWD_PGA_A = 0,
   HWD_PGA_B = 1
}HwdPgaModeT;

/**RF Rx path FSM status enumeration*/
typedef enum
{
   RX_FSM_SHDR,
   RX_FSM_DIVERSITY,
   RX_FSM_MAIN_ONLY,
   RX_FSM_DIV_ONLY,
   RX_FSM_ALL_OFF
}HwdRxFsmT;

#define M_GET_SIG(sig, sigShift)  ((sig&(1<<sigShift))>>sigShift)
#define M_SET_SIG(sig, sigShift)  (sig | (1<<sigShift))

/** PA configuration context */
typedef NV_PACKED_PREFIX struct  HwdRfPaContextT_tag
{
   /** The reference power, in 1/32 dBm. */
   int16            refPower; 
   /** The reference power output PA without feq/temp compensation, in 1/32 dBm. */
   int16            paGain; 
   /** The PA mode info.*/
   uint16           paMode;
   /** The PA control RFGPOs level */
   uint16           paVm0;
   uint16           paVm1;
   /** PA BUCK supply voltage level, unit is mv*/
   uint16           paVdd;
} NV_PACKED_POSTFIX HwdRfPaContextT;


/** PA hysteresis region structure */
typedef struct HwdRfPaHystRegionT_tag
{
   /** The start of hysteresis region, in 1/32 dBm. */
   int16            hystStart; 
   /** The end of hysteresis region, in 1/32 dBm. */
   int16            hystEnd; 
}HwdRfPaHystRegionT;

/** TX and DET path IQ imbalance and DC POC result structure */
typedef struct{    
    int16 gainEst;  /*>  S-3.10, epsilon */
    int16 phaseEst; /*>  S-4.10, -theta/2, in radians */
}HwdDetIqCalParmT;

typedef struct
{
   /** The estimate DC offset in S0.11, det ADC full swing is +/-0.6v, the clock is 13MHz */
   int16 dcoEstI; 
   int16 dcoEstQ; 
}HwdDetDcCalParmT;

typedef struct HwdIqDcCalibParmT_tag
{
   /** The estimate DC offset, TxPath:S0.14, Tx DAC full swing is +/-1.05v. DetPath:S0.11, det ADC full swing is +/-0.6v, 13MHz clock */
   int16            dcoEstI; 
   int16            dcoEstQ; 
   /** The estimate phase error, TxPath:S-4.10, -theta/2, in radians. DetPath:S-4.10, -theta/2, in radians */
   int16            phaseEst; 
   /** The estimate gain error, TxPath:S-4.10, epsilon/2. DetPath:S-3.10, epsilon */
   int16            gainEst; 
}HwdIqDcCalibParmT;

/** Ads structure to provide parameters to the HwdRfTxAgcCtrl() function. */
typedef struct HwdRfTxAgcParamT_tag
{
   /** [in] - Indicate the slot or half-slot boundary */
   uint16    hslotBoundary;
   /** [in] - Indicate whether the PA mode needs change when the timing is critical */
   Bool      paChange;
   /** [in] - Indicate whether the DDPC needs enable */
   Bool      enableDdpc;
   /** [in] - Compensation after PA mode select, in 1/32 dB */
   int16     deltaGain;
   /** [in] - KS error, in 1/32 dB */
   int16     ksErr;
   /** [in] - Target output power at antenna, in 1/32 dB */
   int16     targetTxPwr;
   /** [out] - The residual analog gain that should be applied in digital gain GBB0, in 1/32 dB. */
   int16     gbb0; 
   /** [out] - The digital gain GBB1 code word,it just be used in Denali project */
   uint16    gbb1cw;
   /** [out] - The digital gain GBB1 value, in 1/32 dB,it just be used in Denali project  */
   int16     gbb1;
   /** [out] - The transmitter PGA analog gain, in 1/32 dB. */
   int16     pgaGain; 
   /** [out] - The reference power output PA with feq/temp compensation, in 1/32 dBm. */
   int16     paGain; 
   /** [out] - The PA mode info. */
   int16     paMode;
   /** [out] - The TX phase compensation value with PA mode and PGA-A/B swap. */
   int16     phaseComp;
   /** [out] - The TXUPC clip threshold, unit is 1/32dB,it just be used in Denali project */
   int16     txupcThresh;
   /** [out] - The coupler loss, L1D should add the value to DDPC result, unit is 1/32dB. */
   int16     couplerLoss;
   /** [out] - The TxDet Gain, L1D should minus it from DDPC result, unit is 1/32dB. */
   int16     txDetGain;
   /** [out] - The PGA gain type, 0: PGA-A table, 1: PGA-B table. It just be used for L1 logging purpose */
   HwdPgaModeT       pgaMode;
   /** [out] - The Tx path IQ imbalance and DC offset POC result*/
   HwdIqDcCalibParmT txCalibParm;
   /** [out] - The DET path IQ imbalance and DC offset POC result*/
   HwdIqDcCalibParmT detCalibParm;
} HwdRfTxAgcParamT;



/** Ads structure to provide parameters to the HwdRfTxAgcCtrlImmed() function. */
typedef struct HwdRfTxAgcImmedParamT_tag
{
   /** [in] 每 Target output power at antenna, in 1/32 dB */
   int16     targetTxPwr;
   /** [in] 每 The PA context info that include the PA mode and PA control RFGPOs level. */
   HwdRfPaContextT  paContext;
   /** [in] - The index of PA context table. */
   int16     paTableIdx;
   /** [in] - Indicate whether the DDPC needs enable */
   Bool      enableDdpc;
   /** [in] - The coupler loss, unit is 1/32dB. */
   int16     couplerLoss;
   /** [out] 每 The digital gain GBB1 code word */
   uint16    gbb1cw;
   /** [out] 每 The digital gain GBB1 value, in 1/32 dB */
   int16     gbb1;
   /** [out] 每 The residual analog gain that should be applied in digital gain GBB0, in 1/32 dB. */
   int16     gbb0; 
   /** [out] - The TxDet Gain, L1D should minus it from DDPC result, unit is 1/32dB. */
   int16     txDetGain;
   /** [out] - The temperature and freq coupler loss compensation, unit is 1/32dB. */
   int16     couplerLossComp;
   /** [out] - The Tx path IQ imbalance and DC offset POC result*/
   HwdIqDcCalibParmT txCalibParm;
   /** [out] - The DET path IQ imbalance and DC offset POC result*/
   HwdIqDcCalibParmT detCalibParm;
}HwdRfTxAgcImmedParamT;

/** Ads structure to provide parameters to the HwdRfTxOnImmed() function. */
typedef struct HwdRfTxOnImmedParamT_tag
{
   /** [in] 每 The RF band class type */
   SysCdmaBandT     rfBand;
   /** [in] - The channel number. */
   uint16           channelNum;
   uint8            mode;
}HwdRfTxOnImmedParamT;

typedef struct HwdRfTxOffImmedParamT_tag
{
   /** [in] 每 The RF band class type */
   SysCdmaBandT     rfBand;
   uint8            mode;
}HwdRfTxOffImmedParamT;

/** RXDC Table context */
typedef struct HwdRfRxDcocT_tag
{
  uint16  rxDcocI;  /** S5.0,resolution = 9mV */
  uint16  rxDcocQ;  /** S5.0,resolution = 9mV */
}HwdRfRxDcocT;

typedef struct HwdDfeRxDcT_tag
{
  int16  rxDcI;  /** Q4 mV */
  int16  rxDcQ;  /** Q4 mV */
}HwdDfeRxDcT;

/** Rx path IQ imbalance POC result structure */
typedef struct HwdDfeRxIrrT_tag
{
   /** The estimate phase error in S-4.8, -theta/2, in radians. */
   int16            phaseEst; 
   /** The estimate Q branch gain error in S3.7, it's a log2 value. The I branch gain error is always 0(log2). */
   int16            gainEstQ;
}HwdDfeRxIrrT;

typedef struct
{
	/* RX IRR, config in RXDFE */
	HwdDfeRxIrrT rxIrrHpm[HWD_RF_MAX_NUM_OF_RX];
	HwdDfeRxIrrT rxIrrLpm[HWD_RF_MAX_NUM_OF_RX];

	/* RX DC of DFE*/
	HwdDfeRxDcT dfeRxDcHpm[HWD_RF_MAX_NUM_OF_RX][HWD_RF_RX_GAIN_STEP_MAX_NUM];
	HwdDfeRxDcT dfeRxDcLpm[HWD_RF_MAX_NUM_OF_RX][HWD_RF_RX_GAIN_STEP_MAX_NUM];
}HwdDfeRxPocParmT;

typedef struct
{
  HwdDfeRxPocParmT rxPocParm[HWD_RF_BAND_MAX];
}HwdDfeRxPocTblT;

/** Ads structure to provide parameters to the HwdRfMainRxAgcDcCtrl()
    and HwdRfDivRxAgcDcCtrl() function.
*/
typedef struct HwdRfRxAgcDcParamT_tag
{
   /** [in] - Indicate the slot or half-slot boundary */
   uint16    hslotBoundary;
   /** [in] 每The current Rx power in amplitude log2 format Q7*/
   int16     rxPowerAlog2;
   /** [in] 每The required DCOC that convert to RXADC, the unit is mv */
   int16     reqDcocI;
   /** [in] 每The required DCOC that convert to RXADC, the unit is mv */
   int16     reqDcocQ;
   /** [in] 每This flag is use to indicate the saturation status of RXADC, if the ADC is saturation then reduce 6dB of Rx gain */
   Bool      adcSaturated;
   /** [in] - force rx gain setting, it's use for wake up and power on rx gain setting*/
   Bool      forceRxGainSetting;
   /** [in] - Rx high/low power mode*/
   HwdRfPwrModeT pwrMode;
   /** [out] 每The RF RXAGC gain table index. */
   uint16    gainIndex; 
   /** [out] 每The RF Rx path analog gain in amplitude log2 format Q7*/
   int16     stepGainAlog2; 
   /** [out] 每The residual DCOC that convert to RXADC, the unit is mv */
   int16     resDcocI; 
   /** [out] 每The residual DCOC that convert to RXADC, the unit is mv */
   int16     resDcocQ; 
   /** [out] 每The phase jump compensation value */
   int32     phaseJumpComp;
   /** [out] - The Rx path RF band */
   HwdRfBandT rfBand;
   /** [out] - The RF Rx path */
   HwdRfRxEnumT rxPath;
} HwdRfRxAgcDcParamT;

/** Ads structure to provide parameters to the HwdRfMainRxAgcCtrlImmed()
    and HwdRfDivRxAgcCtrlImmed() function.
*/
typedef struct HwdRfRxAgcImmedParamT_tag
{
   /** [in] 每The real input Rx power from antenna in 1/32dBm*/
   int16     rxPower;
   /** [in] 每The LNA mode*/
   HwdRfLnaModeEnumT lnaMode;
   /** [in] - Rx high/low power mode*/
   HwdRfPwrModeT pwrMode;
   /** [out] 每The RF RXAGC gain table index. */
   uint16    gainIndex; 
   /** [out] 每The RF Rx path analog gain in amplitude log2 format */
   int16     stepGainAlog2;
   /** [out] 每The phase jump compensation value */
   int16     phaseJumpComp;
   /** [out] - The Rx path RF band */
   HwdRfBandT rfBand;
   /** [out] - The RF Rx path */
   HwdRfRxEnumT rxPath;

} HwdRfRxAgcImmedParamT;


/** Ads structure to provide parameters to the HwdRfRxOnImmed() function. */
typedef struct HwdRfRxOnImmedParamT_tag
{
   /** [in] 每 The RF band class type */
   SysCdmaBandT     rfBand;
   /** [in] - The channel number. */
   uint16           channelNum;
}HwdRfRxOnImmedParamT;

typedef struct
{
    uint8    rxDiversityCtrl;
}HwdRfRxDiversityCtrlMsgT;

/** Ads structure to provide parameters to the HwdRfGetPocCfg() function. */
#define RF_POC_CW_TYPE_NUM_MAX             (14)
#define RF_POC_RX_IIP2_ROUTE_NUM_MAX       (10)
#define RF_POC_RX_IRR_DC_ROUTE_NUM_MAX     (4)

typedef enum
{
  MML1_RF_RFIC1   = 0x0000,
  MML1_RF_RFIC2   = 0x0001,
  MML1_RF_PMIC    = 0x0002,
  MML1_RF_MIPI0   = 0x0003,
  MML1_RF_MIPI1   = 0x0004,
  MML1_RF_MIPI2   = 0x0005,
  MML1_RF_MIPI3   = 0x0006,
  MML1_RF_MIPI4   = 0x0007,
  MML1_RF_PORT_CNT,
}MML1_RF_BSIMM_PORT_T;

typedef enum
{
  MML1_MIPI_RW          = 0x0000,
  MML1_MIPI_EXTRW_1BYTE = 0x0001,
  MML1_MIPI_EXTRW_2BYTE = 0x0002,
  MML1_MIPI_EXTRW_3BYTE = 0x0003,
  MML1_MIPI_EXTRW_4BYTE = 0x0004,
  MML1_MIPI_SUPPORT_RW_CNT,
}MML1_MIPI_REG_RW_T;

typedef enum
{
  RxTx_MainOn    = 0x0,
  RxTx_MainOff   = 0x1,
  RxTx_DivOn     = 0x2,
  RxTx_DivOff    = 0x3,
  RxTx_TxOn      = 0x4,
  RxTx_TxOff     = 0x5
}RfRxTxOnOffT;

/** Ads structure to provide parameters to the HwdRfGetHwCapability() function. */
typedef struct
{
  HwdRfCrystalT crystalType;
  bool          crystalInPmic; /** TRUE: 26MHz from PMIC. FALSE: 26MHz from RF */
  bool          enable32kLess; /** TRUE: with 32K less. FALSE: with 32K RTC*/
  uint16        rfRxPathNum;   /** The number of RF device Rx path */
  uint16        numOfSupportedBand;
  SysCdmaBandT  supportedBand[HWD_RF_BAND_MAX];

}HwdRfCapabilityT;

/* TX power backoff item */
typedef NV_PACKED_PREFIX struct
{
  int16 Temp;
  int16 BackOffPwr; /* TX back off power, in unit of 1/32 */
} NV_PACKED_POSTFIX HwdTxPwrBackOffT;

/* TX power backoff parameters */
typedef NV_PACKED_PREFIX struct
{
  HwdTxPwrBackOffT TxPwrBackOff[HWD_TX_PWR_BACK_OFF_HYST_NUM];
} NV_PACKED_POSTFIX HwdTxPwrBackOffTblT;


/* ----------------- Macro definition ---------------------- */
/** Convert dB to amplitude log2(Q5) format */
#define M_DbToAlog2Q5(vALUE)   \
    ((int16)((((int32)(vALUE)) * DBM_TO_ALOG2_CONVERSION_FACTOR_Q16) >> (16 - 5)))

/** Convert dB(Q5) to amplitude log2(Q5) format */
#define M_DbQ5ToAlog2Q5(vALUE) \
    ((int16)((((int32)(vALUE)) * DBM_TO_ALOG2_CONVERSION_FACTOR_Q16) >> 16))

/** Convert dB to amplitude log2(Q7) format */
#define M_DbToAlog2Q7(vALUE)   \
    ((int16)((((int32)(vALUE)) * DBM_TO_ALOG2_CONVERSION_FACTOR_Q16) >> (16 - 7)))

/** Convert dB(Q5) to amplitude log2(Q7) format */
#define M_DbQ5ToAlog2Q7(vALUE) \
    ((int16)((((int32)(vALUE)) * DBM_TO_ALOG2_CONVERSION_FACTOR_Q16) >> (16 - 2)))

/** Convert damplitude log2(Q7) to dB format */
#define M_Alog2Q7ToDb(vALUE)   \
    ((int16)(((int32)(vALUE) << (16-7)) / DBM_TO_ALOG2_CONVERSION_FACTOR_Q16))

/** Convert damplitude log2(Q7) to dB format */
#define M_Alog2Q7ToDbQ5(vALUE)   \
    ((int16)(((int32)(vALUE) << (16-2)) / DBM_TO_ALOG2_CONVERSION_FACTOR_Q16))

/** Convert damplitude log2(Q5) to dB format */
#define M_Alog2Q5ToDb(vALUE)   \
    ((int16)(((int32)(vALUE) << (16-5)) / DBM_TO_ALOG2_CONVERSION_FACTOR_Q16))

/** Convert dB(Q5) to dB format */
#define M_DbQ5ToDb(vALUE)   ((vALUE) >> 5)

/** assertion with condiction, assert when condition is true*/
#define MonFaultCondition(Condition,Unit, Code1, Code2, Type)  \
if(!(Condition)) {MonFault(Unit, Code1, Code2, Type);}

#endif
/*----------------------------------------------------------------------------
 Global Data
----------------------------------------------------------------------------*/
extern HwdRfCalDataStatusSpyT  HwdCalSavedData[SYS_MODE_MAX];
#if defined MTK_PLT_ON_PC
#define HwdSysInLisr() 0
#else
extern uint32          NU_Thread_Id;

#define HwdSysInLisr()                                  \
   ((NU_Thread_Id >= EXE_LISR_START_THREAD_ID) &&     \
     (NU_Thread_Id <  EXE_LAST_LISR_THREAD_ID))     
#endif
/*----------------------------------------------------------------------------
 Global Function Prototypes
----------------------------------------------------------------------------*/

/*****************************************************************************

  FUNCTION NAME:   HwdMainRfPllChannelSet

  DESCRIPTION:     Programs the RF Plls for the specified channel.
                   NOTE: This routine should be used outside the HWD for testing or
                   calibration purposes only!!!

  PARAMETERS:      Band    - CDMA band class
                   Channel - Channel number
                   InterfaceType - 1x or DO

  RETURNED VALUES: None

*****************************************************************************/
extern void  HwdMainRfPllChannelSet(SysCdmaBandT  PllBand, uint32 Channel, uint8 InterfaceType);

/*****************************************************************************

  FUNCTION NAME:    HwdMainRxPllChannelSet

  DESCRIPTION:      This function is used by to tune the RF PLLs to a desired
                    band and frequency channel on the diversity RF path. A callback 
                    function is set to generate an RF tune complete signal back to 
                    the originator once the RFPLLs have settled.

                    NOTE: This routine only accounts for settling of the RF PLLs
                    and not for any other RF components that may need to settle
                    because the RF transceiver may be powered down prior to tuning
                    (e.g. Rx VCO, etc.)
  
  PARAMETERS:       CdmaBand    -- CDMA Band 
                    FreqChan    -- Frequency Channel
                    InterfaceType -- 1x or DO (MpaMainInterfaceType)
                    RspTaskId   -- Task ID mailbox to send designated signal to
                    RspSignal   -- Signal to generate when tuning is complete
                    ForceBand   -- Boolean, used to force PLL band setup

  RETURNED VALUES:  None

*****************************************************************************/
extern void HwdMainRxPllChannelSet(SysCdmaBandT  PllBand, 
                                   uint32         FreqChan, 
                                   uint8           InterfaceType,
                                   ExeTaskIdT     RspTaskId, 
                                   ExeSignalT     RspSignal, 
                                   bool           ForceBand);

/*****************************************************************************
 
  FUNCTION NAME:   HwdDivRfPllChannelSet

  DESCRIPTION:     Programs the Diversity RF Plls for the specified channel.
                   NOTE: This routine should be used outside the HWD for testing or 
                   calibration purposes only!!!

  PARAMETERS:      Band    - CDMA band class
                   Channel - Channel number

  RETURNED VALUES: None

*****************************************************************************/
extern void  HwdDivRfPllChannelSet(SysCdmaBandT  PllBand, uint32 Channel, uint8 InterfaceType);

/*****************************************************************************
 
  FUNCTION NAME:    HwdDivRxPllChannelSet
  
  DESCRIPTION:      This function is used by to tune the RF PLLs to a desired
                    band and frequency channel on the diversity RF path. A callback 
                    function is set to generate an RF tune complete signal back to 
                    the originator once the RFPLLs have settled.

                    NOTE: This routine only accounts for settling of the RF PLLs
                    and not for any other RF components that may need to settle
                    because the RF transceiver may be powered down prior to tuning
                    (e.g. Rx VCO, etc.)
  
  PARAMETERS:       CdmaBand    -- CDMA Band 
                    FreqChan    -- Frequency Channel
                    RspTaskId   -- Task ID mailbox to send designated signal to
                    RspSignal   -- Signal to generate when tuning is complete
                    ForceBand   -- Boolean, used to force PLL band setup

  RETURNED VALUES:  None

*****************************************************************************/
extern void HwdDivRxPllChannelSet(SysCdmaBandT   PllBand, 
                                   uint32         FreqChan,
                                   uint8           InterfaceType,
                                   ExeTaskIdT     RspTaskId, 
                                   ExeSignalT     RspSignal, 
                                   bool           ForceBand);

/*****************************************************************************
 
  FUNCTION NAME:   HwdSecRfPllChannelSet

  DESCRIPTION:     Programs the 2nd main RF Plls for the specified channel.
                   NOTE: This routine should be used outside the HWD for testing or 
                   calibration purposes only!!!

  PARAMETERS:      Band    - CDMA band class
                   Channel - Channel number

  RETURNED VALUES: None

*****************************************************************************/
extern void  HwdSecRfPllChannelSet(SysCdmaBandT  PllBand, uint32 Channel, uint8 InterfaceType);

/*****************************************************************************
 
  FUNCTION NAME:    HwdSecRxPllChannelSet
  
  DESCRIPTION:      This function is used by to tune the RF PLLs to a desired
                    band and frequency channel on the diversity RF path. A callback 
                    function is set to generate an RF tune complete signal back to 
                    the originator once the RFPLLs have settled.

                    NOTE: This routine only accounts for settling of the RF PLLs
                    and not for any other RF components that may need to settle
                    because the RF transceiver may be powered down prior to tuning
                    (e.g. Rx VCO, etc.)
  
  PARAMETERS:       CdmaBand    -- CDMA Band 
                    FreqChan    -- Frequency Channel
                    RspTaskId   -- Task ID mailbox to send designated signal to
                    RspSignal   -- Signal to generate when tuning is complete
                    ForceBand   -- Boolean, used to force PLL band setup

  RETURNED VALUES:  None

*****************************************************************************/
extern void HwdSecRxPllChannelSet(SysCdmaBandT   PllBand, 
                                   uint32         FreqChan,
                                   uint8           InterfaceType,
                                   ExeTaskIdT     RspTaskId, 
                                   ExeSignalT     RspSignal, 
                                   bool           ForceBand);

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
/*****************************************************************************
 
  FUNCTION NAME:    HwdMainTxPllChannelSet

  DESCRIPTION:      This function is used by L1D to tune the main RF Tx PLLs to a
                    desired band and frequency channel. A callback function is set to
                    generate an RF tune complete signal back to L1D once the RF
                    PLLs have settled.

                    NOTE: This routine only accounts for settling of the RF PLLs
                    and not for any other RF components that may need to settle
                    because the RF transceiver may be powered down prior to tuning
                    (e.g. Rx VCO, etc.)
  
  PARAMETERS:       CdmaBand    -- CDMA Band 
                    FreqChan    -- Frequency Channel
                    RspTaskId   -- Task ID mailbox to send designated signal to
                    RspSignal   -- Signal to generate when tuning is complete
                    ForceBand   -- Boolean, used to force PLL band setup
    
  RETURNED VALUES: None

*****************************************************************************/
void HwdMainTxPllChannelSet(SysCdmaBandT PllBand,
                            uint32 FreqChan,
                            ExeTaskIdT RspTaskId,
                            ExeSignalT RspSignal,
                            bool ForceBand,
                            uint8 Mode);
#endif

/*****************************************************************************

  FUNCTION NAME:   HwdRfConstantsPtrGet
  
  DESCRIPTION:     Returns pointer to the modem constants structure, which provides
	               hardware constants required by L1D.
	
  PARAMETERS:      None
	  
  RETURNED VALUES: pointer to HwdRfConstants.
		
*****************************************************************************/
extern HwdRfConstantsT* HwdRfConstantsPtrGet(void);
/*****************************************************************************
 
  FUNCTION NAME:   SetupRfConstants

  DESCRIPTION:     Setups RF Constants

  PARAMETERS:      TxOnId - TX_ON identifier using DEV_TX_ON_DLYMODE_DIN identifiers

  RETURNED VALUES: uint16 - DSPM-accessed address for TX_ON delay comporartors.

*****************************************************************************/
extern void SetupRfConstants(void);

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
/*****************************************************************************
 
  FUNCTION NAME:    HwdPllIsLockDetectEnabled
  
  DESCRIPTION:      This function is the API for requesting the state of pll lock 
                    detect.
  
  PARAMETERS:       None

  RETURNED VALUES:  Enable state

*****************************************************************************/
extern bool HwdPllIsLockDetectEnabled (void);

/*****************************************************************************
 
  FUNCTION NAME:    HwdPllLockLost
  
  DESCRIPTION:      This function deals with the PLL lock lost condition.
  
  PARAMETERS:       None

  RETURNED VALUES:  None

*****************************************************************************/
extern void HwdPllLockLost (void);
#endif

/*****************************************************************************
 
  FUNCTION NAME:   HwdRfSetRxTxIQSwap

  DESCRIPTION:     Sets Rx Tx IQ swap.

  PARAMETERS:      uint32, bool, bool

  RETURNED VALUES: None

*****************************************************************************/
extern void HwdRfSetRxTxIQSwap(HwdRfBandT Band, uint8 RxSwap, uint8 TxSwap, uint8 DivRxSwap);
/*****************************************************************************

  FUNCTION NAME:   HwdRfGetRxTxIQSwap

  DESCRIPTION:     Gets Rx Tx IQ swap.

  PARAMETERS:      uint32, bool*, bool*

  RETURNED VALUES: None

*****************************************************************************/
extern void HwdRfGetRxTxIQSwap(HwdRfBandT Band, uint8 *RxSwap, uint8 *TxSwap, uint8 *DivRxSwap);

/*****************************************************************************
 
  FUNCTION NAME:    HwdRfGetLowGainTxonMask()

  DESCRIPTION:      This function returns the current TxOn bitmask used to program
                    the TX_ON loading Data_IN hardware for the TxAGC Low Gain State
                    and is based on the current band class 

  PARAMETERS:       uint8 MpaTxNum

  RETURNED VALUES:  Pointer to a uint16 updated with the TxON mask

*****************************************************************************/
extern uint16* HwdRfGetLowGainTxonMask(uint8 MpaTxNum);

/*****************************************************************************
 
  FUNCTION NAME:    HwdRfGetMedGainTxonMask()

  DESCRIPTION:      This function returns the current TxOn bitmask used to program
                    the TX_ON loading Data_IN hardware for the TxAGC Med Gain State
                    and is based on the current band class 

  PARAMETERS:       uint8 MpaTxNum

  RETURNED VALUES:  Pointer to a uint16 updated with the TxON mask

*****************************************************************************/
extern uint16* HwdRfGetMedGainTxonMask(uint8 MpaTxNum);

/*****************************************************************************
 
  FUNCTION NAME:    HwdRfGetHighGainTxonMask()

  DESCRIPTION:      This function returns the current TxOn bitmask used to program
                    the TX_ON loading Data_IN hardware for the TxAGC High Gain State
                    and is based on the current band class 

  PARAMETERS:       uint8 MpaTxNum

  RETURNED VALUES:  Pointer to a uint16 updated with the TxON mask

*****************************************************************************/
extern uint16* HwdRfGetHighGainTxonMask(uint8 MpaTxNum);

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
/*****************************************************************************
 
  FUNCTION NAME:    HwdRfGetGainCtrlUnitStruct()

  DESCRIPTION:      This function returns a pointer to the current Gain Control
                    Unit structure (HwdDoGainControlUnitT) used to program the
                    Tx Delay-Loading information to the DSPM and based on the
                    current band class. This information will be reused to program
                    the DO Delayed Load parameters.

  PARAMETERS:       RevProtocol: Rev0/RevA

  RETURNED VALUES:  Pointer to a HwdDoGainControlUnitT struct

*****************************************************************************/
extern HwdDoGainControlUnitT* HwdRfGetGainCtrlUnitStruct(uint8 RevProtocol, HwdDoGainCtrlUnitIdxT HwdDoGainCtrlUnitIdx);
#endif

extern void  HwdRfSetDefault1xAntenna(uint8 Antenna);
extern uint8 HwdRfGetDefault1xAntenna(void);

/* HWD Calibration unit APIs */
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern int16  HwdCalMainDagcToALog2(HwdRfBandT  RfBand, int16 DigiGain, uint8 RxGainSt8);
extern int16  HwdCalDivDagcToALog2(HwdRfBandT  RfBand, int16 DigiGain, uint8 RxGainSt8);
extern int16  HwdCalRxAgcMainGetStepGainAdj(HwdRfBandT RfBand, uint8 OldRxGainSt8, uint8 NewRxGainSt8);
extern int16  HwdCalRxAgcDivGetStepGainAdj(HwdRfBandT RfBand, uint8 OldRxGainSt8, uint8 NewRxGainSt8);
#endif

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern void   HwdRfMainRxLnaCtrl(uint16 GainState, uint8 Interface);
extern void   HwdRfDivRxLnaCtrl(uint16 GainState, uint8 Interface);
#endif

extern bool   HwdRfBandValidate(HwdRfBandT RfBand);

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern uint8  HwdCalDetermineTxGainState(HwdRfBandT  RfBand, int16 TxPwrALog2, uint8 TxGainSt8);
extern uint16 HwdCalDetermineTxHwVal(HwdRfBandT  RfBand, int16 TxPwrALog2, uint8 TxGainSt8);
extern uint8  HwdCalMainDetermineGainState(HwdRfBandT  RfBand, int16 PwrALog2, uint8 RxGainSt8);
extern uint8  HwdCalDivDetermineGainState(HwdRfBandT  RfBand, int16 PwrALog2, uint8 RxGainSt8);
extern void   HwdCalMainDivTableSwitch(HwdCalMainDivTableSwitchT TableSwitch);
#endif
extern void   HwdCal1xImdTableSwitch(HwdCalMainDivTableSwitchT TableSwitch);

extern uint16 *HwdRfReturnImdFeqTapsPtr(void);

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern void   HwdCalApplyMainRssiCorrection(int16 *RxPwrALog2P, uint8 RxGainSt8);
extern void   HwdCalApplyDivRssiCorrection(int16 *RxPwrALog2P, uint8 RxGainSt8);
#else
extern void   HwdCalApplyRssiCorrection(int16 *RxPwrALog2P);
#endif

extern uint16 HwdRfGetSlotTiming(HwdRfTimingSelectT select);
//#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern void   HwdCalTxAgcAltThreshSet(SysAirInterfaceT Mode);

extern void   HwdCalTxAgcAltThreshReset(SysAirInterfaceT Mode);
//#endif

extern int16  HwdCalReadCurrentTemp(SysAirInterfaceT Mode);

extern void   HwdHdetSet1xDataRate(bool TurboDecoder, uint8 RevRc, uint8 Rate);
extern void   HwdHdetSetDoDataRate(bool RevA, uint8 Rate);

extern uint8  HwdRfGetAfcPdmNumber(SysAirInterfaceT Interface);
extern uint8  HwdRfGetTxAgcPdmNumber(SysAirInterfaceT Interface);

#if (SYS_ASIC <= SA_CBP82)
#define HwdGetAfcPdmMaskReg(Interface) HwdPdmMaskRegAddr[HwdRfGetAfcPdmNumber(Interface)]
#define HwdGetTxAgcPdmMaskReg(Interface) HwdPdmMaskRegAddr[HwdRfGetTxAgcPdmNumber(Interface)]
#define HwdGetTxAgcSpdmMaskReg(Interface) HwdPdmMaskRegAddr[HwdRfGetTxAgcSpdmNumber(Interface)]
#define HwdGetAfcPdmMaskRdBackReg(Interface) HwdPdmRdBackRegAddr[HwdRfGetAfcPdmNumber(Interface)]
#define HwdGetTxAgcPdmMaskRdBackReg(Interface) HwdPdmRdBackRegAddr[HwdRfGetTxAgcPdmNumber(Interface)]
#define HwdGetAfcPdmDinReg(Interface) HwdPdmDinRegAddr[HwdRfGetAfcPdmNumber(Interface)]
#define HwdGetTxAgcPdmDinReg(Interface) HwdPdmDinRegAddr[HwdRfGetTxAgcPdmNumber(Interface)]
#endif

extern uint8 HwdGetMaxBandSupport(void);
extern int16 HwdGetAfcHwvInterc(void);
extern int16 HwdGetTxAgcAdj(HwdRfBandT RfBand);
extern int16 HwdGetMainRxAgcAdj(HwdRfBandT RfBand, uint8 GainState);
extern int16 HwdGetDivRxAgcAdj(HwdRfBandT RfBand, uint8 GainState);

extern int16 HwdAfcGetInterceptParm(void);
/* DO RMC RFC AFC calibration routines */
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern uint16 HwdCalibAfcDacValueCalc(int32 AfcValuePpm);
extern int16  HwdAfcDeltaDacValueCalc(int32 FreqErrorQ4, SysAirInterfaceT Interface);
#else
extern uint16 HwdCalibAfcDacValueCalc(int16 AfcOffsetHz, uint16 carrierFreq);
extern uint16 HwdAfcDeltaDacValueCalc(int32 FreqErrorHz, SysAirInterfaceT Interface);
#endif

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
extern uint32 HwdAfcGetCapIdParm(void);
extern uint16 *HwdAfcGetCenterFreqTable(void);
#endif

#ifdef __DYNAMIC_ANTENNA_TUNING__
extern void HwdRfCustDatFeRouteDataProcess(void *MsgDataPtr);
extern void HwdRfCustDatBpiDataProcess(void *MsgDataPtr);
extern void HwdRfCustDatBpiDataPostProcess(void * dataPtr);
#endif

extern void  HwdRfGpsOn(void);
extern void  HwdRfGpsOff(void);

extern bool    HwdSetGpsCalibDbRfDelay(int32 delay1X, int32 delayDO, HwdRfBandT RfBand);
extern int32  HwdGetGpsCalibDbRfDelay1X(HwdRfBandT RfBand);
extern int32  HwdGetGpsCalibDbRfDelayDO(HwdRfBandT RfBand);

extern uint16 HwdGetDefaultTxPath(void);

extern void *HwdRfGetMpaRfAntennaPtr(void);

extern void  HwdCalDisplayRfSpy(SysAirInterfaceT Mode);
extern void  HwdRfGsmCdmaInterferenceCtrl(bool On);

extern bool  HwdRfIsDiversityCompiledIn(void);
extern bool  HwdRfGetPA2Enable(void);
extern bool  HwdRfGetRfSpiDlyEnable(void);

#if !defined(MTK_CBP) || defined(MTK_PLT_ON_PC)
extern uint16 HwdCalSetTxSPdmHwVal(int16 TxPwrALog2, uint8 TxGainSt8);
extern void   HwdRfSetTxSPdmHwValWrite(uint16 TxAgcSPdmVal);
extern uint8  HwdRfGetTxAgcSpdmNumber(SysAirInterfaceT Interface);
extern uint32 HwdRfGetTxAgcSpdmDelayAddr(bool SlotHSlot);
#endif

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
/*****************************************************************************
 
  FUNCTION NAME:    HwdRfGetTemperatureAdc()

  DESCRIPTION:      This function is use to get the temperature measurement result.
                    Note: for 1xRTT mode the read back temperature will be send 
                    from DSPm to CP.

  PARAMETERS:       void

  RETURNED VALUES:  uint16 tempAdc: The temperature ADC value that come from RFIC

*****************************************************************************/
extern uint16 HwdRfGetTemperatureAdc(bool externalMeas);



/*****************************************************************************
 
  FUNCTION NAME:    HwdRfGetTemperatureCelsius()

  DESCRIPTION:      This function is use to get the temperature measurement result.


  PARAMETERS:       adcValue - temperature ADC value

  RETURNED VALUES:  uint8 temperature: The temperature value in celsius degree.

*****************************************************************************/
extern int16 HwdRfGetTemperatureCelsius(uint16 adcValue);

/*****************************************************************************
 
  FUNCTION NAME:    HwdRfStartTemperatureMeas()

  DESCRIPTION: This function is use to start the temperature measurement.
                      The temperature sampling time is about 20uS.
                      bool external - indicate whether start external measurement or internal measurement


  PARAMETERS:       void

  RETURNED VALUES:  void

*****************************************************************************/
extern void HwdRfStartTemperatureMeas(bool externalMeas);

/*****************************************************************************
 
  FUNCTION NAME:    HwdRfGetTsxTemperatureCelsius()

  DESCRIPTION:      This function is use to get the TSX temperature.


  PARAMETERS:       void

  RETURNED VALUES:  uint16 temperature: The temperature value in celsius degree.

*****************************************************************************/
extern int16 HwdRfGetTsxTemperatureCelsius(void);

/*****************************************************************************
 
  FUNCTION NAME:    HwdRfWakeUpRestore()

  DESCRIPTION:      This function is use to restore the RF relative configuration
                    after sleep. RF driver should compare the saved RXAGC gain
                    index with the configured gain index to avoid the wrong 
                    configuration.


  PARAMETERS:       int16  rxgainIndex

  RETURNED VALUES:  void

*****************************************************************************/
extern void HwdRfWakeUpRestore(void);

/*****************************************************************************
  FUNCTION NAME:    HwdRfPowerOnCal()

  DESCRIPTION:      Function to perform the required actions to do the power
                    on calibration.
                    a)	Call the OrionC RF device driver via function pointer to
                        do the power on Rx/Tx calibration.
                    b)	For OrionC RXRC/RXDC/TXDC/TXDNL calibration are needed, the
                        code words writing is done in RF device driver via RFSPI
                        in immediate mode.
                    d)	The DBB TXDC CAL hardware module is triggered in software
                        mode after write RF mode code word (CW1).

                    Note: The calibration implement is done in RF device driver,
                    it's because the calibration items and procedure are
                    different with different RF solution.

  PARAMETERS:       void

  RETURNED VALUES:  void

*****************************************************************************/
extern void HwdRfPowerOnCal(void);

/*****************************************************************************
 
  FUNCTION NAME:    HwdRfPreburstTxCal()

  DESCRIPTION:      Function to perform the required actions to do the Tx power
                    on calibration
                    a)	Call the OrionC RF device driver via function pointer to
                        do the power on Tx calibration.
                    b)	For OrionC only TXDC offset calibration is needed, the
                        code words writing is done in RF device driver via RFSPI
                        in immediate mode.
                    c)	The TXDAC is enabled before start TXDC offset calibration.
                    d)	The DBB TXDC CAL hardware module is triggered in register
                        mode after write RF mode code word (CW1).
                    e)	After calibration the Tx path status is turn to idle mode
                        with code word CW1.

                    Note: The calibration implement is done in RF device driver,
                    it's because the Tx calibration items and procedure are
                    different with different RF solution. For OrionC only TXDC
                    offset calibration is needed.

  PARAMETERS:       void

  RETURNED VALUES:  void

*****************************************************************************/
extern void HwdRfPreburstTxCal(uint8 InterfaceType);

/*****************************************************************************
 
  FUNCTION NAME:    HwdRfGetTxAgcCompensation()

  DESCRIPTION:      This function is used to get the current temperature and
                    frequency compensation in Tx path.

  PARAMETERS:       void

  RETURNED VALUES:  Int16 totalComp - The current total compensation in 1/32dB. 

*****************************************************************************/
#if defined (MTK_DEV_OPTIMIZE_EVL1) && !defined(MTK_PLT_ON_PC)
DEF_CodeInIram(extern int16 HwdRfGetTxAgcCompensation(void));
#else
extern int16 HwdRfGetTxAgcCompensation(void);
#endif
/*****************************************************************************
 
  FUNCTION NAME:    HwdRfTxAgcCtrl()

  DESCRIPTION:      This function is includes these functional:
                    a)	Calculate the PA mode and configure the PA mode RFGPO
                        logic to TXONs.
                    b)	Calculate the PA bias power supply value and configure
                        it to APCDAC
                    c)	Calculate and compensate the PA gain value refer to
                        the calibration data from RFNVRAM.
                    d)	The RF TXAGC gain index LUT and configure it by device
                        driver via TXSPI delay buffer.


  PARAMETERS:       HwdRfTxAgcParamT   *adsPtr - The structure pointer

  RETURNED VALUES:  void

*****************************************************************************/
#if defined (MTK_DEV_OPTIMIZE_EVL1) && !defined(MTK_PLT_ON_PC)
DEF_CodeInIram(extern void HwdRfTxAgcCtrl(HwdRfTxAgcParamT *adsPtr));
#else
extern void HwdRfTxAgcCtrl(HwdRfTxAgcParamT *adsPtr);
#endif

#ifdef MTK_DEV_HW_SIM_RF
extern void HwdRfTxAgcCtrlHwSim(HwdRfTxAgcParamT *adsPtr);
#endif
/*****************************************************************************
 
  FUNCTION NAME:    HwdRfSetTxAgcDelayLoadSlot()

  DESCRIPTION:      This function is used to configure which delay signal will
                    be triggered by delay load. It's used for 1xRTT and EVDO mode.


  PARAMETERS:       Int16 sigMsk - Indicate which signal will be triggered
                                   0: Not trigger
                                   1: Trigger
                    BIT7	 | BIT6	 |  BIT5	   | BIT4	  | BIT3	| BIT2	| BIT1	  |  BIT0
                    -------|-------|-----------|--------|-------|-------|---------|--------
                    paMode | paVdd | phaseComp | pdetRd | txPga | gbb1  | gbb0_ol | gbb0_cl
                    
                    BIT8    | BIT9 |
                    ----------------
                    pDetADC | DDPC |

                    Int16 startOffset - the start offset from the left of delay
                                        load window to the first delay signal of
                                        TXAGC, unit is in chips.

  RETURNED VALUES:  void

*****************************************************************************/
#ifdef MTK_DEV_HW_SIM_RF
#define M_HwdRfSetTxAgcDelayLoadSlot(sIGmSK, sTRAToFFSET) do {\
    HwdRfSetTxAgcDelayLoadSlot(sIGmSK, sTRAToFFSET); \
    HwdRfSetTxAgcDelayLoadSlotHwSim(sIGmSK, sTRAToFFSET); \
} while (0)
#else
#define M_HwdRfSetTxAgcDelayLoadSlot(sIGmSK, sTRAToFFSET) \
    HwdRfSetTxAgcDelayLoadSlot(sIGmSK, sTRAToFFSET)
#endif /* MTK_DEV_HW_SIM_RF */

DEF_CODE(extern void HwdRfSetTxAgcDelayLoadSlot(int16 sigMsk, int16 startOffset));
extern void HwdRfSetTxAgcDelayLoadSlotHwSim(int16 sigMsk, int16 startOffset);

/*****************************************************************************
 
  FUNCTION NAME:    HwdRfSetTxAgcDelayLoadHalfSlot()

  DESCRIPTION:      This function is used to configure which delay signal will
                    be triggered by delay load. It's used for 1xRTT and EVDO mode.


  PARAMETERS:       Int16 sigMsk - Indicate which signal will be triggered
                                   0: Not trigger
                                   1: Trigger
                    BIT7	 | BIT6	 |  BIT5	   | BIT4	  | BIT3	| BIT2	| BIT1	  |  BIT0
                    -------|-------|-----------|--------|-------|-------|---------|--------
                    paMode | paVdd | phaseComp | pdetRd | txPga | gbb1  | gbb0_ol | gbb0_cl

                    BIT8    | BIT9 |
                    ----------------
                    pDetADC | DDPC |
                    
                    Int16 startOffset - the start offset from the left of delay
                                        load window to the first delay signal of
                                        TXAGC, unit is in chips.

  RETURNED VALUES:  void

*****************************************************************************/
#ifdef MTK_DEV_HW_SIM_RF
#define M_HwdRfSetTxAgcDelayLoadHalfSlot(sIGmSK, sTRAToFFSET) do {\
    HwdRfSetTxAgcDelayLoadHalfSlot(sIGmSK, sTRAToFFSET); \
    HwdRfSetTxAgcDelayLoadHalfSlotHwSim(sIGmSK, sTRAToFFSET); \
} while (0)
#else
#define M_HwdRfSetTxAgcDelayLoadHalfSlot(sIGmSK, sTRAToFFSET) \
    HwdRfSetTxAgcDelayLoadHalfSlot(sIGmSK, sTRAToFFSET)
#endif /* MTK_DEV_HW_SIM_RF */

DEF_CODE(extern void HwdRfSetTxAgcDelayLoadHalfSlot(int16 sigMsk, int16 startOffset));
extern void HwdRfSetTxAgcDelayLoadHalfSlotHwSim(int16 sigMsk, int16 startOffset);

/*****************************************************************************
 
  FUNCTION NAME:    HwdRfMainRxAgcDcCtrl()

  DESCRIPTION:      This function is includes these functional for Rx main path:
                    a)	Calculate and compensate the Rx power refers to the
                        calibration data from RFNVRAM.
                    b)	Using Rx power to look up the RF RXAGC table and configure
                        the RF RX gain to RFIC via RFSPI.
                    c)	Feedback the RF step gain to L1 for RX digital gain control.


  PARAMETERS:       HwdRfRxAgcDcParamT   *adsPtr - The structure pointer

  RETURNED VALUES:  void

*****************************************************************************/
extern void HwdRfMainRxAgcDcCtrl(HwdRfRxAgcDcParamT *adsPtr);
#ifdef MTK_DEV_HW_SIM_RF
extern void HwdRfMainRxAgcDcCtrlHwSim(HwdRfRxAgcDcParamT *adsPtr);
#endif

/*****************************************************************************
 
  FUNCTION NAME:    HwdRfDivRxAgcDcCtrl()

  DESCRIPTION:      This function is includes these functional for Rx main path:
                    a)	Calculate and compensate the Rx power refers to the
                        calibration data from RFNVRAM.
                    b)	Using Rx power to look up the RF RXAGC table and configure
                        the RF RX gain to RFIC via RFSPI.
                    c)	Feedback the RF step gain to L1 for RX digital gain control.


  PARAMETERS:       HwdRfRxAgcDcParamT   *adsPtr - The structure pointer

  RETURNED VALUES:  void

*****************************************************************************/
extern void HwdRfDivRxAgcDcCtrl(HwdRfRxAgcDcParamT *adsPtr);
#ifdef MTK_DEV_HW_SIM_RF
extern void HwdRfDivRxAgcDcCtrlHwSim(HwdRfRxAgcDcParamT *adsPtr);
#endif
/*****************************************************************************
 
  FUNCTION NAME:    HwdRfTxAgcCtrlImmed()

  DESCRIPTION:      This function is used to set paMode/paVdd/pgaGain/gbb1/gbb0_ol
                    in immediate mode, the detect path is enable in default.


  PARAMETERS:       HwdRfTxAgcImmedParamT   *adsPtr - The structure pointer.

  RETURNED VALUES:  void

*****************************************************************************/
extern void HwdRfTxAgcCtrlImmed(HwdRfTxAgcImmedParamT *adsPtr);
#ifdef MTK_DEV_HW_SIM_RF
extern void HwdRfTxAgcCtrlImmedHwSim(HwdRfTxAgcImmedParamT *adsPtr);
#endif

/*****************************************************************************
  FUNCTION NAME:     HwdRfTxAgcCtrlImmedForDbg
  DESCRIPTION:       TXAGC without delay loader
  PARAMETERS:        HwdRfTxAgcParamT
  RETURN VALUES:     void
*****************************************************************************/
extern void HwdRfTxAgcCtrlImmedForDbg(HwdRfTxAgcParamT *adsPtr);

/*****************************************************************************
 
  FUNCTION NAME:    HwdRfGetDdpcResultImmed()

  DESCRIPTION:      This function is get the DDPC result in software mode..


  PARAMETERS:       int16  ddpcResul - The analog gain that read back from DDPC,
                                       format is S6.5, unit is 1/32dB.

  RETURNED VALUES:  void

*****************************************************************************/
extern int16 HwdRfGetDdpcResultImmed(void);

/*****************************************************************************
 
  FUNCTION NAME:    HwdRfTxOnImmed()

  DESCRIPTION:      This function is used to turn on TXDAC and RF Tx path immediately.


  PARAMETERS:       HwdRfTxOnAgcImmedParamT *adsPtr - the structure pointer.

  RETURNED VALUES:  void

*****************************************************************************/
extern void HwdRfTxOnImmed(HwdRfTxOnImmedParamT *adsPtr);

/*****************************************************************************
 
  FUNCTION NAME:    HwdRfTxOffImmed()

  DESCRIPTION:      This function is used to turn off TXDAC and RF Tx path immediately.


  PARAMETERS:       void

  RETURNED VALUES:  void

*****************************************************************************/
extern void HwdRfTxOffImmed(HwdRfTxOffImmedParamT *adsPtr);

/*****************************************************************************
 
  FUNCTION NAME:    HwdRfSetFineGain1Immed()

  DESCRIPTION:      This function is used to set GBB1 in immediate mode.


  PARAMETERS:       void

  RETURNED VALUES:  void

*****************************************************************************/
extern void HwdRfSetFineGain1Immed(uint16 fineGainCw);
/*****************************************************************************
 
  FUNCTION NAME:    HwdRfGetRxAgcCompensation()

  DESCRIPTION:      This function is used to get the current temperature and
                    frequency compensation in Rx path.

  PARAMETERS:       rxPath - RF Rx main or diversity path

  RETURNED VALUES:  Int16 totalCompAlog2 - The current total compensation in amplitude
                                           log2 format. 

*****************************************************************************/
extern int16 HwdRfGetRxAgcCompensation(HwdRfMpaEnumT rxPath);
#ifdef MTK_DEV_HW_SIM_RF
extern int16 HwdRfGetRxAgcCompensationHwSim(HwdRfMpaEnumT rxPath);
#endif
/*****************************************************************************
 
  FUNCTION NAME:    HwdRfGetRxRefInfo()

  DESCRIPTION:      This function is get the reference RSSI and reference digital
                    and analog gain from RFNVRAM.

  PARAMETERS:       rxPath - RF Rx main or diversity path
                    refRssiAlog2 - the reference RSSI in amplitude log2 format.
                    refGainAlog2 - the reference gain that include digital
                                   and analog gain in amplitude log2 format.

  RETURNED VALUES:  void 

*****************************************************************************/
extern void HwdRfGetRxRefInfo(HwdRfMpaEnumT rxPath, int16 *refRssiAlog2, int16 *refGainAlog2);


/*****************************************************************************
 
  FUNCTION NAME:    HwdRfMainRxAgcDcCtrl()

  DESCRIPTION:      This function is includes these functional for Rx main path:
                    a)	Calculate and compensate the Rx power refers to the
                        calibration data from RFNVRAM.
                    b)	Using Rx power to look up the RF RXAGC table and configure
                        the RF RX gain to RFIC via RFSPI.
                    c)	Feedback the RF step gain to L1 for RX digital gain control.


  PARAMETERS:       HwdRfRxAgcImmedParamT   *adsPtr - The structure pointer

  RETURNED VALUES:  void

*****************************************************************************/
extern void HwdRfMainRxAgcCtrlImmed(HwdRfRxAgcImmedParamT *adsPtr);
#ifdef MTK_DEV_HW_SIM_RF
extern void HwdRfMainRxAgcCtrlImmedHwSim(HwdRfRxAgcImmedParamT *adsPtr);
#endif
/*****************************************************************************
 
  FUNCTION NAME:    HwdRfDivRxAgcDcCtrl()

  DESCRIPTION:      This function is includes these functional for Rx main path:
                    a)	Calculate and compensate the Rx power refers to the
                        calibration data from RFNVRAM.
                    b)	Using Rx power to look up the RF RXAGC table and configure
                        the RF RX gain to RFIC via RFSPI.
                    c)	Feedback the RF step gain to L1 for RX digital gain control.


  PARAMETERS:       HwdRfRxAgcDcParamT   *adsPtr - The structure pointer

  RETURNED VALUES:  void

*****************************************************************************/
extern void HwdRfDivRxAgcCtrlImmed(HwdRfRxAgcImmedParamT *adsPtr);
#ifdef MTK_DEV_HW_SIM_RF
extern void HwdRfDivRxAgcCtrlImmedHwSim(HwdRfRxAgcImmedParamT *adsPtr);
#endif

/*****************************************************************************
 
  FUNCTION NAME:    HwdRfMainRxOnImmed()

  DESCRIPTION:      This function is used to turn on main Rx path RXADC and 
                    RF Rx path immediately.

  PARAMETERS:       HwdRfRxOnImmedParamT *adsPtr - the structure pointer.

  RETURNED VALUES:  void

*****************************************************************************/
extern void HwdRfMainRxOnImmed(HwdRfRxOnImmedParamT *adsPtr);

/*****************************************************************************
 
  FUNCTION NAME:    HwdRfMainRxOffImmed()

  DESCRIPTION:      This function is used to turn off main Rx path RXADC and 
                    RF Rx path immediately.

  PARAMETERS:       void

  RETURNED VALUES:  void

*****************************************************************************/
extern void HwdRfMainRxOffImmed(void);

/*****************************************************************************
 
  FUNCTION NAME:    HwdRfDivRxOnImmed()

  DESCRIPTION:      This function is used to turn on diversity Rx path RXADC and 
                    RF Rx path immediately.

  PARAMETERS:       HwdRfRxOnImmedParamT *adsPtr - the structure pointer.

  RETURNED VALUES:  void

*****************************************************************************/
extern void HwdRfDivRxOnImmed(HwdRfRxOnImmedParamT *adsPtr);

/*****************************************************************************
 
  FUNCTION NAME:    HwdRfDivRxOffImmed()

  DESCRIPTION:      This function is used to turn off diversity Rx path RXADC and 
                    RF Rx path immediately.

  PARAMETERS:       void

  RETURNED VALUES:  void

*****************************************************************************/
extern void HwdRfDivRxOffImmed(void);


/***********************************************
 FUNCTION NAME: HwdRfSetAfcCapId()
 
 DESCRIPTION:  This fucntion is used to set the AFC CapID value

 PARAMETERS: uint8 CapIdValue

 RETURNS VALUES: void
 ***********************************************/
void HwdRfSetAfcCapId(uint8 CapIdValue);

/***********************************************
 FUNCTION NAME: HwdRfSetAfcDac()
 
 DESCRIPTION:  This fucntion is used to set the AFC DAC value

 PARAMETERS: uint16 AfcDacValue

 RETURNS VALUES: void
 ***********************************************/
void HwdRfSetAfcDac(uint16 AfcDacValue);

/***********************************************
 FUNCTION NAME: HwdRfGetAfcParms()
 
 DESCRIPTION:  This fucntion is used to get the AFC Parameters

 PARAMETERS:  uint8* CapIdvalue
              uint16* AfcDacValue

 RETURNS VALUES: void
 ***********************************************/
void HwdRfGetAfcParms(uint8* CapIdvalue, uint16* AfcDacValue);


/***********************************************
 FUNCTION NAME: HwdRfSetTxSpiData()
 
 DESCRIPTION:  This fucntion is used to set the TX SPI Data

 PARAMETERS:  uint32 Address
              uint32 Data

 RETURNS VALUES: void
 ***********************************************/
void  HwdRfSetTxSpiData(uint32 Address, uint32 Data);

/***********************************************
 FUNCTION NAME: HwdRfGetTxSpiData()
 
 DESCRIPTION:  This fucntion is used to get a  TX SPI Data

 PARAMETERS:  uint32 Address

 RETURNS VALUES: uint32 
 ***********************************************/
uint32 HwdRfGetTxSpiData(uint32 Address);

/***********************************************
 FUNCTION NAME: HwdRfSetRfSpiData()
 
 DESCRIPTION:  This fucntion is used to set the RF SPI Data

 PARAMETERS:  uint32 Address
              uint32 Data

 RETURNS VALUES: void
 ***********************************************/
void  HwdRfSetRfSpiData(uint32 Address, uint32 Data);

/***********************************************
 FUNCTION NAME: HwdRfGetRfSpiData()
 
 DESCRIPTION:  This fucntion is used to get a RF SPI Data

 PARAMETERS:  uint32 Address

 RETURNS VALUES: uint32 
 ***********************************************/
uint32 HwdRfGetRfSpiData(uint32 Address);

/***********************************************
 FUNCTION NAME: HwdRfSetSineTone()
 
 DESCRIPTION:  This fucntion is used to set the Tx Sine Tone Parameters

 PARAMETERS:  uint32 Frequency
              uint8 OnOff

 RETURNS VALUES: void
 ***********************************************/
uint32 HwdRfSetSineTone(uint32 Frequency, uint8 OnOff, bool TestMode);


/***********************************************
 FUNCTION NAME: HwdRfGetRxTempCompensation()
 
 DESCRIPTION: This fucntion is used to get currnt Rx Temperatur Compensation
              for main and diversity path

 PARAMETERS:  HwdRfMpaEnumT RfPath

 RETURNS VALUES: Rx Temperature Compensation for Main and Diversity Path
 ***********************************************/
int16 HwdRfGetRxTempCompensation(HwdRfMpaEnumT RxPath);


/***********************************************
 FUNCTION NAME: HwdRfGetTxTempCompensation()
 
 DESCRIPTION: This fucntion is used to get currnt Tx Temperatur Compensation

 PARAMETERS:  None

 RETURNS VALUES: Tx Temperature Compensation
 ***********************************************/
int16 HwdRfGetTxTempCompensation(void);

 /***********************************************
 FUNCTION NAME: HwdRfCheckPathSet()
 
 DESCRIPTION:  This fucntion is used to check whether TRx path is set or not

 PARAMETERS:  HwdRfMpaEnumT RfPath

 RETURNS VALUES: TRUE if set, FALSE if free
 ***********************************************/
bool HwdRfCheckPathSet(HwdRfMpaEnumT RfPath);

/***********************************************
 FUNCTION NAME: HwdRfPathEmptyCheck()
 
 DESCRIPTION:  This fucntion is used to check whether none of path has been set or not

 PARAMETERS:  void

 RETURNS VALUES: TRUE if free, else FALSE
 ***********************************************/

bool HwdRfPathEmptyCheck(void);


/*****************************************************************************
 
  FUNCTION NAME:    HwdRfGetRfChipId

  DESCRIPTION:      This function is used to get the RF chip ID.

  PARAMETERS:       void

  RETURNED VALUES:  The RF chip

*****************************************************************************/
uint32 HwdRfGetChipId(void);

/*****************************************************************************
 
  FUNCTION NAME:    HwdRfSetPlatform()

  DESCRIPTION:      This function is used assign the RF HWD driver funtion and data
                    pointer to a specific device driver.
                    Note: this function must be called before any other RF driver.

  PARAMETERS:       void

  RETURNED VALUES:  void 

*****************************************************************************/
extern void HwdRfSetPlatform(void);

/*****************************************************************************

  FUNCTION NAME:  HwdAfcDacToFoe

  DESCRIPTION:    This function is use to convert the AFC DAC value to FOE value

  PARAMETERS:     AfcDacValue 

  RETURNED VALUES:  afcFoePpb - the frequency offset error of AT's carrier 
                                compare with AN's carrier in ppb unit in ppb unit.

*****************************************************************************/
extern int16 HwdAfcDacToFoePpb(uint16 AfcDacValue);

/*****************************************************************************

  FUNCTION NAME:  HwdAfcFoePpbToDac

  DESCRIPTION:    This function is use to convert the AFC FOE value to AFC DAC value

  PARAMETERS:     afcFoePpb - the frequency offset error of AT's carrier 
                              compare with AN's carrier in ppb unit in ppb unit.

  RETURNED VALUES: AfcDacValue
*****************************************************************************/
extern uint16 HwdAfcFoePpbToDac(int afcFoePpb);

/***********************************************
 FUNCTION NAME: HwdGetCurrentRfPllBandClass()
 
 DESCRIPTION:  This fucntion is used to get the RF pll
               band class by path.

 PARAMETERS:  SysAirInterfaceT

 RETURNS VALUES: bandClass: bandClass.
 ***********************************************/
extern SysCdmaBandT HwdGetCurrentRfPllBandClass(SysAirInterfaceT Interface);

/***********************************************
 FUNCTION NAME: HwdGetCurrentRfPllFreq()
 
 DESCRIPTION:  This fucntion is used to get the RF pll frequency by path.

 PARAMETERS:  SysAirInterfaceT

 RETURNS VALUES: uint32 freq: RF carrier frequency in KHz unit.
 ***********************************************/
extern uint32 HwdGetCurrentRfPllFreq(SysAirInterfaceT Interface);

/***********************************************
 FUNCTION NAME: HwdGetCurrentRfPllFoeHz()
 
 DESCRIPTION:  This fucntion is used to get the RF pll
               frequency offset error by path.

 PARAMETERS:  SysAirInterfaceT

 RETURNS VALUES: int32 FoeHz: frequency offet error in Hz unit.
 ***********************************************/
extern int32 HwdGetCurrentRfPllFoeHz(SysAirInterfaceT Interface);

/***********************************************
 FUNCTION NAME: HwdGetCurrentRfPllFoePpb()
 
 DESCRIPTION:  This fucntion is used to get the RF pll
               frequency offset error by path.

 PARAMETERS:  SysAirInterfaceT

 RETURNS VALUES: int32 FoePpb: frequency offet error in ppb unit.
 ***********************************************/
extern int32 HwdGetCurrentRfPllFoePpb(SysAirInterfaceT Interface);


/*****************************************************************************
  FUNCTION NAME:   HwdRfOrionPlusSetStxPowerMode

  DESCRIPTION:     Update the STX RE_LOCK_LPM and OOB_LPM power mode status,
                   it should be done before Tx pll tunning (start tx window)

  PARAMETERS:      txRelockLpm
                   txOobLpm

  RETURNED VALUES: void

*****************************************************************************/
extern void HwdRfSetStxPowerMode(HwdRfPwrModeT txRelockLpm,HwdRfPwrModeT dynPmOob);

/*****************************************************************************
  FUNCTION NAME:   HwdRfSetDynTxPowerMode

  DESCRIPTION:     Dynamic Update the EVM_LPM and OOB_LPM power mode status,
                   it should be set before TXAGC.

  PARAMETERS:      txRelockLpm
                   txOobLpm

  RETURNED VALUES: void

*****************************************************************************/
extern void HwdRfSetDynTxPowerMode(HwdRfPwrModeT dynPmEvm,HwdRfPwrModeT dynPmOob);

/***********************************************
 FUNCTION NAME: HwdRxChannelToFrequency()
 
 DESCRIPTION:  Function to translate the channel to carrier frequency.

 PARAMETERS:  channel - the channel number.
              band    - the band class of the Rx path

 RETURNS VALUES: the carrier frequency, the unit is 1kHz.
 ***********************************************/
extern uint32 HwdRxChannelToFrequency (uint32 channel,SysCdmaBandT bandClass);

/***********************************************
 FUNCTION NAME: HwdTxChannelToFrequency()
 
 DESCRIPTION:  Function to translate the channel to carrier frequency.

 PARAMETERS:  channel - the channel number.
              band    - the band class of the Tx path

 RETURNS VALUES: the carrier frequency, the unit is 1kHz.
 ***********************************************/
extern uint32 HwdTxChannelToFrequency (uint32 channel,SysCdmaBandT bandClass);

/***********************************************
 FUNCTION NAME: HwdRfDacToPpb()
 
 DESCRIPTION:  Function to translate the AFCDAC to FOE.

 PARAMETERS:  Dac - the AFCDAC value.

 RETURNS VALUES: the FOE, the unit is ppb.
 ***********************************************/
extern int32 HwdRfDacToPpb(uint32 Dac);

/***********************************************
 FUNCTION NAME: HwdRfPpbToDac()
 
 DESCRIPTION:  Function to translate the FOE to AFCDAC value.

 PARAMETERS:  Ppb - the FOE value.

 RETURNS VALUES: the AFCDAC value
 ***********************************************/
extern uint32 HwdRfPpbToDac(int32 Ppb);

/***********************************************
 FUNCTION NAME: HwdAfcGetScurveParam()
 
 DESCRIPTION:  Function to get initial FOE vlaue in ppb unit.

 PARAMETERS:  void.

 RETURNS VALUES: initial FOE vlaue in ppb unit
 ***********************************************/
extern int32 HwdAfcGetInitFoePpb(void);

/*****************************************************************************//**
 * @brief      Generate the initial s-curve from factory coefficient C0-C3.
 * @param[in]  N/A
 * @return     N/A
 * @date       2015.05.12
 * @todo       N/A
 * @warning    N/A
 *******************************************************************************/
extern void HwdMmAfcTmsFacScurveGenerationProc(void);

/*****************************************************************************//**
 * @brief      Perform TFC interpolation calculation according to TFC SCurve table. 
 *             Using interpolation to find the compensated frequency error.
 *             Linear interpolation: 8 <= u2tfc_ubin_idx <= 45 
 *             Parabolic interpolation: u2tfc_ubin_idx < 8 or 45 < u2tfc_ubin_idx
 * @param[in]  N/A
 * @return     N/A
 * @date       2015.05.03
 * @todo       N/A
 * @warning    N/A
 *******************************************************************************/
extern int32 HwdMmAfcTmsTfcintpInterpCalc(const int32 tfc_u);


/*****************************************************************************
  FUNCTION NAME:   HwdRfSetOrigPocResult

  DESCRIPTION:     Process the original POC result to final POC result

  PARAMETERS:      void

  RETURNED VALUES: void

*****************************************************************************/
extern void HwdRfSetOrigPocResult(void *adsPtr);


/*****************************************************************************
  FUNCTION NAME:   HwdRfGetOrigPocResultSize

  DESCRIPTION:     get the size of original POC result

  PARAMETERS:      void

  RETURNED VALUES: size of original POC result

*****************************************************************************/
extern uint32 HwdRfGetOrigPocResultSize(void);

/*****************************************************************************
  FUNCTION NAME:   HwdRfSetFinalPocResult

  DESCRIPTION:     Save the final POC data from RFNVRAM

  PARAMETERS:      void

  RETURNED VALUES: void

*****************************************************************************/
extern void HwdRfSetFinalPocResult(void *adsPtr,HwdRfBandT rfBand);


/*****************************************************************************
  FUNCTION NAME:   HwdRfGetFinalPocResult

  DESCRIPTION:     get the pointer of POC result structure variable

  PARAMETERS:      void

  RETURNED VALUES: void

*****************************************************************************/
extern void* HwdRfGetFinalPocResult(void);


/*****************************************************************************
  FUNCTION NAME:   HwdRfGetFinalPocResultSize

  DESCRIPTION:     get the size of POC result structure variable

  PARAMETERS:      void

  RETURNED VALUES: void

*****************************************************************************/
extern uint32 HwdRfGetFinalPocResultSize(void);


/*****************************************************************************
 
  FUNCTION NAME:    HwdRfGetRxPocTbl()

  DESCRIPTION:      This function is use feedback final Rx POC result.

  PARAMETERS:       calibParm - The struture pointer

  RETURNED VALUES:  void

*****************************************************************************/
extern void HwdRfGetRxPocTbl(void *calibParm);

/*****************************************************************************
 
  FUNCTION NAME:    HwdRfGetTxPocTbl()

  DESCRIPTION:      This function is use feedback final Tx POC result.

  PARAMETERS:       calibParm - The struture pointer

  RETURNED VALUES:  void

*****************************************************************************/
extern void HwdRfGetTxPocTbl(void *calibParm);

/*****************************************************************************
 
  FUNCTION NAME:    HwdRfGetDetPocTbl()

  DESCRIPTION:      This function is use feedback final DET POC result.

  PARAMETERS:       calibParm - The struture pointer

  RETURNED VALUES:  void

*****************************************************************************/
extern void HwdRfGetDetPocTbl(void *calibParm);


/*****************************************************************************
  FUNCTION NAME:   HwdRfGetPocCfg

  DESCRIPTION:     get the pointer of POC configuration structure variable

  PARAMETERS:      void

  RETURNED VALUES: void

*****************************************************************************/
extern void HwdRfGetPocCfg(void *adsPtr);


/*****************************************************************************
  FUNCTION NAME:   HwdRfGetPocCfgSize

  DESCRIPTION:     get the size of POC configuration structure variable

  PARAMETERS:      void

  RETURNED VALUES: size of POC configuration

*****************************************************************************/
extern uint32 HwdRfGetPocCfgSize(void);

/*****************************************************************************

    FUNCTION NAME:   HwdRfGetHwCapability

    DESCRIPTION:     This routine is used to get the capability of RF platform

    PARAMETERS:      void

    RETURNED VALUES: void

*****************************************************************************/
extern void HwdRfGetHwCapability(HwdRfCapabilityT *adsPtr);

/*****************************************************************************

    FUNCTION NAME:   HwdCalibGetAGpsGrpDly

    DESCRIPTION:     This routine is used to get A-GPS group delay.

    PARAMETERS:      SysCdmaBandT band - CDMA band class.

    RETURNED VALUES: int16 - Group delay.

*****************************************************************************/
extern int16 HwdCalibGetAGpsGrpDly(SysCdmaBandT band);

/*****************************************************************************

    FUNCTION NAME:   HwdCalibGetTxPwrBackOffRegion

    DESCRIPTION:     This routine is used to get Tx Power Backoff region.

    PARAMETERS:      SysCdmaBandT band - CDMA band class.

    RETURNED VALUES: HwdTxPwrBackOffTblT.

*****************************************************************************/
extern HwdTxPwrBackOffTblT *HwdCalibGetTxPwrBackOffRegion(SysCdmaBandT band);

#if defined(SYS_OPTION_CCIRQ_PCCIF) || defined(SYS_OPTION_CCIRQ_CCIRQ)
#if !defined(MTK_PLT_RF_ORIONC)
/*****************************************************************************
 
FUNCTION NAME:   HwdRfPorInit

DESCRIPTION:     register a CC-IRQ callback for POR

PARAMETERS:      void.

RETURNED VALUES: None

*****************************************************************************/
extern void HwdRfPorInit(void);
#endif
#endif

/*****************************************************************************
  FUNCTION NAME:   HwdRfGetRxPathFsm

  DESCRIPTION:     get the current RF Rx path FSM

  PARAMETERS:      void

  RETURNED VALUES: HwdRxFsmT

*****************************************************************************/
extern HwdRxFsmT HwdRfGetRxPathFsm(void);

/*****************************************************************************
  FUNCTION NAME:   HwdRfGetRxCalPower

  DESCRIPTION:     get the Rx calibration cell power

  PARAMETERS:      pwrMode - HPM or LPM
                   lnaMode - LNA mode

  RETURNED VALUES: Rx power in Q5(1/32dBm)

*****************************************************************************/
extern int16 HwdRfGetRxCalPower(HwdRfPwrModeT pwrMode, HwdRfLnaModeEnumT lnaMode);

#if (SYS_BOARD >= SB_JADE)

/*****************************************************************************

  FUNCTION NAME:   HwdTxMaxPowerUpdate
  
  DESCRIPTION:     Update the tx max power according the temprature

  PARAMETERS:      Input temprature
  
  RETURNED VALUES: None.

*****************************************************************************/

extern void HwdTxMaxPowerUpdate(int16 TempCelsiu);
#endif

#endif

#ifdef MTK_CBP
/***********************************************
 FUNCTION NAME: HwdRfPaOn()
 
 DESCRIPTION:  This fucntion is used to turn on the PA immediately in EVDO mode

 PARAMETERS:  void

 RETURNS VALUES: void
 ***********************************************/
void HwdRfPaOn(void);

/***********************************************
 FUNCTION NAME: HwdRfPaOff()
 
 DESCRIPTION:  This fucntion is used to turn off the PA immediately in EVDO mode

 PARAMETERS:  void

 RETURNS VALUES: void
 ***********************************************/
void HwdRfPaOff(void);

/***********************************************
 FUNCTION NAME: HwdRfSetPaStatus()
 
 DESCRIPTION:  This fucntion is used to set PA on/off status

 PARAMETERS:  paStatus - TRUE: PA_ON; FALSE: PA_OFF

 RETURNS VALUES: void
 ***********************************************/
void HwdRfSetPaStatus(bool paStatus);

/***********************************************
 FUNCTION NAME: HwdRfGetPaStatus()
 
 DESCRIPTION:   This fucntion is used to set PA on/off status

 PARAMETERS:    void

 RETURNS VALUES: paStatus - TRUE: PA_ON; FALSE: PA_OFF
 ***********************************************/
extern bool HwdRfGetPaStatus(void);

#endif

#ifdef __SAR_TX_POWER_BACKOFF_SUPPORT__
extern void HwdRfOrionPlusSarUpdateMaxTxPower(HwdRfBandT rfBand, uint16 rfChannel, uint8 InterfaceType);
extern int16 HwdRf1xTxPowerOffset(uint8 antIdx);
extern int16 HwdRfDoTxPowerOffset(uint8 antIdx);
#endif
#ifdef __TX_POWER_OFFSET_SUPPORT__
extern void HwdRfOrionPlusUpdateMaxTxPower(HwdRfBandT rfBand, uint16 rfChannel, uint8 InterfaceType);
#endif



/*****************************************************************************
* End of File
*****************************************************************************/
#endif
/**Log information: \main\Trophy\Trophy_ylxiao_href22033\1 2013-03-18 14:15:36 GMT ylxiao
** HREF#22033, merge 4.6.0**/
/**Log information: \main\Trophy\1 2013-03-19 05:19:56 GMT hzhang
** HREF#22033 to merge 0.4.6 code from SD.**/
/**Log information: \main\Trophy\Trophy_jluo_href22084\1 2013-04-02 09:25:32 GMT jluo
** href#22084,update the driver**/
/**Log information: \main\Trophy\2 2013-04-03 02:36:00 GMT czhang
** href#22084**/
