/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef _HWDDELAYAPI_H
#define _HWDDELAYAPI_H

/*****************************************************************************
* 
* FILE NAME   : hwddelayapi.h
*
* DESCRIPTION : API Header file containing typedefs and definitions pertaining
*               to the Delay Loader Hardware Driver which are local to the HWD unit.
*****************************************************************************/

/*----------------------------------------------------------------------------
 Include Files
----------------------------------------------------------------------------*/
#include "sysdefs.h"
#include "hwdapi.h"
#include "monapi.h"
#include "hwderror.h"
#include "hwdrfbsi.h"
#ifdef MTK_DEV_HW_SIM_RF
#include "hwdhwsim.h"
#endif /* MTK_DEV_HW_SIM_RF */


/*----------------------------------------------------------------------------
 Defines and Macros
----------------------------------------------------------------------------*/
//#define DLY_REG_DUMP_EN

typedef enum
{
    DLYLDR_WIN_SET,
    DLYLDR_CNT_EN,
    DLYLDR_TRIG_SET,
    DLYLDR_REG_WRITE,
    DLYLDR_REG_READ,
    DLYLDR_REG_WRITE_SIM,
    DLYLDR_REG_READ_SIM
} HwdDlyLdrTraceCtrlT;

#ifdef MTK_DEV_HW_SIM_RF
#define HwdDlyWriteHwSim(aDDR, dATA) HwdHwSimRegWrite(aDDR, dATA)
#define HwdDlyReadHwSim(aDDR)        HwdHwSimRegRead(aDDR)
#endif /* MTK_DEV_HW_SIM_RF */

#ifdef DLY_REG_DUMP_EN
#define HwdDlyWrite(aDDR, dATA) do {\
    HwdWrite(aDDR, dATA);   \
    MonTrace(MON_CP_HWD_DLYLDR_TRACE_ID, 3, DLYLDR_REG_WRITE, aDDR, dATA);  \
} while(0)
#define HwdDlyRead(aDDR)                HwdRead(aDDR)
#else /* DLY_REG_DUMP_EN */
#define HwdDlyWrite(aDDR, dATA)         HwdWrite(aDDR, dATA)
#define HwdDlyRead(aDDR)                HwdRead(aDDR)
#endif /* DLY_REG_DUMP_EN */

#define DLYTRIG_INVALID                 (0xffff)

/*----------------------------------------------------------------------------
 Typedefs
----------------------------------------------------------------------------*/
/* Delay Load Trigger Enable in one slot in EVDO*/
typedef enum {
    HWD_DLYTRIG_FIRST = 0,
    HWD_DLYTRIG_SECOND,
#ifndef MTK_DEV_OPTIMIZE_DLYLDR
    HWD_DLYTRIG_BOTH,
#endif
    HWD_DLYTRIG_NONE = 0xff,    /* No delay triggers will be generated */
} HwdDlyLdrTrigNumT;
    

/* Delay Loader offset in chips from the counter start */
typedef struct {
    int16    first;
    int16    second;
} HwdDlyLdrOftT;

/* Delay Loader Registers records */
typedef struct
{
    uint32 cmpRegAddr;
    uint16 mskRegBit;
} HwdDlyTrigConfT;

typedef struct
{
    uint32 disRegAddr;
    uint32 mskRegAddr;
    uint16 disRegBit;
    uint16 mskRegMask;
} HwdDlyDisMskConfT;

typedef struct
{
    uint16 bsiTrig[BSI_NAME_MAX];
    uint16 bpiTrig[2];
} HwdDlyBsiBpiTrigT;

typedef void (*HwdDelayInitTrigTblFuncT)(uint16 *);
typedef void (*HwdDelayGetBsiTrigFuncT)(uint8 bsiName);

/*----------------------------------------------------------------------------
 Global Variables
----------------------------------------------------------------------------*/
extern HwdDlyDisMskConfT *DlyDisMskRegsPtr;
extern HwdDlyTrigConfT *DlyTrigConfPtr[2];
extern HwdDlyBsiBpiTrigT *DlyBsiBpiTrigPtr;

extern HwdDelayInitTrigTblFuncT DlyInitTrigTblFuncPtr;

#ifdef SYS_OPTION_HPUE_ENABLED
extern uint16 DlyMipiHpueTrigJade[];
#endif

#ifdef MTK_DEV_HW_SIM_RF
extern HwdDlyDisMskConfT *DlyDisMskRegsHwSimPtr;
extern HwdDlyTrigConfT *DlyTrigConfHwSimPtr[2];
extern HwdDlyBsiBpiTrigT *DlyBsiBpiTrigHwSimPtr;

extern HwdDelayInitTrigTblFuncT DlyInitTrigTblFuncHwSimPtr;
#endif /* MTK_DEV_HW_SIM_RF */


/*----------------------------------------------------------------------------
 Function Prototypes
----------------------------------------------------------------------------*/
extern void HwdDelayLoadInit(void);
extern void HwdDelayLoadRegInit(void);
extern void HwdDelayLoadWindowSizeSet(uint8 setUpTime);
extern void HwdDelayLoadCounterEnable(BOOL en);
extern void HwdTxAgcSetTpcDelayTrigData(void);
#ifdef SYS_OPTION_HPUE_ENABLED
extern void HwdTxAgcSetHpueDelayTrigData(void);
#endif
#ifdef MTK_DEV_RF_TEST
extern void HwdDlyDumpReg(void);
#endif /* MTK_DEV_RF_TEST */
extern void HwdDelayLoadModeSwitch(SysAirInterfaceT sysMode);

/*----------------------------------------------------------------------------
 Inline Functions
----------------------------------------------------------------------------*/
static __inline void HwdDelayLoadSet(uint16 trigId,
                     HwdDlyLdrTrigNumT trigNum,
                     HwdDlyLdrOftT offset)
{
    uint32 cmpRegAddr;
    uint32 disRegAddr = DlyDisMskRegsPtr[trigId].disRegAddr;
    uint32 mskRegAddr = DlyDisMskRegsPtr[trigId].mskRegAddr;
    uint16 disRegBit = DlyDisMskRegsPtr[trigId].disRegBit;
    uint16 mskRegMask = DlyDisMskRegsPtr[trigId].mskRegMask;
    uint16 mskRegBit;
    uint16 cmpVal[2];

    switch (trigNum)
    {
        case HWD_DLYTRIG_FIRST:
        case HWD_DLYTRIG_SECOND:
            cmpRegAddr = DlyTrigConfPtr[trigNum][trigId].cmpRegAddr;
            mskRegBit = DlyTrigConfPtr[trigNum][trigId].mskRegBit;
            cmpVal[0] = offset.first;
            cmpVal[1] = offset.second;
            /* Disable compare */
            HwdDlyWrite(disRegAddr, HwdDlyRead(disRegAddr) | disRegBit);
            /* Configure the mask */
            HwdDlyWrite(mskRegAddr, (HwdDlyRead(mskRegAddr) & (~mskRegMask)) | mskRegBit);
            /* Configure compare value */
            HwdDlyWrite(cmpRegAddr, 255 - cmpVal[trigNum]);
            /* Enable compare */
            HwdDlyWrite(disRegAddr, HwdDlyRead(disRegAddr) & (~disRegBit));
            break;
        case HWD_DLYTRIG_NONE:
            /* Disable compare */
            HwdDlyWrite(disRegAddr, HwdDlyRead(disRegAddr) | disRegBit);
            break;
        default:
            MonFault(MON_HWD_FAULT_UNIT, HWD_ERR_DLYLDR_FAIL, trigNum, MON_HALT);
            break;
    }
};

#ifdef MTK_DEV_HW_SIM_RF
static __inline void HwdDelayLoadSetHwSim(uint16 trigId,
                     HwdDlyLdrTrigNumT trigNum,
                     HwdDlyLdrOftT offset)
{
    uint32 cmpRegAddr;
    uint32 disRegAddr = DlyDisMskRegsHwSimPtr[trigId].disRegAddr;
    uint32 mskRegAddr = DlyDisMskRegsHwSimPtr[trigId].mskRegAddr;
    uint16 disRegBit = DlyDisMskRegsHwSimPtr[trigId].disRegBit;
    uint16 mskRegMask = DlyDisMskRegsHwSimPtr[trigId].mskRegMask;
    uint16 mskRegBit;
    uint16 cmpVal[2];

    switch (trigNum)
    {
        case HWD_DLYTRIG_FIRST:
        case HWD_DLYTRIG_SECOND:
            cmpRegAddr = DlyTrigConfHwSimPtr[trigNum][trigId].cmpRegAddr;
            mskRegBit = DlyTrigConfHwSimPtr[trigNum][trigId].mskRegBit;
            cmpVal[0] = offset.first;
            cmpVal[1] = offset.second;
            /* Disable compare */
            HwdDlyWriteHwSim(disRegAddr, HwdDlyReadHwSim(disRegAddr) | disRegBit);
            /* Configure the mask */
            HwdDlyWriteHwSim(mskRegAddr, (HwdDlyReadHwSim(mskRegAddr) & (~mskRegMask)) | mskRegBit);
            /* Configure compare value */
            HwdDlyWriteHwSim(cmpRegAddr, 255 - cmpVal[trigNum]);
            /* Enable compare */
            HwdDlyWriteHwSim(disRegAddr, HwdDlyReadHwSim(disRegAddr) & (~disRegBit));
            break;
        case HWD_DLYTRIG_NONE:
            /* Disable compare */
            HwdDlyWriteHwSim(disRegAddr, HwdDlyReadHwSim(disRegAddr) | disRegBit);
            break;
        default:
            MonFault(MON_HWD_FAULT_UNIT, HWD_ERR_DLYLDR_FAIL, trigNum, MON_HALT);
            break;
    }
};
#endif /* MTK_DEV_HW_SIM_RF */

#ifdef MTK_PLT_DENALI
#include "hwddelaydenali.h"
#endif

#if (SYS_BOARD >= SB_JADE)
#include "hwddelayjade.h"
#endif

#ifdef MTK_DEV_HW_SIM_RF
#include "hwddelayhwsim.h"
#endif

/*****************************************************************************
* End of File
*****************************************************************************/
#endif  /* _HWDDELAYAPI_H */
