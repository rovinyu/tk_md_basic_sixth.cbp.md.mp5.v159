/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2012
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*****************************************************************************
 *
 * Filename:
 * ---------
 *   cc_irq_public.h
 *
 * Project:
 * --------
 *   CC IRQ
 *
 * Description:
 * ------------
 *   This Module defines CC IRQ API.
 *
 * Author:
 * -------
 *
 *
 *============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 * $Revision:
 * $Modtime:
 * $Log:
 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *============================================================================
 ****************************************************************************/

#ifndef __CC_IRQ_PUBLIC_H__
#define __CC_IRQ_PUBLIC_H__

#include "monapi.h"
#ifndef MTK_PLT_ON_PC
#ifdef SYS_OPTION_CCIRQ_PCCIF
#ifndef MTK_DEV_C2K_SRLTE_L1
#include "../shared/cross_core/global_modid.h"
#else
#ifndef MTK_DEV_DENALI_SRLTE_PRE_IT
#include "../shared/cross_core_umoly/global_modid.h"
#else
#include "../shared/cross_core_temp/global_modid.h"
#endif /* MTK_DEV_DENALI_SRLTE_PRE_IT */
#endif /* MTK_DEV_C2K_SRLTE_L1 */
#endif /*SYS_OPTION_CCIRQ_PCCIF*/
#ifdef SYS_OPTION_CCIRQ_CCIRQ
#include "../shared/cross_core_umoly/global_modid.h"
#include "../shared/cross_core_umoly/global_cc_irq_id.h"
#include "../shared/cross_core_umoly/global_cc_user_share_mem_id.h"
#include "cc_sys_comm_public.h"
#endif /*SYS_OPTION_CCIRQ_CCIRQ*/
#endif /* MTK_PLT_ON_PC */

/*Public API*/
#define MD1_CORE_NUM                2 /*PScore, L1core*/
#define GlMod_ID_T  uint32

#ifdef SYS_OPTION_CCIRQ_PCCIF
#if !defined(SYS_OPTION_CCIRQ_PCCIF_UT)
#define CC_IRQ_MD1MD2_NUMBER    (CC_IRQ_MD1MD2_END - CC_IRQ_MD1MD2_START)
#define CC_IRQ_MD2MD1_NUMBER    (CC_IRQ_MD2MD1_END - CC_IRQ_MD2MD1_START)
#endif
#define CC_IRQ_SYS_USER_MD1MD2_NUMBER    (CC_IRQ_SYS_USER_MD1MD2_END - CC_IRQ_SYS_USER_MD1MD2_START)
#define CC_IRQ_SYS_USER_MD2MD1_NUMBER    (CC_IRQ_SYS_USER_MD2MD1_END - CC_IRQ_SYS_USER_MD2MD1_START)
#endif /*SYS_OPTION_CCIRQ_PCCIF*/

#ifdef SYS_OPTION_CCIRQ_CCIRQ
#define CC_IRQ_L2P_NUMBER      (CC_IRQ_L2P_END - CC_IRQ_L2P_BASE)
#define CC_IRQ_P2L_NUMBER      (CC_IRQ_P2L_END - CC_IRQ_P2L_BASE)

#define CC_IRQ_L2MD3_NUMBER    (CC_IRQ_L2MD3_END - CC_IRQ_L2MD3_BASE)
#define CC_IRQ_P2MD3_NUMBER    (CC_IRQ_P2MD3_END - CC_IRQ_P2MD3_BASE)
#define CC_IRQ_MD32L_NUMBER    (CC_IRQ_MD32L_END - CC_IRQ_MD32L_BASE)
#define CC_IRQ_MD32P_NUMBER    (CC_IRQ_MD32P_END - CC_IRQ_MD32P_BASE)

#define CC_IRQ_RET_STATUS_T CC_IRQ_RET_STATUS_E

#define spinlockid CC_SPINLOCK_T*

#endif

/* user callback function type */
typedef uint32 ( *CcIrqMsgHandle )(uint32 MsgId, void *MsgBufferP, uint32 MsgSize);
typedef void (*CC_IRQ_CALLBACK_P)(uint32 para0, uint32 para1, uint32 para2);

typedef struct {
	ExeTaskIdT TaskId;
	GlMod_ID_T GlModId;
	ExeMailboxIdT MailboxId;
	CcIrqMsgHandle handle;
} GlobleToLocalModMapTable_T;

typedef enum {
    IRAT_MD1_MSG_ERROR_NONE = 0,
    IRAT_MD1_MSG_ERROR_ID_INVALID,
    IRAT_MD1_MSG_ERROR_SIZE_INCORRECT,
} CC_IRQ_CALLBACK_STATUS_T;


#ifdef SYS_OPTION_CCIRQ_PCCIF
#ifdef MTK_DEV_DENALI_SRLTE_PRE_IT
typedef enum {
    CC_IRQ_MD1MD2_BASE = 0UL,
    /* MD1 to MD2 */
    CC_IRQ_MD1MD2_START = CC_IRQ_MD1MD2_BASE,
    CC_IRQ_MD1MD2_RTB = CC_IRQ_MD1MD2_START,
    CC_IRQ_MD1MD2_SYS,
    CC_IRQ_MD1MD2_END,
    /* MD2 to MD1 */
    CC_IRQ_MD2MD1_START = CC_IRQ_MD1MD2_END,
    CC_IRQ_MD2MD1_RTB = CC_IRQ_MD2MD1_START,
	CC_IRQ_MD2MD1_SYS,
    CC_IRQ_MD2MD1_END
} CC_IRQ_ID_T;
#else
typedef enum {
    CC_IRQ_MD1MD2_BASE = 0UL,
    /* MD1 to MD2 */
    CC_IRQ_MD1MD2_START = CC_IRQ_MD1MD2_BASE,
    CC_IRQ_MD1MD2_SYS = CC_IRQ_MD1MD2_START,
    CC_IRQ_MD1MD2_END,
    /* MD2 to MD1 */
    CC_IRQ_MD2MD1_START = CC_IRQ_MD1MD2_END,
    CC_IRQ_MD2MD1_SYS = CC_IRQ_MD2MD1_START,
    CC_IRQ_MD2MD1_END
} CC_IRQ_ID_T;
#endif

typedef enum {
    CC_IRQ_SUCCESS = 0,
    CC_IRQ_ERR_PARAM        = 0xE001,
    CC_IRQ_ERR_CHANNEL_FULL = 0xE002,
    CC_IRQ_ERR_PEER_ASYNC = 0xE003,
    CC_IRQ_ERR_DISABLED   = 0xE004
} CC_IRQ_RET_STATUS_T;

typedef enum {
    /* MD1 <-> MD2 */
    CC_IRQ_VFLAG_2G_RF_BAND_SYNC,
    CC_IRQ_VFLAG_MD1MD2_END
} CC_IRQ_VFLAG_INDEX_T;

#if (SYS_BOARD == SB_DENALI) && defined(MTK_DEV_32K_LESS)
/* CCIRQ user index */
typedef enum {
    CC_IRQ_SYS_USER_MD1MD2_START = 0,
    /* ==== Start of MD1 to MD2 user index ==== */
    CC_IRQ_SYS_USER_MD1MD2_Cload = CC_IRQ_SYS_USER_MD1MD2_START,
    /* ==== End of MD1 to MD2 user index ==== */
    CC_IRQ_SYS_USER_MD1MD2_END,
    CC_IRQ_SYS_USER_MD2MD1_START = CC_IRQ_SYS_USER_MD1MD2_END,
    /* ==== Start of MD2 to MD1 user index ==== */
    CC_IRQ_SYS_USER_MD2MD1_EPOF = CC_IRQ_SYS_USER_MD2MD1_START,
	CC_IRQ_SYS_USER_MD2MD1_DIV,
	/* ==== End of MD2 to MD1 user index ==== */
    CC_IRQ_SYS_USER_MD2MD1_END
} CC_IRQ_SYS_USER_INDEX_T;
#else
/* CCIRQ user index */
typedef enum {
    CC_IRQ_SYS_USER_MD1MD2_START = 0,
    /* ==== Start of MD1 to MD2 user index ==== */
    CC_IRQ_SYS_USER_MD1MD2_RESERVED = CC_IRQ_SYS_USER_MD1MD2_START,
    /* ==== End of MD1 to MD2 user index ==== */
    CC_IRQ_SYS_USER_MD1MD2_END,
    CC_IRQ_SYS_USER_MD2MD1_START = CC_IRQ_SYS_USER_MD1MD2_END,
    /* ==== Start of MD2 to MD1 user index ==== */
    CC_IRQ_SYS_USER_MD2MD1_EPOF = CC_IRQ_SYS_USER_MD2MD1_START,
    /* ==== End of MD2 to MD1 user index ==== */
    CC_IRQ_SYS_USER_MD2MD1_END
} CC_IRQ_SYS_USER_INDEX_T;
#endif

#if (SYS_BOARD == SB_DENALI) && defined(MTK_DEV_32K_LESS)
/* for function registered by user to execute before/after sync packet to/from MD1. */
/* CC IRQ SYNC index */
typedef enum {
    CC_SYS_SYNC_MD1MD3_START = 0,
    /* ==== Start of MD1 to MD3 sync index(MD1 ping MD3) ==== */
    CC_SYS_SYNC_MD1MD3_LPM_CNT = CC_SYS_SYNC_MD1MD3_START,
    /* ==== End of MD1 to MD3 sync index(MD1 ping MD3) ==== */
    CC_SYS_SYNC_MD1MD3_END,
    CC_SYS_SYNC_MD3MD1_START = CC_SYS_SYNC_MD1MD3_END,
    /* ==== Start of MD3 to MD1 sync index(MD3 ack MD1) ==== */ 
    CC_SYS_SYNC_MD3MD1_RESERVED = CC_SYS_SYNC_MD3MD1_START,
    /* ==== End of MD3 to MD1 sync index(MD3 ack MD1) ==== */
    CC_SYS_SYNC_MD3MD1_END
} CC_SYS_SYNC_INDEX_T;

#define CC_SYS_SYNC_MD1MD3_NUMBER    (CC_SYS_SYNC_MD1MD3_END - CC_SYS_SYNC_MD1MD3_START)
#define CC_SYS_SYNC_MD3MD1_NUMBER    (CC_SYS_SYNC_MD3MD1_END - CC_SYS_SYNC_MD3MD1_START)
typedef void (*CC_SYS_SYNC_CALLBACK_P)(void);
#endif

#endif /*SYS_OPTION_CCIRQ_PCCIF*/

#ifdef SYS_OPTION_CCIRQ_CCIRQ
typedef enum {
    CC_IRQ_CB_ENV_LISR = 0,  /* callback run in LISR context */
    CC_IRQ_CB_ENV_HISR,  /* callback run in HISR context */
    CC_IRQ_CB_ENV_INVALID
} CC_IRQ_CB_ENV;

typedef struct {
	uint32 lock; /*1, other core already get spinlock; 0, can get spinlock*/
	uint32 owner_module; /*no use, just for sync with md1*/
}spinlock_t;

typedef struct
{
   spinlock_t   *spinlock_p; /* point to spinlock_t*/
   uint32       irq_status;
   uint32       direction;   /* to know its direction and already be initial */   
} CC_SPINLOCK_T;

typedef enum {
    CC_SPINLOCK_INFINITE_WAIT = 0,  /* callback run in LISR context */
    CC_SPINLOCK_NO_WAIT,  /* callback run in HISR context */
    CC_SPINLOCK_WAIT_INVALID
} CC_SPINLOCK_WAIT_MODE_T;

typedef enum {
	SPINLOCK_ACCESS_SUCCESS = 0x0,
	SPINLOCK_CONFLICT_ERROR,
	SPINLOCK_ACCESS_FAILED
}SPINLOCK_STATUS_T; 

#endif

extern int32 cc_irq_trigger_interrupt(uint32 cc_index, uint32 para0, uint32 para1, uint32 para2);
extern int32 cc_irq_trigger_interrupt_with_buff(uint32 cc_index, void *addr, uint32 length);
extern int32 cc_irq_mask(uint32 cc_index);
extern int32 cc_irq_unmask(uint32 cc_index);

/*need move to SYS_OPTION_CCIRQ_PCCIF when user modify AFC test for JADE */
#if defined(MTK_DEV_SHARED_AFC_TEST)
extern uint32 * cc_irq_get_256B_shared_memory(void);
#endif

#if ((SYS_BOARD == SB_DENALI) && defined(MTK_DEV_32K_LESS))
extern uint32 * cc_irq_get_256B_shared_memory_32kless(void);
#endif


#ifdef SYS_OPTION_CCIRQ_PCCIF
extern int32 cc_irq_register_callback(uint32 cc_index, CC_IRQ_CALLBACK_P funcp);
extern int32 cc_irq_msg_send(GlMod_ID_T SrcModId, GlMod_ID_T DestModId, uint32 MsgId, void *MsgBufferP, uint32 MsgSize);
extern void cc_sys_comm_excep_handling( uint8 * pExData, uint32 DataLen );
extern int32 cc_irq_user_register_cb(uint32 user_index, CC_IRQ_CALLBACK_P user_cb);
extern uint32* cc_irq_get_shared_vflag(uint32 index, uint32 vflag_id);
extern int32 cc_irq_user_tx(uint32 user_index, void *addr, uint32 length);
#if (SYS_BOARD == SB_DENALI) && defined(MTK_DEV_32K_LESS)
/* function registered by user to execute before sending ping packet to MD1. */
extern int32 cc_sys_sync_md1md3_register_cb(uint32 func_index, CC_SYS_SYNC_CALLBACK_P func_cb);
/* function registered by user to execute after receiving ack packet from MD1 */
extern int32 cc_sys_sync_md3md1_register_cb(uint32 func_index, CC_SYS_SYNC_CALLBACK_P func_cb);
/*Get CCIRQ status*/
extern int32 cc_sys_hs_status(void);
#endif
#endif /*SYS_OPTION_CCIRQ_PCCIF*/

#ifdef SYS_OPTION_CCIRQ_CCIRQ
extern int32 cc_irq_register_callback(uint32 cc_index, CC_IRQ_CALLBACK_P funcp, CC_IRQ_CB_ENV env);
extern uint32 *cc_irq_get_md1_md3_user_shared_memory(uint32 user_id, uint32 size);
extern bool cc_irq_sys_is_ready(void);
extern uint32 cc_irq_sync_get_cc_irq_tx_md1_status(void);
extern int cc_irq_epof_notify_to_md1(void);
extern void cc_irq_get_regs_info(void);
extern void cc_irq_sharemem_dump(uint32* pAdr, uint32* pSize);
extern spinlockid cc_spin_create_spinlock(char *lock_name);
extern uint32 cc_spin_take_spinlock(spinlockid ext_spinlock_id_ptr, CC_SPINLOCK_WAIT_MODE_T wait_mode);
extern void cc_spin_give_spinlock(spinlockid ext_spinlock_id_ptr);
#endif /*SYS_OPTION_CCIRQ_CCIRQ*/


#endif /*__CC_IRQ_PUBLIC_H__*/

