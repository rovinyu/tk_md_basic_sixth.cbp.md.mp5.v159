/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.
*
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
*
* Copyright (c) 2006-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef  _CPBUF_H_
#define  _CPBUF_H_
/*****************************************************************************
*
* FILE NAME   :   cpbuf.h
*
* DESCRIPTION :   CP Buffer Manager
*
* HISTORY     :
*     See Log at end of file
*
*****************************************************************************/

#include "sysdefs.h"
#include "monapi.h"

/*#define SYS_CP_BUFFER_STATUS_DEBUG*/

#define SYS_CP_BUFFER_LOG_FILENAME_LENGTH  6

/* CPBUF: TODO (for debugging) */
/* #define SYS_DEBUG_PRINTF */
#ifdef SYS_DEBUG_FAULT_FILE_INFO
#define CpBufGet(size, CpBufType)  __CpBufGet( __MODULE__, __LINE__, size, CpBufType)
#define CpBufFree( cpPktPtr ) __CpBufFree( __MODULE__, __LINE__, cpPktPtr )
#define CpBufMerge( num, Buf, oList, lnList, mLen, cType) __CpBufMerge( __MODULE__, __LINE__, num, Buf, oList, lnList, mLen, cType)
#endif

#define CPBUF_ONEBUF		  0xFFFF

#define CPBUF_MARKER_SIZE     4
#define CPBUF_MARKER_VALUE    0xBABADADA


/* The actual buffer size includes the marker size and is rounded up to the nearest 4 */
#define CPBUF_SIZE_FWD			640		/* bytes */
#define CPBUF_ACTUAL_SIZE_FWD	((CPBUF_SIZE_FWD+CPBUF_MARKER_SIZE+3) & 0xFFFC)
#define CPBUF_SIZE_REV			252     /* bytes */
#define CPBUF_ACTUAL_SIZE_REV	((CPBUF_SIZE_REV+CPBUF_MARKER_SIZE+3) & 0xFFFC)
#define CPBUF_SIZE_SIG 			100		/* bytes */
#define CPBUF_ACTUAL_SIZE_SIG	((CPBUF_SIZE_SIG+CPBUF_MARKER_SIZE+3) & 0xFFFC)
#define CPBUF_SIZE_HDR			15		/* bytes */
#define CPBUF_ACTUAL_SIZE_HDR	((CPBUF_SIZE_HDR+CPBUF_MARKER_SIZE+3) & 0xFFFC)
#define CPBUF_SIZE_FWD1X	    48		/* bytes */
#define CPBUF_ACTUAL_SIZE_FWD1X	((CPBUF_SIZE_FWD1X+CPBUF_MARKER_SIZE+3) & 0xFFFC)






/* The CP Buffer Pool High/Low Mark */
#if 1//!defined (MTK_DEV_MEMORY_OPT)
#define CPBUF_LOW_WATERMARK_PERCENT	(0.20)
#define CPBUF_HI_WATERMARK_PERCENT	(0.40)
#else
#define CPBUF_LOW_WATERMARK_PERCENT	(0.20)
#define CPBUF_HI_WATERMARK_PERCENT	(0.35)
#endif

/* The Buffer Pool Memory Type */
#define CPBUF_IRAM  0
#define CPBUF_SRAM  1

/*----------------------------------------------------------------------------
 CP Buf Data Struct
----------------------------------------------------------------------------*/
/* Allocation Status of each CP Buf memory buffer */
typedef enum
{
   CPBUF_FREE = 0xFE, 	/* non-zero nor one to avoid unwanted matching of uninitialized data field. */
   CPBUF_BUSY = 0xB0

} CpBufStatusT;


/* Allocation Type of CP Buf memory buffer */
typedef enum
{
   CPBUF_FWD = 0,
   CPBUF_REV,
   CPBUF_SIGNALING_MSG,
   CPBUF_HEADER,
   CPBUF_FWD1X
} CpBufTypeT;

/* for Type of CP Buf SPY  */
typedef enum
{
  FWD_STAS = 0,
  FWD_IRAM_FREE,
  FWD_IRAM_BUSY,
  FWD_SRAM_FREE,
  FWD_SRAM_BUSY,

  REV_STAS,
  REV_IRAM_FREE,
  REV_IRAM_BUSY,
  REV_SRAM_FREE,
  REV_SRAM_BUSY,

  SIG_STAS,
  SIG_IRAM_FREE,
  SIG_IRAM_BUSY,
  SIG_SRAM_FREE,
  SIG_SRAM_BUSY,

  HDR_STAS,
  HDR_IRAM_FREE,
  HDR_IRAM_BUSY,
  HDR_SRAM_FREE,
  HDR_SRAM_BUSY,

  FWD1X_STAS,
  FWD1X_SRAM_FREE,
  FWD1X_SRAM_BUSY

} CpBufStatusTypeT;


/* CpBuffer    - This is the CPBUF header data structure for each corresponding
				 entry of CPBUF in the CPBUF_Pool[]. When a CP pkt is to be
				 stored, multiple CPBUFs may be allocated if the length of the
				 CP pkt exceeds the size of a CPBUF. These multiple CPBUFs
				 are linked together in a linked-list. */

typedef struct cpBuffer
{
   uint32 	*dataPtr;				/* Ptr to the CPBUF addr in CPBUF_Pool[] */
   uint16 	blockCount;				/* Number of consecutive blocks of free cpBuffers starts from the
                                                            current one pointed by dataPtr. Only valid if it is the very first block in a group. */
   uint16 	len;	    				       /* Length of data in bytes.   */
									/* Pkt head has total len of pkt, tail has remaining len */
									/* All intermediate buffers are fully filled, thus len is not updated */
									/* Note that for FWD, SIG and HDR buffers, this is the max sz, since
									   the actual data size is not known when the cpbuffer is allocated.
									   The user can then choose to update this len info later after allocation */
   struct	     cpBuffer *nextPtr;	/* Ptr to the next cpPktHdr in this CP Pkt */
   uint8	     refcount;			/* Number of applications that uses this buffer */
   CpBufStatusT status;			/* Free or Busy */
   CpBufTypeT	 type;			/* partition type. Need this info to Free cpbuf */
   uint8	     memType;		/* 0-IRAM or 1-SRAM */
#ifdef SYS_CP_BUFFER_STATUS_DEBUG
   uint32      Time32k;
   uint8        FileName[SYS_CP_BUFFER_LOG_FILENAME_LENGTH];
   uint16      LineNumber;
#endif
} CpBufferT;

#ifdef SYS_CP_BUFFER_STATUS_DEBUG
typedef PACKED_PREFIX struct
{
   uint32    Time32k;
   uint32 * dataPtr;				/* Ptr to the CPBUF addr in CPBUF_Pool[] */
   uint16 	  blockCount;
   uint16 	  len;	    				       /* Length of data in bytes.   */
   uint8	  refcount;			/* Number of applications that uses this buffer */
   uint8     type;			/* partition type. Need this info to Free cpbuf */
   uint8     FileName[SYS_CP_BUFFER_LOG_FILENAME_LENGTH];
   uint16    LineNumber;
} PACKED_POSTFIX  CpBufEtsLogT;
#endif




/* cpBufQ - This is a generic CpBufferT type Queue Linked-List */
typedef struct cpBufQ
{
   CpBufferT	*head;
   CpBufferT	*tail;
   uint16		count;			/* Total number of CpBufferT in this linked-list */
} CpBufQT;


typedef struct
{
   uint16   NumOfBusyIram;
   uint16   HiWaterMarkIram;
   uint16   NumOfBusySram;
   uint16   HiWaterMarkSram;
} CpBufDebugInfoT;

#ifdef __cplusplus
extern "C" {
#endif

/*----------------------------------------------------------------------------
 CP Pkt Functions
----------------------------------------------------------------------------*/
extern void CpBufMgrInit ( void );

#ifdef SYS_DEBUG_FAULT_FILE_INFO
extern CpBufferT *__CpBufGet(const char *ModuleName, unsigned line, uint16 size, CpBufTypeT CpBufType );
#else
extern CpBufferT *CpBufGet(uint16 size, CpBufTypeT CpBufType );
#endif

#ifdef SYS_DEBUG_FAULT_FILE_INFO
extern CpBufferT *__CpBufMerge
( const char *ModuleName,
  unsigned line,
  uint16 	  numCpBuffers,
  CpBufferT   *cpBufList[],
  uint16	  *offsetList,
  uint16	  *lenList,
  uint16	  *mergedLen,
  CpBufTypeT cpBufType
);
#else
extern CpBufferT *CpBufMerge
( uint16 	  numCpBuffers,
  CpBufferT   *cpBufList[],
  uint16	  *offsetList,
  uint16	  *lenList,
  uint16	  *mergedLen,
  CpBufTypeT cpBufType
);
#endif

extern void  CpBufCopy( CpBufferT *cpPktPtr );
#ifdef SYS_DEBUG_FAULT_FILE_INFO
extern void  __CpBufFree( const char *ModuleName, unsigned line, CpBufferT *cpPktPtr );
#else
extern void  CpBufFree( CpBufferT *cpPktPtr );
#endif
extern void  CpBufStats( CpBufStatusTypeT cpBufType );

extern void  cpBufStatusMsg ( MonCpBufferStatusMsgT *RxMsgP );

extern bool  CpBufChkFree(uint16 size, CpBufTypeT CpBufType );
extern bool  CpBufFwdFlowCtrlOn(void);
extern void cpBufLowWaterMarkProcRev4Enc0 (void);

/*Only use as debug*/
extern void SysCpBufSendEtsLog(uint8* LogPtr, bool CpCrash);

/*----------------------------------------------------------------------------
 Global Data
----------------------------------------------------------------------------*/


#ifdef  __cplusplus
}
#endif  /* __cplusplus */


/*****************************************************************************
* End of File
*****************************************************************************/
#endif
