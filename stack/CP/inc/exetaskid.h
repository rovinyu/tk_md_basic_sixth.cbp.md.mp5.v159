/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.
*
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
*
* Copyright (c) 1998-2011 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
/*****************************************************************************

  FILE NAME:  exetaskid.h

  DESCRIPTION:

    This file contains the defenitions of all the task id's.

*****************************************************************************/

#ifndef MSG_ID_MISMATCH_DETECT

// regular msg id's enum
#define TASKID_SET(name, val) name = val
#define TASKID_NEXT(name) name
#define TASK_IDS typedef enum
#define TASK_IDS_NAME ExeTaskIdT

extern const uint32 ExeValidTaskIdList[];
extern uint32 ExeValidTaskIdListSizeOf(void);

#else

#ifdef  HWD_MSG_ID_MISMATCH_DETECT
#define  EXEValidTaskIdList HwdExeValidTaskIdList
#else
#define  EXEValidTaskIdList ExeValidTaskIdList
#endif

#undef TASKID_SET
#undef TASKID_NEXT
#undef TASK_IDS
#undef TASK_IDS_NAME

#define TASKID_SET(name, val) name
#define TASKID_NEXT(name) name
#define TASK_IDS const uint32 EXEValidTaskIdList[] =
#define TASK_IDS_NAME
#endif

/*---------------------------------------------------------------------
* The enumerated type ExeTaskIdT is used by application tasks
* when they want to call a scheduler service and they need to
* know the task id of a particular task. This enumerated type
* also contains the number of overall tasks that the scheduler
* will deal with. The order of task ids is important and must
* match the order of task control blocks defined in ExeTaskCbT.
* New task ids should be added to the end of the list before
* the EXE_NUM_TASKS entry.
*---------------------------------------------------------------------*/

TASK_IDS
{
#ifdef MTK_DEV_SSDVT
    TASKID_SET(EXE_SSDVT_ID, 0),
    TASKID_NEXT(EXE_NUM_TASKS),
#endif
    TASKID_SET(EXE_IPC_ID, 0),
    TASKID_SET(EXE_IOP_ID, 1),
    TASKID_SET(EXE_ETS_ID, 2),
    TASKID_SET(EXE_L1D_MDM_ID, 3),
    TASKID_SET(EXE_MON_ID, 4),
    TASKID_SET(EXE_MON_IDL_ID, 5),
    TASKID_SET(EXE_HWD_ID, 6),
    TASKID_SET(EXE_DBM_ID, 7),
    TASKID_SET(EXE_TST_ID, 8),
    TASKID_SET(EXE_PSW_ID, 10),
    TASKID_SET(EXE_LMD_ID, 11),
    TASKID_SET(EXE_LMD_S_ID, 12),
    TASKID_SET(EXE_VAL_ID, 13),
    TASKID_SET(EXE_RLP_ID, 15),
    TASKID_SET(EXE_HLP_ID, 16),
    TASKID_SET(EXE_AVI_ID, 17),
    TASKID_SET(EXE_PSW_S_ID, 18),
    TASKID_SET(EXE_UIM_ID, 19),
    TASKID_SET(EXE_UI_ID, 20),
    TASKID_SET(EXE_FSM_ID, 21),
    TASKID_SET(EXE_HSC_ID, 23),
    TASKID_SET(EXE_SLC_ID, 24),
    TASKID_SET(EXE_CLC_ID, 25),
    TASKID_SET(EXE_RMC_ID, 26),
    TASKID_SET(EXE_FCP_ID, 27),
    TASKID_SET(EXE_RCP_ID, 28),
    TASKID_SET(EXE_CSS_ID, 29),
    TASKID_SET(EXE_LSM_ID, 30),
    TASKID_SET(EXE_LEC_ID, 32),
    TASKID_SET(EXE_SEC_ID, 33),
    TASKID_SET(EXE_PST_ID, 35),
    TASKID_SET(EXE_UI_S_ID, 36),
    TASKID_SET(EXE_MEDIA_ID, 37),
    TASKID_SET(EXE_MMD_ID, 38),
    TASKID_SET(EXE_EEP_ID, 39),
    TASKID_SET(EXE_L1CT_ID, 40),
    TASKID_SET(EXE_ATM_ID, 41),
    TASKID_SET(EXE_NAV_ID, 42),
    TASKID_SET(EXE_USRPORT_ID, 43),
    TASKID_SET(EXE_CONSOLE_ID, 44),
    TASKID_SET(EXE_FAKELEC_ID, 45),
    TASKID_SET(EXE_TLS_ID, 46),
    TASKID_SET(EXE_AVP_ID, 47),
    TASKID_SET(EXE_VDIS_ID, 53),
    TASKID_SET(EXE_VCODEC_ID, 54),
    TASKID_SET(EXE_VDM_ID, 55), 
#ifdef SYS_OPTION_IRAT_MMC
    TASKID_SET(EXE_MMC_ID, 56), 
#else
    TASKID_SET(EXE_AVAILABLE1_ID, 56), /* Available: Add a new task id here */
#endif    
#ifdef SYS_OPTION_DUAL_CARDS
   TASKID_SET(EXE_UIM2_ID      , 58),
#else
   TASKID_SET(UNUSED_UIM2_ID   , 58),
#endif

    /************************************/
    /*** Add new task ids here        ***/
    /*** Highest task id should be 70 ***/
    /***      (or EXE_DUMMY_TASK - 1) ***/
    /************************************/

#ifdef MTK_DEV_SLT
    TASKID_SET(EXE_SLT_ID   , 59),
#else
    TASKID_SET(UNUSED_SlT_ID   , 59),
#endif

#if defined(MTK_DEV_C2K_SRLTE_L1) && defined(MTK_PLT_ON_PC) && defined(MTK_PLT_ON_PC_IT)
    TASKID_SET(EXE_STUB_ID, 60),
#endif

#ifndef MTK_DEV_SSDVT
    TASKID_NEXT(EXE_NUM_TASKS),
#endif
    /* Dummy task is an explicit way of indicating when a task ID is
    * not valid or should not be used.
    * It is set to the max application task id value, one less than
    * the LISR thread IDs.
    */
   TASKID_SET(EXE_DUMMY_TASK, 0x47) /* 71 */
} TASK_IDS_NAME;
/* Remember to also update the "CP Task Id" enum in mon_cp_msg.txt and
 * "CP Thread Ids" enum in mon_spy.txt */

