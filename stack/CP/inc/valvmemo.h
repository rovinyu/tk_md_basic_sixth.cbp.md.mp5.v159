/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2005-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef __VAL_VOICE_MEMO_H__
#define __VAL_VOICE_MEMO_H__
/*****************************************************************************

  FILE NAME: valvmemo.h

  DESCRIPTION:

    This file contains the voice memo definitions and prototypes

*****************************************************************************/

 
/*-----------------------------------------------------------------
 * VMEMO Defines and Type Definitions
 *----------------------------------------------------------------*/

#define VAL_VMEM_MAX_FILENAME_LEN  130

typedef enum 
{
   /* Recording voice memo while not in a voice call. For recording memos, the SPKR
    * path is turned ON so that the "white noise" the user hears provides feedback that
    * voice is being recorded.
    */
   VAL_VMEMO_REC_TYPE_OFFLINE_EVRC,       /* SO3  */
   VAL_VMEMO_REC_TYPE_OFFLINE_EVRCB,      /* SO68 */
   VAL_VMEMO_REC_TYPE_OFFLINE_EVRCNW,     /* SO73 */
   VAL_VMEMO_REC_TYPE_OFFLINE_QCELP13K,   /* SO17 */
   VAL_VMEMO_REC_TYPE_OFFLINE_AMR,
   VAL_VMEMO_REC_TYPE_OFFLINE_GSMFR,
   VAL_VMEMO_REC_TYPE_OFFLINE_GSMEFR,

   /* Recording movie while not in a call; this is the same as voice memo 
    * recording (above) except that the speaker path remains OFF. 
    * Extra service options can be added if needed.
    */
   VAL_VMEMO_REC_TYPE_MOVIE_EVRC,         /* SO3  */
   VAL_VMEMO_REC_TYPE_MOVIE_QCELP13K,     /* SO17 */
   VAL_VMEMO_REC_TYPE_MOVIE_AMR,
   VAL_VMEMO_REC_TYPE_MOVIE_PCM,

   /* Recording vocoder packets while in a voice call */
   VAL_VMEMO_REC_TYPE_ONLINE_FWD,     
   VAL_VMEMO_REC_TYPE_ONLINE_REV,
   VAL_VMEMO_REC_TYPE_ONLINE_BOTH,

   /* Recording raw PCM either in or not in a voice call; 
    * if not in a voice call, use MIC path. 
    */
   VAL_VMEMO_REC_TYPE_MIC_PATH_RAW_PCM,
   VAL_VMEMO_REC_TYPE_SPKR_PATH_RAW_PCM,
   VAL_VMEMO_REC_TYPE_MIXED_RAW_PCM       /* Fwd + Rev path for voice call */
}ValVmemoRecTypeT;

typedef enum
{
   VAL_VMEMO_FORMAT_RFC_VOICE_PACKETS,    /* RFC Payload format (EVRC, EVRC-B, AMR, QCP) */
   VAL_VMEMO_FORMAT_RAW_EVRC_PACKETS,     /* EVRC Packets, no header      */
   VAL_VMEMO_FORMAT_RAW_QCELP13K_PACKETS, /* QCELP-13K packets, no header */
   VAL_VMEMO_FORMAT_RAW_PCM_SAMPLES,      /* Raw PCM samples, 160 words   */
   VAL_VMEMO_FORMAT_RAW_EVRCB_PACKETS,    /* EVRC-B packets, no header    */
   VAL_VMEMO_FORMAT_RAW_AMR_PACKETS,      /* AMR packets, no header       */
   VAL_VMEMO_FORMAT_RAW_PCM_AND_SPKR_MIX, /* Raw PCM mixed with SPKR path */
   VAL_VMEMO_FORMAT_RAW_PCM_AND_MIC_MIX,  /* Raw PCM mixed with MIC path  */
   VAL_VMEMO_FORMAT_RAW_EVRCNW_PACKETS,   /* EVRC-NW Packets, no header   */
   VAL_VMEMO_FORMAT_RAW_GSMFR_PACKETS,    /* GSM-FR Packets, no header    */
   VAL_VMEMO_FORMAT_RAW_GSMEFR_PACKETS    /* GSM-EFR Packets, no header   */
} ValVmemoDataFormatT;

typedef enum
{
   VAL_VMEMO_RATE_FULL = 4,
   VAL_VMEMO_RATE_HALF = 3
} ValVmemoMaxRateTypeT;

typedef enum
{
   VAL_VMEMO_SPCH_EIGHTH_RATE  = 1,
   VAL_VMEMO_SPCH_QUARTER_RATE = 2,
   VAL_VMEMO_SPCH_HALF_RATE    = 3,
   VAL_VMEMO_SPCH_FULL_RATE    = 4
} ValVmemoSpchRateTypeT;

/* Data for VAL_VMEMO_RECORD_END_EVENT and VAL_VMEMO_PLAYBACK_END_EVENT */
typedef struct
{
   ValAppStatusT  ReturnCode;
   uint32         NumBytes;    /* Actual # of bytes recorded or played back */
} ValVmemoEventMsgT;

/* Data for VAL_VMEMO_VOICE_DATA_EVENT */
typedef struct
{
    ValVmemoRecTypeT      RecordType;
    uint32                NumDataBytes;
    ValVmemoSpchRateTypeT SpchRate;
    uint8                *VoiceDataP; 
} ValVmemoDataEventMsgT;

/* CALLBACK function prototype used to obtain data from the user during real-time playback */
typedef void (*ValVmemoGetPlayDataFuncT) (uint16  MaxDataBytes,
                                          uint16 *NumVoiceFramesP,
                                          uint16 *NumDataBytesP,
                                          uint8  *DataBuffP);

/*-----------------------------------------------------------------
 * The following definitions are used by LMD VMemo
 *----------------------------------------------------------------*/
/* For VAL_SPEECH_PLAY_COMPLETE_MSG, VAL_VREC_PLAYBACK_CANCEL_MSG and 
 * VAL_VREC_PLAYBACK_COMPLETE_MSG messages.
 */
typedef PACKED_PREFIX struct
{
   ValAppStatusT            ReasonCode;
} PACKED_POSTFIX  ValVmemVrecPlayCompleteMsgT;

/* For VAL_VMEMO_REC_STOP_MSG message */
typedef PACKED_PREFIX struct
{
   ValAppStatusT            ReasonCode;
} PACKED_POSTFIX  ValVmemVrecRecordCancelMsgT;

/*-----------------------------------------------------------------
 * The following message definitions are used by ETS for testing
 *----------------------------------------------------------------*/
typedef PACKED_PREFIX struct
{
  ValVmemoRecTypeT          RecordType;
  bool                      RfcFormat;
  IpcSpchSrvcOptRateT       MaxRate;
  IpcSpchSrvcOptRateT       MinRate;
  char                      FileName [VAL_VMEM_MAX_FILENAME_LEN];
  uint8                    *DataP;
  uint32                    DataBytes;
} PACKED_POSTFIX  ValVmemoRecStartMsgT;

typedef PACKED_PREFIX struct
{
  ValVmemoDataFormatT       DataFormat;  /* Voice packets or PCM samples */
  char                      FileName [VAL_VMEM_MAX_FILENAME_LEN];
  uint8                    *DataP;
  uint32                    DataBytes;
  uint32                    BaseByteOffset;
  uint32                    StartTimeOffset;
} PACKED_POSTFIX  ValVmemoPlayStartMsgT;

typedef PACKED_PREFIX struct
{
  char                      FileName [VAL_VMEM_MAX_FILENAME_LEN];
} PACKED_POSTFIX  ValVmemoTestRecRawPcmMsgT;
 
 
/*-----------------------------------------------------------------
 * VMEMO global functions 
 *----------------------------------------------------------------*/
#ifdef __cplusplus
    extern "C" {
#endif 

//don't modify following Function, brew has referred to it
/*****************************************************************************

  FUNCTION NAME: ValVmemoRegister
  
  DESCRIPTION:   This routine is used to register a callback to notify the 
                 caller when VMemo has completed.
                  
  PARAMETERS:    EventFuncP  - the function pointer
  
  RETURNS:       Register Identifier

*****************************************************************************/
RegIdT ValVmemoRegister (ValEventFunc EventFuncP);

/*****************************************************************************
 
  FUNCTION NAME: ValVmemoUnregister
  
  DESCRIPTION:   This routine unregisters a previously registered callback 
  
  PARAMETERS:    RegId - register identifier of the previously registered callback
  
  RETURNS:       None.
 
*****************************************************************************/
void ValVmemoUnregister (RegIdT RegId);

/*****************************************************************************

  FUNCTION NAME: ValVmemoVoiceDataRegister
  
  DESCRIPTION:   This routine is used to register a callback to send voice data to the caller
                 during a Voice Memo RECORD session. Voice data is sent every 20 msec. The data format
                 is either EVRC, QCELP-13K or PCM samples depending on the format being recorded.
                  
  PARAMETERS:    EventFuncP  - the function pointer
  
  RETURNS:       Register Identifier

*****************************************************************************/
RegIdT ValVmemoVoiceDataRegister (ValEventFunc EventFuncP);

/*****************************************************************************
 
  FUNCTION NAME: ValVmemoVoiceDataUnregister
  
  DESCRIPTION:   This routine unregisters a previously registered VMemo Voice Data callback 
  
  PARAMETERS:    RegId - register identifier of the previously registered callback
  
  RETURNS:       None.
 
*****************************************************************************/
void ValVmemoVoiceDataUnregister (RegIdT RegId); 

/*****************************************************************************

  FUNCTION NAME:   ValVmemoPlayStartWithOffsets

  DESCRIPTION:     Processes a Speech Playback request starting at a particular offset.
                                  
  PARAMETERS:      DataFormat      - Voice packets (QCP format) or raw PCM Samples 
                   FileNameP       - Ptr to filename of voice file; used only if DataP is NULL
                   DataP           - Pointer to speech buffer; if NULL, then FileNameP is used
                   DataBytes       - Number of speech data bytes in DataP buffer
                   GetPlayDataCB   - Callback function to get data from user during
                                     real-time playback
                   BaseByteOffset  - Offset from start of file where vocoder header starts (this
                                     will be same as where data starts when no header is present)
                   StartTimeOffset - Seconds after start of data from where to start playing
                                   
  *** NOTE: The GetPlayDataCB parameter is used only during REAL-TIME playback, and
            should be set to NULL otherwise. 
            
            For real-time playback, the FileNameP AND DataP must both be NULL and the 
            GetPlayDataCB callback function must be defined. The GetPlayDataCB callback 
            function is called by VAL VMemo to obtain data from the user when needed.
            During real-time playback, the user's look-ahead buffer should be at least
            3 voice frames deep, which means that the callback will be executed
            every 3 20msec frames, or every 60 msecs.

  RETURNED VALUES: VAL_APP_OK if success, VAL APP error if failure

*****************************************************************************/
ValAppStatusT ValVmemoPlayStartWithOffsets (ValVmemoDataFormatT DataFormat, 
                                            char               *FileNameP, 
                                            uint8              *DataP, 
                                            uint32              DataBytes,
                                            ValVmemoGetPlayDataFuncT GetPlayDataCB,
                                            uint32              BaseByteOffset,
                                            uint32              StartTimeOffset);

/*****************************************************************************

  FUNCTION NAME:   ValVmemoPlayStart

  DESCRIPTION:     Processes a Speech Playback request.

  PARAMETERS:      DataFormat    - Voice packets (QCP format) or raw PCM Samples 
                   FileNameP     - Ptr to filename of voice file; used only if DataP is NULL
                   DataP         - Pointer to speech buffer; if NULL, then FileNameP is used
                   DataBytes     - Number of speech data bytes in DataP buffer
                   GetPlayDataCB - Callback function to get data from user during
                                   real-time playback
                                   
  *** NOTE: The GetPlayDataCB parameter is used only during REAL-TIME playback, and
            should be set to NULL otherwise. 
            
            For real-time playback, the FileNameP AND DataP must both be NULL and the 
            GetPlayDataCB callback function must be defined. The GetPlayDataCB callback 
            function is called by VAL VMemo to obtain data from the user when needed.
            During real-time playback, the user's look-ahead buffer should be at least
            3 voice frames deep, which means that the callback will be executed
            every 3 20msec frames, or every 60 msecs.

  RETURNED VALUES: VAL_APP_OK if success, VAL APP error if failure

*****************************************************************************/
ValAppStatusT ValVmemoPlayStart (ValVmemoDataFormatT DataFormat, 
                                 char *FileNameP, 
                                 uint8 *DataP, 
                                 uint32 DataBytes,
                                 ValVmemoGetPlayDataFuncT GetPlayDataCB);

//don't modify following Function, brew has referred to it
/*****************************************************************************

  FUNCTION NAME:   ValVmemoRecStart

  DESCRIPTION:     Processes a Speech Record request.

  PARAMETERS:      RecordType   - Offline EVRC/QCELP-13K or Online FWD/REV
                   RfcFormat    - TRUE if QCP Format file is required, FALSE for raw voice data
                   MaxRate      - Max encode rate (Full or Half rate)
                   FileNameP    - Ptr to filename to store voice; used only if DataP is NULL
                   DataP        - Ptr to buffer to store voice; if NULL then use FileNameP
                   MaxDataBytes - Size in bytes of DataP buffer

  RETURNED VALUES: VAL_APP_OK if success, error if failure

*****************************************************************************/
ValAppStatusT ValVmemoRecStart (ValVmemoRecTypeT     RecordType,
                                bool                 RfcFormat,
                                ValVmemoMaxRateTypeT MaxRate,
                                char                *FileNameP,
                                uint8               *DataP, 
                                uint32               MaxDataBytes);

/*****************************************************************************

  FUNCTION NAME: ValVmemoTestRecRawPCM

  DESCRIPTION:   This is a TEST function that initiates recording of
                 Raw PCM voice data.
                 
                 Currently FSM cannot keep up with the 320 bytes/20 msec data rate,
                 so this function should not be used during operational mode.
                 Once the problems are resolved, the ability to record raw PCM will
                 be added to the existing ValVmemoRecStart function interface and this
                 function will be removed.

  PARAMETERS:    FileNameP - Pointer to file name string where data is to be stored

  RETURNS:       VAL_APP_OK if success, error if failure

*****************************************************************************/
ValAppStatusT ValVmemoTestRecRawPCM (char *FileNameP);

/*****************************************************************************

  FUNCTION NAME: ValVmemoTestRecSbcEnc

  DESCRIPTION:   This is a TEST function that initiates recording of
                 SBC Encoder data from the DSPv to a file. This is
                 the only way to test SBC Encoder when there is no
                 bluetooth functionality on the CP. 
                 
                 The recording must start after voice/music is active to 
                 avoid having VAL report conflicts.
                 
                 The SBC Encode data can be 8K voice, MP3, MIDI or any other
                 music at any sampling rate.

  PARAMETERS:    FileNameP - Pointer to file name string where data is to be stored

  RETURNS:       VAL_APP_OK if success, error if failure

*****************************************************************************/
ValAppStatusT ValVmemoTestRecSbcEnc (char *FileNameP);

/*****************************************************************************

  FUNCTION NAME:   ValVmemoPlayPause

  DESCRIPTION:     Processes a request to pause the current speech playback.

  PARAMETERS:      None

  RETURNED VALUES: None

*****************************************************************************/
void ValVmemoPlayPause (void);

/*****************************************************************************

  FUNCTION NAME:   ValVmemoPlayResume

  DESCRIPTION:     Processes a request to resume a speech playback that 
                   is currently paused.

  PARAMETERS:      None

  RETURNED VALUES: None

*****************************************************************************/
void ValVmemoPlayResume (void);

//don't modify following Function, brew has referred to it
/*****************************************************************************

  FUNCTION NAME:   ValVmemoStop

  DESCRIPTION:     Aborts an in-progress voice memo activity. It has no effect
                   if no voice memo activity is currently in progress.
                   If speech playback is currently paused, the pause is cancelled.

  PARAMETERS:      None

  RETURNED VALUES: None

*****************************************************************************/
void ValVmemoStop (void);

/*****************************************************************************

  FUNCTION NAME:   ValVMemoIsInProgress

  DESCRIPTION:     Returns TRUE if any voice memo activity is in progress,
                   FALSE otherwise.

  PARAMETERS:      None

  RETURNED VALUES: TRUE/FALSE

*****************************************************************************/
bool ValVmemoIsInProgress(void);

/*****************************************************************************

  FUNCTION NAME:   ValVmemoIsInPause

  DESCRIPTION:     Returns TRUE if any voice memo playback is in pause,
                   FALSE otherwise.

  PARAMETERS:      None

  RETURNED VALUES: TRUE/FALSE

*****************************************************************************/
bool ValVmemoIsInPause(void);

//don't modify following Function, brew has referred to it
/*****************************************************************************

  FUNCTION NAME:   ValVmemoPause

  DESCRIPTION:     Processes a request to pause the current recording.

  PARAMETERS:      None

  RETURNED VALUES: None

*****************************************************************************/
void ValVmemoRecPause( void );

//don't modify following Function, brew has referred to it
/*****************************************************************************

  FUNCTION NAME:   ValVmemoResume

  DESCRIPTION:     Processes a request to resume a recording that is currently paused.


  PARAMETERS:      None

  RETURNED VALUES: None

*****************************************************************************/
void ValVmemoRecResume( void );

/*****************************************************************************

  FUNCTION NAME:   ValVmemoGetPlayTime

  DESCRIPTION:     Returns number of milliseconds of play time for an RFC formatted
                   voice file (AMR, WAVE or QCELP).

  PARAMETERS:    DataP - Ptr to buffer to store voice; if NULL then use FileHandle
                 FileHandle - file handler
                 DataBytes - Size in bytes of DataP buffer or file

  RETURNED VALUES: Play time in milli-seconds

*****************************************************************************/
uint32 ValVmemoGetPlayTime (uint8 *DataP, ValFsiHandleT FileHandle, uint32 DataBytes);
/*****************************************************************************
  FUNCTION NAME:   ValVmemoElapseTimeGet

  DESCRIPTION:     Returns number of milliseconds of elapsed time.

  PARAMETERS:      MSecP - pointer to elapsed time in msec (OUTPUT)

  RETURNED VALUES: VAL_APP_OK, VAL_APP_INVAL_PLAYBACK_PARMS or VAL_APP_VMEMO_WRONG_STATE_ERR.

*****************************************************************************/
ValAppStatusT ValVmemoElapseTimeGet (uint32 *MSecP);


//don't modify following Function, brew has referred to it
/*****************************************************************************

  FUNCTION NAME:   ValVmemoTotalTimeGet

  DESCRIPTION:   get the duration time of a record file

  PARAMETERS:   DataFormat - Voice Packets (QCP format) or PCM Samples
                 FileNameP - file name pointer
                 MilliSecond - out the time in ms

  RETURNS:   VAL_APP_OK if success, error if failure

*****************************************************************************/
ValAppStatusT ValVmemoTotalTimeGet (ValVmemoDataFormatT DataFormat,  
                                char *FileNameP,
                                uint32 *MilliSecond);

/*****************************************************************************

  FUNCTION NAME: ValVmemoRecordingTimeGet

  DESCRIPTION:   get how long has recorded. this function is requested from UI
                            for get a accurate recording time

  PARAMETERS:    MilliSecondP - out the time in ms

  RETURNS:       VAL_APP_OK if success, error if failure

*****************************************************************************/
ValAppStatusT ValVmemoRecordingTimeGet(uint32 *MilliSecondP);

/*****************************************************************************

  FUNCTION NAME:   ValVmemoSetPlayPosition

  DESCRIPTION:     Sets the active playback to the indicated file position,
                   in seconds since start of file.

  PARAMETERS:      Seconds - Seconds from start of file to continue playback

  RETURNED VALUES: VAL_APP_OK, VAL_APP_WRONG_PLAY_FORMAT or VAL_APP_VMEMO_WRONG_STATE_ERR.

*****************************************************************************/
ValAppStatusT ValVmemoSetPlayPosition (uint32 Seconds);

/*****************************************************************************

  FUNCTION NAME:   ValVmemoFastFwd

  DESCRIPTION:     Move the playback position forward the indicated number of seconds.

  PARAMETERS:      Seconds - Seconds to advance play position

  RETURNED VALUES: VAL_APP_OK, VAL_APP_WRONG_PLAY_FORMAT or VAL_APP_VMEMO_WRONG_STATE_ERR.

*****************************************************************************/
ValAppStatusT ValVmemoFastFwd (uint32 Seconds);

/*****************************************************************************

  FUNCTION NAME:   ValVmemoRewind

  DESCRIPTION:     Move the playback position backward the indicated number of seconds.

  PARAMETERS:      Seconds - Seconds to rewind play position

  RETURNED VALUES: VAL_APP_OK, VAL_APP_WRONG_PLAY_FORMAT or VAL_APP_VMEMO_WRONG_STATE_ERR.

*****************************************************************************/
ValAppStatusT ValVmemoRewind (uint32 Seconds);


/*-----------------------------------------------------------------
 * Functions used by Avp Task only
 *----------------------------------------------------------------*/
/*****************************************************************************
  FUNCTION NAME:  ValVmemoCallback

  DESCRIPTION:    Execute VMEMO registered callback function.

  PARAMETERS:     EventId  - VAL_VMEMO_RECORD_END_EVENT or VAL_VMEMO_PLAYBACK_END_EVENT
                  VoiceLen - Number of bytes recorded or played back
                  ErrStatus - VAL_APP_OK or error

  RET VALUES:     None
*****************************************************************************/
void ValVmemoCallback (uint32 EventId, uint32 VoiceLen, ValAppStatusT ErrStatus);

/*****************************************************************************
  FUNCTION NAME:  ValVmemoDataCallback

  DESCRIPTION:    Execute VMEMO Voice Data registered callback function.

  PARAMETERS:     VoiceDataP   - Voice Data to send to registered user
                  NumDataBytes - Length of voice data in bytes

  RET VALUES:     None
*****************************************************************************/
void ValVmemoDataCallback (uint8 *VoiceDataP, uint32 NumDataBytes);

/*****************************************************************************
  FUNCTION NAME:  ValVmemoGetMaxRecFileSize

  DESCRIPTION:    Calculates the maximum file size that can be recorded.
                  Max size is calculated as a percentage of the device size.
                  
                  This is necessary because the writes become too slow as
                  the device becomes full, and we fall behind.

  PARAMETERS:     FileNameP  - Name of vmemo record file

  RET VALUES:     max file size, or 0 if no room on device
*****************************************************************************/
uint32 ValVmemoGetMaxRecFileSize (char *FileNameP);


#ifdef __cplusplus
    }
#endif /* __cplusplus */

#endif
/**Log information: \main\CBP7FeaturePhone\CBP7FeaturePhone_nicholaszhao_href17384\1 2011-07-08 06:02:34 GMT nicholaszhao
** HREF#17384**/
/**Log information: \main\CBP7FeaturePhone\8 2011-07-12 09:42:23 GMT marszhang
** HREF#17384**/
/**Log information: \main\CBP7FeaturePhone\CBP7FeaturePhone_ygwu_href17989\1 2011-09-23 08:34:31 GMT ygwu
** HREF#17989|ture off amp when vmemo playback is in pause state to eliminate white noise from loud speaker**/
/**Log information: \main\CBP7FeaturePhone\9 2011-09-23 08:50:35 GMT ygwu
** HREF#17989|ture off amp when vmemo playback is in pause state to eliminate white noise from loud speaker**/
