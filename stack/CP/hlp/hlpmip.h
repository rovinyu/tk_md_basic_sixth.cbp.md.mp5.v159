/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.
*
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
*
* Copyright (c) 2006-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef _HLPMIP_H_
#define _HLPMIP_H_
/*****************************************************************************
*
* FILE NAME   : hlpmip.h
*
* DESCRIPTION :
*
*     This include file provides  type declarations and constants for Mobile IP
*
* HISTORY     :
*     See Log at end of file.
*
*****************************************************************************/

/*----------------------------------------------------------------------------
 Include Files
----------------------------------------------------------------------------*/
#include "pswnam.h"

/*----------------------------------------------------------------------------
 Global Defines and Macros
----------------------------------------------------------------------------*/
#define AAA_AUTH_MAX_SIZE            8
#define RSA_PUBKEY_EXPONENT_MAX_SIZE 128
#define RSA_PUBKEY_MODULUS_MAX_SIZE  128
#define DMU_ENCRYPTED_DATA_MAX_SIZE  128

#define MAX_RSA_PUBLIC_KEY_SUPPORTED 2

typedef PACKED_PREFIX struct {
    uint8 MN_AAAH_Key[MN_PASSWD_MAX_SIZE];
    uint8 MN_HA_Key[MN_PASSWD_MAX_SIZE];
    uint8 CHAP_Key[MN_PASSWD_MAX_SIZE];
    uint8 MN_Authenticator[MN_AUTH_MAX_SIZE];
    uint8 AAA_Authenticator[AAA_AUTH_MAX_SIZE];
} PACKED_POSTFIX  HlpDMUKeyDataT;

typedef NV_PACKED_PREFIX struct {
    uint8 nPKOID;
    uint8 nPKOI;
    uint8 nPK_Expansion;
    uint8 nATVAndDMUV;
} NV_PACKED_POSTFIX  HlpRSAPublicKeyInfoHdrT;

typedef NV_PACKED_PREFIX struct {
    HlpRSAPublicKeyInfoHdrT hdrRSAPublicKeyInfo;
    uint8 RSAPubKeyExponent[RSA_PUBKEY_EXPONENT_MAX_SIZE];
    uint8 RSAPubKeyModulus[RSA_PUBKEY_MODULUS_MAX_SIZE];
} NV_PACKED_POSTFIX  HlpRSAPublicKeyInfoT;

typedef PACKED_PREFIX struct {
    uint8 MN_AAA_PASSWORD[HLP_MN_PASSWD_MAX_SIZE];
    uint8 MN_HA_PASSWORD[HLP_MN_PASSWD_MAX_SIZE];
    uint8 MN_SIP_PASSWORD[HLP_MN_PASSWD_MAX_SIZE];
    uint8 MN_AN_PASSWORD[HLP_MN_PASSWD_MAX_SIZE];
} PACKED_POSTFIX HlpDmuSecureDataMsgT;

typedef enum
{
   DMU_RSA_PUBLICKEY_1 = 0,
   DMU_RSA_PUBLICKEY_2
} DmuRsaPublickKeyNumber;

typedef enum
{
   RFC2002,
   RFC2002bis,
   BypassHA
}HAAuthNumber;

typedef enum
{
   NONE_AUTH_ALGO,
   MD5_AUTH_ALGO
}AuthAlgoNumber;

typedef enum
{
   MIP_WFR_REREG_WINDOW,
   MIP_REREG_WINDOW,
   MIP_REREG_RRQ_SENT_WINDOW,
   MIP_REREG_LIFETIME_EXPIRED,
   MIP_DEREG_ALREADY
} ReRegStateT;

#define SECURE_DATA_SIP_PWD_BIT     (1<<0)
#define SECURE_DATA_AN_PWD_BIT      (1<<1)
#define SECURE_PROFILE_BIT          (1<<2)
#ifdef CBP7_EHRPD
#define SECURE_DATA_AKA_PWD_BIT     (1<<3)
#endif

typedef enum
{
   SECURE_DATA_SIP_PWD_SET = SECURE_DATA_SIP_PWD_BIT,
   SECURE_DATA_AN_PWD_SET = SECURE_DATA_AN_PWD_BIT,
#ifdef CBP7_EHRPD
   SECURE_DATA_AKA_PWD_SET = SECURE_DATA_AKA_PWD_BIT,
#endif
   SECURE_SET_BOTH = RSA_PUBLIC_KEY_BIT|SECURE_DATA_SIP_PWD_BIT|SECURE_DATA_AN_PWD_BIT,
   SECURE_PROFILE_SET = SECURE_PROFILE_BIT,
#ifdef CBP7_EHRPD
   SET_ALL = SECURE_DATA_SIP_PWD_BIT|SECURE_DATA_AN_PWD_BIT|SECURE_PROFILE_BIT|SECURE_DATA_AKA_PWD_BIT
#else
   SET_ALL = SECURE_DATA_SIP_PWD_BIT|SECURE_DATA_AN_PWD_BIT|SECURE_PROFILE_BIT
#endif
}SecureDataSetE;
/*----------------------------------------------------------------------------
 Externs Declarations
----------------------------------------------------------------------------*/
extern void HlpMipInit(void);
extern void HlpMipFallbackSipSet(bool bEnable);
extern bool HlpMipFallbackSipGet(void);
extern void HlpMipFallbackSipActive(void);

extern void HlpMipRrpMsg(void *MsgDataP);
extern void HlpMipAgentAdvMsg(void *MsgDataP);
extern void HlpMipUmPppStatusMsg(void *MsgDataP);
extern void HlpMipTimerCallBackMsg(void *MsgDataP);
extern void HlpMipNvmMiscData(void* MsgDataP);
extern void HlpGetMIPData(void);
extern void HlpMipNvmRRAUpdate(uint16 Rra);
extern void HlpMipNvmNumRegRetriesUpdate(uint8 Num);
extern void HlpMipNvmDeRegRetriesUpdate(uint8 Num);
extern void HlpMipNvmReRegOnlyIfTrafficUpdate(bool ReRegOnlyIfTraffic);
extern void HlpMipNvmMipNaiEnabledUpdate(bool NaiEnabled);
extern void HlpMipNvmRegTimeoutUpdate(uint8 Timeout);
extern void HlpMipNvmActivedProfileDataUpdate(HlpHspdSegData * MsgDataP);
extern void HlpMipNvmSecureProfileDataUpdate(uint8 SetType, HlpHspdSecureSegData* MsgDataP);

extern void HlpDMUInit(void);
extern void HlpMipKeysGenEnc(void);
/*extern void HlpResetMNAuthMsg(void *MsgDataP);*/
/*extern void HlpSetMNAuthMsg(void *MsgDataP);*/
extern void HlpPswSDmuKeyGenRspMsg(void);
extern void HlpPswSDmuKeyEncRspMsg(void);
extern void HlpDmuRsaPublicKeyActiveAndMNAuthSetMsg(uint8 SetType, HlpRSAPublicKeyOrgIdInfoT *MsgDataP);

extern void HlpMipStartRrpAdminProhibRetry(void);
extern bool HlpMipCheckRrpAdminProhibRetry(void);
extern bool HlpMipCheckRrpAdminProhibRetryOtherErr(void);
extern void HlpMipClearRrpAdminProhibRetry(void);

extern void HlpMipResetRrpCode(void);
extern void HlpMipSetRrpCode(uint8 RrpCode);
extern uint8 HlpMipGetRrpCode(void);
extern void HlpMIPReRegistration(MipReRegReasonT Reason);
extern void HlpReturnToService(void);

/*****************************************************************************
* $Log: hlpmip.h $
*****************************************************************************/

/*****************************************************************************
* End of File
*****************************************************************************/
#endif


