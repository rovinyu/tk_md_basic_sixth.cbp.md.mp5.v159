#!/usr/bin/perl
#
#  Copyright Statement:
#  --------------------
#  This software is protected by Copyright and the information contained
#  herein is confidential. The software may not be copied and the information
#  contained herein may not be used or disclosed except with the written
#  permission of MediaTek Inc. (C) 2016
#
#  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
#  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
#  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
#  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
#  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
#  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
#  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
#  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
#  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
#  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
#  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
#
#  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
#  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
#  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
#  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
#  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
#
#  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
#  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
#  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
#  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
#  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
#
#*****************************************************************************
#*
#* Filename:
#* ---------
#*   CheckDuplicatedLib.pl
#*
#* Project:
#* --------
#*   >=JADE
#*
#* Description:
#* ------------
#*   This script is to check daplicated libs in link serach path
#*
#*============================================================================
use File::Basename;
#print @ARGV,"\n";
my %count;
my @libs;
my @suggest;
my @repeatbuildlibs;
my $buildpathindex = 0;
my $buildpath;
my $tmpcnt = 0;
my @repeatinfo;
#get path and remove repeat
my @paths = grep {!$count{$_}++} split(/-L/, join("",@ARGV));

#print join("\n",@paths),"\n";
foreach(@paths)
{
	my $path = $_;
	push(@libs,map {basename(/(lib.*\.a)/)} glob "$_/lib*.a");
	if($path =~ /build\/.*\/bin\/lib/)
	{
		$buildpathindex = $tmpcnt;
		$buildpath = $path;				
	}
	$tmpcnt++;
}

#print "buildpathindex=$buildpathindex\n","build path = $buildpath\n";
#print join("  ",@libs),"\n";
%count = ();
map {$count{$_}++; push(@repeatlibs,$_) if($count{$_} >= 2);} @libs;

#print join("  ",@libs),"\n";

if(scalar(@repeatlibs) != 0)
{
	print "\n[ERROR]:There're duplicated libs! It may cause unexpect strange error.\n";
	print "[Duplicated libs]: ",join(" ",@repeatlibs),"\nunder these path:\n";
	foreach(@repeatlibs)
	{
		my $templib = $_;
		foreach(@paths)
    {
    	my $path = $_;
			map {if(basename(/(lib.*\.a)/) eq $templib) {print "$path/$templib\n";}} glob "$_/lib*.a";
		}		
	}

#get the buildpath repeat libraries
	foreach(@repeatlibs)
	{
		my $templib = $_;
		map  {if(basename(/(lib.*\.a)/) eq $templib) {push(@repeatbuildlibs,$templib);}} glob "$buildpath/lib*.a";
	}
#print "old build path = @paths\n";		
#delete build path from all search paths	
	splice(@paths, $buildpathindex, 1);
#print "new build path = @paths\n";	
#search the repeat libs in left paths
#print "repeatbuildlibs = @repeatbuildlibs\n";	
	foreach(@repeatbuildlibs)
	{
		my $duplib = $_;
		foreach(@paths)
    {
    	my $path = $_;
			map {if(basename(/(lib.*\.a)/) eq $duplib) {push(@repeatinfo,"$path/$duplib");}} glob "$_/lib*.a";
		}
	}
	print "\nPlease keep the right library and manual remove others, or you can contact code release owner!\n";
	print "Sugguest remove following lib(s):\n",join("\n",@repeatinfo),"\n" if(scalar(@repeatinfo) != 0);
  die "\n";
}
else
{
	print "repeatlib check done!\n";
}