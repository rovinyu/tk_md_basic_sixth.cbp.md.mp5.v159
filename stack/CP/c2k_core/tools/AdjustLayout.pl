#!/usr/bin/perl
#
#  Copyright Statement:
#  --------------------
#  This software is protected by Copyright and the information contained
#  herein is confidential. The software may not be copied and the information
#  contained herein may not be used or disclosed except with the written
#  permission of MediaTek Inc. (C) 2015
#
#  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
#  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
#  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
#  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
#  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
#  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
#  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
#  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
#  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
#  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
#  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
#
#  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
#  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
#  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
#  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
#  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
#
#  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
#  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
#  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
#  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
#  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
#
#*****************************************************************************
#*
#* Filename:
#* ---------
#*   AdjustLayout.pl
#*
#* Project:
#* --------
#*   JADE
#*
#* Description:
#* ------------
#*   This script is to adjust the RO region size in link file for seccod link
#*
#*============================================================================

($map,$link)  = @ARGV;


open MAP,"<$map" or die "$! $map!";
open LINK, "<$link" or die "$! $link!";
open LINK_BAK, ">$link.bak" or die "$! $link.bak!";

$adjust_adr = 0;
$flag       = 0;
while(<MAP>)
{
#FLASHV          0x00000000        0x4 load address 0x00400ce0
    if(/FLASHV\s+0[xX][0-9a-fA-F]+\s+(0[xX][0-9a-fA-F]+)\s+load address\s+(0[xX][0-9a-fA-F]+)/)
    {
#64KB align, and 1500 bytes left for security boot and ccci header. 
        $adjust_adr = &align(eval($1) + eval($2) + 1500,64*1024);
        $adjust_adr = sprintf("%#x",$adjust_adr);
        #print "find the FLASHV section info : $_, adjust address is : $adjust_adr\n";
        $flag = 1;
    }
}

if($flag == 0)
{
	die "can not find the FLASHV section in file $map !!";
}

while(<LINK>)
{
    	s/(\s+.\s+=\s)(\.)(;.*)/$1$adjust_adr$3/;
    	print LINK_BAK;
}

close MAP;
close LINK;
close LINK_BAK;

unlink "$link";
rename "$link.bak","$link";

sub align
{
	($val,$alignsize) = @_;
#	print "1=@_[0],2=@_[1]\n";
	return ( $val % $alignsize ) ? (int($val / $alignsize) + 1)*$alignsize : $val;
}

