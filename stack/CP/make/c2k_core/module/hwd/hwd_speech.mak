###############################################
# Define source file folder to SRC_LIST
###############################################


SRC_LIST  += hwd/hwd_speech/fdrv/fd216_drv.c
SRC_LIST  += hwd/hwd_speech/fdrv/fd216_dsp_init.c
SRC_LIST  += hwd/hwd_speech/fdrv/fd216_idma.c
SRC_LIST  += hwd/hwd_speech/hwdsph.c
SRC_LIST  += hwd/hwd_speech/hwdaudioservice.c
SRC_LIST  += hwd/hwd_speech/hwdsp_enhance.c
SRC_LIST  += hwd/hwd_speech/hwdafe_common.c
SRC_LIST  += hwd/hwd_speech/hwdam.c
SRC_LIST  += hwd/hwd_speech/hwdsidetone.c
SRC_LIST  += hwd/hwd_speech/hwdsp_c2k.c
SRC_LIST  += hwd/hwd_speech/hwdsal_exp.c
SRC_LIST  += hwd/hwd_speech/hwdsal_impl.c
SRC_LIST  += hwd/hwd_speech/hwdvm.c
SRC_LIST  += hwd/hwd_speech/hwdrawpcmrec_drv.c
SRC_LIST  += hwd/hwd_speech/hwdpcmrec_drv.c
SRC_LIST  += hwd/hwd_speech/hwdbgSnd.c
SRC_LIST  += hwd/hwd_speech/hwdbtsco_drv.c
SRC_LIST  += hwd/hwd_speech/hwddaca.c
SRC_LIST  += hwd/hwd_speech/hwdmedia.c
SRC_LIST  += hwd/hwd_speech/hwdpcm4way.c
SRC_LIST  += hwd/hwd_speech/hwdctm_drv.c

SRC_LIST  += hwd/hwd_speech/od_memcpy.S

ifeq "$(PLATFORM)" "MT6735"	# TODO: 
  SRC_LIST  += hwd/hwd_speech/hwdafe_6735.c
  SRC_LIST  += hwd/hwd_speech/hwdApAudSysConfig_6735.c
# other chips by file
endif

ifeq "$(PLATFORM)" "MT6755"	# TODO: 
  SRC_LIST  += hwd/hwd_speech/hwdafe_6755.c
  SRC_LIST  += hwd/hwd_speech/hwdApAudSysConfig_6755.c
# other chips by file
endif

ifeq "$(PLATFORM)" "MT6797"	# TODO: 
   SRC_LIST  += hwd/hwd_speech/hwdafe_6797.c
   SRC_LIST  += hwd/hwd_speech/hwdApAudSysConfig_6797.c
# other chips by file
endif

ifeq "$(PLATFORM)" "MT6750"	# TODO: 
   SRC_LIST  += hwd/hwd_speech/hwdafe_6750.c
   SRC_LIST  += hwd/hwd_speech/hwdApAudSysConfig_6750.c
endif

ifeq "$(PLATFORM)" "MT6757"	# TODO: 
   SRC_LIST  += hwd/hwd_speech/hwdafe_6757.c
   SRC_LIST  += hwd/hwd_speech/hwdApAudSysConfig_6757.c
endif

ifeq "$(PLATFORM)" "MT6757P"	# TODO: 
   SRC_LIST  += hwd/hwd_speech/hwdafe_6757p.c
   SRC_LIST  += hwd/hwd_speech/hwdApAudSysConfig_6757p.c
endif

###############################################
#  Define include path lists to INC_DIR
###############################################
INC_DIR +=  hwd
INC_DIR +=  hwd/hwd_speech 
INC_DIR +=  hwd/hwd_speech/fdrv
INC_DIR +=  hwd/hwd_speech/lib/MTKINC
INC_DIR +=  val/val_speech
INC_DIR +=  ipc/speech 
INC_DIR +=  hwd/hwd_speech/sddl 
INC_DIR +=  iop 

###############################################
# Define the specified compile options to COMP_DEFS
###############################################
COMP_DEFS = 


###############################################
# Define the source file search paths to SRC_PATH
###############################################
SRC_PATH = 


