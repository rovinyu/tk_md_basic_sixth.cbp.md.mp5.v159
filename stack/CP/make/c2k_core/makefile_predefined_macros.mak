# 
#  Copyright Statement:
#  ---------------------------
#  This software/firmware and related documentation ("MediaTek Software") are
#  protected under relevant copyright laws. The information contained herein 
#  is confidential and proprietary to MediaTek Inc. and/or its licensors.  
#  Without the prior written permission of MediaTek inc. and/or its licensors,
#  any reproduction,modification, use or disclosure of MediaTek Software, and
#  information contained herein, in whole or in part, shall be strictly prohibited.
#   
#  MediaTek Inc.(C)2011.All rights reserved.
#
#  BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND
#  AGREES THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK 
#  SOFTWARE") RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED 
#  TO RECEIVER ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL 
#  WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
#  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR 
#  NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER 
#  WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, 
#  INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER 
#  AGREES TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING 
#  THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE 
#  RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES 
#  CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR 
#  ANY MEDIATEK SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO 
#  CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND 
#  EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT 
#  TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,AT MEDIATEK'S OPTION, 
#  TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,OR REFUND ANY SOFTWARE 
#  LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK 
#  SOFTWARE AT ISSUE. 
#
# *************************************************************************

# Generated at 2015-05-12 10:55:44

# SYS_TARGET = $(ST_??)
#
# This define is used to select source code for the primary system target. 
#-------------------------------------------------------------------------

ST_HW  = 1
ST_SIM = 2

SYS_DEFS += ST_HW ST_SIM

ifneq ($(strip $(SYS_TARGET)),)
   SYS_DEFS += SYS_TARGET
endif

# SYS_BOARD = $(SB_??)
#
# This define is used to select source code for the board type used. 
#-------------------------------------------------------------------

SB_NONE     = 0
SB_BB7      = 1
SB_EVB7     = 2
SB_FEATURE_PHONE = 3
SB_DATACARD = 4
SB_EVB8     = 5
SB_DENALI   = 6
SB_JADE     = 7
SB_EVEREST  = 8
SB_OLYMPUS  = 9

SYS_DEFS += SB_NONE SB_BB7 SB_EVB7 SB_FEATURE_PHONE SB_DATACARD SB_EVB8 \
            SB_DENALI SB_JADE SB_EVEREST SB_OLYMPUS

ifneq ($(strip $(SYS_BOARD)),)
   SYS_DEFS += SYS_BOARD
endif

# SYS_BD_VARIANT = $(SBV_??)
#
# This define is used to select source code for a specific variant
# of the SB_EVB8 board.
# (Other SB_?? boards can have their own variants, too.)
#-------------------------------------------------------------------

SBV_BASE    = 0
SBV_FP      = 1

SYS_DEFS += SBV_BASE SBV_FP

ifneq ($(strip $(SYS_BD_VARIANT)),)
   SYS_DEFS += SYS_BD_VARIANT
endif

# SYS_ASIC = $(SA_??)
#
# This define is used to select source code for the type of process/ASIC used.  
# Since some code is shared between several ASICs, all of the following must be declared.
#-----------------------------------------------------------------------------

SA_FPGA     = 1
SA_CBP70    = 2
SA_CBP71    = 3
SA_CBP63    = 4
SA_CBP80    = 5
SA_CBP82    = 6
SA_MT6735   = 7
SA_MT6755   = 8
SA_MT6750   = 9
SA_MT6797   = 10
SA_MT6757   = 11
SA_MT6757P   = 11
SYS_DEFS += SA_FPGA SA_CBP70 SA_CBP71 SA_CBP63 SA_CBP80 SA_CBP82 \
            SA_MT6735 SA_MT6755 SA_MT6750 SA_MT6797 SA_MT6757 SA_MT6757P

ifneq ($(strip $(SYS_ASIC)),)
   SYS_DEFS += SYS_ASIC
endif


# SYS_VERSION = $(SV_REV_??)
#
# This define is used to select source code which is specific to a particular 
# revision of the ASIC.  
#----------------------------------------------------------------------------

SV_REV_A0   = 10
SV_REV_A1   = 11
SV_REV_B0   = 20
SV_REV_C0   = 30
SV_REV_D0   = 40
                
SYS_DEFS += SV_REV_A0 SV_REV_A1 SV_REV_B0 SV_REV_C0 SV_REV_D0

ifneq ($(strip $(SYS_VERSION)),)
   SYS_DEFS += SYS_VERSION
endif

ifeq "$(USE_VER)" "SV_REV_A0"
SO_VERSION = $(SV_REV_A0)
else
ifeq "$(USE_VER)" "SV_REV_A1"
SO_VERSION = $(SV_REV_A1)
else
ifeq "$(USE_VER)" "SV_REV_B0"
SO_VERSION = $(SV_REV_B0)
else
ifeq "$(USE_VER)" "SV_REV_C0"
SO_VERSION = $(SV_REV_C0)
else
ifeq "$(USE_VER)" "SV_REV_D0"
SO_VERSION = $(SV_REV_D0)
else
SO_VERSION =
endif
endif
endif
endif
endif

# SYS_DSPM_PATCH = $(SP_??)
# or SYS_DSPV_PATCH = $(SP_??)
# This define is used to select source code for the specific patch revision.  
#-----------------------------------------------------------------------------

SP_ALL  = 0
SP_ONE   = 1
SP_TWO   = 2
SP_THREE = 3
SP_FOUR  = 4

SYS_DEFS += SP_ALL SP_ONE SP_TWO SP_THREE SP_FOUR

ifneq ($(strip $(SYS_DSPM_PATCH)),)
   SYS_DEFS += SYS_DSPM_PATCH
endif

ifneq ($(strip $(SYS_DSPV_PATCH)),)
   SYS_DEFS += SYS_DSPV_PATCH
endif


# SYS_OPTION_RF_HW = $(SYS_RF_??)
#
# This define is used to set the Target RF Options
#-----------------------------------------------------------------------------
SYS_RF_GRF_6413     = 13
SYS_RF_FCI_7790     = 14
SYS_RF_MTK_ORIONC   = 15
SYS_RF_MT6176       = 16

SYS_DEFS +=      SYS_RF_GRF_6413 SYS_RF_FCI_7790 SYS_RF_MTK_ORIONC SYS_RF_MT6176

ifneq ($(strip $(SYS_OPTION_RF_HW)),)
   SYS_DEFS += SYS_OPTION_RF_HW
endif


# SYS_CUST_PLT = $(SYS_PLT_??)
#
# This define is used to set the Target custom platform Options
# ---------------------------------
SYS_PLT_SHAMU=0
SYS_PLT_MT6735_EVB=1
SYS_PLT_MT6735_PHONE=2
SYS_PLT_MT6755_EVB=3
SYS_PLT_MT6755_PHONE=4
SYS_PLT_MT6797_EVB=5
SYS_PLT_MT6797_PHONE=6
SYS_PLT_MT6750_PHONE=7
SYS_PLT_MT6757_EVB=9
SYS_PLT_MT6757_PHONE=10

SYS_DEFS += SYS_PLT_SHAMU \
            SYS_PLT_MT6735_EVB SYS_PLT_MT6735_PHONE \
            SYS_PLT_MT6755_EVB SYS_PLT_MT6755_PHONE \
            SYS_PLT_MT6797_EVB SYS_PLT_MT6797_PHONE \
            SYS_PLT_MT6750_PHONE \
            SYS_PLT_MT6757_EVB SYS_PLT_MT6757_PHONE

ifneq ($(strip $(SYS_CUST_PLT)),)
   SYS_DEFS += SYS_CUST_PLT
endif

# SYS_OPTION_IMD_MODULE = $(SYS_IMD_MODULE_ISUSED/NOTUSED)
#
# This define is used to set to use IMD module or not.
#-----------------------------------------------------------------------------
# SYS_IMD_MODULE_ISUSED     Value to identify IMD module is complied, can be used if enabled
# SYS_IMD_MODULE_NOTUSED    Value to identify IMD module is not complied, so cann not be used

SYS_IMD_MODULE_ISUSED    = 0
SYS_IMD_MODULE_NOTUSED   = 1

SYS_DEFS += SYS_IMD_MODULE_ISUSED SYS_IMD_MODULE_NOTUSED

ifneq ($(strip $(SYS_OPTION_IMD_MODULE)),)
   SYS_DEFS += SYS_OPTION_IMD_MODULE
endif

# SYS_OPTION_IMD_GAIN_TABLE = $(SYS_IMD_GAIN_TABLE_SWITCH)
#
# This define is used to set to switch IMD table or not 
#-----------------------------------------------------------------------------
# SYS_IMD_GAIN_TABLE_SWITCH      Value to identify IMD module switch gain table based detection result.
# SYS_IMD_GAIN_TABLE_NOSWITCH    Value to identify IMD module does not switch gain table no matter detection result is. 

SYS_IMD_GAIN_TABLE_NOSWITCH   = 0
SYS_IMD_GAIN_TABLE_SWITCH     = 1

SYS_DEFS += SYS_IMD_GAIN_TABLE_NOSWITCH SYS_IMD_GAIN_TABLE_SWITCH

ifneq ($(strip $(SYS_OPTION_IMD_GAIN_TABLE)),)
   SYS_DEFS += SYS_OPTION_IMD_GAIN_TABLE
endif

# SYS_OPTION_FMP_MMSE_MRC = $(SYS_FMP_MMSE_MRC_??)
#
# This define is used to Compile FMP MMSE-MRC Switch Module or not.
#-----------------------------------------------------------------------------
# SYS_FMP_MMSE_MRC_SWITCH   Value to identify Compile FMP MMSE-MRC Switch Module is complied, can be used if enabled
# SYS_FMP_MMSE_MRC_NOSWITCH Value to identify Compile FMP MMSE-MRC Switch Module is not complied

SYS_FMP_MMSE_MRC_NOSWITCH    = 0
SYS_FMP_MMSE_MRC_SWITCH      = 1

SYS_DEFS += SYS_FMP_MMSE_MRC_NOSWITCH SYS_FMP_MMSE_MRC_SWITCH

ifneq ($(strip $(SYS_OPTION_FMP_MMSE_MRC)),)
   SYS_DEFS += SYS_OPTION_FMP_MMSE_MRC
endif

# SYS_OPTION_RPC_THRESHOLD = $(SYS_RPC_HIGH_THRESHOLD_??)
#
# This define is used to Use High value of MBP_MCD_RPC_RTH or not.
#-----------------------------------------------------------------------------
# SYS_RPC_HIGH_THRESHOLD_USED       
# SYS_RPC_HIGH_THRESHOLD_NOTUSED    

SYS_RPC_HIGH_THRESHOLD_USED    = 0
SYS_RPC_HIGH_THRESHOLD_NOTUSED = 1

SYS_DEFS += SYS_RPC_HIGH_THRESHOLD_USED SYS_RPC_HIGH_THRESHOLD_NOTUSED

ifneq ($(strip $(SYS_OPTION_RPC_THRESHOLD)),)
   SYS_DEFS += SYS_OPTION_RPC_THRESHOLD
endif

# SYS_OPTION_GPS_RF_HW = $(SYS_GPS_RF_??)
#
# This define is used to set the Target GPS RF Options
#-----------------------------------------------------------------------------
# SYS_GPS_RF_NONE           Value to NON GPS RF is in use
# SYS_GPS_RF_GLONAV_1040    Value to identify GloNav 1040

SYS_GPS_RF_NONE         = 0
SYS_GPS_RF_GLONAV_1040  = 3

SYS_DEFS += SYS_GPS_RF_NONE SYS_GPS_RF_GLONAV_1040

ifneq ($(strip $(SYS_OPTION_GPS_RF_HW)),)
   SYS_DEFS += SYS_OPTION_GPS_RF_HW
endif

# SYS_OPTION_EXTERNAL_GPS_HW = $(SYS_EXT_GPS_??)
#
# This define is used to set the Target External GPS Options
#-----------------------------------------------------------------------------
# SYS_EXT_GPS_NONE       Value to NON External GPS is in use
# SYS_EXT_GPS_GNS_7560   Value to identify ST GNS7560
# SYS_EXT_GPS_ON_AP      Value to identify GPS Device for GPS on AP (Bypass AA Msg)  

SYS_EXT_GPS_NONE        = 0
SYS_EXT_GPS_ON_AP       = 2
SYS_EXT_GPS_ON_AP_DEV0  = 3

SYS_DEFS += SYS_EXT_GPS_NONE  SYS_EXT_GPS_ON_AP  SYS_EXT_GPS_ON_AP_DEV0

ifneq ($(strip $(SYS_OPTION_EXTERNAL_GPS_HW)),)
   SYS_DEFS += SYS_OPTION_EXTERNAL_GPS_HW
endif

# Agps option

SYS_AGPS_ENABLE = 1

SYS_DEFS += SYS_AGPS_ENABLE

ifneq ($(strip $(SYS_OPTION_AGPS_ENABLE)),)
   SYS_DEFS += SYS_OPTION_AGPS_ENABLE
endif

ifneq ($(strip $(SYS_OPTION_AP_GPS_HW)),)
  SYS_DEFS +=  SYS_OPTION_AP_GPS_HW
endif 


# SYS_OPTION_REFERENCE_FREQ = $(SYS_REFERENCE_??)
#
# This define is used to identify the Target Clock Reference
#-----------------------------------------------------------------------------
# SYS_REFERENCE_FREQ_19_20    Value to identify 19.20 MHz clock reference
# SYS_TEST_REF_FREQ_39_32     Value to identify 39.3216 MHz clock reference (for test only)
# SYS_REFERENCE_FREQ_26_00    Value to identify 26.00 MHz clock reference (for Denali)
#                                    Used for PLL bypass, so never use with option SYS_OPTION_INTERNAL_PLL
#                                    defined!!!

SYS_REFERENCE_FREQ_19_20   = 0
SYS_TEST_REF_FREQ_39_32    = 1
SYS_REFERENCE_FREQ_26_00   = 2

SYS_DEFS += SYS_REFERENCE_FREQ_19_20 SYS_TEST_REF_FREQ_39_32 SYS_REFERENCE_FREQ_26_00

ifneq ($(strip $(SYS_OPTION_REFERENCE_FREQ)),)
   SYS_DEFS += SYS_OPTION_REFERENCE_FREQ
endif

# SYS_OPTION_32K_CLK_SOURCE = $(SYS_OPTION_32K_CLK_??)
#
# This define is used to identify the 32K clock source
#-----------------------------------------------------------------------------
# SYS_OPTION_32K_CLK_INT_OSC    Value to identify internal 32K osc
# SYS_OPTION_32K_CLK_BYPASS     Value to identify external 32K osc
# SYS_OPTION_32K_CLK_DIV_TCXO Value to identify the usage of TCXO/512 instead of 32K osc 

SYS_OPTION_32K_CLK_INT_OSC     = 0
SYS_OPTION_32K_CLK_BYPASS      = 1
SYS_OPTION_32K_CLK_DIV_TCXO    = 2

SYS_DEFS += SYS_OPTION_32K_CLK_INT_OSC SYS_OPTION_32K_CLK_BYPASS SYS_OPTION_32K_CLK_DIV_TCXO

ifneq ($(strip $(SYS_OPTION_32K_CLK_SOURCE)),)
   SYS_DEFS += SYS_OPTION_32K_CLK_SOURCE
endif


# SYS_OPTION_AUDIO_PATH_FEATURE = $(SYS_OPTION_AUDIO_PATH_FEATURE_??)
#
# This define is used to select audio path feature
SYS_OPTION_AUDIO_PATH_FEATURE_DEFAULT     = 0
SYS_OPTION_AUDIO_PATH_FEATURE_ONE      = 1
SYS_OPTION_AUDIO_PATH_FEATURE_TWO      = 2

SYS_DEFS += SYS_OPTION_AUDIO_PATH_FEATURE_DEFAULT  SYS_OPTION_AUDIO_PATH_FEATURE_ONE SYS_OPTION_AUDIO_PATH_FEATURE_TWO

ifneq ($(strip $(SYS_OPTION_AUDIO_PATH_FEATURE)),)
   SYS_DEFS += SYS_OPTION_AUDIO_PATH_FEATURE
endif


#
# This define is used to select gps path feature
SYS_GPS_DEFAULT     = 0
SYS_GPS_LOCAL_INTERNAL      = 1


SYS_DEFS += SYS_GPS_DEFAULT  SYS_GPS_LOCAL_INTERNAL 

ifneq ($(strip $(SYS_OPTION_GPS_HW)),)
   SYS_DEFS += SYS_OPTION_GPS_HW
endif



# SYS_OPTION_USB
#
# Define USB used
# SYS_USB_NONE        USB not included  
# SYS_USB_SYNOPSYS    USB core in cbp7X and older 
# SYS_USB_SYNOPSYS2  - Adds 8 Interface and EP0 stall capability
# SYS_USB_GUC         USB core for cbp8X
SYS_USB_NONE           =  99
SYS_USB_SYNOPSYS       = 0 
SYS_USB_SYNOPSYS2      = 1 
SYS_USB_SYNOPSYS_LAST  = 2 
SYS_USB_GUC            = 3
SYS_USB_GUC_LAST       = 4

SYS_DEFS += SYS_USB_NONE SYS_USB_SYNOPSYS SYS_USB_SYNOPSYS2  SYS_USB_SYNOPSYS_LAST SYS_USB_GUC SYS_USB_GUC_LAST

ifneq ($(strip $(SYS_OPTION_USB)),)
   SYS_DEFS += SYS_OPTION_USB
endif


# SYS_OPTION_IPC_DEV
#
# Define the device type of AP-CBP, use for flashless support
# SYS_IPC_DEV_NONE   No IPC device  
# SYS_IPC_DEV_USB    USB IPC type
# SYS_IPC_DEV_DPRAM  DPRAM IPC type
# SYS_IPC_DEV_SDIO   SDIO IPC type
# SYS_IPC_DEV_SPI    SPI IPC type
# SYS_IPC_DEV_CCIF   CCIF IPC type
SYS_IPC_DEV_NONE     =  99
SYS_IPC_DEV_USB      = 2 
SYS_IPC_DEV_DPRAM    = 3 
SYS_IPC_DEV_SDIO     = 4 
SYS_IPC_DEV_SPI      = 5
SYS_IPC_DEV_CCIF     = 6

SYS_DEFS += SYS_IPC_DEV_NONE SYS_IPC_DEV_USB SYS_IPC_DEV_DPRAM SYS_IPC_DEV_SDIO SYS_IPC_DEV_SPI SYS_IPC_DEV_CCIF

ifneq ($(strip $(SYS_OPTION_IPC_DEV)),)
   SYS_DEFS += SYS_OPTION_IPC_DEV
endif

# SYS_OPTION_TCXO_CONFIG = $(SYS_OPTION_TCXO_??)
#
# This define is used to identify the TCXO configuration definition
#-----------------------------------------------------------------------------
# SYS_OPTION_TCXO_CONFIG                TCXO configuration definition
#
#      SYS_OPTION_TCXO_SINGLE              single TCXO
#      SYS_OPTION_TCXO_DBL_MAIN            double TCXO - main OSC supplies the ARM reference clock
#      SYS_OPTION_TCXO_DBL_AUX             double TCXO - aux OSC supplies the ARM reference clock

SYS_OPTION_TCXO_SINGLE     = 0
SYS_OPTION_TCXO_DBL_MAIN   = 1
SYS_OPTION_TCXO_DBL_AUX    = 2

SYS_DEFS += SYS_OPTION_TCXO_SINGLE SYS_OPTION_TCXO_DBL_MAIN SYS_OPTION_TCXO_DBL_AUX

ifneq ($(strip $(SYS_OPTION_TCXO_CONFIG)),)
   SYS_DEFS += SYS_OPTION_TCXO_CONFIG
endif

# SYS_OPTION_HDET_HW = $(SYS_HDET_??)
#
# This define is used to set the Target power detector option
#-----------------------------------------------------------------------------
SYS_HDET_LMV221       =  0
SYS_HDET_ADL550       =  1
SYS_HDET_DISCRETE     =  2

SYS_DEFS += SYS_HDET_LMV221 SYS_HDET_ADL550 SYS_HDET_DISCRETE

ifneq ($(strip $(SYS_OPTION_HDET_HW)),)
  SYS_DEFS += SYS_OPTION_HDET_HW
endif
