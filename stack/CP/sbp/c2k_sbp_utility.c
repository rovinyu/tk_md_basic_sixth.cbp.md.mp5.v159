/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2005
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*****************************************************************************
*
* FILE NAME   :
* c2k_sbp_utility.c
*
* DESCRIPTION :
* C2K SBP related functions.
*
*
*
* HISTORY     :
*     See Log at end of file
*
*****************************************************************************/

/*----------------------------------------------------------------------------
 Include Files
----------------------------------------------------------------------------*/
#include "exeapi.h"
#include "monapi.h"
#include "sbpapi.h"
#include "dbmerrs.h"
#ifdef MTK_DEV_CCCI_FS
#include "nvram_data_items_id.h"
#include "nvram_interface.h"
#include "pswnam.h"
#include "../css/cssdbm.h"
#include "../css/csscust.h"
#include "hlpapi.h"
#include "hlpcustom.h"
#include "../hlp/hlpmip.h"
#include "../hlp/IP_CUSTM.H"
#endif

/*----------------------------------------------------------------------------
 Local Defines and Macros
----------------------------------------------------------------------------*/
nvram_ef_c2k_sbp_status_config_struct   sbp_status_features;
nvram_ef_c2k_sbp_data_config_struct     sbp_data_features;


/* default value for SBP status features
   refer to c2k_sbp_status_enum for C2K SBP status features.
*/
const nvram_ef_c2k_sbp_status_config_struct NVRAM_EF_C2K_SBP_STATUS_CONFIG_DEFAULT[] = {
{
    /*first_boot*/
    TRUE,
    /* sbp_id */
    SBP_ID_GENERIC,
    /* c2k_sbp_status_config */
    {
        /*1st byte*/
        (0 << 0) | /*  SBP_STATUS_SLC_INVALID_SECTORID_CHK */
        (0 << 1) | /*  SBP_STATUS_EXT_SMS_PB_INIT  */
        (0 << 2) | /*  SBP_STATUS_MODEM_TURNON_AUTO    */
        (0 << 3) | /*  SBP_STATUS_CHINATELECOM_CARDLOCK    */
        (0 << 4) | /*  SBP_STATUS_CHINATELECOM_EXTENSIONS  */
        (1 << 5) | /*  SBP_STATUS_SYS_OPTION_OTASP */
        (0 << 6) | /*  SBP_STATUS_ALTERNATE_PN */
        (0 << 7) , /*  SBP_STATUS_32K_LESS */

        /*2nd byte*/
        (0 << 0) | /*  SBP_STATUS_CO_CLOCK */
        (0 << 1) | /*  SBP_STATUS_SHDR_HYBRID  */
        (0 << 2) | /*  SBP_STATUS_1X_ADV_SUPPORT   */
        (1 << 3) | /*  SBP_STATUS_SYS_REGISTRATION_THROTTLING_FEATURE  */
        (1 << 4) | /*  SBP_STATUS_SYS_SAFETY_NET_REGISTRATION_FEATURE  */
        (0 << 5) | /*  SBP_STATUS_SYS_ENH_SYSTEM_SELECT_FEATURE    */
        (0 << 6) | /*  SBP_STATUS_SYS_ERI_FEATURE  */
        (0 << 7) , /*  SBP_STATUS_SYS_CSS_1X_MAPE_HOME_SYS_AVOID_FEATURE   */

        /*3rd byte*/
        (0 << 0) | /*  SBP_STATUS_SYS_CSS_1X_RESTRICT_SILENTRETRY_TO_SAME_GEO_FEATURE  */
        (1 << 1) | /*  SBP_STATUS_SYS_CSS_1X_USE_NAM_FOR_VALIDATION_FEATURE    */
        (1 << 2) | /*  SBP_STATUS_SYS_CSS_1X_ONLY_REJECT_REDIR_IF_NEG_IN_PRL_FEATURE   */
        (0 << 3) | /*  SBP_STATUS_MIP  */
        (0 << 4) | /*  SBP_STATUS_MIP_DMU  */
        (1 << 5) | /*  SBP_STATUS_IPv6 */
        (0 << 6) | /*  SBP_STATUS_VZW_DATA_ENHANCEMENT */
        (0 << 7) , /*  SBP_STATUS_GMSS_MRU */

        /*4th byte*/
        (0 << 0) | /*  SBP_STATUS_IDLE_GEO_LIST */
        (0 << 1) | /*  SBP_STATUS_1X_PS_DELAY */
        (0 << 2) | /*  SBP_STATUS_VZW_TDO */
        (0 << 3) | /*  SBP_STATUS_VZW_E911 */
        (0 << 4) | /*  SBP_STATUS_SILENT_REDIAL_ON_SIB8 */
        (0 << 5) | /*  SBP_STATUS_VZW_MEID_ME */
        (0 << 6) | /*  SBP_STATUS_VZW_OTA */
        (0 << 7) , /*  SBP_STATUS_VZW_SMS */

        /*5th byte*/
        (0 << 0) | /*  SBP_STATUS_VZW_SMS_CMAS */
        (0 << 1) | /*  SBP_STATUS_DO_APERSISTENCE_AVOID */
        (0 << 2) | /*  SBP_STATUS_HVOLTE*/
        (1 << 3) | /*  SBP_STATUS_BTSAP */
        (0 << 4) | /*  SBP_STATUS_VZW_TX_CLOSELOOP */
        (1 << 5) | /*  SBP_STATUS_C2KONLY_OOSA_RETRIEVE_OPTIMIZE */
        (0 << 6) | /*  SBP_STATUS_VZW_LTE_SIB8_TIMING */
        (0 << 7) , /*  SBP_STATUS_VZW_SUPL */

        /*6th byte*/
        (0 << 0) | /*  SBP_STATUS_VZW_DISABLE_EVDO_SUSPEND_TIMING */
        (0 << 1) | /*  SBP_STATUS_VZW_SMS_BSP */
        (0 << 2) | /*  SBP_STATUS_CT_AUTO_REG_WHEN_MCC_CHANGED */
        (0 << 3) | /*  SBP_STATUS_SPRINT_QC_REDIRECT */
        (0 << 4) | /*  SBP_STATUS_VZW_FEMTOCELL_FEATURE */
        (0 << 5) | /*  SBP_STATUS_VZW_LTE_MMO_SCAN */
        (1 << 6) | /*  SBP_STATUS_SPRINT_1X_SYS_LOST */
        (0 << 7) , /*  SBP_STATUS_SID_MAPPING_MCC  */

        /*7th byte*/
        (0 << 0) | /*  SBP_STATUS_SPRINT_BSR */
        (0 << 1) | /*  SBP_STATUS_LBS_DATA_LOGGING */
        (0 << 2) | /*  SBP_STATUS_CDG_BSR */
        (0 << 3) | /*  SBP_STATUS_CDG_AVOIDANCE */
        (0 << 4) | /*  SBP_STATUS_SPRINT_TRAFF_NEW_CH_NOT_LOGGING_MRU */
        (0 << 5) | /*  SBP_STATUS_SPRINT_PERSISTENCE_FAIL */
        (0 << 6) | /*  SBP_STATUS_SPRINT_MO_SMS_BREAK_BSR */
        (0 << 7) , /*  SBP_STATUS_SPRINT_SMS */

        /*8th byte*/
        (0 << 0) | /*  SBP_STATUS_SESSION_NEGO_OPTM_POWERON */
        (0 << 1) | /* SBP_STATUS_SPRINT_AT_CMD */
        (0 << 2) | /*  SBP_PRL_ENHANCE_FOR_INT_ROAM */
        (0 << 3) | /*  SBP_CDG143_MAPE_SYS_AVOID_FEATURE */
        (0 << 4) | /*  SBP_VOICE_ROAM_BARRING */
        (0 << 5) | /*  SBP_USE_RESTRICTIVE_SYSTEM_MATCH_FEATURE */
        (0 << 6) | /*  SBP_ASSIGN_MRU0_IF_EMPTY */
        (0 << 7) , /*  SBP_STATUS_SPRINT_SMS_DISABLE_SO_NEG */

        /*9th byte*/
        (0 << 0) | /*  SBP_STATUS_SPRINT_SMS_CMAS */
        (0 << 1) | /*  SBP_STATUS_POWERUP_1X_SCAN */
        (0 << 2) | /*  SBP_FORCE_VOICE_GATING_ENABLE */
        (0 << 3) | /*  SBP_STATUS_SPRINT_BSR_DATA_HOLD */
        (0 << 4) | /*  SBP_STATUS_SPRINT_OPTM_WARM_RESET */
        (0 << 5) | /*  SBP_BAR_DATA_ROAMING */
        (0 << 6) | /*  SBP_VZW_PSW_BYPASS_BAD_SECTOR */
        (0 << 7) , /*  SBP_SO2_OPEN_GPS_EARLY */

        /*10th byte*/
        (0 << 0) |  /*  SBP_STATUS_SPRINT_REPORT_SO */
        (0 << 1) |  /* SBP_PAGING_MONITOR_TIMEOUT_TO_GEOSCAN */
        (0 << 2) |  /* SBP_STATUS_DO_IRAT_PRIORITY_DOWNGRADING */
        (0 << 3) |  /* SBP_STATUS_SPRINT_CALL_RELEASE */
        (0 << 4) |  /* SBP_STATUS_DO_PRL_LIST_ASSOCIATED_ONLY */
        (0 << 5) |  /* SBP_VZW_INTERNATIONAL_ROAMING_REG*/
        (0 << 6) |  /* SBP_STATUS_SPRINT_TRAFF_NEW_CH_LOGGING_LAST_CHANNEL*/
        (0 << 7) ,  /* SBP_VZW_REFRESH_PROCEED_DURING_THE_CALL */

        /*11th byte*/
        (1 << 0) |  /* SBP_DEACTIVATE_C2K_WHEN_UIM_ERROR */
        (0 << 1) |  /* SBP_STATUS_PS_REG_SRCH_IN_SAME_CYCLE_OPTI */
        (0 << 2) |  /* SBP_STATUS_VZW_UNSOLICITED_LOCATION_NOTIFICATION */
        (0 << 3) |  /* SBP_1XRTT_ENHANCE_SILENT_RETRY */
        (0 << 4) |  /* SBP_1XRTT_ENHANCE_AEHO */
        (0 << 5) |  /* SBP_STATUS_VZW_1XRTT_SYSTEM_LOST_SEARCH_FOR_UOI_SR */
        (0 << 6) |  /* SBP_STATUS_VZW_1XRTT_DISABLE_AHO_OPTIMIZE */
        (0 << 7) ,  /* SBP_STATUS_SYS_REG_THROTTLING_ENHANCE */

        /*12th byte*/
        (0 << 0) |   /* SBP_STATUS_OP01_LOCK_PROTECT */
        (0 << 1) |   /* SBP_1XRTT_DISABLE_VOICE_CALL */        
        (0 << 2) ,   /* SBP_ACG_OPS_TETHERING */
     }
}
};

/* default value for SBP data features
   refer to c2k_sbp_data_enum for C2K SBP data features.
*/
const nvram_ef_c2k_sbp_data_config_struct NVRAM_EF_C2K_SBP_DATA_CONFIG_DEFAULT[] = {
{
    {
        0x04, /* SBP_DATA_SUPPORT_PERSONALITY_COUNT */
        0x01, /* SBP_DATA_SUPPORT_NAM_COUNT */
        0x01, /* SBP_DATA_RUIM */
    }
}
};

void sbp_set_status_feature(c2k_sbp_status_enum feature,
                            bool is_turned_on,
                            nvram_ef_c2k_sbp_status_config_struct *sbp_feature_ptr);

void sbp_set_data_feature(c2k_sbp_data_enum feature,
                          uint8 data,
                          nvram_ef_c2k_sbp_data_config_struct *sbp_data_ptr);



/*****************************************************************************
* FUNCTION
*   sbp_query_status_feature()
*
* DESCRIPTION
*   This function is used to query c2k sbp status feature configuration
*
* PARAMETERS
*   feature [IN]    c2k sbp status feature
*
* RETURNS
*   KAL_TRUE    : if this feature is turned on
*   KAL_FALSE   : if this feature is turned off
*****************************************************************************/
bool sbp_query_status_feature(c2k_sbp_status_enum feature)
{
    uint8 *bitmask_ptr;

#ifdef MTK_PLT_ON_PC
    sbp_set_sbp_id();
#endif

    MonAssert(feature < SBP_STATUS_MAX_FEATURE,
              MON_SBP_FAULT_UNIT,
              (uint32)feature,
              (uint32)sbp_status_features.sbp_id,
              MON_HALT);

    bitmask_ptr = &(sbp_status_features.c2k_sbp_status_config[feature / 8]);

    return ((((*bitmask_ptr) & (0x01 << (feature % 8))) != 0)? TRUE : FALSE);

}

/*****************************************************************************
* FUNCTION
*   sbp_set_status_feature()
*
* DESCRIPTION
*   This function is used to set modem configuration
*
* PARAMETERS
*   feature         [IN]    modem feature
*   is_turned_on    [IN]
*   sbp_feature_ptr [IN/OUT]
*
* RETURNS
*   TRUE if success; otherwise FALSE
*****************************************************************************/
void sbp_set_status_feature(c2k_sbp_status_enum feature,
                            bool is_turned_on,
                            nvram_ef_c2k_sbp_status_config_struct *sbp_feature_ptr)
{
    uint8 *bitmask_ptr;

    MonAssert(feature < SBP_STATUS_MAX_FEATURE,
              MON_SBP_FAULT_UNIT,
              (uint32)feature,
              (uint32)sbp_status_features.sbp_id,
              MON_HALT);

    bitmask_ptr = &(sbp_feature_ptr->c2k_sbp_status_config[feature / 8]);

    if (is_turned_on == TRUE)
    {
        *bitmask_ptr = ((*bitmask_ptr) | (0x01 << (feature % 8)));
    }
    else
    {
        *bitmask_ptr = ((*bitmask_ptr) & ~(0x01 << (feature % 8)));
    }
}


/*****************************************************************************
* FUNCTION
*   sbp_query_data_feature()
*
* DESCRIPTION
*   This function is used to query modem configuration data
*
* PARAMETERS
*   feature [IN]    modem feature
*
* RETURNS
*   the unsigned byte value for the feature
*****************************************************************************/
uint8 sbp_query_data_feature(c2k_sbp_data_enum feature)
{

#ifdef MTK_PLT_ON_PC
    sbp_set_sbp_id();
#endif

    MonAssert(feature < SBP_DATA_MAX_FEATURE,
              MON_SBP_FAULT_UNIT,
              (uint32)feature,
              (uint32)sbp_status_features.sbp_id,
              MON_HALT);

    return sbp_data_features.c2k_sbp_data_config[feature];
}


/*****************************************************************************
* FUNCTION
*   sbp_set_data_feature()
*
* DESCRIPTION
*   This function is used to set modem configuration data
*
* PARAMETERS
*   feature [IN]    modem feature
*
* RETURNS
*   the unsigned byte value for the feature
*****************************************************************************/
void sbp_set_data_feature(c2k_sbp_data_enum feature,
                                 uint8 data,
                                 nvram_ef_c2k_sbp_data_config_struct *sbp_data_ptr)
{
    MonAssert(feature < SBP_DATA_MAX_FEATURE,
              MON_SBP_FAULT_UNIT,
              (uint32)feature,
              (uint32)sbp_status_features.sbp_id,
              MON_HALT);

    sbp_data_ptr->c2k_sbp_data_config[feature] = data;
}

/*****************************************************************************
* FUNCTION
*   sbp_query_sbp_id()
*
* DESCRIPTION
*   This function is used to query SBP ID received by modem
*
* PARAMETERS
*   [IN]    N/A
*
* RETURNS
*   the item in c2k_sbp_id_enum as value of SBP ID
*****************************************************************************/
c2k_sbp_id_enum sbp_query_sbp_id(void)
{
    return sbp_status_features.sbp_id;
}

/*****************************************************************************
 * FUNCTION
 *  sbp_set_sbp_id
 * DESCRIPTION
 * Set SBP id by operator macro, this function is only used for Modis IT/UT and
 * target which doesn't support CCCI.
 * PARAMETERS
 *  void
 * RETURNS
 *   void
 *****************************************************************************/
void sbp_set_sbp_id(void)
{
    c2k_sbp_id_enum sbp_id = SBP_ID_MAX_NUMBER;
    static bool     sbp_id_set = FALSE;

    if(!sbp_id_set)
    {
#if defined(CHINATELECOM_EXTENSIONS)
        sbp_id = SBP_ID_CHINATELECOM;
#elif defined(VERIZON_EXTENSIONS)
        sbp_id = SBP_ID_VERIZON;
#else
        sbp_id = SBP_ID_GENERIC;
#endif
        custom_nvram_set_sbp_id(sbp_id);

        sbp_id_set = TRUE;
    }
}

/*****************************************************************************
 * FUNCTION
 *  custom_nvram_set_sbp_id
 * DESCRIPTION
 * Set SBP features and data according to SBP ID.
 * PARAMETERS
 *  sbp_id  [IN]
 * RETURNS
 *   TRUE    : Set SBP ID successfully
 *   FALSE   : Error happens when setting SBP ID
 *****************************************************************************/
void custom_nvram_set_sbp_id(c2k_sbp_id_enum sbp_id)
{
    nvram_ef_c2k_sbp_status_config_struct   sbp_status_feature_buf;

    nvram_ef_c2k_sbp_data_config_struct     sbp_data_feature_buf;

#ifdef MTK_DEV_CCCI_FS /* the following code is activated only if NVRAM interface is ready to use. */
    uint8 c2k_nam1_buf[sizeof(PswIs95NamT)];
    PswIs95NamT *c2k_nam1_p = NULL;
#endif

    MonAssert((sbp_id < SBP_ID_MAX_NUMBER), MON_DBM_FAULT_UNIT, sbp_id, 0, MON_CONTINUE);

#ifdef MTK_DEV_CCCI_FS
    /* Read current SBP status features setting from NVRAM */
    NvramExternalReadData(NVRAM_EF_C2K_SBP_STATUS_CONFIG_LID,
                          1,
                          (uint8*)&sbp_status_feature_buf,
                          NVRAM_EF_C2K_SBP_STATUS_CONFIG_SIZE);

    /* Read current SBP data features setting from NVRAM */
    NvramExternalReadData(NVRAM_EF_C2K_SBP_DATA_CONFIG_LID,
                          1,
                          (uint8*)&sbp_data_feature_buf,
                          NVRAM_EF_C2K_SBP_DATA_CONFIG_SIZE);

    if((sbp_status_feature_buf.first_boot) ||
       (sbp_id != sbp_status_feature_buf.sbp_id))
#endif
    {
        sbp_status_feature_buf  = NVRAM_EF_C2K_SBP_STATUS_CONFIG_DEFAULT[0];
        sbp_data_feature_buf    = NVRAM_EF_C2K_SBP_DATA_CONFIG_DEFAULT[0];

        sbp_status_feature_buf.first_boot = FALSE;
        sbp_status_feature_buf.sbp_id = sbp_id;

        /* Update related NVRAM files if received SBP ID is not 0 */
        switch (sbp_id)
        {
            case SBP_ID_GENERIC:
                sbp_set_status_feature(SBP_STATUS_CT_AUTO_REG_WHEN_MCC_CHANGED, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_POWERUP_1X_SCAN, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_SYS_REG_THROTTLING_ENHANCE, FALSE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_SESSION_NEGO_OPTM_POWERON, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);

                break;

            /* for CT. */
            case SBP_ID_CHINATELECOM:
            {
                #if defined(VERIZON_EXTENSIONS) || \
                    defined(SMARTFREN_EXTENSIONS) || \
                    defined(SPRINT_EXTENSIONS) || \
                    defined(LGT_EXTENSIONS) || \
                    defined(KDDI_EXTENSIONS)

                MonAssert(FALSE,
                          MON_SBP_FAULT_UNIT,
                          sbp_id,
                          0,
                          MON_HALT);
                #endif

                sbp_set_status_feature(SBP_STATUS_CHINATELECOM_CARDLOCK, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_CHINATELECOM_EXTENSIONS, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_MIP, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_CT_AUTO_REG_WHEN_MCC_CHANGED, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_POWERUP_1X_SCAN, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_DEACTIVATE_C2K_WHEN_UIM_ERROR, FALSE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_VZW_LTE_MMO_SCAN, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_SESSION_NEGO_OPTM_POWERON, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);

#ifdef MTK_DEV_CCCI_FS /* the following code is activated only if NVRAM interface is ready to use. */
                /* Read NAM in NVRAM */
                NvramExternalReadData(NVRAM_EF_PSW_NAM1_LID,
                                      1,
                                      c2k_nam1_buf,
                                      sizeof(PswIs95NamT));

                c2k_nam1_p = (PswIs95NamT *)c2k_nam1_buf;
                c2k_nam1_p->CPCA = 283;
                c2k_nam1_p->CSCA = 691;
                c2k_nam1_p->CPCB = 384;
                c2k_nam1_p->CSCB = 777;
                c2k_nam1_p->IMSI_Mp.mcc = PSW_DEFAULT_IMSI_M_MCC;
                c2k_nam1_p->IMSI_Mp.imsi_11_12 = PSW_DEFAULT_IMSI_11_12;

                NvramExternalWriteData(NVRAM_EF_PSW_NAM1_LID,
                                       1,
                                       c2k_nam1_buf,
                                       sizeof(PswIs95NamT));
#ifdef CBP7_EHRPD
                /* set CT specific eHRPD default value in NVRAM */
                {
                    HlpEHrpdSegDataT *pEhrpdSegData;
                    CpBufferT *pCpBuf;

                    pCpBuf = CpBufGet(sizeof(HlpEHrpdSegDataT), CPBUF_REV);
                    if (pCpBuf)
                    {
                        pEhrpdSegData = (HlpEHrpdSegDataT *)pCpBuf->dataPtr;
                        NvramExternalReadData(NVRAM_EF_EHRPD_LID,
                                              1,
                                              (uint8*)pEhrpdSegData,
                                              sizeof(HlpEHrpdSegDataT));
                        pEhrpdSegData->pcmt_val_ehrpd = 600; /* CT, use 600 seconds */
                        pEhrpdSegData->pcmt_val_irat = 600; /* CT, use 600 seconds */
                        NvramExternalWriteData(NVRAM_EF_EHRPD_LID,
                                               1,
                                               (uint8*)pEhrpdSegData,
                                               sizeof(HlpEHrpdSegDataT));
                        CpBufFree(pCpBuf);
                    }
                    else
                    {
                        MonFault(MON_DBM_FAULT_UNIT, DBM_MSG_ID_ERR, 1, MON_CONTINUE);
                    }
                }
#endif
#endif
                break;
            }

            /* for VzW. */
            case SBP_ID_VERIZON:            
            case SBP_ID_OPENMOBILE:
            {
#ifdef MTK_DEV_CCCI_FS /* the following code is activated only if NVRAM interface is ready to use. */
                uint8 css_misc_buf[sizeof(cssMiscDbmSegment)];
                cssMiscDbmSegment *css_misc_p = NULL;
                uint8 css_1x_buf[sizeof(css1xDbmSegment)];
                css1xDbmSegment *css_1x_p = NULL;
                uint16 home_eri_vals[] = {1, 64, 65, 76, 77, 78, 79, 80, 81, 82, 83};
#endif
                sbp_set_status_feature(SBP_STATUS_SYS_ENH_SYSTEM_SELECT_FEATURE, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_SYS_CSS_1X_MAPE_HOME_SYS_AVOID_FEATURE, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_SYS_CSS_1X_RESTRICT_SILENTRETRY_TO_SAME_GEO_FEATURE, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_MIP, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_MIP_DMU, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_VZW_DATA_ENHANCEMENT, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_GMSS_MRU, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                /* sbp_set_status_feature(SBP_STATUS_IDLE_GEO_LIST, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf); */
                sbp_set_status_feature(SBP_STATUS_1X_PS_DELAY, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_VZW_TDO, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_VZW_E911, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_SILENT_REDIAL_ON_SIB8, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_VZW_MEID_ME, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_VZW_OTA, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_VZW_SMS, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_VZW_SMS_CMAS, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_DO_APERSISTENCE_AVOID, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_C2KONLY_OOSA_RETRIEVE_OPTIMIZE, FALSE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_HVOLTE, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_VZW_TX_CLOSELOOP, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_VZW_LTE_SIB8_TIMING, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_VZW_DISABLE_EVDO_SUSPEND_TIMING, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_VZW_SMS_BSP, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_VZW_FEMTOCELL_FEATURE, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_VZW_LTE_MMO_SCAN, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_SID_MAPPING_MCC, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_LBS_DATA_LOGGING, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_SESSION_NEGO_OPTM_POWERON, FALSE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_VZW_PSW_BYPASS_BAD_SECTOR, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_SO2_OPEN_GPS_EARLY, FALSE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_VZW_INTERNATIONAL_ROAMING_REG, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_VZW_REFRESH_PROCEED_DURING_THE_CALL, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_PS_REG_SRCH_IN_SAME_CYCLE_OPTI, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_VZW_UNSOLICITED_LOCATION_NOTIFICATION, FALSE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_1XRTT_ENHANCE_SILENT_RETRY, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_1XRTT_ENHANCE_AEHO, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_VZW_1XRTT_SYSTEM_LOST_SEARCH_FOR_UOI_SR, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_VZW_1XRTT_DISABLE_AHO_OPTIMIZE, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                /* update NAM parameters according to verizon CDMA mode */
#ifdef MTK_DEV_CCCI_FS /* the following code is activated only if NVRAM interface is ready to use. */
                /* Read NAM in NVRAM */
                NvramExternalReadData(NVRAM_EF_PSW_NAM1_LID,
                                      1,
                                      c2k_nam1_buf,
                                      sizeof(PswIs95NamT));

                c2k_nam1_p = (PswIs95NamT *)c2k_nam1_buf;
                c2k_nam1_p->CPCA = 283;
                c2k_nam1_p->CSCA = 691;
                c2k_nam1_p->CPCB = 384;
                c2k_nam1_p->CSCB = 777;
                c2k_nam1_p->IMSI_Mp.mcc = 209;
                c2k_nam1_p->IMSI_Mp.imsi_11_12 = 99;
                c2k_nam1_p->SystemSelect = AUTOMATIC_B;
                /* VZ_REQ_SYSACQ_28592 in Reqs-System Acqusition */

                NvramExternalWriteData(NVRAM_EF_PSW_NAM1_LID,
                                       1,
                                       c2k_nam1_buf,
                                       sizeof(PswIs95NamT));

                NvramExternalReadData(NVRAM_EF_CSS_1X_LID,
                                      1,
                                      css_1x_buf,
                                      sizeof(css1xDbmSegment));

                css_1x_p = (css1xDbmSegment *)css_1x_buf;
                css_1x_p->data.tBsr[CSS_1X_T_BSR_1] = CSS_1X_T_BSR_1_DEFAULT_VZW;
                css_1x_p->data.tBsr[CSS_1X_T_BSR_2] = CSS_1X_T_BSR_2_DEFAULT_VZW;
                css_1x_p->data.tBsr[CSS_1X_T_BSR_NEWSYS] = CSS_1X_T_BSR_NEWSYS_DEFAULT_VZW;
                css_1x_p->data.tBsr[CSS_1X_T_BSR_REDIR]  = CSS_1X_T_BSR_REDIR_DEFAULT_VZW;
                css_1x_p->data.tEmergencySysLostScan  = CSS_1X_EMERGENCY_SYS_LOST_SCAN_TIME_VZW;
                css_1x_p->checksum = 0;
                css_1x_p->checksum = calcChecksum((uint8 *)css_1x_p, sizeof(css1xDbmParameters));

                NvramExternalWriteData(NVRAM_EF_CSS_1X_LID,
                                       1,
                                       css_1x_buf,
                                       sizeof(css1xDbmSegment));

                NvramExternalReadData(NVRAM_EF_CSS_MISC_LID,
                                      1,
                                      css_misc_buf,
                                      sizeof(cssMiscDbmSegment));

                css_misc_p = (cssMiscDbmSegment *)css_misc_buf;
                css_misc_p->data.roamIndForNonPrlSysButHomeInNam = ROAMING_IND_FLASHING;
                css_misc_p->data.homeEriValNum = sizeof(home_eri_vals)/sizeof(uint16);
                memcpy(css_misc_p->data.homeEriVals, home_eri_vals, sizeof(home_eri_vals));
                css_misc_p->data.intlEriValNum = 0;
                css_misc_p->checksum = 0;
                css_misc_p->checksum = calcChecksum((uint8 *)css_misc_p, sizeof(cssMiscDbmSegment));

                NvramExternalWriteData(NVRAM_EF_CSS_MISC_LID,
                                       1,
                                       css_misc_buf,
                                       sizeof(cssMiscDbmSegment));
#endif
                break;
            }

            case SBP_ID_SPRINT:
            case SBP_ID_ACG:
            {
#ifdef MTK_DEV_CCCI_FS /* the following code is activated only if NVRAM interface is ready to use. */
                uint8 css_misc_buf[sizeof(cssMiscDbmSegment)];
                cssMiscDbmSegment *css_misc_p = NULL;
                uint8 css_1x_buf[sizeof(css1xDbmSegment)];
                css1xDbmSegment *css_1x_p = NULL;
                uint8 css_do_buf[sizeof(cssDODbmSegment)];
                cssDODbmSegment *css_do_p = NULL;
                uint16 home_eri_vals[] = {1};
                uint16 International_eriValues[] = {74,
                                                    124,125,126,
                                                    157,158,159,
                                                    193,194,195,196,197,198,
                                                    228,229,230,231,232,233,234,235};
#endif
                sbp_set_status_feature(SBP_STATUS_SYS_CSS_1X_MAPE_HOME_SYS_AVOID_FEATURE, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_MIP, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_MIP_DMU, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_VZW_DATA_ENHANCEMENT, FALSE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_VZW_OTA, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_SPRINT_QC_REDIRECT, FALSE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_GMSS_MRU, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_VZW_TDO, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_VZW_E911, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_SPRINT_1X_SYS_LOST, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_SPRINT_BSR, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_CDG_BSR, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_CDG_AVOIDANCE, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_SPRINT_TRAFF_NEW_CH_NOT_LOGGING_MRU, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_SPRINT_PERSISTENCE_FAIL, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_SPRINT_MO_SMS_BREAK_BSR, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_SPRINT_SMS, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_SPRINT_AT_CMD, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_PRL_ENHANCE_FOR_INT_ROAM, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_CDG143_MAPE_SYS_AVOID_FEATURE, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_VOICE_ROAM_BARRING, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_USE_RESTRICTIVE_SYSTEM_MATCH_FEATURE, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_ASSIGN_MRU0_IF_EMPTY, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_SPRINT_SMS_DISABLE_SO_NEG, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_SPRINT_SMS_CMAS, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_SESSION_NEGO_OPTM_POWERON, FALSE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_SPRINT_OPTM_WARM_RESET, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_VZW_SMS, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_VZW_TX_CLOSELOOP, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_C2KONLY_OOSA_RETRIEVE_OPTIMIZE, FALSE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_SPRINT_REPORT_SO, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                /*sbp_set_status_feature(SBP_STATUS_SPRINT_BSR_DATA_HOLD, FALSE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);*/
                sbp_set_status_feature(SBP_PAGING_MONITOR_TIMEOUT_TO_GEOSCAN, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_DO_IRAT_PRIORITY_DOWNGRADING, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_SPRINT_CALL_RELEASE, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_DO_PRL_LIST_ASSOCIATED_ONLY, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_SPRINT_TRAFF_NEW_CH_LOGGING_LAST_CHANNEL, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_BAR_DATA_ROAMING, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_VZW_LTE_MMO_SCAN, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                /* open GPS data logging feature for Sprint. */
                sbp_set_status_feature(SBP_STATUS_LBS_DATA_LOGGING, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_SPRINT_DDTM, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_STATUS_VZW_LTE_SIB8_TIMING, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);

                sbp_set_status_feature(SBP_ACG_OPS_TETHERING, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                /* update NAM parameters according to Sprint CDMA mode */
#ifdef MTK_DEV_CCCI_FS /* the following code is activated only if NVRAM interface is ready to use. */
                /* Read NAM in NVRAM */
                NvramExternalReadData(NVRAM_EF_PSW_NAM1_LID,
                                      1,
                                      c2k_nam1_buf,
                                      sizeof(PswIs95NamT));

                c2k_nam1_p = (PswIs95NamT *)c2k_nam1_buf;
                c2k_nam1_p->CPCA = 283;
                c2k_nam1_p->CSCA = 691;
                c2k_nam1_p->CPCB = 384;
                c2k_nam1_p->CSCB = 777;
                c2k_nam1_p->IMSI_Mp.mcc = 209;
                c2k_nam1_p->IMSI_Mp.imsi_11_12 = 99;
                c2k_nam1_p->SystemSelect = AUTOMATIC;
                c2k_nam1_p->RoamingSetting = PSW_DEFAULT_ROAM_SETTING_SPRINT; /*Default disallowed domestic data roaming and LTE data roaming*/
                /*#define ROAM_SETTING_DOMESTIC_VOICE 0x01    Sprint Allow voice roaming on domestic system */
                /*#define ROAM_SETTING_DOMESTIC_DATA  0x02    Sprint Allow data roaming on domestic system */
                /*#define ROAM_SETTING_INT_VOICE          0x04    Sprint Allow voice romaing on international system */
                /*#define ROAM_SETTING_INT_DATA           0x08     Sprint Allow data roaming on international system */
                /*#define ROAM_SETTING_LTE_DATA           0x10     Sprint Allow LTE data roaming, not used in C2K saved only for AP get */

                NvramExternalWriteData(NVRAM_EF_PSW_NAM1_LID,
                                       1,
                                       c2k_nam1_buf,
                                       sizeof(PswIs95NamT));

                NvramExternalReadData(NVRAM_EF_CSS_1X_LID,
                                      1,
                                      css_1x_buf,
                                      sizeof(css1xDbmSegment));

                css_1x_p = (css1xDbmSegment *)css_1x_buf;
                css_1x_p->data.tBsr[CSS_1X_T_BSR_1] = CSS_1X_T_BSR_1_DEFAULT_SPRINT;
                css_1x_p->data.tBsr[CSS_1X_T_BSR_2] = CSS_1X_T_BSR_2_DEFAULT_SPRINT;
                css_1x_p->data.tBsr[CSS_1X_T_BSR_NEWSYS] = CSS_1X_T_BSR_NEWSYS_DEFAULT_SPRINT;
                css_1x_p->data.tBsr[CSS_1X_T_BSR_REDIR]  = CSS_1X_T_BSR_REDIR_DEFAULT_SPRINT;
                css_1x_p->data.tBsr[CSS_1X_T_BSR_CALL]   = CSS_1X_T_BSR_CALL_DEFAULT_SPRINT;
                css_1x_p->data.tBsr[CSS_1X_T_BSR_HOLD]   = CSS_1X_T_BSR_HOLD_DEFAULT_SPRINT;
                css_1x_p->data.tAvoidance[CSS_1X_T_MAPE_HOME_AVOIDANCE]  = CSS_1X_HOME_MAPE_AVOIDANCE_DURATION_DEFAULT_SPRINT;
                css_1x_p->checksum = 0;
                css_1x_p->checksum = calcChecksum((uint8 *)css_1x_p, sizeof(css1xDbmParameters));

                NvramExternalWriteData(NVRAM_EF_CSS_1X_LID,
                                       1,
                                       css_1x_buf,
                                       sizeof(css1xDbmSegment));

                NvramExternalReadData(NVRAM_EF_CSS_DO_LID,
                                      1,
                                      css_do_buf,
                                      sizeof(cssDODbmSegment));

                css_do_p = (cssDODbmSegment *)css_do_buf;
                css_do_p->data.tBsr[CSS_DO_T_BSR_1] = CSS_DO_T_BSR_1_DEFAULT_SPRINT;
                css_do_p->data.tBsr[CSS_DO_T_BSR_2] = CSS_DO_T_BSR_2_DEFAULT_SPRINT;
                css_do_p->data.tBsr[CSS_DO_T_BSR_HYBRID] = CSS_DO_T_BSR_HYBRID_DEFAULT_SPRINT;
                css_do_p->data.tBsr[CSS_DO_T_BSR_REDIR]  = CSS_DO_T_BSR_REDIR_DEFAULT_SPRINT;
                css_do_p->data.tBsr[CSS_DO_T_BSR_CALL]   = CSS_DO_T_BSR_CALL_DEFAULT_SPRINT;
                css_do_p->data.tBsr[CSS_DO_T_BSR_HOLD]   = CSS_DO_T_BSR_HOLD_DEFAULT_SPRINT;
                css_do_p->checksum = 0;
                css_do_p->checksum = calcChecksum((uint8 *)css_do_p, sizeof(cssDODbmParameters));

                NvramExternalWriteData(NVRAM_EF_CSS_DO_LID,
                                       1,
                                       css_do_buf,
                                       sizeof(cssDODbmSegment));


                NvramExternalReadData(NVRAM_EF_CSS_MISC_LID,
                                      1,
                                      css_misc_buf,
                                      sizeof(cssMiscDbmSegment));

                css_misc_p = (cssMiscDbmSegment *)css_misc_buf;
                css_misc_p->data.roamIndForNonPrlSysButHomeInNam = ROAMING_IND_OFF;
                css_misc_p->data.homeEriValNum = sizeof(home_eri_vals)/sizeof(uint16);
                memcpy(css_misc_p->data.homeEriVals, home_eri_vals, sizeof(home_eri_vals));
                css_misc_p->data.intlEriValNum = sizeof(International_eriValues)/sizeof(uint16);
                memcpy(css_misc_p->data.intlEriVals, International_eriValues, sizeof(International_eriValues));
                css_misc_p->checksum = 0;
                css_misc_p->checksum = calcChecksum((uint8 *)css_misc_p, sizeof(cssMiscDbmSegment));

                NvramExternalWriteData(NVRAM_EF_CSS_MISC_LID,
                                       1,
                                       css_misc_buf,
                                       sizeof(cssMiscDbmSegment));

                /* set sprint specific NV default value */
                {
                    HlpHspdSegData *pHlpHspdSegData;
                    CpBufferT *pCpBuf;
                    uint8 ProfileIndex;

                    // since the size of HlpHspdSegData is pretty large
                    // use dynamic allocated memory to hold it.
                    pCpBuf = CpBufGet(sizeof(HlpHspdSegData), CPBUF_REV);

                    if (pCpBuf)
                    {
                        pHlpHspdSegData = (HlpHspdSegData*) pCpBuf->dataPtr;
                        NvramExternalReadData(NVRAM_EF_HSPD_LID,
                                1,
                                (uint8*)pHlpHspdSegData,
                                sizeof(HlpHspdSegData));
                        pHlpHspdSegData->MN_HA_AUTH = RFC2002;
                        pHlpHspdSegData->DataTrtlEnabled = FALSE;
                        pHlpHspdSegData->RRA = HLP_DEFAULT_SPRINT_RRA;
                        for (ProfileIndex = 0; ProfileIndex < HLP_MAX_HSPD_PROFILES; ProfileIndex++)
                        {
                            pHlpHspdSegData->ProfileData[ProfileIndex].HA_PRI_IP_ADDR = HLP_DEFAULT_SPRINT_HA_PRI_IP_ADDR;
                            pHlpHspdSegData->ProfileData[ProfileIndex].HA_SEC_IP_ADDR = HLP_DEFAULT_SPRINT_HA_SEC_IP_ADDR;
                        }
                        NvramExternalWriteData(NVRAM_EF_HSPD_LID,
                               1,
                               (uint8*)pHlpHspdSegData,
                               sizeof(HlpHspdSegData));
                        CpBufFree(pCpBuf);
                    }
                    else
                    {
                        MonFault(MON_DBM_FAULT_UNIT, DBM_MSG_ID_ERR, 0, MON_CONTINUE);
                    }
                }

#ifdef CBP7_EHRPD
                /* set SPRINT specific eHRPD default value in NVRAM */
                {
                    HlpEHrpdSegDataT *pEhrpdSegData;
                    CpBufferT *pCpBuf;

                    pCpBuf = CpBufGet(sizeof(HlpEHrpdSegDataT), CPBUF_REV);
                    if (pCpBuf)
                    {
                        pEhrpdSegData = (HlpEHrpdSegDataT *)pCpBuf->dataPtr;
                        NvramExternalReadData(NVRAM_EF_EHRPD_LID,
                                              1,
                                              (uint8*)pEhrpdSegData,
                                              sizeof(HlpEHrpdSegDataT));
                        pEhrpdSegData->pcmt_val_ehrpd = 3600; /* As per GTR-DATA-GSS-Ver1.2, the default value is 3600 seconds */
                        pEhrpdSegData->pcmt_val_irat = 3600; /* As per GTR-DATA-GSS-Ver1.2, the default value is 3600 seconds */
                        NvramExternalWriteData(NVRAM_EF_EHRPD_LID,
                                               1,
                                               (uint8*)pEhrpdSegData,
                                               sizeof(HlpEHrpdSegDataT));
                        CpBufFree(pCpBuf);
                    }
                    else
                    {
                        MonFault(MON_DBM_FAULT_UNIT, DBM_MSG_ID_ERR, 1, MON_CONTINUE);
                    }
                }
#endif
#endif

                break;
            }
            /* for CMCC */
            case SBP_ID_CMCC:
            {
                sbp_set_status_feature(SBP_STATUS_OP01_LOCK_PROTECT, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                sbp_set_status_feature(SBP_DEACTIVATE_C2K_WHEN_UIM_ERROR, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
                break;
            }
            default:
                break;
        }

        /* For tablet project, enable or disable 1xRTT voice call function */
#ifdef MTK_DEV_C2K_DISABLE_VOICE_CALL
        sbp_set_status_feature(SBP_1XRTT_DISABLE_VOICE_CALL, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
#endif

        /**Enable Voice Gating always no matter what Version (OM or CT)*/
        sbp_set_status_feature(SBP_FORCE_VOICE_GATING_ENABLE, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);

       /*For SLT Load, always auto turn on modem, remove cardlock*/
#ifdef MTK_DEV_SLT
       sbp_set_status_feature(SBP_STATUS_MODEM_TURNON_AUTO, TRUE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
       sbp_set_status_feature(SBP_STATUS_CHINATELECOM_CARDLOCK, FALSE, (nvram_ef_c2k_sbp_status_config_struct*)&sbp_status_feature_buf);
#endif
#ifdef MTK_DEV_CCCI_FS
        /* Write the new settings back to NVRAM. */
        NvramExternalWriteData(NVRAM_EF_C2K_SBP_STATUS_CONFIG_LID,
                               1,
                               (uint8*)&sbp_status_feature_buf,
                               NVRAM_EF_C2K_SBP_STATUS_CONFIG_SIZE);

        NvramExternalWriteData(NVRAM_EF_C2K_SBP_DATA_CONFIG_LID,
                               1,
                               (uint8*)&sbp_data_feature_buf,
                               NVRAM_EF_C2K_SBP_DATA_CONFIG_SIZE);

#endif
    }

    memcpy(&sbp_status_features, &sbp_status_feature_buf, sizeof(nvram_ef_c2k_sbp_status_config_struct));

    memcpy(&sbp_data_features, &sbp_data_feature_buf, sizeof(nvram_ef_c2k_sbp_data_config_struct));

    /*print sbp id trace, only CP fault trace is available during NVRAM initialization.*/
    MonFault(MON_DBM_FAULT_UNIT, DBM_MSG_ID_ERR, sbp_status_features.sbp_id, MON_CONTINUE);
}

/*****************************************************************************
 * FUNCTION
 *  sbp_set_tk_bsp_feature
 * DESCRIPTION
 * Set current mode is Turkey or BSP for customer.
 * PARAMETERS
 *  tk_bsp, enum, TK or BSP
 * RETURNS
 *   void
 *****************************************************************************/
void sbp_set_solution_feature(c2k_sbp_soluiton_enum solu)
{
    if (SBP_SOLUTION_TK == solu)
    {
        sbp_set_status_feature(SBP_STATUS_VZW_SMS_BSP, FALSE, &sbp_status_features);
    }
    else if (SBP_SOLUTION_BSP == solu)
    {
        sbp_set_status_feature(SBP_STATUS_VZW_SMS_BSP, TRUE, &sbp_status_features);
    }
    else
    {
        MonFault(MON_DBM_FAULT_UNIT, DBM_MSG_ID_ERR, solu, MON_CONTINUE);
    }
}

/*****************************************************************************
* End of File
*****************************************************************************/
